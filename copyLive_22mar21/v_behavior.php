 
    <style type="text/css">
.highcharts-tooltip h3 {
    margin: 0.3em 0;
}
    </style> 
<script src="<?php echo base_url("js/code/highcharts.js"); ?>" ></script> 
<script src="<?php echo base_url("js/code/highcharts-more.js"); ?>" ></script> 
<script src="<?php echo base_url("js/code/modules/exporting.js"); ?>" ></script> 
<script src="<?php echo base_url("js/code/modules/export-data.js"); ?>" ></script> 
<!--<script src="../../code/modules/exporting.js"></script>
<script src="../../code/modules/export-data.js"></script>-->
<section class="content-header">
  <h1>
  Coating Behavior
  <small></small>
  </h1>
</section>
<section class="content">

  <div class="row">
    <div class="col-xs-12">



      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <div id="formAction">
          <form id="formData" method="post" action="<?php echo site_url("Coating_behavior") ?>" target="_blank">
            <div class="form-group row">

              <div class="form-group col-sm-12 col-sm-3">
                <label for="ID_COMPANY">COMPANY</label>
                <?php if($this->USER->ID_COMPANY): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_COMPANY; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_COMPANY" name="" VALUE="<?php echo $this->USER->ID_COMPANY; ?>" readonly class="form-control" />
                <?php else: ?>
                <select id="ID_COMPANY" class="form-control select2">
                  <?php  foreach($this->list_company as $company): ?>
                    <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>
                <?PHP endif; ?>
              </div>

              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_COMPANY">PLANT</label>
                <?php if($this->USER->ID_PLANT): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_PLANT; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_PLANT" name="" VALUE="<?php echo $this->USER->ID_PLANT; ?>" readonly class="form-control" />
                <?php else: ?>
                <select id="ID_PLANT" class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_AREA">AREA</label>
                <?php if($this->USER->ID_AREA): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_AREA; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_AREA" name="ID_AREA" VALUE="<?php echo $this->USER->ID_AREA; ?>" readonly class="form-control" />
                <?php else: ?>
                <select id="ID_AREA" name="ID_AREA" class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>

            </div>

            <div class="form-group row">

              <div class="form-group col-sm-12 col-sm-3">
                <label for="ID_COMPANY">START DATE</label>
                <INPUT TYPE="TEXT" id="STARTDATE" NAME="STARTDATE" VALUE="" readonly class="form-control" />

              </div>

              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_COMPANY">END DATE</label>
                <INPUT TYPE="TEXT" id="ENDDATE" name="ENDDATE" VALUE="" readonly class="form-control" />

              </div>

            </div>
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn-primary" name="load" id="btLoad">VIEW DATA</button> &nbsp;
                <button class="btn-success" name="load2" id="livePreview">LIVE</button>
              </div>
            </div>
            <hr/>


          </form>
        </div>

          <div id="divTable" style="display:none;">
            <div class="form-group row">
              <div class="col-sm-12">
                <button class="btn-reset" onclick="refresh()">RESET</button>
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span> 
              </div>
            </div>
          </div>

       <div class="row">
        <div id="container" style="height: 655px; width: 880px; margin: 0 auto"></div>
        </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>


    <script type="text/javascript">
    
    </script> 

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }

  canvas {
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  }

</style>


<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery-ui.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<script src="<?php echo base_url("js/chartjs/Chart.bundle.js") ?>"></script>
<script src="<?php echo base_url("js/chartjs/utils.js") ?>"></script>

<script>
 
  $(document).ready(function(){

  $("#STARTDATE").datepicker({
    dateFormat: 'dd/mm/yy'
  });

  $("#ENDDATE").datepicker({
    dateFormat: 'dd/mm/yy'
  });

    $("#ID_COMPANY").change(function(){
      var company = $(this).val(), plant = $('#ID_PLANT');
      $.getJSON('<?php echo site_url("incident/ajax_get_plant/");?>' + company, function (result) {
        var values = result;

        plant.find('option').remove();
        if (values != undefined && values.length > 0) {
          plant.css("display","");
          $(values).each(function(index, element) {
            plant.append($("<option></option>").attr("value", element.ID_PLANT).text(element.NM_PLANT));
          });

        }else{
          plant.find('option').remove();
          plant.append($("<option></option>").attr("value", '00').text("NO PLANT"));
        }
      $("#ID_PLANT").change();
      });
    });

    $("#ID_PLANT").change(function(){
      var plant = $(this).val(), area = $('#ID_AREA');
      $.getJSON('<?php echo site_url("Coating_behavior/ajax_get_area/");?>' + $("#ID_COMPANY").val() + '/' + plant, function (result) {
        var values = result;

        area.find('option').remove();
        if (values != undefined && values.length > 0) {
          area.css("display","");
          $(values).each(function(index, element) {
            area.append($("<option></option>").attr("value", element.ID_AREA).text(element.NM_AREA));
          });
        }else{
          area.find('option').remove();
          area.append($("<option></option>").attr("value", '00').text("NO AREA"));
        }
      });
    });


 jQuery(document).ready(function ($) {
    $("#btLoad").click(function(event){
      event.preventDefault();
      var formData = $("#formData").serializeArray();
    var display = $("#DISPLAY").val();
      $("#saving").css('display','');
      $("#divTable").css("display","");
      // $("#formAction").css("display","none");
      $('#formAction').hide();
 

      $.ajax({
        url: "<?php echo site_url('Coating_behavior/get_ajax');?>",
          type: 'POST',
          data:formData,
          dataType: 'JSON'
      }).then(function (data) {
        $("#saving").css('display','none');
        var result = data;
        var NM_AREA = result.nama_area;
        // console.log(NM_AREA);
        // var datass = [{ x: 2.5, y: 1.6, z: 30, color:'black'},];
        var datass = [{  x: 2.5, y: 1.6, z: 30 , marker: {
                          symbol: 'url(http://karirmu.web.id/wp-content/uploads/2018/10/pusat.png)',
                          lineColor: 'blue',
                          fillColor: 'blue'
                      },
                    dataLabels: {
                        enabled: true,
                        format: 'NORMAL<br><p style="color:yellow;">COATING</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }},{  x: 1.5, y: 1, z: 1 , fillColor:'transparent', color:'white'},];
        for (var i = 0; i < result.data.length; i++) {
          // console.log(result.data[i][0] +"-"+result.data[i][1]+"-"+result.data[i][2]);
          // var datas = { x: Number(result.data[i][2]), y: Number(result.data[i][3]), z: 1, color:'blue', fillColor:'blue', country: result.data[i][0], name: result.data[i][1]};

             var datas = { x: Number(result.data[i][2]), y: Number(result.data[i][3]), z: 1.02, marker: {
                          symbol: 'url(http://karirmu.web.id/wp-content/uploads/2018/10/blue.png)',
                          lineColor: 'blue',
                          fillColor: 'blue'
                      }, color:'blue', fillColor: 'blue', country: result.data[i][0], name: 'JAM '+result.data[i][1]};
          datass.push(datas);
        }

        Highcharts.chart('container', {
          chart: {
            type: 'bubble',
            plotBorderWidth: 1
            // zoomType: 'xy'
          },

          legend: {
            enabled: false
          },

          title: {
            text: 'Coating Behaviour'
          },

          subtitle: {
            text: NM_AREA
          },

          xAxis: {
            gridLineWidth: 1,
            title: {
              text: 'SIM'
            },
            min :1.5,
            tickInterval:0.1,
            max : 3.6,
            plotLines: [{
              color: 'black',
              dashStyle: 'dot',
              width: 2,
              value: 2.5,
              label: {
                rotation: 0,
                y: 1.5,
                style: {
                  fontStyle: 'italic'
                }
              },
              zIndex: 9
            }]
          },

          yAxis: {
            startOnTick: false,
            endOnTick: false,
            title: {
              text: 'ALM'
            },
            labels: {
              format: '{value}'
            },
            maxPadding: 0.2,
            min :1.0,
            tickInterval:0.1,
            max : 2.4,
              plotLines: [{
              color: 'black',
              dashStyle: 'dot',
              width: 2,
              value: 1.6,
              label: {
                align: 'right',
                style: {
                  fontStyle: 'italic'
                },
              },
            }]
          },

          tooltip: {
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><th colspan="2"><h3>{point.country}</h3></th></tr>' +
              '<tr><th>SIM:</th><td>{point.x}</td></tr>' +
              '<tr><th>ALM:</th><td>{point.y}</td></tr>' +
              '<tr><th>TIME:</th><td>{point.name}</td></tr>',
            footerFormat: '</table>',
            followPointer: true
          }, 
                
          plotOptions: {
            series: {
              dataLabels: {
                enabled: false,
                format: '{point.name}'
              }
            },
            bubble:{
              color:'transparent',  
              // minSize:4,
              maxSize:227
            }
          },

          series: [{
            data: datass,
            markerSize: 20,
            Color : "red", //change color here
            markerBorderThickness: 2,
          },{
            data: [

                {
                   x: 1.8,
                    y: 2.2,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Thick coating. <br>Clinker ball <br>or sinterring formation <br><p style="color:yellow;">(Plenty of viscous </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 3.3,
                    y: 2.2,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Very thin coating <br>Dusty clinker <br><p style="color:yellow;">(Litle of viscous </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 1.8,
                    y: 1.3,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Thin coating <br>Refractories attack <br><p style="color:yellow;">(Plenty of fluid </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 3.3,
                    y: 1.3,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: '<br><br>Thin coating <br><p style="color:yellow;">(Litle of fluid </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },
          ]

        });
      });
});



    });


jQuery(document).ready(function ($) {
    $("#livePreview").click(function(event){

      event.preventDefault();
      var formData = $("#formData").serializeArray();
      var display = $("#DISPLAY").val();
      $("#saving").css('display','');
      $("#divTable").css("display","");
      // var warna = ["red", "green", "yellow", "blue", "blue", "blue", "blue", "blue"];
      var warna = ["red", "blue", "blue", "blue", "blue", "blue", "blue", "blue"];
      $('#formAction').hide();
      // console.log(warna);


 
        setInterval(function() {
      $.ajax({
        url: "<?php echo site_url('Coating_behavior/get_ajax_live');?>",
          type: 'POST',
          data:formData,
          dataType: 'JSON'
      }).then(function (data) {
        $("#saving").css('display','none');
        var result = data;
        var NM_AREA = result.nama_area;
        var datass = [{  x: 2.5, y: 1.6, z: 30 , marker: {
                          symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/pusat.png)',
                          lineColor: 'blue',
                          fillColor: 'blue'
                      },
                    dataLabels: {
                        enabled: true,
                        format: 'NORMAL<br><p style="color:yellow;">COATING</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }},{  x: 1.5, y: 1, z: 1 , fillColor:'transparent', color:'white'},];
// console.log(result.data);
        for (var i = 0; i < result.data.length; i++) {
          // console.log(result[i][2]);
          // var datas = { x: Number(result.data[i][0]), y: Number(result.data[i][1]), z: 1, color:'blue', fillColor:'transparent', country: result.data[i][2], name: result.data[i][3]};

             var datas = { x: Number(result.data[i][0]), y: Number(result.data[i][1]), z: 1.02, marker: {
                          symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/blue.png)',
                          lineColor: 'blue',
                          fillColor: 'blue'
                      }, color:'blue', fillColor: 'blue', country: result.data[i][2], name: 'JAM '+result.data[i][3]};
          datass.push(datas);
        }
         // datass[result.data.length+1].color = 'red';
         datass[result.data.length+1].marker= {
                          symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/3D-merah.png)',
                          lineColor: 'red',
                          fillColor: 'red'
                      };

        // console.log(datass);
        Highcharts.chart('container', {
          chart: {
            type: 'bubble',
            // plotBorderWidth: 1
            // zoomType: 'xy'
          },

          legend: {
            enabled: false
          },

          title: {
            text: 'Coating Behaviour'
          },

          subtitle: {
            text: NM_AREA
          },

          xAxis: {
            gridLineWidth: 1,
            title: {
              text: 'SIM'
            },
            min :1.5,
            tickInterval:0.1,
            max : 3.6,
            plotLines: [{
              color: 'black',
              dashStyle: 'dot',
              width: 2,
              value: 2.5,
              label: {
                rotation: 0,
                y: 1.5,
                style: {
                  fontStyle: 'italic'
                }
              },
              zIndex: 9
            }]
          },

          yAxis: {
            startOnTick: false,
            endOnTick: false,
            title: {
              text: 'ALM'
            },
            labels: {
              format: '{value}'
            },
            maxPadding: 0.2,
            min :1.0,
            tickInterval:0.1,
            max : 2.4,
              plotLines: [{
              color: 'black',
              dashStyle: 'dot',
              width: 2,
              value: 1.6,
              label: {
                align: 'right',
                style: {
                  fontStyle: 'italic'
                },
              },
            }]
          },

          tooltip: {
            useHTML: true,
            headerFormat: '<table>',
            pointFormat: '<tr><th colspan="2"><h3> {point.country}</h3></th></tr>' +
              '<tr><th>SIM:</th><td> {point.x}</td></tr>' +
              '<tr><th>ALM:</th><td> {point.y}</td></tr>' +
              '<tr><th>JAM:</th><td> {point.name}</td></tr>',
            footerFormat: '</table>',
            followPointer: true
          }, 
                
          plotOptions: {
            series: {
              dataLabels: {
                enabled: true,
                format: ' '
              }
            },
            bubble:{
              color:'transparent',  
              // minSize:4,
              maxSize:227
            }
          },

          series: [{
            data: datass
          },{
            data: [

                {
                    x: 1.8,
                    y: 2.2,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Thick coating. <br>Clinker ball <br>or sinterring formation <br><p style="color:yellow;">(Plenty of viscous </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 3.3,
                    y: 2.2,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Very thin coating <br>Dusty clinker <br><p style="color:yellow;">(Litle of viscous </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 1.8,
                    y: 1.3,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: 'Thin coating <br>Refractories attack <br><p style="color:yellow;">(Plenty of fluid </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },{
            data: [

                {
                    x: 3.3,
                    y: 1.3,
                    z: 23,
                    marker: {
                        symbol: 'url(http://qmo.semenindonesia.com/assets/images/behavior/awan.png)',
                        lineColor: 'white',
                        fillColor: 'rgba(255,0,0,0.5)'
                    },
                    dataLabels: {
                        enabled: true,
                        format: '<br><br>Thin coating <br><p style="color:yellow;">(Litle of fluid </p><br><p style="color:yellow;">melt phase)</p>',
                        labelFontFamily: "tahoma",
                        labelFontSize:'12'
                    }
                }
            ], 
            name: 'Square'
        },


          // { 
          //   data: [
          //     {  x: 2.5, y: 1.6, z: 30 , color:'black', fillColor:'transparent'} 
          //   ]
          // }
          ]

        });
      });
    }, 30000);
});
});



  $("#ID_COMPANY").change();


});

  function refresh() {
    location.reload();
  }
</script>