<?php

class M_laboratorium Extends DB_QM {
	
	public function get_company(){
		$this->db->order_by("URUTAN");
		return $this->db->get("M_COMPANY")->result();
	}

    public function data_laboratorium(){
        $this->db->where("DELETED IS NULL");
		$this->db->order_by("KODE_LAB");
		$this->db->join("M_COMPANY","M_COMPANY.ID_COMPANY = M_LABORATORIUM.ID_COMPANY","LEFT");
		return $this->db->get("M_LABORATORIUM")->result();
    }
	
	public function save_laboratorium(){
		$kode 	 = $this->input->post('kode_lab');
		$nama	 = $this->input->post('nama_lab');
		$com 	 = $this->input->post('company');
		
		$this->db->set("KODE_LAB", $kode);
		$this->db->set("NAMA_LAB", $nama);
		$this->db->set("ID_COMPANY", $com);
		$q = $this->db->insert("M_LABORATORIUM");

		return $q;
	}
	
	public function edit_laboratorium(){
		$id 	 = $this->input->post('id_lab');
		$kode 	 = $this->input->post('kode_lab');
		$nama	 = $this->input->post('nama_lab');
		$com 	 = $this->input->post('company');
		
		$this->db->set("KODE_LAB", $kode);
		$this->db->set("NAMA_LAB", $nama);
		$this->db->set("ID_COMPANY", $com);
		$this->db->where("ID_LAB", $id);
		$q = $this->db->update("M_LABORATORIUM");

		return $q;
	}
	
	public function delete_laboratorium($id){
		
		$this->db->set("DELETED", 1);
		$this->db->where("ID_LAB", $id);
		$q = $this->db->update("M_LABORATORIUM");

		return $q;
	}
    
}

?>