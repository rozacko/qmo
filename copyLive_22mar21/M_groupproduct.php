<?php

class M_Groupproduct Extends DB_QM {
	private $post  = array();
	private $table = "M_GROUPPRODUCT";
	private $pKey  = "ID_GROUPPRODUCT";
	private $column_order = array(NULL, 'ID_GROUPPRODUCT'); //set column for datatable order
    private $column_search = array('NM_GROUPPRODUCT', 'KD_GROUPPRODUCT'); //set column for datatable search 
    private $order = array("ID_GROUPPRODUCT" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
    }

    public function datalist(){
		$this->db->order_by($this->pKey, "ASC");
		return $this->db->get($this->table)->result();
    }

    public function insert($data){
		$this->db->set($data);
		$this->db->set("ID_GROUPPRODUCT","SEQ_ID_GROUPPRODUCT.NEXTVAL",FALSE);
		$this->db->insert("M_GROUPPRODUCT");
    }

    public function update($data,$ID_GROUPPRODUCT){
		$this->db->set($data);
		$this->db->where("ID_GROUPPRODUCT",$ID_GROUPPRODUCT);
		$this->db->update("M_GROUPPRODUCT");
	}
    
    public function get_data_by_id($ID_GROUPPRODUCT){
		$this->db->where("ID_GROUPPRODUCT",$ID_GROUPPRODUCT);
		return $this->db->get("M_GROUPPRODUCT")->row();
    }
    
    public function delete($ID_GROUPPRODUCT){
		$this->db->where("ID_GROUPPRODUCT",$ID_GROUPPRODUCT);
		$this->db->delete("M_GROUPPRODUCT");
	}
    
    public function get_query(){
		$this->db->select('*');
		$this->db->from($this->table);
	}

	public function get_list() {
		$this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}
}