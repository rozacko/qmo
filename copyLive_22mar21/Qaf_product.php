<?php

class Qaf_product extends QMUser {
	
	public $list_config = array();
	public $list_plant = array();
	public $list_company = array();
	public $list_grouparea = array();
	public $list_product = array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $ID_GROUPAREA;
	public $DISPLAY;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("c_qaf_product");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_product");
	}
	
	public function index(){
		$this->ID_COMPANY 	= $this->input->get("id_company");
		$this->ID_PLANT   	= $this->input->get("id_plan");
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist();
		$this->template->adminlte("v_qaf_product");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_PLANT=NULL){
		$data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_COMPANY = $data_plant->ID_COMPANY;		
		$this->list_company = $this->m_company->datalist();
		$this->list_product = $this->m_product->datalist();
		$this->template->adminlte("v_qaf_product_assign");
	}
	
	public function create(){ 
		
		if($this->input->post()){
			$ID_PLANT = $this->input->post("ID_PLANT");
			$ID_GROUPAREA = $this->input->post("ID_GROUPAREA");
			$DISPLAY = $this->input->post("DISPLAY");
			
			IF($ID_GROUPAREA){
				//clean
				$this->c_qaf_product->clean($ID_PLANT,$ID_GROUPAREA,$this->input->post("OPT_PRODUCT")); #echo $this->c_qaf_product->get_sql();
				
				foreach($this->input->post("OPT_PRODUCT") as $ID_PRODUCT){
					$data = false;
					$data['ID_PLANT'] 		= $ID_PLANT;
					$data['ID_GROUPAREA'] 	= $ID_GROUPAREA;
					$data['ID_PRODUCT'] 	= $ID_PRODUCT;
					
					$cek = $this->c_qaf_product->get_where($data); 
					if (count($cek)<1) { 
						@$this->c_qaf_product->insert($data); #echo $this->c_qaf_product->get_sql();
					}
					
				}
				$this->notice->success("QAF Product Configuration Success.");
			}
		}
		redirect("qaf_product/edit/".$ID_PLANT."/".$ID_GROUPAREA);
	}
	
	public function edit($ID_PLANT=NULL,$ID_GROUPAREA=NULL){ #die("edit");
		$this->load->model("m_product");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant 	= $this->m_plant->datalist($this->data_plant->ID_COMPANY);
		$this->list_grouparea = $this->c_qaf_product->qaf_grouparea();
		$this->list_product = $this->m_product->datalist();
		
		$this->template->adminlte("v_qaf_product_assign");
	}
	
	public function view($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){
		$this->load->model("m_product");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist($this->data_plant->ID_COMPANY);
		$this->list_grouparea = $this->c_qaf_product->list_grouparea($ID_PLANT);
		$this->list_product = $this->m_product->datalist();
		
		$this->template->adminlte("v_qaf_product_view");
	}
	
	// ajax
	public function list_config_grouparea($ID_PLANT=NULL){
		$data = $this->c_qaf_product->list_grouparea($ID_PLANT);  
		echo json_encode($data);
	}
	
	public function async_list_grouparea($ID_PLANT=NULL){
		$data = $this->c_qaf_product->list_grouparea($ID_PLANT);
		echo json_encode($data);
	}
	
	public function async_configuration($ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$data = $this->c_qaf_product->configuration($ID_PLANT,$ID_GROUPAREA); #echo $this->c_qaf_product->get_sql();
		echo json_encode($data);
	}
	
	public function qaf_grouparea(){
		$data = $this->c_qaf_product->qaf_grouparea();
		echo json_encode($data);
	}
}

