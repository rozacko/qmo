<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
 Index Clinker
  <small></small>
  </h1>
</section>
<style>
/*START CSS LOADING*/
  .spinner {
    margin: 100px auto 0;
    width: 70px;
    text-align: center;
  }

  .spinner>div {
    width: 18px;
    height: 18px;
    background-color: #333;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0)
    }
    40% {
      -webkit-transform: scale(1.0)
    }
  }

  @keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0);
      transform: scale(0);
    }
    40% {
      -webkit-transform: scale(1.0);
      transform: scale(1.0);
    }
  }
  /*END CSS LOADING*/
</style>
<!-- Main content -->
<section class="content">

   
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <!--<div class="box-body" style="overflow-x:auto">-->
        <div class="box-body">
            <table class="table" style="border: 1px solid black;">
              <div class="form-group col-sm-2" >
                <div class="form-group">
                  <label>YEAR</label>
                  <SELECT class="form-control select2" name="YEAR" id='tahun'>
                  <?php for($i=2016;$i<=date("Y");$i++): ?>
                    <option value="<?php echo $i; ?>" <?php echo (($this->YEAR!=NULL)? ($this->YEAR==$i):date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                  <?php endfor; ?>
                </SELECT>
                </div>
              </div>
              <div class="form-group col-sm-2">
              <label for="ID_COMPANY">COMPANY</label>
                <?php if($this->USER->ID_COMPANY): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_COMPANY; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_COMPANY" name="ID_COMPANY" VALUE="<?php echo $this->USER->ID_COMPANY; ?>" readonly class="form-control" />
                <?php else: ?>
                <select id="ID_COMPANY" name="ID_COMPANY" class="form-control select2">
                  <?php  foreach($this->list_company as $company): ?>
                    <option value="<?php echo $company->KD_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>
                <?PHP endif; ?>
              </div>
              <div class="form-group col-sm-2">
                <label for="ID_COMPANY">&nbsp;</label>
                <a id='btLoad' class="form-control btn btn-primary"><i class="fa fa-search"> Load</i></a>
              </div>
            </table>
            <div class="row" >
                <div class="col-lg-12">
                    <div style="overflow-x:auto;">
                        <table id="tbl_informasi" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%;font-size=60%">
                  
                        </table>
                    </div>
                </div>
                
                
                <div class="col-lg-6">
                    <div id="grafik_opc" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_nonopc" style="width:100%; height: 400px;"></div>
                </div>
            </div>
        </div>
        
          
          
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<style type="text/css">
  th, td {
    padding: 9px;
    text-align: left;
  }

  div #tb_container {
    width: 250px;
    height:auto;
    min-height:200px;
    height:auto !important;  
    height:200px;
    text-align: center;
  }

  div #tb_label {
    border-radius: 100px;
    border: 3px solid #FFB432;
    width: 100px;
    height: 100px; 
    text-align: center;
    z-index: 1;position:relative;
  }

  #lb_text_opco {
    font-size: 30px;
    color: white;
  }

  #lb_text_nilai {
    float: left;
    font-size: 25px;
    color: white;
    width: 100%;
    margin-top: -8px;
  }

  span h3 { 
    font-weight: bold; 
    text-align: center;
  }

  div #tx_catatan {
    background: #CCDDFF;
    box-shadow: 12px 0 8px -4px rgba(31, 73, 125, 0.5), -12px 0 8px -4px rgba(31, 73, 125, 0.5);
    border-radius: 25px;
    width: 250px; 
    height:500px;
    margin-top: -15px;
    padding: 5px 5px 5px 0px;
    text-align: left;
  }

</style>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
     $('#loading_css').hide();
      $(document).ready(function(){
          
        $('#start, #end').datepicker({
            todayBtn: "linked",
            format: 'mm/yyyy',
            viewMode: 'months',
            minViewMode: 'months',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

          get_table()
          
              $("#btLoad").click(function(){
                 get_table()
              });
    });
    
    
     function get_table(){
        var tahun = $("#tahun").val();
        var company = $("#ID_COMPANY").val();
      $.ajax({
            url: "<?php echo site_url("data_table/tabel_index_clinker");?>",
            data: {"tahun": tahun, "company": company}, 
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                $('#tbl_informasi').html(data.tabel);
                var lebar = parseFloat(data.total_kolom);
                var i;
                for (i = 0; i <= lebar; i++) {
                    var sum_clinker = 0;
                    var sum_semen = 0;
                    var sum_index = 0;
                    $(".clinker_"+i).each(function() {
                        var value = parseFloat($(this).text().replace(/[.]/g, ""));
                        if(!isNaN(value) && value.length != 0 && value !='' && typeof value != "string") {
                            sum_clinker += value;
                        }
                    });
                    $(".clinkertot_"+i).html(addCommas(sum_clinker))
                    
                    $(".semen_"+i).each(function() {
                        var value = parseFloat($(this).text().replace(/[.]/g, ""));
                        if(!isNaN(value) && value.length != 0 && value !='' && typeof value != "string") {
                            sum_semen += value;
                        }
                    });
                    $(".sementot_"+i).html(addCommas(sum_semen))
                    var tot = 0;
                    $(".index_"+i).each(function() {
                        var value = parseFloat($(this).text());
                        if(!isNaN(value) && value.length != 0 && value !='' && typeof value != "string") {
                            sum_index += value;
                            tot+=1;
                        }
                    });
                    var convert = sum_index/tot;
                    $(".indextot_"+i).html(convert.toPrecision(3)+' %')
                }
                //

                 var chart2 = new Highcharts.chart('grafik_opc', {
                 title: {
                      text: 'CLINKER FACTOR OPC' 
                    },
                  credits: {
                    enabled: false
                  },
                xAxis: {
                categories: data.kategori,
                title: {
                      text: ''
                    }
                },
                  yAxis: {
                    title: {
                      text: '%'
                    },
                  },
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  },

                  plotOptions: {
                    series: {
                      label: {
                        connectorAllowed: false
                      }
                    }
                  },colors: [
                                    '#05354d',
                                    '#f7ca78',
                                    '#7bc8a4',
                                    '#93648d',
                                    '#4cc3d9',
                                    '#1f7f91',
                                    '#074b6d',
                                    '#d18700',
                                    '#ffdd9e',
                                ],

                          series: data.dataopc,

                          responsive: {
                            rules: [{
                              condition: {
                                maxWidth: 500
                              },
                              chartOptions: {
                                legend: {
                                  layout: 'horizontal',
                                  align: 'center',
                                  verticalAlign: 'bottom'
                                }
                              }
                            }]
                          }
                        },
                    );
                 var chart3 = new Highcharts.chart('grafik_nonopc', {
                 title: {
                      text: 'CLINKER FACTOR NON OPC' 
                    },
                  credits: {
                    enabled: false
                  },
                xAxis: {
                categories: data.kategori,
                title: {
                      text: ''
                    }
                },
                  yAxis: {
                    title: {
                      text: '%'
                    },
                  },
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  },

                  plotOptions: {
                    series: {
                      label: {
                        connectorAllowed: false
                      }
                    }
                  },colors: [
                                    '#05354d',
                                    '#f7ca78',
                                    '#7bc8a4',
                                    '#93648d',
                                    '#4cc3d9',
                                    '#1f7f91',
                                    '#074b6d',
                                    '#d18700',
                                    '#ffdd9e',
                                ],

                          series: data.datanon,

                          responsive: {
                            rules: [{
                              condition: {
                                maxWidth: 500
                              },
                              chartOptions: {
                                legend: {
                                  layout: 'horizontal',
                                  align: 'center',
                                  verticalAlign: 'bottom'
                                }
                              }
                            }]
                          }
                        },
                    );
                
                
            },
            error: function () {
                
              console.log('Save error');
            }
          });
     }
     
     function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
     
          
</script>
