<?php

class Acclab_kirimsampleblindtest extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_blindtest");
		$this->load->model("m_company");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_component = $this->m_blindtest->list_component();
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);	
		$this->template->adminlte("v_kirimsampleblindtest", $data);
	}

	public function blindtest_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
		$sesi_user = $this->session->userdata();
		$user_in = json_decode(json_encode($sesi_user['USER']), true);  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		if ($user_in["ID_USERGROUP"] && (int) $user_in["ID_USERGROUP"] == 1) {
  			# code...
  		} else {
  			# code...
  			$key['PIC_OBSERVASI'] = $user_in["ID_USER"];
  			$key['PIC_PLANT'] = $user_in["ID_USER"];
  		}

      	$data	= $this->m_blindtest->get_data($key);

  		$return	= array( 
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_blindtest->recFil($key),
  			'recordsTotal'		=>	$this->m_blindtest->recTot($key)
  		);

  		echo json_encode($return);
    }

    public function send_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Send Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENGIRIM'] = $post['user'];
		$data['CREATE_BY'] = $post['user'];

        $pic	= $this->m_blindtest->get_pic($post['FK_ID_PERIODE']);

		$isertd_sample = $this->m_blindtest->sample_send($data);
		if ($isertd_sample) {
			// code...

            $this->send_email($pic->OBSER_EMAIL, $pic->NAMA, $pic->JENIS, $pic->OBSER_FULLNAME);
            $this->send_email($pic->PLANT_EMAIL, $pic->NAMA, $pic->JENIS, $pic->PLANT_FULLNAME);

			$result['msg'] = 'Send Sample Blind Test Success ...';
			$result['status'] = true;
		
			$param['ID'] = $post['FK_ID_PERIODE'];
			$param['STATUS'] = 'KIRIM SAMPLE';
			$param['UPDATE_BY'] = $post['user'];
			$isertd_sample = $this->m_blindtest->update_status($param);
		}
		
		to_json($result);
    }

    public function send_email($to, $title, $jenis, $name){

        $uri = explode('/', $_SERVER['REQUEST_URI']);
        if($uri['1'] == 'DEV'){
            $to = "muh.rudi.hariyanto@gmail.com";
        }

		$from = "qmo.noreply@semenindonesia.com";
		$subject = "Pengiriman Blind Test ".$title;
		$message = "
            Kepada Yth.
            ".$name."

            Dengan hormat,

                Kami telah mengirimkan Blind Test {$title} {$jenis}. 
                Harap melakukan konfirmasi aktifitas di qmo.semenindonesia.com sebagai berikut:
                    1. Konfirmasi penerimaan blind test
                    2. Entry hasil blind test
                Demikian terima kasih

            Regards,
                    
            Koordinator Pelaksanaan ".$title."";
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}

}	

?>
