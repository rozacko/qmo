<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_sample_quality extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_sample_quality");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("C_range_component");
		$this->load->library("excel");
	}

	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_sample_quality", $data);
	}

	public function excel_create(){
		# code...
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
		// header('Content-Type: application/vnd.ms-excel'); //mime type
		// header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		// header('Cache-Control: max-age=0'); //no cache

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename");
		header("Content-Transfer-Encoding: binary ");
		            
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

		// $result['msg'] = 'success';
		// to_json($result);

	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_sample_area(){
		$sample_area= $this->m_sample_quality->samplearealist();
		to_json($sample_area);
	}

	public function ajax_get_sample_area_scm(){
		$sample_area= $this->m_sample_quality->samplearealist_scm();
		to_json($sample_area);
	}

	public function ajax_get_company_scm(){
		$sample_area= $this->m_sample_quality->sample_perusahaan_scm();
		to_json($sample_area);
	}

	public function ajax_get_type_product($all = null){
		$sample_area= $this->m_sample_quality->type_productlist($all);
		foreach ($sample_area as $key => $value) {
			$sample_area[$key]['KD_PRODUCT'] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
		}
		to_json($sample_area);
	}

	public function ajax_get_merk(){
		$sample_area= $this->m_sample_quality->merklist_scm();
		to_json($sample_area);
	}

	public function ajax_get_type_produk_scm(){
		$tdata = array();
		$sample_produk= $this->m_sample_quality->merklist_scmn();

		foreach ($sample_produk as $key => $value) {
			# code...
			$ttype = array();
			$type_produk = $this->m_sample_quality->type_produk_scm($value['KODE_PERUSAHAAN']);

			if ($type_produk) {
				# code...

				foreach ($type_produk as $ind => $val) {
					# code...
					$ttype[] = preg_replace('/\s+/', '', $val['KD_PRODUCT']);
				}

				$tdata[$value['PRODUK']] = $ttype;

			} else {
				# code...
				$ttype[] = 'PCC';
				$tdata[$value['PRODUK']] = $ttype;

			}
			
		}

		to_json($tdata);
	}

	public function ajax_get_merke(){
		$sample_area= $this->m_sample_quality->merklist();
		to_json($sample_area);
	}

	public function ajax_get_component(){
		$componentlist = $this->m_sample_quality->componentlist();
		to_json($componentlist);
	}

	public function ajax_get_component_checklist(){
			$componentlist = $this->m_sample_quality->component_checklist();
			to_json($componentlist);
	}

	public function ajax_get_component_display(){
			$componentlist = $this->m_sample_quality->component_checklist_order();
			to_json($componentlist);
	}

	private function get_component(){
		$param = $this->m_sample_quality->componentlist();

		foreach ($param as $col) {
			$id_comp[] = $col->ID_COMPONENT;
			$header[]['title'] = strtoupper($col->KD_COMPONENT);
		}

		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	public function load_cement(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach ($form as $ind => $value) {
			// code...
			if ($value['name'] == 'oCOMPANY') {
				// code...
				$company_id = $value['value'];
			} elseif ($value['name'] == 'oDATE') {
				// code...
				$t_date = explode("/",$value['value']);
				$date_sampling = (int)$t_date[2].'-'.(int)$t_date[0].'-'.(int)$t_date[1];
			} elseif ($value['name'] == 'oSUB_AREA') {
				// code...
				$sbarea_id = $value['value'];
			} elseif ($value['name'] == 'oMERK') {
				// code...
				$merk_id = $value['value'];
			} elseif ($value['name'] == 'oTYPE_PRODUCT') {
				// code...
				$typeproduct_id = $value['value'];
			}

		}

		$tdata['ID_SAMPLE_AREA'] = $sbarea_id ;
		$tdata['ID_COMPETITOR'] = $merk_id ;
		$tdata['ID_PRODUCT'] = $typeproduct_id ;
		$colHeader 	= array();
		$data 		= array();
		$colHeader 	= $this->get_component();
		$exists_sample 	= $this->m_sample_quality->sample_exists($tdata, $date_sampling);
		for ($i=0; $i < count($colHeader['colHeader']) ; $i++) {
			// code...
			$data[0][$i] = $exists_sample[$i]['COMPONENT_VALUE'];
		}
		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}



	public function save_sample_data_q(){
		$post = $this->input->post();

		$companyid = $post['company_id'];

		$companyname = $post['companyname'];
		$comp = $post['id_comp'];
		$nmcomp = $post['name_comp'];
		$data = $post['data'];

		$inserteddata = array();
		$updateddata = array();
		$deleteddata = array();
		$cinserteddata = 0;
		$cupdateddata = 0;
		$cdeleteddata = 0;
		foreach ($data as $key => $value) {
			$t_date = explode("/",$value[0]);
			if (count($t_date) == 3) {
				// code...
				$date_sampling = (int)$t_date[2].'-'.(int)$t_date[1].'-'.(int)$t_date[0];
				for ($i= 4; $i < count($value); $i++) {
					// code...
					unset($tdata['COMPONENT_VALUE']);
					$tdata['ID_COMPONENT'] = $comp[$i - 4];
					$tdata['COMPONENT_NAME'] = $nmcomp[$i - 4];
					$tdata['ID_COMPANY'] = $companyid ;
					$tdata['COMPANY_NAME'] = $companyname;
					$tdata['SAMPLE_AREA_NAME'] = $value[1];
					$sampleareadata = $this->m_sample_quality->get_sample_area_scm($tdata['SAMPLE_AREA_NAME']);

					$tdata['ID_SAMPLE_AREA'] = $sampleareadata['ID_M_KOTA'];
					$tdata['COMPETITOR_MERK_PRODUCT'] = $value[2];
					$competitordata = $this->m_sample_quality->get_competitor_scm($tdata['COMPETITOR_MERK_PRODUCT']);
					$tdata['ID_COMPETITOR'] = $competitordata['KODE_PERUSAHAAN'];
					$type_product = $this->m_sample_quality->get_tye_product($value[3]);
					$tdata['ID_TYPE_PRODUCT'] = $type_product['ID_PRODUCT'];
					if ($value[$i]) {
						// code...
						$mathround = -1;

						$precissio_conf = $this->m_sample_quality->component_precission(0, (int) $colHeader['id_comp'][$i], 0);
						
						if ($precissio_conf) {
							# code...
							$mathround = (int) $precissio_conf['PRECISSION'];
						}

						$tcomp_value = preg_replace('/\s+/', '', $value[$i]); 

						if (is_numeric($tcomp_value)) {
							# code...
							if ($mathround < 0) {
								# code...
								$tdata['COMPONENT_VALUE'] = (double) $tcomp_value;
							} else if ($mathround == 0) {
								# code...
								$tdata['COMPONENT_VALUE'] = (int) $tcomp_value;
							} else {
								# code...
								$tdata['COMPONENT_VALUE'] = round((double) $tcomp_value, $mathround);
							}
						}
						
						// $tdata['COMPONENT_VALUE'] = preg_replace('/\s+/', '', $value[$i]); 
					}
					$tdata['CREATE_BY'] = $post['user'] ;
						// code...
						$sample_exists = $this->m_sample_quality->is_sample_exists($tdata, $date_sampling);

						if (!$sample_exists && isset($tdata['COMPONENT_VALUE'])) {
							// code...
							// $this->m_sample_quality->sample_insert($tdata, $date_sampling);
							if ($this->m_sample_quality->sample_insert($tdata, $date_sampling)) {
								# code...
								$cinserteddata++;
								// $inserteddata[] = $tdata;
								$inserteddata[] = $this->db->last_query();
							}
						} else if ($sample_exists && isset($tdata['COMPONENT_VALUE'])) {
							// code...
							// $this->m_sample_quality->sample_update($tdata, $date_sampling);
							if ($this->m_sample_quality->sample_update($tdata, $date_sampling)) {
								# code...
								$cupdateddata++;
								// $updateddata[] = $tdata;
								$updateddata[] = $this->db->last_query();
							}
						} else {
							// code...
							// $this->m_sample_quality->sample_delete_exist($tdata, $date_sampling);
							if ($this->m_sample_quality->sample_delete_exist($tdata, $date_sampling)) {
								# code...
								$cdeleteddata++;
								// $deleteddata[] = $tdata;
								$deleteddata[] = $this->db->last_query();
							}
						}

					$sampledata[] = $tdata;
				}
			}
		}

		if ($cinserteddata > 0 || $updateddata > 0 ||$deleteddata > 0 ) {
			// code...
			$result['msg'] = 'success';
		} else {
			// code...
			$result['msg'] = 'error';
		}

		$result['status'] = count($inserteddata) . ' data inserted & '. count($updateddata) . ' data updated & '. count($deleteddata) . ' data deleted';
		// $result['delete'] = $deleteddata;
		// $result['update'] = $updateddata;
		// $result['Insert'] = $inserteddata;

		to_json($result);
	}

	public function delete_sample_data_q(){
		$post = $this->input->post();
		$companyid = $post['company_id'];
		$companyname = $post['companyname'];
		$data = $post['data'];

		$inserteddata = array();
		$updateddata = array();
		$deleteddata = array();
		$cinserteddata = 0;
		foreach ($data as $key => $value) {
			$t_date = explode("/",$value[0]);
			if (count($t_date) == 3) {
				// code...
				$date_sampling = (int) $t_date[2].'-'.(int) $t_date[1].'-'.(int) $t_date[0];

				$tdata['ID_COMPANY'] = (int) $companyid ;
				$sampleareadata = $this->m_sample_quality->get_sample_area_scm($value[1]);
				$tdata['ID_SAMPLE_AREA'] = (int) $sampleareadata['ID_M_KOTA'];
				$type_product = $this->m_sample_quality->get_tye_product($value[3]);
				$tdata['ID_TYPE_PRODUCT'] = (int) $type_product['ID_PRODUCT'];
				$competitordata = $this->m_sample_quality->get_competitor_scm($value[2]);
				$tdata['ID_COMPETITOR'] = (int) $competitordata['KODE_PERUSAHAAN'];
				$delete_data = $this->m_sample_quality->sample_delete($tdata, $date_sampling);	

				if ($delete_data) {
					# code...
					$deleteddata[] = $this->db->last_query;
				} else {
					# code...
				}
					
			}
		}

		if ($deleteddata > 0) {
			// code...
			$result['msg'] = 'success';
		} else {
			// code...
			$result['msg'] = 'error';
		}

		$result['status'] = count($deleteddata) . ' data deleted';
		$result['deleted_list'] = $deleteddata;

		to_json($result);
	}

	public function save_checklist(){
		$post = $this->input->post();
		$datachek = $post['datacheck'];
		$dataunchek = $post['datauncheck'];
		$data = $post['data'];

		foreach ($datachek as $key => $value) {
			$tdata['ID_COMPONENT'] = $value ;
			$tdata['IS_ACTIVE'] = 1 ;
			$checklist_exists = $this->m_sample_quality->is_checklist_exists($tdata);
			if (!$checklist_exists) {
				// code...
				$tdata['CREATE_BY'] = $post['user'] ;
				$this->m_sample_quality->checklist_insert($tdata);
			} else {
				// code...
				$tdata['MODIFIED_BY'] = $post['user'] ;
				$this->m_sample_quality->checklist_update($tdata);
			}
		}

		foreach ($dataunchek as $key => $value) {
			$tdata['ID_COMPONENT'] = $value ;
			$tdata['IS_ACTIVE'] = 0 ;
			$checklist_exists = $this->m_sample_quality->is_checklist_exists($tdata);
			if (!$checklist_exists) {
				// code...
				$tdata['CREATE_BY'] = $post['user'] ;
				$this->m_sample_quality->checklist_insert($tdata);
			} else {
				// code...
				$tdata['MODIFIED_BY'] = $post['user'] ;
				$this->m_sample_quality->checklist_update($tdata);
			}
		}
		$result['msg'] = 'success';
		to_json($result);
	}

	public function save_sample_data(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach ($form as $ind => $value) {
			// code...
			if ($value['name'] == 'oCOMPANY') {
				// code...
				$company_id = $value['value'];
			} elseif ($value['name'] == 'oDATE') {
				// code...
				$t_date = explode("/",$value['value']);
				$date_sampling = (int)$t_date[2].'-'.(int)$t_date[0].'-'.(int)$t_date[1];
			} elseif ($value['name'] == 'oSUB_AREA') {
				// code...
				$sbarea_id = $value['value'];
			} elseif ($value['name'] == 'oMERK') {
				// code...
				$merk_id = $value['value'];
			} elseif ($value['name'] == 'oTYPE_PRODUCT') {
				// code...
				$typeproduct_id = $value['value'];
			}

		}

		foreach ($data[0] as $key => $value) {
			unset($tdata['COMPONENT_VALUE']);
			$tdata['ID_SAMPLE_AREA'] = $sbarea_id ;
			$tdata['ID_COMPETITOR'] = $merk_id ;
			$tdata['ID_PRODUCT'] = $typeproduct_id ;
			$tdata['ID_COMPONENT'] = $comp[$key] ;
			if ($value) {
				// code...
				$tdata['COMPONENT_VALUE'] = $value ;
			}
			$tdata['CREATE_BY'] = $post['user'] ;
			$sample_exists = $this->m_sample_quality->is_sample_exists($tdata, $date_sampling);
			if (!$sample_exists) {
				// code...
				$this->m_sample_quality->sample_insert($tdata, $date_sampling);
			} else {
				// code...
				$this->m_sample_quality->sample_update($tdata, $date_sampling);
			}
			$sampledata[] = $tdata;
		}
		$result['msg'] = 'success';
		$result['data']   = $sampledata;
		to_json($result);
	}

	public function save_table_cement(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;
		$bulan = explode("/", $form->TANGGAL);
		$form->BULAN = $bulan[0];

		//T_PRODUCTION_DAILY (1): ID_CEMENT_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_UPDATE,JAM_UPDATE,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_CEMENT_DAILY, ID_COMPONENT, NILAI

		//read line by line
		$tgl_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_date	  		= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status", $comp);
			$i_remark 		= array_search("_remark", $comp);

			#if(!$row[$i_date]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= str_pad($tgl_auto,2,'0',STR_PAD_LEFT)."/".$form->TANGGAL; # $row[$i_date] dd/mm/yyyy
			$status_mesin 				= strtoupper($row[$i_mesin_status]);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if ($mati == $t_kolom) {
				$tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			$ID_CEMENT_DAILY = $this->t_cement_daily->get_id($tdata[ID_AREA],$tdata[ID_PRODUCT],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			#echo $this->db->last_query() . "\n\n";
			if(!$ID_CEMENT_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);

			}


			#echo $this->db->last_query()."\n\n";
			//D
			for($x=0;$x<$i_mesin_status;$x++){


				// echo '<pre>';
				// var_dump($row[$x]);
				// echo '</pre>';


				$ddata = null;
				$ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
				$ddata['ID_COMPONENT']		= $comp[$x];
				$ddata['NILAI']				= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']			= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_cement_daily->d_exists($ddata)){
						$this->t_cement_daily->d_insert($ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
						$tes_tes_update = $this->t_cement_daily->tes_update($ddata);
					}
					continue;
				}
					$result['tes_tes_update'] = $tes_tes_update;

				/* Check IF CS */
				$str = strtoupper($this->m_component->get_data_by_id($ddata['ID_COMPONENT'])->KD_COMPONENT);
				if (substr_compare($str, 'CS', 0, 2)===0) {
					$ddata['NILAI']	= ($row[$x]=='') ? '':$row[$x];
					if(!$this->t_cement_daily->d_exists($ddata)){
						$this->t_cement_daily->d_insert($ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_cement_daily->d_exists($ddata)){
							$this->t_cement_daily->d_insert($ddata);
						}
						else{
							$this->t_cement_daily->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}

				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}



				if(!$this->t_cement_daily->d_exists($ddata)){
					$this->t_cement_daily->d_insert($ddata);
					#echo $this->db->last_query();die();
				}
				else{
					$this->t_cement_daily->d_update($ddata);
				}
				#echo $this->db->last_query().";".PHP_EOL;
			}

			$tgl_auto++;
			$ct_row++;

		}
		// to_json(array("result" => 'ok'));
		to_json($result);
				// exit;
	}

	private function str_clean($chr){
		// $str ='([^.0-9-]+)';
		// return preg_replace($str, '', $chr);
		return preg_replace('/\s+/', '', $chr);
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color 				= getFixColor($i);
			$nilai 				= array_filter($trace[$i], 'is_numeric');

			if (empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= trim($comp[$i]->title);
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;


			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
