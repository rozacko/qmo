<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use SAPNWRFC\Connection as SapConnection;
use SAPNWRFC\Exception as SapException;

class Sync_qbb extends Home {
	
	
	public function __construct(){
		parent::__construct();
		
		$this->load->helper("string");
		$this->load->model("M_qbb_data","qbb");
	}
	
	// start
	// pengambilan data dari SAP data kualiatas batubara data harian (Hari ini)
	public function index(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		
		$config_prod = [
			'ashost' => '10.15.5.13',
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		
		$config = $config_prod;
	
        try {
            $sapi = new SapConnection($config);
    		
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
			
            if ($rfc == TRUE) {
				$date_now = date('Ymd');
                $code_mat = '112-100-0009';
				$options = [
					'rtrim' => true
				];
				$rfc->setParameterActive('R_MATNR', true);
				$rfc->setParameterActive('R_PERIOD', true);
				$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' 	=> [
								[
									'SIGN' 	=> 'I',
									'OPTION'=> 'EQ',
									'LOW' 	=> $date_now
								]
							]
						];
                $result = $rfc->invoke($parms, $options);
				
				// var_dump($result);
				// exit();
				$fromsap = count($result['R_RESULT']);
                
				$action = $this->qbb->data_from_sap($result['R_RESULT']);
				$existing = $fromsap-$action;
                
				echo "Syny date: ".date('d-m-Y')." => Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing;
            }
			//$rfc->Close();
            //$sapi->Close();
        } catch(SapException $ex) {
            echo 'Exception: ' . $ex->getMessage() . PHP_EOL;
        }
    }
	
	// pengambilan data dari SAP data kualiatas batubara data priode tanggal
	public function rangedate($date_start,$date_end){		//format: YYYYMMDD
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		
		$config_prod = [
			'ashost' => '10.15.5.13',
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		
		$config = $config_prod;
		
        try {
            $sapi = new SapConnection($config);
    		
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
			
            if ($rfc == TRUE) {
                $code_mat = '112-100-0009';
				$options = [
					'rtrim' => true
				];
				$rfc->setParameterActive('R_MATNR', true);
				$rfc->setParameterActive('R_PERIOD', true);
				$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' 	=> [
								[
									'SIGN' 	=> 'I',
									'OPTION'=> 'BT',
									'LOW' 	=> $date_start,
									'HIGH' 	=> $date_end
								]
							]
						];
                $result = $rfc->invoke($parms, $options);
				
				// var_dump($result);
				// exit();
                
				$action = $this->qbb->data_from_sap($result['R_RESULT']);
				//echo $action;
            }
			//$rfc->Close();
            //$sapi->Close();
        } catch(SapException $ex) {
            echo 'Exception: ' . $ex->getMessage() . PHP_EOL;
        }
    }
	
	// pengambilan data dari SAP data kualiatas batubara keseluruhan data
	public function all(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		$config_prod = [
			'ashost' => '10.15.5.13',
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		
		$config = $config_prod;
		
        try {
            $sapi = new SapConnection($config);
    		
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
			
            if ($rfc == TRUE) {
				
                $code_mat = '112-100-0009';
				$options = [
					'rtrim' => true
				];
				$rfc->setParameterActive('R_MATNR', true);
				$rfc->setParameterActive('R_PERIOD', true);
				$parms = [
							'R_MATNR' 	=> $code_mat
						];
                $result = $rfc->invoke($parms, $options);
				
				// var_dump($result);
				// exit();
                
				$action = $this->qbb->data_from_sap($result['R_RESULT']);
				//echo $action;
            }
			//$rfc->Close();
            //$sapi->Close();
        } catch(SapException $ex) {
            echo 'Exception: ' . $ex->getMessage() . PHP_EOL;
        }
    }
	// akhir fungsi get data SAP kualitas batu bara

}

?>