<?php

class M_Incident Extends DB_QM {

	private $post  = array();
	private $table = "M_INCIDENT";
	private $pKey  = "ID_INCIDENT";
	private $column_order = array('ID_INCIDENT'); //set column for datatable order
	private $column_search = array('UPPER(SUBJECT)', 'UPPER(JUDUL_SOLUTION)'); //set column for datatable search
	private $order = array("ID_INCIDENT" => 'DESC'); //default order

	private $post2  = array();
	private $table2 = "V_INCIDENT";
	private $pKey2  = "ID_INCIDENT";
	private $column_order2 = array('ID_INCIDENT'); //set column for datatable order
	private $column_search2 = array('UPPER(SUBJECT)', 'UPPER(JUDUL_SOLUTION)'); //set column for datatable search
	private $order2 = array("ID_INCIDENT" => 'DESC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
	}

	public function datalist(){
		#$this->db->where("a.ID_TEKNISI IS NULL",NULL,FALSE);
		#$this->db->order_by("a.ID_INCIDENT");
		#return $this->db->get("M_INCIDENT a")->result();
		return $this->solved_list();
	}

	public function assigned(){
		$this->db->select("a.*,b.NM_TEKNISI");
		$this->db->join("M_TEKNISI b","a.ID_TEKNISI=b.ID_TEKNISI");
		$this->db->where("a.ID_TEKNISI IS NOT NULL",NULL,FALSE);
		$this->db->where("a.ID_SOLUTION IS NULL",NULL,FALSE);
		$this->db->order_by("a.ID_INCIDENT");
		return $this->db->get("M_INCIDENT a")->result();
	}

	public function solved_list(){
		$this->db->select("a.*,b.NM_TEKNISI,c.JUDUL_SOLUTION");
		$this->db->join("M_TEKNISI b","a.ID_TEKNISI=b.ID_TEKNISI","LEFT");
		$this->db->join("M_SOLUTION c","a.ID_SOLUTION=c.ID_SOLUTION","LEFT");
		#$this->db->where("a.ID_TEKNISI IS NOT NULL",NULL,FALSE);
		#$this->db->where("a.ID_SOLUTION IS NOT NULL",NULL,FALSE);
		$this->db->order_by("a.ID_INCIDENT","DESC");
		return $this->db->get("M_INCIDENT a")->result();
	}


	public function get_solved_by_id($ID_INCIDENT){
		$this->db->select("a.*,b.NM_TEKNISI,c.JUDUL_SOLUTION,d.NM_COMPONENT,d.KD_COMPONENT,c.*,TO_CHAR(c.JAM,'DD-MM-YYYY HH24:MI') AS JAM_ANALISA");
		$this->db->join("M_TEKNISI b","a.ID_TEKNISI=b.ID_TEKNISI");
		$this->db->join("M_SOLUTION c","a.ID_SOLUTION=c.ID_SOLUTION");
		$this->db->join("M_COMPONENT d","a.ID_COMPONENT=d.ID_COMPONENT");
		$this->db->where("a.ID_TEKNISI IS NOT NULL",NULL,FALSE);
		$this->db->where("a.ID_SOLUTION IS NOT NULL",NULL,FALSE);
		$this->db->where("a.ID_INCIDENT",$ID_INCIDENT);
		$this->db->order_by("a.ID_INCIDENT");
		return $this->db->get("M_INCIDENT a")->row();
	}

	public function list_company(){
		$this->db->where("a.KD_INCIDENT !=","2000");
		$this->db->order_by("a.ID_INCIDENT");
		return $this->db->get("M_INCIDENT a")->result();
	}

	public function search(&$keyword){
		$this->db->like("NM_INCIDENT",$keyword);
		return $this->db->get("M_INCIDENT")->result();
	}

	public function data($where){
		$this->db->where($where);
		return  $this->db->get("M_INCIDENT")->row();
	}

	public function get_data_by_id($ID_INCIDENT){
		if($ID_INCIDENT){
            $this->db->select("a.*, b.KD_COMPONENT, b.NM_COMPONENT, c.ID_D_INCIDENT,
                c.JAM, c.ANALISA, c.NOTE, TO_CHAR(c.JAM, 'DD-MM-YYYY HH24:MI') AS JAM_ANALISA");
			$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
			$this->db->join("D_INCIDENT c","a.ID_INCIDENT=c.ID_INCIDENT");
			$this->db->where("a.ID_INCIDENT in ($ID_INCIDENT)");
			$this->db->order_by("JAM_ANALISA", "ASC");
			$result = $this->db->get("M_INCIDENT a")->result();
			// echo $this->db->last_query();
			return $result;
		}else{

			return false;
		}
	}

    public function get_data_distinct_by_id($ID_INCIDENT) {
        if (! $source = $this->get_data_by_id($ID_INCIDENT)) {
            return [];
        }

        $stack = [];

        // distinct the data
        $new_data  = array_filter(
            $source,
            function($data) use (&$stack) {
                $compare_data = [
                    'ID_INCIDENT'   => trim($data->ID_INCIDENT),
                    'KD_COMPONENT'  => trim($data->KD_COMPONENT),
                    'JAM_ANALISA'   => trim($data->JAM_ANALISA),
                    'ANALISA'       => trim($data->ANALISA), 
                ];

                if ( in_array($compare_data, $stack) ) {
                    return false;
                }

                $stack[] = $compare_data;

                return true;
            }
        );

        // reindex the keys
        $new_data = array_combine(range(0, count($new_data) -1), array_values($new_data));

        return $new_data;
    }

	public function data_except_id($where,$skip_id){
		$this->db->where("ID_INCIDENT !=",$skip_id);
		$this->db->where($where);
		return $this->db->get("M_INCIDENT")->row();
	}

	public function insert($data){
		$this->db->set($data);
		$this->db->set("ID_INCIDENT","SEQ_ID_INCIDENT.NEXTVAL",FALSE);
		$this->db->insert("M_INCIDENT");
	}
	public function insert_action($data){
		$this->db->set($data);
		$this->db->set('CREATE_DATE', 'SYSDATE', FALSE);
		$this->db->set("ID_ACTION","SEQ_M_INCIDENT_ACTION.NEXTVAL",FALSE);
		$this->db->insert("M_INCIDENT_ACTION");
	}

	public function update($data,$ID_INCIDENT){
		$this->db->set($data);
		$this->db->where("ID_INCIDENT",$ID_INCIDENT);
		$this->db->update("M_INCIDENT");
	}
	public function update_action($data,$ID_INCIDENT, $ID_ACTION){
		$this->db->set($data);
		$this->db->set('TANGGAL_VERIFIKASI', 'SYSDATE', FALSE);
		$this->db->where("ID_ACTION",$ID_ACTION);
		$this->db->where("ID_INCIDENT",$ID_INCIDENT);
		$this->db->update("M_INCIDENT_ACTION");
	}

	public function delete($ID_INCIDENT){
		$this->db->where("ID_INCIDENT",$ID_INCIDENT);
		$this->db->delete("M_INCIDENT");
	}

	public function assign($data,$ID_INCIDENT){
		$this->db->set($data);
		$this->db->where("ID_INCIDENT",$ID_INCIDENT);
		$this->db->update("M_INCIDENT");
	}

	public function solved($ID_SOLUTION,$ID_INCIDENT, $accessRa, $accessQc){
		$result = $this->db->select('*')->where('ID_INCIDENT',$ID_INCIDENT)->get('M_INCIDENT')->row_array();
		if($accessQc){
			if($result['ID_SOLUTION'] != null || $result['ID_SOLUTION'] != ''){
				$this->db->set("CLOSED_STATUS",'Y');
			}
		}else{
			if($accessRa){
				if($result['CLOSED_STATUS'] == 'N'){
					$this->db->set("ID_NCQR_STATUS",'3');
					$this->db->set("ID_SOLUTION",$ID_SOLUTION);
					$this->db->set("DATE_SOLUTION","SYSDATE",FALSE);
				}
			}
		}

		// exit;
		$this->db->where("ID_INCIDENT",$ID_INCIDENT);
		$this->db->update("M_INCIDENT");
	}

	public function type(){
		return $this->db->get("M_INCIDENT_TYPE")->result();
	}

	public function report($data){
        $id_area= "'".join("','" ,$data['ID_AREA'])."'";
         // $result = implode($resultStrings, ",");
		$this->db->select("a.ID_INCIDENT, a.SUBJECT,to_char(a.TANGGAL,'dd-mm-yyyy hh24:mi') as TANGGAL,c.NM_INCIDENT_TYPE,g.NM_COMPANY,f.NM_PLANT,NM_AREA,i.FULLNAME, a.NM_NCQR_STATUS");
		$this->db->join("M_INCIDENT_TYPE c","a.ID_INCIDENT_TYPE=c.ID_INCIDENT_TYPE");
		// $this->db->join("M_OPCO d","a.ID_AREA=d.ID_AREA and d.ID_JABATAN=3 and d.ID_AREA='".$data['ID_AREA']."'","LEFT");
		// $this->db->join("M_OPCO d","a.ID_AREA=d.ID_AREA and d.ID_JABATAN=3 and d.ID_AREA in (".$id_area.")","LEFT");
		$this->db->join("M_PLANT f","a.ID_PLANT=f.ID_PLANT");
		$this->db->join("M_COMPANY g","f.ID_COMPANY=g.ID_COMPANY");
		$this->db->join("T_CEMENT_HOURLY h","a.ID_PRODUCTION_HOURLY=h.ID_CEMENT_HOURLY","LEFT");
		$this->db->join("M_USERS i","i.ID_USER=h.USER_ENTRY","LEFT");
		$this->db->where_in('a.ID_AREA', $data['ID_AREA']); 
		// $this->db->where("a.ID_AREA",$data['ID_AREA']);
		// if($data['ID_INCIDENT_TYPE']) $this->db->where("a.ID_INCIDENT_TYPE",$data['ID_INCIDENT_TYPE']);
		if($data['ID_INCIDENT_TYPE']) $this->db->where_in('a.ID_INCIDENT_TYPE', $data['ID_INCIDENT_TYPE']); 
		$this->db->where("DATE_DATA BETWEEN TO_DATE ('".$data['STARTDATE']."', 'DD/MM/YYYY') AND to_date( '".$data['ENDDATE']."', 'DD/MM/YYYY' ) ",FALSE,FALSE);
		// $this->db->where("a.TANGGAL >= to_date('".$data['STARTDATE']."','dd/mm/yyyy') AND to_date(to_char(a.TANGGAL,'DDMMYYYY'),'DDMMYYYY') <= to_date('".$data['ENDDATE']."','dd/mm/yyyy') ",FALSE,FALSE);
		// $this->db->where("a.ID_NCQR_STATUS = 1 AND CLOSED_STATUS = 'N' ",FALSE,FALSE);
		$this->db->order_by("a.ID_INCIDENT");
		$r = $this->db->get("V_INCIDENT a"); #echo $this->db->last_query(); die();

        log_message('debug', $this->db->last_query());
		return $r->result();
	}
    
    
	public function report_backup($data){
        $id_area= "'".join("','" ,$data['ID_AREA'])."'";
         // $result = implode($resultStrings, ",");
		$this->db->select("a.ID_INCIDENT, a.SUBJECT,to_char(a.TANGGAL,'dd-mm-yyyy hh24:mi') as TANGGAL,c.NM_INCIDENT_TYPE,d.FULLNAME,g.NM_COMPANY,f.NM_PLANT,e.NM_AREA,i.FULLNAME");
		$this->db->join("M_INCIDENT_TYPE c","a.ID_INCIDENT_TYPE=c.ID_INCIDENT_TYPE");
		// $this->db->join("M_OPCO d","a.ID_AREA=d.ID_AREA and d.ID_JABATAN=3 and d.ID_AREA='".$data['ID_AREA']."'","LEFT");
		$this->db->join("M_OPCO d","a.ID_AREA=d.ID_AREA and d.ID_JABATAN=3 and d.ID_AREA in (".$id_area.")","LEFT");
		$this->db->join("M_AREA e","e.ID_AREA=a.ID_AREA");
		$this->db->join("M_PLANT f","e.ID_PLANT=f.ID_PLANT");
		$this->db->join("M_COMPANY g","f.ID_COMPANY=g.ID_COMPANY");
		$this->db->join("T_CEMENT_HOURLY h","a.ID_PRODUCTION_HOURLY=h.ID_CEMENT_HOURLY","LEFT");
		$this->db->join("M_USERS i","i.ID_USER=h.USER_ENTRY","LEFT");
		$this->db->where_in('a.ID_AREA', $data['ID_AREA']); 
		// $this->db->where("a.ID_AREA",$data['ID_AREA']);
		// if($data['ID_INCIDENT_TYPE']) $this->db->where("a.ID_INCIDENT_TYPE",$data['ID_INCIDENT_TYPE']);
		if($data['ID_INCIDENT_TYPE']) $this->db->where_in('a.ID_INCIDENT_TYPE', $data['ID_INCIDENT_TYPE']); 
		$this->db->where("a.TANGGAL >= to_date('".$data['STARTDATE']."','dd/mm/yyyy') AND to_date(to_char(a.TANGGAL,'DDMMYYYY'),'DDMMYYYY') <= to_date('".$data['ENDDATE']."','dd/mm/yyyy') ",FALSE,FALSE);
		$this->db->order_by("a.ID_INCIDENT");
		$r = $this->db->get("M_INCIDENT a"); #echo $this->db->last_query(); die();
		return $r->result();
	}


	public function get_dashboard($data){
		$r[TOTAL]  = (int)$this->n_total($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']); #echo $this->db->last_query();
		$r[BARU]   = (int)$this->n_baru($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		$r[PROSES] = (int)$this->n_proses($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		$r[SOLVED] = (int)$this->n_solved($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		return $r;
	}

	public function get_dashboard_notifikasi($data){
		$r[MINOR1] = (int)$this->n_minor1($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		$r[MINOR2] = (int)$this->n_minor2($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		$r[MAYOR]  = (int)$this->n_mayor($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		$r[EQR]  = (int)$this->n_eqr($data['ID_COMPANY'],$data['STARTDATE'],$data['ENDDATE']);
		return $r;
	}

	private function n_total($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY') + 1",NULL,FALSE);
		$result = $this->db->get("V_INCIDENT")->row()->JML;
		// echo $this->db->last_query();
		return $result;
		 #die($this->db->last_query());
	}

	private function n_baru($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("DATE_SOLUTION IS NULL",NULL,FALSE);
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML;
	}

	private function n_proses($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("DATE_SOLUTION IS NOT NULL",NULL,FALSE);
		$this->db->where("ID_SOLUTION IS NULL",NULL,FALSE);
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML;
	}

	private function n_solved($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("ID_SOLUTION IS NOT NULL",NULL,FALSE);
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML;
	}

	private function n_minor1($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("ID_INCIDENT_TYPE","1");
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		$res = $this->db->get("V_INCIDENT")->row()->JML; 
		// die($this->db->last_query());
		return $res;
	}

	private function n_minor2($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("ID_INCIDENT_TYPE","2");
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML; #die($this->db->last_query());
	}

	private function n_mayor($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("ID_INCIDENT_TYPE >=","3");
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML; #die($this->db->last_query());
	}

	private function n_eqr($ID_COMPANY,$STARTDATE,$ENDDATE){
		$this->db->select("count(ID_INCIDENT) as JML");
		$this->db->where("ID_COMPANY",$ID_COMPANY);
		$this->db->where("ID_INCIDENT_TYPE >=","4");
		$this->db->where("TANGGAL >= to_date('$STARTDATE','DD/MM/YYYY')",NULL,FALSE);
		$this->db->where("TANGGAL <= to_date('$ENDDATE','DD/MM/YYYY')+1",NULL,FALSE);
		return $this->db->get("V_INCIDENT")->row()->JML; #die($this->db->last_query());
	}

	public function get_notifikasi_company($data){
		$s = "
		select c.ID_COMPANY, b.ID_PLANT, a.ID_AREA, b.NM_PLANT, a.NM_AREA,
		(select count(z.ID_INCIDENT) FROM M_INCIDENT z where z.ID_AREA=a.ID_AREA AND z.ID_INCIDENT_TYPE=1 and z.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and z.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy') ) as MINOR1,
		(select count(z.ID_INCIDENT) FROM M_INCIDENT z where z.ID_AREA=a.ID_AREA AND z.ID_INCIDENT_TYPE=2 and z.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and z.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy') ) as MINOR2,
		(select count(z.ID_INCIDENT) FROM M_INCIDENT z where z.ID_AREA=a.ID_AREA AND z.ID_INCIDENT_TYPE>=3 and z.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and z.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy') ) as MAYOR,
		(select count(z.ID_INCIDENT) FROM M_INCIDENT z where z.ID_AREA=a.ID_AREA AND z.ID_INCIDENT_TYPE>=4 and z.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and z.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy') ) as EQR
		from m_area a, m_plant b, m_company c
		where
		c.ID_COMPANY='".$data[ID_COMPANY]."'
		and a.ID_PLANT=b.ID_PLANT and b.ID_COMPANY=c.ID_COMPANY
		order by ID_PLANT asc, ID_AREA asc";
		$q = $this->db->query($s);
		return $q->result();

	}

	public function avg_score_ncqr($data){
		$s = "select
			a.ID_COMPANY, a.NM_COMPANY,
			(round((select sum(b.SCORE) from V_INCIDENT b where a.ID_COMPANY=b.ID_COMPANY and b.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and b.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy')+1 )
				/(select COUNT(b.ID_INCIDENT) from V_INCIDENT b where a.ID_COMPANY=b.ID_COMPANY and b.TANGGAL >= to_date('".$data[STARTDATE]."','dd/mm/yyyy') and b.TANGGAL <= to_date('".$data[ENDDATE]."','dd/mm/yyyy')+1 ), 2)
			) AS AVG_SCORE
			from m_company a where a.KD_COMPANY != 2000";
		$q = $this->db->query($s);

		// echo $this->db->last_query();

		return $q->result();
	}

	public function list_notif($ID_INCIDENT){
		$this->db->select("b.FULLNAME, b.EMAIL, b.TEMBUSAN, c.NM_JABATAN, to_char(a.TANGGAL,'dd-mm-yyyy hh24:mi') as TANGGAL_NOTIFIKASI");
		$this->db->where("a.ID_INCIDENT",$ID_INCIDENT);
		$this->db->join("M_JABATAN c","a.ID_JABATAN=c.ID_JABATAN");
		$this->db->join("M_OPCO b","a.ID_OPCO=b.ID_OPCO and a.ID_JABATAN=b.ID_JABATAN AND b.DELETED=0 and b.ID_JABATAN=c.ID_JABATAN");
		$this->db->order_by("a.ID_JABATAN, a.TANGGAL","asc");
		return $this->db->get("T_NOTIFIKASI a")->result();
	}


	public function get_query($where= array(), $accessRa, $accessQc){

		if(isset($where)){
			$sql = "";
			if($where['status']){
				$sql .= " a.ID_NCQR_STATUS in (".$where['status'].") ";
			}

			if($where['closed'] == 'Y'){
				if($where['status']){
					$sql .= " OR a.CLOSED_STATUS = 'Y' " ;
				}else{
					$sql .= " a.CLOSED_STATUS = 'Y' " ;
				}
			}else{
				$sql .= " AND a.CLOSED_STATUS = 'N' ";
			}
			$this->db->where($sql);
		}

		// if ($where['id_company']) $this->db->where('e.ID_COMPANY', $where['id_company']);
		// if ($this->USER->ID_COMPANY) $this->db->where('e.ID_COMPANY', $this->USER->ID_COMPANY);
		$this->db->where('e.ID_COMPANY', $where['id_company']);
		
		$condition = "SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT";

		$this->db->select("a.*,b.NM_TEKNISI,c.JUDUL_SOLUTION, d.NM_AREA, e.NM_PLANT, e.ID_COMPANY,
			CASE 
				WHEN (".$condition.") >= 3 AND (".$condition.") < 6 THEN 'M1' 
				WHEN (".$condition.") >= 6 AND (".$condition.") < 9 THEN 'M2' 
				WHEN (".$condition.") = 9 THEN 'MY' 
				WHEN (".$condition.") > 9 THEN 'EQR' 
				ELSE '-'
			END
			 as TYPE");
		$this->db->from($this->table . ' a');
		$this->db->join("M_TEKNISI b","a.ID_TEKNISI=b.ID_TEKNISI","LEFT");
		$this->db->join("M_SOLUTION c","a.ID_SOLUTION=c.ID_SOLUTION","LEFT");
		$this->db->join('M_AREA d', 'a.ID_AREA = d.ID_AREA');
		$this->db->join('M_PLANT e', 'd.ID_PLANT = e.ID_PLANT');

		$this->db->order_by("a.ID_INCIDENT","DESC");

		// echo $this->db->last_query();
	}

	public function get_list($where= array(), $accessRa, $accessQc) {
		$this->get_query($where, $accessRa, $accessQc);
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);

		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}
		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			#echo $this->db->get_compiled_select();exit();
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	/** Count query result after filtered **/
	public function count_filtered($status= array()){
		$this->get_query($status);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all($status= array()){
		$this->get_query($status);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}
    
	//Add fase 2 -------------------------------------------
	
	public function get_query2($where = array(), $accessRa = null, $accessQc = null){
		$this->db->select("*");
		$this->db->from($this->table2 . ' a');
		if(isset($where)){
			$sql = "";
			if($where['status'] && $where['closed']){
				$sql .= " (ID_NCQR_STATUS in (".$where['status'].") OR CLOSED_STATUS = 'Y')";
			}elseif($where['status'] && $where['closed'] == null){
				$sql .= " (ID_NCQR_STATUS in (".$where['status'].")  AND CLOSED_STATUS = 'N') ";
			}elseif($where['status'] == null && $where['closed']){
				$sql .= " CLOSED_STATUS = 'Y'";
			}

			if($where['id_company']){ 
				// $this->db->where('ID_COMPANY', $where['id_company']);
				$this->db->like('ID_COMPANY', $where['id_company']); // izza 2021.03.03
			}elseif($this->USER->ID_COMPANY){
				$this->db->where('ID_COMPANY', $this->USER->ID_COMPANY);
			}

			$this->db->where($sql);
		}
	}

	public function get_list2($where= array(), $accessRa, $accessQc) {
		$this->get_query2($where, $accessRa, $accessQc);
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}
		$this->db->order_by("TANGGAL", "DESC"); // izza 2021.03.03
		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}
		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			#echo $this->db->get_compiled_select();exit();
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}
		return $query->result();
	}

	/** Count query result after filtered **/
	public function count_filtered2($where= array()){
		$this->get_query2($where);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all2($where= array()){
		$this->get_query($where);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}
    
    
	public function ncqr_area($ID_COMPANY=null,$ID_PLANT=NULL){
		$this->db->select("a.ID_AREA, a.KD_AREA, a.NM_AREA");
		$this->db->from("M_AREA a");
		$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT","LEFT");
		$this->db->join("M_COMPANY c","b.ID_COMPANY=c.ID_COMPANY","LEFT");
		$this->db->join("M_GROUPAREA d","a.ID_GROUPAREA=d.ID_GROUPAREA","LEFT");
		$this->db->order_by("a.ID_AREA");
		if($ID_COMPANY) $this->db->where("c.ID_COMPANY",$ID_COMPANY);
		if($ID_PLANT) $this->db->where_in('a.ID_PLANT', $ID_PLANT);
		$this->db->where_in("d.ID_GROUPAREA",array(1,4));
        $data =  $this->db->get();
        // echo $this->db->last_query();
		return $data->result();
	}
    public function ncqr_incident($param){

    	$q = "";
    	foreach ($param as $i => $iv) {
    		if($i == 'TANGGAL'){
    			$q .= "  TO_CHAR(a.{$i}, 'DD/MM/YYYY') = '{$iv}' ";
    		}elseif($i == 'ID_COMPONENT'){
    			if(!empty($q)){
    				$q.='AND';
    			}
    			$q .= "  a.ID_COMPONENT in ({$iv}) ";
    		}else{
    			if(!empty($q)){
    				$q.='AND';
    			}
    			$q .= "  a.{$i} = '{$iv}' ";
    		}
    	}
    	$sql = "";

    	$sql .= "
    		SELECT 
				*
			FROM 
				V_INCIDENT a
			LEFT JOIN M_INCIDENT_TYPE b ON a.ID_INCIDENT_TYPE = b.ID_INCIDENT_TYPE
            LEFT JOIN M_COMPONENT c ON c.ID_COMPONENT = a.ID_COMPONENT
			WHERE {$q} 
			ORDER BY a.ID_INCIDENT DESC
    	";

        log_message('debug', 'SQL List NCQR Incident ' . $sql);

    	$result = $this->db->query($sql);
    	// echo $this->db->last_query();
    	// exit;

    	return $result->result_array();
    }
    public function receiver_incident($param){
    	// print_r($param);
    	$sql = "
    		SELECT 
			M_INCIDENT_ACTION.*,
				TO_CHAR(M_INCIDENT_ACTION.CREATE_DATE, 'DD-MM-YYYY') AS DATE_INCIDENT
			FROM 
				M_INCIDENT_ACTION 
			WHERE 
				ID_INCIDENT = {$param['ID_INCIDENT']}
			ORDER BY ID_ACTION ASC
    	";
    	$result = $this->db->query($sql);
    	// echo $this->db->last_query();
    	// exit;

    	return $result->row();
    }

    public function get_inci($param){
    	// print_r($param);
    	$sql = "
    		SELECT 
				*
			FROM 
				M_INCIDENT_ACTION 
			WHERE 
				ID_INCIDENT = ".$param."
			ORDER BY ID_ACTION ASC
    	";
    	$result = $this->db->query($sql);
    	// echo $this->db->last_query();
    	// exit;

    	return $result->row();
    }

    public function ncqr_incidentCheck($param){

    	$q = "";
    	foreach ($param as $i => $iv) {
    		if($i == 'TANGGAL'){
    			$q .= " AND TO_CHAR(a.{$i}, 'DD/MM/YYYY') = '{$iv}' ";
    		}else{
    			$q .= " AND a.{$i} = '{$iv}' ";
    		}
    	}
    	$sql = "";

    	$sql .= "
    		SELECT inner_query.*, rownum rnum FROM (SELECT a.*, b.NM_TEKNISI, c.JUDUL_SOLUTION, d.NM_AREA, e.NM_PLANT,f.NM_COMPANY, g.KD_COMPONENT ,g.NM_COMPONENT, h.ANALISA as VANALISA, TO_CHAR(h.JAM, 'DD-MM-YYYY HH24:MI') AS JAM_ANALISA, CASE 
				WHEN (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) >= 3 AND (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) < 6 THEN 'M1' 
				WHEN (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) >= 6 AND (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) < 9 THEN 'M2' 
				WHEN (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) = 9 THEN 'MY' 
				WHEN (SELECT count(ID_INCIDENT) FROM D_INCIDENT WHERE ID_INCIDENT=a.ID_INCIDENT) > 9 THEN 'EQR' 
				ELSE '-'
			END
			 as TYPE
				FROM M_INCIDENT a
				LEFT JOIN M_TEKNISI b ON a.ID_TEKNISI=b.ID_TEKNISI
				LEFT JOIN M_SOLUTION c ON a.ID_SOLUTION=c.ID_SOLUTION
				JOIN M_AREA d ON a.ID_AREA = d.ID_AREA
				JOIN M_PLANT e ON d.ID_PLANT = e.ID_PLANT
				JOIN M_COMPANY f ON e.ID_COMPANY = f.ID_COMPANY
				JOIN M_COMPONENT g ON a.ID_COMPONENT = g.ID_COMPONENT
				JOIN D_INCIDENT h ON h.ID_INCIDENT = a.ID_INCIDENT
				WHERE
					a.ID_NCQR_STATUS IS NOT NULL {$q} 
				ORDER BY a.ID_INCIDENT DESC, a.ID_INCIDENT ASC
				) inner_query WHERE rownum <= 1
    	";

    	$result = $this->db->query($sql);

    	return $result->row_array();
    }

    public function mail_incident($param){

    	$q = "";
    	foreach ($param as $i => $iv) {
    		if($i == 'a.ID_AREA'){
				$q .= " AND {$i} = '{$iv}' ";
    		}else{
				$q .= " AND {$i} in ({$iv}) ";
    		}
    	}

    	$sql = "";
    	$sql .= "
    		SELECT
				a.*
			FROM
				M_OPCO a
			JOIN M_AREA b ON
				a.ID_AREA = b.ID_AREA
			JOIN M_PLANT c ON
				b.ID_PLANT = c.ID_PLANT
			JOIN M_COMPANY d ON
				c.ID_COMPANY = d.ID_COMPANY
			WHERE
				a.DELETED = '0' {$q}
			ORDER BY
				a.ID_OPCO
    	";

    	$result = $this->db->query($sql);

    	return $result->result_array();
    } 

    public function getGroupPlant($where = array()){
		$q = "";
    	foreach ($where as $i => $iv) {
			$q .= " AND B.{$i} = '{$iv}' ";
    	}

    	$sql = "
    		SELECT
				DISTINCT
				A.ID_GROUP_PLANT, A.NM_GROUP_PLANT
			FROM
				M_GROUPPLANT A
				JOIN M_PLANT B ON A.ID_GROUP_PLANT = B.ID_GROUP_PLANT
			WHERE
				A.ID_GROUP_PLANT IS NOT NULL {$q} 
			ORDER BY A.ID_GROUP_PLANT ASC
    	";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function getGroupArea($where = array()){
		$q = "";
    	foreach ($where as $i => $iv) {
			$q .= " AND A.{$i} = '{$iv}' ";
    	}

    	$sql = "
    		SELECT
				GA.ID_GROUPAREA, GA.NM_GROUPAREA
			FROM
				M_AREA A
				LEFT JOIN M_GROUPAREA GA ON A.ID_GROUPAREA = GA.ID_GROUPAREA
			WHERE
				GA.ID_GROUPAREA IN (1,4) {$q} 
			GROUP BY
				GA.ID_GROUPAREA, GA.NM_GROUPAREA
    	";

    	$result = $this->db->query($sql);

    	return $result->result_array();

    }

    public function getPlant($id_grouparea, $id_group_plant, $id_plant){

    	$sql = "
    		SELECT
				P.ID_PLANT, P.NM_PLANT
			FROM
				M_PLANT P
			JOIN M_AREA A ON
				P.ID_PLANT = A.ID_PLANT
			WHERE
				A.ID_GROUPAREA = {$id_grouparea}
				AND P.ID_GROUP_PLANT = {$id_group_plant}
				AND P.ID_PLANT in ($id_plant) 
			GROUP BY P.ID_PLANT, P.NM_PLANT ORDER BY P.NM_PLANT
    	";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function getArea($id_plant, $id_grouparea){

    	$sql = "
    		SELECT
				*
			FROM
				M_AREA A
			WHERE
				A.ID_GROUPAREA = {$id_grouparea}
				AND A.ID_PLANT = {$id_plant}
    	";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }

    public function totalIncident($year = 2018, $month = 10, $id_plant = 5, $closed = 'N', $id_grouparea = 4, $id_ncqr_status){
    	$q = "";
    	if($id_ncqr_status){
    		$q .= " AND I.ID_NCQR_STATUS = $id_ncqr_status ";
    	}
    	$sql = "
    		SELECT
				NVL(COUNT(*), 0) AS TOTAL
			FROM
				M_INCIDENT I
				JOIN M_AREA A ON I.ID_AREA = A.ID_AREA
			WHERE
				EXTRACT(YEAR FROM I.TANGGAL) = $year AND EXTRACT(MONTH FROM I.TANGGAL) = $month AND A.ID_PLANT = $id_plant AND CLOSED_STATUS = '$closed'  {$q} AND A.ID_GROUPAREA = $id_grouparea
    	";
    	$query = $this->db->query($sql);

    	// echo $this->db->last_query().'<br><br>';

    	return $query->row_array()['TOTAL'];
    }

    public function totalIncidentFM($year = 2018, $month = 10, $id_plant = 5, $id_area = null, $closed = 'N', $id_grouparea = 4, $id_ncqr_status){
    	$q = "";
    	if($id_ncqr_status){
    		$q .= " AND I.ID_NCQR_STATUS = $id_ncqr_status ";
    	}
    	$sql = "
    		SELECT
				NVL(COUNT(*), 0) AS TOTAL
			FROM
				M_INCIDENT I
				JOIN M_AREA A ON I.ID_AREA = A.ID_AREA
			WHERE
				EXTRACT(YEAR FROM I.TANGGAL) = $year AND EXTRACT(MONTH FROM I.TANGGAL) = $month AND A.ID_PLANT = $id_plant AND  A.ID_AREA = $id_area AND CLOSED_STATUS = '$closed'  {$q} AND A.ID_GROUPAREA = $id_grouparea
    	";
    	$query = $this->db->query($sql);

    	// echo $this->db->last_query().'<br><br>';

    	return $query->row_array()['TOTAL'];
    }

	public function getDetailNcqr($where, $month, $year){

		foreach ($where as $key => $value) {
			if($key == 'ID_NCQR_STATUS'){
				$this->db->where($key." in (".$value.")");
			}else{
				$this->db->where($key." = '".$value."'");
			}
		}
// var_dump($where);

		$this->db->select("ID_INCIDENT, NM_AREA, SUBJECT, NM_INCIDENT_TYPE, to_char(TANGGAL, 'DD-MM-YYYY hh24:mi') AS TANGGAL");
		// $this->db->where($where);
		$this->db->where("EXTRACT(MONTH FROM TANGGAL) = $month");
		$this->db->where("EXTRACT(YEAR FROM TANGGAL) = $year");
		$this->db->order_by("ID_INCIDENT");
		$r = $this->db->get("V_INCIDENT")->result();
		// echo $this->db->last_query();
		return $r;
	}


	public function chek_NCQRComponent($where){
		$this->db->select("count(ID_COMPONENT) as vCONFIGURED");
		$this->db->where($where);
		return $this->db->get("C_NCQR_COMPONENT")->result();

	}

	public function chek_and_setNCQR($where){

        $id_plant = ($where['ID_PLANT'] ? $where['ID_PLANT'] : 'NULL'); 
        $id_area = ($where['ID_AREA'] ? $where['ID_AREA'] : 'NULL'); 
        $id_component = ($where['ID_COMPONENT'] ? $where['ID_COMPONENT'] : 'NULL');
        $nilai =  ($where['NILAI'] ? $where['NILAI'] : 'NULL');
        $tanggal = ($where['TANGGAL'] ?
            sprintf('to_date(%s, \'DD/MM/YYYY\')', $where['TANGGAL']) :
            'NULL');
        $jam_data = ($where['JAM_DATA'] ? $where['JAM_DATA'] : 'NULL'); 
        $id_cement_hourly = ($where['ID_CEMENT_HOURLY'] ? $where['ID_CEMENT_HOURLY'] : 'NULL');
        $id_product = ($where['ID_PRODUCT'] ? $where['ID_PRODUCT'] : 'NULL');
        $id_mesin_status = ($where['ID_MESIN_STATUS'] ? $where['ID_MESIN_STATUS'] : 'NULL');
        $tanggal_insiden = ($where['TANGGAL_INSIDEN'] ?
            sprintf('to_date(%s,\'DD/MM/YYYY\')', $where['TANGGAL_INSIDEN']) :
            'NULL');

		$call_procedure = "CALL SP_NCQR_CEMENT(".$id_plant.", ".$id_area.", ".$id_component.", ".$nilai.", ".$tanggal.", ".$jam_data.", ".$id_cement_hourly.", ".$id_product.", ".$tanggal_insiden.",".$id_mesin_status.")";
        log_message('debug', $call_procedure);
		// echo $call_procedure.'<br>';
   		$this->db->query($call_procedure);
	}

	public function set_SendNotif($listID, $status){
		$this->db->set('SEND_NOTIF', $status);
		if($status == 'N'){
			$this->db->where_in("SEND_NOTIF", 'Y');
		}else{
			$this->db->where_in("ID_INCIDENT", $listID);
		}
		$this->db->update("M_INCIDENT"); 
	}

}
