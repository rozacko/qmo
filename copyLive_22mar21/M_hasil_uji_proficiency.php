<?php

class M_hasil_uji_proficiency Extends DB_QM {

    public function __construct(){
        parent::__construct();
        // $this->db = $this->load->database('mso_prod', TRUE);
    }

    public function get_query(){
		$this->db->select("*");
        $this->db->from("T_PERIODE_PROFICIENCY");
        $this->db->join("T_PROFICIENCY", "T_PERIODE_PROFICIENCY.ID_PP = T_PROFICIENCY.ID_PRIODE");
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", "T_PROFICIENCY.ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->where("T_PERIODE_PROFICIENCY.DELETE_AT IS NULL");
        $this->db->where("T_PROFICIENCY.DELETED_AT IS NULL");
		$this->db->where("T_PROFICIENCY.FILE_PENGEMASAN IS NOT NULL");
		$this->db->where("T_PERIODE_PROFICIENCY.GROUP_PP", "Internal");
	}

	public function get_hasil_uji($ID_PROFICIENCY, $AKSES = "", $USER_ID = ""){
		$this->db->select("*");
		$this->db->from("T_HASIL_UJI_PROFICIENCY");
		$this->db->join("M_LABORATORIUM", "T_HASIL_UJI_PROFICIENCY.ID_LABORATORIUM = M_LABORATORIUM.ID_LAB");
		$this->db->join("T_PENERIMA_PROFICIENCY", "T_HASIL_UJI_PROFICIENCY.ID_PENERIMA_SAMPLE = T_PENERIMA_PROFICIENCY.ID_PENERIMA");
		$this->db->where("T_HASIL_UJI_PROFICIENCY.ID_PROFICIENCY", $ID_PROFICIENCY);
		$this->db->where("T_HASIL_UJI_PROFICIENCY.DELETED_AT IS NULL");
		$this->db->order_by("M_LABORATORIUM.KODE_LAB", "ASC");
		if($AKSES != "all"){
			if($USER_ID != ""){
				$this->db->where("T_PENERIMA_PROFICIENCY.ID_USER_PENERIMA", $USER_ID);
			}
		}
        $data = $this->db->get();
		return $data->result_array();
	}

	public function get_dtl_hasil_uji($ID_HASIL_UJI){
		$this->db->select("*");
		$this->db->from("T_DTL_HASIL_UJI_PROFICIENCY");
		$this->db->join("T_HASIL_UJI_PROFICIENCY", "T_DTL_HASIL_UJI_PROFICIENCY.ID_HASIL_UJI = T_HASIL_UJI_PROFICIENCY.ID_HASIL_UJI");
		$this->db->join("T_PENERIMA_PROFICIENCY", "T_HASIL_UJI_PROFICIENCY.ID_PENERIMA_SAMPLE = T_PENERIMA_PROFICIENCY.ID_PENERIMA");
		$this->db->join("M_DTL_FORM_PENGUJIAN", "T_DTL_HASIL_UJI_PROFICIENCY.ID_QUESTION = M_DTL_FORM_PENGUJIAN.ID_QUESTION");
        $this->db->where("T_DTL_HASIL_UJI_PROFICIENCY.ID_HASIL_UJI", $ID_HASIL_UJI);
        $this->db->where("T_HASIL_UJI_PROFICIENCY.DELETED_AT IS NULL");
        $this->db->where("T_DTL_HASIL_UJI_PROFICIENCY.DELETED_AT IS NULL");
        $this->db->order_by("T_DTL_HASIL_UJI_PROFICIENCY.ID_QUESTION", "ASC");
		// if($AKSES != ""){
		// 	if($USER_ID != ""){
		// 		$this->db->where("T_PENERIMA_PROFICIENCY.ID_USER_PENERIMA", $USER_ID);
		// 	}
		// }
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_dtl_hasil_uji_penerima($ID_PENERIMA){
		$this->db->select("*");
		$this->db->from("T_HASIL_UJI_PROFICIENCY");
		$this->db->where("ID_PENERIMA_SAMPLE", $ID_PENERIMA);
		$this->db->where("DELETED_AT IS NULL");
		$data = $this->db->get();
		return $data->result_array();
	}

	public function get_dtl_hasil_uji_analis($ID_HASIL_UJI){
		$this->db->select("*");
		$this->db->from("T_HASIL_UJI_PROFICIENCY");
		$this->db->where("ID_HASIL_UJI", $ID_HASIL_UJI);
		$this->db->where("DELETED_AT IS NULL");
		$data = $this->db->get();
		return $data->result_array();
	}

    public function get_list() {
        $this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
    }

    public function get_list_pertanyaan($id, $tipe = "", $analis = ""){
		$this->db->select("*");
        $this->db->from("M_FORM_PENGUJIAN");
        $this->db->join("M_DTL_FORM_PENGUJIAN", "M_FORM_PENGUJIAN.ID_FORM = M_DTL_FORM_PENGUJIAN.ID_FORM");
        $this->db->where("M_FORM_PENGUJIAN.DELETED_AT IS NULL");
        $this->db->where("M_DTL_FORM_PENGUJIAN.DELETED_AT IS NULL");
        $this->db->where("M_FORM_PENGUJIAN.ID_KOMODITI", $id);
		$this->db->order_by("M_DTL_FORM_PENGUJIAN.ID_QUESTION", "ASC");
		if($tipe != ""){
			$this->db->where("M_DTL_FORM_PENGUJIAN.TIPE", $tipe);
		}

		if($analis != ""){
			$this->db->join("T_DTL_HASIL_UJI_PROFICIENCY", "M_DTL_FORM_PENGUJIAN.ID_QUESTION = T_DTL_HASIL_UJI_PROFICIENCY.ID_QUESTION");
			$this->db->where("T_DTL_HASIL_UJI_PROFICIENCY.ID_HASIL_UJI", $analis);
		}
        $query = $this->db->get();
        return $query->result();
	}
	
	public function get_list_lab(){
        $this->db->select("*");
        $this->db->from("M_LABORATORIUM");
        $this->db->where("DELETED IS NULL");
        $this->db->order_by("ID_LAB", "ASC");
        $query = $this->db->get();
        return $query->result();
	}
	
	public function add_hasil_uji($data){
		$this->db->set("CREATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
		$this->db->insert("T_HASIL_UJI_PROFICIENCY", $data);
		$id = $this->db->select("MAX(ID_HASIL_UJI) as MAX_ID")->from("T_HASIL_UJI_PROFICIENCY")->get()->row();
		return $id;
	}

	public function update_hasil_uji($data, $ID_HASIL_UJI){
		if(isset($data["APPROVE_AT"])){
			$this->db->set("APPROVE_AT", "TO_DATE('".$data['APPROVE_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
			unset($data["APPROVE_AT"]);	
		}
		$this->db->set("UPDATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
		$this->db->where("ID_HASIL_UJI", $ID_HASIL_UJI)->update("T_HASIL_UJI_PROFICIENCY", $data);
		return true;
	}

	public function add_dtl_hasil_uji($data){
		$this->db->insert_batch("T_DTL_HASIL_UJI_PROFICIENCY", $data);
		if($this->db->error()){
            $status = false;
		}
		else{
            $status = true;
		}

		return $status;
	}

	public function update_dtl_hasil_uji($data){
		$this->db->update_batch("T_DTL_HASIL_UJI_PROFICIENCY", $data, "ID_DTL_HASIL_UJI");
		if($this->db->error()){
            $status = false;
		}
		else{
            $status = true;
		}

		return $status;
	}

	public function detail_answer($ID_PROFICIENCY, $QUEST){
		$sql = "SELECT
		A.ID_HASIL_UJI,
		A.ANSWER AS UJI_1,
		B.ANSWER AS UJI_2,
		(A.ANSWER + B.ANSWER ) / 2 AS UJI_AVG 
	FROM
		(
	SELECT
		A.ID_HASIL_UJI,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%1%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) A
		JOIN (
	SELECT
		A.ID_HASIL_UJI,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%2%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI";

		$data = $this->db->query($sql);
		return $data->result_array();
	}

	public function get_parameter($KOMODITI){
        $form = $this->db->select('ID_FORM')->from('M_FORM_PENGUJIAN')->where('ID_KOMODITI', $KOMODITI)->get()->row();

        $this->db->distinct();
		$this->db->select("PARAMETER");
		$this->db->from("M_DTL_FORM_PENGUJIAN");
        $this->db->where("ID_FORM", $form->ID_FORM);
        $this->db->where('PARAMETER IS NOT NULL');
		
		$data = $this->db->get();
		return $data->result_array();
	}
}