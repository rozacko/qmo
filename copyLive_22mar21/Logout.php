<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Home {

	public function index(){
		$this->session->unset_userdata("USER");
		$this->USER = NULL;
		redirect();
	}

}
