  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/plugins/iCheck/all.css">

    <section class="content-header">
      <h1>
        Data Roles
        <small>information about data roles.</small>
      </h1>

    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <ul class="timeline">
            <li id="layoutList" class="animated fadeIn">
              <i class="fa fa-list bg-blue"></i>

              <div class="timeline-item timeline-primaryhear">
                <span class="time">
                <?php if ($this->PERM_WRITE): ?>
                  <div class="input-group input-group-sm">
                    <a href="<?php echo site_url("roles/add");?>" type="button" class="btn btn-block btn-primary btn-sm"><i class="fa fa-pencil"></i>  Create New</a>
                  </div>
                  <?PHP endif;?>
                 </span>

                <h3 class="timeline-header">Data Roles</h3>

                <div class="timeline-body">
                  <table  id="dt_tables"
                        class="table table-striped table-bordered table-hover dt-responsive nowrap"
                        cellspacing="0"
                        width="100%">
                    <thead>
                      <tr>
                        <!-- <th width="3%">ID</th> -->
                        <th width="5%">No.</th>
                        <th>NAME</th>
                        <th width="3%">CODE</th>
                        <th width="20%"></th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </li>
            <li id="layoutCreate" class="animated fadeIn" style="display: none">
              <i class="fa fa-pencil bg-yellow"></i>
              <div class="timeline-item timeline-primaryhear">
                <h3 class="timeline-header">Add Data Group Menu</h3>
                <div class="timeline-body">
                  <form role="form" method="POST" action="<?php echo site_url("groupmenu/create") ?>" >
                    <div class="box-body">   
                      <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group">                  
                          <label>GROUPMENU NAME</label>
                          <input type="text" class="form-control" name="NM_GROUPMENU" placeholder="Group Menu Name" REQUIRED>
                          </div>                
                        </div>                
                        <div class="col-sm-2">
                          <div class="form-group">                  
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" placeholder="Order">
                          </div>                
                        </div>
                      </div>
                    </div>             

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="submit" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>
            </li>  
            <li id="layoutUpdate" class="animated fadeIn" style="display: none;">
              <i class="fa fa-pencil bg-yellow"></i>
              <div class="timeline-item timeline-primaryhear">
                <h3 class="timeline-header">Update Data Role</h3>
                <div class="timeline-body">
                  <form id="formUpdate" role="form" method="POST" action="<?=base_url('roles/update')?>" >
                    <input id="ID_USERGROUP" type="text" class="form-control" name="ID_USERGROUP" REQUIRED readonly>
                    <div class="box-body">   
                      <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group">                  
                          <label>ROLE NAME</label>
                          <input id="NM_USERGROUP" type="text" class="form-control" name="NM_USERGROUP" placeholder="ROLE NAME" REQUIRED>
                          </div>                
                        </div>                
                        <div class="col-sm-2">
                          <div class="form-group">                  
                            <label>READ</label>
                          <!-- radio -->
                            <label>
                              <input type="radio" name="PERM_READ" value="" class="flat-red"> No
                            </label>
                            <label>
                              <input type="radio" name="PERM_READ" value="1" class="flat-red" checked> Yes
                            </label>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">                  
                            <label>WRITE</label>
                            <label>
                              <input type="radio" name="PERM_WRITE" value="" class="flat-red"> No
                            </label>
                            <label>
                              <input type="radio" name="PERM_WRITE" value="1" class="flat-red" checked> Yes
                            </label>
                          </div>                
                        </div>
                      </div>
                    </div>             

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>
            </li>
            <li id="layoutDetail" class="animated fadeIn" style="display: none">
              <i class="fa fa-list bg-blue"></i>

              <div class="timeline-item timeline-primaryhear">
                <span class="time">
                  <button id="btnBack" class="btn btn-sm btn-default"><i class="fa fa-long-arrow-left"></i> Back</button>
                 </span>

                <h3 class="timeline-header">Detail Roles <span id="title_roles">-</span></h3>

                <div class="timeline-body">
                  <table  id="dt_detTables"
                        class="table table-striped table-bordered table-hover dt-responsive nowrap"
                        cellspacing="0"
                        width="100%">
                    <thead>
                      <tr>
                        <!-- <th width="3%">ID</th> -->
                        <th width="5%">No.</th>
                        <th>NAME</th>
                        <th>COMPANY</th>
                        <th>PLANT</th>
                        <th>AREA</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </li>

            <li>
              <!-- <i class="fa fa-clock-o bg-gray"></i> -->
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section>

<!-- msg confirm -->
<?php if($notice->error): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>

<?php endif; ?>

<?php if($notice->success): ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
<?php endif; ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>

<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<!-- iCheck 1.0.1 -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>

<script>
$(document).ready(function(){
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

  $('#btnCreate').on('click', function(){
    $('#layoutList').hide();
    $('#layoutCreate').show();
    $('#layoutUpdate').hide();
  });
  $('.btnCancel').on('click', function(){
    $('#layoutList').show();
    $('#layoutCreate').hide();
    $('#layoutUpdate').hide();
  });
  $('#btnBack').on('click', function(){
    $('#layoutList').show();
    $('#layoutCreate').hide();
    $('#layoutUpdate').hide();
    $('#layoutDetail').hide();
  });

	$(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});
	
	$(".notice-error").confirm({ 
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});
	
	$(".notice-success").confirm({ 
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-success"
	});
	
	<?php if($notice->error): ?>
	$("#a-notice-error").click();
	<?php endif; ?>
	
	<?php if($notice->success): ?>
	$("#a-notice-success").click();
	<?php endif; ?>


  /** DataTables Init **/
  var table = $("#dt_tables").DataTable({
      language: {
        searchPlaceholder: ""
      },
      "paging": true,
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
              "<'row'<'col-sm-12'rt>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": [
      {
        extend: "pageLength",
        className: "btn-sm bt-separ"
      },
      {
        text: "<i class='fa fa-refresh'></i> Reload",
        className: "btn-sm",
          action: function(){
            dtReload(table);
          }
        }
      ],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url" : '<?php echo site_url("roles/get_list");?>',
      "type": 'post',
        "data": function(data){
          //data.groupmenu = $("#ID_COMPANY").val();
        }
      },
      "initComplete": function(settings, json) {

      },
      "columns" : [
        {data:'1', className: "text-center"},
        {data:'2'},
        {className: "text-right",
          "render": function ( data, type, full, meta ) {
            return '<span class="label label-warning">'+full['3']+'</span>';
          } 
        },
        {
          className: "text-center",
          sortable: false,
          "render": function ( data, type, full, meta ) {
                     return '<?php if($this->PERM_WRITE){?> <div class="btn-group"><button title="View List User" class="btn btn-sm btn-info" typeButton="detail"><i class="fa fa-info-circle"></i></button><button title="Update Role" class="btn btn-sm btn-primary" typeButton="update"><i class="fa fa-pencil"></i></button><button title="Delete Role" class="btn btn-sm btn-danger" typeButton="delete"><i class="fa fa-trash"></i></button></div><?php } ?>';
          }
        },
      ]
    })

  $('#dt_tables tbody').on('click', 'td button', function () {
      // var data = table.row( this ).data();
      // alert( 'You clicked on '+data[0]+'\'s row' );

          var data = table.row($(this).parents('tr')).data();
          var id_roles = data[0];
          $('#title_roles').text(data[2]);
          var typeButton = $(this).attr('typeButton');

        if(typeButton == 'detail'){
          $('#layoutList').hide();
          $('#layoutCreate').hide();
          $('#layoutUpdate').hide();
          $('#layoutDetail').show();
          
          $("#dt_detTables").DataTable().destroy();
          var detTable = $("#dt_detTables").DataTable({
              language: {
                searchPlaceholder: ""
              },
              "paging": true,
              "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
              "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
                      "<'row'<'col-sm-12'rt>>" +
                      "<'row'<'col-sm-5'i><'col-sm-7'p>>",
              "buttons": [ 
              {
                extend: "pageLength",
                className: "btn-sm bt-separ"
              },
              {
                text: "<i class='fa fa-refresh'></i> Reload",
                className: "btn-sm",
                  action: function(){
                    dtReload(dtReload);
                  }
                }
              ],
              "responsive": true,
              "processing": true,
              "serverSide": true,
              "ajax": {
              "url" : '<?php echo site_url("roles/get_detList/");?>'+id_roles,
              "type": 'post',
                "data": function(data){
                  //data.groupmenu = $("#ID_COMPANY").val();
                }
              },
              "initComplete": function(settings, json) {

              },
              "columns" : [
                {data:'1', className: "text-center"},
                {data:'2'},
                {data:'3'},
                {data:'4'},
                {data:'5'}
              ]
            });
          
          }else if(typeButton == 'update'){
            window.location.href = '<?=base_url('roles/edit/')?>'+id_roles; 
          }else{
            var data = table.row($(this).parents('tr')).data();
            var url = '<?php echo site_url("roles/delete/");?>' + data[0];

            $.confirm({
                title: "Remove Role",
                text: "This role will be removed. Are you sure?",
                confirmButton: "Remove",
                confirmButtonClass: "btn-danger",
                cancelButton: "Cancel",
                confirm: function() {
                  window.location.href = url;
                },
                cancel: function() {
                    // nothing to do
                }
            });

          }
  } );


    /** DataTable Ajax Reload **/
    function dtReload(table,time) {
      var time = (isNaN(time)) ? 100:time;
      setTimeout(function(){ table.search('').draw(); }, time);
    }

    //DataTable search on enter
    $('#dt_tables_filter input').unbind();
    $('#dt_tables_filter input').bind('keyup', function(e) {
      var val = this.value;
      var len = val.length;

      if(len==0) table.search('').draw();
      if(e.keyCode == 13) {
        table.search(this.value).draw();
      }
    });
});
</script>
