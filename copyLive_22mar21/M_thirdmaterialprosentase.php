<?php

class M_thirdmaterialprosentase Extends DB_QM {
	private $post  = array();
	private $table = "O_THIRD_MATERIAL";
	private $pKey  = "ID_MATERIAL";
	private $column_order = array(NULL, 'URUTAN'); //set column for datatable order
    private $column_search = array('NAME_COMPETITOR', 'NAME_PRODUCT'); //set column for datatable search 
    private $order = array("A.ID_MATERIAL" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	var $column = array(
		'A.ID_MATERIAL', 'A.KODE_MATERIAL', 'A.NAME_MATERIAL', 'A.ID_MATERIAL'
	);


	var $column_scm = array(
		'A.KODE_PERUSAHAAN', 'A.NAMA_PERUSAHAAN', 'A.INISIAL', 'A.PRODUK', 'A.KELOMPOK'
	);

	public function is_sample_exists($data){
		if (isset($data['ID_MATERIAL'])) {
			# code...
		}
		$this->db->where("LOWER(KODE_MATERIAL)", strtolower($data['KODE_MATERIAL']));
		$this->db->where("LOWER(NAME_MATERIAL)", strtolower($data['NAME_MATERIAL']));
		return $this->db->get("O_THIRD_MATERIAL")->num_rows();
	}

	public function sample_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("O_THIRD_MATERIAL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("KODE_MATERIAL", $data['KODE_MATERIAL']);
		$this->db->set("NAME_MATERIAL", $data['NAME_MATERIAL']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $data['MODIFIED_BY']);
		$this->db->where("ID_MATERIAL", $data['ID_MATERIAL']);
		$query = $this->db->update("O_THIRD_MATERIAL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_delete($data){
		$this->db->where("ID_MATERIAL", $data['ID_MATERIAL']);
		$query = $this->db->delete("O_THIRD_MATERIAL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	function _qry($key){
		$this->db->select('A.*');
		$this->db->from('O_THIRD_MATERIAL A');
		if($key['search']!==''){
			$this->db->or_like('LOWER(A.KODE_MATERIAL)', strtolower($key['search']));
			$this->db->or_like('LOWER(A.NAME_MATERIAL)', strtolower($key['search']));
		}
		$this->db->where('A.DELETE_FLAG', 0);
		$order = $this->column[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function _qryscm($key){
		$this->scmdb->select('A.*');
		$this->scmdb->from('ZREPORT_MS_PERUSAHAAN A');
		if($key['search']!==''){
			$this->scmdb->or_like('LOWER(A.KODE_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.NAMA_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.INISIAL)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.PRODUK)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.KELOMPOK)', strtolower($key['search']));
		}
		$order = $this->column_scm[$key['ordCol']];
		$this->scmdb->order_by($order, $key['ordDir']);

	}

	function get($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();
		return $data;
	}

	function get_data($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil($key){
		$this->_qry($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot(){
		$query			= $this->db->get('O_THIRD_MATERIAL');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function get_data_scm($key){
		$this->_qryscm($key);
		$this->scmdb->limit($key['length'], $key['start']);
		$query		= $this->scmdb->get();
		$data			= $query->result();
		return $data;
	}

	function recFil_scm($key){
		$this->_qryscm($key);
		$query			= $this->scmdb->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_scm(){
		$query			= $this->scmdb->get('ZREPORT_MS_PERUSAHAAN');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	public function datalist(){
		$this->db->order_by("a.ID_COMPETITOR");
		return $this->db->get("O_COMPETITOR a")->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

}
