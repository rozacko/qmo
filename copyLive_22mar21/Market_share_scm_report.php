<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Market_share_scm_report extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_market_share_scm");
		$this->load->model("m_company");
		$this->load->library("excel");
	}

	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_market_share_scm", $data);
	}

	public function excel_create_example(){
		# code...
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
		// header('Content-Type: application/vnd.ms-excel'); //mime type
		// header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		// header('Cache-Control: max-age=0'); //no cache

		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");;
		header("Content-Disposition: attachment;filename=$filename");
		header("Content-Transfer-Encoding: binary ");
		            
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

		// $result['msg'] = 'success';
		// to_json($result);

	}


	public function excel_create($tahun, $bulan){
		# code...

		$return_data = $this->Get_MarketShare_SCM_e( $tahun, $bulan );

		$default_border = array(
	    	'style' => PHPExcel_Style_Border::BORDER_THIN,
	    	'color' => array('rgb'=>'0a0a0a')
	    );

	    $style_text = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
	        )
	    );

	    $style_header = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'00bfff'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

		if ($return_data['Data']) {
			# code...

			$sheet = $this->excel->getActiveSheet();

			$indexsheet = 0;
			$charx = 'A';
			$lastchar = 'A';
			$objWorkSheet = $this->excel->createSheet($indexsheet);
			$objWorkSheet->setTitle('Realisasi Market Share '.$tahun.'-'.$bulan);

			foreach ($return_data['Data']['Company'] as $key => $value) {
				# code...
			    $objWorkSheet->getColumnDimension($charx)->setAutoSize(true);
				$objWorkSheet->setCellValue($charx.'1', $value['INISIAL']);
				$lastchar = $charx;
				$charx++;
			}

		}

		$filename = 'Market Share Periode '.$tahun.'-'.$bulan.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		            
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD


		$objWriter->save("php://output");

	}

	public function Get_MarketShare_SCM($value='') {
		# code...
		$post = $this->input->post();

		$return_data = $this->Get_MarketShare_SCM_e( $post['tahun'], $post['bulan'] );

		to_json($return_data);
	}


	public function Get_MarketShare_SCM_e($tahun, $bulan){
		# code...

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';

		$param['TAHUN'] = $tahun;
		$param['BULAN'] = $bulan;

		$list_company = $this->m_market_share_scm->merklist_scmn();

		$t_comp = array();

		foreach ($list_company as $key => $value) {
			# code...
			$t_comp[(int) $value['KODE_PERUSAHAAN']] = $value;
		}

		$list_company = $t_comp;

		$list_region = $this->m_market_share_scm->region_list_scmn();

		$t_region = array();

		foreach ($list_region as $key => $value) {
			# code...
			if ((int) $value['ID_PULAU'] > 0) {
				# code...
				$detail_pulau = $this->m_market_share_scm->region_list_scm_pulau((int) $value['ID_PULAU']);
				$t_data = $value;

				$t_data['ID_PETA'] = (int) $this->str_clean($value['ID_PETA']);
				$t_data['ID_PROV'] = (int) $this->str_clean($value['ID_PROV']);
				$t_data['ID_REGION'] = (int) $this->str_clean($value['ID_REGION']);
				$t_data['KD_PROV'] = (int) $this->str_clean($value['KD_PROV']);

				$t_data['ID_PULAU'] = (int) $detail_pulau['ID_PULAU'];
				$t_data['KD_PULAU'] = $detail_pulau['KD_PULAU'];
				$t_data['NM_PULAU'] = $detail_pulau['NM_PULAU'];

				$param['PROPINSI'] = $t_data['KD_PROV'];

				$d_data = array();

				foreach ($list_company as $ind => $val) {
					# code...
					$param['KODE_PERUSAHAAN'] = (int) $val['KODE_PERUSAHAAN'];
					$param['TIPE'] = '121-301';
					$realisasi_bag = $this->m_market_share_scm->realisasi_value($param);
					$d_data[(int) $val['KODE_PERUSAHAAN']]['BAG'] = 0;
					if ($realisasi_bag) {
						# code...
						$d_data[(int) $val['KODE_PERUSAHAAN']]['BAG'] = (double) $realisasi_bag['QTY_REAL'];
					}
					$param['TIPE'] = '121-302';
					$realisasi_curah = $this->m_market_share_scm->realisasi_value($param);
					$d_data[(int) $val['KODE_PERUSAHAAN']]['CURAH'] = 0;
					if ($realisasi_bag) {
						# code...
						$d_data[(int) $val['KODE_PERUSAHAAN']]['CURAH'] = (double) $realisasi_curah['QTY_REAL'];
					}
				}

				$t_data['REALISASI_MS'] = $d_data;
				$t_region[$detail_pulau['NM_PULAU']][] = $t_data;
			}
		}

		$return['Status'] = true;
		$return['Message'] = 'Record Found';
		$return['Data']['Company'] = $list_company;
		$return['Data']['Region Data'] = $t_region;

		return $return;
	}

	public function ajax_get_sample_area_scm(){
		$sample_area= $this->m_market_share_scm->samplearealist_scm();
		to_json($sample_area);
	}

	public function ajax_get_company_scm(){
		$sample_area= $this->m_market_share_scm->sample_perusahaan_scm();
		to_json($sample_area);
	}

	public function ajax_get_type_produk_scm(){
		$tdata = array();
		$sample_produk= $this->m_market_share_scm->merklist_scmn();

		foreach ($sample_produk as $key => $value) {
			# code...
			$ttype = array();
			$type_produk = $this->m_market_share_scm->type_produk_scm($value['KODE_PERUSAHAAN']);

			if ($type_produk) {
				# code...

				foreach ($type_produk as $ind => $val) {
					# code...
					$ttype[] = preg_replace('/\s+/', '', $val['KD_PRODUCT']);
				}

				$tdata[$value['PRODUK']] = $ttype;

			} else {
				# code...
				$ttype[] = 'PCC';
				$tdata[$value['PRODUK']] = $ttype;

			}
			
		}

		to_json($tdata);
	}

	private function str_clean($chr){
		return preg_replace('/\s+/', '', $chr);
	}

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
