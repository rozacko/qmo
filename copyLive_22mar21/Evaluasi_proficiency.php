<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluasi_proficiency extends QMUser {

    public function __construct(){
        parent::__construct();
        $this->load->helper("string");
        $this->load->model("M_evaluasi_proficiency");
        $this->load->model("M_sampling");
        $this->load->model("M_hasil_uji_proficiency");
    }

    public function index(){
		$this->template->adminlte("v_evaluasi_proficiency");
    }

    public function add(){
        $proficiency = $this->input->get('proficiency');
        $komoditi = $this->input->get('komoditi');
		$this->template->adminlte("v_evaluasi_proficiency_add");
    }

    public function get_list(){
        $list = $this->M_evaluasi_proficiency->get_list();
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["ID_PP"] = $column->ID_PP;
			$row["TITLE_PP"] = $column->TITLE_PP;
			$row["YEAR_PP"] = $column->YEAR_PP;
			$row["ID_PROFICIENCY"] = $column->ID_PROFICIENCY;
			$row["ID_KOMODITI"] = $column->ID_KOMODITI;
			$row["NAMA_SAMPLE"] = $column->NAMA_SAMPLE;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_evaluasi_proficiency->count_all(),
            "recordsFiltered" => $this->M_evaluasi_proficiency->count_filtered(),
            "data" => $data,
        );

		to_json($output);
    }

    public function zscored(){
        $proficiency = $this->input->get("proficiency");
        $komoditi = $this->input->get("komoditi");

        $this->proficiency = $this->M_sampling->detail($proficiency);
        $this->template->adminlte("v_evaluasi_zscored");
    }

    public function count_zscored(){
        $proficiency = $this->input->get("proficiency");
        $komoditi = $this->input->get("komoditi");

        $result = array();

        $kd_komoditi = $this->M_evaluasi_proficiency->get_kode_komoditi($komoditi);
        $parameter = $this->M_hasil_uji_proficiency->get_parameter($komoditi);
        foreach ($parameter as $key => $value) {
            $get_uji = $this->M_evaluasi_proficiency->get_hasil_uji($proficiency, $value['PARAMETER']);
            $med_si = $this->M_evaluasi_proficiency->get_median_uji($proficiency, $value['PARAMETER']);

            $data_si = array();
            $data_di = array();
            foreach($get_uji as $uji){
                array_push($data_si, (float)$uji["SI"]);
                array_push($data_di, (float)$uji["DI"]);
            }

            $median = array();
            foreach($med_si as $med){
                $data = array(
                    "UJI_1" => $med["MED_UJI_1"],
                    "UJI_2" => $med["MED_UJI_2"],
                    "SI" => (float)$med["MED_SI"],
                    "DI" => (float)$med["MED_DI"],
                );

                array_push($median, $data);
            }

            $q1_si = $this->getFirstQuartile($data_si);
            $q1_di = $this->getFirstQuartile($data_di);

            $q3_si = $this->getThirdQuartile($data_si);
            $q3_di = $this->getThirdQuartile($data_di);

            $niqr_si = 0.7413 * ($q3_si - $q1_si);
            $niqr_di = 0.7413 * ($q3_di - $q1_di);

            $data_hasil = array();
            $data_lab = array();
            $data_zscore = array();

            foreach($get_uji as $uji){
                $med_si = (float)$median[0]["SI"];
                $zbi = round(((float)$uji["SI"] - $med_si) / $niqr_si, 4);
                $data = array(
                    "ID_HASIL_UJI" => $uji["ID_HASIL_UJI"],
                    "KODE_LAB" => $kd_komoditi[0]->KODE_SAMPLE."_".$uji["KODE_LAB"],
                    "UJI_1" => $uji["UJI_1"],
                    "UJI_2" => $uji["UJI_2"],
                    "SI" => (float)$uji["SI"],
                    "DI" => (float)$uji["DI"],
                    "MED_SI" => $med_si,
                    "NIQR_SI" => $niqr_si,
                    "ZBI" => $zbi,
                    "STATUS" => (abs($zbi) > 3 ? "$$" : abs($abi) > 2 ? "$$" : "Ok")
                );

                array_push($data_lab, $kd_komoditi[0]->KODE_SAMPLE."_".$uji["KODE_LAB"]);
                array_push($data_zscore, $zbi);

                array_push($data_hasil, $data);
            }

            array_push($result, array("parameter" => $value['PARAMETER'], "data" => $data_hasil, "lab" => $data_lab, "zscore" => $data_zscore));
        }

        to_json($result);
    }

    public function getFirstQuartile($numbers) {
		$count = $this->numRows($numbers)/2;
		$even = $this->isEven($count);
		$median = 0;
		$num1 = ($count / 2)-1;
		$num2 = ($count / 2);
		if($even) {
			// get two numbers from the numbers list
			$median = ($numbers[$num1] + $numbers[$num2]) / 2;
		} else {
			// get just the middle number
			$median = $numbers[$num1];
		}

		return $median;
	}

    public function getThirdQuartile($numbers) {
        $count = $this->numRows($numbers)/2;
        $even = $this->isEven($count);
        $median = 0;
        $num1 = (($count / 2)-1) + $count;
        $num2 = ($count / 2)+$count;
        if($even) {
            // get two numbers from the numbers list
            $median = ($numbers[$num1] + $numbers[$num2]) / 2;
        } else {
            // get just the middle number
            $median = $numbers[$num1];
        }

        return $median;
    }

    public function numRows($numbers) {
        return count($numbers);
    }

    public function isEven($num) {
        if($num % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
    
}