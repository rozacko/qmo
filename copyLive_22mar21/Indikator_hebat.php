<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator_hebat extends QMUser {

	public $LIST;
	public $ID_JENIS_ASPEK;
	public $JENIS_ASPEK;
	public $ID_ASPEK;
	public $ASPEK;
	public $ID_BATASAN;
	public $BATASAN;
	public $KRITERIA;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_aspek_hebat');
		$this->load->model('M_kriteria_hebat');
		$this->load->model('M_batasan_hebat');
		$this->load->model('M_indikator_hebat');
	}
	
	public function index(){
		$this->template->adminlte("v_indikator_hebat");
	}

	public function ajax_get_aspek($id_jenis_aspek){
		$aspek = $this->M_aspek_hebat->get_where(array("ID_JENIS_ASPEK" => $id_jenis_aspek));
		to_json($aspek);
	}

	public function ajax_get_aspek_bobot($id_aspek){
		$bobot = $this->M_aspek_hebat->get_by_id($id_aspek);
		to_json($bobot);
	}

	public function ajax_get_kriteria($id_aspek){
		$kriteria = $this->M_kriteria_hebat->get_where(array("ID_ASPEK" => $id_aspek));
		to_json($kriteria);
	}

	public function ajax_get_kriteria_by_id($id_kriteria){
		$kriteria = $this->M_kriteria_hebat->get_by_id($id_kriteria);
		to_json($kriteria);
	}

	public function ajax_get_batasan($id_kriteria){
		$kriteria = $this->M_batasan_hebat->get_where(array("ID_KRITERIA" => $id_kriteria));
		to_json($kriteria);
	}

	public function add(){
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->template->adminlte("v_indikator_hebat_add");
	}

	public function edit(){
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->LIST = $this->M_indikator_hebat->get_by_id($this->input->post('ID_INDIKATOR'));
		$this->BATASAN = $this->M_batasan_hebat->get_by_id($this->LIST->ID_BATASAN);
		$this->KRITERIA = $this->M_kriteria_hebat->get_by_id($this->LIST->ID_KRITERIA);
		$this->template->adminlte("v_indikator_hebat_edit", $list);
	}

	public function get_list(){
		$list = $this->M_indikator_hebat->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_INDIKATOR;
			$row[] = $no;
			$row[] = $column->KRITERIA;
			$row[] = $column->BATASAN;
			$row[] = $column->INDIKATOR;
			$row[] = $column->SKOR;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_indikator_hebat->count_all(),
            "recordsFiltered" => $this->M_indikator_hebat->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		$post = $this->input->post();
		unset($post['ID_ASPEK']);
		unset($post['ID_KRITERIA']);

		$this->M_indikator_hebat->insert(array_map('strtoupper', $post));

		if($this->M_indikator_hebat->error()){
			$this->notice->error($this->M_indikator_hebat->error());
			redirect("indikator_hebat/add");
		}
		else{
			$this->notice->success("Indikator_hebat Saved.");
			redirect("indikator_hebat");
		}
	}

	public function update(){
		$post = $this->input->post();
		unset($post['ID_ASPEK']);
		unset($post['ID_KRITERIA']);

		$this->M_indikator_hebat->update($this->input->post('ID_INDIKATOR'), $post);

		if($this->M_indikator_hebat->error()){
			$this->notice->error($this->M_indikator_hebat->error());
			redirect("indikator_hebat/edit");
		}
		else{
			$this->notice->success("Indikator_hebat Updated.");
			redirect("indikator_hebat");
		}
	}

	public function remove(){
		$this->M_indikator_hebat->delete($this->input->post('ID_INDIKATOR'));
		if($this->M_indikator_hebat->error()){
			$this->notice->error($this->M_indikator_hebat->error());
		}
		else{
			$this->notice->success("Indikator_hebat Removed.");
		}
		redirect("indikator_hebat");
	}

}

/* End of file Indikator_hebat.php */
/* Location: ./application/controllers/Indikator_hebat.php */
?>