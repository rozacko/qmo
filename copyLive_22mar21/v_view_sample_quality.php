<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  View Sample Quality
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>

      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_COMPANY">COMPANY</label>
                <select id="ID_COMPANY" name="ID_COMPANY" class="form-control select2">
                </select>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="TANGGAL">PERIODE </label>
                <input name="TANGGAL" id="datepicker" type="text" class="form-control" value="<?php echo date("m/Y");?>">
              </div>
              <div class="form-group col-sm-6 col-sm-4" id="DIVID_PRODUCT">
                <label for="ID_PRODUCT">TYPE</label>
                <input type="checkbox" id="checkbox_type_product" title="Select All" class="pull-right">
                <select id="ID_PRODUCT" name="ID_PRODUCT" class="form-control select2" multiple="multiple" data-placeholder="Select a Type Product"
                        style="width: 100%;">
                  <option value="">Please wait...</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <div class="form-group col-sm-3 col-sm-2">
                <span id="saving" style="">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
                <button data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-primary btn-flat" name="load" id="btLoad" style="display: none;"><i class="fa fa-eye"></i> &nbsp; View Sample Data</button>
              </div>
            </div>
            <hr/>
          <!-- </form> -->

          <div class="row col-sm-12">
              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>
            <div id="example1" style="margin-bottom: 15px;" class="hot handsontable htColumnHeaders" ></div>
          </div>

          <div class="form-group row" id="btnActionGroup" style="display: none; margin-right: 15px;">
            
            <div class="form-group col-sm-3 col-sm-2" style="float: right;">
              <button data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-danger btn-flat" name="btDeleteSample" id="btDeleteSample" style=""><i class="fa fa-trash"></i> &nbsp; Delete</button>
            </div>
            <div class="form-group col-sm-3 col-sm-2" style="float: right;">
              <button data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-warning btn-flat" name="btUpdateSample" id="btUpdateSample"><i class="fa fa-edit"></i> &nbsp; Update</button>
            </div>
            <div class="form-group col-sm-3 col-sm-2" style="float: right;">
              <button data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-success btn-flat" name="btExportSample" id="btExportSample"><i class="fa fa-file"></i> &nbsp; Export</button>
            </div>

          </div>

          <div id="divTable" style="display:none;">
            <div class="form-group row">
              <div class="col-sm-12">
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
                <div id="handsonTable"></div>
              </div>
            </div>
            <div class="form-group row">
            </div>

          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

  <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data saved successfully :)"
  ></a>

  <a  id="a-notice-delete"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data deleted successfully :)"
  ></a>  

  <a  id="a-notice-update"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data updated successfully :)"
  ></a>
<!-- eof msg confirm -->

<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }

 table #tb_dev {
    border-collapse: collapse;
    width: 100%;
  }

  #tb_dev th, td {
      text-align: center;
      padding: 8px;
  }

  #tb_dev tr:nth-child(even){background-color: #f2f2f2}

  #tb_dev th {
      background-color: #ffaf38;
      color: white;
  }
</style>

<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- DateRangePicker CSS -->
<link href="<?php echo base_url("plugins/daterangepicker/daterangepicker.css");?>" rel="stylesheet">

<!-- DateRangePicker JS -->
<script src="<?php echo base_url("plugins/daterangepicker/moment.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/daterangepicker/daterangepicker.min.js");?>"/></script>


<!-- Select2 Plugin-->
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/select2/select2.full.min.js");?>"/></script>

<script>
  $(document).ready(function(){

    var glbidcompany = 0;
    var glbnamecompany = '';
    var oglbcomponent = new Array();
    var oglbcomponentid = new Array();
    var oglbcomponentname = new Array();

    var glbselectedcompid = 0 ;
    var glbselectedperiode = '' ;
    var glbselectedidproduct = '' ;

    loadCompany();

	$("#checkbox_type_product").click(function(){
	      if($("#checkbox_type_product").is(':checked') ){
	          $("#ID_PRODUCT > option").prop("selected","selected");
	          $("#ID_PRODUCT").change();
	      }else{
	          $("#ID_PRODUCT > option").removeAttr("selected");
	           $("#ID_PRODUCT").change();
	       }
	});

    $('#datepicker').daterangepicker();

    loadTypeProductList();

    $("#btLoad").click(function(event){
      $('#btLoad').button('loading');
      loadSampleData();
    });

    $("#btDeleteSample").click(function(event){
      $('#btDeleteSample').button('loading');

      var selected_data = new Array();

      var selected = hot1.getSelected();

      if (selected) {

        console.log(selected);

        var htdata = hot1.getData();

        console.log(htdata);

        hot1.getDataAtCell(0,0);

        for (var i = parseInt(selected[0][0]); i <= parseInt(selected[0][2]); i++) {
          data = hot1.getDataAtRow(i);
          selected_data.push(data);
        }

        console.log(selected_data);

        DeleteSampleQuality(selected_data);

      }

      $('#btDeleteSample').button('reset');
    });

    $("#btUpdateSample").click(function(event){
      $('#btUpdateSample').button('loading');

      UpdateSampleQuality();

      $('#btUpdateSample').button('reset');
    });  

    $("#btExportSample").click(function(event){
      $('#btExportSample').button('loading');
      
        var base_url = '<?php echo site_url();?>';
        var url = "view_sample_quality/excel_create/"+glbselectedcompid+"/"+glbselectedperiode+"/"+glbselectedidproduct;
        location.assign(base_url+url);

      $('#btExportSample').button('reset');
    });  

    $( "#ID_PRODUCT" ).change(function() {

    });

    function getContentData() {
      return [
      ];
    }

    function getRowOptions(data) {
      var initial_coloptions = [
        {type: 'date'},
        {type: 'text'},
        {type: 'text'},
        {type: 'text'},
      ];

      var rowcontent = {
            // type: "numeric" // Got some problem in /PROD/qmonline
            type: "text"
      }

      for (var i = 0; i < (data.length)-4; i++) {
        initial_coloptions[i+4] = rowcontent;
      }

      return initial_coloptions;
    }

    function getColHeader(data) {
      var columnlist = new Array();
      for (var i = 0; i < data.length; i++) {
        columnlist.push(data[i]['title']);
      }

      return columnlist;
    }

    var example1 = document.getElementById('example1'), hot1, exportPlugin1;

    function DeleteSampleQuality(selected_data) {
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");
      var companyid = glbidcompany;
      var companyname = glbnamecompany;
      var handsonData = selected_data;
        $.ajax({
          url: "<?php echo site_url("input_sample_quality/delete_sample_data_q");?>",
          data: {"data": handsonData, "company_id": companyid, "companyname": companyname, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {

              $body.removeClass("loading");
              $body.css("cursor", "default");


              $("#a-notice-delete").click();
              $('#btDeleteSample').button('reset');

              loadSampleData();
          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();
              $('#btDeleteSample').button('reset');
          }
        });
    }

    function UpdateSampleQuality() {
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");
      var companyid = glbidcompany;
      var companyname = glbnamecompany;
      var handsonData = hot1.getData();
        $.ajax({
          url: "<?php echo site_url("input_sample_quality/save_sample_data_q");?>",
          data: {"company_id": companyid, "companyname": companyname, "id_comp": oglbcomponentid, "name_comp": oglbcomponentname, "user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {
              $body.removeClass("loading");
              $body.css("cursor", "default");

              $("#a-notice-update").click();
              $('#btUpdateSample').button('reset');

              loadSampleData();
          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();
              $('#btUpdateSample').button('reset');
          }
        });

    }

    function loadSampleData() {
      body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");

      load_component();

      var id_company = $('#ID_COMPANY').val();
      var name_company = $('#ID_COMPANY option:selected').text();

      glbidcompany = parseInt($('#ID_COMPANY').val());
      glbnamecompany = $('#ID_COMPANY option:selected').text();
      var tanggal = $('#TANGGAL').val();
      var id_typeproduct = $('#ID_PRODUCT').val();
      var type_product = $('#ID_PRODUCT option:selected').text();
      var daterange = $('#datepicker').val();
      var tmpdaterange = daterange.split(' - ');

      glbselectedcompid = $('#ID_COMPANY').val();
      glbselectedperiode = $('#datepicker').val();
      glbselectedperiode = glbselectedperiode.replace(/\ - /g, "~");
      glbselectedperiode = glbselectedperiode.replace(/\//g, "-");
      glbselectedidproduct = ($('#ID_PRODUCT').val()).join('~');
      $("#saving").css('display','');

      $.ajax({
        url: "<?php echo site_url("view_sample_quality/load_sample_quality_data");?>",
        data: {"id_company": id_company, "name_company": name_company, "tanggal": tanggal, "id_typeproduct": id_typeproduct, "type_product": type_product, "startdate": tmpdaterange[0], "enddate": tmpdaterange[1]},
        dataType: 'json',
        type: 'POST',
        success: function (result) {


              $body.removeClass("loading");
              $body.css("cursor", "default");

          if (hot1) {

            hot1.destroy();

          }          

          hot1 = new Handsontable(example1, {
            data: result.data,         
            height: 400,
            rowHeaders: true,
            autoColumnSize : true,
            fixedColumnsLeft: 4,
            manualColumnFreeze: true,
            columnSorting: true,
            manualColumnResize: true,
            outsideClickDeselects: false,
            selectionMode: 'multiple',
            colHeaders: getColHeader(result.header.colHeader),
            afterSelection: function(r,c){
              // var data = this.getDataAtRow(r);
              // console.log(data);
            },
            cells: function(row, col, prop){
              var cellProperties = {};
              if(col <= 3){
                cellProperties.readOnly = 'true'
              }
              return cellProperties
            },
            columns: getRowOptions(result.header.colHeader)
          });

          exportPlugin1 = hot1.getPlugin('exportFile');

          if (result.data) {
            $('#btnActionGroup').show();
          } else {
            $('#btnActionGroup').hide();
          } 

          $("#saving").css('display','none');
          $('#btLoad').button('reset');
        },
        error: function () {

              $body.removeClass("loading");
              $body.css("cursor", "default");
              
          $("#saving").css('display','none');
          $('#btLoad').button('reset');
        }
      });

    }

  function load_component() {
    // body...

    oglbcomponent = new Array();
    oglbcomponentid = new Array();
    oglbcomponentname = new Array();

    $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_component_display");?>', function (result) {
      var values = result;
      if (values != undefined && values.length > 0) {
        for (var i = 0; i < values.length; i++) {
          if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
            oglbcomponent.push(values[i]['KD_COMPONENT']);
            oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
            oglbcomponentname.push(values[i]['KD_COMPONENT']);
          }
        }
      }else{
      }
    });
  }

  /** Notification **/
  $(".notice-error").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-success").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });

});

function loadTypeProductList() {
  var typeproduct = $('#ID_PRODUCT');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product/all");?>', function (result) {
    var values = result;
    typeproduct.find('option').remove();
    if (values != undefined && values.length > 0) {
      typeproduct.css("display","");
      $(values).each(function(index, element) {
        typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
      });
    }else{
      typeproduct.find('option').remove();
      typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
    }

    $('#btLoad').show();
    $('#saving').css('display','none');

    $('.select2').select2();
  });
}

function loadCompany() {
  var merk = $('#ID_COMPANY');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_company_scm");?>', function (result) {
    var values = result;
    merk.find('option').remove();
    if (values != undefined && values.length > 0) {
      merk.css("display","");
      $(values).each(function(index, element) {
        merk.append($("<option></option>").attr("value", element.KODE_PERUSAHAAN).text(element.NAMA_PERUSAHAAN));
      });
      // merk.append($("<option></option>").attr("value", '00').text('-- SMI GROUP --'));
    }else{
      merk.find('option').remove();
      // merk.append($("<option></option>").attr("value", '00').text('-- SMI GROUP --'));
    }
  });
}

</script>
