<?php

class TelegramNotification extends CI_Model {
    // private $token = 'uHicP96wfv';
    private $webhook_token;

    public function __construct() {

        $this->config->load('telegram', TRUE);
        $this->load->model('M_opco');

        $telegram_config = @$this->config->item('default', 'telegram');

        // $this->load->library(
        //     'TelegramLib',
        //     [
        //         'api_key' => $telegram_config['api_key'],
        //         'username' => $telegram_config['username'],
        //         'commands_paths' => $telegram_config['commands_paths'],
        //         'webhook_token' => $telegram_config['webhook_token'],
        //         'webhook_url' => $telegram_config['webhook_url'],
        //     ],
        //     'TelegramLib'
        // );

        // $this->webhook_token = $telegram_config['webhook_token'];

        // $this->TelegramLib->getTelegram()->useGetUpdatesWithoutDatabase();

        $this->load->library('TelegramWsdl', $telegram_config, 'TelegramWsdl');

        $this->load->model('M_opco');
    }

    public function sendMessage($chat_id, $message, $options = [])
    {
        $result = $this->TelegramWsdl->sendMessage($chat_id, $message, $options);

        $return = new stdClass;

        $return->status = isset($result->ok) ? $result->ok : ( $result != false );
        $return->result = $result;

        return $return;
    }

    public function send_to_member($opco, $message) {
        // $opco = $this->M_opco->get_data_by_id($id_opco);
        $result = null;
        
        try {
            $this->opco_can_receive_telegram_notif($opco);

            $result = $this->sendMessage($opco->TELEGRAM_CHAT_ID, $message);

            if ( $result->status === false ) {
                throw new Exception($result->result);
            }

            $status = true;
        } catch (Exception $e) {
            static::log($e->getMessage());

            $result = $e->getMessage();
            $status = false;
        }


        return [
            'data'      => $opco,
            'status'    => $status,
            'message'   => $result,
        ];
    }

    public function send_by_notification_member($id_area, $id_jabatan, $message)
    {
        $opcos = $this->M_opco->notification_member($id_area, $id_jabatan);

        $notifier = $this;

        $result = array_map(
            function($opco) use ($notifier, $message) {
                try {
                    $notifier->opco_can_receive_telegram_notif($opco);

                    $result = $notifier->sendMessage($opco->TELEGRAM_CHAT_ID, $message);
                    
                    if ( $result->status === false ) {
                        throw new Exception($result->result);
                    }

                    $status = true;
                } catch (Exception $e) {
                    static::log($e->getMessage());

                    $status = false;
                    $result = $e->getMessage();
                }


                return [
                    'data'      => $opco,
                    'status'    => $status,
                    'message'   => $result,
                ];
            },
            $opcos
        );


        return $result;
    }

    public static function log($message, $severity = 'error') {
        $message_prefix = '[TELEGRAM NOTIFICATION]';

        log_message($severity, sprintf('%s %s', $message_prefix, $message));
    }

    public function opco_can_receive_telegram_notif($opco) {
        if ( ! $opco ) {
            throw new Exception('Invalid OPCO data');
        }

        if ( (bool) $opco->TELEGRAM_NOTIFICATION_ENABLE !== TRUE ) {
            throw new Exception(
                sprintf('Person %s belum diaktifkan notifikasi telegram', $opco->FULLNAME)
            );
        }

        if ( empty($opco->TELEGRAM_CHAT_ID) ) {
            throw new Exception(
                sprintf('Person %s belum memiliki ID Chat', $opco->TELEGRAM_CHAT_ID)
            );
        }

        return true;
    }

    public function handle_webhook($callable = null) {
        // if ( ! empty($callable)  && is_callable($callable)) {
        //     $this->TelegramLib->getTelegram()->setUpdateFilter($callable);
        // }

        // $this->TelegramLib->handleWebhook();

        $this->TelegramLib->handleWebhook(function($update, $telegram) {
            log_message('debug', json_encode($update));

            $message = $update->getMessage() ?: $update->getEditedMessage();

            log_message('debug', 'TEXT : '. $message->getText());
            log_message('debug', 'Entities : '. json_encode($message->getEntities()));

        });
    }

    public function handle_webhook_with_token($token, $callable) {
        if (! $this->is_valid_webhook_token($token)) {
            log_message('error', 'Receive webhook with invalid token');

            return false;
        }

        $this->handle_webhook($callable);
    }

    public function getTelegramLib() {
        return $this->TelegramLib;
    }

    public function set_webhook($url, $options = []) {
        return $this->TelegramLib->setWebhook($url, $options);
    }

    public function set_webhook_with_certificate($url, $certificate_path, $options = []) {
        $this->set_webhook($url, array_merge(
            [
                'certificate' => $certificate_path,
            ],
            $options
        ));
    }

    public function set_webhook_token($token) {
        $this->webhook_token = $token;

        $this->TelegramLib->setToken($token);
    }

    public function get_webhook_token() {
        return $this->TelegramLib->getToken();
    }

    public function is_valid_webhook_token($token) {
        return $token === $this->webhook_token;
    }

    public function subscribe_by_email($email, $message) {
        $opcos = $this->M_opco->get_by_email($email)->result();
        $chat_id = $message->getChat()->getId();

        if (! $opcos) {
            return 404;
        }

        $this->db->trans_begin();

        foreach($opcos as $opco) {
            $this->M_opco->subscribe_to_chat($opco->ID_OPCO, $chat_id);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            log_message('error', 'ROLLBACK with error ' . $this->db->error());

            return 500;
        }

        $this->db->trans_commit();

        return true;
    }       

    public function unsubscribe_from_chat($message) {
        $chat_id = $message->getChat()->getId();
        $opcos = $this->M_opco->get_by_telegram_chat_id($chat_id)->result();

        if (! $opcos) {
            return 404;
        }

        $this->db->trans_begin();

        foreach($opcos as $opco) {
            $this->M_opco->unsubscribe_chat($opco->ID_OPCO);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            log_message('error', 'ROLLBACK with error ' . $this->db->error());

            return 500;
        }

        $this->db->trans_commit();

        return true;
    }
}
