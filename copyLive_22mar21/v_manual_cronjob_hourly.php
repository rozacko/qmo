<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<style>
.highest{
    color:Red;
}​
</style>

<section class="content-header">
  <h1>
  Manual Cronjob Hourly
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      
      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>
      
      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header"> 
            <div class="form-group row">
              <div class="form-group col-sm-2 col-sm-2 picker_hourly" id=""   >
                <label>Tanggal Data Mulai</label>
                <input name="tgl_data_start" id="tgl_data_start" type="text" class=" form-control"  value="<?php echo date("d/m/Y");?>">
              </div> 
              <div class="form-group col-sm-2 col-sm-2 picker_hourly" id=""   >
                <label>Tanggal Data Selesai</label>
                <input name="tgl_data_end" id="tgl_data_end" type="text" class=" form-control"  value="<?php echo date("d/m/Y");?>">
              </div>  
              <div class="form-group col-sm-2 col-sm-2 picker_hourly" id=""   >
                <label>Tanggal Running Cronjob</label>
                <input name="tgl_run" id="tgl_run" type="text" class="datepicker form-control" value="<?php echo date("d/m/Y");?>">
              </div>  
              <div class="form-group col-sm-2 col-sm-2 picker_hourly" id=""   >
                <label>Jam Runnnig Cronjob</label>
				<select id='jam_run' name='jam_run' class=" form-control">
				<?php 
					for($i=1;$i<=24;$i++){
						echo "<option value='{$i}'>{$i}</option>";
					}
				?>
				</select>
              </div>  
            </div>
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn-primary" name="simpan" id="simpan" onclick="simpan()">Simpan Jadwal</button> &nbsp;  
              </div>
            </div>
            <hr/> 
          
          <div id="divTable" >
            <div class="form-group row">
              <div class="col-sm-12">
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
				<table id="table_id" class="display">
					<thead>
						<tr> 
							<th>No</th> 
							<th>Tgl Data Mulai</th> 
							<th>Tgl Data Selesai</th> 
							<th>Tgl Jadwal</th> 
							<th>Jam</th> 
							<th>Create By</th> 
							<th>Create Date</th> 
							<th>Total Insert</th> 
							<th>Total Update</th> 
							<th>Status</th> 
							<th>ID</th> 
							<th>Action</th> 
						</tr>
					</thead>
					<tbody> 
					</tbody>
				</table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
</style>


<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery-ui.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery.dataTables.min.js"); ?>" ></script>
<script src="<?php echo base_url("plugins/datepicker/bootstrap-datepicker.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery.dataTables.min.css"); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("plugins/datepicker/datepicker3.css"); ?>" />

<script src="<?php echo base_url("js/bootstrap-dialog.min.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/bootstrap-dialog.min.css"); ?>" /> 

<script>
    var TabelData;
  $(document).ready(function(){ 
  
        TabelData = $('#table_id').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',  
            "columnDefs": [
                {"visible": false, "targets": 10},  
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>cronjob_convert/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
 
                            var aksi = '<center></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        if( json[i].STATUS==0){
							var status = 'Terjadwal'
						}else{
							var status = 'Done';
						}
                        return_data.push({
                            'id': no,
                            'tgl_data_start': json[i].TGL_DATA_START,
                            'tgl_data_end': json[i].TGL_DATA_END, 
                            'tgl_jadwal': json[i].TGL_JADWAL, 
                            'jam': json[i].JAM_JADWAL, 
                            'create_by': json[i].FULLNAME, 
                            'create_date': json[i].CREATE_DATE, 
                            'insert_tot': json[i].INSERT_HEADER, 
                            'update_tot': json[i].UPDATE_HEADER,
                            'status': status, 
                            'id_jadwal': json[i].ID_JADWAL,  
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'tgl_data_start'},
                {data: 'tgl_data_end'}, 
                {data: 'tgl_jadwal'},
                {data: 'jam'},
                {data: 'create_by'}, 
                {data: 'create_date'},
                {data: 'insert_tot'},
                {data: 'update_tot'},
                {data: 'status'},
                {data: 'id_jadwal'},  
                {data: 'aksi'},
            ],
        });
	
	$('#tgl_data_start, #tgl_data_end').datepicker({
        showOn: "both",
        beforeShow: customRange,
        format: "dd/mm/yyyy",
        autoclose: true
    });	
		
	$(".datepicker").datepicker({ 
		format: 'dd/mm/yyyy',
        autoclose: true
	}); 
	 
    $("#btLoad").click(function(event){
      event.preventDefault();
      $("#handsonTable").html("");
      var formData = $("#formData").serializeArray();
	    var display = $("#DISPLAY").val();
      $("#saving").css('display','none');
      $("#divTable").css("display","");
		
      //Set table columns | Update setting
      $.post('<?php echo site_url("cronjob_convert");?>/insert_from_daily', formData, function (result) {
			// var datas = JSON.parse(result);
			console.log(result);
    		 $("#handsonTable").html(result); 
			  
		
      },"json");
    });
    
  
});

 
    function simpan() {
        var tgl_data_start = $("#tgl_data_start").val();
        var tgl_data_end = $("#tgl_data_end").val();
        var tgl_run = $("#tgl_run").val();  
        var jam_run = $("#jam_run").val(); 
        if (tgl_data_start == "" || tgl_data_end == "" || tgl_run == "" || jam_run == "" ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Form Harus Diisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>cronjob_convert/simpan", {
            "tgl_data_start": tgl_data_start,"tgl_data_end": tgl_data_end,"tgl_run": tgl_run,"jam_run": jam_run, 
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") { 
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Jadwal</b>",
            "message": "Anda yakin ingin menghapus Data \"" + kolom['tgl_data_start'] + "\" -  \"" + kolom['tgl_data_end'] + "\"  Jadwal \"" + kolom['tgl_jadwal'] + "\" Jam \"" + kolom['jam'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_jadwal'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }
	

    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>cronjob_convert/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }
  
  
  
        function informasi(tipe, pesan) {
            var judul = "<b><i class='fa fa-info-circle'></i>&nbsp;Informasi</b>";
            if(tipe == BootstrapDialog.TYPE_WARNING) {
                judul = "<b><i class='fa fa-exclamation-circle'></i>&nbsp;Kesalahan</b>";
            } else if(tipe == BootstrapDialog.TYPE_DANGER) {
                judul = "<b><i class='fa fa-times-circle'></i>&nbsp;Gagal</b>";
            } else if(tipe == BootstrapDialog.TYPE_SUCCESS) {
                judul = "<b><i class='fa fa-check-circle'></i>&nbsp;Berhasil</b>";
            }
            BootstrapDialog.show({
                "type": tipe,
                "title": judul,
                "message": pesan,
                "buttons": [{
                    "cssClass": "btn btn-default btn-xs",
                    "icon": "fa fa-check",
                    "label": "OK",
                    "action": function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }
		
		
function customRange(input) {

    if (input.id == 'tgl_data_end') {
        var minDate = new Date($('#tgl_data_start').val());
        minDate.setDate(minDate.getDate() + 1)

        return {
            minDate: minDate

        };
    }

    return {}

}
</script>
