

<?php

// var_dump($this->list_company);
// exit;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quality Management Online</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url("assets/templates/adminlte/bootstrap/css/bootstrap.min.css"); ?>">
  
  <!-- Font Awesome -->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"-->
  <link href="<?php echo base_url("css/font-awesome.min.css");?>" rel="stylesheet">

  <!-- Ionicons -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/AdminLTE.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/skins/skin-red-light.min.css") ?>">
  <link rel="stylesheet" href="<?php echo base_url("plugins/pnotify/pnotify.custom.min.css") ?>">

</head>
<body class="hold-transition login-page skin-red-light s">
<div class="login-box">
  <div class="login-logo">
    <a href="/"><b>Quality Management</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><img src="<?php echo base_url("images/LogoSemenIndonesia.jpg") ?>" /></p>
	<?php if($notice->error): ?>
	<p class="login-error"><?php echo $notice->error; ?></p>
	<?php endif; ?>
    <form action="<?php echo site_url("login/registerProcess") ?>" method="post">
      <div class="form-group has-feedback">
        <input type="text" id="fullName" class="form-control" placeholder="Full Name" name="FULLNAME">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" id="uName" class="form-control" placeholder="User Name" name="USERNAME">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="eMail" name="EMAIL" class="form-control" placeholder="Email" type="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <?php
      // var_dump($this->list_company);
      ?>
      <div class="form-group has-feedback">
        <select class="select2 form-control" name="ID_COMPANY" id="id_company">
          <option value="">Choose Company...</option>
          <?php

          foreach ($this->list_company as $i => $v) {
            echo '<option '.$select.' kd_company="'.$v->KD_COMPANY.'" value="'.$v->ID_COMPANY.'">'.$v->NM_COMPANY.'</option>';
          }
          ?>
        </select>
      </div>
      <div class="form-group has-feedback">
        <select class="select2 form-control" name="ID_PLANT" id="id_plant">
          <option value="">Choose Plant...</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <select class="select2 form-control" name="ID_AREA" id="id_area">
          <option value="">Choose Area...</option>
        </select>
      </div>

      <div class="row">
        <div class="col-xs-8">
            <a href="<?php echo site_url('login') ?>"><label style="cursor: pointer;font-weight: normal;"> I already have a membership</label></a><br>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" id="btnRegister" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

  <script src="http://10.15.2.130/DEV/qmonline/templates/adminlte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>js/support.js"></script>
  <script src="<?=base_url()?>plugins/pnotify/pnotify.custom.min.js"></script>

<style>
.login-box {
	width: 500px;
}
.login-error {
	display: block;
	padding: 10px;
	border: 1px solid red;
	color: red;
}
</style>
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"/></script> 
<script src="<?php echo base_url("plugins/select2/select2.min.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script type="text/javascript">



  $(function(){
     $('.select2').select2();
     listPlant();
     listArea();

      var options = {
      url: function(username) {
        return '<?php echo site_url("login/get_username/") ?>';
      },

      getValue: function(element) {
        return element.mk_nama;
      },

      //Description
      template: {
        type: "description",
        fields: {
          description: "MK_CCTR_TEXT"
        }
      },

      //Set return data
      list: {
        onClickEvent: function() {
          var selectedItemValue = $("#fullName").getSelectedItemData().USERNAME;
          var selectedItemValueMail = $("#fullName").getSelectedItemData().mk_email;
          var selectedItemValuComp = $("#fullName").getSelectedItemData().company;

          if(selectedItemValuComp == 7000){
            selectedItemValuComp == 2000;
          }

          $("#uName").val(selectedItemValue).trigger("change");
          $("#eMail").val(selectedItemValueMail).trigger("change");


            var dtCompany = new Array();

              $('#id_company > option').each(function() {
                dtCompany.push({id: $(this).val(), text: $(this).text(), code: $(this).attr('kd_company')});
                  // alert($(this).text() + ' ' + $(this).val());
              });
              $('#id_company').html('');

              dtOption = '';
              $(dtCompany).each(function(i, v) {
                if(v.code == selectedItemValuComp){
                  dtOption += '<option selected kd_company="'+v.code+'" value="'+v.id+'">'+v.text+'</option>';
                }else{
                  dtOption += '<option kd_company="'+v.code+'" value="'+v.id+'">'+v.text+'</option>';
                }
              $('#id_company').html(dtOption);

            });
            // console.log(dtOption);

        }
      },

      ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
          dataType: "json"
        }
      },

      //Sending post data
      preparePostData: function(data) {
        data.username = $("#fullName").val();
        return data;
      },
      theme: "plate-dark", //square | round | plate-dark | funky
      requestDelay: 400
    };

    $("#fullName").easyAutocomplete(options);

  });

  $('#id_company').on('change', function(){
    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
    listArea();
  });
  $('#id_plant').on('change', function(){
    idPlant = $("option:selected", this).val();
    listArea(idPlant);
  });

  function listPlant(idCompany = ''){
      $.ajax({
        url : './getPlant/'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#id_plant').html('');
        $.map( data, function( val, i ) {
          $('#id_plant').append('<option value="'+val.ID_PLANT+'">'+val.NM_PLANT+'</option>');
        });
      });

      // $('select2').select2();
  }

  function listArea(idPlant = ''){
      $.ajax({
        url : './getArea/'+idPlant,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#id_area').html('');
        $.map( data, function( val, i ) {
          $('#id_area').append('<option value="'+val.ID_AREA+'">'+val.NM_AREA+'</option>');
        });
      });

      // $('select2').select2();
  }

    $("form").on("submit", function(e) {
      e.preventDefault();
      var post_url = $(this).attr("action"); //get form action url
      var request_method = $(this).attr("method");
      var form_data = $(this).serialize();
      // console.log( $( this ).serialize() );
      NM_COMPANY = $("#id_company option:selected").text();

      $('#btnRegister').attr('disabled', true);
      
    $.ajax({
        url : post_url,
        type: request_method,
        data : form_data + '&NM_COMPANY='+NM_COMPANY
      }).done(function(response){
        response = JSON.parse(response);
        if(response['status'] == 'success'){
          notif(1, response['message']);
            setTimeout(function(){ window.location = "./"; }, 2500);
        }else{
          notif(4, response['message']);
        }
        $('#btnRegister').attr('disabled', false);
      });
    });


</script>
</body>
</html>
