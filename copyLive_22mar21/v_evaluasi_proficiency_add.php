<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Upload Excel Evaluasi Uji Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header"></div>
				<div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="<?= base_url('evaluasi_proficiency/action_input/').$this->input->get('proficiency'); ?>" method="POST" encytpe="multipart/form-data">
                                <div class="box-body" style="background-color:#c5d5ea;">
                                    <div class="form-group">
                                        <li><b>Uji Proficiency :</b></li>
                                        <li><b>Komoditi :</b></li>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 clearfix">
                                            <label>FILE EXCEL EVALUASI</label>
                                            <input type="file" class="form-control" id="FILE_EVALUASI" name="FILE_EVALUASI" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 clearfix">
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>