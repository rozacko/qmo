<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  GLOBAL REPORT
  <small></small>
  </h1>
</section>
<!-- Main content -->
<!-- <section class="content">
  
</div>
 -->




<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
    
      
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData">
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_COMPANY">MONTH</label><br>
                <SELECT class="form-control select2" name="MONTH" id='bulan'>
                  <?php for($i=1;$i<=12;$i++): ?>
                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                  <?php endfor; ?>
                </SELECT>
              </div>
              <div class="form-group col-sm-6 col-sm-2">
                <label for="ID_COMPANY">YEAR</label><br>
                <SELECT class="form-control select2" name="YEAR" id='tahun'>
                <?php for($i=2016;$i<=date("Y");$i++): ?>
                  <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                <?php endfor; ?>
                </SELECT>
              </div>
            <div class="form-group col-sm-2">
              <label for="ID_COMPANY">&nbsp;</label><br>
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn btn-sm btn-primary" name="load" id="btLoad"><i class="fa fa-eye"></i> Load Data</button>
              </div>
              
            </div>
            <div class="form-group col-sm-2">
                  <label for="">&nbsp;</label><br>
<!--                   <a id="btn-Convert-Html2Image" class="btn btn-sm btn-success" href="#"> <i class="fa fa-image"></i> Export Image</a>
 -->            </div>
            </div>
          </form>
          <hr/>
          
          <div class="row">
            <div class="col-xs-12">
              <div id="container" style="height: 600px"></div>
            </div>
          </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<a  id="a-notice-error"
  class="notice-error"
  style="display:none";
  href="#"
  data-title="Alert"
  data-text=""
></a>
<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  .boxPlot_div { margin:auto;margin-bottom:10px; }
  hr { margin-top: 10px; }

  /* CSS Plotly */
  .annotation {
    border: 20px solid black;
  }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 ) 
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>') 
                  50% 50% 
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;   
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }

  #ap_1 {
    float: left;
  }
</style>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>


function setReport(res){
  colorsSquare = ['#FFD700', '#C0C0C0', '#CD853F', '#EEEEEE', '#008B8B'];

  Highcharts.chart('container', {
   title: {
          text: 'Combination chart'
      },
      xAxis: {
          categories: ['']
      },
      labels: {
          items: [{
              style: {
                  left: '50px',
                  top: '18px',
                  color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
              }
          }]
      },
      tooltip: {
          enabled: false
      },
      colors: ['#FFD700', '#C0C0C0', '#CD853F', '#EEEEEE', '#008B8B'],
      plotOptions: {
          pie: {
              startAngle: -36
          }
      },
      series: [{
          type: 'pie',
          data: res.pie,
          center: [690, 280],
          size: 300,
          showInLegend: false,
          dataLabels: {
                enabled: true,
                format: '<span style="font-size: 16px">{point.name} <br> <span style="font-size: 16px">{point.value}</span></span>',
                distance: -50,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 4
                }
            },
      }],
      chart: {
        style: {
            // fontFamily: '"Source Sans Pro", sans-serif'
        },
        width: '1300',
          backgroundColor: 'white',
          events: {
              load: function () {

                  // Draw the flow chart
                  var ren = this.renderer,
                      colors = Highcharts.getOptions().colors,
                      rightArrow = ['M', 60, 0, 'L', 100, 0, 'L', 95, 5, 'M', 100, 0, 'L', 95, -5],
                      leftArrow = ['M', 35, 0, 'L', 0, 0, 'L', 5, 5, 'M', 0, 0, 'L', 5, -5];

                  // Separator, Kriteria
                  ren.path(['M', 290, 40, 'L', 290, 690])
                      .attr({
                          'stroke-width': 2,
                          stroke: 'silver',
                          dashstyle: 'dash'
                      })
                      .add();

                  // Headers
                  ren.label('KRITERIA', 10, 40).css({fontWeight: 'bold'}).add();
                  ren.label(' UTILIZATION OF NCQR <br>(NON CONFORMITY QUALITY REPORT) ', 10, 82)
                  .attr({
                      fill: colors[0],
                      stroke: 'white',
                      'stroke-width': 2,
                      padding: 12,
                      r: 5,
                      width: 230
                  })
                  .css({
                      color: 'white'
                  })
                  .add()
                  .shadow(true);

  ren.label('UPLOADING TIME NOTOFICATION <br>AND THE MOST COMPLETED DATA <br>ON QM ONLINE ) ', 10, 145)
                      .attr({
                          fill: colors[2],
                          stroke: 'white',
                          'stroke-width': 2,
                          padding: 12,
                          r: 5,
                          width: 230
                      })
                      .css({
                          color: 'white'
                      })
                      .add()
                      .shadow(true);
                      
                      ren.label('CEMENT QUALITY : <br>QAF OF SETTING TIME) ', 10, 225)
                      .attr({
                          fill: colors[3],
                          stroke: 'white',
                          'stroke-width': 2,
                          padding: 12,
                          r: 5,
                          width: 230
                      })
                      .css({
                          color: 'white'
                      })
                      .add()
                      .shadow(true);

                      ren.label('CEMENT QUALITY : <br>QAF OF COMPRESSIVE STRENGTH ) ', 10, 290)
                      .attr({
                          fill: colors[4],
                          stroke: 'white',
                          'stroke-width': 2,
                          padding: 12,
                          r: 5,
                          width: 230
                      })
                      .css({
                          color: 'white'
                      })
                      .add()
                      .shadow(true);


                      // KOTAK 1  
                      
                      ren.label('', 610, 55)
                      .attr({
                          fill: colorsSquare[0],
                          stroke: 'black', 'stroke-width': 2, padding: 12,
                          r: 5, width: 200, height: 100
                      }).css({
                          color: 'white'
                      }).add().shadow(true);
                      ren.label('ACTION PLAN', 680, 65).add();
                      ren.label((res['act_plan'][0]), 615, 90).add();
                      // Anak panah 1
                      ren.path(['M', 250, 210, 'L', 250, 185, 'L', 245, 190, 'M', 250, 185, 'L', 255, 190])
                      .attr({
                          'stroke-width': 2,
                          stroke: colors[1]
                      })
                      .translate(470, -3)
                      .add();

                      if(res.pie.length > 1){
                        // KOTAK 2  
                        ren.label('', 900, 240)
                        .attr({
                            fill: colorsSquare[1], stroke: 'black', 'stroke-width': 2, padding: 12,
                          r: 5, width: 200, height: 100
                        }).css({
                            color: 'white'
                        }).add().shadow(true);
                        // Anak panah 2
                        ren.path(leftArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(546, 310)
                        .add();
                        ren.label('ACTION PLAN', 960, 250).add();
                        ren.label((res['act_plan'][1]), 910, 285).add();

                        // KOTAK 3  
                        ren.label('', 840, 450)
                        .attr({
                            fill: colorsSquare[2], stroke: 'black', 'stroke-width': 2, padding: 12,
                            r: 5, width: 200, height: 100
                        }).css({
                            color: 'white'
                        }).add().shadow(true);
                        //anak panah 3
                        ren.path(rightArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(740, 515)
                        .add();
                        ren.label('ACTION PLAN', 900, 460).add();
                        ren.label((res['act_plan'][2]), 845, 485).add();

                        // KOTAK 4  
                        ren.label('', 380, 450)
                        .attr({
                            fill: colorsSquare[3], stroke: 'black', 'stroke-width': 2, padding: 12,
                            r: 5, width: 200, height: 100
                        }).css({
                            color: 'white'
                        }).add().shadow(true);
                        ren.label('ACTION PLAN', 440, 460).add();
                        ren.label((res['act_plan'][3]), 385, 485).add();
                        //anak panah 4
                        ren.path(leftArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(605, 515)
                        .add();
                        ren.path(['M', 250, 230, 'L', 250, 185, 'M', 250, 185])
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(550, 285)
                        .add();


                        // KOTAK 5  
                        ren.label('', 320, 240)
                        .attr({
                            fill: colorsSquare[4], stroke: 'black', 'stroke-width': 2, padding: 12,
                            r: 5, width: 200, height: 100
                        }).css({
                            color: 'white'
                        }).add().shadow(true);
                        ren.label('ACTION PLAN', 380, 250).add();
                        ren.label((res['act_plan'][4]), 330, 285).add();
                        // Anak panah 5
                        ren.path(rightArrow)
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(798, 310)
                        .add();
                        ren.path(['M', 250, 230, 'L', 250, 185, 'M', 250, 185])
                        .attr({
                            'stroke-width': 2,
                            stroke: colors[1]
                        })
                        .translate(390, 285)
                        .add();


                      }

              }
          }
      },
      title: {
          text: 'Global Report',
          style: {
              color: 'black'
          }
      }

  });
}


























  // var element = $("#divTable"); // global variable
  // var getCanvas; // global variable
        
  

  $(document).ready(function(){
    
    $('.select2').select2();

    $("#btLoad").click(function(event){
      event.preventDefault();

      /* Animation */
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");

      /* Initialize Vars */

      $.ajax({
          type: "POST",
          dataType: "json",
          data: {
            opt_company: $("#opt_company").val(),
            MONTH: $("#bulan").val(),
            YEAR: $("#tahun").val(),
          },
          //url: '<?php echo base_url("js/json/global.json");?>',
          url: '<?php echo base_url("h_report_score/global_json");?>',
          success: function(res){
            $body.removeClass("loading");
            $body.css("cursor", "default"); 
            // res = Object.values(res);
            // console.log(res);
            setReport(res);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $body.removeClass("loading");
            $body.css("cursor", "default");
            $("#a-notice-error").data("text", 'Oops! Something went wrong.<br>Please check message in console');
            $("#a-notice-error").click();

            console.log("XHR: " + JSON.stringify(jqXHR));
            console.log("Status: " + textStatus);
            console.log("Error: " + errorThrown);
          }
        });
    });

    /** Notification **/
    $(".notice-error").confirm({ 
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });
  });
</script>