<?php

class Pairing_trend extends QMUser {

    public function __construct(){
		parent::__construct();
        $this->load->helper("string");
        $this->load->helper("color");
        $this->load->model("m_company");
        $this->load->model("m_pairing_trend");
        $this->load->model("c_range_qaf");
    }
    
    public function index(){
        $this->list_company = $this->m_company->datalist();
        $this->template->adminlte("v_pairing_trend");
    }

    public function ajax_get_product_by_company(){
        $company_ids = (NULL==$this->input->get('company_ids'))? [0]:$this->input->get('company_ids');

        $c_product = $this->m_pairing_trend->data_product_company($company_ids);
        $exists_product= [];
        $c_product = array_filter(
            $c_product,
            function($product) use (&$exists_product) {
                if (in_array($product->ID_PRODUCT, $exists_product)) {
                    return false;
                }

                $exists_product[] = $product->ID_PRODUCT;

                return true;
            }
        );

        if ( $c_product ) {
            // reindex the key
            $c_product = array_combine(range(0, count($c_product)-1), $c_product);
        }
    
		to_json($c_product);
    }
    // start create izza jan 2021
    public function ajax_get_product_by_company_read(){
        $company_ids = (NULL==$this->input->get('company_ids'))? [0]:$this->input->get('company_ids');

        $c_product = $this->m_pairing_trend->data_product_company_read($company_ids); // izza
        $exists_product= [];
        $c_product = array_filter(
            $c_product,
            function($product) use (&$exists_product) {
                if (in_array($product->ID_PRODUCT, $exists_product)) {
                    return false;
                }

                $exists_product[] = $product->ID_PRODUCT;

                return true;
            }
        );

        if ( $c_product ) {
            // reindex the key
            $c_product = array_combine(range(0, count($c_product)-1), $c_product);
        }
    
		to_json($c_product);
    }
    // end create izza jan 2021

    public function ajax_get_component_by_company(){
        $company_ids = (NULL==$this->input->get('company_ids'))? [0]:$this->input->get('company_ids');
        
        $c_component = $this->m_pairing_trend->data_component_company($company_ids);
        $exists_component= [];
        $c_component = array_filter(
            $c_component,
            function($component) use (&$exists_component) {
                if (in_array($component->ID_COMPONENT, $exists_component)) {
                    return false;
                }

                $exists_component[] = $component->ID_COMPONENT;

                return true;
            }
        );

        if ( $c_component ) {
            // reindex the key
            $c_component = array_combine(range(0, count($c_component)-1), $c_component);
        }
    
		to_json($c_component);
    }

    public function trend_chart(){
        $month_start = $this->input->post('month_start');
		$year_start = $this->input->post('year_start');
		$month_end = $this->input->post('month_end');
        $year_end = $this->input->post('year_end');
        
        $month_start = str_pad($month_start, 2, '0', STR_PAD_LEFT);
        $month_end = str_pad($month_end, 2, '0', STR_PAD_LEFT);

        $date_start = date('d/m/Y', strtotime("01-{$month_start}-{$year_start}"));
        $date_end = date('t/m/Y', strtotime("01-{$month_end}-{$year_end}"));

        $company = $this->input->post('companys');
        $component = $this->input->post('components');
        $product = $this->input->post('products');

        $get_company = $this->m_pairing_trend->get_company($company);
        $get_component = $this->m_pairing_trend->get_component($component);
        $get_product = $this->m_pairing_trend->get_product($product);

        $data_pairing = $this->m_pairing_trend->get_data_pairing($company, $product, $component, $date_start, $date_end);
        $data_pairing_by_plant = $this->m_pairing_trend->get_data_pairing($company, $product, $component, $date_start, $date_end, TRUE);
        $min_max_value = $this->m_pairing_trend->get_min_max_value_pairing($company, $product, $component, $date_start, $date_end);
        if($data_pairing){
            $dt_range = array();
            foreach($data_pairing as $range){
                array_push($dt_range, $range['PERIODE']);
            }
            $dt_range = array_values(array_unique($dt_range));

            foreach($get_company as $dt_company){
                $u = array();
                $i = 0;

                if($dt_company['USE_DETAIL_PLANT'] == 'Y'){
                    $get_plant = $this->m_pairing_trend->get_area($dt_company['ID_COMPANY']);
                    foreach($get_plant as $dt_plant){
                        $data["title"] = $dt_plant['NM_PLANT']." : ".$get_product[0]['NM_PRODUCT'];
                        $data["company"] = $dt_plant['NM_PLANT'];
                        $data["value"] = array();

                        foreach($get_component as $dt_component){

                            $value["name"] = trim($dt_component['KD_COMPONENT']);
                            $value["marker"] = array("color" => "rgba(".getFixColor($i).", 1.0)", "dash" => "solid", "width" => "3");
                            $value["type"] = "lines";
                            $value["y"] = array();
                            $value["x"] = $dt_range;
                            $value["mode"] = "lines+text";
                            $value["text"] = array();
                            $value["textposition"] = "top";
                            $val = null;

                            foreach($dt_range as $r){
                                foreach($data_pairing_by_plant as $dt_pair_plant){
                                    if($r == $dt_pair_plant['PERIODE'] && $dt_component['ID_COMPONENT'] == $dt_pair_plant['ID_COMPONENT'] && $dt_plant['ID_PLANT'] == $dt_pair_plant['ID_PLANT']){
                                        $val = (int)$dt_pair_plant['AVG_NILAI'];
                                    }
                                }
                                array_push($value["y"], $val);
                                array_push($value["text"], (String)$val);
                            }
    
                            array_push($data["value"], $value);
                            // MIN MAX
                            
                            $min["name"] = "MIN ".trim($dt_component['KD_COMPONENT']);
                            $min["mode"] = "lines";
                            $min["line"] = array("dash" => "dashdot", "width" => 3, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                            $min["x"] = $dt_range;
                            $min["y"] = array();
                            $min["mode"] = "lines+text";
                            $min["text"] = array();
                            $min["textposition"] = "top";
                            $min_val = null;

                            $max["name"] = "MAX ".trim($dt_component['KD_COMPONENT']);
                            $max["mode"] = "lines";
                            $max["line"] = array("dash" => "dashdot", "width" => 3, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                            $max["x"] = $dt_range;
                            $max["y"] = array();
                            $max["mode"] = "lines+text";
                            $max["text"] = array();
                            $max["textposition"] = "top";
                            $max_val = null;

                            foreach ($dt_range as $r) {
                                foreach ($min_max_value as $mmv) {
                                    if($dt_component['ID_COMPONENT'] == $mmv['ID_COMPONENT'] && $dt_company['ID_COMPANY'] == $mmv['ID_COMPANY']){
                                        $min_val = (double)$mmv['MIN_AVG'];
                                        $max_val = (double)$mmv['MAX_AVG'];
                                    }
                                }
                                array_push($min["y"], $min_val);
                                array_push($max["y"], $max_val);
                            }

                            array_push($data["value"], $min);
                            array_push($data["value"], $max);
                            $i++;
                        }
                        $json[] = $data;
                    }
                } else {
                    $data["title"] = $dt_company['NM_COMPANY']." : ".$get_product[0]['NM_PRODUCT'];
                    $data["company"] = $dt_company['NM_COMPANY'];
                    $data["value"] = array();

                    
                    foreach($get_component as $dt_component){

                        $value["name"] = trim($dt_component['KD_COMPONENT']);
                        $value["marker"] = array("color" => "rgba(".getFixColor($i).", 1.0)", "dash" => "solid", "width" => "3");
                        $value["type"] = "lines";
                        $value["y"] = array();
                        $value["x"] = $dt_range;
                        $value["mode"] = "lines+text";
                        $value["text"] = array();
                        $value["textposition"] = "top";
                        $val = null;

                        foreach($dt_range as $r){
                            foreach($data_pairing as $dt_pair){
                                if($r == $dt_pair['PERIODE'] && $dt_component['ID_COMPONENT'] == $dt_pair['ID_COMPONENT'] && $dt_company['ID_COMPANY'] == $dt_pair['ID_COMPANY']){
                                    $val = (int)$dt_pair['AVG_NILAI'];
                                }
                            }
                            array_push($value["y"], $val);
                            array_push($value["text"], (String)$val);
                        }

                        array_push($data["value"], $value);

                        $minmax = $this->c_range_qaf->configuration_component($dt_company['ID_COMPANY'], '81', $product[0], $dt_component['ID_COMPONENT']);
                        // MIN MAX
                        $min["name"] = "MIN ".trim($dt_component['KD_COMPONENT']);
                        $min["mode"] = "lines";
                        $min["line"] = array("dash" => "dashdot", "width" => 3, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                        $min["x"] = $dt_range;
                        $min["y"] = array();
                        $min["mode"] = "lines+text";
                        $min["text"] = array();
                        $min["textposition"] = "top";
                        $min_val = null;

                        $max["name"] = "MAX ".trim($dt_component['KD_COMPONENT']);
                        $max["mode"] = "lines";
                        $max["line"] = array("dash" => "dashdot", "width" => 3, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                        $max["x"] = $dt_range;
                        $max["y"] = array();
                        $max["mode"] = "lines+text";
                        $max["text"] = array();
                        $max["textposition"] = "top";
                        $max_val = null;

                        foreach ($dt_range as $r) {
                            foreach ($min_max_value as $mmv) {
                                if($dt_component['ID_COMPONENT'] == $mmv['ID_COMPONENT'] && $dt_company['ID_COMPANY'] == $mmv['ID_COMPANY']){
                                    $min_val = (double)$mmv['MIN_AVG'];
                                    $max_val = (double)$mmv['MAX_AVG'];
                                }
                            }

                            if($minmax['V_MAX'] ==  null){
                                array_push($max["y"], $max_val); //JIKA QAF RANGE COMPONENT KOSONG MAKA NILAI MAX DIAMBIL DARI NILAI CHART TERTINGGI
                            } else {
                                array_push($max["y"], $minmax['V_MAX']); //JIKA QAF RANGE COMPONENT ADA MAKA NILAI MAX DIAMBIL DARI NILAI RANGE QAF
                            }

                            if($minmax['V_MIN'] == null || $minmax['V_MIN'] == 0){
                                array_push($min["y"], $min_val);
                            } else {
                                array_push($min["y"], $minmax['V_MIN']);
                            }
                            
                            
                        }

                        
                        array_push($data["value"], $min);

                        if($minmax['V_MAX'] != '9999'){
                            array_push($data["value"], $max); // JIKA QAF RANGE COMPONENT MAX = 99999 MAKA TIDAK DITAMBILKAN LINE CHART MAX
                        }
                        
                        $i++;
                    }

                    $json[] = $data;
                }
                
            }

            
            $y_range = array();
            foreach ($json as $key => $value) {
                foreach($value['value'] as $v_key => $v_val){
                    foreach($v_val['y'] as $y_val){
                        array_push($y_range, $y_val);
                    }
                    
                }
            }

            $min = min($y_range);
            $max = max($y_range) + 30;

            if($min > 0){
                $min = $min - 30;
            }
            $range = array($min, $max);
            $status = "200";
        } else {
            $json = array();
            $range = array();
            $status = "404";
        }
        
        
        to_json(array("status" => $status,"data" => $json, "y_range" => $range));
    }

    public function boxplot_chart(){
        $month_start = $this->input->post('month_start');
		$year_start = $this->input->post('year_start');
		$month_end = $this->input->post('month_end');
        $year_end = $this->input->post('year_end');
        
        $month_start = str_pad($month_start, 2, '0', STR_PAD_LEFT);
        $month_end = str_pad($month_end, 2, '0', STR_PAD_LEFT);

        $date_start = date('d/m/Y', strtotime("01-{$month_start}-{$year_start}"));
        $date_end = date('t/m/Y', strtotime("01-{$month_end}-{$year_end}"));

        $company = $this->input->post('companys');
        $component = $this->input->post('components');
        $product = $this->input->post('products');

        $get_company = $this->m_pairing_trend->get_company($company);
        $get_component = $this->m_pairing_trend->get_component($component);
        $get_product = $this->m_pairing_trend->get_product($product);

        $data_pairing = $this->m_pairing_trend->get_boxplot_pairing($company, $product, $component, $date_start, $date_end);
        if($data_pairing){
            $dt_range = array();
            foreach($data_pairing as $range){
                array_push($dt_range, $range['PERIODE']);
            }
            $dt_range = array_values(array_unique($dt_range));

            foreach($get_company as $dt_company){
                $i = 0;
                $data["title"] = $dt_company['NM_COMPANY']." : ".$get_product[0]['NM_PRODUCT'];
                $data["company"] = $dt_company['NM_COMPANY'];
                $data["value"] = array();

                foreach($get_component as $dt_component){
                    $value["name"] = $dt_component['KD_COMPONENT'];
                    $value["type"] = "box";
                    $value["marker"] = array("color" => "rgba(".getFixColor($i++).", 0.1)");
                    
                    $value["y"] = array();
                    $value["x"] = array();

                    $value["mode"] = "lines+text";
                    $value["text"] = array();
                    $value["textposition"] = "top";
                    
                    $minmax = $this->c_range_qaf->configuration_component($dt_company['ID_COMPANY'], '81', $product[0], $dt_component['ID_COMPONENT']);

                    foreach($dt_range as $r){
                        foreach($data_pairing as $dt_pair){
                            if($r == $dt_pair['PERIODE'] && $dt_component['ID_COMPONENT'] == $dt_pair['ID_COMPONENT'] && $dt_company['ID_COMPANY'] == $dt_pair['ID_COMPANY']){
                                $y_val = $dt_pair['NILAI'];
                                $x_val = $dt_pair['PERIODE'];
                            }
                        }
                        array_push($value["y"], $y_val);
                        array_push($value["x"], $x_val);
                    }
                    array_push($data["value"], $value);

                    // MIN
                    $min["name"] = "MIN ".trim($dt_component['KD_COMPONENT']);
                    $min["mode"] = "lines";
                    $min["type"] = "lines";
                    $min["marker"] = array("color" => "rgba(".getFixColor($i++).", 0.3)", "dash" => "dashdot");
                    $min["line"] = array("dash" => "dashdot", "width" => 1, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                    $min["x"] = $dt_range;

                    $y_min_val = array();
                    foreach($value['y'] as $y_val){
                        array_push($y_min_val, min(explode(",",$y_val)));
                    }

                    $data_min = array();
                    foreach($y_min_val as $y_min){
                        if($minmax['V_MIN'] != null || $minmax != 0){
                            array_push($data_min, $minmax['V_MIN']);
                        } else {
                            array_push($data_min, min($y_min_val));
                        }
                        
                    }

                    $min["y"] = $data_min;
                    $min["mode"] = "lines+text";
                    $min["text"] = array();
                    $min["textposition"] = "top";
                    $min_val = null;

                    array_push($data["value"], $min);

                    // MAX
                    $max["name"] = "MAX ".trim($dt_component['KD_COMPONENT']);
                    $max["mode"] = "lines";
                    $max["type"] = "lines";
                    $max["marker"] = array("color" => "rgba(".getFixColor($i++).", 0.3)");
                    $max["line"] = array("dash" => "dashdot", "width" => 1, "color" => "rgba(".getFixColor($i + 4).", 1.0)");
                    $max["x"] = $dt_range;

                    $y_max_val = array();
                    foreach($value['y'] as $y_val){
                        array_push($y_max_val, max(explode(",",$y_val)));
                    }

                    $data_max = array();
                    foreach($y_max_val as $y_max){
                        array_push($data_max, max($y_max_val));
                    }

                    $max["y"] = $data_max;
                    $max["mode"] = "lines+text";
                    $max["text"] = array();
                    $max["textposition"] = "top";
                    $max_val = null;

                    if($minmax['V_MAX'] != '9999'){
                        array_push($data["value"], $max);
                    }
                   
                }

                $json[] = $data;
            }

            // ARRAY FORMATING PLOTLY
            $y_range = array();
            foreach($json as $js){
                $dt["title"] = $js['title'];
                $dt['value'] = array();
                foreach($js['value'] as $js_v){
                    $dt_v['name'] = trim($js_v['name']);
                    $dt_v['type'] = $js_v['type'];
                    $dt_v['marker'] = $js_v['marker'];
                    $dt_y = "";
                    $dt_x = "";

                    $j = 0;
                    foreach($js_v['y'] as $y){
                        $y_val = explode(",", $y);
                        
                        foreach($y_val as $y_key){
                            $dt_x .= $js_v['x'][$j].",";
                        }
                        $j++;

                        $dt_y .= $y.',';
                    }

                    $dt_v['y'] = explode(",", substr($dt_y, 0, -1));
                    array_push($y_range, max(explode(",", substr($dt_y, 0, -1))));
                    array_push($y_range, min(explode(",", substr($dt_y, 0, -1))));

                    $dt_v['x'] =  explode(",", substr($dt_x, 0, -1));
                    array_push($dt['value'], $dt_v);
                }

                $json2[] = $dt;
            }

            $min = min($y_range);
            $max = max($y_range) + 30;

            if($min > 0){
                $min = $min - 30;
            }
            $range = array($min, $max);
            $status = "200";
        } else {
            $json2 = array();
            $range = array();
            $status = "404";
        }

        to_json(array("status" => $status, "data" => $json2, "y_range" => $range));

    }
}