<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_proficiency extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_report_proficiency","report");
		$this->load->model("M_uji_proficiency_setup","setup");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$this->list_data = $this->report->get_list();
		//print_r($this->db->last_query());
		//exit();
		$this->template->adminlte("v_report_proficiency");
    }
	
	public function do_upload(){
		$this->load->library('form_validation');
		$this->load->helper('url');
		
		$slug = $this->input->post('slug');
		$id = $this->input->post('id_pro');

		$folder = "./assets/uploads/laporan_proficiency/";
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = $slug.".".$ext;

		$this->report->insert_laporan($id, $path);
		$url = "report_proficiency";
		redirect(site_url($url));
	}
	
	
}

?>