   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penerimaan Sample Blind Test
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="row">
        <div class="col-xs-12">    
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Pelaksanaan Blind Test</h3>
                <!-- <div class="input-group input-group-sm" style="width: 150px; float: right;">
                  <a id="AddNew" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a>
                </div> -->
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">No. </th>
                        <th width="*">PELAKSANAAN</th>
                        <th width="*">PLANT</th>
                        <th width="*">TAHUN</th>
                        <th width="*">START DATE</th>
                        <th width="*">END DATE</th>
                        <!-- <th width="*">PIC OBSERVASI</th> -->
                        <th width="*">PIC PLANT</th>
                        <!-- <th width="*">TANGGAL KIRIM</th> -->
                        <th width="*">TANGGAL TERIMA</th>
                        <th width="5%">ACTION</th>
                      </tr>
                    </thead>
                  </table>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">    
    
          <div class="box" id="entryBlindTest" style="display: none;">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Entry Blind test</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="CancelEntry" type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                </div>
                </div>
              </div>

              <div class="box-body">

                <div class="row col-sm-6">
                  <table class="table table-bordered table-striped table-responsive" style="width: 100%;">
                    <thead>
                      <th colspan="3">INFORMASI BLIND TEST<span id="id_setup" style="display: none;"></span><span id="id_periode" style="display: none;"></span></th>
                    </thead>
                    <tbody style="font-size: x-small;">
                      <tr>
                        <td width="25%">PELAKSANAAN</td>
                        <td width="2%"> : </td>
                        <td width="*"><span id="pelaksanaan"></span></td>
                      </tr>
                      <tr>
                        <td>PLANT</td>
                        <td> : </td>
                        <td><span id="plant"></span></td>
                      </tr>
                      <tr>
                        <td>TAHUN</td>
                        <td> : </td>
                        <td><span id="tahun"></span></td>
                      </tr>
                      <tr>
                        <td>START DATE</td>
                        <td> : </td>
                        <td><span id="start_date"></span></td>
                      </tr>
                      <tr>
                        <td>END DATE</td>
                        <td> : </td>
                        <td><span id="end_date"></span></td>
                      </tr>
                      <tr>
                        <td>PIC OBSERVASI</td>
                        <td> : </td>
                        <td><span id="pic_observasi"></span></td>
                      </tr>
                      <tr>
                        <td>PIC PLANT</td>
                        <td> : </td>
                        <td><span id="pic_plant"></span></td>
                      </tr>
                      <tr>
                        <td>TANGGAL KIRIM</td>
                        <td> : </td>
                        <td><span id="tanggal_kirim"></span></td>
                      </tr>
                      <tr>
                        <td>TANGGAL TERIMA</td>
                        <td> : </td>
                        <td><span id="tanggal_terima"></span></td>
                      </tr>
                    </tbody>
                    <!-- <tfoot>
                      <td colspan="3">ENTRY DATA DIBAWAH INI <i class="fa fa-hand-o-down"></i></td>
                    </tfoot> -->
                  </table>
                </div>

                <div class="row col-sm-6">


                  <div class="box-body" id="forminputblind" style="">
            <div class="box box-warning box-solid col-sm-12 col-md-12 col-lg-12">
              <div class="box-header with-border">
                <h3 class="box-title"> ENTRY DATA DIBAWAH INI <i class="fa fa-hand-o-down"></i> </h3>
                <div class="box-tools pull-right">
                  <!-- <a id="SubmitBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a>  -->

                  <a id="SubmitBlind" onclick="submitsample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a> 
                            <!-- <a id="ReloadDataBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-refresh"></i></a> --> 
                </div>
              </div>
              <div class="box-body" style="">

                        <div id="example1" class="hot handsontable htColumnHeaders" style="margin-left: 15px; margin-right: : 15px; max-height: 350px !important;"></div>
                        <div class="form-group row" style="margin-left: 15px; margin-right: : 15px;">
                          <div class="col-sm-2" style="">
                            <label for="addNewRow"></label>
                            <!-- <button class="btn btn-block btn-success btn-flat addNewRow" style="display: none;" name="addNewRow" id="addNewRow"><i class="fa fa-plus"></i> Row</button> -->
                            <a  style="display: none;" name="addNewRow" id="addNewRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-success btn-sm addNewRow"><i class="fa fa-plus"></i> Row</a>
                          </div>
                          <div class="col-sm-2" style="">
                            <label for="delLastRow"></label>
                            <!-- <button class="btn  btn-danger btn-flat delLastRow" style="display: none;" name="delNewRow" id="delLastRow"><i class="fa fa-trash"></i> Row</button> -->
                            <a style="display: none;" name="delNewRow" id="delLastRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-danger btn-sm delLastRow"><i class="fa fa-trash"></i> Row</a>
                          </div>

                          <div class="col-sm-2" style="">
                            <!-- <button class="btn  btn-primary btn-flat" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" name="savesample" id="savesample"><i class="fa fa-save"></i> &nbsp;Save</button> -->
                            <a  name="savesample" id="savesample" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-primary btn-sm"><i class="fa fa-save"></i> &nbsp;Save Draft Blind Test</a>
                          </div>
                        </div>

              </div>
            </div>
          </div>

					<div class="box-body" style="margin-left: 5px; display: none;">
						<div class="box box-danger box-solid col-sm-12 col-md-12 col-lg-12">
							<div class="box-header with-border">
								<h3 class="box-title"> Blind Test Record Preview </h3>
								<div class="box-tools pull-right">
									<a id="SubmitBlind_" onclick="submitsample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a> 
                  					<a id="ReloadDataBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-refresh"></i></a>
								</div>
							</div>
							<div class="box-body" style="">

			                  <table  id="blind_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
			                    <thead>
			                      <tr>
			                        <!-- <th width="5%">No. </th> -->.
			                        <th width="50%">KOMPONEN</th>
			                        <th width="*">TESTING 1</th>
			                        <th width="*">TESTING 2</th>
			                        <th width="5%">ACTION</th>
			                      </tr>
			                    </thead>
			                  </table>

							</div>
						</div>
					</div>
                </div>

                
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script type="text/javascript">
  
  function receiptsample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menerima sample sekarang ?");
    if (r == true) {
      // console.log("testing");
      // $("#ReloadData").click();
      $.ajax({
        url: "<?php echo site_url("acclab_ujisampleblindtest/receipt_blindtest");?>",
        data: {"FK_ID_PERIODE": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }
  
  function deletesample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menghapus data ini sekarang ?");
    if (r == true) {
      // console.log("testing");
      // $("#ReloadData").click();
      $.ajax({
        url: "<?php echo site_url("acclab_ujisampleblindtest/delete_blindtest");?>",
        data: {"ID": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadDataBlind").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }
  
  function submitsample() {
    // body...
    var r = confirm("Apakah anda yakin untuk submit data ini sekarang ?");
    if (r == true) {
      // console.log("testing");
      // $("#ReloadData").click();
      $.ajax({
        url: "<?php echo site_url("acclab_ujisampleblindtest/submit_blindtest");?>",
        data: {"ID": $('#id_setup').text(), "FK_ID_PERIODE": $('#id_periode').text(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data id_periode
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#CancelEntry").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      // console.log(idsetup);
    } else {
    }
  }

$(document).ready(function(){

  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable; 
  var oBlindTable;
  var glbproduk = new Array();
  // var example1 = document.getElementById('example1'), hot1;

  reloadtabel(); 

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });
  $(document).on('click',"#ReloadDataBlind",function () {
  	reloadblindtabel($('#id_setup').text());
  });
  
  // $(document).on('click',".btEntry",function () {
  //   $('#viewArea').hide();
  //   $('#entryBlindTest').show();
  //   loadtabelinput();
  // });
  
  $(document).on('click',"#CancelEntry",function () {
    $('#entryBlindTest').hide();
    $('#viewArea').show();
    $("#ReloadData").click(); 
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    // $('#saving').show();
    // $('#formArea').show();
    // $('#viewArea').hide();  

    // $('#UpdateArea').show();
    // $('#AddArea').hide(); 

    // var data = oTable.row($(this).parents('tr')).data();

    // $('#ID_MATERIAL').val(data['ID_MATERIAL']);
    // $('#KODE_MATERIAL').val(data['KODE_MATERIAL']);
    // $('#NAME_MATERIAL').val(data['NAME_MATERIAL']);
    // $('#H20_PERCENT').val(data['H2O_PERCENTAGE']);
    // $('#COMP_RELATION').val(data['ID_COMPONENT']);
    // $('#COMP_PERCENT').val(data['COMP_PERCENTAGE']);
    // $('#saving').hide();
  });

  function reloadblindtabel(idsetup, action = true) {
    // body...

    oBlindTable = $('#blind_tables').dataTable({    	
        "paging":   false,
		destroy: true,
      processing: true,
    //   serverSide: true,
      select: true,
		"ajax": {
			"url": "<?php echo site_url("acclab_ujisampleblindtest/ajax_get_blindtest");?>/"+idsetup+"/"+action,
			"type": "POST"
		}
	});
    
    // $('#ReloadData').show();
    // $('#AddNew').show();
  }

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    // $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("acclab_ujisampleblindtest/blindtest_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "RNUM", "width": 50},
        {"data": "NAMA"},
        {"data": "NAMA_LAB"},
        {"data": "TAHUN"},
        {"data": "SETUP_START"},
        {"data": "SETUP_END"},
        // {"data": "NAMA_PIC_OBS"},
        {"data": "NAMA_PIC_PLANT"},
        // {"data": "TANGGAL_KIRIM"},
        {"data": "TANGGAL_TERIMA"},
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){
            // return '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';
            // console.log(row);
            // console.log(data);
            // console.log(index);
            // console.log(index['TANGGAL_KIRIM']);
              var btn = '';

            if (index['STATUS_ENTRY'] == 0) {
              <?php if($this->PERM_WRITE): ?>
              btn = '<button title="Terima Sample" class="btTerima btn btn-warning btn-xs" type="button" onclick="receiptsample('+index['ID']+')"><i class="fa fa-download"></i> Terima Blind Test</button>';
              <?PHP endif; ?>
              return btn;
            } else if (index['STATUS_ENTRY'] == 1) {
              <?php if($this->PERM_WRITE): ?>
              btn = '<button title="Entry Blind Test" class="btEntry btn btn-success btn-xs" type="button"><i class="fa fa-edit"></i> Entry Blind Test</button>';
              <?PHP endif; ?>
              return btn;
            } else if (index['STATUS_ENTRY'] == 2) {
              btn = '<button title="Detail Blind Test" class="btDetail btn btn-primary btn-xs" type="button"><i class="fa fa-document"></i> Detail Blind Test</button>';
              return btn;
            } else {
              // return '';
              return btn;
            }

          }
        },
      ]
    });
    
    $('#ReloadData').show();
    // $('#AddNew').show();
  }


// $(document).on('click', ".btDeleteBlind", function() {
//     // body...
//     // $(document).off('focusin.modal');
//     var data = oBlindTable.row($(this).parents('tr')).data();
//     // console.log(data);
//     console.log(data);
// });

$(document).on('click', ".btEntry", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    // console.log(data);
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    $('#SubmitBlind, #savesample').show();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    loadtabelinput(data['ID']);
    // reloadblindtabel(data['ID']);
});

$(document).on('click', ".btDetail", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    // console.log(data);
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    // $('#SubmitBlind').hide();    
    $('#SubmitBlind, #savesample').hide();  

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    // loadtabelinput();
    loadtabelinput(data['ID']);
    // reloadblindtabel(data['ID'], false); 
});

  var example1 = document.getElementById('example1'), hot1;
  var oglbcomponent = new Array();
  var oglbcomponentid = new Array();
  var oglbcomponentname = new Array();

  document.querySelector('.addNewRow').addEventListener('click', function() {
    hot1.alter('insert_row', hot1.countRows());
  });

  document.querySelector('.delLastRow').addEventListener('click', function() {
    hot1.alter('remove_row', hot1.countRows() - 1);
  });

    function getListComponent() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_ujisampleblindtest/get_component");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_COMPONENT']);

          }
        }else{
        }
      });
      return datal;
    }

  function getContentData() {
      return [
      ];
    }

    function getRowOptions() {
      var initial_coloptions = [
        // {
        //   data: 'produk',
        //   type: 'autocomplete',
        //   source: getListComponent(),
        //   strict: true,
        //   allowInvalid: false,
        //   colWidths: 250,
        //   visibleRows: 10
        // },
        {
          type: 'text',
          // data: 'component',
          colWidths: 200,          
          readOnly: true,
          // editor: false
        },
        {
          type: 'numeric',          
          strict: true,
          allowInvalid: false,
          // data: 'vdata',
          // colWidths: 200,
          // editor: 'numeric'
        },
        {
          type: 'numeric',    
          strict: true,
          allowInvalid: false,
          // data: 'vsd',
          // colWidths: 200,
          // editor: 'numeric'
        },
        {
          type: 'text',
          // data: 'satuan',         
          readOnly: true,
          // colWidths: 200,
          // editor: false
        }
      ];
      return initial_coloptions;
    }

    function getColHeader() {
      var columnlist = ['Component', 'Testing 1', 'Testing 2', 'Satuan'];
      return columnlist;
    }

    function getRowOptions__() {
      var initial_coloptions = [
        // {
        //   type: 'date'
        // },
        {
          data: 'produk',
          type: 'autocomplete',
          source: getListComponent(),
          strict: true,
          allowInvalid: false,
          colWidths: 250,
          visibleRows: 10
        },
        {
          type: 'text',
        },
        {
          type: 'text',
        }
      ];

      // var rowcontent = {
      //       type: "numeric"
      // }

      // for (var i = 0; i < oglbcomponent.length; i++) {
      //   initial_coloptions[i+4] = rowcontent;
      // }
      return initial_coloptions;
    }

    function getColHeader__() {
      var columnlist = ['Component', 'Testing Result 1', 'Testing Resul 2'];
      // for (var i = 0; i < oglbcomponent.length; i++) {
      //   columnlist.push(oglbcomponent[i]);
      // }
      return columnlist;
    }

    function getTypeProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_ujisampleblindtest/ajax_get_type_product");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['KD_PRODUCT']);
          }
        }else{
        }
      });
      
      return datal;
    }

    function getTypeProduct(merk) {
      return glbproduk[merk];
    }

    function getListArea() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_KOTA']);
          }
        }else{
        }
      });
      return datal;
    }

  function loadtabelinput(idsetup) {

    if (hot1) {

      hot1.destroy();

    }   

    var htdata = [];

    $.getJSON('<?php echo site_url("acclab_ujisampleblindtest/get_dataformcomponent");?>/'+idsetup, function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          htdata = values;
        }

        $.getJSON('<?php echo site_url("acclab_ujisampleblindtest/ajax_get_component_display");?>', function (result) {
          var values = result;
          if (values != undefined && values.length > 0) {
            for (var i = 0; i < values.length; i++) {
              if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
                oglbcomponent.push(values[i]['KD_COMPONENT']);
                oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
                oglbcomponentname.push(values[i]['KD_COMPONENT']);
              }
            }
            hot1 = new Handsontable(example1, {
              // data: getContentData(),
              data: htdata,
              height: 500,              
              // width: '100%',
              // height: '100%',
              autoColumnSize : true,
              fixedColumnsLeft: 3,
              manualColumnFreeze: true,
              manualColumnResize: true,
              colHeaders: getColHeader(),
              columns: getRowOptions(),
            });
            if (values.length == 0) { hot1.alter('insert_row', hot1.countRows()); }           

            hot1.updateSettings({
              afterChange: function(changes, src) {
                var row = changes[0][0],
                  col = changes[0][1],
                  newVal = changes[0][3];
              }
            })

            // $('#delLastRow').show();
            // $('#addNewRow').show();
            // $('#savesample').show();
          }else{
          }
        });

      });

  }

  function loadtabelinput__() {

    if (hot1) {

      hot1.destroy();

    }   

    $.getJSON('<?php echo site_url("acclab_ujisampleblindtest/ajax_get_component_display");?>', function (result) {
      var values = result;
      if (values != undefined && values.length > 0) {
        for (var i = 0; i < values.length; i++) {
          if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
            oglbcomponent.push(values[i]['KD_COMPONENT']);
            oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
            oglbcomponentname.push(values[i]['KD_COMPONENT']);
          }
        }
        hot1 = new Handsontable(example1, {
          data: getContentData(),
          height: 250,
          autoColumnSize : true,
          fixedColumnsLeft: 3,
          manualColumnFreeze: true,
          manualColumnResize: true,
          colHeaders: getColHeader(),
          columns: getRowOptions(),
        });
        hot1.alter('insert_row', hot1.countRows());

        hot1.updateSettings({
          afterChange: function(changes, src) {
            var row = changes[0][0],
              col = changes[0][1],
              newVal = changes[0][3];

              // if (newVal == 'OPC' || newVal == 'PPC' || newVal == 'PCC') {}


              // if (col == 'produk') {

              //   var cellProperties = hot1.getCellMeta(row, 3);


              //   if (parseInt((glbproduk[newVal]).length) == 1 ) {

              //           hot1.setCellMeta(row, 3, 'type', 'text');
              //           hot1.setDataAtCell(row, 3, glbproduk[newVal][0]);
              //     cellProperties.readOnly = 'true';

              //   } else if (parseInt((glbproduk[newVal]).length) > 1 ) {

              //     hot1.setCellMeta(row, 3, 'type', 'autocomplete');
              //     cellProperties.source = getTypeProduct(newVal);
              //     hot1.render();

              //     var cellPropertiestmp = hot1.getCellMeta(row, 3);

              //   }
              // }
          }
        })

        $('#delLastRow').show();
        $('#addNewRow').show();
        // $('#savesample').show();
      }else{
      }
    });

  }

   $(document).on('click',"#savesample",function () {
      saveBlindTest();
  });


    // $('button[name=savesample]').click(function () {
    //   saveBlindTest();
    // });

    function saveBlindTest() {
        $body = $("body");
        $body.addClass("loading");
        $body.css("cursor", "progress");

      $('#savesample').button('loading');

        // var companyid = $("#oCOMPANYn").val();
        // var companyname = $("#oCOMPANYn option:selected").text();
        var handsonData = hot1.getData();
        $.ajax({
          url: "<?php echo site_url("acclab_ujisampleblindtest/save_sample_data_blindtest");?>",
          data: {"user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData, "id_setup": $('#id_setup').text(), "id_periode": $('#id_periode').text()}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              if (res.msg == 'success') {
                // $("#a-notice-success").click();                
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
                var rowcount = hot1.countRows();
                for (var i = 0; i < rowcount; i++) {
                  hot1.alter('remove_row', hot1.countRows() - 1);
                }               
                hot1.alter('insert_row', hot1.countRows());
              }
              else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              $('#savesample').button('reset');              
              // $('#entryBlindTest').hide();
              // $('#viewArea').show();
              loadtabelinput($('#id_setup').text());

          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();

              $('#savesample').button('reset');
          }
        });

    }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
