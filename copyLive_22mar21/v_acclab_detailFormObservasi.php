
<section class="content-header">
    <h1>
        Form Observasi (Akurasi Labor SIG)
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <form class="form-inline">
                        <?php if($this->PERM_WRITE): ?>
                            <div class="input-group input-group-sm" style="width: 150px; ">
                                <a data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary btn-sm">Create new</a>
                            </div>
                        <?PHP endif; ?>
                    </form>
                    <hr/>
                </div>

                <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover dt-responsive nowrap "
                            cellspacing="0"
                            width="100%">
                        <thead>
                        <tr>
                            <th width="1">No.</th>
                            <th >Nama Field</th>
                            <th ></th>
                        </tr>
                        </thead>
                        <tbody style="font-weight: normal;">
                        <?php
                        $count = 1;
                        foreach ($this->list_data as $dt) { ?>
                            <tr>
                                <td><?= $count++; ?></td>
                                <td><?= $dt->NAMA_FIELD;?></td>
                                <!-- <td>
					</td> -->
                                <td>
                                    <a data-toggle="modal" data-target="#editModal<?php echo $dt->ID; ?>"><button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-edit "></i> Edit</button></a>
                                    <a href="<?php echo site_url("acclab_masterFormObservasi/deleteField/{$dt->ID}_{$this->key}") ?>" onClick="return doconfirm();"><button title="Delete" class="btEdit btn btn-danger btn-xs" type="button"><i class="fa fa-trash "></i> Delete</button></a>
                                </td>
                            </tr>
                            <div id="editModal<?php echo $dt->ID; ?>" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <form role="form" method="POST" action="<?php echo site_url("acclab_masterFormObservasi/updateField/") ?>" >
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b>Tambah Field Form:</b> </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group c-group after-add-more" id="utama">
                                                    <div class="col-sm-12">
                                                        <label>Nama Field </label>
                                                        <input type="text" value="<?php echo $dt->NAMA_FIELD; ?>" class="form-control" name="nama_field" placeholder="Nama Field"  >
                                                        <input type="hidden" value="<?php echo $dt->ID; ?>" class="form-control" name="id" placeholder="Nama Field"  >
                                                        <input type="hidden" value="<?php echo $this->key; ?>" class="form-control" name="fk" placeholder="Nama Field"  >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="margin-top: 2em;">
                                                <button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <form role="form" method="POST" action="<?php echo site_url("acclab_masterFormObservasi/addField/") ?>" >
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Tambah Periode Akurasi Labor:</b> </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group c-group after-add-more" id="utama">
                        <div class="col-sm-12">
                            <label>Nama Field </label>
                            <input type="text" class="form-control" name="nama_field" placeholder="Nama Field"  >
                            <input type="hidden" value="<?php echo $this->key; ?>" class="form-control" name="fk" placeholder="Nama Field"  >
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 2em;">
                    <button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
    <a  id="a-notice-error"
        class="notice-error"
        style="display:none";
        href="#"
        data-title="Something Error"
        data-text="<?php echo $notice->error; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->error; ?>');
    </script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
    <a  id="a-notice-success"
        class="notice-success"
        style="display:none";
        href="#"
        data-title="Done!"
        data-text="<?php echo $notice->success; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->success; ?>');
    </script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
    .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
    $(document).ready(function(){

        /** DataTables Init **/
        var table = $("#dt_tables").DataTable();
    });
</script>
<script>
    function doconfirm(){
        job=confirm("Are you sure you want to delete data?");
        if(job!=true){
            return false;
        }
    }
</script>