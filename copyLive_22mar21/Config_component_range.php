<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_component_range extends QMUser {

	public $list_company = array();
	public $list_component = array();

	public $id_company;
	public $id_plant;
	public $id_grouparea;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("join_grouparea", "j_garea");
		$this->load->model("c_component_range", "cc_range");
	}
	
	public function index(){
		$this->ID_COMPANY = $this->session->flashdata('company');
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_c_component_range");
	}

	public function get_grouparea($id_plant=NULL){
		$output = $this->j_garea->get_grouparea_list($id_plant);
		to_json($output);
	}

	public function get_parameter($id_grouparea=NULL,$DISPLAY='D'){
		$output = $this->cc_range->get_range_component($id_grouparea,$DISPLAY);
		to_json($output);
	}

	public function get_join_grouparea(){
		$list = $this->cc_range->get_list(0);
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->ID_GROUPAREA;
			$row[] = $no;
			$row[] = $field->NM_COMPANY;
			$row[] = $field->NM_PLANT;
			$row[] = $field->NM_GROUPAREA;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->cc_range->count_all(0),
            "recordsFiltered" => $this->cc_range->count_filtered(0),
            "data" => $data,
        );

		to_json($output);
	}

	public function get_c_parameter(){
		$list = $this->cc_range->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->ID_COMPONENT;
			$row[] = $no;
			$row[] = $field->KD_COMPONENT;
			$row[] = $field->NM_COMPONENT;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->cc_range->count_all(),
            "recordsFiltered" => $this->cc_range->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function edit(){
		$this->id_plant 	= $this->input->post('plant');
		$this->id_grouparea = $this->input->post('grouparea');
		$this->id_company 	= $this->m_plant->get_data_by_id($this->id_plant)->ID_COMPANY;

		$this->list_company   = $this->m_company->datalist();
		$this->list_component = $this->cc_range->get_list();
		
		$this->template->adminlte("v_c_component_range_edit");
	}

	public function create(){
		$id_param  	= $this->input->post('id_param');
		$plant 		= $this->input->post('plant');
		$grouparea 	= $this->input->post('grouparea');
		$display 	= $this->input->post('display');
		$company 	= $this->m_plant->get_data_by_id($plant)->ID_COMPANY;
		
		for ($i=0 ; $i < count($id_param); $i++ ) { 
			$min_param = (int) $this->input->post("min_param_$id_param[$i]");
			$max_param = (int) $this->input->post("max_param_$id_param[$i]");

			$data['ID_PARAMETER'] = $id_param[$i];
			$data['V_MIN'] = $min_param;
			$data['V_MAX'] = $max_param;

			$this->cc_range->clean($data['ID_PARAMETER']);
			$this->cc_range->insert($data);
		}

		$this->notice->success("Configuration Saved");
		$this->session->set_flashdata('company', $company);
		$this->session->set_flashdata('plant', $plant);
		$this->session->set_flashdata('display', $display);
		redirect("config_component_range");
	}
}

/* End of file Config_component_range.php */
/* Location: ./application/controllers/Config_component_range.php */
?>
