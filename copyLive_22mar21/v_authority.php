   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Authority Configuration
      </h1>

    </section> 

    <!-- Main content -->
    <section class="content">

       <div class="row">
        <div class="col-xs-12">
		
		
          <div class="box">
		   <table><tr><td>
           <div class="box-header">
			  <form class="form-inline"> 
				<div class="form-group">
				  <select id="ID_USERGROUP" class="form-control select2">
					<?php  foreach($this->list_usergroup as $usergroup): ?>
					  <option value="<?php echo $usergroup->ID_USERGROUP;?>" ><?php echo $usergroup->NM_USERGROUP;?></option>
					<?php endforeach; ?> 
				  </select>
				</div>
				<div class="form-group">
				  <select id="ID_GROUPMENU" class="form-control select2">
					  <OPTION VALUE="">ALL MENU</OPTION>
					<?php  foreach($this->list_groupmenu as $groupmenu): ?>
					  <option value="<?php echo $groupmenu->ID_GROUPMENU;?>" ><?php echo $groupmenu->NM_GROUPMENU;?></option>
					<?php endforeach; ?>
				  </select>
				</div>
				<!-- <div class="form-group">
					<select class="select2 form-control" name="ID_SUBMENU" id="ID_SUBMENU">
						<option value="">Choose Submenu...</option>
					</select>
				</div> --> 
			  </form>
			</div>
			</td></tr> 
            <!-- /.box-header -->
            <tr><td>
            <div class="box-body table-responsive no-padding">
              <table class="table-fixed table-hover">
                <tr>
                  <th>No</th>
                  <th>Menu Name</th>
                  <th align=center style="text-align: center">Authorized</th>
                  <th align=center style="text-align: center">Read</th>
                  <th align=center style="text-align: center">Write</th>
                </tr>
								<?php 
								// echo '<pre>';
								// print_r($this->list_menu);
								// echo '</pre>';
								
								$x=1; foreach($this->list_menu as $menu): ?>
                <tr class="trcauth authtr-<?php echo $menu->ID_MENU ?>" id_menu="<?php echo $menu->ID_MENU ?>"  id_groupmenu="<?php echo $menu->ID_GROUPMENU ?>"  >
                  <td class="tdauth-<?php echo $menu->ID_MENU ?>"><?php echo $x++; ?></td>
                  <td><?php echo '<span class="label label-danger">'.$menu->NM_GROUPMENU.'</span>' .' <i class="fa fa fa-long-arrow-right"></i> '. '<span class="label label-warning">'.$menu->NM_PARENT.'</span>'. ($menu->NM_PARENT ? ' <i class="fa fa fa-long-arrow-right"></i> ' : '') . '<span class="label label-success">'.$menu->NM_MENU.'</span>' ?></td>
                  <td align=center><input type="checkbox" name="auth[<?php echo $menu->ID_MENU ?>]" value="<?php echo $menu->ID_MENU ?>" class="cauth" <?php echo (!$this->PERM_WRITE)?"READONLY DISABLED":""; ?> /></td>
                  <td align=center><input type="checkbox" name="read[<?php echo $menu->ID_MENU ?>]" value="<?php echo $menu->ID_MENU ?>" id="cread_<?=$menu->ID_MENU?>" class="cread" DISABLED <?php echo (!$this->PERM_WRITE)?"READONLY DISABLED":""; ?> /></td>
                  <td align=center><input type="checkbox" name="write[<?php echo $menu->ID_MENU ?>]" value="<?php echo $menu->ID_MENU ?>" id="cwrite_<?=$menu->ID_MENU?>" class="cwrite" DISABLED <?php echo (!$this->PERM_WRITE)?"READONLY DISABLED":""; ?> /></td>
                </tr>
                <?php endforeach; ?>
              </table>
            </div>
             </td></tr></table>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
	
<script language="javascript" type="text/javascript" src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>

$(document).ready(function(){
	$('.select2').select2();

 $('#ID_GROUPMENU').on('change', function(){
		id_groupmenu = $(this).val();

		if(id_groupmenu){
			$('#ID_SUBMENU').html('<option value="">Loading...</option>');
			$.ajax({
				url : '<?=base_url('authority/get_detlist/')?>'+id_groupmenu,
				type: 'GET',
			}).done(function(data){
				$('#ID_SUBMENU').html('<option value="">Choose Submenu...</option>');
				$.each(data['data'], function(i, v) {
					$('#ID_SUBMENU').append('<option value="'+v[0]+'">'+v[3]+'</option>');
				});
			});
		}else{
			$('#ID_SUBMENU').html('<option value="">Choose Submenu...</option>');
		}
	});

	$(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel", 
		confirmButtonClass: "btn-danger"
	});
	<?php if($this->PERM_WRITE): ?>
	$(".cauth").click(function(){
		if(this.checked){
			$(this).closest('td').next('td').html('<input type="checkbox" name="read['+this.value+']" id="cread_'+this.value+'"  value="'+this.value+'" class="cread" checked="checked" <?php echo (!$this->PERM_WRITE)?"READONLY DISABLED":""; ?> disabled>');
			// $(this).closest('td').next('td').next('td').find('.cread').attr({'checked': true, 'disabled': true});
			$(this).closest('td').next('td').next('td').find('.cwrite').attr({'checked': false, 'disabled': false});
		}else{
			$(this).closest('td').next('td').html('<input type="checkbox" name="read['+this.value+']" id="cread_'+this.value+'" value="'+this.value+'" class="cread" <?php echo (!$this->PERM_WRITE)?"READONLY DISABLED":""; ?> disabled>');
			// $(this).closest('td').next('td').next('td').find('.cread').attr({'checked': false, 'disabled': true});
			$(this).closest('td').next('td').next('td').find('.cwrite').attr({'disabled': true, 'checked': false});
		}
		//auth--------
		$.get("<?php echo site_url("authority"); ?>/"+((this.checked)?"auth":"deauth")+"/"+$("#ID_USERGROUP").val()+"/"+this.value);
	});

	$(".cwrite").click(function(){
		if(this.checked){
			$.get("<?php echo site_url("authority"); ?>/write/"+$("#ID_USERGROUP").val()+"/"+this.value);
		}else{
			$.get("<?php echo site_url("authority"); ?>/dewrite/"+$("#ID_USERGROUP").val()+"/"+this.value);
		}
	});
	<?php endif; ?>
	$("#ID_USERGROUP").change(function(){
		$('.cread').prop({"checked": false, "disabled": true});
		$('.cwrite').prop({"checked": false, "disabled": true});
		var url = "<?php echo site_url("authority/authlist/") ?>"+this.value;
		$.getJSON(url,function(data){
			var authorized = [];
			console.log(data);
			data.forEach(function(r){
				authorized.push(r.ID_MENU);
				if(r.READ == 1){
					$('#cread_'+r.ID_MENU).prop("checked", true);
					$('#cwrite_'+r.ID_MENU).prop("disabled", false);
				}
				if(r.WRITE == 1){
					$('#cwrite_'+r.ID_MENU).prop("checked", true);
					$('#cwrite_'+r.ID_MENU).prop("disabled", false);
				}
			});
			
			var auth = $(".cauth");
			for(i=0; i < auth.length; i++){
				$(auth[i]).prop("checked",(($.inArray(auth[i].value,authorized)>-1)?true:false));
			}
		});
	});
	
	$("#ID_USERGROUP").change();
	
	
	$("#ID_GROUPMENU").change(function(){
		var trmenu = $(".trcauth");
		var id_groupmenu = this.value;
		var tdnumber = 1;	
		for(i=0; i < trmenu.length; i++){
			var display = ($(trmenu[i]).attr("id_groupmenu") == id_groupmenu)?"":"none";
			if(id_groupmenu == "") display = "";
			$(trmenu[i]).css("display",display);
			if(display == ""){
				var tdauth = ".tdauth-"+$(trmenu[i]).attr("id_menu");
				$(tdauth).html(tdnumber);
				tdnumber++;
			}
		}
	});

});
</script>
