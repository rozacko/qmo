<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Home {

	public $list_company = array();
	    
	public function index(){
        // echo 123;
        // exit;
		if($this->USER){
			$this->session->unset_userdata("USER");
			$this->session->unset_userdata("MENU");
			$this->USER = NULL;
		}
		$this->template->login("login");
	}

	function list_active(){
		// $this->load->model("m_menu");
		// $this->m_menu->list_active(758);
	}

	public function verification($redirect=NULL){
		$statusActive = 'Login invalid';
		if($this->input->post("USERNAME") && $this->input->post("PASSWORD")){
		// 				$pwPost = $this->input->post('PASSWORD');
		// 				$pwPost = md5(md5($pwPost));
		// 				echo md5(md5($this->input->post('PASSWORD')));
		// exit;
				
			$this->load->library("password");
			$this->load->model("m_user");
			$user = $this->m_user->data(array('USERNAME'=>strtolower($this->input->post("USERNAME")))); #die($this->m_user->get_sql());

			if($user){	
				if($user->ISACTIVE == 'N' || $user->DELETED == '1'){
					$statusActive = 'Account not active!';
					unset($user);
					$user = NULL;
				}else{
					if($user->LDAP == 'Y'){
						if(!$this->login_ldap($this->input->post("USERNAME"),$this->input->post("PASSWORD"))){
							if(!$this->login_ldap($this->input->post("USERNAME")."@smig.corp",$this->input->post("PASSWORD"))){
								unset($user);
								$user = NULL;
							}
						}			 	
					}else{
						// echo $this->input->post('PASSWORD');
						$pwPost = $this->input->post('PASSWORD');
						$pwPost = md5(md5($pwPost));
						// echo md5(md5($this->input->post('PASSWORD')));
						$pwDb	= $user->PASSWORD;
						if($pwPost == $pwDb){
						}else{
							unset($user);
							$user = NULL;
						}
					}		
				}		
			}		

			// var_dump($user);
			// exit;
	
			if($user){
				$this->session->set_userdata("USER",$user);
			}
		}
		
		if(!$user){
			$redirect = "login";
			$this->notice->error($statusActive);
		}
		
		redirect($redirect);
	}

	public function set_log_user($ID_USER){
		$this->load->model("log_users");
		$this->log_users->set($ID_USER);
	}
	
	function login_ldap($username,$password){
		$ldap['user'] = $username; 
		$ldap['pass'] = $password;				
		$ldap['host'] = '10.15.3.120';
		$ldap['port'] = 389;
		$ldap['conn'] = @ldap_connect($ldap['host'], $ldap['port']);
		
		if(!$ldap['conn']){
			return false;
		}
		else{
			#var_dump($ldap);
			@ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
			@$ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
			
			#var_dump($ldap); 
			if(!$ldap['bind']){
				return false;
			}
			@ldap_close($ldap['conn']);
			return true;
		}
	}


	//REGISTER----------------------------------------
	public function register(){
		$this->load->model("m_company");

		if($this->USER){
			$this->session->unset_userdata("USER");
			$this->session->unset_userdata("MENU");
			$this->USER = NULL;
		}

		$this->list_company = $this->m_company->list_company();

		$this->template->login("register");

	}

	public function getPlant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$list_plant = array();
		if($ID_COMPANY){
			$list_plant = $this->m_plant->datalist($ID_COMPANY);
		}
		$list_plant[] = array('ID_PLANT' => '', 'NM_PLANT' => 'Choose Plant...');
		sort($list_plant);
		echo json_encode($list_plant);
	}
	public function getArea($ID_PLANT=NULL){
		$this->load->model("m_area");
		$list_area = array();
		if($ID_PLANT){
			$list_area = $this->m_area->list_area($ID_PLANT);
		}
		$list_area[] = array('ID_AREA' => '', 'NM_AREA' => 'Choose Area...');
		sort($list_area);
		echo json_encode($list_area);
	}


	public function get_username(){
		$this->load->model("employee");

		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		echo json_encode($get);
	}

	public function registerProcess(){
		// error_reporting(1);
		$this->load->model("employee");
		$this->load->model("m_user");
		$param = $this->input->post();

		// print_r($param);
		// exit;


		if(!$param['FULLNAME']){
			$this->response(array('status' => 'error', 'message' => 'Full Name cannot be empty!'));
			exit;
		}
		if(!$param['USERNAME']){
			$this->response(array('status' => 'error', 'message' => 'Username cannot be empty!'));
			exit;
		}
		if(!$param['EMAIL']){
			$this->response(array('status' => 'error', 'message' => 'Email cannot be empty!'));
			exit;
		}
		if(!$param['ID_COMPANY']){
			$this->response(array('status' => 'error', 'message' => 'Company cannot be empty!'));
			exit;
		}

		$param['ISACTIVE'] = 'N';
		$key = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$encript = str_replace(array('$','/'), array('',''), password_hash((md5($param['USERNAME'].$param['EMAIL'].strtotime(date('Ymd')))), PASSWORD_DEFAULT));
		$param['VERCODE'] = $encript;


		$uname = array('USERNAME'=> trim(strtolower($param['USERNAME'])));
		$cek = $this->m_user->data($uname);
		// echo $this->db->last_query();
		if($cek){
			$this->response(array('status' => 'error', 'message' => 'Username already exists!'));
		}
		else{
			$mail_cc = $this->employee->chekKaryawans(trim(strtolower('HERI.PURNOMO')))->mk_email;
			$check = $this->employee->chekKaryawans(trim(strtolower($param['USERNAME'])));

			$nopeg = '';
			$ukname = '';
			if($check){
				$nopeg = $check->mk_nopeg;
				$ukname = $check->muk_nama;
			}
			$param['NOPEG'] = $nopeg;
			$compname = $param['NM_COMPANY'];
			unset($param['NM_COMPANY']);

			$insertId = $this->m_user->insertGetId($param); #die($this->m_user->get_sql());
			$id_user = $insertId;

			$to = array();
			$cc = array();
			
			if($this->serverHost() == 'DEV'){
				#DEV-------------------------------------------------- START
				$to[] = 'm.r.sucahyo2@gmail.com';
				$cc = array('indra.nofiandi@semenindonesia.com', '95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');
				#DEV-------------------------------------------------- END
			}else{
				#PROD-------------------------------------------------- START
				$to = $this->m_user->getAdminEmail($param['ID_COMPANY']);
				$cc[] = $mail_cc;
				#PROD-------------------------------------------------- END
			}

			if($insertId){
				$this->load->library('email');
				$this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');

				$this->email->to($to);
				$this->email->cc($cc);
				
				// $this->email->cc('Email Pak Heri');
				$this->email->subject('QMO Approval');
				
				$url = base_url('requester/approve/'.$id_user.'/'.$encript);
				$dataMail = array(
					'url' => $url,
					'name' => $param['FULLNAME'], 
					'nopeg' => $nopeg, 
					'email' => $param['EMAIL'], 
					'ukname' => $ukname, 
					'compname' => $compname, 
					'otorisasi' => '-'
				);
				$this->email->message($this->templateMail($dataMail));

				if($this->email->send()){
					$this->response(array('status' => 'success', 'message' => 'Account request was successfully sent, confirmation will be sent by email'));
				}else{
					$this->response(array('status' => 'error', 'message' => $this->m_user->error()));
				}
			}
			else{
				$this->response(array('status' => 'error', 'message' => $this->m_user->error()));
			}
		}
	}

	function templateMail($data = array('url' => '-', 'name' => '-', 'nopeg' => '-', 'email' => '-', 'ukname' => '-', 'compname' => '-', 'otorisasi' => '-')){
		$isiemail = "";

		$alert			= "font-size: 16px; color: #fff; font-weight: 500; padding: 20px; text-align: center; border-radius: 3px 3px 0 0;";
		$alert_good		= "background: #1ab394;";
		$body_wrap 		= "background-color: #f6f6f6; width: 100%;";
		$container 		= "display: block !important; max-width: 600px !important; margin: 0 auto !important; /* makes it centered */ clear: both !important;";
		$content 		= "max-width: 600px; margin: 0 auto; display: block; padding: 20px;";
		$content_wrap	= "padding: 20px;";
		$btn_primary 	= "text-decoration: none; color: #FFF; background-color: #1ab394; border: solid #1ab394; border-width: 5px 10px; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize;";
		$content_block	= "padding: 0 0 20px;";
		$main 			= "background: #fff; border: 1px solid #e9e9e9; border-radius: 3px;";
		$footer			= "width: 100%; clear: both; color: #999; padding: 20px;";
		
		$isiemail .= '
			<table style="'.$body_wrap.'">
				<tr>
					<td></td>
					<td style="'.$container.'" width="600">
						<div style="'.$content.'">
							<table style="'.$main.'" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="'.$alert . $alert_good.'">
										Notif: Request for creating a QM account.
									</td>
								</tr>
								<tr>
									<td style="'.$content_wrap.'">
										<table>
											<tr>
												<td colspan="3" class="'.$content_block.'">
													The following is information about people who want to have an account on QM.
												</td>
											</tr>
											<tr>
												<td width="100">Nama</td>
												<td>:</td>
												<td width="300">'.$data["name"].'</td>
											</tr>
											<tr>
												<td>Nopeg</td>
												<td>:</td>
												<td>'.$data["nopeg"].'</td>
											</tr>
											<tr>
												<td>Email</td>
												<td>:</td>
												<td>'.$data["email"].'</td>
											</tr>
											<tr>
												<td>Unit Kerja</td>
												<td>:</td>
												<td>'.$data["ukname"].'</td>
											</tr>
											<tr>
												<td>Perusahaan</td>
												<td>:</td>
												<td>'.$data["compname"].'</td>
											</tr>
											<tr>
												<td>Otorisasi</td>
												<td>:</td>
												<td>'.$data["otorisasi"].'</td>
											</tr>
										</table>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td style="'.$content_block.'"><br>
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'">
													Please do something about this request, and give authorization if approved.
													<br>
													The following button will direct you to the system for approval
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'" align="center">
													<a href="'.$data["url"].'" style="'.$btn_primary.'">Do Something.</a>
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'">
													Thank you for your time.
												</td>
											</tr>
										</table>
										
									</td>
								</tr>
							</table>
							<div style="'.$footer.'">
								<table width="100%">
									<tr>
										<td class="'.$content_block.'"><a href="http://qmo.semenindonesia.com">QMO</a> &copy; Quality Management Online.</td>
									</tr>
								</table>
							</div></div>
					</td>
					<td></td>
				</tr>
			</table>
		';
		
		return $isiemail;
	}

}
