<?php

class M_user Extends DB_QM {
	private $post  = array();
	private $table = "M_USERS";
	private $pKey  = "ID_USER";
	private $column_order = array('ID_USER'); //set column for datatable order
    private $column_search = array('UPPER(FULLNAME)', 'UPPER(USERNAME)', 'UPPER(NM_USERGROUP)', 'UPPER(NM_COMPANY)'); //set column for datatable search
    private $order = array("ID_USER" => 'ASC'); //default order
	private $comp = null;
	private $req = null;

	public function __construct(){
		$this->post = $this->input->post();
	}

	public function datalist($company = null){
		if($company){
			$this->db->where("a.ID_COMPANY", $company);
		}else{
			$this->db->where("a.ISACTIVE","Y");
		}
		$this->db->select("a.*,b.NM_USERGROUP");
		$this->db->join("M_USERGROUP b","a.ID_USERGROUP=b.ID_USERGROUP","LEFT");
		$this->db->where("a.DELETED","0");
		// $this->db->where("a.DEVELOPER","N");
		$this->db->order_by("a.ID_USER");
		return $this->db->get("M_USERS a")->result();
	}

	public function get_username_registered($username=''){
		$username = strtolower($username);
		$this->db->select("a.ID_USER,b.NM_COMPANY as COMPANY,a.FULLNAME, a.EMAIL, a.USERNAME");
		$this->db->join("M_COMPANY b","a.ID_COMPANY=b.ID_COMPANY","LEFT");
		$this->db->like('LOWER(a.FULLNAME)', "$username", 'BOTH');
		$this->db->order_by('a.FULLNAME', 'asc');
		$this->db->where("a.ISACTIVE","Y");
		$this->db->where("a.DELETED","0");

		$query = $this->db->get('M_USERS a');

		// echo $this->db->last_query();
		return $query->result();
	}


	public function search(&$keyword){
		$this->db->like("NM_USERS",$keyword);
		return $this->db->get("M_USERS")->result();
	}

	public function data($where){
		$this->db->select("a.*, b.NM_COMPANY, c.NM_PLANT, d.NM_AREA, e.*");
		$this->db->join("M_COMPANY b","a.ID_COMPANY=b.ID_COMPANY","LEFT");
		$this->db->join("M_PLANT c","a.ID_PLANT=c.ID_PLANT","LEFT");
		$this->db->join("M_AREA d","a.ID_AREA=d.ID_AREA","LEFT");
		$this->db->join("M_GROUPAREA e","e.ID_GROUPAREA=d.ID_GROUPAREA","LEFT");
		$this->db->where("lower(a.USERNAME)",$where['USERNAME']);
		$this->db->where("a.DELETED","0");
		return  $this->db->get("M_USERS a")->row();
	}

	public function get_data_by_id($ID_USER){
		$this->db->where("ID_USER",$ID_USER);
		return $this->db->get("M_USERS")->row();
	}

	public function get_data($where){
		$this->db->select("a.*, b.*, c.NM_PLANT, d.NM_AREA");
		$this->db->join("M_COMPANY b","a.ID_COMPANY=b.ID_COMPANY","LEFT");
		$this->db->join("M_PLANT c","a.ID_PLANT=c.ID_PLANT","LEFT");
		$this->db->join("M_AREA d","a.ID_AREA=d.ID_AREA","LEFT");
		$this->db->where($where);
		return $this->db->get("M_USERS a")->row();
	}

	public function data_except_id($where,$skip_id){
		$where['DELETED'] = "0";
		$this->db->where("ID_USER !=",$skip_id);
		$this->db->where($where);
		return $this->db->get("M_USERS")->row();
	}

	public function insert($data){
		$this->db->set($data);
		$this->db->set("ID_USER","SEQ_ID_USER.NEXTVAL",FALSE);
		$q = $this->db->insert("M_USERS");

		return $q;
	}

	public function insertGetId($data){
		$this->db->set($data);
		$this->db->set("ID_USER","SEQ_ID_USER.NEXTVAL",FALSE);
		$result = $this->db->insert("M_USERS");

		$lastId = false;
		if($result){
			$lastId = $this->maxId();
		}

		return $lastId;
	}

	public function maxId(){
	    $this->db->select_max('ID_USER');
	    $this->db->where('DELETED', '0');
	    $result = $this->db->get('M_USERS');
	    return $result->row()->ID_USER;
	}

	public function update($data,$ID_USER){
		$this->db->set($data);
		$this->db->where("ID_USER",$ID_USER);
		$this->db->update("M_USERS");
	}

	public function delete($ID_USER){
		$this->db->set("DELETED","1");
		$this->db->where("ID_USER",$ID_USER);
		$this->db->update("M_USERS");
	}

	public function activate($id, $status){
		$this->db->set("ISACTIVE", $status);
		$this->db->where("ID_USER", $id);
		$this->db->update("M_USERS");

		// echo $this->db->last_query();
	}

	public function get_query(){
		$this->db->select("a.*,b.NM_USERGROUP, c.NM_COMPANY");
		$this->db->from('M_USERS a');
		$this->db->join("M_USERGROUP b","a.ID_USERGROUP=b.ID_USERGROUP","LEFT");
		$this->db->join("M_COMPANY c","a.ID_COMPANY=c.ID_COMPANY","LEFT");
		$this->db->where("a.DELETED","0");
		$this->db->where("a.DEVELOPER","N");
		$this->db->order_by("a.ID_USER", "DESC");
		if($this->comp){
			$this->db->where("a.ID_COMPANY", $this->comp);
		}
		
		if($this->req == 'requester'){
			$this->db->order_by("a.ISACTIVE", "ASC");
			$this->db->where("a.ISACTIVE","N");
			$this->db->where("a.FLAG_ACTIVATION", 1);
		}else{
			$this->db->where_in("a.ISACTIVE", array("Y","N"));
		}
		// exit;
		#echo $this->db->get_compiled_select();exit();
	}

	public function get_list($company = null, $req = null) {
		if($company){
			$ses = $this->session->userdata('USER');
			$this->comp = $ses->ID_COMPANY;
		}
		if($req){
			$this->req = $req;
		}

		$this->get_query();
		$i = 0;

		//Loop column search
		// var_dump($this->column_search);
		// exit;
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
			#echo $this->db->get_compiled_select();exit();
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}
		#echo $this->db->get_compiled_select();exit();
		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function userRoles($id_user){
		$sql = "
			SELECT 
				UG.NM_USERGROUP 
			FROM 
				M_ROLES R 
				LEFT JOIN M_USERS U ON R.ID_USER = U.ID_USER
				LEFT JOIN M_USERGROUP UG ON UG.ID_USERGROUP = R.ID_USERGROUP
			WHERE
				R.ID_USER = '{$id_user}'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	public function getAdminEmail($id_company){
		$sql = "
			SELECT
				A.EMAIL
			FROM
				M_USERS A
			LEFT JOIN M_ROLES B ON
				A.ID_USER = B.ID_USER
			WHERE
				B.ID_USERGROUP = 162 
				AND A.ID_COMPANY = {$id_company}
				AND A.DELETED = 0
				AND A.ISACTIVE = 'Y'
		";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getData(){
		return $this->db->get("M_USERS")->result_array();
	}


}
