<section class="content-header">
	<h1>
	  	Scoring Bar Chart
	  	<small></small>
  	</h1>
</section>

<section class="content" >
  	<div class="row" >
    	<div class="col-md-12">
      		<div class="box">
  				<div class="box-body">
  					<div class="col-md-2">
                		<div class="form-group">
		                  <label>YEAR</label>
		                  <select id="tahun" class="form-control select2">
		                  	<?php
		                  		for($i=2016; $i<=date('Y'); $i++){
		                  			if(date('Y') == $i){
		                  				echo '<option selected value="'.$i.'">'.$i.'</option>';
		                  			}else{
		                  				echo '<option value="'.$i.'">'.$i.'</option>';
		                  			}
		                  		}
		                  	?>
		                  </select> 
                		</div>
  					</div>
  					<div class="col-md-2">
                		<div class="form-group">
		                  <label>PERIODE</label>
		                  <select id="tipe" class="form-control select2">
                  			<option value="1">1 Bulan</option>
                  			<option value="3">3 Bulan</option>
                  			<option value="6">6 Bulan</option>
                  			<option value="12">12 Bulan</option>
		                  </select>
                		</div>
  					</div>
  					<div class="col-md-8">
                		<div class="form-group">
		                  <label>&nbsp;</label><br>
		                  <button class="btn btn-sm btn-primary" id="btLoad"> <i class="fa fa-eye"></i> Load data</button>
                		</div>
  					</div>
  					<div class="col-md-12">
  						<hr>
  					</div>
		            <div class="row" >
		                <div class="col-lg-12">
		                    <div id="grafik_all" style="width:100%; height: 400px;"></div>
		                </div>
		            </div>
      			</div>
    		</div>
  		</div>
	</div>
</section>

<script type="text/javascript">
	$(function(){
		$('.select2').select2();
	});

	  	$(document).ready(function(){
	    	grafik_all()
          	$("#btLoad").click(function(){
            	grafik_all()
          	});
	      
	  });
	  
	  
  	function grafik_all(){
      	var tahun = $("#tahun").val();
	  	var tipe = $("#tipe").val();
	    $('#grafik_all').hide();
		$('#loading_css').show('700');
		$.ajax({
	        url: "<?=base_url('h_report_score/grafik_skor_bar')?>",
	        type: "POST",
	        data: {
	          'tahun' : tahun,
	          'tipe' : tipe,
	        },
	        dataType: "JSON",
	        success: function (data) {
             	$('#grafik_all').show();
	          var chart2 = new Highcharts.chart('grafik_all', {
						         title: {
						              text: 'SI HEBAT SCORING '+tahun+' ('+tipe+' Bulanan)' 
						            },
						          credits: {
						            enabled: false
						          },
						          exporting: {
								      allowHTML: true
								    },
						        xAxis: {
						        categories: data.kategori,
						        title: {
						              text: ''
						            }
						        },
						          yAxis: {
						            title: {
						              text: 'skor'
						            },
						          },
						          legend: {
						              layout: 'horizontal',
						              align: 'center',
						              verticalAlign: 'bottom'
						          },

						          plotOptions: {
						            series: {
						              label: {
						                connectorAllowed: false
						              }
						            }
						          },colors: [
	                            '#05354d',
	                            '#f7ca78',
	                            '#7bc8a4',
	                            '#93648d',
	                            '#4cc3d9',
	                            '#1f7f91',
	                            '#074b6d',
	                            '#d18700',
	                            '#ffdd9e',
	                        ],
			              	series: data.grafik,
			              	plotOptions: {
				          		column: {
					              	depth: 25,
					              	dataLabels: {
					              		_useHTML: true,
					                	enabled: true,
					                	crop: false,
					                	rotation: 270,
					                	x: 0,
                						y: -14,
	                	                style: {
				                      		fontSize: 10, 
						                	overflow: 'none',
				                      		textOutline: '1px contrast'
						                }
					           	 	}
					          	}
					      	},
			              	responsive: {
			                rules: [{
			                  	condition: {
				                    maxWidth: 500
			                  	},
			                  	chartOptions: {
			                    	legend: {
			                      		layout: 'horizontal',
			                      		align: 'center',
			                      		verticalAlign: 'bottom'
			                    	}
			                  	}
                			}]
          				}
            		},
        		);
      		} 
		});
  	}

</script>