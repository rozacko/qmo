<?php
/*
 * =((C102*D72)+(N72*E102)+(C103*D82)+(N82*E103))/(G102+G103)


KOL QAF
=PRODUKSI_OPC*QAFTOTAL_OPC/PRODUKSI_TOTAL  



QAF PLANT

*/

class Qaf_daily Extends DB_QM {
	
	public function generate_report($DATA,$REPORT){
            
		switch($REPORT){
			case "cement":
				$this->db->query("call HITUNG_QAF_DAILY(?,?,?,?)",array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_PRODUCT'],$DATA['ID_AREA']));
//                            echo "forca";    
//                            var_dump(array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_PRODUCT'],$DATA['ID_AREA']));
			break;
			case "cs":
				$this->db->query("call HITUNG_QAF_CS(?,?,?,?)",array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_AREA'],$DATA['ID_PRODUCT']));
			break;
			case "st":
				$this->db->query("call HITUNG_QAF_ST(?,?,?,?)",array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_AREA'],$DATA['ID_PRODUCT']));
			break;
			case "clinker":
				$this->db->query("call HITUNG_QAF_CLINKER(?,?,?)",array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_AREA']));
			break;
			case "company":
				$this->db->query("call HITUNG_QAF_COMPANY(?,?,?)",array($DATA['MONTH'],$DATA['YEAR'],$DATA['ID_COMPANY']));
			break;
		}
	}
	
	public function report_company($DATA){ 
		$this->db->where("a.BULAN",$DATA['MONTH']);
		$this->db->where("a.TAHUN",$DATA['YEAR']);
		$this->db->where("c.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
		$this->db->join("M_PLANT c","b.ID_PLANT=c.ID_PLANT and c.ID_COMPANY='".$DATA['ID_COMPANY']."'");
		$this->db->order_by("b.ID_AREA","asc");
		$s = $this->db->get("qaf_company a"); #die($this->db->last_query());
		return $s->result();
	}
	
	public function report($DATA){

		//mencari bulan tahun untuk component 28d
		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);


		// QUERY PERBAIKAN FAJAR
		$s = "SELECT distinct A .ID_QAF_DAILY, A .BULAN, A .TAHUN, A .ID_PRODUCT, A .ID_COMPONENT,
            CASE 
			WHEN A.ID_COMPONENT = 21
				THEN (
					SELECT distinct
					qaf_daily.S_DATA FROM qaf_daily 
					WHERE
					qaf_daily.BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM'))
					AND qaf_daily.TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY')) 
					AND qaf_daily.ID_PRODUCT = A.ID_PRODUCT
					AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.ID_COMPONENT = A.ID_COMPONENT
			 	)
			WHEN A .S_DATA = 0 AND A .S_IN = 0
            THEN (
                SELECT distinct
                qaf_daily.S_DATA FROM qaf_daily 
                WHERE
                qaf_daily.BULAN = A .BULAN AND qaf_daily.TAHUN = A .TAHUN AND qaf_daily.ID_PRODUCT = A .ID_PRODUCT
                AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.ID_COMPONENT = A .ID_COMPONENT
                )
            ELSE A .S_DATA END S_DATA,

            CASE 
			WHEN A.ID_COMPONENT = 21
				THEN (
					SELECT distinct qaf_daily.S_IN FROM qaf_daily 
					WHERE qaf_daily.BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM')) 
					AND qaf_daily.TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY'))
					AND qaf_daily.ID_PRODUCT = A .ID_PRODUCT AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.	ID_COMPONENT = 21
					)
			WHEN A .S_DATA = 0 AND A .S_IN = 0
            THEN (
                SELECT distinct qaf_daily.S_IN FROM qaf_daily WHERE qaf_daily.BULAN = A .BULAN AND qaf_daily.TAHUN = A .TAHUN
                AND qaf_daily.ID_PRODUCT = A .ID_PRODUCT AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.ID_COMPONENT = A .ID_COMPONENT
				)
            ELSE
                A .S_IN
            END 
                S_IN, A .PERSEN_QAF, A .AVG_QAF, A .ID_AREA, b.ID_AREA, b.ID_PLANT, b.KD_AREA, b.NM_AREA, b.ID_GROUPAREA,
                c.ID_C_PARAMETER_ORDER, c.ID_PARAMETER, c.URUTAN AS sortBy, c.ID_GROUPAREA, c.ID_COMPONENT, c.DISPLAY 
            FROM qaf_daily A
            JOIN M_AREA b ON A .ID_AREA = b.ID_AREA
            JOIN C_PARAMETER_ORDER c ON A .ID_COMPONENT = c.ID_COMPONENT AND b.ID_GROUPAREA = c.ID_GROUPAREA AND c.DISPLAY = 'D'
            WHERE
				a.BULAN = '".$DATA['MONTH']."' AND a.TAHUN = '".$DATA['YEAR']."' AND a.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' AND a.ID_AREA = '".$DATA['ID_AREA']."'

            /*
            UNION ALL

            SELECT A .ID_QAF_DAILY, A .BULAN, A .TAHUN, A .ID_PRODUCT, A .ID_COMPONENT,
                CASE WHEN A .S_DATA = 0 AND A .S_IN = 0
                THEN (
                    SELECT qaf_daily.S_DATA
                    FROM qaf_daily 
                    WHERE
                    qaf_daily.BULAN = '".$DATA['MONTH']."' - 1 AND qaf_daily.TAHUN = '".$DATA['YEAR']."' AND qaf_daily.ID_PRODUCT = A .ID_PRODUCT
                    AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.ID_COMPONENT = A .ID_COMPONENT
                    )
                ELSE A .S_DATA END S_DATA,
            CASE
            WHEN A .S_DATA = 0 AND A .S_IN = 0 THEN
				(
                SELECT qaf_daily.S_IN FROM qaf_daily WHERE qaf_daily.BULAN = '".$DATA['MONTH']."' - 1 AND qaf_daily.TAHUN = '".$DATA['YEAR']."'
                AND qaf_daily.ID_PRODUCT = A .ID_PRODUCT AND qaf_daily.ID_AREA = A .ID_AREA AND qaf_daily.ID_COMPONENT = A .ID_COMPONENT
				)
            ELSE
                A .S_IN
            END 
                A.S_DATA, A.S_IN, A .PERSEN_QAF, A .AVG_QAF, A .ID_AREA, b.ID_AREA, b.ID_PLANT, b.KD_AREA, b.NM_AREA, b.ID_GROUPAREA,
                c.ID_C_PARAMETER_ORDER, c.ID_PARAMETER, c.URUTAN, c.ID_GROUPAREA, c.ID_COMPONENT, c.DISPLAY 
            FROM qaf_daily A
            JOIN M_AREA b ON A .ID_AREA = b.ID_AREA
            JOIN C_PARAMETER_ORDER c ON A .ID_COMPONENT = c.ID_COMPONENT AND b.ID_GROUPAREA = c.ID_GROUPAREA AND c.DISPLAY = 'D'
			WHERE
				a.BULAN = '".$DATA['MONTH']."' - 1 AND a.TAHUN = '".$DATA['YEAR']."' AND a.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' AND a.ID_AREA = '".$DATA['ID_AREA']."' AND A .ID_COMPONENT = '21'
             */
            ORDER BY sortBy ASC
            "; #echo "<pre>$s</pre>";
               // var_dump($this->db->last_query()); 

        log_message('debug', $s);
		return $this->db->query($s)->result();
	} 
	
	public function report_cs($DATA){
		// $this->db->where("a.BULAN",$DATA['MONTH']);
		// $this->db->where("a.TAHUN",$DATA['YEAR']);
		// $this->db->where("a.ID_AREA",$DATA['ID_AREA']);
		// $this->db->where("a.ID_PRODUCT",$DATA['ID_PRODUCT']);
		// $this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
		// $this->db->join("C_PARAMETER_ORDER c","a.ID_COMPONENT=c.ID_COMPONENT and b.ID_GROUPAREA=c.ID_GROUPAREA and c.DISPLAY='D'");
		// $this->db->order_by("c.URUTAN","asc");
		// $s = $this->db->get("QAF_CS a"); 
		// #die($this->db->last_query());
		// return $s->result();

		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);
		//cs28d ambil bulan lalu
		$query = "SELECT z.*
			FROM (
			SELECT a.*,b.KD_AREA,b.NM_AREA, b.ID_PLANT, c.urutan
			FROM QAF_CS a 
			JOIN M_AREA b ON a.ID_AREA=b.ID_AREA 
			JOIN C_PARAMETER_ORDER c ON a.ID_COMPONENT=c.ID_COMPONENT and b.ID_GROUPAREA=c.ID_GROUPAREA and c.DISPLAY='D' 
			WHERE a.BULAN = '".$DATA['MONTH']."' AND a.TAHUN = '".$DATA['YEAR']."' AND a.ID_AREA = '".$DATA['ID_AREA']."' 
				AND a.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' 
				and a.ID_COMPONENT != 21
			union all
			SELECT a.*,b.KD_AREA,b.NM_AREA, b.ID_PLANT, c.urutan
			FROM QAF_CS a 
			JOIN M_AREA b ON a.ID_AREA=b.ID_AREA 
			JOIN C_PARAMETER_ORDER c ON a.ID_COMPONENT=c.ID_COMPONENT and b.ID_GROUPAREA=c.ID_GROUPAREA and c.DISPLAY='D' 
			WHERE a.BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM')) 
			AND a.TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY')) 
			AND a.ID_AREA = '".$DATA['ID_AREA']."' AND a.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' 
			and a.ID_COMPONENT = 21
			) z
			order by z.urutan asc";
			return $this->db->query($query)->result();
	}
	
	public function report_st($DATA){
		//master
		// $this->db->where("a.BULAN",$DATA['MONTH']);
		// $this->db->where("a.TAHUN",$DATA['YEAR']);
		// $this->db->where("a.ID_AREA",$DATA['ID_AREA']);
		// $this->db->where("a.ID_PRODUCT",$DATA['ID_PRODUCT']);
		// $this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
		// $this->db->join("C_PARAMETER_ORDER c","a.ID_COMPONENT=c.ID_COMPONENT and b.ID_GROUPAREA=c.ID_GROUPAREA and c.DISPLAY='D'");
		// $this->db->order_by("c.URUTAN","asc");
		// $s = $this->db->get("QAF_ST a"); 
		// #die($this->db->last_query());
		// return $s->result();

		$query = ("
				SELECT
				 *
				FROM
					QAF_ST A
				JOIN M_AREA b ON A .ID_AREA = b.ID_AREA
				JOIN C_PARAMETER_ORDER c ON A .ID_COMPONENT = c.ID_COMPONENT
				AND b.ID_GROUPAREA = c.ID_GROUPAREA
				AND c.DISPLAY = 'D'	
				INNER JOIN (SELECT MAX(X.ID_QAF_DAILY) ID_QAF_DAILY FROM QAF_ST X GROUP BY X.ID_COMPONENT) XA ON XA.ID_QAF_DAILY = A.ID_QAF_DAILY

				WHERE
					A .BULAN = '".$DATA['MONTH']."'
				AND A .TAHUN = '".$DATA['YEAR']."'
				AND A .ID_AREA = '".$DATA['ID_AREA']."'
				AND A .ID_PRODUCT = '".$DATA['ID_PRODUCT']."'
				AND b.ID_GROUPAREA = c.ID_GROUPAREA
				AND c.DISPLAY = 'D' 
				ORDER BY
					c.URUTAN ASC
			");
		return $this->db->query($query)->result();
	}

	public function report_st_outsilo($DATA){
		

		$query = ("
				SELECT
				 *
				FROM
					qaf_daily A
				JOIN M_AREA b ON A .ID_AREA = b.ID_AREA
				JOIN C_PARAMETER_ORDER c ON A .ID_COMPONENT = c.ID_COMPONENT
				AND b.ID_GROUPAREA = c.ID_GROUPAREA
				AND c.DISPLAY = 'D'					
				WHERE
					A .BULAN = '".$DATA['MONTH']."'
				AND A .TAHUN = '".$DATA['YEAR']."'
				AND A .ID_AREA = '".$DATA['ID_AREA']."'
				AND A .ID_PRODUCT = '".$DATA['ID_PRODUCT']."'
				AND b.ID_GROUPAREA = c.ID_GROUPAREA
				AND c.DISPLAY = 'D' 
				and a.ID_COMPONENT in (22,23)
				ORDER BY
					c.URUTAN ASC
			");
		return $this->db->query($query)->result();
	}
	
	public function report_clinker($DATA){
		$this->db->where("a.BULAN",$DATA['MONTH']);
		$this->db->where("a.TAHUN",$DATA['YEAR']);
		$this->db->where("a.ID_AREA",$DATA['ID_AREA']);
		$this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
		$this->db->join("C_PARAMETER_ORDER c","a.ID_COMPONENT=c.ID_COMPONENT and c.ID_GROUPAREA=4 and c.DISPLAY='H'");
		$this->db->order_by("c.URUTAN","asc");
		$s = $this->db->get("QAF_CLINKER a"); 
		#die($this->db->last_query());
		return $s->result();
	}
	
	public function report_clinker2($DATA, $ID_AREA){
		$this->db->where("a.BULAN",$DATA['MONTH']);
		$this->db->where("a.TAHUN",$DATA['YEAR']);
		$this->db->where("a.ID_AREA",$ID_AREA);
		$this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
		$this->db->join("C_PARAMETER_ORDER c","a.ID_COMPONENT=c.ID_COMPONENT and c.ID_GROUPAREA=4 and c.DISPLAY='H'");
		$this->db->order_by("c.URUTAN","asc");
		$s = $this->db->get("QAF_CLINKER a"); 
		// echo $this->db->last_query();
		return $s->result();
	}
	
	public function component($DATA,$COMPONENT=NULL){ #var_dump($DATA); exit;
		//$this->db->distinct();
		$date = date('d/m/Y');
		$DISPLAY = ($DATA['ID_GROUPAREA'] == 1|| $DATA['ID_GROUPAREA'] == 81)?'D':'H';
		
		$this->db->select("b.*,c.V_MIN, c.V_MAX");
		$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
		$this->db->join("C_RANGE_QAF c","a.ID_GROUPAREA=c.ID_GROUPAREA and a.ID_COMPONENT=c.ID_COMPONENT and b.ID_COMPONENT=c.ID_COMPONENT");
		$this->db->join("M_PLANT d","d.ID_COMPANY=c.ID_COMPANY and d.ID_COMPANY='".$DATA['ID_COMPANY']."' and d.ID_PLANT='".$DATA['ID_PLANT']."'");
		$this->db->join("C_PARAMETER_ORDER e","a.ID_GROUPAREA=e.ID_GROUPAREA and c.ID_GROUPAREA=e.ID_GROUPAREA and e.ID_COMPONENT=b.ID_COMPONENT and e.DISPLAY='".$DISPLAY."' ");
		$this->db->where("d.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->where("a.ID_GROUPAREA",$DATA['ID_GROUPAREA']);
		$this->db->where("c.FLAG",'1');
		// $this->db->where("TO_CHAR(c.PERIODE, 'DD/MM/YYYY') > TO_CHAR(TO_DATE('".$date."','DD/MM/YYYY'), 'DD/MM/YYYY')");
		$this->db->where("c.V_MIN IS NOT NULL",NULL,NULL);
		$this->db->where("c.V_MAX IS NOT NULL",NULL,NULL);
		if($COMPONENT) $this->db->where_in("b.ID_COMPONENT",$COMPONENT);
		if($DATA['ID_PRODUCT']) $this->db->where_in("c.ID_PRODUCT",$DATA['ID_PRODUCT']);
		$this->db->order_by("e.URUTAN","ASC");
		$s = $this->db->get("C_QAF_COMPONENT a"); #echo $this->db->last_query(); exit;
		return $s->result();
	}
	public function component3($DATA,$COMPONENT=NULL){ #var_dump($DATA); exit;
		//$this->db->distinct();
		$date = date('d/m/Y');
		$DISPLAY = ($DATA['ID_GROUPAREA'] == 1|| $DATA['ID_GROUPAREA'] == 81)?'D':'H';
		
		$this->db->select("b.*,c.V_MIN, c.V_MAX");
		$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
		$this->db->join("C_RANGE_QAF c","a.ID_GROUPAREA=c.ID_GROUPAREA and a.ID_COMPONENT=c.ID_COMPONENT and b.ID_COMPONENT=c.ID_COMPONENT");
		$this->db->join("M_PLANT d","d.ID_COMPANY=c.ID_COMPANY and d.ID_COMPANY='".$DATA['ID_COMPANY']."' and d.ID_PLANT='".$DATA['ID_PLANT']."'");
		$this->db->join("C_PARAMETER_ORDER e","a.ID_GROUPAREA=e.ID_GROUPAREA and c.ID_GROUPAREA=e.ID_GROUPAREA and e.ID_COMPONENT=b.ID_COMPONENT and e.DISPLAY='".$DISPLAY."' ");
		$this->db->where("d.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->where_in("a.ID_GROUPAREA",array(1,81));
		$this->db->where("c.FLAG",'1');
		// $this->db->where("TO_CHAR(c.PERIODE, 'DD/MM/YYYY') > TO_CHAR(TO_DATE('".$date."','DD/MM/YYYY'), 'DD/MM/YYYY')");
		$this->db->where("c.V_MIN IS NOT NULL",NULL,NULL);
		$this->db->where("c.V_MAX IS NOT NULL",NULL,NULL);
		if($COMPONENT) $this->db->where_in("b.ID_COMPONENT",$COMPONENT);
		if($DATA['ID_PRODUCT']) $this->db->where_in("c.ID_PRODUCT",$DATA['ID_PRODUCT']);
		$this->db->order_by("e.URUTAN","ASC");
		$s = $this->db->get("C_QAF_COMPONENT a"); #echo $this->db->last_query(); exit;
		return $s->result();
	}

	public function component_outsilo($DATA,$COMPONENT=NULL){ #var_dump($DATA); exit;
		//$this->db->distinct();
		$date = date('d/m/Y');
		$DISPLAY = ($DATA['ID_GROUPAREA'] == 1|| $DATA['ID_GROUPAREA'] == 81)?'D':'H';
		
		$this->db->select("b.*,c.V_MIN, c.V_MAX");
		$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
		$this->db->join("C_RANGE_QAF c","a.ID_GROUPAREA=c.ID_GROUPAREA and a.ID_COMPONENT=c.ID_COMPONENT and b.ID_COMPONENT=c.ID_COMPONENT");
		$this->db->join("M_PLANT d","d.ID_COMPANY=c.ID_COMPANY and d.ID_COMPANY='".$DATA['ID_COMPANY']."' and d.ID_PLANT='".$DATA['ID_PLANT']."'" , "LEFT");
		$this->db->join("C_PARAMETER_ORDER e","a.ID_GROUPAREA=e.ID_GROUPAREA and c.ID_GROUPAREA=e.ID_GROUPAREA and e.ID_COMPONENT=b.ID_COMPONENT and e.DISPLAY='".$DISPLAY."' ");
		$this->db->where("d.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->where_in("a.ID_GROUPAREA",array(81));
		$this->db->where("c.FLAG",'1');
		// $this->db->where("TO_CHAR(c.PERIODE, 'DD/MM/YYYY') > TO_CHAR(TO_DATE('".$date."','DD/MM/YYYY'), 'DD/MM/YYYY')");
		$this->db->where("c.V_MIN IS NOT NULL",NULL,NULL);
		$this->db->where("c.V_MAX IS NOT NULL",NULL,NULL);
		if($COMPONENT) $this->db->where_in("b.ID_COMPONENT",$COMPONENT);
		if($DATA['ID_PRODUCT']) $this->db->where_in("c.ID_PRODUCT",$DATA['ID_PRODUCT']);
		$this->db->order_by("e.URUTAN","ASC");
		$s = $this->db->get("C_QAF_COMPONENT a"); #echo $this->db->last_query(); exit;
		return $s->result();
	}
	
	public function component2($DATA,$ID_PLANT, $COMPONENT=NULL){ #var_dump($DATA); exit;
		//$this->db->distinct();
		$date = date('d/m/Y');
		$DISPLAY = ($DATA['ID_GROUPAREA'] == 1|| $DATA['ID_GROUPAREA'] == 81)?'D':'H';
		
		$this->db->select("b.*,c.V_MIN, c.V_MAX");
		$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
		$this->db->join("C_RANGE_QAF c","a.ID_GROUPAREA=c.ID_GROUPAREA and a.ID_COMPONENT=c.ID_COMPONENT and b.ID_COMPONENT=c.ID_COMPONENT");
		$this->db->join("M_PLANT d","d.ID_COMPANY=c.ID_COMPANY and d.ID_COMPANY='".$DATA['ID_COMPANY']."' and d.ID_PLANT='".$ID_PLANT."'");
		$this->db->join("C_PARAMETER_ORDER e","a.ID_GROUPAREA=e.ID_GROUPAREA and c.ID_GROUPAREA=e.ID_GROUPAREA and e.ID_COMPONENT=b.ID_COMPONENT and e.DISPLAY='".$DISPLAY."' ");
		$this->db->where("d.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->where("a.ID_GROUPAREA",$DATA['ID_GROUPAREA']);
		$this->db->where("c.FLAG",'1');
		// $this->db->where("TO_CHAR(c.PERIODE, 'DD/MM/YYYY') > TO_CHAR(TO_DATE('".$date."','DD/MM/YYYY'), 'DD/MM/YYYY')");
		$this->db->where("c.V_MIN IS NOT NULL",NULL,NULL);
		$this->db->where("c.V_MAX IS NOT NULL",NULL,NULL);
		if($COMPONENT) $this->db->where_in("b.ID_COMPONENT",$COMPONENT);
		if($DATA['ID_PRODUCT']) $this->db->where_in("c.ID_PRODUCT",$DATA['ID_PRODUCT']);
		$this->db->order_by("e.URUTAN","ASC");
		$s = $this->db->get("C_QAF_COMPONENT a"); #echo $this->db->last_query(); exit;
		return $s->result();
	}
	
	public function component1($DATA, $ID_PLANT, $COMPONENT=NULL){ #var_dump($DATA); exit;
		//$this->db->distinct();
		$date = date('d/m/Y');
		$DISPLAY = ($DATA['ID_GROUPAREA'] == 1 || $DATA['ID_GROUPAREA'] == 81)?'D':'H';
		
		$this->db->select("b.*,c.V_MIN, c.V_MAX, COUNT(A.ID_COMPONENT) as JUMLAH");
		$this->db->join("M_COMPONENT b","a.ID_COMPONENT=b.ID_COMPONENT");
		$this->db->join("C_RANGE_QAF c","a.ID_GROUPAREA=c.ID_GROUPAREA and a.ID_COMPONENT=c.ID_COMPONENT and b.ID_COMPONENT=c.ID_COMPONENT");
		$this->db->join("M_PLANT d","d.ID_COMPANY=c.ID_COMPANY and d.ID_COMPANY='".$DATA['ID_COMPANY']."' and d.ID_PLANT='".$ID_PLANT."'");
		$this->db->join("C_PARAMETER_ORDER e","a.ID_GROUPAREA=e.ID_GROUPAREA and c.ID_GROUPAREA=e.ID_GROUPAREA and e.ID_COMPONENT=b.ID_COMPONENT and e.DISPLAY='".$DISPLAY."' ");
		$this->db->where("d.ID_COMPANY",$DATA['ID_COMPANY']);
		$this->db->where("a.ID_GROUPAREA",$DATA['ID_GROUPAREA']);
		$this->db->where("c.FLAG",'1');
		// $this->db->where("TO_CHAR(c.PERIODE, 'DD/MM/YYYY') > TO_CHAR(TO_DATE('".$date."','DD/MM/YYYY'), 'DD/MM/YYYY')");
		$this->db->where("c.V_MIN IS NOT NULL",NULL,NULL);
		$this->db->where("c.V_MAX IS NOT NULL",NULL,NULL);
		if($COMPONENT) $this->db->where_in("b.ID_COMPONENT",$COMPONENT);
		if($DATA['ID_PRODUCT']) $this->db->where_in("c.ID_PRODUCT",$DATA['ID_PRODUCT']);
		$this->db->order_by("e.URUTAN","ASC");
		$s = $this->db->get("C_QAF_COMPONENT a"); #echo $this->db->last_query(); exit;
		return $s->result();

	}
	
	public function get_area_by_group1($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		// echo 'aaaa';
		// echo $param_in['ID_PLANT'];
		// echo $ID_GROUPAREA;
		// $this->db->select("a.ID_AREA");
			$this->db->distinct("a.ID_AREA");
			$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
		if($ID_PRODUCT) $this->db->join("C_QAF_PRODUCT c","a.ID_PLANT=b.ID_PLANT and c.ID_GROUPAREA=a.ID_GROUPAREA and c.ID_PLANT=a.ID_PLANT and c.ID_PLANT=b.ID_PLANT");
			$this->db->where("a.ID_GROUPAREA",$ID_GROUPAREA);
			// $this->db->where("a.ID_PLANT",$ID_PLANT);
			$this->db->where_in('a.ID_PLANT', $param_in['ID_PLANT']);
			$this->db->where("b.ID_COMPANY",$ID_COMPANY);
		if($ID_PRODUCT) $this->db->where("c.ID_PRODUCT",$ID_PRODUCT);
			$this->db->order_by("a.NM_AREA","asc");
			// return $this->db->get("M_AREA a")->result();

		$data = $this->db->get("M_AREA a");
		// echo $this->db->last_query();
		return $data->result();
	}


	public function get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		$this->db->distinct("a.ID_AREA");
		// $this->db->select("a.ID_AREA");
		$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
		if($ID_PRODUCT) 
		$this->db->join("C_QAF_PRODUCT c","a.ID_PLANT=b.ID_PLANT and c.ID_GROUPAREA=a.ID_GROUPAREA and c.ID_PLANT=a.ID_PLANT and c.ID_PLANT=b.ID_PLANT");
		$this->db->where("a.ID_GROUPAREA",$ID_GROUPAREA);
		$this->db->where_in('a.ID_PLANT', $ID_PLANT);
		$this->db->where("b.ID_COMPANY",$ID_COMPANY);
		if($ID_PRODUCT) $this->db->where("c.ID_PRODUCT",$ID_PRODUCT);
		$this->db->order_by("a.NM_AREA","asc");
		$data = $this->db->get("M_AREA a");
		//echo $this->db->last_query();
		return $data->result();
	}

	public function get_area_by_group2($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		
			$this->db->distinct("a.ID_AREA");
			$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
		if($ID_PRODUCT) $this->db->join("C_QAF_PRODUCT c","a.ID_PLANT=b.ID_PLANT and c.ID_GROUPAREA=a.ID_GROUPAREA and c.ID_PLANT=a.ID_PLANT and c.ID_PLANT=b.ID_PLANT");
			$this->db->where_in("a.ID_GROUPAREA",$ID_GROUPAREA);
			// $this->db->where("a.ID_PLANT",$ID_PLANT);
			$this->db->where_in('a.ID_PLANT', $param_in['ID_PLANT']);
			$this->db->where("b.ID_COMPANY",$ID_COMPANY);
		if($ID_PRODUCT) $this->db->where("c.ID_PRODUCT",$ID_PRODUCT);
			$this->db->order_by("a.NM_AREA","asc");
			// return $this->db->get("M_AREA a")->result();

		$data = $this->db->get("M_AREA a");
		// echo $this->db->last_query();
		return $data->result();
	}

	public function get_area_product_outsilo($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		
			$this->db->distinct("a.ID_AREA");
			$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");		
			$this->db->where_in("a.ID_GROUPAREA",$ID_GROUPAREA);
			$this->db->where_in('a.ID_PLANT', $param_in['ID_PLANT']);
			$this->db->where("b.ID_COMPANY",$ID_COMPANY);
			$this->db->order_by("a.NM_AREA","asc");

		$data = $this->db->get("M_AREA a");
		// echo $this->db->last_query();
		return $data->result();
	}
	public function get_area_by_group_time($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		$this->db->distinct("a.ID_AREA");
		// $this->db->select("a.ID_AREA");
		$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
		if($ID_PRODUCT) 
		$this->db->join("C_QAF_PRODUCT c","a.ID_PLANT=b.ID_PLANT and c.ID_GROUPAREA=a.ID_GROUPAREA and c.ID_PLANT=a.ID_PLANT and c.ID_PLANT=b.ID_PLANT");
		$this->db->where_in("a.ID_GROUPAREA",$ID_GROUPAREA);
		$this->db->where_in('a.ID_PLANT', $ID_PLANT);
		$this->db->where("b.ID_COMPANY",$ID_COMPANY);
		if($ID_PRODUCT) $this->db->where("c.ID_PRODUCT",$ID_PRODUCT);
		$this->db->order_by("a.NM_AREA","asc");
		$data = $this->db->get("M_AREA a");
		//echo $this->db->last_query();
		return $data->result();
	}
	public function get_area_by_group_outsilo($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT=NULL){
		
			$this->db->distinct();
			$this->db->select("a.ID_AREA,a.KD_AREA,a.NM_AREA,a.ID_GROUPAREA,
			b.ID_COMPANY,b.ID_PLANT,b.KD_PLANT,b.NM_PLANT,
			c.ID_PRODUCT");
			$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
			$this->db->join("C_PRODUCT c","c.ID_AREA = a.ID_AREA");
			$this->db->where_in("a.ID_GROUPAREA",$ID_GROUPAREA);
			// $this->db->where("a.ID_PLANT",$ID_PLANT);
			$this->db->where_in('a.ID_PLANT', $param_in['ID_PLANT']);
			$this->db->where("b.ID_COMPANY",$ID_COMPANY);
			$this->db->order_by("a.NM_AREA,c.ID_PRODUCT","asc");
			// return $this->db->get("M_AREA a")->result();

		$data = $this->db->get("M_AREA a");
		// echo $this->db->last_query();
		return $data->result();
	}
	
	public function qaf_produksi($DATA, $param_in){ 
			$data = implode("','",$DATA['ID_PLANT']);
			$query = " AND A.id_plant IN ('".$data."')";

            $areas_res = $this->db->query('SELECT a.ID_AREA 
                FROM m_area a
                WHERE a.ID_GROUPAREA=? '.$query,
                [
                    $DATA['ID_GROUPAREA']
                ]
            );

            $id_areas = array_map(function($area) {
                    return "'" . $area->ID_AREA . "'";
                },
                $areas_res->result()
            );

            if (! $id_areas ) {
                return [];
            }

            $str_areas = implode(',', $id_areas);

			//perbaikan FAJAR
            $s = "SELECT a.*, q.jum1 as QAF, q.jml_data as JML_COMPONENT, p.NILAI as PRODUKSI
                FROM m_area a 
                LEFT JOIN 
                (SELECT 
					SUM(persen_qaf) as jum1, COUNT(*) as jml_data, ID_AREA
					FROM (
						SELECT b.persen_qaf, b.ID_QAF_DAILY, b.ID_AREA FROM qaf_daily b where b.BULAN = '".$DATA['MONTH']."' 
							AND b.TAHUN = '".$DATA['YEAR']."' AND b.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' 
                            AND b.ID_AREA in (" . $str_areas . ") 
                        /*
                        UNION ALL
						SELECT k.persen_qaf, k.ID_QAF_DAILY, k.ID_AREA FROM qaf_daily k where k.BULAN = '".$DATA['MONTH']."' - 1 
						AND k.TAHUN = '".$DATA['YEAR']."' AND k.ID_PRODUCT = '".$DATA['ID_PRODUCT']."' AND k.ID_COMPONENT = '21'
                        AND k.ID_AREA in (" . $str_areas . ") 
                        */
					) Z
					WHERE Z.ID_AREA in (" . $str_areas . ") 
                        AND Z.persen_qaf != 0
                    GROUP BY ID_AREA
                ) q on q.ID_AREA = a.ID_AREA
                /*
                (SELECT COUNT (c.ID_COMPONENT)
                    FROM
                        C_RANGE_QAF c
                    WHERE
                        c.ID_GROUPAREA = A.ID_GROUPAREA 
                        AND c.ID_PRODUCT = '".$DATA['ID_PRODUCT']."'
                        AND c.ID_COMPANY = '".$DATA['ID_COMPANY']."'
                        AND c.FLAG = '1'
                        AND c.V_MIN is not null
                ) JML_COMPONENT,
                 */
                LEFT JOIN 
                (SELECT 
                    d.NILAI, E.ID_AREA
                    FROM D_PRODUCTION d, T_PRODUCTION e 
                    WHERE e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
                    AND e.TAHUN='".$DATA['YEAR']."' 
                    AND E.ID_AREA in (". $str_areas .") ".(($DATA['ID_GROUPAREA']==1)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
                  ) p on p.ID_AREA = a.ID_AREA
                WHERE a.ID_AREA in (" . $str_areas. ")
                ORDER BY a.ID_AREA"; #echo "<pre>$s</pre>";

        log_message('debug', $s);

			//query mastert
		// $s = "select a.*,(
		// 	select sum(b.persen_qaf) from qaf_daily b where b.id_area=a.id_area 
		// 	AND b.BULAN = '".$DATA['MONTH']."'
		// 	and b.TAHUN = '".$DATA['YEAR']."' 
		// 	".(($DATA['ID_GROUPAREA']==1)?" AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
		// 	) as QAF,
		// 	(select count(c.ID_COMPONENT) from C_QAF_COMPONENT c where c.ID_GROUPAREA=a.ID_GROUPAREA) JML_COMPONENT,
		// 	(select 
		// 		d.NILAI from D_PRODUCTION d, T_PRODUCTION e 
		// 		where e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
		// 		AND e.TAHUN='".$DATA['YEAR']."' 
		// 		AND E.ID_AREA=A.ID_AREA ".(($DATA['ID_GROUPAREA']==1)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
		// 	  ) as PRODUKSI
		// 	from m_area a where a.ID_GROUPAREA='".$DATA['ID_GROUPAREA']."' ". $query; #echo "<pre>$s</pre>";

		$data = $this->db->query($s);
		// echo $this->db->last_query();

		return $data->result();
	}

	public function qaf_produksi_outsilo($DATA, $param_in){ 
		$data = implode("','",$DATA['ID_PLANT']);
		$query = " AND A.id_plant IN ('".$data."')";

		$areas_res = $this->db->query('SELECT a.ID_AREA 
			FROM m_area a
			WHERE a.ID_GROUPAREA=? '.$query,
			[
				$DATA['ID_GROUPAREA']
			]
		);

		$id_areas = array_map(function($area) {
				return "'" . $area->ID_AREA . "'";
			},
			$areas_res->result()
		);

		if (! $id_areas ) {
			return [];
		}

		$str_areas = implode(',', $id_areas);

		//perbaikan FAJAR
		$s = "SELECT a.*,
						q.jml_qaf AS QAF,
						q.jml_data AS JML_COMPONENT,
						p.NILAI AS PRODUKSI,
						q.ID_PRODUCT
			FROM m_area a 
			LEFT JOIN 
			(SELECT SUM( persen_qaf ) AS jml_qaf,COUNT( * ) AS jml_data,ID_AREA, ID_PRODUCT
				FROM (
					SELECT b.persen_qaf,b.ID_QAF_DAILY,	b.ID_AREA,	b.ID_PRODUCT
					FROM qaf_daily b where b.BULAN = '".$DATA['MONTH']."' 
						AND b.TAHUN = '".$DATA['YEAR']."' 
						AND b.ID_AREA in (" . $str_areas . ") 			
				) Z
				WHERE Z.ID_AREA in (" . $str_areas . ") 
					AND Z.persen_qaf != 0
				GROUP BY ID_AREA,ID_PRODUCT
			) q on q.ID_AREA = a.ID_AREA			
			LEFT JOIN 
			(SELECT  d.NILAI, E.ID_AREA, D.ID_PRODUCT
				FROM D_PRODUCTION d, T_PRODUCTION e 
				WHERE e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND E.ID_AREA in (". $str_areas .") 
			  ) p on p.ID_AREA = a.ID_AREA and p.ID_PRODUCT = q.ID_PRODUCT
			WHERE a.ID_AREA in (" . $str_areas. ")
			ORDER BY a.ID_AREA"; #echo "<pre>$s</pre>";

		log_message('debug', $s);

		$data = $this->db->query($s);

		return $data->result();
	}

	public function qaf_produksi_outsilo_sum($DATA, $param_in){ 
		$data = implode("','",$DATA['ID_PLANT']);
		$query = " AND A.id_plant IN ('".$data."')";

		$areas_res = $this->db->query('SELECT a.ID_AREA 
			FROM m_area a
			WHERE a.ID_GROUPAREA=? '.$query,
			[
				$DATA['ID_GROUPAREA']
			]
		);

		$id_areas = array_map(function($area) {
				return "'" . $area->ID_AREA . "'";
			},
			$areas_res->result()
		);

		if (! $id_areas ) {
			return [];
		}

		$str_areas = implode(',', $id_areas);		
		$str_produk = implode(',',$DATA['LIST_PRODUK']);

		//mencari bulan tahun untuk component 28d
		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);

		//summary produksi by produk
		$s = "SELECT a.*,
						to_number(q.result_qaf) AS QAF,
						q.ttal_data AS JML_COMPONENT,
						p.NILAI AS PRODUKSI						
			FROM m_product a 	
			left join
			(
				select sum(qaf_prd) as ttal_qaf, count(qaf_prd) as ttal_data,
					(sum(qaf_prd) / count(qaf_prd) ) as result_qaf,ID_PRODUCT 
				from (
						SELECT SUM( persen_qaf ) AS jml_qaf,COUNT( * ) AS jml_data,ID_AREA, ID_PRODUCT,
							(SUM( persen_qaf ) / COUNT( * )) as QAF_PRD
						FROM (
							--ambil qaf dan componenct 28d untuk bulan kemarin
							select *
								from(
								SELECT persen_qaf,ID_QAF_DAILY, ID_AREA, ID_PRODUCT ,ID_Component,BULAN,TAHUN
											FROM qaf_daily b where BULAN = '".$DATA['MONTH']."' AND TAHUN = '".$DATA['YEAR']."' AND ID_AREA in (" . $str_areas . ") 
										and id_component not in ('21')	
								union all
								SELECT persen_qaf,ID_QAF_DAILY, ID_AREA, ID_PRODUCT ,ID_Component,BULAN,TAHUN
											FROM qaf_daily b 
											where BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM'))
											AND TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY'))
											AND ID_AREA in (" . $str_areas . ") 
										and id_component in ('21')	
								)
								where id_component in (select id_component from C_RANGE_QAF 
														where flag=1 and  id_company=".$DATA['ID_COMPANY'] ." and ID_GROUPAREA=81)

						) Z
						WHERE Z.ID_AREA in (" . $str_areas . ")		
						AND Z.persen_qaf != 0 		 				
						GROUP BY Z.ID_AREA, Z.ID_PRODUCT
				) ttl 		
				group by ttl.ID_PRODUCT
			) q on q.ID_PRODUCT = a.ID_PRODUCT 	
			LEFT JOIN 
			(SELECT  d.NILAI, D.ID_PRODUCT
				FROM D_PRODUCTION_OUTSILO d 
				left join T_PRODUCTION_OUTSILO e on e.ID_PRODUCTION_OUTSILO=d.ID_PRODUCTION_OUTSILO
				WHERE E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND e.ID_COMPANY = ".$DATA['ID_COMPANY'] ." 
				and d.NILAI is not null
				and d.FLAG_SUM =1 
			  ) p on p.ID_PRODUCT = a.ID_PRODUCT
			WHERE a.ID_PRODUCT in (".$str_produk.") 
			ORDER BY a.ID_PRODUCT";

		log_message('debug', $s);

		$data = $this->db->query($s);

		return $data->result();
	}
	
	public function qaf_produksi_cs($DATA){
            // var_dump($DATA);
		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);
		
		$s = "select a.*,(

				select round ( (sum(z.persen_qaf)/ count(z.id_area) ) , 2)
				FROM (
						select b.persen_qaf, b.ID_AREA
						from qaf_cs b 
						where  b.BULAN = '".$DATA['MONTH']."'
						and b.S_DATA != 0
						and b.TAHUN = '".$DATA['YEAR']."' 
						and b.BULAN IS NOT NULL 
						AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."'
						and b.ID_COMPONENT != 21
						union all
						select b.persen_qaf, b.ID_AREA
						from qaf_cs b 
						where  b.BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM'))
						and b.S_DATA != 0
						and b.TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY'))
						and b.BULAN IS NOT NULL 
						AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."'
						and b.ID_COMPONENT = 21
				) z	
				where z.id_area = a.id_area
				group by z.id_area
			
			) as QAF,
			(select count(c.ID_COMPONENT) from C_QAF_COMPONENT c where c.ID_GROUPAREA=a.ID_GROUPAREA) JML_COMPONENT,
			(select 
				d.NILAI from D_PRODUCTION d, T_PRODUCTION e 
				where e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND E.ID_AREA=A.ID_AREA ".(($DATA['ID_GROUPAREA']==1)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			  ) as PRODUKSI,
			(select sum(f.NILAI) from D_CEMENT_DAILY f, T_CEMENT_DAILY g where f.ID_CEMENT_DAILY=g.ID_CEMENT_DAILY and 
			TO_CHAR(g.DATE_DATA, 'YYYYMM')='".$DATA['YEAR'].$DATA['MONTH']."' and g.ID_PRODUCT='".$DATA['ID_PRODUCT']."'
			AND g.ID_AREA=A.ID_AREA  ) as PRODUKSICEMENT
			from m_area a where a.id_plant='".$DATA['ID_PLANT']."' AND a.ID_GROUPAREA in ('81','".$DATA['ID_GROUPAREA']."') 		
		"; #echo "<pre>$s</pre>";
		return $this->db->query($s)->result();
	}
	
	public function qaf_produksi_st($DATA){
		$s = "select a.*,(
			select round(sum(b.persen_qaf)/count(a.id_area) ,2)
			from qaf_st b where b.id_area=a.id_area 
			AND b.BULAN = '".$DATA['MONTH']."'
			and b.TAHUN = '".$DATA['YEAR']."' 
			and b.BULAN IS NOT NULL
			".(($DATA['ID_GROUPAREA']==1 || $DATA['ID_GROUPAREA']==81)?" AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")." 
			
			) as QAF,
			(select count(c.ID_COMPONENT) from C_QAF_COMPONENT c where c.ID_GROUPAREA=a.ID_GROUPAREA) as JML_COMPONENT,
			(select 
				d.NILAI from D_PRODUCTION d, T_PRODUCTION e 
				where e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND E.ID_AREA=A.ID_AREA ".(($DATA['ID_GROUPAREA']==1 || $DATA['ID_GROUPAREA']==81)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			  ) as PRODUKSI,
			(select sum(f.NILAI) from D_CEMENT_DAILY f, T_CEMENT_DAILY g where f.ID_CEMENT_DAILY=g.ID_CEMENT_DAILY and 
			TO_CHAR(g.DATE_DATA, 'YYYYMM')='".$DATA['YEAR'].$DATA['MONTH']."' and g.ID_PRODUCT='".$DATA['ID_PRODUCT']."'
			AND g.ID_AREA=A.ID_AREA  ) as PRODUKSICEMENT
			from m_area a where a.id_plant='".$DATA['ID_PLANT']."' AND a.ID_GROUPAREA in ('81','".$DATA['ID_GROUPAREA']."') 		
		"; #echo "<pre>$s</pre>";
		return $this->db->query($s)->result();
	}
	
	public function qaf_produksi_clinker($DATA){
		$s = "select a.*,(
			select sum(b.persen_qaf) from qaf_clinker b where b.id_area=a.id_area 
			AND b.BULAN = '".$DATA['MONTH']."'
			and b.TAHUN = '".$DATA['YEAR']."' 
			".(($DATA['ID_GROUPAREA']==1)?" AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			) as QAF,
			(select count(c.ID_COMPONENT) from C_QAF_COMPONENT c where c.ID_GROUPAREA=a.ID_GROUPAREA) JML_COMPONENT,
			(select 
				d.NILAI from D_PRODUCTION d, T_PRODUCTION e 
				where e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND E.ID_AREA=A.ID_AREA ".(($DATA['ID_GROUPAREA']==1)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			  ) as PRODUKSI
			from m_area a where a.id_plant='".$DATA['ID_PLANT']."' AND a.ID_GROUPAREA='".$DATA['ID_GROUPAREA']."' 		
		"; #echo "<pre>$s</pre>";
		return $this->db->query($s)->result();
	}

	public function qaf_produksi_clinker1($DATA){
			$data = implode("','",$DATA['ID_PLANT']);
			$query = " AND A.id_plant IN ('".$data."')";
		$s = "select a.*,(
			select sum(b.persen_qaf) from qaf_clinker b where b.id_area=a.id_area 
			AND b.BULAN = '".$DATA['MONTH']."'
			and b.TAHUN = '".$DATA['YEAR']."' 
			".(($DATA['ID_GROUPAREA']==1)?" AND b.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			) as QAF,
			(
				SELECT COUNT (c.ID_COMPONENT)
				FROM QAF_CLINKER c
				WHERE c.ID_AREA = A.ID_AREA and S_DATA > 0 and c.BULAN = '".$DATA['MONTH']."' 
				and c.TAHUN = ".$DATA['YEAR']."
			) JML_COMPONENT,
			(select 
				d.NILAI from D_PRODUCTION d, T_PRODUCTION e 
				where e.ID_PRODUCTION=d.ID_PRODUCTION and E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND E.ID_AREA=A.ID_AREA ".(($DATA['ID_GROUPAREA']==1)?" and D.ID_PRODUCT='".$DATA['ID_PRODUCT']."' ":"")."
			  ) as PRODUKSI
			from m_area A JOIN M_PLANT  H ON A.ID_PLANT = H.ID_PLANT 
	join M_GROUPPLANT  G ON H.ID_GROUP_PLANT = G.ID_GROUP_PLANT where H.ID_COMPANY='".$DATA['ID_COMPANY']."' AND a.ID_GROUPAREA='".$DATA['ID_GROUPAREA']."' "
	. $query ." order by a.ID_AREA"; #echo "<pre>$s</pre>";
		// $data = $this->db->query($s);
		// echo $this->db->last_query();
		return $this->db->query($s)->result();
	}

	//compresive strength Outsilo
	public function qaf_cs_outsilo_sum($DATA, $param_in){ 
		$data = implode("','",$DATA['ID_PLANT']);
		$query = " AND A.id_plant IN ('".$data."')";

		$areas_res = $this->db->query('SELECT a.ID_AREA 
			FROM m_area a
			WHERE a.ID_GROUPAREA=? '.$query,
			[
				$DATA['ID_GROUPAREA']
			]
		);

		$id_areas = array_map(function($area) {
				return "'" . $area->ID_AREA . "'";
			},
			$areas_res->result()
		);

		if (! $id_areas ) {
			return [];
		}		

		$str_areas = implode(',', $id_areas);		
		$str_produk = implode(',',$DATA['LIST_PRODUK']);

		//mencari bulan tahun untuk component 28d
		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);

		//summary produksi by produk
		$s = "SELECT a.*,
						to_number(q.result_qaf) AS QAF,
						q.ttal_data AS JML_COMPONENT,
						p.NILAI AS PRODUKSI						
			FROM m_product a 	
			left join
			(
				select sum(qaf_prd) as ttal_qaf, count(qaf_prd) as ttal_data,
					(sum(qaf_prd) / count(qaf_prd) ) as result_qaf,ID_PRODUCT 
				from (
						SELECT SUM( persen_qaf ) AS jml_qaf,COUNT( * ) AS jml_data,ID_AREA, ID_PRODUCT,
							(SUM( persen_qaf ) / COUNT( * )) as QAF_PRD
						FROM (
							--ambil qaf dan componenct 28d untuk bulan kemarin
							select *
								from(
								SELECT persen_qaf,ID_QAF_DAILY, ID_AREA, ID_PRODUCT ,ID_Component,BULAN,TAHUN
											FROM qaf_daily b where BULAN = '".$DATA['MONTH']."' AND TAHUN = '".$DATA['YEAR']."' AND ID_AREA in (" . $str_areas . ") 
										and id_component not in ('21') and id_component in ('19','20')	and S_DATA > 0	
								union all
								SELECT persen_qaf,ID_QAF_DAILY, ID_AREA, ID_PRODUCT ,ID_Component,BULAN,TAHUN
											FROM qaf_daily b 
											where BULAN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'MM'))
											AND TAHUN = to_number(to_char(add_months(to_date('".$tahunbulan."','YYYYMM'),-1),'YYYY'))
											AND ID_AREA in (" . $str_areas . ") 
										and id_component in ('21') and S_DATA > 0	
								)
								where id_component in ('19','20','21')

						) Z
						WHERE Z.ID_AREA in (" . $str_areas . ")												
						GROUP BY Z.ID_AREA, Z.ID_PRODUCT
				) ttl 		
				group by ttl.ID_PRODUCT
			) q on q.ID_PRODUCT = a.ID_PRODUCT 	
			LEFT JOIN 
			(SELECT  d.NILAI, D.ID_PRODUCT
				FROM D_PRODUCTION_OUTSILO d 
				left join T_PRODUCTION_OUTSILO e on e.ID_PRODUCTION_OUTSILO=d.ID_PRODUCTION_OUTSILO
				WHERE E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND e.ID_COMPANY = ".$DATA['ID_COMPANY'] ." 
				and d.NILAI is not null
				and d.FLAG_SUM =1 
			  ) p on p.ID_PRODUCT = a.ID_PRODUCT
			WHERE a.ID_PRODUCT in (".$str_produk.") 
			ORDER BY a.ID_PRODUCT";

		log_message('debug', $s);

		$data = $this->db->query($s);

		return $data->result();
	}
	//compresive strength Outsilo
	public function qaf_st_outsilo_sum($DATA, $param_in){ 
		$data = implode("','",$DATA['ID_PLANT']);
		$query = " AND A.id_plant IN ('".$data."')";

		$areas_res = $this->db->query('SELECT a.ID_AREA 
			FROM m_area a
			WHERE a.ID_GROUPAREA=? '.$query,
			[
				$DATA['ID_GROUPAREA']
			]
		);

		$id_areas = array_map(function($area) {
				return "'" . $area->ID_AREA . "'";
			},
			$areas_res->result()
		);

		if (! $id_areas ) {
			return [];
		}		

		$str_areas = implode(',', $id_areas);		
		$str_produk = implode(',',$DATA['LIST_PRODUK']);

		//mencari bulan tahun untuk component 28d
		$tahunbulan = $DATA['YEAR'].sprintf('%02d', $DATA['MONTH']);

		//summary produksi by produk
		$s = "SELECT a.*,
						to_number(q.result_qaf) AS QAF,
						q.ttal_data AS JML_COMPONENT,
						p.NILAI AS PRODUKSI						
			FROM m_product a 	
			left join
			(
				select sum(qaf_prd) as ttal_qaf, count(qaf_prd) as ttal_data,
					(sum(qaf_prd) / count(qaf_prd) ) as result_qaf,ID_PRODUCT 
				from (
						SELECT SUM( persen_qaf ) AS jml_qaf,COUNT( * ) AS jml_data,ID_AREA, ID_PRODUCT,
							(SUM( persen_qaf ) / COUNT( * )) as QAF_PRD
						FROM (
							--ambil qaf dan componenct 28d untuk bulan kemarin
							select *
								from(
								SELECT persen_qaf,ID_QAF_DAILY, ID_AREA, ID_PRODUCT ,ID_Component,BULAN,TAHUN
											FROM qaf_daily b where BULAN = '".$DATA['MONTH']."' AND TAHUN = '".$DATA['YEAR']."' AND ID_AREA in (" . $str_areas . ") 
										 and id_component in ('22','23') and S_DATA > 0									
								)
								where id_component in ('22','23')

						) Z
						WHERE Z.ID_AREA in (" . $str_areas . ")								 				
						GROUP BY Z.ID_AREA, Z.ID_PRODUCT
				) ttl 		
				group by ttl.ID_PRODUCT
			) q on q.ID_PRODUCT = a.ID_PRODUCT 	
			LEFT JOIN 
			(SELECT  d.NILAI, D.ID_PRODUCT
				FROM D_PRODUCTION_OUTSILO d 
				left join T_PRODUCTION_OUTSILO e on e.ID_PRODUCTION_OUTSILO=d.ID_PRODUCTION_OUTSILO
				WHERE E.BULAN='".$DATA['MONTH']."' 
				AND e.TAHUN='".$DATA['YEAR']."' 
				AND e.ID_COMPANY = ".$DATA['ID_COMPANY'] ." 
				and d.NILAI is not null
				and d.FLAG_SUM =1 
			  ) p on p.ID_PRODUCT = a.ID_PRODUCT
			WHERE a.ID_PRODUCT in (".$str_produk.") 
			ORDER BY a.ID_PRODUCT";

		log_message('debug', $s);

		$data = $this->db->query($s);

		return $data->result();
	}
}
