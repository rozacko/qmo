<?php

class M_report_up Extends DB_QM {
	
	public function get_list($id_pp = false){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, MU.FULLNAME, TPP.*");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.GROUP_PP != 'Internal'");
		$this->db->where("TPP.DELETE_BY IS NULL");
		if($id_pp != null){
			$this->db->where("TP.ID_PRIODE", $id_pp);
		} 
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
	}
	
	public function get_list_non_admin($id_pic){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, MU.FULLNAME, TPP.*");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TP.ID_PIC", $id_pic);
		$this->db->where("TPP.GROUP_PP != 'Internal'");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
	}
	
	public function insert_laporan_pdf($id, $path){
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_PROFICIENCY SET FILE_LAPORAN = '{$path}', FILE_LAPORAN_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS') WHERE ID_PROFICIENCY = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function insert_laporan_exc($id, $path){
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_PROFICIENCY SET FILE_SAMPLE = '{$path}', FILE_SAMPLE_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS') WHERE ID_PROFICIENCY = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
}

?>