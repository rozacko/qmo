<?php

class Coating_behavior extends QMUser {

	public $list_data = array();
	public $list_company = array();
	public $list_type = array();
	public $data_incident;
	public $solution;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_incident");
		$this->load->model("m_company");
		$this->load->model("c_ncqr_product");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
	}

	public function index(){  
		$this->load->model("m_company");
		$this->list_company = $this->m_company->datalist();
		$this->list_type = $this->m_incident->type();
		$this->template->adminlte("v_behavior");
	}

	public function get_ajax(){
			$t_prod 	= $this->t_cement_hourly->data_behavior(" DATE_DATA >= TO_DATE ('".$this->input->post('STARTDATE')."', 'dd/mm/yyyy') AND TO_DATE (TO_CHAR ( DATE_DATA, 'DDMMYYYY'),'DDMMYYYY') <= TO_DATE ('".$this->input->post('ENDDATE')."', 'dd/mm/yyyy') AND ID_AREA='".$this->input->post('ID_AREA')."'");
			$get_area 	= $this->t_cement_hourly->get_area($this->input->post('ID_AREA'));
			foreach ($get_area as $key1 => $value1) {
				$area = $value1['NM_AREA'];
			}
			$data = array();

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {

				$data[$key][] = $row->DATE_DATA; 
				$data[$key][] = $row->JAM_DATA; 
				$d_prod = $this->d_cement_hourly->behavior_by_id($row->ID_CEMENT_HOURLY);
				foreach ($d_prod as $k => $vl) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
				}
			}
		}
 		echo json_encode(array("data" => $data, "nama_area" => $area));
	}

	public function get_ajax_live(){ 
		// echo $this->input->post('ID_AREA');exit;
			$date = date("d-m-Y");
		
			$t_prod 	= $this->t_cement_hourly->data_where_behavior(" ID_AREA='".$this->input->post('ID_AREA')."' AND A.ID_MESIN_STATUS IS NULL ", $date);
			$get_area 	= $this->t_cement_hourly->get_area($this->input->post('ID_AREA'));
			foreach ($get_area as $key1 => $value1) {
				$area = $value1['NM_AREA'];
			}
			$data = array();
		if (!empty($t_prod)) {
				$nill = 0;
			foreach ($t_prod as $key => $row) {
					$d_prod = $this->d_cement_hourly->data_where_behavior_live($row->ID_CEMENT_HOURLY);
				    //echo '<br>';var_dump($d_prod);

					$l = 0;
					$kosong = false;
					foreach ($d_prod as $k => $vl) {
						if($vl->NILAI!=''){
							$l++;
							$data[$nill][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
							$kosong = true;
						}else{
							
						}

						if($nill >= 8){
							break;
						}
					}
					if($kosong){
						$data[$nill][] = $row->DATE_DATA; 
						$data[$nill][] = $row->JAM_DATA; 
							$nill++;
						// $data[$key][] = ($nil=='') ? '': (float) $nil;
					}
				}
		}
		$data = array_reverse($data);
 		echo json_encode(array("data" => $data, "nama_area" => $area));
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->c_ncqr_product->ncqr_area_behavior($ID_COMPANY,$ID_PLANT);# echo $this->m_area->get_sql();
		echo json_encode($area);
	}

 

}

?>
