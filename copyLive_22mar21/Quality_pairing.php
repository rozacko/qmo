<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality_pairing extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_product");
		$this->load->model("c_parameter");
		$this->load->model("m_product");
		$this->load->model("m_component");
		$this->load->model("m_area");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("c_qaf_component");
	}

	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		// $this->list_grouparea = $this->m_grouparea->datalist_auth($this->USER->ID_GROUPAREA);
		// $this->list_product   = $this->m_product->datalist();
		// $this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_quality_pairing", $data);
	}

	public function ajax_get_plant_by_company(){
        $opt_c = (NULL==$this->input->get('company_ids'))? [0]:$this->input->get('company_ids');

		$plants = $this->m_plant->datalist($opt_c);
		to_json($plants);
	}

	public function ajax_get_product_by_plant(){
		$plant_ids = (NULL==$this->input->get('plant_ids'))? [0]:$this->input->get('plant_ids');

		$c_product = $this->c_product->datalist_multi(NULL, $plant_ids, NULL);

        // Make sure we deliver unique product
        $exists_product= [];
        $c_product = array_filter(
            $c_product,
            function($product) use (&$exists_product) {
                if (in_array($product->ID_PRODUCT, $exists_product)) {
                    return false;
                }

                $exists_product[] = $product->ID_PRODUCT;

                return true;
            }
        );

        if ( $c_product ) {
            // reindex the key
            $c_product = array_combine(range(0, count($c_product)-1), $c_product);
        }
    
		to_json($c_product);
    }
    
    // start create izza jan 2021
    public function ajax_get_product_by_plant_read(){
		$plant_ids = (NULL==$this->input->get('plant_ids'))? [0]:$this->input->get('plant_ids');

		$c_product = $this->c_product->datalist_multi_read(NULL, $plant_ids, NULL); // izza

        // Make sure we deliver unique product
        $exists_product= [];
        $c_product = array_filter(
            $c_product,
            function($product) use (&$exists_product) {
                if (in_array($product->ID_PRODUCT, $exists_product)) {
                    return false;
                }

                $exists_product[] = $product->ID_PRODUCT;

                return true;
            }
        );

        if ( $c_product ) {
            // reindex the key
            $c_product = array_combine(range(0, count($c_product)-1), $c_product);
        }
    
		to_json($c_product);
    }
    // end create izza jan 2021

	public function ajax_get_component_by_product(){
		$product_ids = (null !== $this->input->get('product_ids')) ? $this->input->get('product_ids'):array(0);
		$plant_ids = (null !== $this->input->get('plant_ids')) ? $this->input->get('plant_ids'):array(0);

        // begin grab the grouparea
        $areas = $this->m_area->datalist_multi(NULL, $plant_ids, NULL);
        $grouparea_ids = array_map(
            function($area) {
                return $area->ID_GROUPAREA;
            },
            $areas
        );

        $grouparea_ids = array_unique($grouparea_ids);

        $c_parameter = [];
        $c_qaf_component = [];
        $qaf_groups = [];
		if (FALSE !== $idx = array_search("1", $grouparea_ids)) {
            $qaf_groups[] = "1";
            array_splice($grouparea_ids, $idx, 1);
        }

		if (FALSE !== $idx = array_search("4", $grouparea_ids)) {
            $qaf_groups[] = "4";
            array_splice($grouparea_ids, $idx, 1);
        }


        if ( $qaf_groups && $plant_ids ) {
            $c_qaf_component = $this->c_qaf_component->or_where($plant_ids, $qaf_groups);
        }

        if ( $grouparea_ids && $plant_ids ) {
            $c_parameter = $this->c_parameter->or_where($plant_ids, $grouparea_ids, "D");
        }

        // merge from both group area
        $components = array_merge($c_qaf_component, $c_parameter);

        // then, distinct it
        // 1. sort by ID component
        usort($components, function($a, $b) {
            return ($a->ID_COMPONENT < $b->ID_COMPONENT) ? 
                -1 : ( $a->ID_COMPONENT == $b->ID_COMPONENT ? 0 : 1 );
        });
        // 2. make unique
        $temp_stack = [];
        $components = array_filter(
            $components,
            function($comp) use (&$temp_stack) {
                if (in_array($comp->ID_COMPONENT, $temp_stack)) {
                    return false;
                }

                $temp_stack[] = $comp->ID_COMPONENT;
                return true;
            }
        );
        // 3. reindex the keys when possible
        if (count($components) > 0) {
            $components = array_combine(range(0, count($components) - 1), $components);
        }
        // 4. done


        to_json($components);
	}

	public function generate_graph(){
		$month_start = $this->input->post('month_start');
		$year_start = $this->input->post('year_start');
		$month_end = $this->input->post('month_end');
    	$year_end = $this->input->post('year_end');

        
        $month_start = str_pad($month_start, 2, '0', STR_PAD_LEFT);
        $month_end = str_pad($month_end, 2, '0', STR_PAD_LEFT);

        $date_start = date('d/m/Y', strtotime("01-{$month_start}-{$year_start}"));
        $date_end = date('t/m/Y', strtotime("01-{$month_end}-{$year_end}"));

        $plants = $this->input->post('plants');
        $components = $this->input->post('components');
        $product = $this->input->post('products');

        // TODO lakukan validasi disini
        //
         

        
        // $mod   		= ( in_array($grouparea[0], array("1","4")) ) ?  
        // $mod = "t_cement_daily";  // t_production_daily;

        $production = $this->model_getdata($plants, $product, $components, $date_start, $date_end);

        // to_json($production);

        // Karena satu chart menampilkan satu plant, kita kelompokkan disini

		// $parameter	= (in_array($grouparea[0], array("1","4"))) ?  array($product[0],$component):$component;

		// $cmp = array();
		// $plotdata = array();
		// $komponen = "";

		// echo $mod;exit();
		// $production = $this->{$mod}->get_qtrend($date, $area, $parameter, $dh_ly, $grouparea[0]);


		#$temp_prod  = new stdClass();
		#$tmp = array();
		#echo $this->db->last_query();

        /* Struktur yang dibutuhkan untuk tiap grafik
          $data Array of dataset
           [
               x: ['2013-10-04', '2013-11-04', '2013-12-04'],
               y: [1, 3, 6],
               type: 'scatter',
               name: "SiO2"
           ],
           [
               x: ['2013-10-04', '2013-11-04', '2013-12-04'],
               y: [3, 4, 8],
               type: 'scatter',
               name: "KO3",
           ],
           ... other component
          $layout = [
            xaxis: {
              type: "category",
            },
            yaxis: {
              type: "category",
              range: [0, 10]
           }
          ]
          
          in Javascript
          Plotly.newPlot(data, layout)
        */ 
		if($production){

            $data_plants = [];
			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_PLANT);
				$dash  = getDash($prod->ID_PLANT);

				// if (in_array($prod->ID_PLANT, $area)) {
				// 	if (in_array($prod->ID_COMPONENT, $component)) {
                        // $key_component = array_search($prod->ID_COMPONENT, $component);
                        // $key_area = array_search($prod->ID_PLANT, $area);
				// 		$cmp[$component[$key_component]][$key_area]['y'][]  = $prod->AVGNILAI;
				// 		$cmp[$component[$key_component]][$key_area]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
				// 		$cmp[$component[$key_component]][$key_area]['line'] = array('dash' => 'solid','width' => "3", "color" => "rgba($color,1.0)");
				// 		$cmp[$component[$key_component]][$key_area]['type'] = "scatter";
				// 		$cmp[$component[$key_component]][$key_area]['mode'] = "lines+markers";
				// 		$cmp[$component[$key_component]][$key_area]['marker'] = array("symbol" => "square", "size" => 8);
				// 		$cmp[$component[$key_component]][$key_area]['name'] = trim($prod->AREA);
				// 		$cmp[$component[$key_component]][$key_area]['parameter']  = trim($prod->KD_COMPONENT);

				// 	}
				// }
                //
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT][] = $prod;

                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['y'][]  = $prod->AVGNILAI;
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['text'][]  = round($prod->AVGNILAI);
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['x'][]  = $prod->PERIODE;
                // $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['line'] = array('dash' => 'solid','width' => "3", "color" => "rgba($color,1.0)");
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['type'] = "scatter";
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['mode'] = "lines+text";
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['marker'] = array("symbol" => "square", "size" => 8);
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['name'] = trim($prod->NM_COMPONENT);
                $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['chart_title'] = trim($prod->NM_PLANT);
            // $data_plants[$prod->ID_PLANT][$prod->ID_COMPONENT]['parameter']  = trim($prod->KD_prod->ID_PLANTONENT);
			}

			$ab = array();
			$k = 0;
			foreach ($data_plants as $key => $value) {
				$ab[$k]['data'] = array_values($data_plants[$key]);
				$k++;
			}

			

			if (isset($max)) {
				$k = 0;
				foreach ($max as $key => $value) {
					$ck = $max[$key];
					foreach ($ck as $ky => $v) {
						$pk[$ky] = count($ck[$ky]['y']);
					}
					$maks = array_keys($pk, max($pk));
					$ab[$k]['data'][] = $max[$key][$maks[0]];
					$k++;
				}
			}
			
			if (isset($min)) {
				$k = 0;
				foreach ($min as $key => $value) {
					$cl = $min[$key];
					foreach ($cl as $ky => $v) {
						$pl[$ky] = count($cl[$ky]['y']);
					}
					$mins = array_keys($pl, max($pl));
					$ab[$k]['data'][] = $min[$key][$mins[0]];
					$k++;
				}
			}

            log_message('debug', 'struktur pairing output' . json_encode($ab));

            // cari nilai max dan min agar y-range nya seragam (?)
            // $min = NULL;
            // $max = NULL;
            // foreach ($ab as $plant) {
            //     foreach($plant['data'] as $component) {
            //         $tmp_min = min($component['y']);
            //         $tmp_max = max($component['y']);

            //         if (is_null($min) || $tmp_min < $min) {
            //             $min = $tmp_min;
            //         }

            //         if (is_null($max) || $tmp_max > $max) {
            //             $max = $tmp_max;
            //         }
            //     }
            // }
            // $range = [$min, $max]; // min, max

            $min_max_arr = [];
            foreach ($ab as $plant) {
                foreach ($plant['data'] as $value) {
                    foreach ($value['y'] as $y) {
                        array_push($min_max_arr, $y);
                    }
                }
            }
            $range = [min($min_max_arr), max($min_max_arr)];


            // Misalkan, di 
            // - plant A ada data periode: 01/2019, 04/2019, 05/2019, 
            // - plant B ada data periode: 01/2019, 02/2019, 03/2019, 04/2019, 05/2019, 
            // block berikut, akan menambahkan periode yang "hilang" ke plant A
            
            // BEGIN solve missing period
            // seragamkan category di x
            // use dumping mode
            $x_category = [];
            foreach ($ab as $plant) {
                foreach($plant['data'] as $component) {
                    $x_category = array_merge($x_category, $component['x']);
                }
            }

            $x_category = array_unique($x_category);
            sort($x_category);

            // Restructure y value based on new category values.
            // Cara berikut untuk menambahkan x yang "hilang" dengan nilai di 
            // set NULL. 
            // (Sepertinya) Cara ini cocok untuk data component x-y dengan jumlah 
            // sedikit
            foreach ($ab as &$plant) {
                foreach($plant['data'] as &$component) {
                    // cari index dari nilai y, jika ada y yang baru
                    $xy_current = array_combine($component['x'], $component['y']);
                    $xy_new = [];
                    foreach($x_category as $x) {
                        // tambahkan dengan nilai NULL jika xy_pair ada yang 
                        // kurang
                        if (! array_key_exists($x, $xy_current)) {
                            $xy_new[$x] = NULL;
                        }
                    }

                    $xy_current = array_merge($xy_current, $xy_new);

                    // resort based-on x category, ascending
                    ksort($xy_current);

                    // reassign to $component
                    $component['x'] = array_keys($xy_current);
                    $component['y'] = array_values($xy_current);
                }
            }

            // END solve missing period

			//Generate Layout
            // echo "<pre>";
			foreach ($ab as $key => $value) {
                // print_r($value);
				$lay[$key]['layout'] = array(
                    "width" => 500,
					"title" => "Plant " . trim($value['data'][0]['chart_title']),
					"xaxis" => array(
						"title" => "",
						// "titlefont" => array('size' => ($dh_ly=='H') ? 18:14),
						// "tickfont" => array('size' => ($dh_ly=='H') ? 5:10),
						"autorange" => TRUE
					),
                    "yaxis" => array(
                        "title" => 'NILAI',
                        "range" => $range,
                    ),
					"legend" => array(
                        "font" => array(
                            "size" => 10
                        ),
                        "orientation" => "h",
                        // "yanchor"     => "bottom",
                        // "y"           => 1.02,
                        // "xanchor"     => "center",
                        // "x"           => 0.5,
					),
					"showlegend" => TRUE
				);
			}
			$plotdata['msg'] = "200";
		}else{
			$plotdata['msg'] = "404";
		}

		$plotdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$plotdata['layout'] = array_values((empty($lay)) ? array(0): $lay);
		to_json($plotdata);
	}

	private function getColor($num) {
        $hash = md5('warnawarni' . $num);
        return array(hexdec(substr($hash, 0, 2)), hexdec(substr($hash, 2, 2)), hexdec(substr($hash, 4, 2)));
	}

    private function model_getdata($plant_ids, $product_type, $components, $date_from, $date_to) {
// SELECT cp.*, mp.ID_PLANT, mp.NM_PLANT, tpd.DATE_DATA , ma.NM_AREA, mp2.NM_PRODUCT, mc.NM_COMPONENT, dpd.ID_COMPONENT, dpd.NILAI 
        $sql = <<<SQL
SELECT tb2.*, mp3.NM_PRODUCT, mp3.KD_PRODUCT, mc.NM_COMPONENT, mc.KD_COMPONENT, mp4.NM_PLANT, mp4.KD_PLANT
FROM (
    SELECT ID_PLANT, ID_PRODUCT, ID_COMPONENT, PERIODE, COALESCE(AVG(NILAI), NULL) AS AVGNILAI
    FROM (
        SELECT mp.ID_PLANT, tcd.ID_PRODUCT, dcd.ID_COMPONENT, 
            to_char(tcd.DATE_DATA, 'MM/YYYY') AS PERIODE, 
            dcd.NILAI
        FROM C_PRODUCT cp
        JOIN M_AREA ma ON ma.ID_AREA = cp.ID_AREA 
        JOIN M_PLANT mp ON mp.ID_PLANT  = ma.ID_PLANT 
        LEFT JOIN T_CEMENT_DAILY tcd ON tcd.ID_AREA = cp.ID_AREA AND tcd.ID_PRODUCT = cp.ID_PRODUCT 
        LEFT JOIN D_CEMENT_DAILY dcd ON dcd.ID_CEMENT_DAILY  = tcd.ID_CEMENT_DAILY 
        WHERE 
            mp.ID_PLANT in ?
            AND cp.ID_PRODUCT in ?
            AND dcd.ID_COMPONENT in ?
            AND tcd.DATE_DATA BETWEEN to_date(?, 'DD/MM/YYYY') AND to_date(?, 'DD/MM/YYYY')

/*
        UNION ALL

        SELECT mp.ID_PLANT, cp.ID_PRODUCT, dpd.ID_COMPONENT, 
            to_char(tpd.DATE_DATA, 'MM/YYYY') AS PERIODE, 
            dpd.NILAI
        FROM C_PRODUCT cp
        JOIN M_AREA ma ON ma.ID_AREA = cp.ID_AREA 
        JOIN M_PLANT mp ON mp.ID_PLANT  = ma.ID_PLANT 
        LEFT JOIN T_PRODUCTION_DAILY tpd ON tpd.ID_AREA = cp.ID_AREA 
        LEFT JOIN D_PRODUCTION_DAILY dpd ON dpd.ID_PRODUCTION_DAILY  = tpd.ID_PRODUCTION_DAILY 
        WHERE 
            mp.ID_PLANT in ?
            AND cp.ID_PRODUCT in ?
            AND dpd.ID_COMPONENT in ?
            AND tpd.DATE_DATA BETWEEN to_date(?, 'DD/MM/YYYY') AND to_date(?, 'DD/MM/YYYY')
*/
    ) tb
    GROUP BY 
        tb.ID_PLANT, 
        tb.ID_PRODUCT, 
        tb.ID_COMPONENT, 
        tb.PERIODE
) tb2
LEFT JOIN M_COMPONENT mc ON mc.ID_COMPONENT = tb2.ID_COMPONENT
LEFT JOIN M_PRODUCT mp3 ON mp3.ID_PRODUCT = tb2.ID_PRODUCT
LEFT JOIN M_PLANT mp4 ON mp4.ID_PLANT = tb2.ID_PLANT
ORDER BY 
    tb2.ID_PLANT, 
    tb2.ID_PRODUCT,
    tb2.ID_COMPONENT
SQL;

        $params = [
            $plant_ids, $product_type, $components, $date_from, $date_to,
            $plant_ids, $product_type, $components, $date_from, $date_to,
        ];

        $query = $this->db->query($sql, $params);

        log_message('debug', "pairing product SQL " . $this->db->last_query());

        return $query->result();
    }

}

/* End of file Quality_trend.php */
/* Location: ./application/controllers/Quality_trend.php */
?>
