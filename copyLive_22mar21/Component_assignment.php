<?php

class Component_assignment extends QMUser {
	
	public $list_config = array();
	public $list_plant = array();
	public $list_company = array();
	public $list_grouparea = array();
	public $list_component = array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $ID_GROUPAREA;
	public $DISPLAY;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("c_parameter");
		$this->load->model("m_company");
		$this->load->model("m_plant");
	}
	
	public function index(){
		$this->ID_COMPANY 	= $this->input->get("id_company");
		$this->ID_PLANT   	= $this->input->get("id_plan");
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist();
		$this->template->adminlte("v_parameter");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function create(){ 
		
		if($this->input->post()){
			$ID_PLANT 		= $this->input->post("ID_PLANT");
			$ID_GROUPAREA 	= $this->input->post("ID_GROUPAREA");
			$DISPLAY 		= $this->input->post("DISPLAY");
			$ID_COMPANY 	= $this->input->post("ID_COMPANY");
			
			if ($ID_COMPANY == 7) {
				if($ID_GROUPAREA){	
					$this->load->model("c_parameter_order");
					$ID_PLANT = 'SMIG';

					//clean
					$this->c_parameter_order->clean($ID_GROUPAREA,$this->input->post("OPT_COMPONENT"),$DISPLAY);
					#echo $this->db->last_query();die();
					//Reinsert
					foreach($this->input->post("OPT_COMPONENT") as $ID_COMPONENT){
						$data = false;
						$data['ID_GROUPAREA'] 	= $ID_GROUPAREA;
						$data['ID_COMPONENT'] 	= $ID_COMPONENT;
						$data['DISPLAY'] 		= $DISPLAY;

						$cek = $this->c_parameter_order->get_where($data);
						if (!$cek) {
							@$this->c_parameter_order->insert($data);
						}
						
					}
				}
			}else{
				if($ID_GROUPAREA){
					//clean
					$this->c_parameter->clean($ID_PLANT,$ID_GROUPAREA,$this->input->post("OPT_COMPONENT"),$DISPLAY);
					
					foreach($this->input->post("OPT_COMPONENT") as $ID_COMPONENT){
						$data = false;
						$data['ID_PLANT'] 		= $ID_PLANT;
						$data['ID_GROUPAREA'] 	= $ID_GROUPAREA;
						$data['ID_COMPONENT'] 	= $ID_COMPONENT;
						$data['DISPLAY'] 		= $DISPLAY;
						// @$this->c_parameter->insert($data);
						
						$cek = $this->c_parameter->get_where($data);
						if (!$cek) {
							@$this->c_parameter->insert($data);
						}
					}
					
					
				}
			}
			
			$this->notice->success("Component Assigned Success");
		}
		redirect("component_assignment/edit/".$ID_PLANT."/".$ID_GROUPAREA."/".$DISPLAY);
	}
	
	public function edit($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){ #die("edit");
		$this->load->model("m_component");
		$this->list_company = $this->m_company->datalist();
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->list_component = $this->m_component->datalist();
		
		if ($ID_PLANT == 'SMIG') {
			$this->load->model("m_grouparea");
			$this->ID_COMPANY = 7;
			$this->list_plant = NULL;
			$this->list_grouparea = $this->m_grouparea->datalist();
		}else{
			$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
			$this->ID_PLANT = $ID_PLANT;
			$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
			$this->list_plant 	= $this->m_plant->datalist($this->data_plant->ID_COMPANY);
			$this->list_grouparea = $this->c_parameter->list_grouparea($ID_PLANT);			
		}
		#echo $ID_PLANT;
		$this->template->adminlte("v_parameter_edit");
	}
	
	public function view($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){
		$this->load->model("m_component");
		$this->list_company = $this->m_company->datalist();
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->list_component = $this->m_component->datalist();
		if ($ID_PLANT == 'SMIG') {
			$this->load->model("m_grouparea");
			$this->ID_COMPANY = 7;
			$this->list_plant = NULL;
			$this->list_grouparea = $this->m_grouparea->datalist();
		}else{
			$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
			$this->ID_PLANT = $ID_PLANT;
			$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
			$this->list_plant = $this->m_plant->datalist($this->data_plant->ID_COMPANY);
			$this->list_grouparea = $this->c_parameter->list_grouparea($ID_PLANT);
		}
		
		$this->template->adminlte("v_parameter_view");
	}
	
	// ajax
	public function list_config_grouparea($ID_PLANT=NULL){
		if ($ID_PLANT=='SMIG') {
			$this->load->model("m_grouparea");
			$this->list_company = $this->m_company->get_data_by_id(7);
			$this->list_config = $this->m_grouparea->datalist();
			$this->template->ajax_response("v_config_grouparea_global");
		}else{
			$this->list_config = $this->c_parameter->list_config($ID_PLANT); 
			$this->template->ajax_response("v_config_grouparea");
		}
	}
	
	public function async_list_grouparea($ID_PLANT=NULL){
		$data = $this->c_parameter->list_grouparea($ID_PLANT);
		echo json_encode($data);
	}
	
	public function async_configuration($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY='DAILY'){
		if ($ID_PLANT=='SMIG') {
			$this->load->model("c_parameter_order");
			$data = $this->c_parameter_order->get_component($ID_GROUPAREA,$DISPLAY);
		}else{
			$data = $this->c_parameter->configuration($ID_PLANT,$ID_GROUPAREA,$DISPLAY);# echo $this->c_parameter->get_sql();
		}
		echo json_encode($data);
	}
}

