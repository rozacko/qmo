<?php

class Groupproduct extends QMUser {
	
	public $list_data = array();
	public $data_grouparea;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_groupproduct");
    }

    public function index(){
		$this->template->adminlte("v_groupproduct");
    }

    public function add(){
		$this->template->adminlte("v_groupproduct_add");
    }
    
    public function create(){
		$this->m_groupproduct->insert($this->input->post());
		if($this->m_groupproduct->error()){
			$this->notice->error($this->m_groupproduct->error());
			redirect("groupproduct/add");
		}
		else{
			$this->notice->success("Group Product Data Saved.");
			redirect("groupproduct");
		}
    }

    public function update($ID_GROUPPRODUCT){
		$this->m_groupproduct->update($this->input->post(),$ID_GROUPPRODUCT);
		if($this->m_groupproduct->error()){
			$this->notice->error($this->m_groupproduct->error());
			redirect("groupproduct/edit/".$ID_GROUPPRODUCT);
		}
		else{
			$this->notice->success("Group Product Data Updated.");
			redirect("groupproduct");
		}
    }
    
    public function delete($ID_GROUPPRODUCT){
		$this->m_groupproduct->delete($ID_GROUPPRODUCT);
		if($this->m_groupproduct->error()){
			$this->notice->error($this->m_groupproduct->error());
		}
		else{
			$this->notice->success("Group Product Data Removed.");
		}
		redirect("groupproduct");
	}
    
    public function edit($ID_GROUPPRODUCT){
		$this->data_groupproduct = $this->m_groupproduct->get_data_by_id($ID_GROUPPRODUCT);
		$this->template->adminlte("v_groupproduct_edit");
	}
    
    public function get_list(){
		$list = $this->m_groupproduct->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_GROUPPRODUCT;
			$row[] = $no;
			$row[] = $column->KD_GROUPPRODUCT;
			$row[] = $column->NM_GROUPPRODUCT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_groupproduct->count_all(),
            "recordsFiltered" => $this->m_groupproduct->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
}