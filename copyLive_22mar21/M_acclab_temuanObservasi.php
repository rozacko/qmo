<?php

class M_acclab_temuanObservasi Extends DB_QM {

    protected $detailForm = "QMLAB_DETAILFORMOBSERVASI", $form = "QMLAB_FORMOBSERVASI", $temuan = "QMLAB_TEMUANOBSERVASI", $obsin = "QMLAB_OBSERVASIINPUT", $setup = "QMLAB_SETUP";
    protected $uiForm = "";
	
	public function data_lab(){
		$this->db->order_by("NAMA_LAB");
		$this->db->where("DELETED IS NULL");
		return $this->db->get("M_LABORATORIUM")->result();
	}
	
	public function get_list($id_pp = false){
		$this->db->select("TP.FK_ID_PERIODE, TP.ID, TP.JENIS, TP.LAB, TP.PIC_OBSERVASI, TP.PIC_PLANT, TP.START_DATE, TP.END_DATE, TP.STATUS, ML.NAMA_LAB, MU.FULLNAME as FULLNAME_OBSERVASI, MU2.FULLNAME as FULLNAME_PLANT, TPP.NAMA, TPP.TAHUN");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.PIC_OBSERVASI","LEFT");
		$this->db->join("M_USERS MU2","MU2.ID_USER = TP.PIC_PLANT","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.LAB","LEFT");
		$this->db->join("QMLAB_PERIODE TPP","TPP.ID = TP.FK_ID_PERIODE","LEFT");
		$this->db->where("TP.DELETE_BY IS NULL");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->WHERE("TP.JENIS = 'OBSERVASI'");
		if($id_pp != null){
			$this->db->where("TP.FK_ID_PERIODE", $id_pp);
		}
		$this->db->order_by("TP.FK_ID_PERIODE");
		$this->db->order_by("TPP.TAHUN","DESC");
		$this->db->order_by("TP.CREATE_DATE","DESC");
//        $this->db->group_by("TP.FK_ID_PERIODE, TP.ID, TP.JENIS, TP.LAB, TP.PIC_OBSERVASI, TP.PIC_PLANT, TP.START_DATE, TP.END_DATE, TP.STATUS, ML.NAMA_LAB, MU.FULLNAME as FULLNAME_OBSERVASI, MU2.FULLNAME as FULLNAME_PLANT, TPP.NAMA, TPP.TAHUN");
		$query = $this->db->get('QMLAB_SETUP TP');
		return $query->result();
	}

	public function get_tindakLanjutList($id_pp = false) {
        $this->db->select("TOB.FK_ID_PELAKSANAAN, TOB.ID, TP.JENIS, TP.LAB, TP.PIC_OBSERVASI, TP.PIC_PLANT, TP.START_DATE, TP.END_DATE, TP.STATUS, ML.NAMA_LAB, MU.FULLNAME as FULLNAME_OBSERVASI, MU2.FULLNAME as FULLNAME_PLANT, TPP.NAMA, TPP.TAHUN, TOB.REKOMENDASI");
        $this->db->join("QMLAB_SETUP TP","TOB.FK_ID_PELAKSANAAN = TP.ID","LEFT");
        $this->db->join("QMLAB_PERIODE TPP","TPP.ID = TP.FK_ID_PERIODE","LEFT");
        $this->db->join("M_USERS MU","MU.ID_USER = TP.PIC_OBSERVASI","LEFT");
        $this->db->join("M_USERS MU2","MU2.ID_USER = TP.PIC_PLANT","LEFT");
        $this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.LAB","LEFT");
        $this->db->where("TOB.DELETE_BY IS NULL");
        $this->db->where("TP.DELETE_BY IS NULL");
        $this->db->where("TPP.DELETE_BY IS NULL");
        $this->db->WHERE("TP.JENIS = 'OBSERVASI'");
        if($id_pp != null){
            $this->db->where("TP.FK_ID_PERIODE", $id_pp);
        }
        $this->db->order_by("TOB.FK_ID_PELAKSANAAN");
        $this->db->order_by("TPP.TAHUN","DESC");
        $this->db->order_by("TP.CREATE_DATE","DESC");
//        $this->db->group_by("TP.FK_ID_PERIODE, TP.ID, TP.JENIS, TP.LAB, TP.PIC_OBSERVASI, TP.PIC_PLANT, TP.START_DATE, TP.END_DATE, TP.STATUS, ML.NAMA_LAB, MU.FULLNAME as FULLNAME_OBSERVASI, MU2.FULLNAME as FULLNAME_PLANT, TPP.NAMA, TPP.TAHUN");
        $query = $this->db->get('QMLAB_TEMUANOBSERVASI TOB');
        return $query->result();
    }

	public function get_masterForm() {
        $this->db->select("QF.*, PR.NAMA_KATEGORI AS PARENT_NAME");
        $this->db->join("(SELECT * FROM {$this->form}) PR", "QF.PARENT_ID = PR.ID", "LEFT");
        $this->db->where("QF.DELETE_BY IS NULL");
        $this->db->where("PR.DELETE_BY IS NULL");
        $this->db->order_by("QF.KODE, QF.ID");

        $sql = $this->db->get("{$this->form} QF");
        return $sql->result();
    }

    public function get_allTag() {
	    $this->db->where("JENIS_KATEGORI", "NAV");
	    $this->db->where("DELETE_BY IS NULL");
	    $this->db->order_by("KODE");
	    $query = $this->db->get($this->form);
	    return $query->result();
    }
    
    public function get_detailHasil($data) {
	    $this->db->where("FK_ID_FIELD", $data["idField"]);
	    $this->db->where("FK_ID_TEMUAN", $data["idTemuan"]);
	    $this->db->where("DELETE_BY IS NULL");
	    $sql = $this->db->get($this->obsin);

	    $res = array(
	        "status" => true,
            "msg" => "Get Data Success",
            "data" => $sql->row_array()
        );

	    return $res;
    }

    public function get_detailForm($id) {
	    $this->db->select('*');
	    $this->db->where("FK_ID_FORM", $id);
        $this->db->where("DELETE_BY IS NULL");
	    $query = $this->db->get("QMLAB_DETAILFORMOBSERVASI");
	    return $query->result();
    }

    public function get_komponen($level = 1, $parent = 0) {
	    $uiForm = "";
        $this->db->where("PARENT_ID", $parent);
        $this->db->where("DELETE_BY IS NULL");
        $forms = $this->db->get($this->form)->result_array();

        if ($forms[0]["JENIS_KATEGORI"] == "NAV") {

            $uiForm .= '<div class="container">';
            //Nav Start
            $uiForm .= '<div id="exTab' . $level .'" class="container">';
            $uiForm .= '<ul class="nav nav-tabs">';
            foreach ($forms as $form) {
                $uiForm .= '<li class="">';
                $uiForm .= '<a  href="#content' . $form["ID"] . '" data-toggle="tab">' . $form["KODE"] .' - ' . $form["NAMA_KATEGORI"] . '</a>';
                $uiForm .= '</li>';
            }
            $uiForm .= '</ul>';
            $uiForm .='</div>';
            //Nav End

            //Tab Content Start
            $uiForm .= '<div class="tab-content">';
            foreach ($forms as $form) {
                //Check if there any child with this parent
                $this->db->where("PARENT_ID", $form["ID"]);
                $this->db->where("DELETE_BY IS NULL");
                $childs = $this->db->get($this->form)->result_array();

                $uiForm .= '<div class="tab-pane" id="content' . $form["ID"] . '">';
                //Child Content Start
                if (count($childs) > 0) {
                    $child = $this->get_komponen($childs[0]["LEVEL"], $form["ID"]);
                    $uiForm .= $child;
                }
                //Child Content End
                $uiForm .= '</div>';
            }
            $uiForm .= '</div>';
            //Tab Content End
            $uiForm .= '</div>';
        } else {
            foreach ($forms as $form) {
                $uiForm .= '<div class="form-group row">';
                $uiForm .= '<div class="col-sm-12">';
                $uiForm .= '<label>' . $form["NAMA_KATEGORI"] . '</label>';
                $uiForm .= '<input type="text" class="form-control fieldTemuan" id="field_' . $form["ID"] . '" name="field_' . $form["ID"] . '" placeholder="' . $form["NAMA_FIELD"] . '"  >';
                $uiForm .= '</div>';
                $uiForm .= '</div>';
            }
        }
        return $uiForm;
    }

    public function get_komponenTindak($level = 1, $parent = 0) {
        $uiForm = "";
        $this->db->where("PARENT_ID", $parent);
        $this->db->where("DELETE_BY IS NULL");
        $forms = $this->db->get($this->form)->result_array();

        if ($forms[0]["JENIS_KATEGORI"] == "NAV") {

            $uiForm .= '<div class="container">';
            //Nav Start
            $uiForm .= '<div id="exTab' . $level .'" class="container">';
            $uiForm .= '<ul class="nav nav-tabs">';
            foreach ($forms as $form) {
                $uiForm .= '<li class="">';
                $uiForm .= '<a  href="#content' . $form["ID"] . '" data-toggle="tab">' . $form["KODE"] .' - ' . $form["NAMA_KATEGORI"] . '</a>';
                $uiForm .= '</li>';
            }
            $uiForm .= '</ul>';
            $uiForm .='</div>';
            //Nav End

            //Tab Content Start
            $uiForm .= '<div class="tab-content">';
            foreach ($forms as $form) {
                //Check if there any child with this parent
                $this->db->where("PARENT_ID", $form["ID"]);
                $this->db->where("DELETE_BY IS NULL");
                $childs = $this->db->get($this->form)->result_array();

                $uiForm .= '<div class="tab-pane" id="content' . $form["ID"] . '">';
                //Child Content Start
                if (count($childs) > 0) {
                    $child = $this->get_komponenTindak($childs[0]["LEVEL"], $form["ID"]);
                    $uiForm .= $child;
                }
                //Child Content End
                $uiForm .= '</div>';
            }
            $uiForm .= '</div>';
            //Tab Content End
            $uiForm .= '</div>';
        } else {
            foreach ($forms as $form) {
                $uiForm .= '<div class="form-group row">';
                $uiForm .= '<div class="col-sm-6">';
                $uiForm .= '<label>' . $form["NAMA_KATEGORI"] . '</label>';
                $uiForm .= '<input type="text" disabled class="form-control fieldTemuan" id="field_' . $form["ID"] . '" name="field_' . $form["ID"] . '" placeholder="' . $form["NAMA_FIELD"] . '"  >';
                $uiForm .= '<a onclick="openModal(' . $form["ID"] . ');"><button title="Detail" class="btEdit  btn btn-warning btn-xs" type="button"><i class="fa fa-edit "></i>Tindak Lanjut</button></a>';
                $uiForm .= '</div>';
                $uiForm .= '</div>';
            }
        }
        return $uiForm;
    }

    public function get_currentTemuan($temuanID) {
	    $this->db->where("FK_ID_TEMUAN", $temuanID);
	    $this->db->where("DELETE_BY IS NULL");
	    $sql = $this->db->get($this->obsin);

	    $data = array(
	        "status" => true,
            "msg" => "Get Data Temuan Success",
            "data" => $sql->result_array()
        );
	    return $data;
    }

    public function get_formTindakLanjut() {
        $form = $this->get_komponenTindak(1, 0);
        $data = array(
            "status" => true,
            "msg" => "Get Form Success",
            "data" => $form
        );
        return $data;
    }
    
    public function get_inputForm() {
	    $form = $this->get_komponen(1, 0);
	    $data = array(
	        "status" => true,
            "msg" => "Get Form Success",
            "data" => $form
        );
	    return $data;
    }
    
    public function saveField() {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");
        $field = $this->input->post("nama_field");
        $fk = $this->input->post("fk");

        $sql = "INSERT INTO {$this->detailForm} (FK_ID_FORM, NAMA_FIELD, CREATE_DATE, CREATE_BY) VALUES ({$fk}, '{$field}', TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')";

        return $this->db->query($sql);
    }

    public function saveForm() {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");
        $form = $this->input->post("NAMA_FORM");
        $kode = $this->input->post("KODE");
        $jenis = $this->input->post("JENIS");
        $parent = $this->input->post("ID_PARENT");
        $level = ($parent != 0) ? $this->getCurrentLevel($parent) : 1;

        $sql = "INSERT INTO {$this->form} (KODE, PARENT_ID, NAMA_KATEGORI, JENIS_KATEGORI, LEVEL_KATEGORI, CREATE_DATE, CREATE_BY) VALUES ('{$kode}', {$parent}, '{$form}', '{$jenis}', {$level}, TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')";

        return $this->db->query($sql);
    }
    
    public function saveTindakan($data) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");

        $this->updateSetupStatus($data["idPelaksana"], "TINDAK_LANJUT");
        $sql = "UPDATE {$this->obsin} SET OBSERVATION_RESULT = '{$data["result"]}', COMPLIANCE = '{$data["compliance"]}', UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID = {$data["idObsin"]}";

        return $this->db->query($sql);
    }

    public function getCurrentLevel($parentID) {
	    $level = 1;
	    $this->db->where("ID", $parentID);
	    $data = $this->db->get($this->form)->row_array();

	    return $data["LEVEL"] + 1;
    }

    public function updateField() {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");



        $sql = "UPDATE {$this->form} SET NAMA_KATEGORI = '{$field}', UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}";

        return $this->db->query($sql);
    }

    public function updateForm() {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");
        $field = $this->input->post("NAMA_FORM");
        $id = $this->input->post("ID");
        $parent = $this->input->post("ID_PARENT");
        $kode = $this->input->post("KODE");
        $jenis = $this->input->post("JENIS");
        $level = ($parent != 0) ? $this->getCurrentLevel($parent) : 1;

        $sql = "UPDATE {$this->form} SET NAMA_KATEGORI = '{$field}', LEVEL_KATEGORI = {$level}, UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}";

        return $this->db->query($sql);
    }

    public function deleteField($id) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");

        $sql = "UPDATE {$this->detailForm} SET DELETE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), DELETE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}";

        return $this->db->query($sql);
    }

    public function deleteForm($id) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d", strtotime($date_now));

        $sql = "UPDATE {$this->form} SET DELETE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), DELETE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}";

        return $this->db->query($sql);
    }

    public function doEditTemuan($data) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");
        $fail = 0;

        $idPelaksana = (int)$data["idPelaksana"];
        $rekomendasi = $data["rekomendasi"];
        $idTemuan = (int)$data["idTemuan"];

        if ($idTemuan !== 0) {
            $fields = $data["field_list"];

            $this->db->trans_start();

            foreach ($fields as $field) {
                $content = $field["val"];
                $id = (int)str_replace("field_", "", $field["id"]);

                $sql = "UPDATE {$this->obsin} SET CONTENT = '{$content}', UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE FK_ID_TEMUAN = {$idTemuan} AND FK_ID_FIELD = {$id}";
                $this->db->query($sql);
            }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $fail = 1;
            } else {
                $this->db->trans_commit();
            }

        } else {
            $fail = 1;
        }

        $response = array(
            'status' => ($fail == 0) ? true : false,
            'msg' => ($fail == 0) ? 'Submit Data Success' : 'Something went wrong',
            'data' => $data
        );

        return $response;
    }

    public function doSubmitTemuan($data) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");
        $fail = 0;

        $idPelaksana = (int)$data["idPelaksana"];
        $rekomendasi = $data["rekomendasi"];
        $idTemuan = $this->createNewTemuan($idPelaksana, $rekomendasi);

        if ($idTemuan !== 0) {
            $fields = $data["field_list"];

            $this->db->trans_start();

            foreach ($fields as $field) {
                $content = $field["val"];
                $id = (int)str_replace("field_", "", $field["id"]);

                $sql = "INSERT INTO {$this->obsin} (FK_ID_TEMUAN, FK_ID_FIELD, CONTENT, CREATE_DATE, CREATE_BY) VALUES ({$idTemuan}, {$id}, '{$content}', TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')";
                $this->db->query($sql);
            }

            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $fail = 1;
            } else {
                $this->db->trans_commit();
                $this->updateSetupStatus($idPelaksana, "ISI_TEMUAN");
            }

        } else {
            $fail = 1;
        }

        $response = array(
            'status' => ($fail == 0) ? true : false,
            'msg' => ($fail == 0) ? 'Submit Data Success' : 'Something went wrong',
            'data' => $data
        );

        return $response;
    }

    public function updateSetupStatus($idPelaksana, $status) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");

        $sql = "UPDATE {$this->setup} SET STATUS = '{$status}', UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID = {$idPelaksana}";

        return $this->db->query($sql);
    }

    public function getAvaibleField() {
	    $this->db->where("DELETE_BY IS NULL");
	    $data = $this->db->get($this->detailForm)->result_array();

	    return $data;
    }

    public function createNewTemuan($idPelaksana, $rekom) {
        $sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
        $date_in 	= date("Y-m-d H:i:s");

        $sql = "INSERT INTO {$this->temuan} (FK_ID_PELAKSANAAN, CREATE_DATE, CREATE_BY, REKOMENDASI) VALUES ({$idPelaksana}, TO_DATE('{$date_in}', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}', '{$rekom}')";
        $create = $this->db->query($sql);
        $create_id = 0;

        if ($create) {
            $last_ins = $this->db->query("SELECT * FROM {$this->temuan} ORDER BY ID DESC")->row_array();
            $create_id = $last_ins["ID"];
        }

        return $create_id;
    }
}

?>