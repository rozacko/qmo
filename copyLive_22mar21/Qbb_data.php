<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	use SAPNWRFC\Connection as SapConnection;
	use SAPNWRFC\Exception as SapException;
		
class Qbb_data extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_qbb_data","qbb");
	}
	
	public function index(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		
		if(isset($_POST["start_load"])){
			$a = $_POST["start_load"];
			$b = $_POST["end_load"];
			
			$start  = date('Y-m-d', strtotime($a));
			$end	= date('Y-m-d', strtotime($b));
			
			$this->start_data = $start;
			$this->end_data = $end;
		} else { 
			$today = date("Y-m-d");  
			$start 	= date('Y-m-d', strtotime('-1 month')); //date('Y')."-".."-".date("d");
			$end	= date("Y-m-d");	
			$this->start_data = $start;
			$this->end_data = $end;
		}
		// echo $start. " - ";
		// echo $end;
		// exit();
		$this->list_data = $this->qbb->get_data_periode($start, $end);
		
		$this->template->adminlte("v_qbb_data");
    }
	
	public function data_sap_qbb(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		$start  = $_POST["start_sync"];
		$end	= $_POST["end_sync"];
		$date_start 	= date('Ymd', strtotime($start));
		$date_end 	= date('Ymd', strtotime($end));
		
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_prod = [
			'ashost' => '10.15.5.13',
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];

        try {
            $sapi = new SapConnection($config_prod);
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
            if ($rfc == TRUE) {
                $code_mat = '112-100-0009';
				$options = [
					'rtrim' => true
				];
				$rfc->setParameterActive('R_MATNR', true);
				$rfc->setParameterActive('R_PERIOD', true);
				$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' 	=> [
								[
									'SIGN' 	=> 'I',
									'OPTION'=> 'BT',
									'LOW' 	=> $date_start,
									'HIGH' 	=> $date_end
								]
							]
						];
                $result = $rfc->invoke($parms, $options);

				$fromsap = count($result['R_RESULT']);
				$action = $this->qbb->data_from_sap($result['R_RESULT']);
				$existing = $fromsap-$action;
				
				$this->notice->success("Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing);
            }
        } catch(SapException $ex) {
            $error = 'Exception: ' . $ex->getMessage() . PHP_EOL;
			$this->notice->error($error);
        }
		redirect("qbb_data");
    }
	
	public function info_server(){
		phpinfo();
	}
	
}

?>