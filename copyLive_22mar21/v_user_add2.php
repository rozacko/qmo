<style type="text/css">
  .select2-results__option .wrap:before{
    font-family:fontAwesome;
    color:#999;
    content:"\f096";
    width:25px;
    height:25px;
    padding-right: 10px;
    
  }
  .select2-results__option[aria-selected=true] .wrap:before{
      content:"\f14a";
  }

  /* not required css */

  .row
  {
    padding: 10px;
  }

  .select2-multiple, .select2-multiple2
  {
    width: 50%
  }
</style>

   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      	Master Data User
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
       <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Add </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="bodyContent">


            	<form role="form" method="POST" action="<?php echo site_url("user/createProcess")?>" >
		          <div class="box-body" style="">
		            <div class="form-group">
		                <label>FULLNAME </label>
		                <input type="text" class="form-control" id="FULLNAME" name="FULLNAME" placeholder="Full Name" value="<?php echo $this->data_user->FULLNAME ?>" />
		            </div>
                <div class="form-group">
                    <label>USERNAME </label>
                    <input type="text" class="form-control" id="USERNAME" name="USERNAME" placeholder="User Name"  value="<?php echo $this->data_user->USERNAME ?>">
                </div>
		            <div class="form-group">
		                <label>EMAIL </label>
		                <input type="text" class="form-control" id="EMAIL" name="EMAIL" placeholder="User Name"  value="<?php echo $this->data_user->USERNAME ?>">
		            </div>
		            <?php
                // echo $this->adm;
                // var_dump($this->list_usergroup);
                ?>
		            <div class="form-group">
		                <label>USER GROUP </label>
                    <input type="checkbox" id="checkbox" title="Select All" class="pull-right">
		                <select multiple="multiple" class="form-control select2"  style="width: 100%" id="ID_USERGROUP" NAME="ID_USERGROUP" >
                      <?php
                      foreach ($this->list_usergroup as $i => $v) {
                        if($this->adm){
                          echo '<option value="'.$v->ID_USERGROUP.'">'.$v->NM_USERGROUP.'</option>';
                        }else{
                          if($v->ID_USERGROUP != 1){
                            echo '<option value="'.$v->ID_USERGROUP.'">'.$v->NM_USERGROUP.'</option>';
                          }
                        }
                      }
                      ?>
		                </select>
		            </div>
		            
		            <div class="form-group" id="div_company">
		                <label>COMPANY </label>
		                <select class="form-control select2" style="width: 100%" name="ID_COMPANY" id="ID_COMPANY">
						          <option value="">Choose Company...</option>
                        <?php 
                        print_r($this->list_company);
                        foreach ($this->list_company as $i => $v) {
                          if($this->data_user->ID_COMPANY){
                            if($this->data_user->ID_COMPANY == $v->ID_COMPANY){
                              echo '<option selected kd_company="'.$v->KD_COMPANY.'" value="'.$v->ID_COMPANY.'">'.$v->NM_COMPANY.'</option>';
                            }
                          }else{
                            echo '<option kd_company="'.$v->KD_COMPANY.'" value="'.$v->ID_COMPANY.'">'.$v->NM_COMPANY.'</option>';
                          }
                        }
                        ?>
		                </select>
		            </div>
		            
		            <div class="form-group" id="div_plant">
		                <label>PLANT </label>
		                <select class="form-control select2" style="width: 100%" NAME="ID_PLANT" id="ID_PLANT">
							<option value=""></option>
		                </select>
		            </div>
		            
		            <div class="form-group" id="div_area" >
		                <label>AREA</label>
		                <select class="form-control select2" style="width: 100%" NAME="ID_AREA" id="ID_AREA">
        							<option value=""></option>
        						</select>
		            </div>
                <div class="form-group" id="div_jobgrup" >
                    <label>Job Grup</label>                   
                    <select class="form-control select" style="width: 100%" name="FLAG_GRUP" id="FLAG_GRUP">
                      <option value="">Choose Job Group...</option>
                      <?php  
                      
                      foreach($this->list_jobgrup as $jg): ?>
                      
		                  <option value="<?php echo $jg[FLAG_GRUP] ?>"><?php echo $jg[NM_JOBGRUP] ?></option>
		                  <?php endforeach; ?>
		                </select>
                </div>
                <div class="form-group" id="div_area" >
                    <label>ACTIVE DIRECTORY</label><br>
                    <input type="radio" name="LDAP" checked="" <?=$this->data_user->LDAP == 'Y' ? 'checked' : '' ?> value="Y"> Active 
                    <input type="radio" name="LDAP" <?=$this->data_user->LDAP == 'N' ? 'checked' : '' ?> value="N"> Non Active 
                </div>



		          </div>
		          <!-- /.box-body -->
		          <div class="box-footer">
		            <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?=base_url('user')?>">
                  <button type="button" class="btn btn-danger">Cancel</button>
                </a>
		          </div>
		        </form>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Additional CSS -->
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"/></script>        
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
    
<script language="javascript" type="text/javascript">

$(function(){

    /* Auto Complete */
  var options = {
    url: function(username) {
      return '<?php echo site_url("user/get_username/") ?>';
    },
    getValue: function(element) {
      return element.mk_nama;
    },
    template: {
      type: "description",
      fields: {
        description: "MK_CCTR_TEXT"
      }
    },
    list: {
      onSelectItemEvent: function() {
        var selectedUsername = $("#FULLNAME").getSelectedItemData().USERNAME;
        var selectedEmail = $("#FULLNAME").getSelectedItemData().mk_email;
        var selectedItemValuComp = $("#FULLNAME").getSelectedItemData().company;

        $("#USERNAME").val(selectedUsername).trigger("change");
        $("#EMAIL").val(selectedEmail).trigger("change");

        var dtCompany = new Array();

          $('#ID_COMPANY > option').each(function() {
            dtCompany.push({id: $(this).val(), text: $(this).text(), code: $(this).attr('kd_company')});
          });
          $('#ID_COMPANY').html('');

          dtOption = '';
          $(dtCompany).each(function(i, v) {
            if(v.code == selectedItemValuComp){
              dtOption += '<option selected kd_company="'+v.code+'" value="'+v.id+'">'+v.text+'</option>';
            }else{
              dtOption += '<option kd_company="'+v.code+'" value="'+v.id+'">'+v.text+'</option>';
            }
          $('#ID_COMPANY').html(dtOption);
        });
          console.log(dtOption);

      }
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },

    //Sending post data
    preparePostData: function(data) {
      data.username = $("#FULLNAME").val();
      return data;
    },
    theme: "plate-dark", //square | round | plate-dark | funky
    requestDelay: 400
  };

  $("#FULLNAME").easyAutocomplete(options);

  $("#checkbox").click(function(){
      if($("#checkbox").is(':checked') ){
          $("#ID_USERGROUP > option").prop("selected","selected");
          $("#ID_USERGROUP").trigger("change");
      }else{
          $("#ID_USERGROUP > option").removeAttr("selected");
           $("#ID_USERGROUP").trigger("change");
       }
  });

// $("#button").click(function(){
//        alert($("#e1").val());
// });
	USER_COMPANY = '<?php echo $this->data_user->ID_COMPANY ?>';
	USER_PLANT = '<?php echo $this->data_user->ID_PLANT ?>';
	USER_AREA = '<?php echo $this->data_user->ID_AREA ?>';

	$('.select2').select2();

	listPlant(USER_COMPANY);
	listArea(USER_PLANT);

  $('#ID_COMPANY').on('change', function(){
    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
  });	
  $('#ID_PLANT').on('change', function(){
    idPlant = $("option:selected", this).val();
    listArea(idPlant);
  });

  function listPlant(idCompany = ''){

      $.ajax({
        url : '<?=base_url("login/getPlant/")?>'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#ID_PLANT').html('');
        $.map( data, function( val, i ) {
        	user_plant = '<?=$this->data_user->ID_PLANT?>';
        	selected = '';
        	if(val.ID_PLANT == user_plant){
        		selected = 'selected';
        	}
          $('#ID_PLANT').append('<option value="'+val.ID_PLANT+'" '+selected+'>'+val.NM_PLANT+'</option>');
        });
      });

      $('select2').select2();
  }

  function listArea(idPlant = ''){
      $.ajax({
        url : '<?=base_url("login/getArea/")?>'+idPlant,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#ID_AREA').html('');
        $.map( data, function( val, i ) {
        	user_area = '<?=$this->data_user->ID_AREA?>';
        	selected = '';
        	if(val.ID_AREA == user_area){
        		selected = 'selected';
        	}
          $('#ID_AREA').append('<option value="'+val.ID_AREA+'" '+selected+'>'+val.NM_AREA+'</option>');
        });
      });

      $('select2').select2();
  }

  /* Auto Complete */
  var options = {
    url: function(username) {
      return '<?php echo site_url("user/get_username/") ?>';
    },

    getValue: function(element) {
      return element.MK_NAMA;
    },

    //Description
    template: {
      type: "description",
      fields: {
        description: "MK_CCTR_TEXT"
      }
    },
    
    //Set return data
    list: {
      onSelectItemEvent: function() {
        var selectedItemValue = $("#fullName").getSelectedItemData().USERNAME;
        $("#uName").val(selectedItemValue).trigger("change");
      }
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },

    //Sending post data
    preparePostData: function(data) {
      data.username = $("#fullName").val();
      return data;
    },
    theme: "plate-dark", //square | round | plate-dark | funky
    requestDelay: 400
  };

  $("#fullName").easyAutocomplete(options);

});

  $("form").on("submit", function(e) {
    e.preventDefault();
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method");
    var form_data = $(this).serialize();
    var valUserGroup = $('#ID_USERGROUP').val();
    // console.log(form_data);
    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data + '&ID_USERGROUP='+valUserGroup
    }).done(function(response){
    	response = JSON.parse(response);
    	if(response['status'] == 'success'){
    		typeStatus = 1;
    		$('#bodyContent').html('<div class="keterangan" style="text-align: center"><h5>Success add user account</h5></div>');
    	}else{
    		typeStatus = 4;
    	}
		notif(typeStatus, response['message']);
    });
  });

</script>