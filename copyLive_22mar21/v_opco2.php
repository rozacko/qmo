<section class="content-header">
<h1>
  Notify Reciever
  <small></small>
</h1>
</section>

<div class="row" style="margin:3px;">
<div class="col-xs-12">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">List data</h3>
      <a href="opco/add"><button class="pull-right btn btn-sm btn-primary">Create New</button></a>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="form-group row">
        <div class="form-group col-sm-12 col-sm-2">
                <label for="ID_COMPANY">GROUP NOTIFICATION</label>
               
                <select id="ID_JABATAN" name="ID_JABATAN" class="form-control select2">
                  <option value="">All Group Notification</option>
                  <?php  foreach($this->list_jabatan as $jabatan): ?>
                    <option value="<?php echo $jabatan->ID_JABATAN;?>" <?php echo ($this->ID_JABATAN == $jabatan->ID_JABATAN)?"SELECTED":"";?> ><?php echo $jabatan->NM_JABATAN;?></option>
                  <?php endforeach; ?>
                </select>
              </div>
        <div class="form-group col-sm-12 col-sm-2">
        <label for="ID_COMPANY">COMPANY</label>
          <select id="ID_COMPANY" class="form-control select2">
          <option value="">All Company</option>
            <?php  foreach($this->list_company as $company): ?>
            <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group col-sm-12 col-sm-2">
        <label for="ID_PLANT">PLANT</label>
          <select id="ID_PLANT"  class="form-control select2">
            <option value="">All Plant</option>
          </select>
        </div>
        <div class="form-group col-sm-12 col-sm-2">
                <label for="ID_GROUPAREA">GROUP AREA</label>
                <select id="ID_GROUPAREA"  class="form-control select2">
                  <option value="">All Group Area</option>
                </select>
              </div>
        <div class="form-group col-sm-12 col-sm-2">
        <label for="ID_AREA">AREA</label>
          <select id="ID_AREA"  class="form-control select2">
            <option value="">All Area</option>
          </select>
        </div>
        <div class="form-group col-sm-12 col-sm-2">
        <label></label><br/>
          <button id="btn-filter-data" class="btn btn-info">Load Data</button>
        </div>
      </div>
      <div class="table-responsive">
        <table  id="tbList" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
              <th>No</th>
              <th>Notif Group</th>
              <th>Name</th>
              <th>Email</th>
              <th>Company</th>
              <th>Plant</th>
              <th>Area</th>
              <th>Send As</th>
              <th width="30%"></th>
          </tr>
          </thead>
        </table>
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col -->
</div>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>

  $(document).on("click", "#btn-filter-data", function(){
    dtTbl();
  });

 function dtTbl(){
   var notif = $("#ID_JABATAN").val();
   var company = $("#ID_COMPANY").val();
   var plant = $("#ID_PLANT").val();
   var grouparea = $("#ID_GROUPAREA").val();
   var area = $("#ID_AREA").val();
    $("#tbList").DataTable();
    $("#tbList").DataTable().destroy();

    $('#tbList').DataTable({
      "processing": true,
      "ajax":  {
        "url" : "<?=base_url('opco/getDataList')?>",
        "type" : "POST",
        "data": {
          "notif":notif,
          "company":company,
          "plant":plant,
          "grouparea":grouparea,
          "area":area,
        }
      },
      "columns": [
        { "className": "text-center", "data": "NO" },
        { "data": "NM_JABATAN" },
        { "data": "FULLNAME" },
        { "data": "EMAIL" },
        { "data": "NM_COMPANY" },
        { "data": "NM_PLANT" },
        { "data": "NM_AREA" },
        { "data": "TEMBUSAN", "render": function(data, type, row, meta){
          return ((row.TEMBUSAN)?"MAIL COPY":"NOTIFICATION");
        } },
        {
          "sortable": false,
          "className": "text-center",
          "render": function ( data, type, row, meta ) {
             var buttonList = '<div class="btn-group">'+
                              '<a style="color: #fff" href="opco/edit/'+row.ID_OPCO+'"><button type="button" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>'+
                              '<button type="button" id_opco="'+row.ID_OPCO+'" class="btn btn-danger btn-xs delete" data-title="Remove Group Employee" data-text="This employee group will be removed. Are you sure?"><i class="fa fa-times"></i></button>'+
                              '</div>';
             return buttonList;
           }
        },
      ],
      "initComplete":function( settings, json){
          $(document).on('click',".delete",function () {
            var id_opco = $(this).attr('id_opco');

            $.confirm({
                title: 'Delete',
                text: 'Are you sure?',
                confirmButton: 'Delete',
                confirmButtonClass: '',
                cancelButton: "Cancel",
                confirm: function() {
                  window.location.href = '<?php echo site_url("opco/delete/") ?>'+id_opco;
                },
                cancel: function() {
                    // nothing to do
                }
            });
          });
        }
    });
  }

  $(function() {
    dtTbl();
    // $("#ID_COMPANY").change();
  });

  $("#ID_COMPANY").change(function(){
      var company = $(this).val(), plant = $('#ID_PLANT');
      $.getJSON('<?php echo site_url("opco/ajax_get_plant/");?>' + company, function (result) {
        var values = result;
        
        plant.find('option').remove();
        if (values != undefined && values.length > 0) {
          plant.css("display","");
          $('#ID_GROUPAREA').css("display","");
          plant.append($("<option></option>").attr("value", "").text("All Plant"));
          $(values).each(function(index, element) {
            plant.append($("<option></option>").attr("value", element.ID_PLANT).text(element.NM_PLANT));
          });
          
        }else{
          plant.find('option').remove();
          plant.append($("<option></option>").attr("value", '00').text("NO PLANT"));
        }
      // $("#ID_PLANT").change();
      });
    });

    $("#ID_PLANT").change(function(){
      var plant = $(this).val(), grouparea = $('#ID_GROUPAREA');
      $.getJSON('<?php echo site_url("opco/ajax_get_grouparea/");?>' + $("#ID_COMPANY").val() + '/' + plant, function (result) {
        var values = result;
        
        grouparea.find('option').remove();
        if (values != undefined && values.length > 0) {
          grouparea.css("display","");
          grouparea.append($("<option></option>").attr("value", "").text("All Group Area"));
          $(values).each(function(index, element) {
            if(!$("#ID_GROUPAREA option[value='"+element.ID_GROUPAREA+"']").length > 0){
              grouparea.append($("<option></option>").attr("value", element.ID_GROUPAREA).text(element.NM_GROUPAREA));
            }
          });
        }else{
          grouparea.find('option').remove();
          grouparea.append($("<option></option>").attr("value", '00').text("NO GROUP AREA"));
        }
        // $("#ID_GROUPAREA").change();
      });
    });

    $("#ID_GROUPAREA").change(function(){
      var grouparea = $(this).val(), area = $('#ID_AREA');
      $.getJSON('<?php echo site_url("opco/ajax_get_area/");?>' + $("#ID_COMPANY").val() + '/' + $("#ID_PLANT").val() + '/' + grouparea, function (result) {
        var values = result;
        
        area.find('option').remove();
        if (values != undefined && values.length > 0) {
          area.css("display","");
          area.append($("<option></option>").attr("value", "").text("All Area"));
          $(values).each(function(index, element) {
            area.append($("<option></option>").attr("value", element.ID_AREA).text(element.NM_AREA));
          });
        }else{
          area.find('option').remove();
          area.append($("<option></option>").attr("value", '00').text("NO AREA"));
        }
        // $("#ID_AREA").change();
      });
    });

</script>
