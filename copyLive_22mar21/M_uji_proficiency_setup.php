<?php

class M_uji_proficiency_setup Extends DB_QM {
	
	public function data_comodity(){
		$this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->result();
	}
	 
	public function data_pic(){
		/*
		$this->db->select("M_USERS.*,  M_AREA.NM_AREA");
		$this->db->join("M_AREA","M_USERS.ID_AREA = M_AREA.ID_AREA","LEFT")
		$this->db->where("M_USERS.DELETED","0");
		$this->db->where("M_USERS.ISACTIVE","Y");
		$this->db->where("M_USERS.EMAIL IS NOT NULL");
		$this->db->where("M_USERS.FULLNAME IS NOT NULL");
		$this->db->order_by("M_USERS.FULLNAME");
		return $this->db->get("M_USERS")->result();
		*/
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			right join M_ROLES mr on mu.id_user = mr.id_user and mr.ID_USERGROUP = 65
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	public function data_periode(){
		$this->db->where("DELETE_BY IS NULL");
		$this->db->where("GROUP_PP","Internal");
		$this->db->order_by("YEAR_PP","DESC");
		return $this->db->get("T_PERIODE_PROFICIENCY")->result();
	}
	
	public function data_lab(){
		$this->db->order_by("NAMA_LAB");
		$this->db->where("DELETED IS NULL");
		return $this->db->get("M_LABORATORIUM")->result();
	}
	
	public function save_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		 = $this->input->post('ID_PP');
		$id_comodity = $this->input->post('ID_KOMODITI');
		$id_lab 	 = $this->input->post('ID_LAB');
		$id_pic		 = $this->input->post('ID_PIC');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->set("ID_PRIODE", $id_pp);
		$this->db->set("ID_KOMODITI", $id_comodity);
		$this->db->set("ID_LAB", $id_lab);
		$this->db->set("ID_PIC", $id_pic);
		$this->db->set("CREATED_AT","TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("CREATED_BY", $user_in->FULLNAME);
		$q = $this->db->insert("T_PROFICIENCY");

		return $q;
	}
	
	public function get_proficiency_now($id_pp, $id_comodity, $id_lab, $id_pic, $date_now, $create_by_now){
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->where("ID_PRIODE", $id_pp);
		$this->db->where("ID_KOMODITI", $id_comodity);
		$this->db->where("ID_LAB", $id_lab);
		$this->db->where("ID_PIC", $id_pic); 
		$this->db->where("CREATED_BY", $create_by_now);
		$this->db->where("CREATED_AT","TO_DATE('{$date_in}', 'YYYY-MM-DD')", false);
		return $this->db->get("T_PROFICIENCY")->row();
	}
	
	public function save_activity($id_proficiency){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		$end	  = $this->input->post('end_date');
		
        $total = count($activity);
		$id = (int)$id_proficiency;

		//melakukan perulangan input
		for($i=0; $i<$total; $i++){
			if($activity[$i] == null or $activity[$i] == ''){
				continue;
			} 
				$start_in 	= date("Y-m-d", strtotime($start[$i]));
				$end_in 	= date("Y-m-d", strtotime($end[$i]));
				$date_now 	= date("Y-m-d");
				$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
				// $this->db->set("ID_PROFICIENCY", $id);
				// $this->db->set("ACTIVITY_NAME", $activity[$i]);
				// $this->db->set("PLAN_DATE_START", $start_in);
				// $this->db->set("PLAN_DATE_END", $end_in);
				// $this->db->set("CREATED_AT", "CURRENT_DATE", false);
				// $this->db->set("CREATED_BY", $user_in->FULLNAME);
				// $q = $this->db->insert("T_ACTIVITY");
				
				$sql = "
					INSERT INTO T_ACTIVITY (ID_PROFICIENCY, ACTIVITY_NAME, PLAN_DATE_START, PLAN_DATE_END, CREATED_AT, CREATED_BY) VALUES ('{$id}', '{$activity[$i]}', TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$end_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')
				";
				$q = $this->db->query($sql);
			
		}
		return $q;
	}
	
	public function get_list($id_pp = false){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, ML.NAMA_LAB, MU.FULLNAME, TPP.*");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.ID_LAB","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->where("TPP.GROUP_PP","Internal");
		if($id_pp != null){
			$this->db->where("TP.ID_PRIODE", $id_pp);
		}
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
	}
	
	public function get_data_by_id($id){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, ML.NAMA_LAB, MU.FULLNAME");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.ID_LAB","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TP.ID_PROFICIENCY", $id);
		$this->db->order_by("TP.CREATED_AT");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->row();
	}
	
	// timeline get data
	public function get_data_activity($id){
		$this->db->where("ID_PROFICIENCY", $id);
		$this->db->where("DELETED_BY IS NULL");
		$this->db->order_by("PLAN_DATE_START");
		$query = $this->db->get('T_ACTIVITY');
		return $query->result();
	}
	
	public function update_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		 = $this->input->post('ID_PP');
		$id_proficiency = $this->input->post('ID_PROFICIENCY');
		$id_comodity = $this->input->post('ID_KOMODITI');
		$id_lab 	 = $this->input->post('ID_LAB');
		$id_pic		 = $this->input->post('ID_PIC');
		
		$this->db->set("ID_PRIODE", $id_pp);
		$this->db->set("ID_KOMODITI", $id_comodity); 
		$this->db->set("ID_LAB", $id_lab);
		$this->db->set("ID_PIC", $id_pic);
		$this->db->set("UPDATED_AT","CURRENT_DATE", false);
		$this->db->set("UPDATED_BY", $user_in->FULLNAME);
		$this->db->where("ID_PROFICIENCY",$id_proficiency);
		$q = $this->db->update("T_PROFICIENCY");

		return $q;
	}
	
	public function hapus_proficiency($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$this->db->set("DELETED_AT","CURRENT_DATE", false);
		$this->db->set("DELETED_BY", $user_in->FULLNAME);
		$this->db->where("ID_PROFICIENCY",$id);
		$q = $this->db->update("T_PROFICIENCY");
		
		if($q){
			$this->db->set("DELETED_AT","CURRENT_DATE", false);
			$this->db->set("DELETED_BY", $user_in->FULLNAME);
			$this->db->where("ID_PROFICIENCY",$id);
			$query = $this->db->update('T_ACTIVITY');
		}
		return $query;
	}
	
	public function update_activity(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_activity = $this->input->post('ID_ACTIVITY');
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		$end	  = $this->input->post('end_date');
		
		$start_in 	= date("Y-m-d", strtotime($start));
		$end_in 	= date("Y-m-d", strtotime($end));
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_ACTIVITY SET ACTIVITY_NAME = '{$activity}', PLAN_DATE_START = TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), PLAN_DATE_END = TO_DATE('{$end_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id_activity}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function hapus_activity($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			UPDATE T_ACTIVITY SET DELETED_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), DELETED_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function get_priode_pro($id_priode){
		$this->db->where("DELETE_BY IS NULL");
		$this->db->where("ID_PP", $id_priode);
		$this->db->order_by("YEAR_PP","DESC");
		return $this->db->get("T_PERIODE_PROFICIENCY")->row();
	}
	
	public function get_email_pic($id_pic){
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null and
				mu.id_user = {$id_pic}
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->row();
	}
	
	public function get_komoditi($id_komoditi){
		$this->db->where("ID_SAMPLE", $id_komoditi);
		$this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->row();
	}
	
	public function fase_proficiency($id_pro){
		$sql = "
			SELECT *
				  FROM (select * from T_ACTIVITY where ID_PROFICIENCY = '{$id_pro}' and act_date_start is not null order by act_date_start desc)
			WHERE rownum = 1
		";
		$q = $this->db->query($sql);
		return $q->row();
	}
}

?>