<?php

class M_acclab_setuppelaksanaan Extends DB_QM {
	
	public function data_comodity(){
		$this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->result();
	}
	 
	public function data_pic(){
		/*
		$this->db->select("M_USERS.*,  M_AREA.NM_AREA");
		$this->db->join("M_AREA","M_USERS.ID_AREA = M_AREA.ID_AREA","LEFT")
		$this->db->where("M_USERS.DELETED","0");
		$this->db->where("M_USERS.ISACTIVE","Y");
		$this->db->where("M_USERS.EMAIL IS NOT NULL");
		$this->db->where("M_USERS.FULLNAME IS NOT NULL");
		$this->db->order_by("M_USERS.FULLNAME");
		return $this->db->get("M_USERS")->result();
		*/
		// $sql = "
		// 	select mu.*, ma.nm_area from m_users mu
		// 	left join m_area ma on mu.id_area = ma.id_area
		// 	right join M_ROLES mr on mu.id_user = mr.id_user and mr.ID_USERGROUP = 65
		// 	where 
		// 		mu.deleted = 0 and
		// 		mu.isactive = 'Y' and
		// 		mu.email is not null and
		// 		mu.fullname is not null
		// 	order by
		// 		mu.fullname
		// ";
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	public function data_periode(){
		$this->db->where("DELETE_BY IS NULL");
		// $this->db->where("GROUP_PP","Internal");
		$this->db->order_by("TAHUN","DESC");
		return $this->db->get("QMLAB_PERIODE")->result();
	}
	
	public function data_lab(){
		$this->db->order_by("NAMA_LAB");
		$this->db->where("DELETED IS NULL");
		return $this->db->get("M_LABORATORIUM")->result();
	}

	public function data_evaluasi(){
		$this->db->order_by("NAMA_STANDARD");
		$this->db->where("DELETE_DATE IS NULL");
		$this->db->where("IS_ACTIVE", 1);
		return $this->db->get("QMLAB_BLINDSTD")->result();
	}
	
	public function save_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		  = $this->input->post('ID_PP');
		$jenis 		  = $this->input->post('JENIS');
		$start_date	  = $this->input->post('START_DATE');
		$end_date	  = $this->input->post('END_DATE');
		$id_lab 	  = $this->input->post('ID_LAB');
		if ($jenis != 'OBSERVASI') {
			$id_std   = $this->input->post('FK_ID_STD');
		}else{
			$id_std   = 0;
		}
		$id_pic_obs	  = $this->input->post('ID_PIC_OBS');
		$id_pic_plant = $this->input->post('ID_PIC_PLANT');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->set("FK_ID_PERIODE", $id_pp);
		$this->db->set("JENIS", $jenis);
		$this->db->set("LAB", $id_lab);
		$this->db->set("FK_ID_STD", $id_std);
		$this->db->set("PIC_OBSERVASI", $id_pic_obs);
		$this->db->set("PIC_PLANT", $id_pic_plant);
		$this->db->set("STATUS", 'WAITING');
		$this->db->set("START_DATE","TO_DATE('{$start_date} 00:00:00', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("END_DATE","TO_DATE('{$end_date} 23:59:59', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("CREATE_DATE","TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("CREATE_BY", $user_in->FULLNAME);
		$q = $this->db->insert("QMLAB_SETUP");

		return $q;
	}
	
	public function get_proficiency_now($id_pp, $id_comodity, $id_lab, $id_pic, $date_now, $create_by_now){
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->where("FK_ID_PERIODE", $id_pp);
		$this->db->where("ID_KOMODITI", $id_comodity);
		$this->db->where("LAB", $id_lab);
		$this->db->where("ID_PIC", $id_pic); 
		$this->db->where("CREATE_BY", $create_by_now);
		$this->db->where("CREATE_DATE","TO_DATE('{$date_in}', 'YYYY-MM-DD')", false);
		return $this->db->get("QMLAB_SETUP")->row();
	}
	
	public function save_activity($id_proficiency){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		$end	  = $this->input->post('end_date');
		
        $total = count($activity);
		$id = (int)$id_proficiency;

		//melakukan perulangan input
		for($i=0; $i<$total; $i++){
			if($activity[$i] == null or $activity[$i] == ''){
				continue;
			} 
				$start_in 	= date("Y-m-d", strtotime($start[$i]));
				$end_in 	= date("Y-m-d", strtotime($end[$i]));
				$date_now 	= date("Y-m-d");
				$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
				// $this->db->set("ID_PROFICIENCY", $id);
				// $this->db->set("ACTIVITY_NAME", $activity[$i]);
				// $this->db->set("PLAN_DATE_START", $start_in);
				// $this->db->set("PLAN_DATE_END", $end_in);
				// $this->db->set("CREATE_DATE", "CURRENT_DATE", false);
				// $this->db->set("CREATE_BY", $user_in->FULLNAME);
				// $q = $this->db->insert("T_ACTIVITY");
				
				$sql = "
					INSERT INTO T_ACTIVITY (ID_PROFICIENCY, ACTIVITY_NAME, PLAN_DATE_START, PLAN_DATE_END, CREATE_DATE, CREATE_BY) VALUES ('{$id}', '{$activity[$i]}', TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$end_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')
				";
				$q = $this->db->query($sql);
			
		}
		return $q;
	}
	
	public function get_list($id_pp = false){
		$this->db->select("TP.*, ML.NAMA_LAB, MU.FULLNAME as FULLNAME_OBSERVASI, MU2.FULLNAME as FULLNAME_PLANT, TPP.NAMA, TPP.TAHUN, STD.NAMA_STANDARD, STD.KODE_STANDARD");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.PIC_OBSERVASI","LEFT");
		$this->db->join("M_USERS MU2","MU2.ID_USER = TP.PIC_PLANT","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.LAB","LEFT");
		$this->db->join("QMLAB_PERIODE TPP","TPP.ID = TP.FK_ID_PERIODE","LEFT");
		$this->db->join("QMLAB_BLINDSTD STD","STD.ID = TP.FK_ID_STD","LEFT");
		$this->db->where("TP.DELETE_BY IS NULL");
		$this->db->where("TPP.DELETE_BY IS NULL");
		if($id_pp != null){
			$this->db->where("TP.FK_ID_PERIODE", $id_pp);
		}
		$this->db->order_by("TPP.TAHUN","DESC");
		$this->db->order_by("TP.CREATE_DATE","DESC");
		$query = $this->db->get('QMLAB_SETUP TP');
		return $query->result();
	}
	
	public function get_data_by_id($id){
		$this->db->select("TP.*");
		$this->db->where("TP.DELETE_BY IS NULL");
		$this->db->where("TP.ID", $id);
		$query = $this->db->get('QMLAB_SETUP TP');
		return $query->row();
	}

	public function get_status()
	{
		$list_status = array(
			'OPEN' => "APPROVED ATASAN",
			'WAITING' => 'WAITING APPROVAL',
			'ISI_TEMUAN' => 'ISI TEMUAN',
			'TINDAK_LANJUT' => 'TINDAK LANJUT' 
		);
		
		return $list_status;
	}
	
	// timeline get data
	public function get_data_activity($id){
		$this->db->where("ID_PROFICIENCY", $id);
		$this->db->where("DELETE_BY IS NULL");
		$this->db->order_by("PLAN_DATE_START");
		$query = $this->db->get('T_ACTIVITY');
		return $query->result();
	}
	
	public function update_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id 		  = $this->input->post('ID');
		$id_pp 		  = $this->input->post('ID_PP');
		$jenis 		  = $this->input->post('JENIS');
		$start_date	  = $this->input->post('START_DATE');
		$end_date	  = $this->input->post('END_DATE');
		$id_lab 	  = $this->input->post('ID_LAB');
		if ($jenis != 'OBSERVASI') {
			$id_std   = $this->input->post('FK_ID_STD');
		}else{
			$id_std   = 0;
		}
		$id_pic_obs	  = $this->input->post('ID_PIC_OBS');
		$id_pic_plant = $this->input->post('ID_PIC_PLANT');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->set("FK_ID_PERIODE", $id_pp);
		$this->db->set("JENIS", $jenis);
		$this->db->set("LAB", $id_lab);
		$this->db->set("FK_ID_STD", $id_std);
		$this->db->set("PIC_OBSERVASI", $id_pic_obs);
		$this->db->set("PIC_PLANT", $id_pic_plant);
		$this->db->set("STATUS", 'WAITING');
		$this->db->set("START_DATE","TO_DATE('{$start_date} 00:00:00', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("END_DATE","TO_DATE('{$end_date} 23:59:59', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("UPDATE_DATE","CURRENT_DATE", false);
		$this->db->set("UPDATE_BY", $user_in->FULLNAME);
		$this->db->where("ID",$id);
		$q = $this->db->update("QMLAB_SETUP");

		return $q;
	}

	public function update_status($id, $status){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->set("STATUS", $status);
		$this->db->set("UPDATE_DATE","CURRENT_DATE", false);
		$this->db->set("UPDATE_BY", $user_in->FULLNAME);
		$this->db->where("ID",$id);
		$q = $this->db->update("QMLAB_SETUP");

		return $q;
	}
	
	public function hapus_proficiency($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$this->db->set("DELETE_DATE","CURRENT_DATE", false);
		$this->db->set("DELETE_BY", $user_in->FULLNAME);
		$this->db->where("ID",$id);
		$q = $this->db->update("QMLAB_SETUP");
		
		// if($q){
		// 	$this->db->set("DELETE_DATE","CURRENT_DATE", false);
		// 	$this->db->set("DELETE_BY", $user_in->FULLNAME);
		// 	$this->db->where("ID_PROFICIENCY",$id);
		// 	$query = $this->db->update('T_ACTIVITY');
		// }
		return $query;
	}
	
	public function update_activity(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_activity = $this->input->post('ID_ACTIVITY');
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		$end	  = $this->input->post('end_date');
		
		$start_in 	= date("Y-m-d", strtotime($start));
		$end_in 	= date("Y-m-d", strtotime($end));
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_ACTIVITY SET ACTIVITY_NAME = '{$activity}', PLAN_DATE_START = TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), PLAN_DATE_END = TO_DATE('{$end_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_DATE = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id_activity}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function hapus_activity($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			UPDATE T_ACTIVITY SET DELETE_DATE = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), DELETE_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function get_priode_pro($id_priode){
		$this->db->where("DELETE_BY IS NULL");
		$this->db->where("ID", $id_priode);
		$this->db->order_by("TAHUN","DESC");
		return $this->db->get("QMLAB_PERIODE")->row();
	}
	
	public function get_email_pic($id_pic){
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null and
				mu.id_user = {$id_pic}
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->row();
	}
	
	public function get_komoditi($id_komoditi){
		$this->db->where("ID_SAMPLE", $id_komoditi);
		$this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->row();
	}
	
	public function fase_proficiency($id_pro){
		$sql = "
			SELECT *
				  FROM (select * from T_ACTIVITY where ID_PROFICIENCY = '{$id_pro}' and act_date_start is not null order by act_date_start desc)
			WHERE rownum = 1
		";
		$q = $this->db->query($sql);
		return $q->row();
	}
}

?>