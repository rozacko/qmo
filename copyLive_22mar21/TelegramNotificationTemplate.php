<?php

class TelegramNotificationTemplate {

  /**
   * Build message string from selected template
   *
   * @param string  $template Name of template
   * @param array   $data     Array of data to be injected to template
   * @return string  
   */
  public function buildMessageFromTemplate($templateName, $data) {
    extract($data);

    $text = $this->getTemplate($templateName);
    
    return static::parse($text, $data);
  }


  /**
   * Parser for this template
   *
   * @param string  $subject Text to be parsed
   * @param array   $data    Array of data to be embeded to $subject
   * @return string
   */
  static public function parse($subject, $data) {
    $search = array_map(function($key) {
        return sprintf('{%s}', $key); 
      }, 
      array_keys($data)
    );

    $replacement = array_values($data);

    return str_replace($search, $replacement, $subject);
  }

  /**
   * Get template
   *
   * @param string $name name of template (still dummy)
   * @return string
   */
  protected function getTemplate($name) {
    return <<<EOF
NON CONFIRMITY QUALITY REPORT


{KD_COMPONENT} OUT OF STANDARD ({NM_INCIDENT_TYPE})


COMPANY: {COMPANY}
PLANT:  {PLANT} 
AREA: {AREA}
PRODUCT: {PRODUCT}

{PRODUCT_COMPONENTS}

Click link below to Solve this NCQR

{LINK_SOLVE_NCQR}

EOF;

  }

}

/* test 
$template = new Telegram_notifcation_template;

$message = $template->buildMessageFromTemplate('halo', [
  'KD_COMPONENT' => '44949',
  'NM_INCIDENT_TYPE' => 'error',
  'COMPANY' => 'SIG',
  'PLANT' => '64474',
  'AREA'  => 'District 77',
  'PRODUCT' => '####',
  'LINK_SOLVE_NCQR' => 'https://google.com/search?q=halo'
]);

echo $message;
*/
