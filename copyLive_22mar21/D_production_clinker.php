<?php defined('BASEPATH') OR exit('No direct script access allowed');

class D_production_clinker extends DB_QM {

	/** Private Var for datatable **/
    private $post  = array();
	private $table = "D_PRODUCTION_CLINKER";
	private $pKey  = "ID_PRODUCTION_CLINKER";
	private $column_order = array(NULL, 'ID_PRODUCTION_CLINKER','ID_PLANT','ID_AREA','NO_FIELD'); //set column for datatable order
    private $column_search = array('ID_PLANT', 'ID_PRODUCTION_CLINKER', 'ID_AREA'); //set column for datatable search 
    private $order = array("NO_FIELD" => 'ASC'); //default order

    public function __construct(){
    	parent::__construct();
    	$this->post = $this->input->post();
    }

	/** Fetch table **/
	public function get(){
		$this->db->from($this->table);
	}

	/** Generate Query for DataTables **/
	public function get_query(){
		$this->get();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}
	}

	/** Execute get_query and return DataTables Data **/
	public function get_list() {
		$this->get_query();

		if($this->input->post('length') != -1){
			$this->db->limit($this->input->post('length'),$this->input->post('start'));
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	/** Get data by id **/
	public function get_by_id($key){
		$this->db->where($this->pKey, $key);
		$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_nilai($id_prod='', $no_field='',$id_area=''){	
		$this->db->where('ID_PRODUCTION_CLINKER', $id_prod);	
		//$this->db->where('NO_FIELD', $no_field);
		if($id_area !='') $this->db->where('ID_AREA', $id_area);	
		//if($id_produk !='') $this->db->where('ID_PRODUCT', $id_produk);
		$query = $this->db->get($this->table);
		//return $query->row()->NILAI;
		return $query->row();
	}
	public function get_factorcomp($id_prod='', $no_field='',$id_comp=''){	
		$this->db->where('ID_PRODUCTION_CLINKER', $id_prod);
		if($id_comp !='') $this->db->where('ID_COMPANY', $id_comp);	
		$query = $this->db->get($this->table);
		return $query->row();
	}
	public function get_nilaiopc($id_prod='', $no_field=''){
		$this->db->where('ID_PRODUCTION_CLINKER', $id_prod);
		$this->db->where('NO_FIELD', $no_field);
		$query = $this->db->get($this->table);
		//return $query->row()->NILAI;
		return $query->row()->OPC;
	}
	public function get_nilainonopc($id_prod='', $no_field=''){
		$this->db->where('ID_PRODUCTION_CLINKER', $id_prod);
		$this->db->where('NO_FIELD', $no_field);
		$query = $this->db->get($this->table);
		//return $query->row()->NILAI;
		return $query->row()->NON_OPC;
	}

	/** Get where **/
	public function get_where($where){
		$this->db->where($where);
		$query = $this->db->get($this->table);
		return $query->result();      
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count where **/
	public function count_where($where){
		$this->db->where($where);
		$query = $this->db->get($this->table);
		return (int) $query->num_rows();
	}

	/** Execute insert query **/
	public function insert($data) {
		$res = array();
		//$this->db->set($this->pKey,"SEQ_".$this->pKey.".NEXTVAL",FALSE);
		$this->db->set($data);
		$this->db->insert($this->table);
	}

	/** Execute update query **/
	public function update($id,$data) {
		$this->db->where($this->pKey, $id);
		$this->db->update($this->table, $data);
	}

	/** Execute update where query **/
	public function update_where($where,$data) {
		$this->db->where($where);
		$this->db->update($this->table, $data);
	}

	/** Execute delete query by pKey **/
	public function delete($id) {
		$res = array();
		$this->db->delete($this->table, array($this->pKey=>$id));
	}

	/** Execute delete query with where **/
	public function delete_where($where) {
		$res = array();
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	public function exists($data){
		$this->db->where($this->pKey, $data[$this->pKey]);
		$this->db->where("ID_COMPANY", $data['ID_COMPANY']);
		$this->db->where("ID_AREA", $data['ID_AREA']);
		return $this->db->get($this->table)->num_rows();
	}
	// public function exists_sum($data){
	// 	$this->db->where($this->pKey, $data[$this->pKey]);
	// 	$this->db->where("ID_COMPANY", $data['ID_COMPANY']);
	// 	$this->db->where("ID_PRODUCT", $data['ID_PRODUCT']);
	// 	return $this->db->get($this->table)->num_rows();
	// }

}