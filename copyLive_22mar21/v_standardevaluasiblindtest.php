   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Standard Sample Blind Test
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="row">
        <div class="col-xs-12">    
          <div class="box" id="viewArea">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Standard Blind Test</h3>
                <div class="input-group input-group-sm" style="width: 150px; float: right;">
              <?php if($this->PERM_WRITE): ?>
                  <a id="AddNew" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a> &nbsp; | &nbsp;
              <?PHP endif; ?>
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>
              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th width="5%">No. </th>
                      <th width="*">NAMA STANDARD</th>
                      <th width="*">KODE STANDARD</th>
                      <th width="*">TANGGAL BUAT</th>
                      <th width="*">STATUS</th>
                      <th width="5%">ACTION</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">    
    
          <div class="box" id="entryBlindTest" style="display: none;">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Display Blind test</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="CancelEntry" type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </div>

            <div class="box-body">

              <div class="row col-sm-6">
                <table class="table table-bordered table-striped table-responsive" style="width: 100%;">
                  <thead>
                    <th colspan="3">INFORMASI STANDARD BLIND TEST<span id="id_setup" style="display: none;"></span><span id="id_periode" style="display: none;"></span></th>
                    </thead>
                  <tbody style="font-size: x-small;">
                    <tr>
                      <td width="25%">Kode</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" name="stdid" id="stdid" style="display: none;" class="form-control" required=""><input type="text" name="stdkode" id="stdkode" class="form-control" required=""></td>
                    </tr>
                    <tr>
                      <td>Nama</td>
                      <td> : </td>
                      <td><input type="text" name="stdnama" id="stdnama" class="form-control" required=""></td>
                    </tr>
                  </tbody>
                </table>
                <div class="box-tools pull-right evaluatestd" style="margin-bottom: 25px;">
              <?php if($this->PERM_WRITE): ?>
                  <a id="EvaluasiBlind" onclick="evaluatesample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Simpan Data</a>

              <?PHP endif; ?>
                </div>
              </div>

              <div class="row col-sm-6" id="formpreview">

                <div class="box-body" id="forminputblind" style="">
                  <div class="box box-warning box-solid col-sm-12 col-md-12 col-lg-12">
                    <div class="box-header with-border">
                      <h3 class="box-title"> ENTRY DATA DIBAWAH INI <i class="fa fa-hand-o-down"></i> </h3>
                    </div>
                    <div class="box-body" style="">
                      <div id="example1" class="hot handsontable htColumnHeaders"></div>
                      <!-- style="margin-left: 15px; margin-right: : 15px; max-height: 350px !important;" -->
                      <div class="form-group row" style="margin-left: 15px; margin-right: : 15px;">
                        <div class="col-sm-2" style="">
              <?php if($this->PERM_WRITE): ?>
                          <label for="addNewRow"></label>
                          <a  style="display: none;" name="addNewRow" id="addNewRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-success btn-sm addNewRow"><i class="fa fa-plus"></i> Row</a>

              <?PHP endif; ?>
                        </div>
                        <div class="col-sm-2" style="">
              <?php if($this->PERM_WRITE): ?>
                          <label for="delLastRow"></label>
                          <a style="display: none;" name="delNewRow" id="delLastRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-danger btn-sm delLastRow"><i class="fa fa-trash"></i> Row</a>

              <?PHP endif; ?>
                        </div>
                        <div class="col-sm-2" style="">
              <?php if($this->PERM_WRITE): ?>
                          <a  name="savesample" id="savesample" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn  btn-primary btn-sm"><i class="fa fa-save"></i> &nbsp;Save Component</a>

              <?PHP endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="box-body" style="margin-left: 5px; display: none;">
                  <div class="box box-danger box-solid col-sm-12 col-md-12 col-lg-12">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Standard Blind Test Preview </h3>
                      <div class="box-tools pull-right">
              <?php if($this->PERM_WRITE): ?>
                        <a id="SubmitBlind" onclick="submitsample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a>

              <?PHP endif; ?> 
                        <a id="ReloadDataBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                      </div>
                    </div>
                    <div class="box-body" style="">
                      <table  id="blind_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th width="50%">KOMPONEN</th>
                            <th width="*">DATA</th>
                            <th width="*">SD</th>
                            <th width="*">SATUAN</th>
                            <!-- <th width="5%">ACTION</th> -->
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="box-footer">
            </div>
          </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


  
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

  
<script type="text/javascript">
    $(document).ready(function() {
    
    $("#a0").on("change",function(){ 
      var dateSet = $("#a0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#a1").val(newnextdate);
    });
    $("#b0").on("change",function(){ 
      var dateSet = $("#b0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#b1").val(newnextdate);
    });
    $("#c0").on("change",function(){ 
      var dateSet = $("#c0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#c1").val(newnextdate);
    });
    $("#d0").on("change",function(){ 
      var dateSet = $("#d0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#d1").val(newnextdate);
    });
    $("#e0").on("change",function(){ 
      var dateSet = $("#e0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#e1").val(newnextdate);
    });
    $("#f0").on("change",function(){ 
      var dateSet = $("#f0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#f1").val(newnextdate);
    });
    
    $('select').selectize({
          sortField: 'text'
      });
    
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
      
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>


<script type="text/javascript">
  
  function receiptsample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menerima sample sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_standardevaluasiblindtest/receipt_blindtest");?>",
        data: {"FK_ID_PERIODE": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }
  
  function deletesample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menghapus data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_standardevaluasiblindtest/delete_stddetailblindtest");?>",
        data: {"ID": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadDataBlind").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }
  
  function submitsample() {
    // body...
    var r = confirm("Apakah anda yakin untuk submit data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_standardevaluasiblindtest/submit_blindtest");?>",
        data: {"ID": $('#id_setup').text(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#CancelEntry").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }
  
  function evaluatesample() {
    // body...
    var r = confirm("Apakah anda yakin untuk menyimpan data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_standardevaluasiblindtest/save_stdblindtest");?>",
        data: {"ID": $('#stdid').val(), "KODE_STANDARD": $('#stdkode').val(),  "NAMA_STANDARD": $('#stdnama').val(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#stdid, #stdkode, #stdnama").val('');
            $("#CancelEntry").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }

$(document).ready(function(){

  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable; 
  var oBlindTable;
  var glbproduk = new Array();

  reloadtabel(); 

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });
  $(document).on('click',"#ReloadDataBlind",function () {
  	reloadblindtabel($('#stdid').val());
  });
  
  
  $(document).on('click',"#CancelEntry",function () {
    $('#entryBlindTest').hide();
    $('#viewArea').show();
    $("#ReloadData").click(); 
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',"#savesample",function () {    
      saveBlindTest();
  });

  function reloadblindtabel(idsetup, action = true) {
    // body...
    oBlindTable = $('#blind_tables').dataTable({    	
      "paging":   false,
      destroy: true,
      processing: true,
      select: true,
      "ajax": {
        "url": "<?php echo site_url("acclab_standardevaluasiblindtest/ajax_get_stdblindtest");?>/"+idsetup+"/"+action,
        "type": "POST"
      }
    });
    
  }

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("acclab_standardevaluasiblindtest/blindtest_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "RNUM", "width": 50},
        {"data": "NAMA_STANDARD"},
        {"data": "KODE_STANDARD"},
        {"data": "TANGGAL_BUAT"},
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){
            if (parseInt(index['IS_ACTIVE']) == 0) {              
              return 'NON-ACTIVE';
            }
            else {
              return 'ACTIVE';
            }

          }
        },
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){
            var btn = '';
              <?php if($this->PERM_WRITE): ?>
              btn = '<button title="Edit Standard" class="btDetail btn btn-warning btn-xs" type="button"><i class="fa fa-edit"></i> Edit</button> | <button title="Hapus Standard" class="btDelete btn btn-danger btn-xs" type="button" ><i class="fa fa-trash"></i> Hapus</button>';
              <?PHP endif; ?>
              return btn;

          }
        },
      ]
    });
    
    $('#ReloadData').show();
  }

$(document).on('click', ".btEntry", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    // $('#SubmitBlind').show();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    loadtabelinput(data['ID']);
    reloadblindtabel(data['ID']);
});

$(document).on('click', ".btDelete", function() {
    // body...
    var data = oTable.row($(this).parents('tr')).data();
    var r = confirm("Apakah anda yakin untuk menghapus data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_standardevaluasiblindtest/delete_stdblindtest");?>",
        data: {"ID": data['ID'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
});

$(document).on('click', ".btDetail", function() {
    // body...
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    $('#SubmitBlind').hide(); 
    $('#formpreview').show();   

    $('#stdid').val(data['ID']);
    $('#stdkode').val(data['KODE_STANDARD']);
    $('#stdnama').val(data['NAMA_STANDARD']);

    loadtabelinput(data['ID']);
    reloadblindtabel(data['ID']); 
});

$(document).on('click', "#AddNew", function() {
    // body...
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').hide();
    $('#formpreview').hide();
    $('.evaluatestd').show(); 
    $("#stdid, #stdkode, #stdnama").val('');  
});

$(document).on('click', ".btEvaluasi", function() {
    // body...
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').hide();    

    $('#SubmitBlind').hide();   
    $('.evaluatestd').show();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);
    reloadblindtabel(data['ID'], false); 
});

  var example1 = document.getElementById('example1'), hot1;
  var oglbcomponent = new Array();
  var oglbcomponentid = new Array();
  var oglbcomponentname = new Array();

  document.querySelector('.addNewRow').addEventListener('click', function() {
    hot1.alter('insert_row', hot1.countRows());
  });

  document.querySelector('.delLastRow').addEventListener('click', function() {
    hot1.alter('remove_row', hot1.countRows() - 1);
  });

    function getListComponent() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_standardevaluasiblindtest/get_component");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_COMPONENT']);

          }
        }else{
        }
      });
      return datal;
    }

  function getContentData() {
      // return [
      // ];

      // var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_standardevaluasiblindtest/get_dataformcomponent");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          return values;
        }else{
          return [
          ];
        }
      });
      // return datal;
    }

    function getRowOptions() {
      var initial_coloptions = [
        // {
        //   data: 'produk',
        //   type: 'autocomplete', 
        //   source: getListComponent(),
        //   strict: true,
        //   allowInvalid: false,
        //   colWidths: 250,
        //   visibleRows: 10
        // },
        {
          type: 'text',
          // data: 'component',
          colWidths: 200,          
          readOnly: true,
          // editor: false
        },
        {
          type: 'numeric',          
          strict: true,
          allowInvalid: false,
          // data: 'vdata',
          // colWidths: 200,
          // editor: 'numeric'
        },
        {
          type: 'numeric',    
          strict: true,
          allowInvalid: false,
          // data: 'vsd',
          // colWidths: 200,
          // editor: 'numeric'
        },
        {
          type: 'text',
          // data: 'satuan',         
          readOnly: true,
          // colWidths: 200,
          // editor: false
        }
      ];
      return initial_coloptions;
    }

    function getColHeader() {
      var columnlist = ['Component', 'Value Data', 'Value SD', 'Satuan'];
      return columnlist;
    }

    function getTypeProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_standardevaluasiblindtest/ajax_get_type_product");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['KD_PRODUCT']);
          }
        }else{
        }
      });
      
      return datal;
    }

    function getTypeProduct(merk) {
      return glbproduk[merk];
    }

    function getListArea() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_KOTA']);
          }
        }else{
        }
      });
      return datal;
    }

  function loadtabelinput(idstd) {

    if (hot1) {

      hot1.destroy();

    }   

    var htdata = [];

    $.getJSON('<?php echo site_url("acclab_standardevaluasiblindtest/get_dataformcomponent");?>/'+idstd, function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          htdata = values;
        }

        $.getJSON('<?php echo site_url("acclab_standardevaluasiblindtest/ajax_get_component_display");?>', function (result) {
          var values = result;
          if (values != undefined && values.length > 0) {
            for (var i = 0; i < values.length; i++) {
              if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
                oglbcomponent.push(values[i]['KD_COMPONENT']);
                oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
                oglbcomponentname.push(values[i]['KD_COMPONENT']);
              }
            }
            hot1 = new Handsontable(example1, {
              // data: getContentData(),
              data: htdata,
              height: 500,              
              // width: '100%',
              // height: '100%',
              autoColumnSize : true,
              fixedColumnsLeft: 3,
              manualColumnFreeze: true,
              manualColumnResize: true,
              colHeaders: getColHeader(),
              columns: getRowOptions(),
            });
            if (values.length == 0) { hot1.alter('insert_row', hot1.countRows()); }           

            hot1.updateSettings({
              afterChange: function(changes, src) {
                var row = changes[0][0],
                  col = changes[0][1],
                  newVal = changes[0][3];
              }
            })

            // $('#delLastRow').show();
            // $('#addNewRow').show();
            $('#savesample').show();
          }else{
          }
        });

      });

  }

    // $('button[name=savesample]').click(function () {
    //   saveBlindTest();
    // });

    function saveBlindTest() {
        $body = $("body");
        $body.addClass("loading");
        $body.css("cursor", "progress");

      $('#savesample').button('loading');
        var handsonData = hot1.getData();
        $.ajax({
          url: "<?php echo site_url("acclab_standardevaluasiblindtest/save_standard_data_blindtest");?>",
          data: {"user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData, "FK_ID_STD": $('#stdid').val()}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              if (res.msg == 'success') {
                // $("#a-notice-success").click();
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
                var rowcount = hot1.countRows();
                for (var i = 0; i < rowcount; i++) {
                  hot1.alter('remove_row', hot1.countRows() - 1);
                }               
                hot1.alter('insert_row', hot1.countRows());
              }
              else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              $('#savesample').button('reset');              
              $('#entryBlindTest').hide();
              $('#viewArea').show();
              $("#ReloadData").click(); 

          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();

              $('#savesample').button('reset');
          }
        });

    }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
