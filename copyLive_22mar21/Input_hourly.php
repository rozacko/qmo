<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once (dirname(__FILE__) . "/incident.php");

class Input_hourly extends QMUser {

	public function __construct(){ 
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("C_range_component");
		$this->load->model("m_incident");
		$this->load->model("m_t_notifikasi");
        $this->load->model('TelegramNotificationTemplate');
        $this->load->model('TelegramNotification');
        $this->load->model('NotificationGroup');
        $this->load->model('M_opco');
        $this->load->library('libmail');
	}
	public function index(){
        $this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_hourly", $data);
	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY, $this->USER->ID_PLANT);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
		to_json($area);
	}

	private function get_component($id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'H');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->KD_COMPONENT);
		}

		//Tambahkan Remark
		if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';
		$header[]['title'] = 'KODE';
		$header[]['title'] = 'LOKASI';
		$header[]['title'] = 'TYPE';

		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';
		$id_comp[] = '_kode';
		$id_comp[] = '_lokasi';
		$id_comp[] = '_type';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_hourly
		$colHeader 	= array();
		$data 		= array();
		$t_prod 	= $this->t_production_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-5;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_hourly->get_by_id($row->ID_PRODUCTION_HOURLY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

	public function load_cement(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_hourly
		$colHeader 	= array();
		$data 		= array();
		if ($form->ID_GROUPAREA==4) {
            $this->db->select('mp.*')
                ->join('M_PRODUCT mp', 'mp.ID_PRODUCT = a.ID_PRODUCT', 'left');
			$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(a.DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."'");
		}else{
            $this->db->select('mp.*')
                ->join('M_PRODUCT mp', 'mp.ID_PRODUCT = a.ID_PRODUCT', 'left');
			$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(a.DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."' AND a.ID_PRODUCT='".$form->ID_PRODUCT."'");
		}
		#echo $this->db->last_query();die();
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-5;

        $produk_awal = $this->t_cement_hourly->produk_cek($form->ID_PRODUCT)->KD_PRODUCT;
        $original_produk_awal = $produk_awal;
		if (!empty($t_prod)) {
            $jam = 0;
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_cement_hourly->get_by_id($row->ID_CEMENT_HOURLY);
                $count_component_data = 0;
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;

                        if ($vl->NILAI != '') {
                            $count_component_data++;
                        }
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
                
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
                
                $produk_awal = str_replace("S","", str_replace("R","",$produk_awal));
                $jam +=1;
                $cek = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS);
                // if($cek->NM_MESIN_STATUS=='OFF'){
                    // mungkin maksudnya cek apa ada entri untuk pada tanggal, area, dan jam di tempat lain ?
                    $status_produk= $this->t_cement_hourly->data_where_cek("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."' AND KD_MESIN_STATUS IS NULL AND JAM_DATA='".$jam."'");
                    log_message('debug', 'SQL >> ' . $this->db->last_query());
                    log_message('debug', 'Status produk >> ' . json_encode($status_produk));
                    log_message('debug', 'Produk awal >> ' . $produk_awal);
                    if($form->ID_GROUPAREA==1){
                        // ada entri produk lain di tanggal, area, dan jam yang sama (nilai $status_produk ada)
                        // dan baris ini belum ada isinya
                        if(count($status_produk)!=0 && $count_component_data > 0){
                            $data[$key][] = 'R'.$row->KD_PRODUCT;
                            // if($jam=='1'&&$status_produk->KD_PRODUCT!=$produk_awal||$status_produk->KD_PRODUCT==$produk_awal){
                            //     $produk_awal = "R".$status_produk->KD_PRODUCT;
                            //     $data[$key][] =$produk_awal;
                            // }else if($jam=='1'&&$status_produk->KD_PRODUCT!=$produk_awal||$status_produk->KD_PRODUCT!=$produk_awal){
                            //     $produk_awal = "S".$status_produk->KD_PRODUCT;
                            //     $data[$key][] =$produk_awal;
                            // } else {
                            //     $data[$key][] = '';
                            // }
                        }else{
                            $data[$key][] ='';
                        }
                    }else{
                         $data[$key][] ='';
                    }
                    $data[$key][] = $row->LOCATION;
                    // yang ditampilkan disini adalah kode produk yang sudah pernah diinput nilainya
                    // $data[$key][] =$status_produk->KD_PRODUCT;

                    // sekarang kita gunakan langsung kode produk dari baris yang dimaksud
                    $data[$key][] = $row->KD_PRODUCT;
                // }else{
                    // $produk = $this->t_cement_hourly->produk_cek($form->ID_PRODUCT)->KD_PRODUCT;
                    // if($produk==$produk_awal){
                        // $produk_awal = "R".$produk;
                    // }else{
                        // $produk_awal = "S".$produk;
                    // }
                    // $data[$key][] =$produk_awal;
                    // $data[$key][] =$row->LOCATION;
                    // $data[$key][] =$produk;
                // }
                
			}

            // reverse loop untuk mengisi kolom kode yang kosong dengan nilai 
            // terdekat yang tidak kosong setelahnya
            $latest_kode = '';
            $latest_row = null;
            $col_index_kode = count($data[0]) - 3;
            for($j = count($data) - 1; $j >= 0; $j--) {
                if ( "" === $current_kode = $data[$j][$col_index_kode] ) {
                    $data[$j][$col_index_kode] = $latest_kode;
                }
                
                $latest_row = $j;
                $latest_kode = $current_kode;
            }
		}else{
            $l_header =  count($colHeader['id_comp'])-3;
            $date_now = date('d/m/Y');
            if($date_now==$form->TANGGAL){
                $jam_now = floatval(date('H'))-1;
            }else{
                $jam_now = 24;
            }
            $key= 0;

            // assignment berikut sebenarnya tidak perlu, 
            // sekedar utuk menyamakan logikanya dengan yang diatas (di blok if)
            $count_component_data = 0; 
            for($i=1;$i<$jam_now;$i++){
                for($x=1;$x<=$l_header;$x++){
                    
                    $data[$key][] = "";

                };
                $produk_awal = str_replace("S","", str_replace("R","",$produk_awal));
                  
                $status_produk= $this->t_cement_hourly->data_where_cek("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."' AND KD_MESIN_STATUS IS NULL AND JAM_DATA='".$i."'");
                
                 if($form->ID_GROUPAREA==1){
                    if(count($status_produk)!=0 && $count_component_data > 0){
                        $data[$key][] = $row->KD_PRODUCT;
                        // if($jam=='1'&&$status_produk->KD_PRODUCT!=$produk_awal||$status_produk->KD_PRODUCT==$produk_awal){
                        //     $produk_awal = "R".$status_produk->KD_PRODUCT;
                        //     $data[$key][] =$produk_awal;
                        // }else if($jam=='1'&&$status_produk->KD_PRODUCT!=$produk_awal||$status_produk->KD_PRODUCT!=$produk_awal){
                        //     $produk_awal = "S".$status_produk->KD_PRODUCT;
                        //     $data[$key][] =$produk_awal;
                        // }
                    }else{
                        $data[$key][] ='';
                    }
                 }else{
                     $data[$key][] ='';
                 }
                 
                $data[$key][] = "";
                $data[$key][] = $row->KD_PRODUCT; // $status_produk->KD_PRODUCT;
                 $key+=1;
                
            }
        }
            $combo_temp =array();
            $combo  =   $this->t_cement_hourly->get_combo_location("ID_COMPANY='" .$form->ID_COMPANY."'");
            foreach($combo as $val){
                array_push($combo_temp, $val['NM_STORAGE']);
            }

		$result['combo'] = $combo_temp;
		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

    private function fill_column_of_rows_with_value($data, $col_index, $row_range_index, $value) {
        foreach($row_range_index as $index) {
            if( !isset($data[$i]) ) {
                continue;
            }

            if ( !isset($data[$i][$col_index]) ) {
                continue;
            }

            $data[$i][$col_index] = $value;
        }

        return $data;
    } 

	public function save_table(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;

		//read line by line
		$jam_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if($row[($t_kolom-5)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}
			if ($mati == $t_kolom) {

				//BEFORE
				// $tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}


			$ID_PRODUCTION_HOURLY = $this->t_production_hourly->get_id($tdata[ID_AREA],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			$jam_auto++;

            $arrInputData = array();
			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_HOURLY'] 	= $ID_PRODUCTION_HOURLY;
				$ddata['ID_COMPONENT']					= $comp[$x];
				$ddata['NILAI']									= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']							= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if($ddata['NILAI']){
					if ($range) {
						$range = $range[0];

						if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
							$msg['result'] 	= 'nok';
							$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
							$msg['col']		= $x;
							$msg['row']		= $ct_row;
							to_json($msg);
							continue;
						}
					}else{
						to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
						continue;
					}
				}
			}
            
			$ct_row++;
		}

		$post = $this->input->post(); 
		$post['user_id'] = $this->USER->ID_USER;

		$dataParamas = urlencode(serialize($post));
		$params = "params={$dataParamas}"; 
		$url = base_url("Input_hourly/save_table_process_cli");
        $command = 'curl -d ' .$params. ' ' . $url . ' > /dev/null &';
        log_message('debug', $command);

		exec($command);

        to_json(array("result" => 'ok', "msg" => 'The process was successfully executed'));
	}

	public function save_table_process_cli(){
		$post = unserialize($_POST['params']);

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];
		$user_id = $post['user_id'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;

		//T_PRODUCTION_HOURLY (1): ID_PRODUCTION_HOURLY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_ENTRY,JAM_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_HOURLY (M): ID_PRODUCTION_HOURLY, ID_COMPONENT, NILAI

		//read line by line
		$jam_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if($row[($t_kolom-5)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}
			if ($mati == $t_kolom) {

				//BEFORE
				// $tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}


			$ID_PRODUCTION_HOURLY = $this->t_production_hourly->get_id($tdata[ID_AREA],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			#echo $this->db->last_query() . "\n";
			if(!$ID_PRODUCTION_HOURLY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_ENTRY'] = $user_id;
				$ID_PRODUCTION_HOURLY = $this->t_production_hourly->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_UPDATE'] = $user_id;


				$this->t_production_hourly->update($tdata,$ID_PRODUCTION_HOURLY);
				//echo $this->db->last_query() . "\n";
			}

			$jam_auto++;

            $arrInputData = array();
			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_HOURLY'] 	= $ID_PRODUCTION_HOURLY;
				$ddata['ID_COMPONENT']					= $comp[$x];
				$ddata['NILAI']									= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']							= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_production_hourly->d_exists($ddata)){
                        
                            // array_push($arrInputData, $ddata);
						$this->t_production_hourly->d_insert($ddata);
					}
					else{
						$this->t_production_hourly->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_production_hourly->d_exists($ddata)){
							$this->t_production_hourly->d_insert($ddata);
                            // array_push($arrInputData, $ddata);
						}
						else{
							$this->t_production_hourly->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}
				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}

				if(!$this->t_production_hourly->d_exists($ddata)){
					$this->t_production_hourly->d_insert($ddata);
                            // array_push($arrInputData, $ddata);
				}
				else{
					$this->t_production_hourly->d_update($ddata);
				}
			}
            
			$ct_row++;
		}
		to_json(array("result" => 'ok'));
	}

	public function save_table_cement(){

		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

        log_message('debug', " save_table_cement() params \n" . json_encode($post));

		foreach($form as $r){
			$tmp[$r['name']] = $r['value'];
		}

		$form = (object)$tmp;

		//read line by line
		$jam_auto = 1;
		$ct_row	  = 0;
        $sqlarray=array();
        $listID = array(); 

		foreach($data as $y => $row){ //y index

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);
			$i_lokasi		= array_search("_lokasi",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['LOCATION']		    = $row[$i_lokasi];
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			$exists = null;

            

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if($row[($t_kolom-5)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				// status -----------------------------------
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}

			if ($mati == $t_kolom) {

				//BEFORE
				// $tdata['ID_MESIN_STATUS'] = 0;
				$status_mesin = 'OFF';
			}
			$ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($tdata['ID_AREA'],$tdata['ID_PRODUCT'],$tdata['DATE_DATA'],$tdata['JAM_DATA']);


            // Jika sudah ada entri untuk area, tanggal, jam dan produk selain ini, maka error
            $any_row_data = array_filter(
                $row,
                function($cell, $key) use ($i_mesin_status){
                    // cek hanya cell yang menyimpan nilai.  Hitung jika ada isinya
                    return $key < $i_mesin_status && $cell !== '';
                },
                ARRAY_FILTER_USE_BOTH
            );

            // cek hanya jika ada isian nilai
            if ( $any_row_data ) {
                $entri_produk_lain = $this->t_cement_hourly->count_entry_product_another_than(
                    $tdata['ID_PRODUCT'],
                    $tdata['ID_AREA'],
                    $tdata['DATE_DATA'],
                    $tdata['JAM_DATA'] ,
                    $ID_CEMENT_HOURLY
                );

                log_message('debug', 'SQL count entri other product ' . $this->db->last_query());

                if ( FALSE === $entri_produk_lain ) {
                    to_json(array(
                        "result" => 'nok',
                        "msg" => 'Unknown error while checking other product entry'
                    ));
                    return;
                }

                if ( $entri_produk_lain > 0 ) {
                    to_json(array(
                        "result" => 'nok',
                        "msg" => sprintf('Entry on hour %s already exists for other Product Type', $tdata['JAM_DATA'])
                    ));
                    return;
                }

            }

			$jam_auto++;
            $arrInputData = array();
            $arrEditData = array();
            for($x=0;$x<$i_mesin_status;$x++){
                $ddata = null;
                $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                $ddata['ID_COMPONENT']		= $comp[$x];
                $ddata['NILAI']				= $this->str_clean($row[$x]);
                $ddata['NO_FIELD']			= "$x";
                
                /* Check Mesin Status, Data NULL if Status OFF */
                if ($status_mesin=='OFF'){
                    $ddata['NILAI']	= '';
                }

                /* Check Global Range */
                $range = $this->C_range_component->get_id($comp[$x]);
                $range = $range[0];

                if($ddata['NILAI']){
	                if ($range) {
	                    if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {

	                        $msg['result'] 	= 'nok';
	                        $msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of of Range";
	                        $msg['col']		= $x;
	                        $msg['row']		= $ct_row;
	                        to_json($msg);
	                        continue;
	                    }
	                }else{
	                    to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
	                    continue;
	                }
                }



            } 
			$ct_row++;
		}

		$post = $this->input->post(); 
		$post['user_id'] = $this->USER->ID_USER;

		$dataParamas = urlencode(serialize($post));
		$params = "params={$dataParamas}";
		$url = base_url("Input_hourly/save_table_cement_process_cli");

        // FOR DEBUGGING
        // simple random char to create unique (?) log tracking ID
        $bytes = random_bytes(10); $log_tracking_id = bin2hex($bytes);
        $log_tracking_params = "log_tracking_id=$log_tracking_id";

        $curl_output = APPPATH . "/logs/curl-save_table_cement_process_cli-" . $log_tracking_id . ".log";

        $command = ' curl -v -d ' .$params. ' -d ' . $log_tracking_params . " --output $curl_output " . $url . ' > /dev/null  &';
        log_message('debug', $command);
		exec($command);

        to_json(array("result" => 'ok', "msg" => 'The process was successfully executed'));
	}

	public function setTestExec(){
		$data = unserialize($_POST['data']);
		echo "string";
		for($i=1; $i<=50; $i++){
			$data['ID'] = $i;
			$this->m_runbg->insert($data);
		}
	}

	public function save_table_cement_process_cli(){
        ini_set('max_execution_time', 600);

		// $post = $this->input->post();
		$post = unserialize($_POST['params']);
        $log_tracking_id = $this->input->post('log_tracking_id'); 

        log_message('debug', 'Start tracking ID '. $log_tracking_id. ' | save_table_cement_process_cli() params ' . json_encode($post));

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];
		$user_id = $post['user_id'];

		foreach($form as $r){
			$tmp[$r['name']] = $r['value'];
		}

		$form = (object)$tmp;

		//read line by line
		$jam_auto = 1;
		$ct_row	  = 0;
        $sqlarray=array();
        $listID = array(); 

		foreach($data as $y => $row){ //y index
            log_message('debug', 'Tracking ID '. $log_tracking_id. ' | Row ke '. $y);

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);
			$i_lokasi		= array_search("_lokasi",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['LOCATION']		    = $row[$i_lokasi];
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if($row[($t_kolom-5)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				// status -----------------------------------
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}

			if ($mati == $t_kolom) {

				//BEFORE
				// $tdata['ID_MESIN_STATUS'] = 0;
				$status_mesin = 'OFF';
			}
			$ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($tdata['ID_AREA'],$tdata['ID_PRODUCT'],$tdata['DATE_DATA'],$tdata['JAM_DATA']);
			if(!$ID_CEMENT_HOURLY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_ENTRY'] = $user_id;
				$ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);

			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_UPDATE'] = $user_id;
				$this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
			}

			// INFO menyebabkan bug lompatan jam entri +1 pada pengirman data NCQR SP_NCQR_CEMENT() --> stored procedure
			// kita increment-kan diakhir loop
			// $jam_auto++;

			//D
            // if($form->ID_AREA=="21"){
                $arrInputData = array();
                $arrEditData = array();
                for($x=0;$x<$i_mesin_status;$x++){
                    $ddata = null;
                    $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                    $ddata['ID_COMPONENT']		= $comp[$x];
                    $ddata['NILAI']				= $this->str_clean($row[$x]);
                    $ddata['NO_FIELD']			= "$x";
                    
          			$paramsNcqr['ID_PLANT'] = $form->ID_PLANT; 
					$paramsNcqr['ID_AREA'] = $form->ID_AREA; 
					$paramsNcqr['ID_COMPONENT'] = $comp[$x];
					$paramsNcqr['NILAI'] = $this->str_clean($row[$x]); 
					$paramsNcqr['TANGGAL'] = "'".date('d/m/Y', strtotime(str_replace('/', '-', $form->TANGGAL)))."'"; //d-M-y
					$paramsNcqr['JAM_DATA'] = $jam_auto; 
					$paramsNcqr['ID_CEMENT_HOURLY'] = $ID_CEMENT_HOURLY;
					$paramsNcqr['ID_PRODUCT'] = $form->ID_PRODUCT;
		  			$paramsNcqr['ID_MESIN_STATUS'] = $tdata['ID_MESIN_STATUS'];
                    $paramsNcqr['TANGGAL_INSIDEN'] = $paramsNcqr['TANGGAL'];

                    /* Check Mesin Status, Data NULL if Status OFF */
                    if ($status_mesin=='OFF'){
                        $ddata['NILAI']	= '';
                        if(!$this->t_cement_hourly->d_exists($ddata)){
                            $this->t_cement_hourly->d_insert($ddata);

                        } 
                        else{
                            $this->t_cement_hourly->d_update($ddata);
                        }
                        continue;
                    }

                    /* Check Global Range */
                    $range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);

                    if ($range) {
                        $range = $range[0];

                        if ($ddata['NILAI']=='') {
                            if(!$this->t_cement_hourly->d_exists($ddata)){
                                // array_push($arrInputData, $ddata);
                                $this->t_cement_hourly->d_insert($ddata);
                            }
                            else{
                            	$this->t_cement_hourly->d_update($ddata);
                            }
                            continue;
                        }

                        if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
                            $msg['result'] 	= 'nok';
                            $msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
                            $msg['col']		= $x;
                            $msg['row']		= $ct_row;
                            to_json($msg);
                            continue;
                        }
                    }else{
                        to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
                        continue;
                    }

                    if(!$this->t_cement_hourly->d_exists($ddata)){
                        $this->t_cement_hourly->d_insert($ddata);
                    }
                    else{
                        $this->t_cement_hourly->d_update($ddata);
                    }

                    // Set NCQR -------------------
                    // Disable this due to already done at D_CEMENT_HOURLY trigger
                    // log_message('debug', 'PARAMS NCQR ' . json_encode($paramsNcqr));
                    // $this->m_incident->chek_and_setNCQR($paramsNcqr);

                    //Check NCQR ---------------------
                    $paramIncident['ID_AREA'] = $form->ID_AREA;
                    $paramIncident['ID_NCQR_STATUS'] = 1;
                    $paramIncident['SEND_NOTIF'] = 'N';

					if($form->ID_GROUPAREA == 1){
			    		$paramIncident['ID_COMPONENT'] = '15,13,7';
			    		$paramIncident['ID_PRODUCT'] = $form->ID_PRODUCT;
			    	}else{
			    		$paramIncident['ID_COMPONENT'] = '12';
			    	}

                    $paramIncident['ID_GROUPAREA'] = $form->ID_GROUPAREA;

					$incident = $this->m_incident->ncqr_incident($paramIncident);
                    log_message('debug', 'NCQR Incident params ' . json_encode($paramIncident));
                    log_message('debug', sprintf(
                        'NCQR Incident count %s. %s notification',
                        count($incident),
                        count($incident) > 0 ? 'Processing' : 'No'
                    ));

					if(count($incident) > 0){ 
						log_message('debug', 'should invoke getIncident()');
						$listID = $this->getIncident($paramIncident);
					}

                } 
			$ct_row++;

			// jam_auto dipindah kesini
			$jam_auto++;
		}
		$this->m_incident->set_SendNotif(null, 'N');
        
        log_message('debug', 'End tracking ID ' . $log_tracking_id);
	}

	//FASE 2-----------------------------------


	public function insert_from_qcx(){
        // $tanggal = $this->input->post('tanggal');
        // $myDateTime = DateTime::createFromFormat('d/m/Y', $tanggal);
        // $tanggal_c = $myDateTime->format('Ymd');
        // $tanggal_c = '20180810';
        $tanggal = date('d/m/Y');
        $tanggal_c = date("Ymd");
		$data =  file_get_contents("http://10.15.5.150/dev/par4digma/api/index.php/plant_rembang/dump_qmtbl_clinker?ymd=".$tanggal_c."");
        $decode = json_decode($data, TRUE);
        // print_r($decode);exit;
        // $id_area = $this->input->post('area');
        $id_area = "88"; // Area rembang
        $id_product = '';
        // $plant = $this->input->post('plant');
        $plant = "18"; // Plant clinker
        // $grouparea = $this->input->post('grouparea');
        $grouparea = "4"; // Kiln
        $component = $this->c_parameter->configuration($plant, $grouparea,'H');
        
        if($tanggal==date('d/m/Y')){
            $now = date('H');
        }else{
            $now = 24;
        }
        $cek_insert = 0;
        $cek_update = 0;
        for($i=1;$i<=$now;$i++){
            $ada = '0';
            $ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($id_area,$id_product,$tanggal,$i);
            $tdata['ID_AREA']			= $id_area;
            $tdata['ID_PRODUCT']		= $id_product;
            $tdata['ID_MESIN_STATUS']	= '';
            $tdata['LOCATION']		    = '';
            $tdata['MESIN_REMARK']		= '';
            $tdata['DATE_DATA']			= $tanggal; # dd/mm/yyyy
            $tdata['JAM_DATA']			= $i;
            $status_mesin 				= '';
            
            foreach($decode  as $val){
                $jam_data=intval($val['hourmin'])+1;
                if($jam_data==$i){
                    $ada = '1';
                    // PROSES gg
                    if(!$ID_CEMENT_HOURLY){
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                        $cek_insert += 1;
                        $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                    }
                    else{
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_UPDATE'] = $this->USER->ID_USER;
                        $cek_update += 1;
                        $this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
                    }
                    // NIlai COMPONENT
                    
                    
                    $no_field=0;
                    foreach ($component as $col) {
                        $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                        $nm = trim($cmp->KD_COMPONENT," ");
                        if($nm=='C3S'){
                            $nilai = $val['cl_c3s'];
                        }else if($nm=='FCaO'){
                            $nilai =$val['cl_fcao'];
                            // $nilai =$val['cl_fcao'];
                        }else if($nm=='LSF'){
                            $nilai =$val['cl_lsf'];
                        }else if($nm=='TEMP'){
                            $nilai =$val['cl_temp'];
                        }else{
                            $nilai ='';
                        }
                        
                            $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                            $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                            $ddata['NILAI']				= $this->str_clean($nilai);
                            $ddata['NO_FIELD']			= "$no_field";
                            
                            if(!$this->t_cement_hourly->d_exists($ddata)){
                                $this->t_cement_hourly->d_insert($ddata);
                            }
                            else{
                                if($nm=='C3S'||$nm=='FCaO'||$nm=='LSF'||$nm=='TEMP'){
                                    $this->t_cement_hourly->d_update($ddata);
                                }
                            }
                            
                            $no_field +=1;
                    }
                }
            }
                if($ada == '0'){
                // Hanya insert
                    if(!$ID_CEMENT_HOURLY){
                        
                        $cek_insert += 1;
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                            
                         $cek +=1;
                        // echo '1';
                        $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                        
                        $cek +=1;
                        $no_field=0;
                        foreach ($component as $col) {
                            $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                            $nm = trim($cmp->KD_COMPONENT," ");
                            $nilai ='';
                        
                            $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                            $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                            $ddata['NILAI']				= $this->str_clean($nilai);
                            $ddata['NO_FIELD']			= "$no_field";
                            
                        
                            $this->t_cement_hourly->d_insert($ddata);
                            
                            $no_field +=1;
                        }
                    }
                }
        }

		$paramIncident['ID_AREA'] = $id_area;
		if($id_area == 1){
    		$paramIncident['ID_COMPONENT'] = '15,13,7';
    	}else{
    		$paramIncident['ID_COMPONENT'] = '12';
    	}

		$incident = $this->m_incident->ncqr_incident($paramIncident);

		$statusInc = 'ok';

		$param = array(
			'ID_AREA' => $id_area,
			'ID_GROUPAREA' => $paramIncident['ID_COMPONENT']
		);

		if(count($incident) > 0){
			$this->getIncident($param);
		}
        echo "berhasil";
    	// to_json(array("insert" => $cek_insert,"update" => $cek_update,"result" => 'ok'));      

	}
    

	private function str_clean($chr=''){
		$str ='([^.0-9-]+)';
		return preg_replace($str, '', $chr);
	}

	public function tes($value=''){
		var_dump($this->str_clean('+-'));
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color = getFixColor($i);
			$nilai = array_filter($trace[$i], 'is_numeric');

			if(empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= $comp[$i]->title;
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;

			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}


	// //CHECK NCQR ----------------------------------
	// public function setNCQR($params){
		
	//   	//Check_NCQRComponent ----------------------
	//   	$result = $this->m_incident->chek_and_setNCQR($params);
	// }

	public function getIncident($post = array()){

 		// $post = $this->input->post();
   //      $post = array(
   //          'ID_AREA' => 88,
   //          'ID_GROUPAREA' => 4
   //      ); 
 
        $param['ID_AREA'] = $post['ID_AREA'];
		$param['SEND_NOTIF'] = 'N';

        if($post['ID_GROUPAREA'] == 1){
            $param['ID_COMPONENT'] = '15,13,7';
        }else{
            $param['ID_COMPONENT'] = '12';
            unset($param['ID_PRODUCT']);
        }

        $param['ID_NCQR_STATUS'] = 1;
        
        $incident = $this->m_incident->ncqr_incident($param);
        $listID = array();
        log_message('debug', 'the incidents inside getIncident() ' . json_encode($incident));

        foreach ($incident as $i => $iv) {
        	$listID[] = $iv['ID_INCIDENT'];

            $detail = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']);
            $detail_distinct = $this->m_incident->get_data_distinct_by_id($iv['ID_INCIDENT']);
            log_message('debug', 'NCQR detail incident SQL ' . $this->db->last_query());
            log_message('debug', 'NCQR detail incident data ' . json_encode($detail));
            log_message('debug', 'NCQR detail incident distinct data ' . json_encode($detail_distinct));

            // use distinct list here
            $detail = $detail_distinct;

            $max_detail_data_show = 3;
            // selain EQR tampilkan hanya 3 detail data terakhir
            if ( $detail && $iv['ID_INCIDENT_TYPE'] != 4 ) {
                if (count($detail) > $max_detail_data_show) {
                    $detail = array_slice($detail, count($detail) - $max_detail_data_show);
                }
            }

            // $incident[$i]['DETAIL'] = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']); # echo $this->m_incident->get_sql();
            $subject = $iv['NM_COMPANY'].' ('.$iv['NM_PLANT'].' - '.$iv['NM_AREA'].')';
        
            switch ($iv['ID_INCIDENT_TYPE']) {
                case '1':
                    $listJabatan = '1';
                    break;
                case '2':
                    $listJabatan = '1,2';
                    break; 
                case '3':
                    $listJabatan = '1,2,3';
                    break;
                case '4':
                    $listJabatan = '1,2,3,21';
                default:
                    $listJabatan = '1,2,3,21';
            }
            $paramMail['b.ID_AREA'] = $param['ID_AREA'];
            $paramMail['ID_JABATAN'] = $listJabatan;


            // Begin EMAIL SERVICE
            $listMail = $this->m_incident->mail_incident($paramMail);
            // echo $this->db->last_query();
            //
            log_message('debug', 'Send notif to list of ' . json_encode($listMail));
            
            $to = array();
            $cc = array();
            if($this->serverHost() == 'DEV'){
                $to[] = 'dummymail@example.com';
                $cc = array('95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');
            }else{
                foreach ($listMail as $j => $jv) {
                    if($iv['TEMBUSAN'] == 1){
                        $cc[] = $jv['EMAIL'];
                    }else{
                        $to[] = $jv['EMAIL'];
                    }
                }
            }

            $output = array(
                'URL'       => base_url('incident/solve/'.$iv['ID_INCIDENT']),
                'INCIDENT'  => $iv,
                'DETAIL'    => $detail,
                'MAIL'      => array('to'=>$to,'cc'=>$cc),
                'TO'        => $to,
                'CC'        => $cc,
                'SUBJECT'   => $subject
            );

            $msg = $this->libmail->ncqr($output);

            $this->load->library('email');
            $this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');

            $this->email->to($to); 
            $this->email->cc($cc);
            
            $this->email->subject($subject);
            $this->email->message($msg);

            log_message('debug', sprintf(
                "Send NCQR Notif email (ID Incident: %s)\n".
                "To: %s\n".
                "CC: %s\n".
                "Subject: %s\n".
                "Body: %s\n",
                $iv['ID_INCIDENT'],
                json_encode($to),
                json_encode($cc),
                $subject,
                $msg
                )
            );

            // TODO commented only for development
            // if ( true ) {
            if($this->email->send()){
                $status = 'OK';
                foreach ($listMail as $j => $jv) {
                 	$this->m_t_notifikasi->insert($jv['ID_OPCO'], $iv['ID_INCIDENT'], $jv['ID_JABATAN'], $iv['ID_INCIDENT_TYPE']);   
				}

            }else{
                $status = 'FAIL';
            }

            // /End EMAIL SERVICE

            // Begin TELEGRAM SERVICE
            $plain_product_components = '';
            foreach($detail as $det) {
                $plain_product_components .=
                    "Component: " . trim($det->KD_COMPONENT) . "\n" .
                    "Time: " . $det->JAM_ANALISA . "\n" .
                    "Analize: " . $det->ANALISA . "\n" .
                    "Standard: " . $det->NILAI_STANDARD_MIN . " - " . $det->NILAI_STANDARD_MAX . "\n" .
                    "\n";
            }


            $telegram_message = $this->TelegramNotificationTemplate
                ->buildMessageFromTemplate(
                    'default',
                    [
                        'KD_COMPONENT'      => trim($iv['KD_COMPONENT']),
                        'NM_INCIDENT_TYPE'  => $iv['NM_INCIDENT_TYPE'] ? $iv['NM_INCIDENT_TYPE'] : "EQR",
                        'COMPANY'           => $iv['NM_COMPANY'],
                        'PLANT'             => $iv['NM_PLANT'],
                        'AREA'              => $iv['NM_AREA'],
                        'PRODUCT'           => $iv['NM_PRODUCT'] ? trim($iv['NM_PRODUCT']) : '-',
                        'PRODUCT_COMPONENTS'=> $plain_product_components,
                        'LINK_SOLVE_NCQR'   => $output['URL'],
                    ]
                );


            foreach($listMail as $opco_member) {
                $opco_member = (object) $opco_member;

                log_message('debug', sprintf(
                    "Send NCQR Notif telegram (ID Incident: %s)\n".
                    "Opco: %s\n".
                    "CHAT_ID: %s\n".
                    "Fullname: %s\n".
                    "Message: %s\n",
                    $iv['ID_INCIDENT'],
                    json_encode($opco_member),
                    $opco_member->TELEGRAM_CHAT_ID,
                    $opco_member->FULLNAME,
                    $telegram_message
                ));

                $this->TelegramNotification->send_to_member(
                    $opco_member,
                    $telegram_message
                 );
            }
             
            // /End TELEGRAM SERVICE
            
            // Begin TELEGRAM GROUP SERVICE
            
            // Note $iv -> incident
            $incident = is_object($iv) ? $iv : (object) $iv;

            $notifier_group = $this->NotificationGroup;

            log_message('debug', 'INCIDENT '.json_encode($incident));

            array_map(
                function($id_jabatan) use($notifier_group, $incident, $telegram_message){
                    $opco_notification_group = $notifier_group->find_one_by_area_jabatan($incident->ID_AREA, $id_jabatan);

                    log_message('debug', 'Send Telegram notif to Group '. json_encode($opco_notification_group) . "\n" . $telegram_message);

                    if (!$opco_notification_group ) {
                        log_message('debug', 'No OPCO Notification Group found for ');

                        return false;
                    }

                    log_message('debug', sprintf(
                        "Send NCQR Notif telegram group (ID Incident: %s)\n".
                        "Opco notification group: %s\n".
                        "CHAT_ID: %s\n".
                        "ID Area: %s\n".
                        "ID Jabatan: %s\n".
                        "Message: %s\n",
                        $incident->ID_INCIDENT,
                        json_encode($opco_notification_group),
                        $opco_notification_group->TELEGRAM_CHAT_ID,
                        $opco_notification_group->ID_AREA,
                        $opco_notification_group->ID_JABATAN,
                        $telegram_message
                    ));

                    return $notifier_group->send_message_to_group(
                        $opco_notification_group,
                        $telegram_message
                    );

                },
                explode(',', $listJabatan)
            );

            // /End TELEGRAM GROUP SERVICE
        }

        log_message('debug' , 'listID ' . json_encode($listID));

        if ( $listID ) {
            $this->m_incident->set_SendNotif($listID, 'Y');
            log_message('debug', 'SQL update SEND NOTIF  ' . $this->db->last_query());
        }
		return $listID;
    }
}

/* End of file Input_hourly.php */
/* Location: ./application/controllers/Input_hourly.php */
?>
