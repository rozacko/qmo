<?php

class Authority extends QMUser { 
	 
	public $list_authority = array();
	public $list_menu	   = array();
	public $list_usergroup = array();
	public $list_groupmenu = array();
	public $data_authority;
	
	public function __construct(){
		parent::__construct(); 
		$this->load->helper("string");

		$this->load->model("m_authority");
		$this->load->model("m_menu"); 
		$this->load->model("m_usergroup");
		$this->load->model("m_groupmenu");
	}
	
	public function index(){  
		$this->libExternal('select2');
		$this->list_menu	  = $this->m_menu->datalist2();
		// var_dump($this->list_menu);
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->list_groupmenu = $this->m_groupmenu->datalist();
		$this->template->adminlte("v_authority");
	}
	
	
	public function authlist($ID_USERGROUP=NULL){
		echo json_encode($this->m_authority->datalist($ID_USERGROUP));
	} 
	
	public function auth($ID_USERGROUP,$ID_MENU){
		// print_r($data);
		// exit;
		$data['ID_USERGROUP'] = $ID_USERGROUP;
		$data['ID_MENU']	  = $ID_MENU;
		$data['READ']	  		= 1;
		$wdata['ID_MENU']	  = $ID_MENU;
		
		$wparent['ID_MENU'] = $this->m_menu->data($wdata)->PARENT;
		$parent = $this->m_authority->data($wparent);

		if(count($parent) == 0){
			$dataPartent['ID_USERGROUP'] = $ID_USERGROUP;
			$dataPartent['ID_MENU'] = $wparent['ID_MENU'];
			$dataPartent['READ']	  		= 1;

			$this->m_authority->insert($dataPartent);
		}

		$menu = $this->m_authority->data($data);
		if(count($menu) == 0){
			$this->m_authority->insert($data);
		}

	}
	
	public function deauth($ID_USERGROUP,$ID_MENU){
		$data['ID_USERGROUP'] = $ID_USERGROUP;
		$data['ID_MENU']	  = $ID_MENU;
		
		$wdata['ID_MENU']	  = $ID_MENU;
		$this->m_authority->delete($data);

		$wparent['PARENT'] = $this->m_menu->data($wdata)->PARENT;
		$parent = $this->m_menu->datachild($wparent);
		
		$par = array();
		foreach ($parent as $key => $value) {
			$par[] = $value['ID_MENU'];
		}
		$strparent = join(',', $par);
		
		$where['ID_USERGROUP'] = $ID_USERGROUP;
		$where['ID_MENU'] = $strparent;
		$listauth = $this->m_authority->dataIn($where);
		// echo $this->db->last_query();

		if(count($listauth) == 0){
			$datap['ID_USERGROUP'] = $ID_USERGROUP;
			$datap['ID_MENU']	  = $wparent['PARENT'];
			$this->m_authority->delete($datap);
		}
	}

	public function write($ID_USERGROUP,$ID_MENU){
		$where['ID_USERGROUP'] 	= $ID_USERGROUP;
		$where['ID_MENU']	  	= $ID_MENU;
		$data['WRITE']	  		= 1;

		$this->m_authority->update($data, $where);
	}

	public function dewrite($ID_USERGROUP,$ID_MENU){
		$where['ID_USERGROUP'] 	= $ID_USERGROUP;
		$where['ID_MENU']	  	= $ID_MENU;
		$data['WRITE']	  		= null;

		$this->m_authority->update($data, $where);
	}

	public function get_detlist($id_groupmenu){
		$list = $this->m_menu->get_list($id_groupmenu);
		// print_r($list);
		// echo $this->db->last_query();
		$data = array();
		$no   = 1;

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_MENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NM_MENU;
			$row[] = $column->URL_MENU;
			$row[] = $column->ID_GROUPMENU;
			$data[] = $row;	
		}

		$output = array(
            // "draw" => $this->input->post('draw'),
            // "recordsTotal" => $this->m_menu->count_all($id_groupmenu),
            // "recordsFiltered" => $this->m_menu->count_filtered($id_groupmenu),
            "data" => $data,
		);
		
		// return $output;
		to_json($output);
	}

}	

?>
