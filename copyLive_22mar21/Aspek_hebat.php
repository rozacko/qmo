<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspek_hebat extends QMUser {

	public $LIST;
	public $JENIS_ASPEK;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_aspek_hebat');
	}
	
	public function index(){
		$this->template->adminlte("v_aspek_hebat");
	}

	public function add(){
		$this->template->adminlte("v_aspek_hebat_add");
	}

	public function edit(){
		$this->LIST = $this->M_aspek_hebat->get_by_id($this->input->post('ID_ASPEK'));
		$this->template->adminlte("v_aspek_hebat_edit", $list);
	}

	public function get_list(){
		$list = $this->M_aspek_hebat->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_ASPEK;
			$row[] = $no;
			$row[] = $column->ASPEK;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_aspek_hebat->count_all(),
            "recordsFiltered" => $this->M_aspek_hebat->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		$this->M_aspek_hebat->insert(array_map('strtoupper', $this->input->post()));
		if($this->M_aspek_hebat->error()){
			$this->notice->error($this->M_aspek_hebat->error());
			redirect("aspek_hebat/add");
		}
		else{
			$this->notice->success("Aspek Saved.");
			redirect("aspek_hebat");
		}
	}

	public function update(){
		$data["ASPEK"] = strtoupper($this->input->post('ASPEK'));

		$this->M_aspek_hebat->update($this->input->post('ID_ASPEK'), $data);
		
		if($this->M_aspek_hebat->error()){
			$this->notice->error($this->M_aspek_hebat->error());
			redirect("aspek_hebat/edit");
		}
		else{
			$this->notice->success("Aspek Updated.");
			redirect("aspek_hebat");
		}
	}

	public function remove(){
		$this->M_aspek_hebat->delete($this->input->post('ID_ASPEK'));
		if($this->M_aspek_hebat->error()){
			$this->notice->error($this->M_aspek_hebat->error());
		}
		else{
			$this->notice->success("Aspek Removed.");
		}
		redirect("aspek_hebat");
	}

}

/* End of file Aspek_hebat.php */
/* Location: ./application/controllers/Aspek_hebat.php */
?>