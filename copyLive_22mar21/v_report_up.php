
<section class="content-header">
  <h1>
     Report (Uji Proficiency Nasional / Internasional)
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">
			
            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap"
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th >No.</th>
					<th >Periode Proficiency</th>
	                <th >Commodity</th>
					<th >PIC</th>
					<th>Laporan (.pdf)</th>
                    <th>File Excel</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
					<td><?= $dt->TITLE_PP;?> [<?= $dt->GROUP_PP;?> - <?= $dt->YEAR_PP;?>]</td>
                    <td><?= $dt->NAMA_SAMPLE;?> </td>
					<td><?= $dt->FULLNAME;?> </td>
					<td>
					<?php
						if($dt->FILE_LAPORAN != null){ ?>
						 <a href="<?php echo site_url('assets/uploads/laporan_proficiency/'); ?><?= $dt->FILE_LAPORAN ?>" target="_blank"> Lihat disini </a>
					<?php
							$ket = "Reupload";
						} else {
							echo "Belum Tesedia";
							$ket = "Upload";
						}
					?>
					</td>
                    <td>
					<?php
						if($dt->FILE_SAMPLE != null){ ?>
						 <a href="<?php echo site_url('assets/uploads/laporan_proficiency/'); ?><?= $dt->FILE_SAMPLE ?>" target="_blank"> Lihat disini </a>
					<?php
							$ket1 = "Reupload";
						} else {
							echo "Belum Tesedia";
							$ket1 = "Upload";
						}
					?>
					</td>
                    <td> 
                    <?php if($this->PERM_WRITE): ?>
                      <a ><button title="Upload Laporan (.pdf)" class="btEdit btn btn-primary btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID_PROFICIENCY;?>"><i class="fa fa-upload"></i> <?= $ket;?> Laporan</button></a>
					  <a ><button title=" (.pdf)" class="btEdit btn btn-default btn-xs" type="button" data-toggle="modal" data-target="#upModal<?= $dt->ID_PROFICIENCY;?>"><i class="fa fa-upload"></i> <?= $ket1;?> Excel</button></a>
                    <?php endif; ?>
                    </td>
                  </tr>
				  
<!-- Modal  -->
<div id="editModal<?= $dt->ID_PROFICIENCY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("report_up/do_upload/pdf"); ?>" enctype="multipart/form-data">
    <!-- Modal content-->
	 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Laporan Proficiency:</b> <?= $dt->TITLE_PP." ".$dt->GROUP_PP." ".$dt->YEAR_PP." - ".$dt->NAMA_SAMPLE; ?></h4>
      </div>
      <div class="modal-body">
		<?php
			$this->load->library('form_validation');
			$this->load->helper('url');
			$slug = url_title($dt->ID_PROFICIENCY." ".$dt->TITLE_PP." ".$dt->GROUP_PP." ".$dt->YEAR_PP." ".$dt->NAMA_SAMPLE);
		?>
				<input type="hidden" name="id_pro" value="<?= $dt->ID_PROFICIENCY;?>" />
				<input type="hidden" name="slug" value="<?= $slug;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-12 clearfix">
					<label>Upload Laporan </label>
					<input type="file" class="form-control" name="file" accept="application/pdf">
				  </div>
				  
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

<!-- Modal  -->
<div id="upModal<?= $dt->ID_PROFICIENCY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("report_up/do_upload/exc"); ?>" enctype="multipart/form-data">
    <!-- Modal content-->
	 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>File Excel Proficiency:</b> <?= $dt->TITLE_PP." ".$dt->GROUP_PP." ".$dt->YEAR_PP." - ".$dt->NAMA_SAMPLE; ?></h4>
      </div>
      <div class="modal-body">
		<?php
			$this->load->library('form_validation');
			$this->load->helper('url');
			$slug = url_title($dt->ID_PROFICIENCY." ".$dt->TITLE_PP." ".$dt->GROUP_PP." ".$dt->YEAR_PP." ".$dt->NAMA_SAMPLE);
		?>
				<input type="hidden" name="id_pro" value="<?= $dt->ID_PROFICIENCY;?>" />
				<input type="hidden" name="slug" value="<?= $slug;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-12 clearfix">
					<label>Upload File Excel </label>
					<input type="file" class="form-control" name="file" accept=".xls,.xlsx">
				  </div>
				  
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>
				  
				  
                <?php } ?>
                </tbody>
	          </table>
            </div>
            
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>