<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_qbb_data extends DB_QM {

    function __construct(){
        parent::__construct();
    }
    
    public function get_data_peryear(){
        $year = date('Y');
        //--WHERE TO_CHAR(CREATE_AT, 'YYYY') = '$year'
        $sql = "
            SELECT * FROM T_KUALITAS_MATERIAL 
            ORDER BY CREATE_AT DESC   
        ";
        return $this->db->query($sql)->result_array();
    }
    
    public function get_data_periode($start, $end){
        $sql = "
            SELECT * FROM T_KUALITAS_MATERIAL 
                WHERE TO_CHAR(CREATE_AT, 'YYYY-MM-DD') BETWEEN '$start' AND '$end'
              ORDER BY CREATE_AT DESC
        ";
        return $this->db->query($sql)->result_array();
    }
	
    public function data_from_sap($dt_in){
        $row_insert = 0;
        $result = array();
        foreach ($dt_in as $data) {
            $date_at 	= date('d-m-Y', strtotime($data['R_BUDAT']));
            
            $result = array(             
                'KD_MATERIAL'           => $data['R_MATNR'],
                'NAMA_MATERIAL'         => $data['R_MAKTX'],
                'KD_VENDOR'             => (int)$data['R_LIFNR'], 
                'NAMA_VENDOR'           => $data['R_LIFNRNM'],
                'KD_PLANT'              => (int)$data['R_WERKS'],
                'NAMA_PLANT'            => $data['R_WERKSNM'],
                'CREATE_AT'             => $date_at,
                'TONASE'                => (float)$data['R_QTY'],
                'TOTAL_MOISTURE'        => (float)$data['R_TM'],
                'INHERENT_MOISTURE'     => (float)$data['R_IM'],
                'ASH_CONTENT'           => (float)$data['R_AC'],
                'CALORI_AR'             => (float)$data['R_GHVARB'],
                'CALORI_ADB'            => (float)$data['R_GHVADB'],
                'ID_COMPANY'            => $data['R_BUKRS']
            );      
            $cek = count($this->is_existing($result));
            if($cek == 1){
                continue;
            } else {
                $input_data = $this->set_data_to_qmo($result);
                if($input_data){
                    $row_insert++;
                }
            }
        } 
        return $row_insert;
    }
    
    private function is_existing($data){
        $KD_MATERIAL      = $data['KD_MATERIAL'];
        $NAMA_MATERIAL    = $data['NAMA_MATERIAL'];
        $KD_VENDOR        = $data['KD_VENDOR']; 
        $NAMA_VENDOR           = $data['NAMA_VENDOR'];
        $KD_PLANT              = $data['KD_PLANT'];
        $NAMA_PLANT            = $data['NAMA_PLANT'];
        $CREATE_AT             = $data['CREATE_AT'];
        $TONASE                = $data['TONASE'];
        $TOTAL_MOISTURE        = $data['TOTAL_MOISTURE'];
        $INHERENT_MOISTURE     = $data['INHERENT_MOISTURE'];
        $ASH_CONTENT           = $data['ASH_CONTENT'];
        $CALORI_AR             = $data['CALORI_AR'];
        $CALORI_ADB            = $data['CALORI_ADB'];
        $ID_COMPANY            = $data['ID_COMPANY'];
       
        $sql = "
            SELECT * FROM T_KUALITAS_MATERIAL 
                WHERE KD_MATERIAL = '".$KD_MATERIAL."' AND 
                NAMA_MATERIAL = '".$NAMA_MATERIAL."' AND 
                KD_VENDOR = ".$KD_VENDOR." AND 
                NAMA_VENDOR = '".$NAMA_VENDOR."' AND 
                KD_PLANT = ".$KD_PLANT." AND 
                NAMA_PLANT = '".$NAMA_PLANT."' AND 
                CREATE_AT = TO_DATE('".$CREATE_AT."', 'DD-MM-YYYY HH24:MI:SS') AND 
                TONASE = ".$TONASE." AND 
                TOTAL_MOISTURE = ".$TOTAL_MOISTURE." AND 
                INHERENT_MOISTURE = ".$INHERENT_MOISTURE." AND 
                ASH_CONTENT = ".$ASH_CONTENT." AND 
                CALORI_AR = ".$CALORI_AR." AND 
                CALORI_ADB = ". $CALORI_ADB." AND
                ID_COMPANY = ".$ID_COMPANY
        ;
        return $this->db->query($sql)->result_array();
    }
    
    private function set_data_to_qmo($data){
        $date_now = date('d-m-Y H:m:s');
        $KD_MATERIAL      = $data['KD_MATERIAL'];
        $NAMA_MATERIAL    = $data['NAMA_MATERIAL'];
        $KD_VENDOR        = $data['KD_VENDOR']; 
        $NAMA_VENDOR           = $data['NAMA_VENDOR'];
        $KD_PLANT              = $data['KD_PLANT'];
        $NAMA_PLANT            = $data['NAMA_PLANT'];
        $CREATE_AT             = $data['CREATE_AT'];
        $TONASE                = $data['TONASE'];
        $TOTAL_MOISTURE        = $data['TOTAL_MOISTURE'];
        $INHERENT_MOISTURE     = $data['INHERENT_MOISTURE'];
        $ASH_CONTENT           = $data['ASH_CONTENT'];
        $CALORI_AR             = $data['CALORI_AR'];
        $CALORI_ADB            = $data['CALORI_ADB'];
        $ID_COMPANY            = $data['ID_COMPANY'];
        
        $sql = "
            INSERT INTO
				T_KUALITAS_MATERIAL(
                    KD_MATERIAL,
                    NAMA_MATERIAL,
                    KD_VENDOR,
                    NAMA_VENDOR,
                    KD_PLANT,
                    NAMA_PLANT,
                    CREATE_AT,
                    TONASE,
                    TOTAL_MOISTURE,
                    INHERENT_MOISTURE,
                    ASH_CONTENT,
                    CALORI_AR,
                    CALORI_ADB,
                    SYNC_AT,
                    ID_COMPANY
                )
                VALUES(
                    '$KD_MATERIAL',
                    '$NAMA_MATERIAL',
                    $KD_VENDOR,
                    '$NAMA_VENDOR',
                    $KD_PLANT,
                    '$NAMA_PLANT',
                    TO_DATE('".$CREATE_AT."', 'DD-MM-YYYY'),
                    $TONASE,
                    $TOTAL_MOISTURE,
                    $INHERENT_MOISTURE,
                    $ASH_CONTENT,
                    $CALORI_AR,
                    $CALORI_ADB,
                    TO_DATE('".$date_now."', 'DD-MM-YYYY HH24:MI:SS'),
                    $ID_COMPANY
                )
        ";
        $hasil = $this->db->query($sql);
		return $hasil;
    }

}

?>
