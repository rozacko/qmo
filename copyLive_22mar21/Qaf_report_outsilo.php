<?php

class Qaf_report_outsilo extends QMUser {
	
	public $list_data = array();
	public $list_product = array();
	public $list_qaf = array();
	public $NM_PRODUCT;
	public $list_component = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");

		$this->load->model("c_range_qaf");
		$this->load->model("qaf_daily");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_product");
		$this->load->model("M_hebat_report");
		$this->load->model("c_qaf_product");
		
	}
	
	public function index(){
		$this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_qaf_report_outsilo");
	}
	
	public function get_company_list(){
		echo json_encode($this->m_company->list_company());
	}
	
	
	public function generate_company_report(){
		$this->qaf_daily->generate_report($this->input->post(),'company'); //echo $this->qaf_daily->get_sql();
	}
	
	public function get_company_report(){ #var_dump($this->input->post()); 
		$this->NM_COMPANY = $this->m_company->get_data_by_id($this->input->post("ID_COMPANY"))->NM_COMPANY;
		$this->list_qaf = $this->qaf_daily->report_company($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_report_company");
	}
	
	public function generate_report($REPORT='cement'){
		//cari product
		$param = $this->input->post();
		//id_product = 121,146,147
		$idproduct = $this->c_qaf_product->configuration($param[ID_PLANT],$param[ID_GROUPAREA]); //$ID_PLANT,$ID_GROUPAREA

		//call function untuk menghitung qaf dan simpan ke QAFDAILY
		foreach($idproduct as $prd){
			$param['ID_PRODUCT'] = $prd->ID_PRODUCT;
			$this->qaf_daily->generate_report($this->input->post(),$REPORT); #echo $this->qaf_daily->get_sql();
			//print_r($param);
		}		
		//$this->qaf_daily->generate_report($this->input->post(),$REPORT); #echo $this->qaf_daily->get_sql();
	}
	public function generate_report_cs(){
		$this->generate_report('cs');
	}
	public function generate_report_st(){
		$this->generate_report('st');
	}
	
	public function get_report(){ #var_dump($this->input->post());
		
		 $this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		 $this->AREA = $this->m_area->get_data_by_id_cement($this->input->post("ID_AREA"));
		// echo $this->qaf_daily->get_sql(); 
		$this->list_component = $this->qaf_daily->component_outsilo($this->input->post()); //echo $this->qaf_daily->get_sql()."<br><br>";
		//var_dump($this->list_component); 
		$this->list_qaf = $this->qaf_daily->report($this->input->post());  //echo $this->qaf_daily->get_sql();	
		$this->template->nostyle("v_qaf_report"); //total	
		//$this->template->nostyle("v_qaf_outsilo_table"); //total
	}
		
	public function report_produksi(){ #var_dump($this->input->post());
	
		$param = $this->input->post();
		$listproduk = $this->m_area->datalistProdukOutsilo($param[ID_COMPANY]); 
		
		foreach($listproduk as $v){
			$produke[] = $v->ID_PRODUCT;
		}		
		//print_r($produke);
		$param[LIST_PRODUK] = $produke;
		
		$this->list_qaf = $this->qaf_daily->qaf_produksi_outsilo_sum($param); //echo $this->qaf_daily->get_sql();
		
		foreach($this->list_qaf as $i => $r){
			//perarea
			//$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
			$qaf_prod = (float)$r->QAF;///(int)$r->JML_COMPONENT;
			$this->list_qaf[$i]->QAF_PRODUCT = is_nan($qaf_prod) ? 0 : round($qaf_prod,2);
		}
		//print_r($this->list_qaf);
		
		$this->template->nostyle("v_qaf_produksi_outsilo");
	}
	
	
	public function export_report(){
		$this->data = json_encode($this->m_incident->report($this->input->post()));
		$this->template->nostyle("xls_qaf_incident_report");
	}	
	
	// public function grafik_qaf(){
	// 	$this->list_company = $this->m_company->list_company_auth();
	// 	$this->template->adminlte("v_grafik_qaf");
	// }
	
	public function get_area_by_group(){

				//$ID_GROUPAREA = $this->input->post('ID_GROUPAREA');
				$id_plant = $this->input->post('id_plant'); 
				// var_dump($id_plant);
				// die;
				//$ID_PRODUCT = $this->input->post('ID_PRODUCT'); 
				$ID_COMPANY = $this->input->post('ID_COMPANY'); 

				$param_in = array();
				if($id_plant){
					$param_in['ID_PLANT'] = $id_plant;
				} 
				$ID_GROUPAREA[]=81;
				//$reseult = $this->qaf_daily->get_area_by_group2($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT); 
				$reseult = $this->qaf_daily->get_area_by_group_outsilo($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT); 
				
				//echo $this->qaf_daily->get_sql();

				echo json_encode($reseult);
	}

		
	public function compressive_strength($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_COMPANY=NULL,$ID_PRODUCT=NULL){
		$this->libExternal('select2');
		$ID_GROUPAREA=81; //outsilo
		switch($sm){		
			case 'get_area':	
				$ID_GROUPAREA=array(81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group2($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			case 'get_area_cs':	
				$ID_GROUPAREA=array(81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group_time($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				// $this->template->adminlte("v_qaf_cs");
				$this->template->adminlte("v_qaf_cs_outsilo");

			break;
		}	
	}

	public function get_report_cs(){ #var_dump($this->input->post());
		
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->AREA = $this->m_area->get_data_by_id_cement($this->input->post("ID_AREA"));
		$COMPONENT = array('19','20','21');
	   $this->list_component = $this->qaf_daily->component_outsilo($this->input->post(),$COMPONENT); //echo $this->qaf_daily->get_sql()."<br><br>";
	   
	   //var_dump($this->list_component); 
	   $this->list_qaf = $this->qaf_daily->report_cs($this->input->post());  //echo $this->qaf_daily->get_sql();	
	   $this->template->nostyle("v_qaf_report"); //total	
	   //$this->template->nostyle("v_qaf_outsilo_table"); //total
   }
	
   public function report_cs_produksi(){ #var_dump($this->input->post());
	
		$param = $this->input->post();
		$listproduk = $this->m_area->datalistProdukOutsilo($param[ID_COMPANY]); 
		
		foreach($listproduk as $v){
			$produke[] = $v->ID_PRODUCT;
		}		
		//print_r($produke);
		$param[LIST_PRODUK] = $produke;
		
		$this->list_qaf = $this->qaf_daily->qaf_cs_outsilo_sum($param); //echo $this->qaf_daily->get_sql();
		
		foreach($this->list_qaf as $i => $r){
			//perarea
			//$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
			$qaf_prod = (float)$r->QAF;///(int)$r->JML_COMPONENT;
			$this->list_qaf[$i]->QAF_PRODUCT = is_nan($qaf_prod) ? 0 : round($qaf_prod,2);
		}
		//print_r($this->list_qaf);
		
		$this->template->nostyle("v_qaf_produksi_outsilo");
	}

	public function setting_time($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_COMPANY=NULL,$ID_PRODUCT=NULL){
		$this->libExternal('select2');
		$ID_GROUPAREA=81; //outsilo
		switch($sm){		
			case 'get_area':	
				$ID_GROUPAREA=array(81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group2($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			case 'get_area_cs':	
				$ID_GROUPAREA=array(81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group_time($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				// $this->template->adminlte("v_qaf_cs");
				$this->template->adminlte("v_qaf_st_outsilo");

			break;
		}	
	}

	public function get_report_st(){ #var_dump($this->input->post());
		
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->AREA = $this->m_area->get_data_by_id_cement($this->input->post("ID_AREA"));
		$COMPONENT = array('22','23');
	   $this->list_component = $this->qaf_daily->component_outsilo($this->input->post(),$COMPONENT); //echo $this->qaf_daily->get_sql()."<br><br>";
	   
	   //var_dump($this->list_component); 
	   $this->list_qaf = $this->qaf_daily->report_st_outsilo($this->input->post()); // echo $this->qaf_daily->get_sql();	
	   $this->template->nostyle("v_qaf_report"); //total	
	   //$this->template->nostyle("v_qaf_outsilo_table"); //total
   }
	
   public function report_st_produksi(){ #var_dump($this->input->post());
	
		$param = $this->input->post();
		$listproduk = $this->m_area->datalistProdukOutsilo($param[ID_COMPANY]); 
		
		foreach($listproduk as $v){
			$produke[] = $v->ID_PRODUCT;
		}		
		//print_r($produke);
		$param[LIST_PRODUK] = $produke;
		
		$this->list_qaf = $this->qaf_daily->qaf_st_outsilo_sum($param); //echo $this->qaf_daily->get_sql();
		
		foreach($this->list_qaf as $i => $r){
			//perarea
			//$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
			$qaf_prod = (float)$r->QAF;///(int)$r->JML_COMPONENT;
			$this->list_qaf[$i]->QAF_PRODUCT = is_nan($qaf_prod) ? 0 : round($qaf_prod,2);
		}
		//print_r($this->list_qaf);
		
		$this->template->nostyle("v_qaf_produksi_outsilo");
	}

	
	// public function json_plant_list($ID_COMPANY){
	// 	$this->load->model("m_plant");
	// 	$data = $this->list_plant = $this->m_plant->datalist($ID_COMPANY);
	// 	echo json_encode($data);
	// }
	

	// public function async_list_product_qaf($ID_PLANT=NULL,$ID_GROUPAREA=NULL){
	// 	$otsilo=  array(102,101,104,103,105);
	// 	if(in_array($ID_PLANT,$otsilo)){
	// 		$ID_GROUPAREA=81;
	// 	}
	// 	$this->load->model("c_range_qaf");
	// 	$data = $this->c_range_qaf->list_configured_product_qaf($ID_PLANT,$ID_GROUPAREA);  #echo $this->c_range_qaf->get_sql();
	// 	echo json_encode($data);
	// }

	// public function async_list_product_qaf1(){
	// 	$this->load->model("c_range_qaf");

	// 			//$ID_GROUPAREA = $this->input->post('ID_GROUPAREA');
	// 			$id_plant = $this->input->post('id_plant'); 

	// 			$param_in = array();
	// 			if($id_plant){
	// 				$param_in['ID_PLANT'] = $id_plant;
	// 			} 
	// 			$ID_GROUPAREA[]=1;
	// 			$ID_GROUPAREA[]=81; //tmbah outsilo
	// 	//$data = $this->c_range_qaf->list_configured_product_qaf1($param_in,$ID_GROUPAREA); #echo $this->c_range_qaf->get_sql();
	// 	$data = $this->c_range_qaf->list_configured_product_qaf2($param_in,$ID_GROUPAREA);
	// 	echo json_encode($data);
	// }
	
	// public function calculate_qaf_cs(){
	// 	// $this->M_hebat_report->calculate_qaf_cs($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
	// 	echo "done";
	// }

	// public function json_line($tipe=''){
	// 	$tahun = $this->input->post('tahun');
	// 	$opco  = $this->input->post('opt_company');
	// 	if(!$_POST['opt_company']){
	// 		echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
	// 		exit;
	// 	}
	// 	if(!$_POST['tahun']){
	// 		echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
	// 		exit;
	// 	}

	// 	switch ($tipe) {
	// 		case 'clinker':
	// 			$query = "qaf_clinker_progress";
	// 			$title = "QAF CLINKER PROGRESS";
	// 			break;

	// 		case 'st':
	// 			$query = "qaf_st";
	// 			$title = "QAF OF SETTING TIME";
	// 			break;

	// 		case 'cs':
	// 			$query = "qaf_cs";
	// 			$title = "QAF OF COMPRESSIVE STRENGTH";
	// 			break;
			
	// 		default:
	// 			$query = "qaf_clinker_progress";
	// 			$title = "QAF CLINKER PROGRESS";
	// 			break;
	// 	}

	// 	if($query == 'qaf_clinker_progress'){
	// 		$id_plant  = $this->input->post('id_plant');

	// 		$param = array();
	// 		$param_in = array();

	// 		$param['TAHUN'] = $tahun;
	// 		$param['ID_COMPANY'] = $opco;

	// 		if($id_plant){ 
	// 			$param_in['ID_PLANT'] = $id_plant;
	// 		}

	// 		$result = $this->M_hebat_report->qaf_clinker_progress2($param, $param_in);
	// 	}else{
	// 		$result = $this->M_hebat_report->{$query}($opco, $tahun);
	// 	}
		
	// 	$temp   = array();
	// 	$lay    = array();
	// 	$data   = array();

	// 	if ($result) {
	// 		foreach ($result as $res) {
	// 			$color 			= monthColor($res->BULAN);
	// 			$temp['x'][]  	= $res->BULAN;
	// 			$temp['y'][]  	= $res->NILAI;
	// 			$temp['type'] 	= 'scatter';
	// 			$temp['line'] 	= array('dash' => 'dashdot');
	// 			$temp['marker'] = array('symbol' => "square", "size" => 8);
	// 			$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
	// 			$temp['hoverinfo'] 	= 'x+y';
	// 		}

	// 		$lay['autosize'] = true;
	// 		$lay['yaxis'] 	 = array(
	// 							"showspikes" => true,
	// 							"showticklabels" => true,
	// 							"title" => 'TOTAL QAF',
	// 							"showline" => true,
	// 							"showgrid" => true,
	// 							"gridwidth" => 4,
	// 							"type" => 'linear',
	// 							"autorange" => true,
	// 						);
	// 		$lay['dragmode'] 	= 'zoom';
	// 		$lay['xaxis'] 	= array(
	// 							"title" => "MONTH",
	// 							"showgrid" => false,
	// 							"showline" => true,
	// 							"type" => 'linear',
	// 							"autorange" => false,
	// 							"range" => array(0,12),
	// 						);
	// 		$lay['images'] 	= array(
	// 							array(
	// 								"yanchor" => "middle",
	// 						        "layer" => "above",
	// 						        "xref" => "paper",
	// 						        "yref" => "y",
	// 						        "sizex" => 0.4,
	// 						        "sizey" => 0.5,
	// 						        "source" => base_url("images/" . $opco . ".png"),
	// 						        "y" => 0,
	// 						        "x" => -0.23
	// 							)
	// 						);
	// 		$lay['title'] 		= $title;
	// 		$lay['showlegend'] 	= false;
	// 		$lay['hovermode'] 	= 'closest';
	// 		$code				= 200;
	// 	}else{
	// 		$temp = NULL;
	// 		$lay  = NULL;
	// 		$code = 404;
	// 	}

		

	// 	$data['data'] 	= array($temp);
	// 	$data['layout'] = $lay;
	// 	$data['result'] = $code;
	// 	to_json($data);
	// }
}	

?>
