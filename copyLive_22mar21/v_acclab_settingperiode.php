
<section class="content-header">
  <h1>
     Periode (Akurasi Labor SIG)
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">
			<table><tr><td>
            <div class="box-header">
              <form class="form-inline">
	          <?php if($this->PERM_WRITE): ?>
	            <div class="input-group input-group-sm" style="width: 150px; ">
	              <a data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
	            </div>
	          <?PHP endif; ?>
	          </form>
          	  <hr/>
              
            </div>
            </td></tr>
            <!-- /.box-header -->
            <tr><td>
            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap"
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th >No.</th>
					<!-- <th >Group Proficiency</th> -->
	                <th >Nama Akurasi Labor</th>
	                <th >Tahun</th>
	                <th>Start Date</th>
	                <th>End Date</th>
					<th >Status</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
                    <!-- <td><?= $dt->GROUP_PP;?> </td> -->
                    <td><?= $dt->NAMA;?> </td>
                    <td><?= $dt->TAHUN;?> </td>
                    <td><?= $dt->START_DATE;?> </td>
                    <td><?= $dt->END_DATE;?> </td>
					<td><?= $dt->STATUS;?> </td>
                    <td> 
                    <?php if($this->PERM_WRITE): ?>
					  <button title="Edit" class="btEdit btn btn-success btn-xs" type="button" data-toggle="modal" data-target="#upsModal<?= $dt->ID; ?>"><i class="fa fa-retweet"></i> Update Status</button>
					  
                      <a href="<?php echo site_url("acclab_setuppelaksanaan/pelaksanaan/");?><?= $dt->ID; ?>"><button title="Pelaksanaan" class=" btn btn-default btn-xs" type="button"><i class="fa fa-link"></i> Set Up Pelaksanaan</button></a>
					  
					 <button title="Edit" class="btEdit btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID; ?>"><i class="fa fa-pencil-square-o"></i> Edit</button>
					  
                      <a href="<?php echo site_url("acclab_settingperiode/do_delete");?>/<?= $dt->ID; ?>" onClick="return doconfirm();"><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button></a>
                    <?php endif; ?>
                    </td>
                  </tr>

<!-- Modal edit periode proficiency -->
<div id="upsModal<?= $dt->ID; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form role="form" method="POST" action="<?php echo site_url("acclab_settingperiode/do_edit_status") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Update Status:</b> [<?= $dt->TAHUN;?>] <?= $dt->NAMA;?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="id_acclab"  value="<?= $dt->ID;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-12 clearfix">
					<label>Update Status Priode Akurasi Labor</label>
					<select class="form-control select2" name="status_acclab" >
						<option value="<?= $dt->STATUS;?>" <?php if($dt->STATUS == null){ echo 'style="display: none;"';} ?> ><?= $dt->STATUS;?></option>
						<option value="" disabled="true" <?php if($dt->STATUS == null){ echo 'selected="true"';} ?>>-- Pilih status --</option>
						<option value="OPEN" <?php if($dt->STATUS == "OPEN"){ echo 'style="display: none;"';} ?> >OPEN</option>
						<option value="ONPROGRESS" <?php if($dt->STATUS == "ONPROGRESS"){ echo 'style="display: none;"';} ?> >ONPROGRESS</option>
						<option value="CLOSED" <?php if($dt->STATUS == "CLOSED"){ echo 'style="display: none;"';} ?> >CLOSED</option>
					</select>    
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>		

		
<!-- Modal edit periode proficiency -->
<div id="editModal<?= $dt->ID; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("acclab_settingperiode/do_edit") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Edit Akurasi Labor :</b> [<?= $dt->TAHUN;?>] <?= $dt->NAMA;?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="id_acclab"  value="<?= $dt->ID;?>" />
				<div class="form-group c-group after-add-more" id="utama">
				  <div class="col-sm-4 ">
					<label>Nama Akurasi Labor</label>
					<input type="text" class="form-control" name="nama_acclab" placeholder="Nama Akurasi Labor" value="<?= $dt->NAMA;?>" >
				  </div> 
				  <div class="col-sm-2 ">
					<label>Tahun </label>
					<input type="number" class="form-control" name="tahun_acclab" placeholder="Tahun" value="<?= $dt->TAHUN;?>" >
				  </div> 
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date" placeholder="Start Date" value="<?= date('Y-m-d', strtotime($dt->START_DATE));?>" >
				  </div> 
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date" placeholder="End Date" value="<?= date('Y-m-d', strtotime($dt->END_DATE));?>" >
				  </div> 
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>
				  
                <?php } ?>
                </tbody>
	          </table>
            </div>
            </td></tr></table>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
	
<!-- Modal add  -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("acclab_settingperiode/do_add") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Tambah Periode Akurasi Labor:</b> </h4>
      </div>
      <div class="modal-body">
				<div class="form-group c-group after-add-more" id="utama">
                  <!-- <div class="col-sm-3 clearfix">
					<label>Group Proficiency </label>
					<select class="form-control select2" NAME="group_acclab" disabled>
					
					  <option value="Internal" >Internal</option>
					
					</select>    
				  </div> -->
				  <div class="col-sm-4 ">
					<label>Nama Akurasi Labor </label>
					<input type="text" class="form-control" name="nama_acclab" placeholder="Nama Akurasi Labor"  >
				  </div> 
				  <div class="col-sm-2 ">
					<label>Tahun </label>
					<input type="number" class="form-control" name="tahun_acclab" placeholder="Tahun"  >
				  </div> 
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date" placeholder="Start Date"  >
				  </div> 
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date" placeholder="End Date"  >
				  </div> 
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>