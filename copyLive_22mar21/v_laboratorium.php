
<section class="content-header">
  <h1>
     Data Laboratorium
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">
			<table><tr><td>
            <div class="box-header">
              <form class="form-inline">
	          <?php if($this->PERM_WRITE): ?>
	            <div class="input-group input-group-sm" style="width: 150px; ">
	              <a data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
	            </div>
	          <?PHP endif; ?>
	          </form>
          	  <hr/>
              
            </div>
            </td></tr>
            <!-- /.box-header -->
            <tr><td>
            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap"
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th >No.</th>
	                <th >Code </th>
	                <th >Laboratorium</th>
					<th >Company</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
                    <td><?= $dt->KODE_LAB;?> </td>
                    <td><?= $dt->NAMA_LAB;?> </td>
                    <td>
					<?php 
					if($dt->ID_COMPANY != 0){
						echo $dt->NM_COMPANY;
					}else {
						echo "OTHER";
					}
					?> 
					</td>
                    <td> 
                    <?php if($this->PERM_WRITE): ?>
                      <button title="Edit" class="btEdit btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID_LAB; ?>" ><i class="fa fa-pencil-square-o"></i> Edit</button>
                      <a href="<?php echo site_url("laboratorium/do_delete/");?><?= $dt->ID_LAB; ?>" onClick="return doconfirm();"><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button></a>
                    <?php endif; ?>
                    </td>
                  </tr>

<!-- Modal edit laboratorium -->
<div id="editModal<?= $dt->ID_LAB; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("laboratorium/do_edit") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Edit Laboratorium:</b> [<?= $dt->KODE_LAB;?>] <?= $dt->NAMA_LAB;?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="id_lab"  value="<?= $dt->ID_LAB;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-4 clearfix">
					<label>Laboratorium Code </label>
					<input type="text" class="form-control" name="kode_lab" placeholder="Laboratorium Code" value="<?= $dt->KODE_LAB;?>" >
				  </div>
				  <div class="col-sm-4 ">
					<label>Laboratorium name </label>
					<input type="text" class="form-control" name="nama_lab" placeholder="Laboratorium name" value="<?= $dt->NAMA_LAB;?>" required>
				  </div> 
				  <div class="col-sm-4 ">
					<label>Company</label>
					<select class="form-control select2" NAME="company" required>
						
					<?php  foreach($this->list_company as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_COMPANY ?>" <?php echo ($dt->ID_COMPANY == $dt_pic->ID_COMPANY)?"SELECTED":""; ?> ><?php echo $dt_pic->NM_COMPANY ?></option>
					<?php endforeach; ?>
						<option value="0" <?php echo ($dt->ID_COMPANY == 0)?"SELECTED":""; ?>> OTHER</option>
					</select>   
				  </div> 
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

                <?php } ?>
                </tbody>
	          </table>
            </div>
            </td></tr></table>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- Modal add Laboratorium -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("laboratorium/do_add") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Add Laboratorium:</b> </h4>
      </div>
      <div class="modal-body">
				<div class="form-group c-group after-add-more" id="utama">
                   <div class="col-sm-4 clearfix">
					<label>Laboratorium Code </label>
					<input type="text" class="form-control" name="kode_lab" placeholder="Laboratorium Code"  >
				  </div>
				  <div class="col-sm-4 ">
					<label>Laboratorium name </label>
					<input type="text" class="form-control" name="nama_lab" placeholder="Laboratorium name" required>
				  </div> 
				  <div class="col-sm-4 ">
					<label>Company </label>
					<select class="form-control select2" NAME="company" required>
					<?php  foreach($this->list_company as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_COMPANY ?>"><?php echo $dt_pic->NM_COMPANY ?></option>
					<?php endforeach; ?>
					<option value="0"> OTHER </option>
					</select>   
				  </div> 
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>