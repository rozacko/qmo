<style>
.nav-tabs-custom > .nav-tabs > li:first-of-type.active > a {
    border-left-color: transparent;
    background-color: #ffffff;
    color: #333357;
}
.nav-tabs-custom > .nav-tabs > li.active > a {
    border-top-color: transparent;
    border-left-color: #f4f4f4;
    border-right-color: #f4f4f4;
    background-color: #fff;
    color: #502d2c;
}
</style>
<section class="content-header">
    <h1>
        Setting Time QAF Report
        <!-- <small>information about qaf compressive Strength.</small> -->
    </h1>

</section>

<section class="content">

      <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#tab_1-1" data-toggle="tab">Table</a></li>
              <li><a href="#tab_2-2" data-toggle="tab">Trend</a></li>
              <!-- <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li> -->
              <li class="pull-left header">
              <!-- <i class="fa fa-th"></i>  -->
              Report Setting Time</li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">
                  <?php $this->template->nostyle("v_qaf_st2");?>
                </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-2">
              <?php $this->template->nostyle("v_hebat_graph_qaf_st2");?>

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section>

<!-- msg confirm -->
<?php if($notice->error): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>

<?php endif; ?>

<?php if($notice->success): ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
<?php endif; ?>
<!-- eof msg confirm -->
	