<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  QAF CEMENT PROGRESS
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
    
      
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" class="col-md-12 row">
            <div class="form-group">
              <div class="col-sm-4">
                <div class="row">
                  <div class="col-sm-8 ">
                    <label class="control-label" >Company</label>
                    <select id="opt_company" name="opt_company" class="form-control select2">
                        <option value="">Choose Company...</option>
                      <?php  foreach($this->list_company as $company):?>
                        <option value="<?php echo $company->ID_COMPANY;?>"><?php echo $company->NM_COMPANY;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <label class="control-label" >Year</label><br>
                    <select class="form-control select2" name="YEAR" style="width:100%" id='tahun'>
                    <?php for($i=2016;$i<=date("Y");$i++): ?>
                      <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                    <?php endfor; ?>
                    </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-4">
                <label class="control-label" >Plant</label>
                <input type="checkbox" id="checkallplant" title="Select All" class="pull-right">

                <select multiple="multiple" class="form-control input-sm select2" style="width: 100%" name="id_plant" id="id_plant">
                    <option value="">Choose Plant...</option>
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-4">
                <label class="control-label" >Product</label>
                <input type="checkbox" id="checkallproduct" title="Select All" class="pull-right">
                <select  multiple="" id="id_product" name="id_product" class="form-control select2">
                 <!--  <option value="">Choose Product...</option>
                  <?php  foreach($this->list_product as $v):?>
                    <option value="<?php echo $v->ID_PRODUCT;?>"><?php echo $v->NM_PRODUCT;?></option>
                  <?php endforeach; ?> -->
                </select>
              </div>
            </div>
            <div class="form-group">
              <div class="form-group col-sm-12">
                <hr/> 
                <button class="btn-primary btn btn-sm btn-icon" name="load" id="btLoad"><i class="fa fa-eye"></i> Load</button>
              </div>
            </div>
            
          </form>
          <style type="text/css">
            #container, #sliders {
                min-width: 310px; 
                max-width: 800px;
                margin: 0 auto;
            }
            #container {
                height: 400px; 
            }
          </style>
          <div class="col-md-12 row">

            <div id="container"></div>
            <div id="sliders"></div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<a  id="a-notice-error"
  class="notice-error"
  style="display:none";
  href="#"
  data-title="Alert"
  data-text=""
></a>
<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  .boxPlot_div { margin:auto;margin-bottom:10px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 ) 
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>') 
                  50% 50% 
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;   
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>

function ssetChart(dataChart, urlImage){
  Highcharts.chart('container', {
    exporting: {
      chartOptions: {
        chart: {
          events: {
            load: function() {
              this.renderer.image('http://pt-hikari.com//po-content/po-upload/kikuchi.jpg', 0, 100, 80, 80).add();
            }
          }
        }
      }
    },
    chart: {
      renderTo: 'container',
      events: {
        load: function() {
          this.renderer.image('http://pt-hikari.com//po-content/po-upload/kikuchi.jpg', 0, 100, 80, 80).add();
        }
      },

      type: 'column',
      options3d: {
          enabled: true,
          alpha: 15,
          beta: 15,
          depth: 50,
          viewDistance: 25
      }
    },
    series: [{
      data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
    }]
  });

}

function setChart(dataChart, urlImage){
  $('#sliders').html('<table><tr><td>Alpha Angle</td><td><input id="alpha" type="range" min="0" max="45" value="15"/> <span id="alpha-value" class="value"></span></td></tr><tr><td>Beta Angle</td><td><input id="beta" type="range" min="-45" max="45" value="15"/> <span id="beta-value" class="value"></span></td></tr><tr><td>Depth</td><td><input id="depth" type="range" min="20" max="100" value="50"/> <span id="depth-value" class="value"></span></td></tr></table>');

  var months = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Des"]; 
  var chart = new Highcharts.Chart({
      chart: {
          renderTo: 'container',
          events: {
            load: function() {
              this.renderer.image(urlImage, 10, 250, 80, 80).add();
            }
          },

          type: 'column',
          options3d: {
              enabled: true,
              alpha: 15,
              beta: 15,
              depth: 50,
              viewDistance: 25
          }
      },
      title: {
          text: 'CEMENT QAF PROGRESS'
      },
      xAxis: {
        categories: months
      },
      subtitle: {
          text: ''
      },
      plotOptions: {
          column: {
              depth: 25,
              dataLabels: {
                enabled: true,
                crop: false,
                overflow: 'none'
            }
          }
      },
      series: [{
          data: dataChart
      }],
      legend: {
      enabled: false,
    },
    credits: {
      enabled: false
    }
  });

  function showValues() {
      $('#alpha-value').html(chart.options.chart.options3d.alpha);
      $('#beta-value').html(chart.options.chart.options3d.beta);
      $('#depth-value').html(chart.options.chart.options3d.depth);
  }

  // Activate the sliders
  $('#sliders input').on('input change', function () {
      chart.options.chart.options3d[this.id] = parseFloat(this.value);
      showValues();
      chart.redraw(false);
  });

  showValues();
}

  $(document).ready(function(){
    $('.select2').select2();

     // Set up the chart
          


  $('#opt_company').on('change', function(){
    $("#checkallplant").removeAttr('checked');
    $("#checkallproduct").removeAttr('checked');
    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
  }); 


  $("#checkallplant").click(function(){
    if($("#checkallplant").is(':checked') ){
        $("#id_plant > option").prop("selected","selected");
        $("#id_plant").trigger("change");
    }else{
        $("#id_plant > option").removeAttr("selected");
         $("#id_plant").trigger("change");
     }
  });

  $("#checkallproduct").click(function(){
    if($("#checkallproduct").is(':checked') ){
        $("#id_product > option").prop("selected","selected");
        $("#id_product").trigger("change");
    }else{
        $("#id_product > option").removeAttr("selected");
         $("#id_product").trigger("change");
     }
  });


  function listPlant(idCompany = ''){

      $.ajax({
        url : '<?=base_url("login/getPlant/")?>'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#id_plant').html('');
        $.map( data, function( val, i ) {
          selected = '';
          if(val.ID_PLANT != ''){
            // selected = 'selected';
            $('#id_plant').append('<option value="'+val.ID_PLANT+'" '+selected+'>'+val.NM_PLANT+'</option>');
          }
        });
      });

      $('.select2').select2();
  }

  $('#id_plant').on('change', function(){
    $("#checkallproduct").removeAttr('checked');

    idPlant = $(this).val();
    listProduct(idPlant);
  }); 

  function listProduct(idPlant = ''){
    $.post( "<?=base_url("hebat_graph/async_list_product_qaf")?>", { id_plant: idPlant})
    .done(function(data){
        data = JSON.parse(data);
        $('#id_product').html('');
        $.map( data, function( val, i ) {
          console.log(val);
          selected = '';
          if(val.ID_PRODUCT != ''){
            // selected = 'selected';
            $('#id_product').append('<option value="'+val.ID_PRODUCT+'" '+selected+'>'+val.KD_PRODUCT+'</option>');
          }
        });
      });

      $('.select2').select2();
  }



    $("#btLoad").click(function(event){
      event.preventDefault();

      /* Animation */
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");

       /* COUNT QAF REPORT YEARLY */
	  $.post('<?php echo site_url("hebat_graph/calculate_qaf_cement"); ?>',{
		  YEAR: $("#tahun").val() , 
		  ID_COMPANY:$("#opt_company").val(),
      ID_PLANT: $("#id_plant").val(),
      ID_PRODUCT: $("#id_product").val()
	   },
	   function(){
		    /* Initialize Vars */
			$.ajax({
			  type: "POST",
			  dataType: "json",
			  data: {
  				opt_company: $("#opt_company").val(),
  				tahun: $("#tahun").val(),
          id_plant: $("#id_plant").val(),
          id_product: $("#id_product").val()
			  },
			  url: '<?php echo site_url("hebat_graph/generate_json3dchart/cement");?>',
			  success: function(res){
				$body.removeClass("loading");
				$body.css("cursor", "default");          
        if (res.status == 'success') {
          setChart(res.series, res.image);
				}else{
				  $("#divTable").css('display','none');
				  $("#a-notice-error").data("text", 'Oops! No data found');
				  $("#a-notice-error").click();
				}


			  },
			  error: function(jqXHR, textStatus, errorThrown) {
				$body.removeClass("loading");
				$body.css("cursor", "default");
				$("#a-notice-error").data("text", 'Oops! Something went wrong.<br>Please check message in console');
				$("#a-notice-error").click();

				console.log("XHR: " + JSON.stringify(jqXHR));
				console.log("Status: " + textStatus);
				console.log("Error: " + errorThrown);
			  }
			});
		}
	  );
    });

    /** Notification **/
    $(".notice-error").confirm({ 
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });
  });
</script>
