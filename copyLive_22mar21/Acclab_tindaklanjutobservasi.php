<?php

class Acclab_tindaklanjutobservasi extends QMUser {

	public $list_data = array();
	public $jsonData;
	public $data_groupmenu;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_acclab_temuanObservasi", "observasi");
	}

	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_data = $this->observasi->get_tindaklanjutlist();
		$this->template->adminlte("v_acclab_tindaklanjutObservasi");
	}

	public function getCurrentTemuan() {
		$data = $this->observasi->get_currentTemuan($this->input->post("temuanID"));
		to_json($data);
	}
	
	public function getKomponenForm() {
		$data = $this->observasi->get_formTindakLanjut();
		to_json($data);
	}

	public function getDetail() {
		$data = $this->observasi->get_detailHasil($this->input->post());
		to_json($data);
	}

	public function submitTindakLanjut() {
		$insert = $this->observasi->saveTindakan($this->input->post());

		$data = array(
			"status" => $insert,
			"msg" => ($insert) ? "Data Submitted" : "Oopps something went wrong, please try again"
		);

		to_json($data);
	}
}

?>
