<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
                    <h3 class="box-title">LIST DATA HASIL  <?= strtoupper($this->proficiency->TITLE_PP.' '.$this->proficiency->NAMA_SAMPLE.' TAHUN '. $this->proficiency->YEAR_PP); ?></h3>
                </div>
				<div class="box-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="<?= base_url('hasil_uji_proficiency') ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a><br/>&nbsp;
                            <!-- <a href="<?= base_url('hasil_uji_proficiency/export_to_excel/').$this->input->get('proficiency')."/".$this->input->get('komoditi') ?>" class="btn btn-sm btn-info"><i class="fa fa-file-excel-o"></i> Download Data</a><br/>&nbsp; -->
                            <div class="table-responsive">
                                <table id="dt_peserta" class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">NO</th>
                                            <th>PENERIMA SAMPLE</th>
                                            <th width="10%">ANALIS</th>
                                            <th width="10%">STATUS</th>
                                            <th width="10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $j=1; foreach($this->penerima as $pp){ ?>
                                        <tr>
                                            <td rowspan="<?= (count($pp['DETAIL'][0]) > 0) ? count($pp['DETAIL'][0]) + 1 : '' ?>"><?= $j++; ?></td>
                                            <td rowspan="<?= (count($pp['DETAIL'][0]) > 0) ? count($pp['DETAIL'][0]) + 1 : '' ?>"><?= $pp['PENERIMA']['NAMA_PENERIMA']; ?></td>
                                            <td colspan="3"><a href="<?= base_url('hasil_uji_proficiency/add').'?proficiency='.$this->proficiency->ID_PROFICIENCY.'&komoditi='.$this->proficiency->ID_KOMODITI.'&penerima='.$pp['PENERIMA']['ID_PENERIMA'];?>" class="btn btn-block btn-xs btn-info"><i class="fa fa-edit"></i> Input Hasil Uji</a></td>
                                            
                                        </tr>
                                            <?php
                                            if(count($pp['DETAIL'][0]) > 0){
                                                foreach($pp['DETAIL'][0] as $dtl){ ?>
                                                    <tr>
                                                        <td><?= $dtl['ANALIS'] ?></td>
                                                        <td><?= $dtl['STATUS'] ?></td>
                                                        <td>
                                                            <?php
                                                                $status = $dtl['STATUS'];
                                                                if($status == "DRAFT"){
                                                                    echo "<a class='btn btn-xs btn-primary' href='".base_url("hasil_uji_proficiency/add")."?proficiency=".$this->proficiency->ID_PROFICIENCY."&komoditi=".$this->proficiency->ID_KOMODITI."&penerima=".$pp['PENERIMA']['ID_PENERIMA']."&analis=".$dtl['ID_HASIL_UJI']."'><i class='fa fa-pencil'></i> Input Hasil Uji</a>";
                                                                } else if($status == "APPROVED"){
                                                                    echo "<a class='btn btn-xs btn-success' href='".base_url("hasil_uji_proficiency/add")."?proficiency=".$this->proficiency->ID_PROFICIENCY."&komoditi=".$this->proficiency->ID_KOMODITI."&penerima=".$pp['PENERIMA']['ID_PENERIMA']."&analis=".$dtl['ID_HASIL_UJI']."&view=Y'><i class='fa fa-pencil'></i> View Hasil Uji</a>";
                                                                } else if($status == "REJECTED"){
                                                                    echo "<a class='btn btn-xs btn-warning' href='".base_url("hasil_uji_proficiency/add")."?proficiency=".$this->proficiency->ID_PROFICIENCY."&komoditi=".$this->proficiency->ID_KOMODITI."&penerima=".$pp['PENERIMA']['ID_PENERIMA']."&analis=".$dtl['ID_HASIL_UJI']."&revisi=Y'><i class='fa fa-edit'></i> Input Revisi Hasil Uji</a>";
                                                                } else {
                                                                    $sess = $this->session->userdata()['ROLE']; 
                                                                    $roles = ""; 
                                                                    foreach($sess as $s){ 
                                                                        $roles .= $s['NM_USERGROUP']." "; 
                                                                    }

                                                                    if(strpos($roles, 'ADMINISTRATOR') !== FALSE ||  strpos($roles, 'Approval Uji Proficiency') !== FALSE ){
                                                                        echo "<a class='btn btn-xs btn-success' href='".base_url("hasil_uji_proficiency/add")."?proficiency=".$this->proficiency->ID_PROFICIENCY."&komoditi=".$this->proficiency->ID_KOMODITI."&penerima=".$pp['PENERIMA']['ID_PENERIMA']."&analis=".$dtl['ID_HASIL_UJI']."&approver=Y'><i class='fa fa-check'></i> Validasi Hasil Uji</a>";
                                                                    }
                                                                }
                                                            ?>
                                                        </td>
                                                    </tr>
                                            <?php
                                                }
                                            } ?>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div><hr/>
                            <div class="table-responsive">
                                
                                <table class="table table-bordered table-striped">
                                    <thead style="background-color:#5b9bd5;color:white;">
                                        <tr>
                                            <th style="text-align: center; vertical-align: middle;">Kode Lab</th>
                                            <th style="text-align: center; vertical-align: middle;">Lab Penguji</th>
                                            <th style="text-align: center; vertical-align: middle;">Nama Analis / Penguji</th>
                                            <?php foreach($this->header as $h){ ?>
                                            <th style="text-align: center; vertical-align: middle;"><?= $h->QUEST_TEXT; ?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; foreach($this->list_hasil_uji as $dt_u){ ?>
                                        <tr>
                                            <td><?= $this->kd_komoditi[0]->KODE_SAMPLE."_".$dt_u['HASIL_UJI']['KODE_LAB'] ?></td>
                                            <td><?= $dt_u['HASIL_UJI']['NAMA_LAB'] ?></td>
                                            <td><?= $dt_u['HASIL_UJI']['ANALIS'] ?></td>
                                            <?php foreach($dt_u['DETAIL'][0] as $dt_l){ ?>
                                            <td style="text-align: right;"><?= ($dt_l['QUEST_TYPE'] != "FILE") ? $dt_l['ANSWER'] : "<a target='_blank' href='".base_url('assets/uploads/proficiency/').$dt_l['ANSWER']."'>".$dt_l['ANSWER']."</a>" ?></td>
                                            <?php } ?>
                                        </tr>
                                        <?php $i++; } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script>
	$(document).ready(function(){
        // $("#dt_peserta").DataTable();
    });
</script>