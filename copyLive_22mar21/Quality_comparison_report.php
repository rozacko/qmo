<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality_comparison_report extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_quality_comparison_report");
		$this->load->model("m_sample_quality");
		$this->load->model("m_thirdmaterial");
		$this->load->model("m_company");
		$this->load->library("excel");
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	public function index(){
		$this->template->adminlte("v_quality_comparison_report");
	}

	public function normalaize($value){
		# code...
		if ((double) $value == 0) {
			# code...
			return '-';
		} else {
			# code...
			return (double) $value;
		}
		
	}

	public function excel_create( $id_company, $tanggal, $id_typeproduct, $areagroup){
		# code...

		$sni_name_product = 'OPC';

		if ((int) $id_typeproduct == 0) {
		  	$sni_name_product = 'PCC';
		}

		$tanggal = str_replace("-", "/", $tanggal);
		$tanggal = str_replace("~", " - ", $tanggal);
		$company_detail = $this->m_quality_comparison_report->get_competitor_detail_scm($id_company);

		$default_border = array(
	    	'style' => PHPExcel_Style_Border::BORDER_THIN,
	    	'color' => array('rgb'=>'0a0a0a')
	    );

	    $style_text = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
	        )
	    );

	    $style_header = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'00bfff'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

	    $style_header_yellow = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'ffff00'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

	    $style_header_green = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'56ff0c'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

	    $style_header_pink = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'ffddd9'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

	    $style_header_water = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'b0ebff'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

		if (strtolower($areagroup) == 'province') {
			# code...
			$datacomparison = $this->load_tabel_report_province($id_company, $tanggal, $id_typeproduct, $areagroup);

			if ($datacomparison['Message'] == 'Record Found') {
				# code...

    			$sheet = $this->excel->getActiveSheet();

				$indexsheet = 0;
				foreach ($datacomparison['Data'] as $key => $value) {
					# code...
					//activate worksheet number 1
					$objWorkSheet = $this->excel->createSheet($indexsheet);
					//name the worksheet
					$objWorkSheet->setTitle($key);

				    $objWorkSheet->getColumnDimension('A')->setAutoSize(true);

					//set cell A1 content with some text
					$objWorkSheet->setCellValue('A1', $key);
					$objWorkSheet->setCellValue('A2', 'Parameter');

					$objWorkSheet->mergeCells('B1:B2')->setCellValue('B1', $company_detail['INISIAL']."\n (Data QM)");
				    $objWorkSheet->getStyle('B1:B2')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('B1:B2')->applyFromArray( $style_text );
					$objWorkSheet->mergeCells('C1:C2')->setCellValue('C1', $company_detail['INISIAL']."\n (Data SemNas)");
				    $objWorkSheet->getStyle('C1:C2')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('C1:C2')->applyFromArray( $style_text );

					$tcompetitor = array();

					foreach ($value['Competitor'] as $tind => $val) {
						# code...
						$tcompetitor[] = $val['Competitor Inisial'];

					}

					$x = 0;
					$charx = 'D'; 

					while($x < (int) count($value['Competitor']) && $charx < 'ZZ') {

						$objWorkSheet->mergeCells($charx.'1:'.$charx.'2')->setCellValue($charx.'1', $tcompetitor[$x]);

						$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray(
							array(
								'font'  => array(
									'bold'  => true,
									'size'  => 8,
									'name'  => 'Verdana'
								)
							)
					    );

				    	$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_header );
				    	$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_text );

						$charx++; 
					    $x++;
					} 

					$objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray( $style_text );

				    $tavgcolumn = $charx;

					$objWorkSheet->mergeCells($charx.'1:'.$charx.'2')->setCellValue($charx.'1', 'AVG Ind');
				    $objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_text );
					$charx++;
					$objWorkSheet->setCellValue($charx.'2', 'MIN');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
					$tindexnum = $charx;
					$charx++;
					$objWorkSheet->mergeCells($tindexnum.'1:'.$charx.'1')->setCellValue($tindexnum.'1', 'SMIG Standard');
					$objWorkSheet->setCellValue($charx.'2', 'MAX');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
				    $tindexnummax = $charx;
				    $charx++;
					$objWorkSheet->setCellValue($charx.'2', 'MIN');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
					$sniindexnum = $charx;
					$charx++;
					$objWorkSheet->mergeCells($sniindexnum.'1:'.$charx.'1')->setCellValue($sniindexnum.'1', 'SNI '.$sni_name_product);
					$objWorkSheet->setCellValue($charx.'2', 'MAX');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );

					$objWorkSheet->getStyle('A1:'.$charx.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objWorkSheet->getStyle('A1:'.$charx.'1')->getAlignment()->setWrapText(true);

					$objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle($tavgcolumn.'2:'.$tindexnummax.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle($sniindexnum.'2:'.$charx.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $startindval = 3;

					foreach ($datacomparison['Checklist'] as $indcomp => $detailcomp) {
						# code...

						$objWorkSheet->setCellValue('A'.$startindval, $detailcomp['Component Name']);

						$objWorkSheet->setCellValue('B'.$startindval,  $this->normalaize($datacomparison['Data QM'][$detailcomp['Id Component']]));

						$averageind = 0;

						if ($value['SMIG'][$id_company] && $value['SMIG'][$id_company]['Data'][$detailcomp['Id Component']]) {
							# code...
							$objWorkSheet->setCellValue('C'.$startindval,  $this->normalaize($value['SMIG'][$id_company]['Data'][$detailcomp['Id Component']]['ReRata']));
							$averageind = $averageind + ((double) $value['SMIG'][$id_company]['Data'][$detailcomp['Id Component']]['ReRata'] * (double) $value['SMIG'][$id_company]['Market Share (%)']);
						} else {
							$objWorkSheet->setCellValue('C'.$startindval,  $this->normalaize(0));
						}

						$charxrows = 'D'; 

						foreach ($value['SMIG'] as $compcode => $compdata) {
							# code...
							if ( (int) $compcode != (int) $id_company) {
								# code...
								if ($compdata['Data'][$detailcomp['Id Component']]) {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize($compdata['Data'][$detailcomp['Id Component']]['ReRata']));
									$averageind = $averageind + ((double) $compdata['Data'][$detailcomp['Id Component']]['ReRata'] * (double) $compdata['Market Share (%)']);
								} else {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize(0));
								}
								$charxrows++;
							}
						}

						foreach ($value['Competitor'] as $compcode => $compdata) {
							# code...
							if ( (int) $compcode != (int) $id_company) {
								# code...
								if ($compdata['Data'][$detailcomp['Id Component']]) {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize($compdata['Data'][$detailcomp['Id Component']]['ReRata']));
									$averageind = $averageind + ((double) $compdata['Data'][$detailcomp['Id Component']]['ReRata'] * (double) $compdata['Market Share (%)']);
								} else {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize(0));
								}
								$charxrows++;
							}
						}

						$objWorkSheet->setCellValue($tavgcolumn.''.$startindval, $this->normalaize($averageind));
						$objWorkSheet->setCellValue($tindexnum.''.$startindval, $this->normalaize($detailcomp['V_Min']));
						$objWorkSheet->setCellValue($tindexnummax.''.$startindval, $this->normalaize($detailcomp['V_Max']));
						$objWorkSheet->setCellValue($sniindexnum.''.$startindval, $this->normalaize($detailcomp['SNI_Min']));
						$objWorkSheet->setCellValue($charx.''.$startindval, $this->normalaize($detailcomp['SNI_Max']));

						$startindval++;
					}

					$objWorkSheet->setCellValue('A'.$startindval, 'Market Share');

					if ($value['SMIG'][$id_company]['Market Share (%)']) {
						# code...
						$objWorkSheet->mergeCells('B'.$startindval.':C'.$startindval)->setCellValue('B'.$startindval, round((double) $value['SMIG'][$id_company]['Market Share (%)'] * 100, 2).' %');
					} else {
						# code...
						$objWorkSheet->mergeCells('B'.$startindval.':C'.$startindval)->setCellValue('B'.$startindval, '0 %');
					}
					

					$charxrowsms = 'D';

					foreach ($value['Competitor'] as $compcode => $compdata) {
						# code...
						if ( (int) $compcode != (int) $id_company) {
							# code...
							if ($compdata['Market Share (%)']) {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval, round((double) $compdata['Market Share (%)'] * 100, 2).' %');
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval,  '0 %');
							}

							$charxrowsms++;
						}
					}

					$objWorkSheet->mergeCells($tavgcolumn.''.$startindval.':'.$charx.''.$startindval)->setCellValue($tavgcolumn.''.$startindval, '100 %');

					$objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray( $style_header_green );
					$objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray( $style_text );

				    $objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );


					$objWorkSheet->getRowDimension(1)->setRowHeight(-1); 
					$objWorkSheet->getRowDimension(2)->setRowHeight(-1); 

					$indexsheet++;
				}

			} else {
				# code...
			}
			
		} else {
			# code...
			$datacomparison = $this->load_tabel_report_national_c($id_company, $tanggal, $id_typeproduct, $areagroup);

			if ($datacomparison['Message'] == 'Record Found') {
				# code...

    			$sheet = $this->excel->getActiveSheet();

				$indexsheet = 0;
					# code...
					//activate worksheet number 1
					$objWorkSheet = $this->excel->createSheet($indexsheet);
					//name the worksheet
					$objWorkSheet->setTitle('National');

				    $objWorkSheet->getColumnDimension('A')->setAutoSize(true);

					//set cell A1 content with some text
					$objWorkSheet->setCellValue('A1', 'National');
					$objWorkSheet->setCellValue('A2', 'Parameter');

					$tcompetitor = array();

					foreach ($datacomparison['Data']['SMI'] as $tind => $val) {
						# code...
						$tcompetitor[] = $val['INISIAL'];

					}

					foreach ($datacomparison['Data']['Competitor'] as $tind => $val) {
						# code...
						$tcompetitor[] = $val['INISIAL'];

					}

					$x = 0;
					$charx = 'B'; 

					while($x < ((int) count($datacomparison['Data']['Competitor']) + (int) count($datacomparison['Data']['SMI'])) && $charx < 'ZZ') {

						$objWorkSheet->mergeCells($charx.'1:'.$charx.'2')->setCellValue($charx.'1', $tcompetitor[$x]);

						$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray(
							array(
								'font'  => array(
									'bold'  => true,
									'size'  => 8,
									'name'  => 'Verdana'
								)
							)
					    );

				    	$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_header );
				    	$objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_text );

						$charx++; 
					    $x++;
					} 

					$objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('A1:'.$charx.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray( $style_header );
				    $objWorkSheet->getStyle('A2:'.$charx.'2')->applyFromArray( $style_text );

				    $tavgcolumn = $charx;

					$objWorkSheet->mergeCells($charx.'1:'.$charx.'2')->setCellValue($charx.'1', 'AVG Ind');
				    $objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'1:'.$charx.'2')->applyFromArray( $style_text );
					$charx++;
					$objWorkSheet->setCellValue($charx.'2', 'MIN');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
					$tindexnum = $charx;
					$charx++;
					$objWorkSheet->mergeCells($tindexnum.'1:'.$charx.'1')->setCellValue($tindexnum.'1', 'SMIG Standard');
					$objWorkSheet->setCellValue($charx.'2', 'MAX');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
				    $tindexnummax = $charx;
				    $charx++;
					$objWorkSheet->setCellValue($charx.'2', 'MIN');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );
					$sniindexnum = $charx;
					$charx++;
					$objWorkSheet->mergeCells($sniindexnum.'1:'.$charx.'1')->setCellValue($sniindexnum.'1', 'SNI '.$sni_name_product);
					$objWorkSheet->setCellValue($charx.'2', 'MAX');
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($charx.'2:'.$charx.'2')->applyFromArray( $style_text );

					$objWorkSheet->getStyle('A1:'.$charx.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$objWorkSheet->getStyle('A1:'.$charx.'1')->getAlignment()->setWrapText(true);

					$objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray( $style_header_yellow );
				    $objWorkSheet->getStyle($tavgcolumn.'1:'.$tindexnummax.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle($tavgcolumn.'2:'.$tindexnummax.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray( $style_header_green );
				    $objWorkSheet->getStyle($sniindexnum.'1:'.$charx.'1')->applyFromArray( $style_text );

				    $objWorkSheet->getStyle($sniindexnum.'2:'.$charx.'2')->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

				    $startindval = 3;

					foreach ($datacomparison['Checklist'] as $indcomp => $detailcomp) {
						# code...

						$objWorkSheet->setCellValue('A'.$startindval, $detailcomp['Component Name']);

						$averageind = 0;

						$charxrows = 'B'; 

						foreach ($datacomparison['Data']['SMI'] as $compcode => $compdata) {
							# code...
								if ($compdata['Component Data'][$detailcomp['Id Component']]) {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize($compdata['Component Data'][$detailcomp['Id Component']]['ReRata']));
									$averageind = $averageind + ((double) $compdata['Component Data'][$detailcomp['Id Component']]['ReRata'] * (double) $compdata['Prosentase MS']);
								} else {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize(0));
								}
								$charxrows++;
						}

						foreach ($datacomparison['Data']['Competitor'] as $compcode => $compdata) {
							# code...
							if ( (int) $compcode != (int) $id_company) {
								# code...
								if ($compdata['Component Data'][$detailcomp['Id Component']]) {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize($compdata['Component Data'][$detailcomp['Id Component']]['ReRata']));
									$averageind = $averageind + ((double) $compdata['Component Data'][$detailcomp['Id Component']]['ReRata'] * (double) $compdata['Prosentase MS']);
								} else {
									# code...
									$objWorkSheet->setCellValue($charxrows.''.$startindval,  $this->normalaize(0));
								}
								$charxrows++;
							}
						}

						$objWorkSheet->setCellValue($tavgcolumn.''.$startindval, $this->normalaize($averageind));
						$objWorkSheet->setCellValue($tindexnum.''.$startindval, $this->normalaize($detailcomp['V_Min']));
						$objWorkSheet->setCellValue($tindexnummax.''.$startindval, $this->normalaize($detailcomp['V_Max']));
						$objWorkSheet->setCellValue($sniindexnum.''.$startindval, $this->normalaize($detailcomp['SNI_Min']));
						$objWorkSheet->setCellValue($charx.''.$startindval, $this->normalaize($detailcomp['SNI_Max']));

						$startindval++;
					}

					$objWorkSheet->setCellValue('A'.$startindval, ' Market Share ');
					$objWorkSheet->setCellValue('A'.($startindval + 1), ' Clinker Factor Predicted ');
					$objWorkSheet->setCellValue('A'.($startindval + 2), ' H2O in Cement Predicted ');

					$charxrowsms = 'B';

					foreach ($datacomparison['Data']['SMI'] as $compcode => $compdata) {
						# code...
							if ($compdata['Prosentase MS']) {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval, round((double) $compdata['Prosentase MS'] * 100, 2).' %');
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval,  '0 %');
							}

							if ($compdata['CLINKER FACTOR PREDICTED']) {
								# code...
								// $objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1), $this->normalaize(round((double) $compdata['CLINKER FACTOR PREDICTED'], 2)));
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1), $this->normalaize(round((double) $compdata['CLINKER FACTOR PREDICTED (%)'], 2)));
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1),  $this->normalaize(0));
							}

							if ($compdata['H2O IN CEMENT PREDICTED']) {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 2), $this->normalaize(round((double) $compdata['H2O IN CEMENT PREDICTED'], 2)));
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 2),  $this->normalaize(0));
							}

							$charxrowsms++;
					}

					foreach ($datacomparison['Data']['Competitor'] as $compcode => $compdata) {
						# code...
						if ( (int) $compcode != (int) $id_company) {
							# code...
							if ($compdata['Prosentase MS']) {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval, round((double) $compdata['Prosentase MS'] * 100, 2).' %');
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.$startindval,  '0 %');
							}

							if ($compdata['CLINKER FACTOR PREDICTED']) {
								# code...
								// $objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1), $this->normalaize(round((double) $compdata['CLINKER FACTOR PREDICTED'], 2)));
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1), $this->normalaize(round((double) $compdata['CLINKER FACTOR PREDICTED (%)'], 2)));
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 1),  $this->normalaize(0));
							}

							if ($compdata['H2O IN CEMENT PREDICTED']) {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 2), $this->normalaize(round((double) $compdata['H2O IN CEMENT PREDICTED'], 2)));
							} else {
								# code...
								$objWorkSheet->setCellValue($charxrowsms.''.($startindval + 2),  $this->normalaize(0));
							}

							$charxrowsms++;
						}
					}

					$objWorkSheet->mergeCells($tavgcolumn.''.$startindval.':'.$charx.''.$startindval)->setCellValue($tavgcolumn.''.$startindval, '100 %');
					$objWorkSheet->mergeCells($tavgcolumn.''.($startindval + 1).':'.$charx.''.($startindval + 1))->setCellValue($tavgcolumn.''.($startindval + 1), '');
					$objWorkSheet->mergeCells($tavgcolumn.''.($startindval + 2).':'.$charx.''.($startindval + 2))->setCellValue($tavgcolumn.''.($startindval + 2), '');

					$objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray( $style_header_green );
					$objWorkSheet->getStyle('A'.($startindval + 1).':'.$charx.''.($startindval + 1))->applyFromArray( $style_header_pink );
					$objWorkSheet->getStyle('A'.($startindval + 2).':'.$charx.''.($startindval + 2))->applyFromArray( $style_header_water );

					$objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray( $style_text );
					$objWorkSheet->getStyle('A'.($startindval + 1).':'.$charx.''.($startindval + 1))->applyFromArray( $style_text );
					$objWorkSheet->getStyle('A'.($startindval + 2).':'.$charx.''.($startindval + 2))->applyFromArray( $style_text );

				    $objWorkSheet->getStyle('A'.$startindval.':'.$charx.''.$startindval)->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );
				    $objWorkSheet->getStyle('A'.($startindval + 1).':'.$charx.''.($startindval + 1))->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'color' => array('rgb' => 'bd0000'),
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );
				    $objWorkSheet->getStyle('A'.($startindval + 2).':'.$charx.''.($startindval + 2))->applyFromArray(
						array(
							'font'  => array(
								'bold'  => true,
								'size'  => 8,
								'name'  => 'Verdana'
							)
						)
				    );

					$objWorkSheet->getRowDimension(1)->setRowHeight(-1); 
					$objWorkSheet->getRowDimension(2)->setRowHeight(-1); 

					$indexsheet++;

			} else {
				# code...
			}
		}

		$company_name = '';

		if (strtolower($areagroup) == 'province') {
			# code...
			$company_name = $company_detail['NAMA_PERUSAHAAN'].' ';
		}

		$filename = $areagroup.' QM Report '.$company_name.' Periode : '.$tanggal.'.xls'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			            
			//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
			//if you want to save it as .XLSX Excel 2007 format
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
			//force user to download the Excel file without writing it to server's HD


			$objWriter->save("php://output");
		 
		

	}

	public function load_tabel_report_national_c($id_company, $dtanggal, $id_typeproduct, $areagroup){

		$post = $this->input->post();
		$id_company = $id_company;
		$rangetanggal = $dtanggal;
		$splitrange = explode(' - ', $rangetanggal);
		$tgl['start'] = date_format(date_create($splitrange[0]), "Y-m-d");
		$tgl['end'] = date_format(date_create($splitrange[1]), "Y-m-d");
		$months['start'] = date_format(date_create($splitrange[0]), "Y-m");
		$months['end'] = date_format(date_create($splitrange[1]), "Y-m");
		$tanggal = '05-2018';
		$tdata = array();
		$bulan = array();
		$third_material = array();
		$detailtanggal = explode('-', $tanggal);
		$bulan[] = $detailtanggal[0];
		$tahun = $detailtanggal[1];
		$id_typeproduct = $id_typeproduct;
		$areagroup = $areagroup;

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$cluster = $this->m_quality_comparison_report->get_cluster_national($months, (int) $id_typeproduct);
		$total_ms = $this->m_quality_comparison_report->get_total_ms_scm($months, (int) $id_typeproduct);

		$param = $this->m_sample_quality->component_checklist_order();

		$component_relation	= $this->m_thirdmaterial->get_component_relation();

		foreach ($component_relation as $key => $value) {
			# code...
			$third_material[$value['ID_COMPONENT']]['H2O %'] = $value['H2O_PERCENTAGE'];
			$third_material[$value['ID_COMPONENT']]['Material Code'] = $value['ID_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Material Name'] = $value['KODE_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Material ID'] = $value['NAME_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Component Code'] = $value['KD_COMPONENT'];
			$third_material[$value['ID_COMPONENT']]['Component Name'] = $value['NM_COMPONENT'];
			$third_material[$value['ID_COMPONENT']]['Component ID'] = $value['ID_COMPONENT'];			
			$third_material[$value['ID_COMPONENT']]['Component %'] = $value['COMP_PERCENTAGE'];
		}

		if ($cluster) {
			# code...
			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			$id_comp = array();
			
			$param = $this->m_sample_quality->component_checklist_order();

			foreach ($param as $key => $value) {

				# code...
				if ((int)$value['STATUS_CHECKLIST'] == 1) {
					# code...
					$tcomp['Id Component'] = (int) $value['ID_COMPONENT'];
					$tcomp['Component Code'] = $value['KD_COMPONENT'];
					$tcomp['Component Name'] = $value['NM_COMPONENT'];
					$smigstd = $this->m_sample_quality->component_min_max($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
					if ($smigstd) {
						# code...
						$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
						$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
					} else {
						# code...
						$tcomp['V_Min'] = 0;
						$tcomp['V_Max'] = 0;
					}

					$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
					if ($snistd) {
						# code...
						$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
						$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
					} else {
						# code...
						$tcomp['SNI_Min'] = 0;
						$tcomp['SNI_Max'] = 0;
					}

					$tcomp['Value_Precission'] = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
					
					$id_comp[] = $tcomp;
				}
			}

			$return['Checklist'] = $id_comp;

			foreach ($cluster as $key => $value) {
				# code...
				$tmp_comp = array();
				if ($value['KELOMPOK'] == 'SMI') {
					# code...				
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['KODE_PERUSAHAAN'] = $value['KODE_PERUSAHAAN'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['NAMA_PERUSAHAAN'] = $value['NAMA_PERUSAHAAN'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['INISIAL'] = $value['INISIAL'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['PRODUK'] = $value['PRODUK'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['REALISASI_MS'] = (double) $value['REALISASI_MS'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['TOTAL_MS'] = (double) $total_ms['REALISASI_MS'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['Prosentase MS'] = (double)((double) $value['REALISASI_MS'] / (double) $total_ms['REALISASI_MS']);

					$cfprediction = 0;
					$hhoincement = 0;

					foreach ($param as $ind => $val) {
						# code...
						if ((int)$val['STATUS_CHECKLIST'] == 1) {
							# code...
							$tcomp['Id Component'] = (int) $val['ID_COMPONENT'];
							$tcomp['Component Code'] = $val['KD_COMPONENT'];
							$tcomp['Component Name'] = $val['NM_COMPONENT'];
							$smigstd = $this->m_sample_quality->component_min_max($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($smigstd) {
								# code...
								$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
								$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
							} else {
								# code...
								$tcomp['V_Min'] = 0;
								$tcomp['V_Max'] = 0;
							}

							$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($snistd) {
								# code...
								$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
								$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
							} else {
								# code...
								$tcomp['SNI_Min'] = 0;
								$tcomp['SNI_Max'] = 0;
							}

							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $tcomp['Id Component'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							$tcomp['Value_Precission'] = $mathround;
							

							$component_value = $this->m_quality_comparison_report->get_component_value_national($value['KODE_PERUSAHAAN'], $tgl, $id_typeproduct, $tcomp['Id Component']);

							if ($component_value) {
								# code...
								if ($tcomp['Value_Precission'] < 0) {
									# code...
									$tcomp['ReRata'] = (double) $component_value['RERATA'];
								} else if ($tcomp['Value_Precission'] == 0) {
									# code...
									$tcomp['ReRata'] = (int) $component_value['RERATA'];
								} else {
									# code...
									// $tcomp['ReRata'] = round((double) $component_value['RERATA'], $tcomp['Value_Precission'] );
									$tcomp['ReRata'] = number_format($component_value['RERATA'], $tcomp['Value_Precission'], '.', '');
								}
							} else {
								# code...
								$tcomp['ReRata'] = (double) 0;
							}

							if ($third_material[$val['ID_COMPONENT']]) {
								# code...
								$hhoincement += (double)((double) $tcomp['ReRata'] * ((double)($third_material[$val['ID_COMPONENT']]['H2O %']) / 100));								
								$cfprediction += (double)((double) $tcomp['ReRata'] / ((double)($third_material[$val['ID_COMPONENT']]['Component %']) / 100));
							}
							
							
							$tmp_comp[$tcomp['Id Component']] = $tcomp;
						}
					}

					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['Component Data'] = $tmp_comp;

					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['H2O IN CEMENT PREDICTED'] = (double) $hhoincement;	

					if ((double) $cfprediction == 0 || (double) $hhoincement == 0 ) {
						# code...						
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double) 0;
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double) 0;
					} else {
						# code...
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement)/100);
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement));
					}
					

					
				} else {
					# code...
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['KODE_PERUSAHAAN'] = $value['KODE_PERUSAHAAN'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['NAMA_PERUSAHAAN'] = $value['NAMA_PERUSAHAAN'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['INISIAL'] = $value['INISIAL'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['PRODUK'] = $value['PRODUK'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['REALISASI_MS'] = (double) $value['REALISASI_MS'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['TOTAL_MS'] = (double) $total_ms['REALISASI_MS'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['Prosentase MS'] = (double)((double) $value['REALISASI_MS'] / (double) $total_ms['REALISASI_MS']);

					$cfprediction = 0;
					$hhoincement = 0;

					foreach ($param as $ind => $val) {
						# code...
						if ((int)$val['STATUS_CHECKLIST'] == 1) {
							# code...
							$tcomp['Id Component'] = (int) $val['ID_COMPONENT'];
							$tcomp['Component Code'] = $val['KD_COMPONENT'];
							$tcomp['Component Name'] = $val['NM_COMPONENT'];
							$smigstd = $this->m_sample_quality->component_min_max($months, (int) $val['ID_COMPONENT'], $id_typeproduct);
							if ($smigstd) {
								# code...
								$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
								$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
							} else {
								# code...
								$tcomp['V_Min'] = 0;
								$tcomp['V_Max'] = 0;
							}

							$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
							if ($snistd) {
								# code...
								$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
								$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
							} else {
								# code...
								$tcomp['SNI_Min'] = 0;
								$tcomp['SNI_Max'] = 0;
							}

							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $tcomp['Id Component'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							$tcomp['Value_Precission'] = $mathround;

							$component_value = $this->m_quality_comparison_report->get_component_value_national($value['KODE_PERUSAHAAN'], $tgl, $id_typeproduct, $tcomp['Id Component']);

							if ($component_value) {
								# code...
								if ($tcomp['Value_Precission'] < 0) {
									# code...
									$tcomp['ReRata'] = (double) $component_value['RERATA'];
								} else if ($tcomp['Value_Precission'] == 0) {
									# code...
									$tcomp['ReRata'] = (int) $component_value['RERATA'];
								} else {
									# code...
									// $tcomp['ReRata'] = round((double) $component_value['RERATA'], $tcomp['Value_Precission'] );									
									$tcomp['ReRata'] = number_format($component_value['RERATA'], $tcomp['Value_Precission'], '.', '');
								}
							} else {
								# code...
								$tcomp['ReRata'] = (double) 0;
							}

							if ($third_material[$val['ID_COMPONENT']]) {
								# code...
								$hhoincement += (double)((double) $tcomp['ReRata'] * ((double)($third_material[$val['ID_COMPONENT']]['H2O %']) / 100));								
								$cfprediction += (double)((double) $tcomp['ReRata'] / ((double)($third_material[$val['ID_COMPONENT']]['Component %']) / 100));
							}
							
							
							$tmp_comp[$tcomp['Id Component']] = $tcomp;
						}
					}

					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['Component Data'] = $tmp_comp;			

					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['H2O IN CEMENT PREDICTED'] = (double) $hhoincement;		
					
					if ((double) $cfprediction == 0 || (double) $hhoincement == 0 ) {
						# code...						
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double) 0;
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double) 0;
					} else {
						# code...
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement)/100);
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement));
					}

				}
			}

			$return['Data'] = $tdata;

		}

		return $return;

	}

	public function load_tabel_report_province($id_company, $dtanggal, $id_typeproduct, $areagroup){

		$post = $this->input->post();
		$id_company = $id_company;
		$rangetanggal = $dtanggal;
		$splitrange = explode(' - ', $rangetanggal);
		$tgl['start'] = date_format(date_create($splitrange[0]), "Y-m-d");
		$tgl['end'] = date_format(date_create($splitrange[1]), "Y-m-d");
		$months['start'] = date_format(date_create($splitrange[0]), "Y-m");
		$months['end'] = date_format(date_create($splitrange[1]), "Y-m");
		$tanggal = '05-2018';
		$bulan = array();
		$detailtanggal = explode('-', $tanggal);
		$bulan[] = $detailtanggal[0];
		$tahun = $detailtanggal[1];
		$id_typeproduct = $id_typeproduct;
		$areagroup = $areagroup;
		
		$return = array();
		$tdata = array();
		$id_comp = array();
		$tdataqm = array();

		$tgroup_area = array();

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$cluster = $this->m_quality_comparison_report->get_cluster($id_company, $tgl, $id_typeproduct, $areagroup);
		foreach ($cluster as $key => $value) {
			$clusterarea = $this->m_quality_comparison_report->get_area_sample_list_scm($value);

			if ($clusterarea) {
				$cluster[$key]['ID_M_KOTA'] = $clusterarea['ID_M_KOTA'];
				$cluster[$key]['KD_PROP'] = $clusterarea['KD_PROP'];
				$cluster[$key]['KD_AREA'] = $clusterarea['KD_AREA'];
				$cluster[$key]['KD_KOTA'] = $clusterarea['KD_KOTA'];
				$cluster[$key]['NM_KOTA'] = $clusterarea['NM_KOTA'];
			}

			$clusterprov = $this->m_quality_comparison_report->get_prov_sample_list_scm($clusterarea['KD_PROP']);

			if ($clusterprov) {
				$cluster[$key]['NM_PROV'] = $clusterprov['NM_PROV'];
				$cluster[$key]['KD_PROV'] = $clusterprov['KD_PROV'];
				$cluster[$key]['KD_PROP_ABBR'] = $clusterprov['KD_PROP_ABBR'];
				$cluster[$key]['KD_PETA'] = $clusterprov['KD_PETA'];
			}

			$tgroup_area[$clusterprov['NM_PROV']]['KD_PROV'] = $cluster[$key]['KD_PROV'];
			$tgroup_area[$clusterprov['NM_PROV']]['NM_PROV'] = $cluster[$key]['NM_PROV'];
			$tgroup_area[$clusterprov['NM_PROV']]['LIST_AREA'][] = (int) $cluster[$key]['ID_M_KOTA'];
		}		

		$dataqm = $this->m_sample_quality->dataQM($tgl, $id_typeproduct);

		$return['Data QM'] = $dataqm;

		foreach ($dataqm as $ind => $val) {
			$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $val['ID_COMPONENT'], $id_typeproduct);
			$mathround = -1;

			if ($precissio_conf) {
				# code...
				$mathround = (int) $precissio_conf['PRECISSION'];
			}

			if ($mathround < 0) {
				# code...
				$tdataqm[$val['ID_COMPONENT']] = (double) $val['RERATA'];
			} else if ($mathround == 0) {
				# code...
				$tdataqm[$val['ID_COMPONENT']] = (int) $val['RERATA'];
			} else {
				# code...
				// $tdataqm[$val['ID_COMPONENT']] = round((double) $val['RERATA'], $mathround );
				$tdataqm[$val['ID_COMPONENT']] = number_format($val['RERATA'], $mathround, '.', '');

			}
		}

		if ($tdataqm) {
			# code...
			$return['Data QM'] = $tdataqm;
		}

		$param = $this->m_sample_quality->component_checklist_order();

		foreach ($param as $key => $value) {

			# code...
			if ((int)$value['STATUS_CHECKLIST'] == 1) {
				# code...
				$tcomp['Id Component'] = (int) $value['ID_COMPONENT'];
				$tcomp['Component Code'] = $value['KD_COMPONENT'];
				$tcomp['Component Name'] = $value['NM_COMPONENT'];
				$smigstd = $this->m_sample_quality->component_min_max($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
				if ($smigstd) {
					# code...
					$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
					$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
				} else {
					# code...
					$tcomp['V_Min'] = 0;
					$tcomp['V_Max'] = 0;
				}

				$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
				if ($snistd) {
					# code...
					$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
					$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
				} else {
					# code...
					$tcomp['SNI_Min'] = 0;
					$tcomp['SNI_Max'] = 0;
				}
				
				$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

				if ($precissio_conf) {
					# code...
					$tcomp['Value_Precission'] = (int) $precissio_conf['PRECISSION'];
				} else {
					# code...
					$tcomp['Value_Precission'] = null;
				}
				
				$id_comp[] = $tcomp;
			}
		}

		$return['Checklist'] = $id_comp;

		if ($cluster) {
			# code...
			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			foreach ($tgroup_area as $ind => $val) {

				# code...

				$clusterdetail = $this->m_quality_comparison_report->get_cluster_detail($id_company, $tgl, $id_typeproduct, $areagroup, $val['LIST_AREA']);

				if ($clusterdetail) {
					# code...

					foreach ($clusterdetail as $key => $value) {
						# code...

						$clustercompetitor = $this->m_quality_comparison_report->get_competitor_detail_scm($value['ID_COMPETITOR']);

						if ($clusterarea) {
							$clusterdetail[$key]['KODE_PERUSAHAAN'] = $clustercompetitor['KODE_PERUSAHAAN'];
							$clusterdetail[$key]['NAMA_PERUSAHAAN'] = $clustercompetitor['NAMA_PERUSAHAAN'];
							$clusterdetail[$key]['INISIAL'] = $clustercompetitor['INISIAL'];
							$clusterdetail[$key]['PRODUK'] = $clustercompetitor['PRODUK'];
							if ($clustercompetitor['KELOMPOK'] == 'SMI') {
								# code...
								$clusterdetail[$key]['IS_SMIG'] = (int) 1;
							} else {
								# code...
								$clusterdetail[$key]['IS_SMIG'] = (int) 0;
							}
							
						}
					}

					foreach ($clusterdetail as $key => $value) {

						if ((int) $value['IS_SMIG'] == 1 && (int) $value['ID_COMPANY'] == (int) $id_company && (int) $value['ID_COMPETITOR'] == (int) $id_company) {
							# code...
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAMA_PERUSAHAAN'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Competitor Inisial'] = $value['INISIAL'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Merk Product'] = $value['PRODUK'];

							$ms_total = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, null, (int) $id_typeproduct);
							$ms_qty = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, $value['ID_COMPETITOR'], (int) $id_typeproduct);

							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share Total'] = (double) $ms_total['MS_QTY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share Qty'] = (double) $ms_qty['MS_QTY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share (%)'] =  (double)((double) $ms_qty['MS_QTY'] / (double) $ms_total['MS_QTY']);
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];
							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							if ($mathround < 0) {
								# code...
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
							} else if ($mathround == 0) {
								# code...
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (int) $value['RERATA'];
							} else {
								# code...
								// $tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = round((double) $value['RERATA'], $mathround );
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = number_format($value['RERATA'], $mathround, '.', '');
							}

						} else {
							# code...
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAMA_PERUSAHAAN'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Competitor Inisial'] = $value['INISIAL'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Merk Product'] = $value['PRODUK'];

							$ms_total = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, null, (int) $id_typeproduct);
							$ms_qty = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, $value['ID_COMPETITOR'], (int) $id_typeproduct);
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share Total'] = (double) $ms_total['MS_QTY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share Qty'] = (double) $ms_qty['MS_QTY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share (%)'] =  (double)((double) $ms_qty['MS_QTY'] / (double) $ms_total['MS_QTY']);
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];
							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							if ($mathround < 0) {
								# code...
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
							} else if ($mathround == 0) {
								# code...
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (int) $value['RERATA'];
							} else {
								# code...
								// $tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = round((double) $value['RERATA'], $mathround );
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = number_format($value['RERATA'], $mathround, '.', '');
							}

						}

					}

					$return['Data'] = $tdata;
				}

			}
		}

		return $return;
	}

	public function load_tabel_report_national(){

		$post = $this->input->post();
		$id_company = $post['id_company'];
		$rangetanggal = $post['tanggal'];
		$splitrange = explode(' - ', $rangetanggal);
		$tgl['start'] = date_format(date_create($splitrange[0]), "Y-m-d");
		$tgl['end'] = date_format(date_create($splitrange[1]), "Y-m-d");
		$months['start'] = date_format(date_create($splitrange[0]), "Y-m");
		$months['end'] = date_format(date_create($splitrange[1]), "Y-m");
		$tanggal = '05-2018';
		$tdata = array();
		$bulan = array();
		$third_material = array();
		$detailtanggal = explode('-', $tanggal);
		$bulan[] = $detailtanggal[0];
		$tahun = $detailtanggal[1];
		$id_typeproduct = $post['id_typeproduct'];
		$areagroup = $post['areagroup'];

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$cluster = $this->m_quality_comparison_report->get_cluster_national($months, (int) $id_typeproduct);
		$total_ms = $this->m_quality_comparison_report->get_total_ms_scm($months, (int) $id_typeproduct);

		$param = $this->m_sample_quality->component_checklist_order();

		$component_relation	= $this->m_thirdmaterial->get_component_relation();

		foreach ($component_relation as $key => $value) {
			# code...
			$third_material[$value['ID_COMPONENT']]['H2O %'] = $value['H2O_PERCENTAGE'];
			$third_material[$value['ID_COMPONENT']]['Material Code'] = $value['ID_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Material Name'] = $value['KODE_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Material ID'] = $value['NAME_MATERIAL'];
			$third_material[$value['ID_COMPONENT']]['Component Code'] = $value['KD_COMPONENT'];
			$third_material[$value['ID_COMPONENT']]['Component Name'] = $value['NM_COMPONENT'];
			$third_material[$value['ID_COMPONENT']]['Component ID'] = $value['ID_COMPONENT'];			
			$third_material[$value['ID_COMPONENT']]['Component %'] = $value['COMP_PERCENTAGE'];
		}

		if ($cluster) {
			# code...
			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			$id_comp = array();

			$param = $this->m_sample_quality->component_checklist_order();

			foreach ($param as $key => $value) {

				# code...
				if ((int)$value['STATUS_CHECKLIST'] == 1) {
					# code...
					$tcomp['Id Component'] = (int) $value['ID_COMPONENT'];
					$tcomp['Component Code'] = $value['KD_COMPONENT'];
					$tcomp['Component Name'] = $value['NM_COMPONENT'];
					$smigstd = $this->m_sample_quality->component_min_max($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
					if ($smigstd) {
						# code...
						$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
						$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
					} else {
						# code...
						$tcomp['V_Min'] = 0;
						$tcomp['V_Max'] = 0;
					}

					$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
					if ($snistd) {
						# code...
						$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
						$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
					} else {
						# code...
						$tcomp['SNI_Min'] = 0;
						$tcomp['SNI_Max'] = 0;
					}
			
					$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

					if ($precissio_conf) {
						# code...
						$tcomp['Value_Precission'] = (int) $precissio_conf['PRECISSION'];
					} else {
						# code...
						$tcomp['Value_Precission'] = null;
					}
					
					$id_comp[] = $tcomp;
				}
			}

			$return['Checklist'] = $id_comp;

			// $return['Data'] = $cluster;

			foreach ($cluster as $key => $value) {
				# code...
				$tmp_comp = array();
				if ($value['KELOMPOK'] == 'SMI') {
					# code...				
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['KODE_PERUSAHAAN'] = $value['KODE_PERUSAHAAN'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['NAMA_PERUSAHAAN'] = $value['NAMA_PERUSAHAAN'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['INISIAL'] = $value['INISIAL'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['PRODUK'] = $value['PRODUK'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['REALISASI_MS'] = (double) $value['REALISASI_MS'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['TOTAL_MS'] = (double) $total_ms['REALISASI_MS'];
					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['Prosentase MS'] = (double)((double) $value['REALISASI_MS'] / (double) $total_ms['REALISASI_MS']);

					$cfprediction = 0;
					$hhoincement = 0;

					foreach ($param as $ind => $val) {
						# code...
						if ((int)$val['STATUS_CHECKLIST'] == 1) {
							# code...
							$tcomp['Id Component'] = (int) $val['ID_COMPONENT'];
							$tcomp['Component Code'] = $val['KD_COMPONENT'];
							$tcomp['Component Name'] = $val['NM_COMPONENT'];
							$smigstd = $this->m_sample_quality->component_min_max($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($smigstd) {
								# code...
								$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
								$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
							} else {
								# code...
								$tcomp['V_Min'] = 0;
								$tcomp['V_Max'] = 0;
							}

							$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($snistd) {
								# code...
								$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
								$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
							} else {
								# code...
								$tcomp['SNI_Min'] = 0;
								$tcomp['SNI_Max'] = 0;
							}
							
							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $tcomp['Id Component'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							$tcomp['Value_Precission'] = $mathround;

							$component_value = $this->m_quality_comparison_report->get_component_value_national($value['KODE_PERUSAHAAN'], $tgl, $id_typeproduct, $tcomp['Id Component']);

							if ($component_value) {
								# code...
								if ($tcomp['Value_Precission'] < 0) {
									# code...
									$tcomp['ReRata'] = (double) $component_value['RERATA'];
								} else if ($tcomp['Value_Precission'] == 0) {
									# code...
									$tcomp['ReRata'] = (int) $component_value['RERATA'];
								} else {
									# code...
									// $tcomp['ReRata'] = round((double) $component_value['RERATA'], $tcomp['Value_Precission'] );
									$tcomp['ReRata'] = number_format($component_value['RERATA'], $tcomp['Value_Precission'], '.', '');
								}
							} else {
								# code...
								$tcomp['ReRata'] = (double) 0;
							}

							if ($third_material[$val['ID_COMPONENT']]) {
								# code...
								$hhoincement += (double)((double) $tcomp['ReRata'] * ((double)($third_material[$val['ID_COMPONENT']]['H2O %']) / 100));								
								$cfprediction += (double)((double) $tcomp['ReRata'] / ((double)($third_material[$val['ID_COMPONENT']]['Component %']) / 100));
							}
							
							
							$tmp_comp[$tcomp['Id Component']] = $tcomp;
						}
					}

					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['Component Data'] = $tmp_comp;

					$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['H2O IN CEMENT PREDICTED'] = (double) $hhoincement;	

					if ((double) $cfprediction == 0 || (double) $hhoincement == 0 ) {
						# code...						
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double) 0;
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double) 0;
					} else {
						# code...
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement)/100);
						$tdata[$value['KELOMPOK']][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement));
					}
					

					
				} else {
					# code...
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['KODE_PERUSAHAAN'] = $value['KODE_PERUSAHAAN'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['NAMA_PERUSAHAAN'] = $value['NAMA_PERUSAHAAN'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['INISIAL'] = $value['INISIAL'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['PRODUK'] = $value['PRODUK'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['REALISASI_MS'] = (double) $value['REALISASI_MS'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['TOTAL_MS'] = (double) $total_ms['REALISASI_MS'];
					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['Prosentase MS'] = (double)((double) $value['REALISASI_MS'] / (double) $total_ms['REALISASI_MS']);

					$cfprediction = 0;
					$hhoincement = 0;

					foreach ($param as $ind => $val) {
						# code...
						if ((int)$val['STATUS_CHECKLIST'] == 1) {
							# code...
							$tcomp['Id Component'] = (int) $val['ID_COMPONENT'];
							$tcomp['Component Code'] = $val['KD_COMPONENT'];
							$tcomp['Component Name'] = $val['NM_COMPONENT'];
							$smigstd = $this->m_sample_quality->component_min_max($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($smigstd) {
								# code...
								$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
								$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
							} else {
								# code...
								$tcomp['V_Min'] = 0;
								$tcomp['V_Max'] = 0;
							}

							$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $tcomp['Id Component'], $id_typeproduct);
							if ($snistd) {
								# code...
								$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
								$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
							} else {
								# code...
								$tcomp['SNI_Min'] = 0;
								$tcomp['SNI_Max'] = 0;
							}
							
							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $tcomp['Id Component'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							$tcomp['Value_Precission'] = $mathround;

							$component_value = $this->m_quality_comparison_report->get_component_value_national($value['KODE_PERUSAHAAN'], $tgl, $id_typeproduct, $tcomp['Id Component']);

							if ($component_value) {
								# code...
								if ($tcomp['Value_Precission'] < 0) {
									# code...
									$tcomp['ReRata'] = (double) $component_value['RERATA'];
								} else if ($tcomp['Value_Precission'] == 0) {
									# code...
									$tcomp['ReRata'] = (int) $component_value['RERATA'];
								} else {
									# code...
									// $tcomp['ReRata'] = round((double) $component_value['RERATA'], $tcomp['Value_Precission'] );
									$tcomp['ReRata'] = number_format($component_value['RERATA'], $tcomp['Value_Precission'], '.', '');
								}

							} else {
								# code...
								$tcomp['ReRata'] = (double) 0;
							}

							if ($third_material[$val['ID_COMPONENT']]) {
								# code...
								$hhoincement += (double)((double) $tcomp['ReRata'] * ((double)($third_material[$val['ID_COMPONENT']]['H2O %']) / 100));								
								$cfprediction += (double)((double) $tcomp['ReRata'] / ((double)($third_material[$val['ID_COMPONENT']]['Component %']) / 100));
							}
							
							
							$tmp_comp[$tcomp['Id Component']] = $tcomp;
						}
					}

					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['Component Data'] = $tmp_comp;			

					$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['H2O IN CEMENT PREDICTED'] = (double) $hhoincement;		
					
					if ((double) $cfprediction == 0 || (double) $hhoincement == 0 ) {
						# code...						
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double) 0;
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double) 0;
					} else {
						# code...
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement)/100);
						$tdata['Competitor'][$value['KODE_PERUSAHAAN']]['CLINKER FACTOR PREDICTED (%)'] = (double)((double) (100-((double) $cfprediction)+(double) $hhoincement));
					}

				}
			}

			$return['Data'] = $tdata;

		}

		to_json($return);

	}

	public function load_tabel_report(){

		$post = $this->input->post();
		$id_company = $post['id_company'];
		$rangetanggal = $post['tanggal'];
		$splitrange = explode(' - ', $rangetanggal);
		$tgl['start'] = date_format(date_create($splitrange[0]), "Y-m-d");
		$tgl['end'] = date_format(date_create($splitrange[1]), "Y-m-d");
		$months['start'] = date_format(date_create($splitrange[0]), "Y-m");
		$months['end'] = date_format(date_create($splitrange[1]), "Y-m");
		$tanggal = '05-2018';
		$bulan = array();
		$detailtanggal = explode('-', $tanggal);
		$bulan[] = $detailtanggal[0];
		$tahun = $detailtanggal[1];
		$id_typeproduct = $post['id_typeproduct'];
		$areagroup = $post['areagroup'];
		
		$return = array();
		$tdata = array();
		$id_comp = array();
		$tdataqm = array();

		$tgroup_area = array();

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$cluster = $this->m_quality_comparison_report->get_cluster($id_company, $tgl, $id_typeproduct, $areagroup);
		foreach ($cluster as $key => $value) {
			$clusterarea = $this->m_quality_comparison_report->get_area_sample_list_scm($value);

			if ($clusterarea) {
				$cluster[$key]['ID_M_KOTA'] = $clusterarea['ID_M_KOTA'];
				$cluster[$key]['KD_PROP'] = $clusterarea['KD_PROP'];
				$cluster[$key]['KD_AREA'] = $clusterarea['KD_AREA'];
				$cluster[$key]['KD_KOTA'] = $clusterarea['KD_KOTA'];
				$cluster[$key]['NM_KOTA'] = $clusterarea['NM_KOTA'];
			}

			$clusterprov = $this->m_quality_comparison_report->get_prov_sample_list_scm($clusterarea['KD_PROP']);

			if ($clusterprov) {
				$cluster[$key]['NM_PROV'] = $clusterprov['NM_PROV'];
				$cluster[$key]['KD_PROV'] = $clusterprov['KD_PROV'];
				$cluster[$key]['KD_PROP_ABBR'] = $clusterprov['KD_PROP_ABBR'];
				$cluster[$key]['KD_PETA'] = $clusterprov['KD_PETA'];
			}

			$tgroup_area[$clusterprov['NM_PROV']]['KD_PROV'] = $cluster[$key]['KD_PROV'];
			$tgroup_area[$clusterprov['NM_PROV']]['NM_PROV'] = $cluster[$key]['NM_PROV'];
			$tgroup_area[$clusterprov['NM_PROV']]['LIST_AREA'][] = (int) $cluster[$key]['ID_M_KOTA'];
		}		

		$dataqm = $this->m_sample_quality->dataQM($tgl, $id_typeproduct);

		$return['Data QM'] = $dataqm;

		foreach ($dataqm as $ind => $val) {

			$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $val['ID_COMPONENT'], $id_typeproduct);

			$mathround = -1;

			if ($precissio_conf) {
				# code...
				$mathround = (int) $precissio_conf['PRECISSION'];
			}

			if ($mathround < 0) {
				# code...
				$tdataqm[$val['ID_COMPONENT']] = (double) $val['RERATA'];
			} else if ($mathround == 0) {
				# code...
				$tdataqm[$val['ID_COMPONENT']] = (int) $val['RERATA'];
			} else {
				# code...
				// $tdataqm[$val['ID_COMPONENT']] = round((double) $val['RERATA'], $mathround );
				$tdataqm[$val['ID_COMPONENT']] = number_format($val['RERATA'], $mathround, '.', '');
			}
		}

		if ($tdataqm) {
			# code...
			$return['Data QM'] = $tdataqm;
		}

		$param = $this->m_sample_quality->component_checklist_order();

		foreach ($param as $key => $value) {

			# code...
			if ((int)$value['STATUS_CHECKLIST'] == 1) {
				# code...
				$tcomp['Id Component'] = (int) $value['ID_COMPONENT'];
				$tcomp['Component Code'] = $value['KD_COMPONENT'];
				$tcomp['Component Name'] = $value['NM_COMPONENT'];
				$smigstd = $this->m_sample_quality->component_min_max($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
				if ($smigstd) {
					# code...
					$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
					$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
				} else {
					# code...
					$tcomp['V_Min'] = 0;
					$tcomp['V_Max'] = 0;
				}


				$snistd = $this->m_sample_quality->component_SNI_standard($months, (int) $value['ID_COMPONENT'], $id_typeproduct);
				if ($snistd) {
					# code...
					$tcomp['SNI_Min'] = round((double) $snistd['MIN_VALUE'], 1);
					$tcomp['SNI_Max'] = round((double) $snistd['MAX_VALUE'], 1);
				} else {
					# code...
					$tcomp['SNI_Min'] = 0;
					$tcomp['SNI_Max'] = 0;
				}

				$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

				if ($precissio_conf) {
					# code...
					$tcomp['Value_Precission'] = (int) $precissio_conf['PRECISSION'];
				} else {
					# code...
					$tcomp['Value_Precission'] = null;
				}
				
				$id_comp[] = $tcomp;
			}
		}

		$return['Checklist'] = $id_comp;

		if ($cluster) {
			# code...
			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			foreach ($tgroup_area as $ind => $val) {

				# code...

				$clusterdetail = $this->m_quality_comparison_report->get_cluster_detail($id_company, $tgl, $id_typeproduct, $areagroup, $val['LIST_AREA']);

				if ($clusterdetail) {
					# code...

					foreach ($clusterdetail as $key => $value) {
						# code...

						$clustercompetitor = $this->m_quality_comparison_report->get_competitor_detail_scm($value['ID_COMPETITOR']);

						if ($clusterarea) {
							$clusterdetail[$key]['KODE_PERUSAHAAN'] = $clustercompetitor['KODE_PERUSAHAAN'];
							$clusterdetail[$key]['NAMA_PERUSAHAAN'] = $clustercompetitor['NAMA_PERUSAHAAN'];
							$clusterdetail[$key]['INISIAL'] = $clustercompetitor['INISIAL'];
							$clusterdetail[$key]['PRODUK'] = $clustercompetitor['PRODUK'];
							if ($clustercompetitor['KELOMPOK'] == 'SMI') {
								# code...
								$clusterdetail[$key]['IS_SMIG'] = (int) 1;
							} else {
								# code...
								$clusterdetail[$key]['IS_SMIG'] = (int) 0;
							}
							
						}
					}

					foreach ($clusterdetail as $key => $value) {

						if ((int) $value['IS_SMIG'] == 1 && (int) $value['ID_COMPANY'] == (int) $id_company && (int) $value['ID_COMPETITOR'] == (int) $id_company) {
							# code...
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAMA_PERUSAHAAN'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Competitor Inisial'] = $value['INISIAL'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Merk Product'] = $value['PRODUK'];

							$ms_total = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, null, (int) $id_typeproduct);
							$ms_qty = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, $value['ID_COMPETITOR'], (int) $id_typeproduct);

							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share Total'] = (double) $ms_total['MS_QTY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share Qty'] = (double) $ms_qty['MS_QTY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Market Share (%)'] =  (double)((double) $ms_qty['MS_QTY'] / (double) $ms_total['MS_QTY']);
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
							$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];
							
							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							if ($mathround < 0) {
								# code...
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
							} else if ($mathround == 0) {
								# code...
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (int) $value['RERATA'];
							} else {
								# code...
								// $tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = round((double) $value['RERATA'], $mathround );
								$tdata[$val['NM_PROV']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = number_format($value['RERATA'], $mathround, '.', '');
							}

						} else {
							# code...
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAMA_PERUSAHAAN'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Competitor Inisial'] = $value['INISIAL'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Merk Product'] = $value['PRODUK'];

							$ms_total = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, null, (int) $id_typeproduct);
							$ms_qty = $this->m_quality_comparison_report->get_marketshare_value_scm($val['KD_PROV'], $months, $value['ID_COMPETITOR'], (int) $id_typeproduct);
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share Total'] = (double) $ms_total['MS_QTY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share Qty'] = (double) $ms_qty['MS_QTY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Market Share (%)'] =  (double)((double) $ms_qty['MS_QTY'] / (double) $ms_total['MS_QTY']);
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
							$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];

							$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

							$mathround = -1;

							if ($precissio_conf) {
								# code...
								$mathround = (int) $precissio_conf['PRECISSION'];
							}

							if ($mathround < 0) {
								# code...
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
							} else if ($mathround == 0) {
								# code...
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (int) $value['RERATA'];
							} else {
								# code...
								// $tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = round((double) $value['RERATA'], $mathround );
								$tdata[$val['NM_PROV']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = number_format($value['RERATA'], $mathround, '.', '');
							}

						}

					}

					$return['Data'] = $tdata;
				}

			}
		}

		to_json($return);
	}

	public function load_tabel_report_backup(){

		$post = $this->input->post();

		$id_company = $post['id_company'];
		$tanggal = $post['tanggal'];
		$id_typeproduct = $post['id_typeproduct'];
		$areagroup = $post['areagroup'];
		
		$return = array();
		$tdata = array();
		$id_comp = array();
		$tdataqm = array();

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$cluster = $this->m_quality_comparison_report->get_cluster($id_company, $tanggal, $id_typeproduct, $areagroup);

		$dataqm = $this->m_sample_quality->dataQM($tanggal, $id_typeproduct);

		$return['Data QM'] = $dataqm;

		foreach ($dataqm as $ind => $val) {
			$tdataqm[$val['ID_COMPONENT']] = (double) $val['RERATA'];
		}

		if ($tdataqm) {
			# code...

			$return['Data QM'] = $tdataqm;
		}

		$param = $this->m_sample_quality->component_checklist_order();

		foreach ($param as $key => $value) {

			# code...
			if ((int)$value['STATUS_CHECKLIST'] == 1) {
				# code...
				$tcomp['Id Component'] = (int) $value['ID_COMPONENT'];
				$tcomp['Component Code'] = $value['KD_COMPONENT'];
				$tcomp['Component Name'] = $value['NM_COMPONENT'];
				$smigstd = $this->m_sample_quality->component_min_max($tanggal, (int) $value['ID_COMPONENT'], $id_typeproduct);
				if ($smigstd) {
					# code...
					$tcomp['V_Min'] = (double) $smigstd['V_MIN'];
					$tcomp['V_Max'] = (double) $smigstd['V_MAX'];
				} else {
					# code...
					$tcomp['V_Min'] = 0;
					$tcomp['V_Max'] = 0;
				}
				
				$precissio_conf = $this->m_sample_quality->component_precission($months, (int) $value['ID_COMPONENT'], $id_typeproduct);

				if ($precissio_conf) {
					# code...
					$tcomp['Value_Precission'] = (int) $precissio_conf['PRECISSION'];
				} else {
					# code...
					$tcomp['Value_Precission'] = null;
				}
				
				$id_comp[] = $tcomp;
			}
		}

		$return['Checklist'] = $id_comp;

		if ($cluster) {
			# code...
			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			$clusterdetail = $this->m_quality_comparison_report->get_cluster_detail($id_company, $tanggal, $id_typeproduct, $areagroup);

			if ($clusterdetail) {
				# code...

				foreach ($clusterdetail as $key => $value) {
					# code...

					if ((int) $value['IS_SMIG'] == 1 && (int) $value['ID_COMPANY'] == (int) $id_company) {
						# code...
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAME_COMPETITOR'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Merk Product'] = $value['MERK_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Id Type'] = $value['TYPE_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Type Product'] = $value['KD_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];
						$tdata[$value['GROUP_AREA']]['SMIG'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
					} else {
						# code...
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Competitor Name'] = $value['NAME_COMPETITOR'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Merk Product'] = $value['MERK_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Id Type'] = $value['TYPE_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Type Product'] = $value['KD_PRODUCT'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Is SMIG'] = (int) $value['IS_SMIG'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Id Company'] = (int) $value['ID_COMPANY'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Name'] = $value['KD_COMPONENT'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['Component Kode'] = $value['KD_COMPONENT'];
						$tdata[$value['GROUP_AREA']]['Competitor'][$value['ID_COMPETITOR']]['Data'][$value['ID_COMPONENT']]['ReRata'] = (double) $value['RERATA'];
					}

				}

				$return['Data'] = $tdata;
			}
		}

		to_json($return);
	}


	public function cobaview(){
        $this->template->adminlte("v_cobacobacomparison");

    }

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
