   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Data Sample Area
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">


          <div class="box" id="formArea" style="display: none;">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Sample Area</h3>
              </div>

              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>

              <div class="box-body">
              	
                  <div class="form-group clearfix">
                    <div class="col-sm-4 ">
                    <label>NAME AREA</label><span  style="color:red; float: right;">(*)</span>
                    <input type="text" class="form-control hidden" name="ID_SAMPLE_AREA" id="ID_SAMPLE_AREA" placeholder="Id Area" required >
                    <input type="text" class="form-control" name="NAME_SAMPLE_AREA" id="NAME_SAMPLE_AREA" placeholder="Name Area" required >
                    </div>

                    <div class="col-sm-4 ">
                    <label>NAME PROVINCE</label><span  style="color:red; float: right;">(*)</span>
                    <input type="text" class="form-control" name="NAME_PROVINCE" id="NAME_PROVINCE" placeholder="Name Province" required >
                    </div>

                  </div>

                  <div class="form-group clearfix">
                    <div class="col-sm-4 ">
                    <label>NAME ISLAND</label><span  style="color:red; float: right;">(*)</span>
                    <input type="text" class="form-control" name="NAME_ISLAND" id="NAME_ISLAND" placeholder="Name Island" required >
                    </div>

                    <div class="col-sm-4 ">
                    <label>NAME REGION</label><span  style="color:red; float: right;">(*)</span>
                    <input type="text" class="form-control" name="NAME_REGION" id="NAME_REGION" placeholder="Name Region" required >
                    </div>

                  </div>

              </div>

              <div class="box-footer">
                <button type="button" id="AddArea" class="btn btn-primary"><i class="fa fa-save"></i>  &nbsp;Save</button>
                <button type="button" id="UpdateArea" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-default btnCancel">Cancel</button>
              </div>
            </div>
          </div>
		
		
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Sample Area ( <i> scm.semenindonesia.com </i> )</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">No.</th>
                        <th width="*">CODE AREA</th>
                        <th width="*">NAME AREA</th>
                        <th width="*">CODE PROVINCE</th>
                        <th width="*">NAME PROVINCE</th>
                        <th width="*">CODE MAP</th>
                        <th width="*">CODE ISLAND</th>
                        <th width="*">NAME ISLAND</th>
                      </tr>
                    </thead>
                  </table>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>

<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">

<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- TypeHead js -->
<script src="<?php echo base_url("plugins/typehead/bootstrap-typeahead.js");?>"></script>


<script>

$(document).ready(function(){

  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable;

  reloadtabel(); 

  $(document).on('click',"#AddNew",function () {
    
    $("#saving").css('display','');
    $('#formArea').show();
    $('#viewArea').hide();

    $('#UpdateArea').hide();
    $('#AddArea').show();

    $('#ID_SAMPLE_AREA').val('');
    $('#NAME_SAMPLE_AREA').val('');
    
    $("#saving").css('display','none');
  });

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });


  $(document).on('click',"#AddArea",function () {
    
    $("#saving").css('display','');
    if ($('#NAME_SAMPLE_AREA').val() != '' && $('#NAME_PROVINCE').val() != '' && $('#NAME_ISLAND').val() != '' && $('#NAME_REGION').val() != '') {
      $.ajax({
        url: "<?php echo site_url("sample_area/save_area");?>",
        data: {"NAME_AREA": $('#NAME_SAMPLE_AREA').val(), "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }              
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
      $("#a-notice-error").click();
      if ($('#NAME_SAMPLE_AREA').val() == '') {
        $('#NAME_SAMPLE_AREA').focus();
      } else if ($('#NAME_PROVINCE').val() == '') {
        $('#NAME_PROVINCE').focus();
      } else if ($('#NAME_ISLAND').val() == '') {
        $('#NAME_ISLAND').focus();
      } else {
        $('#NAME_REGION').focus();
      }
    }
    
    $("#saving").css('display','none');
  });

  $(document).on('click',"#UpdateArea",function () {
    
    $("#saving").css('display','');
    if ($('#NAME_SAMPLE_AREA').val() != '' && $('#NAME_PROVINCE').val() != '' && $('#NAME_ISLAND').val() != '' && $('#NAME_REGION').val() != '') {
      $.ajax({
        url: "<?php echo site_url("sample_area/update_area");?>",
        data: {"ID_SAMPLE_AREA": $('#ID_SAMPLE_AREA').val(), "NAME_AREA": $('#NAME_SAMPLE_AREA').val(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });      
    } else {
      $("#a-notice-error").click();
      if ($('#NAME_SAMPLE_AREA').val() == '') {
        $('#NAME_SAMPLE_AREA').focus();
      } else if ($('#NAME_PROVINCE').val() == '') {
        $('#NAME_PROVINCE').focus();
      } else if ($('#NAME_ISLAND').val() == '') {
        $('#NAME_ISLAND').focus();
      } else {
        $('#NAME_REGION').focus();
      }
    }
    
    $("#saving").css('display','none');
  });

  $(document).on('click',".btnCancel",function () {
    
    $("#saving").css('display','');
    $('#formArea').hide();
    $('#viewArea').show();

    $('#ID_SAMPLE_AREA').val('');
    $('#NAME_SAMPLE_AREA').val('');
    $('#NAME_PROVINCE').val('');
    $('#NAME_ISLAND').val('');
    $('#NAME_REGION').val('');
    reloadtabel();
    
    $("#saving").css('display','none');      
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    
    $("#saving").css('display','');
    $('#formArea').show();
    $('#viewArea').hide();  

    $('#UpdateArea').show();
    $('#AddArea').hide(); 

    var data = oTable.row($(this).parents('tr')).data();

    $('#ID_SAMPLE_AREA').val(data['ID_SAMPLE_AREA']);
    $('#NAME_SAMPLE_AREA').val(data['NAME_AREA']);
    $('#NAME_PROVINCE').val(data['NAME_PROVINCE']);
    $('#NAME_ISLAND').val(data['NAME_ISLAND']);
    $('#NAME_REGION').val(data['NAME_REGION']);
    
    $("#saving").css('display','none');
  });

  /** btDelete Click **/
  $(document).on('click',".btDelete",function () {
    var data = oTable.row($(this).parents('tr')).data();
    $.confirm({
        title: "Remove Component",
        text: "This sample area will be removed. Are you sure?",
        confirmButton: "Remove",
        confirmButtonClass: "btn-danger",
        cancelButton: "Cancel",
        confirm: function() {
          $.ajax({
            url: "<?php echo site_url("sample_area/delete_area");?>",
            data: {"ID_SAMPLE_AREA": data['ID_SAMPLE_AREA'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
              if (res['status'] == true) {
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
              } else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              reloadtabel();            
            },
            error: function () {
              $("#a-notice-error").click();
            }
          });
        },
        cancel: function() {
        }
    });
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("sample_area/area_list_scm");?>',
        type: "POST"
      },
      columns: [
        {"data": "ID_M_KOTA", "width": 50},
        {"data": "KD_KOTA"},
        {"data": "NM_KOTA"},
        {"data": "KD_PROV"},
        {"data": "NM_PROV"},
        {"data": "KD_PETA"},
        {"data": "KD_PULAU"},
        {"data": "NM_PULAU"},
      ]
    });

    $('#ReloadData').show();
  }

	$(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});

	$(".notice-error").confirm({ 
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});
	
	$(".notice-success").confirm({ 
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-success"
	});
		
	<?php if($notice->error): ?>
	$("#a-notice-error").click();
	<?php endif; ?>
	
	<?php if($notice->success): ?>
	$("#a-notice-success").click();
	<?php endif; ?>
	
});
</script>
