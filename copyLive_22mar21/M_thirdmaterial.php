<?php

class M_thirdmaterial Extends DB_QM {
	private $post  = array();
	private $table = "O_THIRD_MATERIAL";
	private $pKey  = "ID_MATERIAL";
	private $column_order = array(NULL, 'URUTAN'); //set column for datatable order
    private $column_search = array('NAME_COMPETITOR', 'NAME_PRODUCT'); //set column for datatable search 
    private $order = array("A.ID_MATERIAL" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	var $column = array(
		'A.ID_MATERIAL', 'A.KODE_MATERIAL', 'A.NAME_MATERIAL', 'A.ID_MATERIAL'
	);


	var $column_scm = array(
		'A.KODE_PERUSAHAAN', 'A.NAMA_PERUSAHAAN', 'A.INISIAL', 'A.PRODUK', 'A.KELOMPOK'
	);	

	public function list_component($ID_COMPONENT=null){
		$this->db->where("LOWER(a.KD_COMPONENT) !=","h2o");
		$this->db->or_where("LOWER(a.KD_COMPONENT) !=","h2o");
		if($ID_COMPONENT) $this->db->where("a.ID_COMPONENT", $ID_COMPONENT);
		return $this->db->get("M_COMPONENT a")->result();
	}

	public function get_third_material_list(){
		$this->db->order_by("ID_MATERIAL", "ASC");
		return $this->db->get("O_THIRD_MATERIAL a")->result_array();
	}

	public function get_component_relation(){		
		$this->db->select('A.*, B.KD_COMPONENT, B.NM_COMPONENT');
		$this->db->from('O_THIRD_MATERIAL A');
		$this->db->join('M_COMPONENT B', 'A.ID_COMPONENT = B.ID_COMPONENT');
		$this->db->order_by("A.ID_COMPONENT", "ASC");
		$query		= $this->db->get();
		$data			= $query->result_array();
		return $data;
	}

	public function get_SNI_standard_component($type, $comp){		
		$this->db->from('O_SNI_STANDARD_COMP A');
		$this->db->where("ID_COMPONENT", $comp);
		$this->db->where("ID_TYPE_PRODUCT", $type);
		$this->db->order_by("A.ID_SNI_STD", "DESC");
		$query		= $this->db->get();
		$data			= $query->row_array();
		return $data;
	}

	public function get_config_precission_component($comp){		
		$this->db->from('O_PRECISSION_CONFIG A');
		$this->db->where("ID_COMPONENT", $comp);
		$this->db->order_by("A.ID_PRECISSION_CONFIG", "DESC");
		$query		= $this->db->get();
		$data			= $query->row_array();
		return $data;
	}

	public function get_all_component_list(){		
		$this->db->from('M_COMPONENT A');
		$this->db->order_by("A.ID_COMPONENT", "ASC");
		$query		= $this->db->get();
		$data			= $query->result_array();
		return $data;
	}

	public function is_sample_exists($data){
		if (isset($data['ID_MATERIAL'])) {
			# code...
		}
		$this->db->where("LOWER(KODE_MATERIAL)", strtolower($data['KODE_MATERIAL']));
		$this->db->where("LOWER(NAME_MATERIAL)", strtolower($data['NAME_MATERIAL']));
		return $this->db->get("O_THIRD_MATERIAL")->num_rows();
	}

	public function sample_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("O_THIRD_MATERIAL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("KODE_MATERIAL", $data['KODE_MATERIAL']);
		$this->db->set("NAME_MATERIAL", $data['NAME_MATERIAL']);
		$this->db->set("H2O_PERCENTAGE", $data['H2O_PERCENTAGE']);
		$this->db->set("ID_COMPONENT", $data['ID_COMPONENT']);
		$this->db->set("COMP_PERCENTAGE", $data['COMP_PERCENTAGE']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $data['MODIFIED_BY']);
		$this->db->where("ID_MATERIAL", $data['ID_MATERIAL']);
		$query = $this->db->update("O_THIRD_MATERIAL");

		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_delete($data){
		$this->db->where("ID_MATERIAL", $data['ID_MATERIAL']);
		$query = $this->db->delete("O_THIRD_MATERIAL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}


	public function is_SNI_exists($type, $comp, $usernam){
		$this->db->where("ID_COMPONENT", (int) $comp);
		$this->db->where("ID_TYPE_PRODUCT", (int) $type);
		$query = $this->db->get("O_SNI_STANDARD_COMP")->num_rows();
		return $query;

	}

	public function SNI_insert($type, $comp, $data, $usernam){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("ID_COMPONENT", (int) $comp);
		$this->db->set("ID_TYPE_PRODUCT", (int) $type);
		if ($data[1] != '' && $data[1] && (int) $data[1] != 0) {
			# code...
			$this->db->set("MIN_VALUE", (float) $data[1]);
		}

		if ($data[2] != '' && $data[2] && (int) $data[2] != 0) {
			# code...
			$this->db->set("MAX_VALUE", (float) $data[2]);
		}

		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("CREATE_BY", $usernam);
		$query = $this->db->insert("O_SNI_STANDARD_COMP");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function SNI_update($type, $comp, $data, $usernam){
		$dtnow = date("Y-m-d H:i:s");
		if ($data[1] != '' && $data[1] && (int) $data[1] != 0) {
			# code...
			$this->db->set("MIN_VALUE", (float) $data[1]);
		} else {
			# code...
			$this->db->set("MIN_VALUE", null );
		}

		if ($data[2] != '' && $data[2] && (int) $data[2] != 0) {
			# code...
			$this->db->set("MAX_VALUE", (float) $data[2]);
		} else {
			# code...
			$this->db->set("MAX_VALUE", null );
		}

		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $usernam);
		$this->db->where("ID_COMPONENT", $comp);
		$this->db->where("ID_TYPE_PRODUCT", $type);
		$query = $this->db->update("O_SNI_STANDARD_COMP");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function SNI_delete($type, $comp){
		$this->db->where("ID_COMPONENT", $comp);
		$this->db->where("ID_TYPE_PRODUCT", $type);
		$query = $this->db->delete("O_SNI_STANDARD_COMP");

		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function is_config_precission_exists( $comp, $usernam){
		$this->db->where("ID_COMPONENT", (int) $comp);
		$query = $this->db->get("O_PRECISSION_CONFIG")->num_rows();
		return $query;

	}

	public function config_precission_insert( $comp, $data, $usernam){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("ID_COMPONENT", (int) $comp);
		if ($data[1] != '' ) {
			# code...
			$this->db->set("PRECISSION", (float) $data[1]);
		}

		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("CREATE_BY", $usernam);
		$query = $this->db->insert("O_PRECISSION_CONFIG");

		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function config_precission_update($comp, $data, $usernam){
		$dtnow = date("Y-m-d H:i:s");
		if ($data[1] != '' ) {
			# code...
			$this->db->set("PRECISSION", (float) $data[1]);
		} else {
			# code...
			$this->db->set("PRECISSION", null );
		}

		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $usernam);
		$this->db->where("ID_COMPONENT", $comp);
		$query = $this->db->update("O_PRECISSION_CONFIG");

		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function config_precission_delete($comp){
		$this->db->where("ID_COMPONENT", $comp);
		$query = $this->db->delete("O_PRECISSION_CONFIG");

		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	function _qry($key){
		$this->db->select('A.*, B.KD_COMPONENT, B.NM_COMPONENT');
		$this->db->from('O_THIRD_MATERIAL A');
		$this->db->join('M_COMPONENT B', 'A.ID_COMPONENT = B.ID_COMPONENT');
		if($key['search']!==''){
			$this->db->or_like('LOWER(A.KODE_MATERIAL)', strtolower($key['search']));
			$this->db->or_like('LOWER(A.NAME_MATERIAL)', strtolower($key['search']));
		}
		$this->db->where('A.DELETE_FLAG', 0);
		$order = $this->column[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function _qryscm($key){
		$this->scmdb->select('A.*');
		$this->scmdb->from('ZREPORT_MS_PERUSAHAAN A');
		if($key['search']!==''){
			$this->scmdb->or_like('LOWER(A.KODE_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.NAMA_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.INISIAL)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.PRODUK)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.KELOMPOK)', strtolower($key['search']));
		}
		$order = $this->column_scm[$key['ordCol']];
		$this->scmdb->order_by($order, $key['ordDir']);

	}

	function get($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();
		return $data;
	}

	function get_data($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil($key){
		$this->_qry($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot(){
		$query			= $this->db->get('O_THIRD_MATERIAL');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function get_data_scm($key){
		$this->_qryscm($key);
		$this->scmdb->limit($key['length'], $key['start']);
		$query		= $this->scmdb->get();
		$data			= $query->result();
		return $data;
	}

	function recFil_scm($key){
		$this->_qryscm($key);
		$query			= $this->scmdb->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_scm(){
		$query			= $this->scmdb->get('ZREPORT_MS_PERUSAHAAN');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	public function datalist(){
		$this->db->order_by("a.ID_COMPETITOR");
		return $this->db->get("O_COMPETITOR a")->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

}
