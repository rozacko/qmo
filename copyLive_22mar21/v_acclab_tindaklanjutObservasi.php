
<section class="content-header">
    <h1>
        Tindak Lanjut Observasi (Akurasi Labor SIG)
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <form class="form-inline">

                    </form>
                    <!--                    <hr/>-->

                </div>

                <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover dt-responsive nowrap "
                            cellspacing="0"
                            width="100%">
                        <thead>
                        <tr>
                            <th width="1">No.</th>
                            <th >Nama Akurasi Labor</th>
                            <th width="1">Tahun </th>
                            <th >Jenis Pelaksanaan</th>
                            <th>Plant</th>
                            <th >PIC Observasi</th>
                            <th >PIC Plant</th>
                            <th >Start Date</th>
                            <th >End Date</th>
                            <th width="1">Status</th>
                            <th ></th>
                        </tr>
                        </thead>
                        <tbody style="font-weight: normal;">
                        <?php
                        $count = 1;
                        foreach ($this->list_data as $dt) { ?>
                            <tr>
                                <td><?= $count++; ?></td>
                                <td><?= $dt->NAMA;?></td>
                                <td><?= $dt->TAHUN;?></td>
                                <td><?= $dt->JENIS;?></td>
                                <td><?= $dt->NAMA_LAB;?> </td>
                                <td><?= $dt->FULLNAME_OBSERVASI;?> </td>
                                <td><?= $dt->FULLNAME_PLANT;?> </td>
                                <td><?= $dt->START_DATE;?> </td>
                                <td><?= $dt->END_DATE;?> </td>
                                <td><?= $dt->STATUS;?> </td>
                                <td>
<!--                                    <a href="#"><button title="Detail" class="btEdit  btn btn-warning btn-xs" type="button"><i class="fa fa-arrows "></i> Detail</button></a>-->
                                    <a id="tindakLanjutPress<?php echo $dt->ID; ?>" data-rekom="<?php echo $dt->REKOMENDASI; ?>" onclick="openInput(<?php echo $dt->FK_ID_PELAKSANAAN . ', ' . $dt->ID; ?>);"><button title="Input Temuan & Rekomendasi" class="btEdit  btn btn-success btn-xs" type="button"><i class="fa fa-calendar "></i> Input Tindak Lanjut</button></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>

    <div class="row" id="inputSection" hidden>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3>Input Tindak Lanjut</h3>
                    <hr/>
                </div>
                <form role="form">
                    <input type="hidden" id="idPelaksana" name="idPelaksana">
                    <input type="hidden" id="idTemuan" name="idTemuan">
                    <div class="box-body" id="formBody">
                    </div>
                    <div class="form-group c-group after-add-more">
                        <div class="col-sm-12">
                            <label>Rekomendasi </label>
                            <input type="text" disabled class="form-control" id="rekomendasi" name="rekomendasi" placeholder="Isikan Rekomendasi"  >
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top: 2em;">
                        <button onclick="closeInput();" type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<div id="tindakModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <form role="form">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Input Tindak Lanjut:</b> </h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="idObsin" name="idObsin">
                    <div class="form-group c-group after-add-more">
                        <div class="col-sm-12">
                            <label>Hasil Observasi </label>
                            <input type="text" class="form-control" id="result" name="result" placeholder="Isikan hasil Observasi"  >
                        </div>
                    </div>
                    <div class="form-group c-group after-add-more">
                        <div class="col-sm-12">
                            <label>Compliance </label>
                            <input type="text" class="form-control" id="compliance" name="compliance" placeholder="Isikan Compliance"  >
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 2em;">
                    <button onclick="submitTindakLanjut()" type="button" class="btn btn-primary" style="margin-top: 2em;">Save</button>
                    <button onclick="closeModal()" type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
    <a  id="a-notice-error"
        class="notice-error"
        style="display:none";
        href="#"
        data-title="Something Error"
        data-text="<?php echo $notice->error; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->error; ?>');
    </script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
    <a  id="a-notice-success"
        class="notice-success"
        style="display:none";
        href="#"
        data-title="Done!"
        data-text="<?php echo $notice->success; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->success; ?>');
    </script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
    .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
    $(document).ready(function(){

        /** DataTables Init **/
        var table = $("#dt_tables").DataTable();
        getForm();
    });

    function getForm() {
        $.ajax({
            url: '<?php echo site_url("acclab_tindaklanjutobservasi/getKomponenForm"); ?>',
            type: "POST",
            dataType: "JSON",
            success: function (res) {
                if (res.status == true) {
                    console.log(res);
                    document.getElementById("formBody").innerHTML = res.data;
                    $("#a-notice-success").data("text", res.msg);
                    $("#a-notice-success").click();
                    $("#ReloadData").click();
                    $("#inputModal").modal('hide');
                } else {
                    $("#a-notice-error").data("text", res.msg);
                    $("#a-notice-error").click();
                }
            },
            error: function () {
                $("#a-notice-error").click();
            }
        });
    }

    function openInput(fkid, temuanID) {
        var x = document.getElementById("tindakLanjutPress" + temuanID);
        var rekom = x.getAttribute('data-rekom');
        console.log(rekom);

        $.ajax({
            url: '<?php echo site_url("acclab_tindaklanjutobservasi/getCurrentTemuan"); ?>',
            data: {"temuanID": temuanID},
            type: "POST",
            dataType: "JSON",
            success: function (res) {
                if (res.status == true) {
                    console.log(res);
                    var data = res.data;
                    data.forEach(setInput);

                    $("#a-notice-success").data("text", res.msg);
                    $("#a-notice-success").click();
                    $("#ReloadData").click();
                } else {
                    $("#a-notice-error").data("text", res.msg);
                    $("#a-notice-error").click();
                }
            },
            error: function () {
                $("#a-notice-error").click();
            }
        });

        document.getElementById('idPelaksana').value = fkid;
        document.getElementById('idTemuan').value = temuanID;
        if (rekom === "") {
            document.getElementById('rekomendasi').value = "-";
        } else {
            document.getElementById('rekomendasi').value = rekom;
        }
        document.getElementById("inputSection").removeAttribute("hidden");
    }

    function setInput(item, index) {
        console.log("field_" + item.FK_ID_FIELD + " - " + item.CONTENT);
        if (item.CONTENT !== "" && item.CONTENT !== null) {
            document.getElementById("field_" + item.FK_ID_FIELD).value = item.CONTENT;
        } else {
            document.getElementById("field_" + item.FK_ID_FIELD).value = "-";
        }
    }

    function closeInput() {
        document.getElementById("inputSection").setAttribute("hidden", "true");
    }

    function openModal(id) {
        var idTemuan = $('#idTemuan').val();
        $.ajax({
            url: '<?php echo site_url("acclab_tindaklanjutobservasi/getDetail"); ?>',
            data: {"idTemuan": idTemuan, "idField": id},
            type: "POST",
            dataType: "JSON",
            success: function (res) {
                if (res.status == true) {
                    console.log(res);
                    var dataRes = res.data;
                    document.getElementById("idObsin").value = dataRes["ID"];
                    document.getElementById("result").value = dataRes["OBSERVATION_RESULT"];
                    document.getElementById("compliance").value = dataRes["COMPLIANCE"];
                    $("#a-notice-success").data("text", res.msg);
                    $("#a-notice-success").click();
                    $("#ReloadData").click();
                } else {
                    $("#a-notice-error").data("text", res.msg);
                    $("#a-notice-error").click();
                }
            },
            error: function () {
                $("#a-notice-error").click();
            }
        });
        $("#tindakModal").modal('show');
    }

    function closeModal() {
        $("#tindakModal").modal('hide');
    }

    function submitTindakLanjut() {
        var result = $('#result').val();
        var compliance = $('#compliance').val();
        var idObsin = $('#idObsin').val();
        var idPelaksana = $('#idPelaksana').val();

        var formData = {"idPelaksana": idPelaksana, "result": result, "compliance": compliance, "idObsin": idObsin};

        ask = confirm("Are you sure you want to submit data?");
        if (ask) {
            $.ajax({
                url: '<?php echo site_url("acclab_tindaklanjutobservasi/submitTindakLanjut"); ?>',
                data: formData,
                type: "POST",
                dataType: "JSON",
                success: function (res) {
                    if (res.status == true) {
                        console.log(res);
                        $("#a-notice-success").data("text", res.msg);
                        $("#a-notice-success").click();
                        $("#ReloadData").click();
                        document.getElementById("result").value = "";
                        document.getElementById("compliance").value = "";
                        $("#tindakModal").modal('hide');
                    } else {
                        $("#a-notice-error").data("text", res.msg);
                        $("#a-notice-error").click();
                    }
                },
                error: function () {
                    $("#a-notice-error").click();
                }
            });
        }
    }
</script>
<script>
    function doconfirm(){
        job=confirm("Are you sure you want to delete data?");
        if(job!=true){
            return false;
        }
    }
</script>