<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_aspek extends QMUser {

	public $LIST;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
	}
	
	public function index(){
		$this->template->adminlte("v_jenis_aspek");
	}

	public function add(){
		$this->template->adminlte("v_jenis_aspek_add");
	}

	public function edit(){
		$this->LIST = $this->M_jenis_aspek->get_by_id($this->input->post('ID_JENIS_ASPEK'));
		
		$this->template->adminlte("v_jenis_aspek_edit", $list);
	}

	public function get_list(){
		$list = $this->M_jenis_aspek->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_JENIS_ASPEK;
			$row[] = $no;
			$row[] = $column->JENIS_ASPEK;
			$row[] = $column->BOBOT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_jenis_aspek->count_all(),
            "recordsFiltered" => $this->M_jenis_aspek->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		$this->M_jenis_aspek->insert(array_map('strtoupper', $this->input->post()));
		if($this->M_jenis_aspek->error()){
			$this->notice->error($this->M_jenis_aspek->error());
			redirect("jenis_aspek/add");
		}
		else{
			$this->notice->success("Jenis Aspek Data Saved.");
			redirect("jenis_aspek");
		}
	}

	public function update(){
		$data['JENIS_ASPEK'] = strtoupper($this->input->post('JENIS_ASPEK'));
		$data['BOBOT'] = strtoupper($this->input->post('BOBOT'));
		$this->M_jenis_aspek->update($this->input->post('ID_JENIS_ASPEK'), $data);
		
		if($this->M_jenis_aspek->error()){
			$this->notice->error($this->M_jenis_aspek->error());
			redirect("jenis_aspek/edit");
		}
		else{
			$this->notice->success("Jenis Aspek Data Updated.");
			redirect("jenis_aspek");
		}
	}

	public function remove(){
		$this->M_jenis_aspek->delete($this->input->post('ID_JENIS_ASPEK')); 
		if($this->M_jenis_aspek->error()){
			$this->notice->error($this->M_jenis_aspek->error());
		}
		else{
			$this->notice->success("Jenis Aspek Data Removed.");
		}
		redirect("jenis_aspek");
	}

}

/* End of file Jenis_aspek.php */
/* Location: ./application/controllers/Jenis_aspek.php */
?>
