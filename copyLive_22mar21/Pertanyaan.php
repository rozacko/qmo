<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pertanyaan extends QMUser {

    public $list_form = array();
    public $list_product = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper("string");
        $this->load->model("m_pertanyaan");
        $this->load->model("m_form_pengujian");
    }

    public function index(){
        $this->libExternal('select2');
        $this->list_form = $this->m_form_pengujian->data_list();
		$this->template->adminlte("v_form_pertanyaan");
    }

    public function get_list(){
        $id_form = $this->input->post("id_form");
        $tipe = $this->input->post("tipe");

        $list = $this->m_pertanyaan->get_list($id_form, $tipe);
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["ID_QUESTION"] = $column->ID_QUESTION;
			$row["QUEST_TEXT"] = $column->QUEST_TEXT;
			$row["QUEST_TYPE"] = $column->QUEST_TYPE;
			$row["QUEST_OPTIONAL"] = $column->QUEST_OPTIONAL;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_pertanyaan->count_all($id_form, $tipe),
            "recordsFiltered" => $this->m_pertanyaan->count_filtered($id_form, $tipe),
            "data" => $data,
        );

		to_json($output);
    }

    public function create(){
		$this->m_pertanyaan->insert($this->input->post());
		if($this->m_pertanyaan->error()){
			$status = false;
		}
		else{
            $status = true;
        }
        
        echo $status;
    }

    public function detail($ID_QUESTION){
        $list = $this->m_pertanyaan->detail($ID_QUESTION);
        to_json($list);
    }

    public function update($ID_QUESTION){
		$this->m_pertanyaan->update($this->input->post(), $ID_QUESTION);
		if($this->m_pertanyaan->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
	}

    public function delete($ID_QUESTION){
		$this->m_pertanyaan->delete($ID_QUESTION);
		if($this->m_pertanyaan->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
	}
}