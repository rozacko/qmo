<?php

class Plant extends QMUser {
	
	public $list_plant = array();
	public $list_company = array();
	
	public $ID_COMPANY;
	public $data_plant;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_plant");
		$this->load->model("m_company");
		$this->ID_COMPANY = ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$this->ID_COMPANY;
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist($this->ID_COMPANY); #echo $this->m_plant->get_sql();
		$this->template->adminlte("v_plant");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$ID_COMPANY = ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$ID_COMPANY;
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->list_company = $this->m_company->datalist($this->ID_COMPANY);
		$this->template->adminlte("v_plant_add");
	}
	
	public function create($ID_COMPANY=NULL){
		$this->m_plant->insert($this->input->post());
		if($this->m_plant->error()){
			$this->notice->error($this->m_plant->error());
			redirect("plant/add/".$this->input->post("ID_COMPANY"));
		}
		else{
			$this->notice->success("Plant Data Saved.");
			redirect("plant/by_company/".$this->input->post("ID_COMPANY"));
		}
	}
	
	public function edit($ID_PLANT){

		$this->list_company = $this->m_company->datalist($this->ID_COMPANY);
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		$this->template->adminlte("v_plant_edit");
	}
	
	public function update($ID_PLANT){
		$this->m_plant->update($this->input->post(),$ID_PLANT);
		if($this->m_plant->error()){
			$this->notice->error($this->m_plant->error());
			redirect("plant/edit/".$ID_PLANT);
		}
		else{
			$this->notice->success("Plant Data Updated.");
			redirect("plant/by_company/".$this->input->post("ID_COMPANY"));
		}
	}
	
	public function delete($ID_PLANT,$ID_COMPANY=NULL){
		$this->m_plant->delete($ID_PLANT);
		if($this->m_plant->error()){
			$this->notice->error($this->m_plant->error());
		}
		else{
			$this->notice->success("Plant Data Removed.");
		}
		redirect("plant/by_company/".$ID_COMPANY);
	}
	
	
	/* ajax */
	/* ajax */
	public function json_plant_list($ID_COMPANY){
		$data = $this->list_plant = $this->m_plant->datalist($ID_COMPANY);
		echo json_encode($data);
	}

	public function get_list(){
		$list = $this->m_plant->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_PLANT;
			$row[] = $no;
			$row[] = $column->NM_COMPANY;
			$row[] = $column->KD_PLANT;
			$row[] = $column->NM_PLANT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_plant->count_all(),
            "recordsFiltered" => $this->m_plant->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
	
	public function getPlant($ID_COMPANY=NULL){
		$list_plant = array();
		if($ID_COMPANY){
			$list_plant = $this->m_plant->datalist($ID_COMPANY);
		}
		$list_plant[] = array('ID_PLANT' => '', 'NM_PLANT' => 'Choose Plant...');
		sort($list_plant);
		echo json_encode($list_plant);
	}

}	



?>
