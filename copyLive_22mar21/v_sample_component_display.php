   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sample Component 
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">
		
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Display Configuration</h3>
              </div>

              <div class="box-body">
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
                <div class="row" style="margin: 20px;">
                  <div class="form-group col-sm-3 col-sm-3 " id="checkbox1">
                  </div>
                  <div class="form-group col-sm-3 col-sm-3 " id="checkbox2">
                  </div>
                  <div class="form-group col-sm-3 col-sm-3 " id="checkbox3">
                  </div>
                  <div class="form-group col-sm-3 col-sm-3 " id="checkbox4">
                  </div>

                  </div>
              </div>

              <div class="box-footer">
                <div class="input-group input-group-sm" style="width: 150px; float: right;">
                  <a id="savecomponent" style="display: none;" type="button" class="btn btn-block btn-success btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>"><i class="fa fa-save"></i> Save Changes</a>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>

$(document).ready(function(){

  $("#saving").css('display','');

  loadchecklist();

  $(document).on('click',"#AddNew",function () {
    
    $("#saving").css('display','');
    $('#formArea').show();
    $('#viewArea').hide();

    $('#UpdateArea').hide();
    $('#AddArea').show();

    $('#ID_SAMPLE_AREA').val('');
    $('#NAME_SAMPLE_AREA').val('');
    
    $("#saving").css('display','none');
  });

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });

  $("#savecomponent").click(function(event){
    $("#saving").css('display','');
    $('#savecomponent').button('loading');
      var arrcomponentchecked = new Array();
      var arrcomponentunchecked = new Array();
      var componentceklist = document.getElementsByName("listcomponent[]");
      for (var i=0; i < componentceklist.length; i++) {
          if (componentceklist[i].checked == true) {
              arrcomponentchecked.push(componentceklist[i].value);
          } else {
              arrcomponentunchecked.push(componentceklist[i].value);
          }
      }

      $.ajax({
        url: "<?php echo site_url("input_sample_quality/save_checklist");?>",
        data: {"datacheck": arrcomponentchecked, "datauncheck": arrcomponentunchecked, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {

          if (res.msg == 'success') {
            $("#a-notice-success").click();
          }
          else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }

          loadchecklist();

          $('#savecomponent').button('reset');

        },
        error: function () {
          $("#a-notice-error").click();
          $('#savecomponent').button('reset');
        }
      });

    });

  function loadchecklist() {
      var checkboxlist1 = '';
      var checkboxlist2 = '';
      var checkboxlist3 = '';
      var checkboxlist4 = '';
      var checkboxlist5 = '';
      var checkboxlist6 = '';
      $('#checkbox1').html(checkboxlist1);
              $('#checkbox2').html(checkboxlist2);
                      $('#checkbox3').html(checkboxlist3);
                              $('#checkbox4').html(checkboxlist4);
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_component_checklist");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {

            var ischeck = '';

            if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
              ischeck = 'checked';
            }

            if (i % 4 == 0 || i == 0) {
              checkboxlist1 += '<div class="checkbox"><label><input type="checkbox" name="listcomponent[]" class="listcomponent" value="'+values[i]['ID_COMPONENT']+'" '+ischeck+'><b>'+values[i]['KD_COMPONENT']+'</b> ( <i>'+ values[i]['NM_COMPONENT'] +'</i> )</label></div>';
            } else if (i % 4 == 1) {
              checkboxlist2 += '<div class="checkbox"><label><input type="checkbox" name="listcomponent[]" class="listcomponent" value="'+values[i]['ID_COMPONENT']+'" '+ischeck+'><b>'+values[i]['KD_COMPONENT']+'</b> ( <i>'+ values[i]['NM_COMPONENT'] +'</i> )</label></div>';
            } else if (i % 4 == 2) {
              checkboxlist3 += '<div class="checkbox"><label><input type="checkbox" name="listcomponent[]" class="listcomponent" value="'+values[i]['ID_COMPONENT']+'" '+ischeck+'><b>'+values[i]['KD_COMPONENT']+'</b> ( <i>'+ values[i]['NM_COMPONENT'] +'</i> )</label></div>';
            } else if (i % 4 == 3) {
              checkboxlist4 += '<div class="checkbox"><label><input type="checkbox" name="listcomponent[]" class="listcomponent" value="'+values[i]['ID_COMPONENT']+'" '+ischeck+'><b>'+values[i]['KD_COMPONENT']+'</b> ( <i>'+ values[i]['NM_COMPONENT'] +'</i> )</label></div>';
            }

          }
        }

        $('#checkbox1').html(checkboxlist1);
                $('#checkbox2').html(checkboxlist2);
                        $('#checkbox3').html(checkboxlist3);
                                $('#checkbox4').html(checkboxlist4);

                                                $("#saving").css('display','none');
                                                $('#savecomponent').show();
      });


    }

	
});
</script>
