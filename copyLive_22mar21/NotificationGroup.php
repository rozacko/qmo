<?php

class NotificationGroup extends DB_QM {
    protected $table = 'M_OPCO_NOTIFICATION_GROUP';
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('TelegramNotification');
    }
    
    public function sendMessage($chat_id, $message, $options = []) {
        return $this->TelegramNotification->sendMessage($chat_id, $message, $options);
    }

    public function send_message_to_group($opco_group, $message, $options = []) {
        // $opco_group = $this->find_one_group($id_area, $id_jabatan);
        $result = null;
        
        try {
            $this->can_receive_telegram_notif($opco_group);

            $result = $this->TelegramNotification->sendMessage(
                $opco_group->TELEGRAM_CHAT_ID,
                $message,
                $options
            );

            if ( $result->status === false ) {
                throw new Exception($result->result);
            }

            $status = true;
        } catch (Exception $e) {
            $this->TelegramNotification::log($e->getMessage());

            $result = $e->getMessage();
            $status = false;
        }

        return [
            'data'      => $opco_group,
            'status'    => $status,
            'message'   => $result,
        ];
    }

    public function find_by_id($id_notification_group) {
        $res = $this->db->from($this->table)
            ->where('ID_OPCO_NOTIFICATION_GROUP', $id_notification_group)
            ->get();
        
        return $res ? $res->first_row() : false;
    }

    public function find_one_by_area_jabatan($id_area, $id_jabatan) {
        $res = $this->db->from($this->table)
            ->where('ID_AREA', $id_area)
            // ->where('ID_JABATAN', $id_jabatan)
            ->get();
        
        return $res ? $res->first_row() : false;
    }

    public function can_receive_telegram_notif($group) {
        if ( ! $group ) {
            throw new Exception('Invalid OPCO group data');
        }

        if ( (bool) $group->TELEGRAM_NOTIFICATION_ENABLE !== TRUE ) {
            throw new Exception(
                sprintf('Group OPCO area %s, jabatan %s belum diaktifkan '.
                    'notifikasi telegram',
                    $group->ID_AREA,
                    $group->ID_JABATAN
                )
            );
        }

        if ( empty($group->TELEGRAM_CHAT_ID) ) {
            throw new Exception(
                sprintf('Group %s belum memiliki ID Chat', $group->ID_OPCO_NOTIFICATION_GROUP)
            );
        }

        return true;
    }

    public function insert($data = []) {
        $this->db->set($data);
        $this->db->set("ID_OPCO_NOTIFICATION_GROUP","SEQ_ID_OPCO_NOTIFICATION_GROUP.NEXTVAL",FALSE);
        return $this->db->insert($this->table);
    }

    public function update($data, $id_notification_group) {
        return $this->db->set($data)
            ->where('ID_OPCO_NOTIFICATION_GROUP', $id_notification_group)
            ->update($this->table);

    }

    public function get_data_by_id($ID_OPCO_NOTIFICATION_GROUP) {
        $this->db->select("a.*,b.ID_PLANT,c.ID_COMPANY,d.ID_GROUPAREA");
        $this->db->from($this->table . ' a');
        $this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
        $this->db->join("M_PLANT c","b.ID_PLANT=c.ID_PLANT");
        $this->db->join("M_GROUPAREA d","b.ID_GROUPAREA=d.ID_GROUPAREA");
        $this->db->where('ID_OPCO_NOTIFICATION_GROUP', $ID_OPCO_NOTIFICATION_GROUP);
        return $this->db->get()->first_row();
    }

    public function datalist() {
        $this->db->select("a.*,b.NM_AREA,c.NM_PLANT,d.NM_COMPANY");
        $this->db->from($this->table . ' a') ;
        $this->db->join("M_AREA b","a.ID_AREA=b.ID_AREA");
        $this->db->join("M_PLANT c","b.ID_PLANT=c.ID_PLANT");
        $this->db->join("M_COMPANY d","c.ID_COMPANY=d.ID_COMPANY");
        // $this->db->join("M_JABATAN e","a.ID_JABATAN=e.ID_JABATAN");
        // $this->db->where("a.DELETED","0");
        // $this->db->order_by("a.ID_OPCO");
        return $this->db->get();

        return $this->db->from($this->table)
            ->join()
            ->get();
    }

    public function check_duplicate($id_area, $ID_OPCO_NOTIFICATION_GROUP = null) {
        $this->db->select("count(*) as COUNT")
            ->from($this->table)
            ->where('ID_AREA', $id_area);

        if ($ID_OPCO_NOTIFICATION_GROUP) {
            $this->db->where('ID_OPCO_NOTIFICATION_GROUP !=', $ID_OPCO_NOTIFICATION_GROUP);
        }
        
        $count = $this->db->get()->first_row()->COUNT;

        return $count > 0;
    }

    public function delete($ID_OPCO_NOTIFICATION_GROUP) {
        // TODO use softdelete
        return $this->db->from($this->table)
            ->where('ID_OPCO_NOTIFICATION_GROUP', $ID_OPCO_NOTIFICATION_GROUP)
            ->delete();

    }
}
