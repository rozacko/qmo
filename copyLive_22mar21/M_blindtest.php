<?php

class M_blindtest Extends DB_QM {

	private $post  = array();
	private $table = "O_THIRD_MATERIAL";
	private $table_blindtest = "QMLAB_BLINDTEST";
	private $table_setup = "QMLAB_SETUP";
	private $table_periode = "QMLAB_PERIODE";
	private $pKey  = "ID_MATERIAL";
	private $column_order = array(NULL, 'URUTAN'); //set column for datatable order 
    private $column_search = array('NAME_COMPETITOR', 'NAME_PRODUCT'); //set column for datatable search 
    private $order = array("A.ID" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
        $this->scmdb = $this->load->database('scm', TRUE);
        // $this->db = $this->load->database('mso_prod', TRUE);
	}

	var $column = array(
		'A.ID', 'B.NAMA', 'D.NAMA_LAB', 'B.TAHUN', 'A.SETUP_START', 'A.SETUP_END', 'POBS.NAMA_PIC_OBS', 'PIC.NAMA_PIC_PLANT', 'C.TANGGAL_KIRIM', 'A.ID'
	);

	var $column_receipt = array(
		'A.ID', 'B.NAMA', 'D.NAMA_LAB', 'B.TAHUN', 'A.SETUP_START', 'A.SETUP_END', 'POBS.NAMA_PIC_OBS', 'PIC.NAMA_PIC_PLANT', 'C.TANGGAL_KIRIM', 'C.TANGGAL_TERIMA', 'A.ID'
	);

	var $column_evaluasi = array(
		'A.ID', 'B.NAMA', 'D.NAMA_LAB', 'B.TAHUN', 'A.SETUP_START', 'A.SETUP_END', 'POBS.NAMA_PIC_OBS', 'PIC.NAMA_PIC_PLANT', 'C.TANGGAL_KIRIM', 'C.TANGGAL_TERIMA', 'A.ID'
	);

	var $column_tindaklanjut = array(
		'A.ID', 'B.NAMA', 'D.NAMA_LAB', 'B.TAHUN', 'A.SETUP_START', 'A.SETUP_END', 'POBS.NAMA_PIC_OBS', 'PIC.NAMA_PIC_PLANT', 'C.TANGGAL_KIRIM', 'C.TANGGAL_TERIMA', 'A.ID'
	);

	var $column_standardevaluasi = array(
		'A.ID', 'A.NAMA_STANDARD', 'A.KODE_STANDARD', 'A.CREATE_DATE', 'A.ID'
	);

	public function list_component($ID_COMPONENT=null){
		$this->db->where("LOWER(a.KD_COMPONENT) !=","h2o");
		$this->db->or_where("LOWER(a.KD_COMPONENT) !=","h2o");
		if($ID_COMPONENT) $this->db->where("a.ID_COMPONENT", $ID_COMPONENT);
		return $this->db->get("M_COMPONENT a")->result();
	}

	public function list_stdevaluasi($ID_COMPONENT=null){
		if($ID_COMPONENT) $this->db->where("a.ID", $ID_COMPONENT);
		return $this->db->get("QMLAB_BLINDSTD a")->result();
	}

	public function stdsample_submit($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("QMLAB_BLINDSTD");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function ttl_submit($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("ID",$data['ID']);
		unset($data['ID']);
		$this->db->set($data);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_BLINDEVALUASI");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function stdsample_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("ID",$data['ID']);
		unset($data['ID']);
		$this->db->set($data);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_BLINDSTD");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function update_status($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("ID",$data['ID']);
		unset($data['ID']);
		$this->db->set($data);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_SETUP");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_send($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("TGL_KIRIM", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("QMLAB_BLINDTEST");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_evaluate($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("ID",$data['ID']);
		unset($data['ID']);
		$this->db->set($data);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_BLINDTEST");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_receipt($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("FK_ID_PERIODE",$data['FK_ID_PERIODE']);
		unset($data['FK_ID_PERIODE']);
		$this->db->set($data);
		$this->db->set("TGL_TERIMA", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_BLINDTEST");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_submit($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->where("FK_ID_PERIODE",$data['FK_ID_PERIODE']);
		unset($data['FK_ID_PERIODE']);
		$this->db->set($data);
		$this->db->set("UPDATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->update("QMLAB_BLINDTEST");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	function _qry($key){
		$this->db->select("A.*, TO_CHAR(A.START_DATE, 'YYYY-MM-DD') AS SETUP_START, TO_CHAR(A.END_DATE, 'YYYY-MM-DD') AS SETUP_END, B.NAMA, B.TAHUN, TO_CHAR(B.START_DATE, 'YYYY-MM-DD') AS PERIODE_START, TO_CHAR(B.END_DATE, 'YYYY-MM-DD') AS PERIODE_END, B.STATUS AS STATUS_PELAKSANAAN, C.PENGIRIM, TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') AS TANGGAL_KIRIM, C.PENERIMA, TO_CHAR(C.TGL_TERIMA, 'YYYY-MM-DD') AS TANGGAL_TERIMA, C.CATATAN_KIRIM, C.CATATAN_TERIMA, POBS.FULLNAME AS NAMA_PIC_OBS, PIC.FULLNAME AS NAMA_PIC_PLANT, D.NAMA_LAB");
		$this->db->from('QMLAB_SETUP A');
		$this->db->join('QMLAB_PERIODE B', 'A.FK_ID_PERIODE = B.ID');
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$this->db->join('M_LABORATORIUM D', 'A.LAB = D.ID_LAB');
		$this->db->join('M_USERS POBS', 'A.PIC_OBSERVASI = POBS.ID_USER');
		$this->db->join('M_USERS PIC', 'A.PIC_PLANT = PIC.ID_USER');
		if($key['search']!==''){
			$this->db->where("(LOWER(B.NAMA) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(D.NAMA_LAB) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR B.TAHUN LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.START_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.END_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(POBS.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(PIC.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!')");
		}
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('B.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		// if ($key['PIC_OBSERVASI'] || $key['PIC_PLANT']) {
		// 	# code...
		// 	$this->db->where("(A.PIC_OBSERVASI = ".$key['PIC_OBSERVASI']." OR A.PIC_PLANT = ".$key['PIC_PLANT']." )");
		// }
		$order = $this->column[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function get_data($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil($key){
		$this->_qry($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot(){
		$this->db->where('DELETE_DATE IS NULL');
		$this->db->where('JENIS', "BLIND TEST");
		$query			= $this->db->get('QMLAB_SETUP');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function _qry_receipt($key){
		$this->db->select("A.*, TO_CHAR(A.START_DATE, 'YYYY-MM-DD') AS SETUP_START, TO_CHAR(A.END_DATE, 'YYYY-MM-DD') AS SETUP_END, B.NAMA, B.TAHUN, TO_CHAR(B.START_DATE, 'YYYY-MM-DD') AS PERIODE_START, TO_CHAR(B.END_DATE, 'YYYY-MM-DD') AS PERIODE_END, B.STATUS AS STATUS_PELAKSANAAN, C.PENGIRIM, TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') AS TANGGAL_KIRIM, C.PENERIMA, TO_CHAR(C.TGL_TERIMA, 'YYYY-MM-DD') AS TANGGAL_TERIMA, C.CATATAN_KIRIM, C.CATATAN_TERIMA, POBS.FULLNAME AS NAMA_PIC_OBS, PIC.FULLNAME AS NAMA_PIC_PLANT, D.NAMA_LAB, C.STATUS_ENTRY");
		$this->db->from('QMLAB_SETUP A');
		$this->db->join('QMLAB_PERIODE B', 'A.FK_ID_PERIODE = B.ID');
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$this->db->join('M_LABORATORIUM D', 'A.LAB = D.ID_LAB');
		$this->db->join('M_USERS POBS', 'A.PIC_OBSERVASI = POBS.ID_USER');
		$this->db->join('M_USERS PIC', 'A.PIC_PLANT = PIC.ID_USER');
		if($key['search']!==''){
			$this->db->where("(LOWER(B.NAMA) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(D.NAMA_LAB) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR B.TAHUN LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.START_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.END_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(POBS.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(PIC.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!')");
		}
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('B.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		$this->db->where('C.TGL_KIRIM IS NOT NULL');
		// if ($key['PIC_OBSERVASI'] || $key['PIC_PLANT']) {
		// 	# code...
		// 	$this->db->where("(A.PIC_OBSERVASI = ".$key['PIC_OBSERVASI']." OR A.PIC_PLANT = ".$key['PIC_PLANT']." )");
		// }
		$order = $this->column_receipt[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function get_receipt($key){
		$this->_qry_receipt($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil_receipt($key){
		$this->_qry_receipt($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_receipt(){
		$this->db->where('DELETE_DATE IS NULL');
		$this->db->where('JENIS', "BLIND TEST");
		$query			= $this->db->get('QMLAB_SETUP');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function _qry_evaluasi($key){
		$this->db->select("A.*, TO_CHAR(A.START_DATE, 'YYYY-MM-DD') AS SETUP_START, TO_CHAR(A.END_DATE, 'YYYY-MM-DD') AS SETUP_END, B.NAMA, B.TAHUN, TO_CHAR(B.START_DATE, 'YYYY-MM-DD') AS PERIODE_START, TO_CHAR(B.END_DATE, 'YYYY-MM-DD') AS PERIODE_END, B.STATUS AS STATUS_PELAKSANAAN, C.PENGIRIM, TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') AS TANGGAL_KIRIM, C.PENERIMA, TO_CHAR(C.TGL_TERIMA, 'YYYY-MM-DD') AS TANGGAL_TERIMA, C.CATATAN_KIRIM, C.CATATAN_TERIMA, POBS.FULLNAME AS NAMA_PIC_OBS, PIC.FULLNAME AS NAMA_PIC_PLANT, D.NAMA_LAB, C.STATUS_ENTRY, C.ID AS ID_BLINDTEST, E.NAMA_STANDARD, E.KODE_STANDARD, C.STATUS_EVALUASI");
		$this->db->from('QMLAB_SETUP A');
		$this->db->join('QMLAB_PERIODE B', 'A.FK_ID_PERIODE = B.ID');
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$this->db->join('QMLAB_BLINDSTD E', 'A.FK_ID_STD = E.ID', 'LEFT');
		$this->db->join('M_LABORATORIUM D', 'A.LAB = D.ID_LAB');
		$this->db->join('M_USERS POBS', 'A.PIC_OBSERVASI = POBS.ID_USER');
		$this->db->join('M_USERS PIC', 'A.PIC_PLANT = PIC.ID_USER');
		if($key['search']!==''){
			$this->db->where("(LOWER(B.NAMA) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(D.NAMA_LAB) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR B.TAHUN LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.START_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.END_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(POBS.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(PIC.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!')");
		}
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('B.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		$this->db->where('C.TGL_KIRIM IS NOT NULL');
		$this->db->where('C.TGL_TERIMA IS NOT NULL');
		$this->db->where('C.STATUS_ENTRY', 2);
		// if ($key['PIC_OBSERVASI'] || $key['PIC_PLANT']) {
		// 	# code...
		// 	$this->db->where("(A.PIC_OBSERVASI = ".$key['PIC_OBSERVASI']." OR A.PIC_PLANT = ".$key['PIC_PLANT']." )");
		// }
		$order = $this->column_evaluasi[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function get_evaluasi($key){
		$this->_qry_evaluasi($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil_evaluasi($key){
		$this->_qry_evaluasi($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_evaluasi(){
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		$this->db->where('C.TGL_KIRIM IS NOT NULL');
		$this->db->where('C.TGL_TERIMA IS NOT NULL');
		$this->db->where('C.STATUS_ENTRY', 2);
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$query			= $this->db->get('QMLAB_SETUP A');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function _qry_tindaklanjut($key){
		$this->db->select("A.*, TO_CHAR(A.START_DATE, 'YYYY-MM-DD') AS SETUP_START, TO_CHAR(A.END_DATE, 'YYYY-MM-DD') AS SETUP_END, B.NAMA, B.TAHUN, TO_CHAR(B.START_DATE, 'YYYY-MM-DD') AS PERIODE_START, TO_CHAR(B.END_DATE, 'YYYY-MM-DD') AS PERIODE_END, B.STATUS AS STATUS_PELAKSANAAN, C.PENGIRIM, TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') AS TANGGAL_KIRIM, C.PENERIMA, TO_CHAR(C.TGL_TERIMA, 'YYYY-MM-DD') AS TANGGAL_TERIMA, C.CATATAN_KIRIM, C.CATATAN_TERIMA, POBS.FULLNAME AS NAMA_PIC_OBS, PIC.FULLNAME AS NAMA_PIC_PLANT, D.NAMA_LAB, C.STATUS_ENTRY, C.ID AS ID_BLINDTEST, E.NAMA_STANDARD, E.KODE_STANDARD, C.STATUS_EVALUASI");
		$this->db->from('QMLAB_SETUP A');
		$this->db->join('QMLAB_PERIODE B', 'A.FK_ID_PERIODE = B.ID');
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$this->db->join('QMLAB_BLINDSTD E', 'A.FK_ID_STD = E.ID', 'LEFT');
		$this->db->join('M_LABORATORIUM D', 'A.LAB = D.ID_LAB');
		$this->db->join('M_USERS POBS', 'A.PIC_OBSERVASI = POBS.ID_USER');
		$this->db->join('M_USERS PIC', 'A.PIC_PLANT = PIC.ID_USER');
		if($key['search']!==''){
			$this->db->where("(LOWER(B.NAMA) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(D.NAMA_LAB) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR B.TAHUN LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.START_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(A.END_DATE, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(POBS.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(PIC.FULLNAME) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR TO_CHAR(C.TGL_KIRIM, 'YYYY-MM-DD') LIKE '%".strtolower($key['search'])."%' ESCAPE '!')");
		}
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('B.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		$this->db->where('C.TGL_KIRIM IS NOT NULL');
		$this->db->where('C.TGL_TERIMA IS NOT NULL');
		$this->db->where('C.STATUS_ENTRY', 2);
		$this->db->where('C.STATUS_EVALUASI', 1);
		// if ($key['PIC_OBSERVASI'] || $key['PIC_PLANT']) {
		// 	# code...
		// 	$this->db->where("(A.PIC_OBSERVASI = ".$key['PIC_OBSERVASI']." OR A.PIC_PLANT = ".$key['PIC_PLANT']." )");
		// }
		$order = $this->column_tindaklanjut[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function get_tindaklanjut($key){
		$this->_qry_tindaklanjut($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil_tindaklanjut($key){
		$this->_qry_tindaklanjut($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_tindaklanjut(){
		$this->db->where('A.DELETE_DATE IS NULL');
		$this->db->where('A.JENIS', "BLIND TEST");
		$this->db->where('C.TGL_KIRIM IS NOT NULL');
		$this->db->where('C.TGL_TERIMA IS NOT NULL');
		$this->db->where('C.STATUS_ENTRY', 2);
		$this->db->join('QMLAB_BLINDTEST C', 'A.ID = C.FK_ID_PERIODE', 'LEFT');
		$query			= $this->db->get('QMLAB_SETUP A');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function _qry_standardevaluasi($key){
		$this->db->select("A.*, TO_CHAR(A.CREATE_DATE, 'YYYY-MM-DD') AS TANGGAL_BUAT, TO_CHAR(A.UPDATE_DATE, 'YYYY-MM-DD') AS TANGGAL_UPDATE");
		$this->db->from('QMLAB_BLINDSTD A');
		if($key['search']!==''){
			$this->db->where("(LOWER(A.NAMA_STANDARD) LIKE '%".strtolower($key['search'])."%' ESCAPE '!' OR LOWER(A.KODE_STANDARD) LIKE '%".strtolower($key['search'])."%' ESCAPE '!')");
		}
		$this->db->where('A.DELETE_DATE IS NULL');
		$order = $this->column_standardevaluasi[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function get_standardevaluasi($key){
		$this->_qry_standardevaluasi($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil_standardevaluasi($key){
		$this->_qry_standardevaluasi($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_standardevaluasi(){
		$this->db->where('A.DELETE_DATE IS NULL');
		$query			= $this->db->get('QMLAB_BLINDSTD A');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

	public function type_productlist($all){

		if (!$all) {
			# code...			
			$this->db->where("LOWER(a.KD_PRODUCT)", 'ppc');
			$this->db->or_where("LOWER(a.KD_PRODUCT)", 'opc');
			$this->db->or_where("LOWER(a.KD_PRODUCT)", 'pcc');
		}

		$this->db->order_by("a.ID_PRODUCT");
		return $this->db->get("M_PRODUCT a")->result_array();
	}

	public function component_checklist_order(){
		$this->db->select("A.ID_COMPONENT,	A.KD_COMPONENT,	A.NM_COMPONENT,	COALESCE (B.IS_ACTIVE, 0) AS STATUS_CHECKLIST,	C.URUTAN");
		$this->db->join("O_CHECKLIST B","A.ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("C_PARAMETER_ORDER C","A.ID_COMPONENT = C.ID_COMPONENT", 'left');
		$this->db->where("C.ID_GROUPAREA", 1);
		$this->db->where("C.DISPLAY", 'D');
		$this->db->where("B.IS_ACTIVE", 1);
  		$this->db->order_by("C.URUTAN,	A.ID_COMPONENT");
		return $this->db->get("M_COMPONENT A")->result_array();
	}

	public function componentlist(){
		$this->db->order_by("a.ID_COMPONENT");
		return $this->db->get("M_COMPONENT a")->result();
	}

	public function componentmaster(){
		$this->db->order_by("a.ID_COMPONENT");
		return $this->db->get("M_COMPONENT a")->result_array();
	}

	public function input_stdblindtest($post) {
		# code...
		$qry = "
                MERGE INTO QMLAB_BLINDSTDDETAIL USING dual ON (
                   	FK_ID_STD = ". $post['FK_ID_STD'] ." AND
                    FK_ID_COMPONENT = ". $post['FK_ID_COMPONENT'] ."
                )
                WHEN MATCHED THEN UPDATE SET
                    DATA = ". $post['DATA'] .",
                    SD = ". $post['SD'] .",
                    UPDATE_BY = '". $post['USER'] ."',
                    UPDATE_DATE = SYSDATE
                WHEN NOT MATCHED THEN
                INSERT (
                    FK_ID_STD,
                    FK_ID_COMPONENT,
                    DATA,
                    SD,
                    CREATE_BY,
                    CREATE_DATE
                )
                VALUES (
                    ". $post['FK_ID_STD'] .",
                    ". $post['FK_ID_COMPONENT'] .",
                    ". $post['DATA'] .",
                    ". $post['SD'] .",
                    '". $post['USER'] ."',
                    SYSDATE
                    )
                ";
        // echo "$qry";
        // exit();
		return $this->db->query($qry);
	}

	public function input_formblindtest($post) {
		# code...
		$qry = "
                MERGE INTO QMLAB_BLINDSETTING USING dual ON (
                   	FK_ID_COMPONENT = ". $post['FK_ID_COMPONENT'] ."
                )
                WHEN MATCHED THEN UPDATE SET
                    ORDER_COMP = ". $post['ORDER_COMP'] .",
                    NM_GROUP = '". $post['GROUP'] ."',
                    SATUAN = '". $post['SATUAN'] ."',
                    UPDATE_BY = '". $post['USER'] ."',
                    UPDATE_DATE = SYSDATE
                WHEN NOT MATCHED THEN
                INSERT (
                    FK_ID_COMPONENT,
                    ORDER_COMP,
                    NM_GROUP,
                    SATUAN,
                    CREATE_BY,
                    CREATE_DATE
                )
                VALUES (
                    ". $post['FK_ID_COMPONENT'] .",
                    ". $post['ORDER_COMP'] .",
                    '". $post['GROUP'] ."',
                    '". $post['SATUAN'] ."',
                    '". $post['USER'] ."',
                    SYSDATE
                    )
                ";
        // echo "$qry";
        // exit();
		return $this->db->query($qry);
	}

	public function input_blindtest($post) {
		# code...
		$qry = "
                MERGE INTO QMLAB_BLINDINPUT USING dual ON (
                   	FK_ID_SETUP = ". $post['FK_ID_SETUP'] ." AND
                    FK_ID_COMPONENT = ". $post['FK_ID_COMPONENT'] ."
                )
                WHEN MATCHED THEN UPDATE SET
                    TESTING_1 = ". $post['TESTING_1'] .",
                    TESTING_2 = ". $post['TESTING_2'] .",
                    AVR = ". $post['AVR'] .",
                    UPDATE_BY = '". $post['USER'] ."',
                    UPDATE_DATE = SYSDATE
                WHEN NOT MATCHED THEN
                INSERT (
                    FK_ID_SETUP,
                    FK_ID_COMPONENT,
                    TESTING_1,
                    TESTING_2,
                    AVR,
                    CREATE_BY,
                    CREATE_DATE
                )
                VALUES (
                    ". $post['FK_ID_SETUP'] .",
                    ". $post['FK_ID_COMPONENT'] .",
                    ". $post['TESTING_1'] .",
                    ". $post['TESTING_2'] .",
                    ". $post['AVR'] .",
                    '". $post['USER'] ."',
                    SYSDATE
                    )
                ";
        // echo "$qry";
        // exit();
		return $this->db->query($qry);
	}

	public function evaluate_blindtest($post) {
		# code...
		$qry = "
                MERGE INTO QMLAB_BLINDEVALUASI USING dual ON (
                   	FK_ID_BLIND = ". $post['FK_ID_BLIND'] ." AND
                    FK_ID_COMPONENT = ". $post['FK_ID_COMPONENT'] ."
                )
                WHEN MATCHED THEN UPDATE SET
                    N_EVALUASI = ". $post['N_EVALUASI'] .",
                    SCORE_EVALUASI = ". $post['SCORE_EVALUASI'] .",
                    UPDATE_BY = '". $post['USER'] ."',
                    UPDATE_DATE = SYSDATE
                WHEN NOT MATCHED THEN
                INSERT (
                    FK_ID_BLIND,
                    FK_ID_COMPONENT,
                    N_EVALUASI,
                    SCORE_EVALUASI,
                    CREATE_BY,
                    CREATE_DATE
                )
                VALUES (
                    ". $post['FK_ID_BLIND'] .",
                    ". $post['FK_ID_COMPONENT'] .",
                    ". $post['N_EVALUASI'] .",
                    ". $post['SCORE_EVALUASI'] .",
                    '". $post['CREATE_BY'] ."',
                    SYSDATE
                    )
                ";
        // echo "$qry";
        // exit();
		return $this->db->query($qry);
	}

	public function blind_delete($id){
		$this->db->where("ID", $id);
		$query = $this->db->delete("QMLAB_BLINDINPUT");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function formblind_delete($id){
		$this->db->where("ID", $id);
		$query = $this->db->delete("QMLAB_BLINDSETTING");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function stdblind_delete($id){
		$this->db->where("ID", $id);
		$query = $this->db->delete("QMLAB_BLINDSTD");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function stddetailblind_delete($id){
		$this->db->where("ID", $id);
		$query = $this->db->delete("QMLAB_BLINDSTDDETAIL");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sampleblindtest($idsetup){
		$this->db->select("A.*, B.NM_COMPONENT");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->where("A.FK_ID_SETUP", $idsetup);
		return $this->db->get("QMLAB_BLINDINPUT A")->result_array();
	}

	public function formblindtest(){
		$this->db->select("A.*, B.NM_COMPONENT");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		return $this->db->get("QMLAB_BLINDSETTING A")->result_array();
	}

	public function evaluasisampleblindtest($idsetup){
		$this->db->select("F.ID AS ID_BLINDTEST, A .*, B.NM_COMPONENT, C.KODE_STANDARD, C.NAMA_STANDARD, D.DATA, D.SD, E.N_EVALUASI, M.TESTING_1, M.TESTING_2");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');

		$this->db->join("(SELECT * FROM QMLAB_BLINDINPUT WHERE FK_ID_SETUP = '".$idsetup."') M","M.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');

		$this->db->join("QMLAB_BLINDTEST F","M.FK_ID_SETUP = F.FK_ID_PERIODE", 'left');
		$this->db->join("QMLAB_SETUP S","M.FK_ID_SETUP = S.ID", 'left');
		$this->db->join('QMLAB_PERIODE M', 'S.FK_ID_PERIODE = M.ID');
		$this->db->join("QMLAB_BLINDSTD C","S.FK_ID_STD = C.ID", 'left');
		$this->db->join("QMLAB_BLINDSTDDETAIL D","D.FK_ID_STD = C.ID AND D.FK_ID_COMPONENT = M.FK_ID_COMPONENT", 'left');
		$this->db->join("QMLAB_BLINDEVALUASI E","M.FK_ID_COMPONENT = E.FK_ID_COMPONENT AND F.ID = E.FK_ID_BLIND", 'left');
		// $this->db->where("A.FK_ID_SETUP", $idsetup);
		$this->db->where('S.DELETE_DATE IS NULL');
		$this->db->where('M.DELETE_DATE IS NULL');
		$this->db->order_by("A.ORDER_COMP", "ASC");
		return $this->db->get("QMLAB_BLINDSETTING A")->result_array();
	}

	public function valuetindaklanjutblindtest($idsetup){
		$this->db->select("A .*, B.NM_COMPONENT, E.ID AS ID_EVALUASI, E.N_EVALUASI, E.I_MAN, E.I_METHODE, E.I_MACHINE, E.I_ENVIRONMENT, E.P_MAN, E.P_METHODE, E.P_MACHINE, E.P_ENVIRONMENT, E.VERIFIKASI, E.SCORE_EVALUASI, ST.ID AS ID_BLINDTEST");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("QMLAB_BLINDEVALUASI E","A.FK_ID_COMPONENT = E.FK_ID_COMPONENT", 'left');
		$this->db->join("QMLAB_BLINDTEST BT","E.FK_ID_BLIND = BT.ID", 'left');
		$this->db->join("QMLAB_SETUP ST","BT.FK_ID_PERIODE = ST.ID", 'left');
		$this->db->where("E.ID", $idsetup);
		$this->db->order_by("A.ORDER_COMP", "ASC");
		return $this->db->get("QMLAB_BLINDSETTING A")->row_array();
	}

	public function tindaklanjutblindtest($idsetup){
		$this->db->select("F.FK_ID_PERIODE AS ID_BLINDTEST, A .*, B.NM_COMPONENT, C.KODE_STANDARD, C.NAMA_STANDARD, D.DATA, D.SD, E.ID AS ID_EVALUASI, M.TESTING_1, M.TESTING_2, E.N_EVALUASI, E.I_MAN, E.I_METHODE, E.I_MACHINE, E.I_ENVIRONMENT, E.P_MAN, E.P_METHODE, E.P_MACHINE, E.P_ENVIRONMENT, E.VERIFIKASI, E.SCORE_EVALUASI");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("(SELECT * FROM QMLAB_BLINDINPUT WHERE FK_ID_SETUP = '".$idsetup."') M","M.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("QMLAB_BLINDTEST F","M.FK_ID_SETUP = F.FK_ID_PERIODE", 'left');
		$this->db->join("QMLAB_SETUP S","M.FK_ID_SETUP = S.ID", 'left');
		$this->db->join('QMLAB_PERIODE M', 'S.FK_ID_PERIODE = M.ID');
		$this->db->join("QMLAB_BLINDSTD C","S.FK_ID_STD = C.ID", 'left');
		$this->db->join("QMLAB_BLINDSTDDETAIL D","D.FK_ID_STD = C.ID AND D.FK_ID_COMPONENT = M.FK_ID_COMPONENT", 'left');
		$this->db->join("QMLAB_BLINDEVALUASI E","M.FK_ID_COMPONENT = E.FK_ID_COMPONENT AND F.ID = E.FK_ID_BLIND", 'left');
		// $this->db->where("A.FK_ID_SETUP", $idsetup);
		$this->db->where('S.DELETE_DATE IS NULL');
		$this->db->where('M.DELETE_DATE IS NULL');
		$this->db->order_by("A.ORDER_COMP", "ASC");
		return $this->db->get("QMLAB_BLINDSETTING A")->result_array();
	}


	public function stdsampleblindtest($idsetup){
		$this->db->select("A.*, B.NM_COMPONENT");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->where("A.FK_ID_STD", $idsetup);
		return $this->db->get("QMLAB_BLINDSTDDETAIL A")->result_array();
	}

	public function compstdsampleblindtest($idstd){
		$this->db->select("A.*, B.NM_COMPONENT, C.DATA, C.SD");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("(SELECT * FROM QMLAB_BLINDSTDDETAIL WHERE FK_ID_STD = '".$idstd."') C","C.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		// $this->db->join("QMLAB_BLINDSTDDETAIL C","C.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		// $this->db->where("C.FK_ID_STD", $idstd);
		$this->db->order_by("ORDER_COMP", "ASC");
		return $this->db->get("QMLAB_BLINDSETTING A")->result_array();
	}

	public function compdatasampleblindtest($idstd){
		$this->db->select("A.*, B.NM_COMPONENT, C.TESTING_1, C.TESTING_2");
		$this->db->join("M_COMPONENT B","A.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("(SELECT * FROM QMLAB_BLINDINPUT WHERE FK_ID_SETUP = '".$idstd."') C","C.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		// $this->db->join("QMLAB_BLINDSTDDETAIL C","C.FK_ID_COMPONENT = B.ID_COMPONENT", 'left');
		// $this->db->where("C.FK_ID_STD", $idstd);
		$this->db->order_by("A.ORDER_COMP", "ASC");
		return $this->db->get("QMLAB_BLINDSETTING A")->result_array();
	}

    public function get_pic($id){
        $this->db->select("QP.NAMA, QMLAB_SETUP.PIC_OBSERVASI, QMLAB_SETUP.PIC_PLANT, QMLAB_SETUP.JENIS, OB.FULLNAME AS OBSER_FULLNAME, OB.EMAIL AS OBSER_EMAIL, PL.FULLNAME AS PLANT_FULLNAME, PL.EMAIL AS PLANT_EMAIL");
        $this->db->from("QMLAB_SETUP");
        $this->db->join("QMLAB_PERIODE QP", "QMLAB_SETUP.FK_ID_PERIODE = QP.ID");
        $this->db->join("M_USERS OB", "QMLAB_SETUP.PIC_OBSERVASI = OB.ID_USER");
        $this->db->join("M_USERS PL", "QMLAB_SETUP.PIC_PLANT = PL.ID_USER");
        $this->db->where("QMLAB_SETUP.ID", $id);
        return $this->db->get()->row();
    }



}
