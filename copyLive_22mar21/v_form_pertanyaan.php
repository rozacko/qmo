<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>List Pertanyaan Pengujian Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="input-group input-group-sm" style="width: 150px; ">
						<button id="btn-new" type="button" class="btn btn-block btn-primary btn-sm">Create New</button>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-4">
							<select class="form-control select2" id="ID_FORM" name="ID_FORM">
							<?php  foreach($this->list_form as $form): ?>
							<option value="<?php echo $form->ID_FORM;?>"><?php echo $form->TITLE."-".$form->NAMA_SAMPLE;?></option>
							<?php endforeach; ?>
							</select>
						</div>
						<div class="col-md-2">
							<select class="form-control select2" id="TIPE_FORM" name="TIPE">
								<option value="">TIDAK ADA TIPE</option>
								<option value="KIMIA">KIMIA</option>
								<option value="FISIKA">FISIKA</option>
							</select>
						</div>
						<div class="col-md-2">
							<button id="view_data" class="btn btn-info">View Data</button>
						</div>
					</div>
					<br/>
					<div class="table-responsive">
						<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="3%">No</th>
									<th>Pertanyaan</th>
									<th>Tipe</th>
									<th>Opsi</th>
									<th width="10%"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-default fade" id="modal-form">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<form role="form" method="POST" >
						<div class="box-body" style="background-color:#c5d5ea;">
							<div class="form-group">
								<div class="col-sm-12 clearfix">
									<label>UJI PROFICIENCY</label>
									<select class="form-control select2" id="ID_FORM_UJI" name="ID_FORM">
										<?php  foreach($this->list_form as $form): ?>
										<option value="<?php echo $form->ID_FORM;?>"><?php echo $form->TITLE."-".$form->NAMA_SAMPLE;?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 clearfix">
									<label>TIPE</label>
									<select class="form-control select2" id="TIPE_UJI" name="TIPE">
										<option value="">TIDAK ADA TIPE</option>
										<option value="KIMIA">KIMIA</option>
										<option value="FISIKA">FISIKA</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-12 clearfix">
									<label>PERTANYAAN</label>
									<input type="hidden" class="form-control" id="ID_QUESTION" name="ID_QUESTION">
									<input type="text" class="form-control" id="QUEST_TEXT" name="QUEST_TEXT" placeholder="Text Pertanyaan" REQUIRED >
								</div>
							</div>
                            <div class="form-group">
								<div class="col-sm-12 clearfix">
									<label>PARAMETER (Keyword Component)</label>
									<input type="text" class="form-control" id="PARAMETER" name="PARAMETER" placeholder="Patameter" >
                                    <sub>Digunakan untuk menampilkan komponen pada evaluasi zscored.<br/>
                                    Jika komponen pertanyaan tidak ditampilkan pada evaluasi zscored maka bisa dikosongi<br/>
                                    ex: Pertanyaan : Pengujian SiO2, (0.01% terdekat), Uji ke-1. Maka Keyword Component Disikan : SiO2</sub>
								</div>
							</div>
							<div class="form-group">                  
								<div class="col-sm-12 clearfix">
									<label>TIPE PERTANYAAN</label>
									<select class="form-control" id="TIPE" name="QUEST_TYPE">
										<option value="TEXT">TEXT</option>
										<option value="OPTION">OPTION</option>
										<option value="FILE">FILE</option>
									</select>
								</div>                
							</div>
							<div class="form-group" style="display:none;" id="form-optional">                  
								<div class="col-sm-12 clearfix">
									<label>OPTIONAL VALUE</label>
									<textarea class="form-control" rows="3" name="QUEST_OPTIONAL" ID="QUEST_OPTIONAL"></textarea>
									<span>Opsi Pilihan, Jika lebih dari satu pilihan bisa menggunakan pemisah titik koma ( ; ) (ex: Wet Analysis;XRF;)</span>
								</div>                
							</div>
							<div class="form-group">                  
								<div class="col-sm-12 clearfix">
									<input type="checkbox" id="IS_REQUIRED" name="IS_REQUIRED" value="Y">
									<label for="IS_REQUIRED"> Is Required Form</label><br>
								</div>                
							</div>
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
            </div>
		</div>
	</div>
</section>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
	$(document).ready(function(){
		table_list();
	});

	$("#view_data").on("click", function(e){
		table_list();
	});

	function table_list(){
		var table = $('#dt_tables').DataTable({ 
            "processing" : true, 
            "serverSide" : true, 
            "destroy" : true,
            "autoWidth" : false,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('pertanyaan/get_list/');?>",
                "type": "POST",
				"data": {
					"id_form": $("#ID_FORM").val(),
					"tipe": $("#TIPE_FORM").val()
				}
            },
            "columns" : [
                {data: 'NO', name: 'NO', orderable: false, searchable: false, width:'3%'},
                {data: 'QUEST_TEXT', name: 'QUEST_TEXT'},
                {data: 'QUEST_TYPE', name: 'QUEST_TYPE'},
                {data: 'QUEST_OPTIONAL', name: 'QUEST_OPTIONAL'},
                {data: 'NO', name: 'NO', orderable: false, searchable: false, render: function(e, t, f){
                    return '<center>'+
                    '<button id="btn-edit" data-ID_QUESTION="'+f.ID_QUESTION+'" class="btn btn-xs btn-icon icon-left btn-warning"><i class="fa fa-edit"></i> Edit</button>&nbsp;'+
                    '<button id="btn-delete" data-ID_QUESTION="'+f.ID_QUESTION+'" class="btn btn-xs btn-icon icon-left btn-danger"><i class="fa fa-trash"></i> Delete</button>'+
                    '</center>';
                }},
            ],
        });
	}

    $("#btn-new").on("click", function(e){
		$('form')[0].reset();
		$("#ID_QUEST").val("");
		$(".modal-title").html("Add Pertanyaan Pengujian");
		$("#modal-form").modal("show");
	});

	$("#TIPE").on("change", function(e){
		var tipe = $(this).val();

		if(tipe == "OPTION"){
			$("#form-optional").css("display", "block");
		} else {
			$("#form-optional").css("display", "none");
		}
	});

	$("form").on("submit", function(e) {
		e.preventDefault();
		var id = $("#ID_QUESTION").val();
		var type = id == "" ? "Add" : "Edit";
		var post_url = id == "" ? "create" : "update/"+id;
		var request_method = $(this).attr("method");
		var form_data = $(this).serialize();
		
		$.ajax({
			url : '<?php echo site_url("pertanyaan") ?>'+'/'+post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
				alert(type+" Pertanyaan Form Uji Proficiency Successfully");
				$("#QUEST_TEXT").val("");
				$("#modal-form").modal("hide");
				$("#dt_tables").DataTable().ajax.reload();
			} else {
				alert(type+" Pertanyaan Form Uji Proficiency Failed")
			}
		});
	});

	$(document).on("click", "#btn-edit", function(e){
		var id = $(this).data("id_question");
		$.ajax({
			url : '<?php echo site_url("pertanyaan/detail") ?>/'+id,
		}).done(function(response){
			$("#ID_QUESTION").val(response['ID_QUESTION']);
			$("#ID_FORM_UJI").val(response['ID_FORM']);
			$("#TIPE_UJI").val(response['TIPE']);
            $("#QUEST_TEXT").val(response['QUEST_TEXT']);
            $("#PARAMETER").val(response['PARAMETER']);
			$("#TIPE").val(response['QUEST_TYPE']).change();
			if(response['QUEST_TYPE'] == "OPTION"){
				$("#form-optional").css("display", "block");
			} else {
				$("#form-optional").css("display", "none");
			}
			$("#QUEST_OPTIONAL").val(response['QUEST_OPTIONAL']);
			$(".modal-title").html("Edit Pertanyaan Pengujian");
			$("#modal-form").modal("show");
		});
	})

	$(document).on("click", "#btn-delete", function(e){
		var id = $(this).data("id_question");
		var c = confirm("Apakah yakin ingin menghapus ?");
		if(c){
			$.ajax({
				url : '<?php echo site_url("pertanyaan/delete") ?>'+'/'+id,
				type: "POST",
			}).done(function(response){
				if(response == 1){
					alert("Delete Form Uji Proficiency Successfully");
					$("#dt_tables").DataTable().ajax.reload();
				} else {
					alert("Delete Form Uji Proficiency Failed")
				}
			});
		}
	});
</script>