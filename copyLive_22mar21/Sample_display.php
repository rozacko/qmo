<?php

class Sample_display extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_sampleproduct");
	}
	
	public function index(){
		$this->list_component = $this->m_sampleproduct->datalist();
		$this->template->adminlte("v_sample_component_display");
	}

	public function save_product(){
		
		$result['msg'] = 'Cannot Insert New Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['NAME_PRODUCT'] = $post['NAME_PRODUCT'];
		$data['CREATE_BY'] = $post['user'];

		$isexsist_product = $this->m_sampleproduct->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_sampleproduct->sample_insert($data);
			if ($isertd_product) {
				// code...
				$result['msg'] = 'Insert New Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}
		
		to_json($result);
	}

	public function update_product(){
		
		$result['msg'] = 'Cannot Update Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_COMPETITOR'] = $post['ID_COMPETITOR'];
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['NAME_PRODUCT'] = $post['NAME_PRODUCT'];
		$data['MODIFIED_BY'] = $post['user'];

		$isexsist_product = $this->m_sampleproduct->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_sampleproduct->sample_update($data);

			if ($isertd_product) {
				// code...
				$result['msg'] = 'Update Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}

		to_json($result);
	}

	public function delete_product(){
		
		$result['msg'] = 'Cannot Delete Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_COMPETITOR'] = $post['ID_COMPETITOR'];
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['NAME_PRODUCT'] = $post['NAME_PRODUCT'];
		$data['DELETE_BY'] = $post['user'];

		$isertd_product = $this->m_sampleproduct->sample_delete($data);
		if ($isertd_product) {
			// code...
			$result['msg'] = 'Delete Product Success ...';
			$result['status'] = true;
		}

		to_json($result);
	}

	public function product_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		// $data	= $this->Job_m->get($key);

      	$data	= $this->m_sampleproduct->get_data($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_sampleproduct->recFil($key),
  			'recordsTotal'		=>	$this->m_sampleproduct->recTot($key)
  		);

  		echo json_encode($return);
    }

}	

?>
