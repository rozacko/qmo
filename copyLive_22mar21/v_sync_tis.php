<style>
    .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>

<section class="content-header">
	<h1>Sync Data TIS</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Sync Manual Data TIS</h3>
				</div>
				<form role="form" method="POST" action="<?php echo site_url("sync_tis/do_upload") ?>" enctype="multipart/form-data">
					<div class="box-body" style="background-color:#fffaae;">
						<p>Download Template Upload Data TIS <a target="_blank" href="<?= base_url('assets/uploads/proficiency/TEMPLATE_GRABING15.xlsx') ?>">Disini !!!</a></p><hr/>
						<div class="form-group">
							<div class="col-sm-4 clearfix">
								<input type="checkbox" id="manual" name="manual" value="manual">
								<label for="manual"> Upload File Excel Manual</label><br>
							</div>
							<div class="col-sm-4 clearfix" id="file-excel" style="display: none;">
								<label>FILE EXCEL</label>
								<input type="file" class="form-control" name="DATA_EXCEL" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
							</div>
							<div class="col-sm-4 clearfix">
								<button type="submit" id="submit" class="btn btn-primary">Sync Data</button>
							</div>
							
						</div>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</section>
<a id="a-notice-error" class="notice-error" style="display:none"; href="#" data-title="Alert" data-text=""></a>
<div class="modal_load"><!-- Loading modal --></div>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script type="text/javascript">
	$(document).on("submit", "form", function(e){
		e.preventDefault();
		var form_data = new FormData($(this)[0]);
		$.ajax({
			"async": true,
            "crossDomain": true,
            "cache": false,
            "processData": false,
            "contentType": false,
			"url": "<?= base_url('sync_tis/do_sync') ?>",
			"method": "POST",
			"data": form_data,
		}).done(function (response) {
			if(response.status == "200"){
				$("#a-notice-error").data("text", "Sync Data TIS Successfully <br/> Data Insert : "+response.data.TOTAL_ADD_DATA+" Data <br/> Data Update : "+response.data.TOTAL_UPDATE_DATA+" Data");
				$("#a-notice-error").click();
				return false;
			} else {
				$("#a-notice-error").data("text", 'Sync Data TIS is Failed');
				$("#a-notice-error").click();
				return false;
			}
		});
	});

	$(".notice-error").confirm({
        confirm: function(button) { /* Nothing */ },
        confirmButton: "OK",
        cancelButton: "Cancel",
        confirmButtonClass: "btn-danger"
    });

    $(document).on('click', '#manual', function(e) {
    	$("#file-excel").toggle(this.checked);

        
    });
</script>