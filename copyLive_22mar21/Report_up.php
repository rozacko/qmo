<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_up extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_report_up","report");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		//print_r($this->db->last_query());
		//exit();
		
		$role_in = $sesi_user['ROLE'];
		
		$akses = "";
		foreach($role_in as $dt_role){
			if($dt_role['NM_USERGROUP'] == "ADMINISTRATOR"){
				$akses = "admin";
			}
		}
		
		if($akses == "admin" or $user_in->ID_USERGROUP == 1){
			$this->list_data = $this->report->get_list();
		} else {
			$this->list_data = $this->report->get_list_non_admin($user_in->ID_USER);
		}		
		
		$this->template->adminlte("v_report_up");
    }
	
	public function do_upload($tipe){
		$this->load->library('form_validation');
		$this->load->helper('url');
		
		$slug = $this->input->post('slug');
		$id = $this->input->post('id_pro');

		$folder = "./assets/uploads/laporan_proficiency/";
		$path = $_FILES['file']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['file']['tmp_name'], $file_path);
		$path = $slug.".".$ext;
		
		if($tipe == "pdf"){
			$this->report->insert_laporan_pdf($id, $path);
		} else {
			$this->report->insert_laporan_exc($id, $path);
		}
		$url = "report_up";
		redirect(site_url($url));
	}
	
	
}

?>