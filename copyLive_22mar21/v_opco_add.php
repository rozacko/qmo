<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<style type="text/css">
</style>
<section class="content-header">
  <h1>
  Add Notify Reciever
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      

      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" method="POST" action="<?php echo site_url("opco/create") ?>" >
            <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">GROUP NOTIFICATION</label>
               
                <select id="ID_JABATAN" name="ID_JABATAN" class="form-control select2">
                  <?php  foreach($this->list_jabatan as $jabatan): ?>
                    <option value="<?php echo $jabatan->ID_JABATAN;?>" <?php echo ($this->ID_JABATAN == $jabatan->ID_JABATAN)?"SELECTED":"";?> ><?php echo $jabatan->NM_JABATAN;?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_COMPANY">&nbsp;&nbsp;</label><br />
                <input type=checkbox name=TEMBUSAN value=1 /> SEND AS A COPY LETTER
              </div>
            </div>
            
            <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">COMPANY</label>
                <?php if($this->USER->ID_COMPANY): ?>
                <input type=text value="<?php echo $this->USER->NM_COMPANY ?>" class="form-control" readonly />
                <input type=hidden id="ID_COMPANY"  value="<?php echo $this->USER->ID_COMPANY ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_COMPANY" class="form-control select2">
                  <?php  foreach($this->list_company as $company): ?>
                    <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_COMPANY">PLANT</label>
                <?php if($this->USER->ID_PLANT): ?>
                <input type=text value="<?php echo $this->USER->NM_PLANT ?>" class="form-control" readonly />
                <input type=hidden id="ID_PLANT" value="<?php echo $this->USER->ID_PLANT ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_PLANT"  class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
            </div>

            <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">GROUP AREA</label>
                <?php if($this->USER->ID_GROUPAREA): ?>
                <input type=text value="<?php echo $this->USER->NM_GROUPAREA ?>" class="form-control" readonly />
                <input type=hidden id="ID_GROUPAREA" value="<?php echo $this->USER->ID_GROUPAREA ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_GROUPAREA"  class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_AREA">AREA</label>
                <?php if($this->USER->ID_AREA): ?>
                <input type=text value="<?php echo $this->USER->NM_AREA ?>" class="form-control" readonly />
                <input type=hidden id="ID_AREA" NAME="ID_AREA" value="<?php echo $this->USER->ID_AREA ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_AREA" name="ID_AREA" class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>

            </div>
        
       <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">FULLNAME</label>
                
                <input id="fullName" type=text value="<?php echo $this->data_opco->FULLNAME ?>" NAME="FULLNAME" class="form-control"   required />
                
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_AREA">EMAIL</label>
               
                <input id="eMail" type=text value="<?php echo $this->data_opco->EMAIL ?>" NAME="EMAIL" class="form-control"   required />
               
              </div>

            </div>

       <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="TELEGRAM_CHAT_ID">TELEGRAM CHAT ID</label>
                
                <input id="telegram_chat_id" type=text value="<?php echo $this->data_opco->TELEGRAM_CHAT_ID?>" NAME="TELEGRAM_CHAT_ID" class="form-control" />
                
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <input type=checkbox name="TELEGRAM_NOTIFICATION_ENABLE"
                    value="1"
                    <?php echo $this->data_opco->TELEGRAM_NOTIFICATION_ENABLE == 1 ? "checked" : "" ?>
                    id="TELEGRAM_NOTIFICATION_ENABLE"/>
                <label for="TELEGRAM_NOTIFICATION_ENABLE"> ENABLE TELEGRAM NOTIFICATION </label>
              </div>
        </div>
            
                <button type="submit" class="btn btn-primary">Submit</button>
          </form>
          
          

        </div>
        
        
        
        <div class="box">
        <!-- /.box-header -->
      <div class="box-body table-responsive">
        <table id="tb_conf" class="table-fixed table-hover" style="width: 100%">
          <thead>
            <tr>
              <th>NO.</th>
              <th>NOTIF GROUP</th>
              <th>COMPANY</th>
              <th>PLANT</th>
              <th>AREA</th>
              <th>NAME</th>
              <th>EMAIL</th>
              <th>SEND AS</th>
              <th>TELEGRAM NOTIF</th>
            </tr>
          </thead>
          <tbody id="body_conf">
          </tbody>
        </table>
      </div>            
    </div>
        
        
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
</style>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"/></script> 
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script>

  $(document).ready(function(){
    $("#tb_conf").DataTable(); 
  });

  $("form").on("submit", function(e) {
    e.preventDefault();
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method");
    var form_data = $(this).serialize();
    // console.log( $( this ).serialize() );
    
    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data
    }).done(function(response){
      if(response == 3){
        alert("Data Notify Reciever Already Exist")
      } else if(response == 1){
        alert("Add Notify Reciever Successfully")
        $("#ID_PLANT").change();
      } else {
        alert("Add Notify Reciever Failed")
      }
    });
  });

  $("#ID_COMPANY").change(function(){
      var company = $(this).val(), plant = $('#ID_PLANT');
      $.getJSON('<?php echo site_url("opco/ajax_get_plant/");?>' + company, function (result) {
        var values = result;
        
        plant.find('option').remove();
        if (values != undefined && values.length > 0) {
          plant.css("display","");
          $('#ID_GROUPAREA').css("display","");
          $(values).each(function(index, element) {
            plant.append($("<option></option>").attr("value", element.ID_PLANT).text(element.NM_PLANT));
          });
          
        }else{
          plant.find('option').remove();
          plant.append($("<option></option>").attr("value", '00').text("NO PLANT"));
        }
      $("#ID_PLANT").change();
      });
    });

   $("#ID_PLANT").change(function(){
      var plant = $(this).val(), grouparea = $('#ID_GROUPAREA');
      $.getJSON('<?php echo site_url("opco/ajax_get_grouparea/");?>' + $("#ID_COMPANY").val() + '/' + plant, function (result) {
        var values = result;
        
        grouparea.find('option').remove();
        if (values != undefined && values.length > 0) {
          grouparea.css("display","");
          $(values).each(function(index, element) {
            if(!$("#ID_GROUPAREA option[value='"+element.ID_GROUPAREA+"']").length > 0){
              grouparea.append($("<option></option>").attr("value", element.ID_GROUPAREA).text(element.NM_GROUPAREA));
            }
          });
        }else{
          grouparea.find('option').remove();
          grouparea.append($("<option></option>").attr("value", '00').text("NO GROUP AREA"));
        }
        $("#ID_GROUPAREA").change();
      });
    });

   $("#ID_GROUPAREA").change(function(){
      var grouparea = $(this).val(), area = $('#ID_AREA');
      $.getJSON('<?php echo site_url("opco/ajax_get_area/");?>' + $("#ID_COMPANY").val() + '/' + $("#ID_PLANT").val() + '/' + grouparea, function (result) {
        var values = result;
        
        area.find('option').remove();
        if (values != undefined && values.length > 0) {
          area.css("display","");
          $(values).each(function(index, element) {
            area.append($("<option></option>").attr("value", element.ID_AREA).text(element.NM_AREA));
          });
        }else{
          area.find('option').remove();
          area.append($("<option></option>").attr("value", '00').text("NO AREA"));
        }
        $("#ID_AREA").change();
      });
    });

   $("#ID_AREA").change(function(){
    var tr = null;

    // tr = "<tr><td colspan=8><img src='<?php echo base_url("images/hourglass.gif");?>'> Please wait...</td></tr>";
    $.getJSON("<?php echo site_url("opco/notification_member") ?>/"+this.value+"/"+$("#ID_JABATAN").val(),function(data){
      tr = "";
      if(data.length == 0){
        // tr = "<tr><td colspan=8>There is no opco was added.</td></tr>";
      }
      else{
        var x=1;
        $.each(data,function(key,r){
          tr += "<tr>"; 
          tr += "<td align=center>"+(x++)+"</td>";
          tr += "<td>"+r.NM_JABATAN+"</td>";
          tr += "<td>"+r.NM_COMPANY+"</td>";
          tr += "<td>"+r.NM_PLANT+"</td>";
          tr += "<td>"+r.NM_AREA+"</td>";
          tr += "<td>"+r.FULLNAME+"</td>";
          tr += "<td>"+r.EMAIL+"</td>";         
          tr += "<td>"+((r.TEMBUSAN)?"MAIL COPY":"NOTIFICATION")+"</td>";
          tr += "<td>"+((r.TELEGRAM_NOTIFICATION_ENABLE == 1)?"Yes":"No")+"</td>";
          tr +="</tr>";
        });
        // console.log(tr);
        $("#tb_conf").DataTable().destroy();
        $("#body_conf").html(tr);
        $("#tb_conf").DataTable();
        }
      
    });



  });

    $("#ID_JABATAN").change(function(){
    $("#ID_AREA").change();
  });
  
  $("#ID_COMPANY").change();

  /* Auto Complete */
  var options = {
    url: function(username) {
      return '<?php echo site_url("opco/get_username_registered/") ?>';
    },
    getValue: function(element) {
      return element.FULLNAME;
    },
    //Description
    template: {
      type: "description",
      fields: {
        description: "COMPANY"
      }
    },

    //Set return data
    list: {
      onSelectItemEvent: function() {
        var selectedItemValue = $("#fullName").getSelectedItemData().EMAIL;
        $("#eMail").val(selectedItemValue).trigger("change");
      }
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },

    //Sending post data
    preparePostData: function(data) {
      data.username = $("#fullName").val();
      return data;
    },
    theme: "plate-dark", //square | round | plate-dark | funky
    requestDelay: 400
  };

  $(function(){
    $("#fullName").easyAutocomplete(options);
  });

</script>
