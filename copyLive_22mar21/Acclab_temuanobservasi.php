<?php

class Acclab_temuanobservasi extends QMUser {
	
	public $list_data = array();
	public $inputted = array();
	public $key;
	public $jsonData;
	public $data_groupmenu;
	 
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_acclab_temuanObservasi", "observasi");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_data = $this->observasi->get_list();
		$this->inputted = $this->observasi->get_tindaklanjutlist();
//		$this->list_form = $thsi-observasi->get_komponen();
//		$temp = array();
//		$arrayind = 0;
//		$fk_temp = 0;
//		$picText = "";
//		for ($i = 0; $i < count($this->list_data); $i++) {
//			if ($fk_temp !== $this->list_data[$i]->FK_ID_PERIODE) {
//				$picText = "" . $this->list_data[$i]->FULLNAME_OBSERVASI;
//				$fk_temp = $this->list_data[$i]->FK_ID_PERIODE;
//				$temp[] = $this->list_data[$i];
//				$arrayind++;
//			} else {
//				$picText .= ", " . $this->list_data[$i]->FULLNAME_OBSERVASI;
//				$temp[$arrayind - 1]->FULLNAME_OBSERVASI = $picText;
//			}
//		}
//		$this->list_data = $temp;
		$this->jsonData = json_encode($this->list_data);
		$this->template->adminlte("v_acclab_temuanObservasi");
	}

	public function submitTemuan() {
		$data = $this->observasi->doSubmitTemuan($this->input->post());
		to_json($data);
	}

	public function editTemuan() {
		$data = $this->observasi->doSubmitTemuan($this->input->post());
		to_json($data);
	}

	public function getForm() {
		$data = $this->observasi->get_inputForm();
		to_json($data);
	}

}	

?>
