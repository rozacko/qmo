<?php

class M_Sampleproduct Extends DB_QM {
	private $post  = array();
	private $table = "O_COMPETITOR";
	private $pKey  = "ID_COMPETITOR";
	private $column_order = array(NULL, 'URUTAN'); //set column for datatable order
    private $column_search = array('NAME_COMPETITOR', 'NAME_PRODUCT'); //set column for datatable search 
    private $order = array("A.KODE_PERUSAHAAN" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	var $column = array(
		'A.ID_COMPETITOR', 'A.NAME_COMPETITOR', 'A.MERK_PRODUCT', 'B.ID_PRODUCT', 'A.ID_COMPETITOR'
	);


	var $column_scm = array(
		'A.KODE_PERUSAHAAN', 'A.NAMA_PERUSAHAAN', 'A.INISIAL', 'A.PRODUK', 'A.KELOMPOK'
	);

	public function is_sample_exists($data){
		if (isset($data['ID_COMPETITOR'])) {
			# code...
		}
		$this->db->where("LOWER(NAME_COMPETITOR)", strtolower($data['NAME_COMPETITOR']));
		$this->db->where("LOWER(MERK_PRODUCT)", strtolower($data['MERK_PRODUCT']));
		$this->db->where("LOWER(TYPE_PRODUCT)", strtolower($data['TYPE_PRODUCT']));
		return $this->db->get("O_COMPETITOR")->num_rows();
	}

	public function sample_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("O_COMPETITOR");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function get_typeProduct($data){
		$this->db->select('B.*, A.KODE_PERUSAHAAN');
		$this->db->from('O_TYPE_PRODUCT_SCM A');
		$this->db->join("M_PRODUCT B", "A.TYPE_PRODUCT = B.ID_PRODUCT");
		$this->db->where('A.KODE_PERUSAHAAN', $data['KODE_PERUSAHAAN']);
		$this->db->where("(LOWER(B.KD_PRODUCT) = 'ppc' OR LOWER(B.KD_PRODUCT) = 'pcc' OR LOWER(B.KD_PRODUCT) = 'opc')");
		$this->db->where('A.DELETE_FLAG', 0);
		$query		= $this->db->get();
		return $query->result_array();
	}

	public function sample_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("NAME_COMPETITOR", $data['NAME_COMPETITOR']);
		$this->db->set("MERK_PRODUCT", $data['MERK_PRODUCT']);
		$this->db->set("TYPE_PRODUCT", $data['TYPE_PRODUCT']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $data['MODIFIED_BY']);
		$this->db->where("ID_COMPETITOR", $data['ID_COMPETITOR']);
		$query = $this->db->update("O_COMPETITOR");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function is_product_type_exists($data){
		$this->db->where("KODE_PERUSAHAAN", $data['KODE_PERUSAHAAN']);
		return $this->db->get("O_TYPE_PRODUCT_SCM")->num_rows();
	}

	public function product_type_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("TYPE_PRODUCT", $data['TYPE_PRODUCT']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $data['MODIFIED_BY']);
		$this->db->where("KODE_PERUSAHAAN", $data['KODE_PERUSAHAAN']);
		$query = $this->db->update("O_TYPE_PRODUCT_SCM");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}
	
	public function reset_product_type_exists($data){
		$this->db->where("KODE_PERUSAHAAN", $data);
		$query = $this->db->delete("O_TYPE_PRODUCT_SCM");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function get_id_product_type($data){
		$this->db->from('M_PRODUCT A');
		$this->db->where("LOWER(A.KD_PRODUCT)", strtolower($data));
		$query		= $this->db->get();
		return $query->row_array();
	}

	public function product_type_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("O_TYPE_PRODUCT_SCM");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_delete($data){
		$this->db->where("ID_COMPETITOR", $data['ID_COMPETITOR']);
		$query = $this->db->delete("O_COMPETITOR");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	function _qry($key){
		$this->db->select('A.*, B.*');
		$this->db->from('O_COMPETITOR A');
		$this->db->join("M_PRODUCT B", "A.TYPE_PRODUCT = B.ID_PRODUCT");
		if($key['search']!==''){
			$this->db->or_like('LOWER(A.NAME_COMPETITOR)', strtolower($key['search']));
			$this->db->or_like('LOWER(A.MERK_PRODUCT)', strtolower($key['search']));
			$this->db->or_like('LOWER(B.KD_PRODUCT)', strtolower($key['search']));
		}
		$this->db->where('A.DELETE_FLAG', 0);
		$order = $this->column[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function _qryscm($key){
		$this->scmdb->select('A.*');
		$this->scmdb->from('ZREPORT_MS_PERUSAHAAN A');
		if($key['search']!==''){
			$this->scmdb->or_like('LOWER(A.KODE_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.NAMA_PERUSAHAAN)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.INISIAL)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.PRODUK)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.KELOMPOK)', strtolower($key['search']));
		}
		$order = $this->column_scm[$key['ordCol']];
		$this->scmdb->order_by($order, $key['ordDir']);

	}

	function get($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();
		return $data;
	}

	function get_data($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();
		return $data;
	}

	function recFil($key){
		$this->_qry($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot(){
		$query			= $this->db->get('O_COMPETITOR');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function get_data_scm($key){
		$this->_qryscm($key);
		$this->scmdb->limit($key['length'], $key['start']);
		$query		= $this->scmdb->get();
		$data			= $query->result_array();
		return $data;
	}

	function recFil_scm($key){
		$this->_qryscm($key);
		$query			= $this->scmdb->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_scm(){
		$query			= $this->scmdb->get('ZREPORT_MS_PERUSAHAAN');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	public function datalist(){
		$this->db->order_by("a.ID_COMPETITOR");
		return $this->db->get("O_COMPETITOR a")->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

}
