<?php

class M_form_pengujian Extends DB_QM {
	private $post  = array();
	private $table = "M_FORM_PENGUJIAN";
	private $pKey  = "ID_FORM";
	private $column_order = array(NULL, "ID_FORM"); //set column for datatable order
    private $column_search = array("TITLE"); //set column for datatable search 
    private $order = array("ID_FORM" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
	}
	
	public function data_list(){
		$this->get_query();
		$query = $this->db->get();
		return $query->result();
	}

    public function detail($ID_FORM){
        $this->get_query();
        $this->db->where($this->table.".ID_FORM", $ID_FORM);
        $query = $this->db->get();
		return $query->result();
	
	}

    public function get_query(){
		$this->db->select("*");
        $this->db->from($this->table);
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", $this->table.".ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->where($this->table.".DELETED_AT IS NULL");
	}

	public function list_komoditi(){
		$this->db->select("*");
        $this->db->from("M_SAMPLE_UJI_PROFICIENCY");
		$this->db->where("DELETED IS NULL");
		$query = $this->db->get();
		return $query->result();
	}

    public function get_list() {
		$this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
    }
    
    public function insert($data){
        $this->db->set($data);
        $this->db->set("CREATED_AT", "CURRENT_DATE", false);
		$this->db->insert($this->table);
    }
    
    public function update($data, $ID_FORM){
        $this->db->set($data);
        $this->db->set("UPDATED_AT", "CURRENT_DATE", false);
        $this->db->where("ID_FORM",$ID_FORM);
		$this->db->update($this->table);
    }
    
    public function delete($ID_FORM){
        $this->db->set("DELETED_AT", "CURRENT_DATE", false);
		$this->db->where("ID_FORM",$ID_FORM);
		$this->db->update($this->table);
	}
}