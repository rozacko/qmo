<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Penerimaan Sample Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
                    <div class="input-group input-group-sm" style="width: 150px; ">
						<a href="<?= base_url('uji_proficiency_penerimaan/add') ?>" id="btn-new" type="button" class="btn btn-block btn-primary btn-sm">Input Konfirmasi Penerimaan</a>
					</div>
                </div>
				<div class="box-body">
					<div class="table-responsive">
						<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="3%">No</th>
									<th>Uji Proficiency</th>
									<th>Komoditi</th>
									<th>Tanggal Penerimaan Sample</th>
									<th>PIC Penerima Sample</th>
									<th>Tanggal Start Uji</th>
									<th>Tanggal End Uji</th>
									<th width="10%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
	$(document).ready(function(){
		var table = $('#dt_tables').DataTable({ 
            "processing" : true, 
            "serverSide" : true, 
            "destroy" : true,
            "autoWidth" : false,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('uji_proficiency_penerimaan/get_list') ?>",
                "type": "POST"
            },
            "columns" : [
                {data: 'NO', name: 'NO', orderable: false, searchable: false, width:'3%'},
                {data: 'TITLE_PP', name: 'TITLE_PP', render: function(e, t, f){
					return f.TITLE_PP+" "+f.YEAR_PP
				}},
                {data: 'NAMA_SAMPLE', name: 'NAMA_SAMPLE'},
                {data: 'TGL_PENERIMAAN', name: 'TGL_PENERIMAAN'},
                {data: 'NAMA_PENERIMA', name: 'NAMA_PENERIMA'},
                {data: 'TGL_UJI_START', name: 'TGL_UJI_START'},
                {data: 'TGL_UJI_END', name: 'TGL_UJI_END'},
                {data: 'NO', name: 'NO', orderable: false, searchable: false, render: function(e, t, f){
					if(f.TGL_UJI_START == null){
						return '<center>'+
						'<button id="btn-input" data-ID_PROFICIENCY="'+f.ID_PROFICIENCY+'" data-ID_PENERIMA="'+f.ID_PENERIMA+'" class="btn btn-xs btn-icon icon-left btn-info"><i class="fa fa-upload"></i> Input Tanggal Uji Sample</button>'+
						'</center>';
					} else {
						return '';
					}
                   
                }},
            ],
        });

		$(document).on("click", "#btn-input", function(e){
			var proficiency = $(this).data("id_proficiency");
			var id = $(this).data("id_penerima");
			window.location = "<?= base_url('uji_proficiency_penerimaan/add_tgl_uji') ?>?proficiency="+proficiency+"&id="+id;
		});
	});
</script>