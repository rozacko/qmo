<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Input Sample Provider Uji Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
                <b>Periode Proficiency: </b> <?= $this->proficiency->TITLE_PP;?> [<?= $this->proficiency->GROUP_PP;?> - <?= $this->proficiency->YEAR_PP;?>] | <b>Commodity: </b> <?= $this->proficiency->NAMA_SAMPLE; ?> | <b>Lab: </b> <?= $this->proficiency->NAMA_LAB; ?> | <b>PIC: </b> <?= $this->proficiency->FULLNAME; ?>
                </div>
				<div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li <?= $this->status_proficiency == "FILE_SAMPLE" ? 'class="active"' : "" ?>>
                                <a <?php if( $this->status_proficiency == "FILE_PETUNJUK_TEKNIS" || $this->status_proficiency == "FILE_SAMPLE" || $this->status_proficiency == "FILE_HOMOGEN" || $this->status_proficiency == "FILE_PENGEMASAN" || $this->status_proficiency == "PENGIRIMAN" || $this->status_proficiency == "DONE"){ ?>
                                href="#tab_persiapan" data-toggle="tab" <?php } ?>>Persiapan Sample</a>
                            </li>
                            <li <?= $this->status_proficiency == "FILE_HOMOGEN" ? 'class="active"' : "" ?>>
                                <a <?php if($this->status_proficiency == "FILE_PETUNJUK_TEKNIS" || $this->status_proficiency == "FILE_HOMOGEN" || $this->status_proficiency == "FILE_PENGEMASAN" || $this->status_proficiency == "PENGIRIMAN" || $this->status_proficiency == "DONE"){ ?> 
                                href="#tab_uji" data-toggle="tab" <?php } ?>>Uji Homogenitas & Stabilitas</a>
                            </li>
                            <li <?= $this->status_proficiency == "FILE_PENGEMASAN" ? 'class="active"' : "" ?>>
                                <a <?php if( $this->status_proficiency == "FILE_PETUNJUK_TEKNIS" || $this->status_proficiency == "FILE_PENGEMASAN" || $this->status_proficiency == "PENGIRIMAN" || $this->status_proficiency == "DONE"){ ?> 
                                href="#tab_pengemasan" data-toggle="tab" <?php } ?>>Pengemasan Sample</a>
                            </li>
                            <li <?= $this->status_proficiency == "FILE_PETUNJUK_TEKNIS" ? 'class="active"' : "" ?>>
                                <a <?php if($this->status_proficiency == "FILE_PETUNJUK_TEKNIS" || $this->status_proficiency == "FILE_PENGEMASAN" || $this->status_proficiency == "PENGIRIMAN" || $this->status_proficiency == "DONE"){ ?> 
                                href="#tab_petunjuk_teknis" data-toggle="tab" <?php } ?>>File Petunjuk Teknis</a>
                            </li>
                            <li <?= $this->status_proficiency == "PENGIRIMAN" ? 'class="active"' : "" ?>>
                                <a <?php if($this->status_proficiency == "PENGIRIMAN" || $this->status_proficiency == "DONE"){ ?> 
                                href="#tab_pengiriman" data-toggle="tab" <?php } ?>>Pengiriman Sample</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane <?= $this->status_proficiency == "FILE_SAMPLE" ? 'active' : "" ?>" id="tab_persiapan">
                                <div class="row">
                                    <form action="<?= base_url('sampling/action_input/').$this->input->get('id') ?>" method="POST" enctype="multipart/form-data">
                                        <?php if($this->status_proficiency != "PENGIRIMAN"){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <label>FOTO AKTIVITAS PERSIAPAN</label>
                                                <input type="file" class="form-control" name="FILE_SAMPLE[]" multiple="multiple" accept="image/*" required >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <?php if($this->proficiency->FILE_SAMPLE != ""){ ?>
                                                    <?php
                                                        $f_sample = explode(";", $this->proficiency->FILE_SAMPLE);
                                                        for($fs=0;$fs<count($f_sample);$fs++){
                                                    ?>
                                                    <img height="300px" width="300px" src="<?= base_url('assets/uploads/proficiency/').$f_sample[$fs]; ?>" />
                                                    <?php } } ?>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane <?= $this->status_proficiency == "FILE_HOMOGEN" ? 'active' : "" ?>" id="tab_uji">
                                <div class="row">
                                    <form action="<?= base_url('sampling/action_input/').$this->input->get('id') ?>" method="POST" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <label>FILE EXCEL HOMOGENITAS</label>
                                                <?php if($this->proficiency->FILE_HOMOGEN_STABIL_1 == ""){ ?>
                                                    <input type="file" class="form-control" name="FILE_HOMOGEN_STABIL_1" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"  required />
                                                <?php } else { ?>
                                                    <br/><a class="btn btn-sm btn-info" target="_blank" href="<?= base_url('assets/uploads/proficiency/').$this->proficiency->FILE_HOMOGEN_STABIL_1 ?>"">Download Excel Homogenitas</a>
                                                    <br/>&nbsp;<input type="file" class="form-control" name="FILE_HOMOGEN_STABIL_1" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if($this->status_proficiency != "PENGIRIMAN"){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <label>FOTO AKTIVITAS PENGUJIAN HOMOGENITAS</label>
                                                <input type="file" class="form-control" name="FILE_HOMOGEN_STABIL_2[]" multiple="multiple" accept="image/*" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="form-group">
                                                <div class="col-sm-12 clearfix">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <?php if($this->proficiency->FILE_HOMOGEN_STABIL_2 != ""){ 
                                                    $f_homogen = explode(";", $this->proficiency->FILE_HOMOGEN_STABIL_2);
                                                    for($fh=0;$fh<count($f_homogen);$fh++){    
                                                ?>
                                                <img height="300px" width="300px" src="<?= base_url('assets/uploads/proficiency/').$f_homogen[$fh]; ?>" />
                                                    <?php } } ?>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane <?= $this->status_proficiency == "FILE_PENGEMASAN" ? 'active' : "" ?>" id="tab_pengemasan">
                                <div class="row">
                                    <form action="<?= base_url('sampling/action_input/').$this->input->get('id') ?>" method="POST" enctype="multipart/form-data">
                                        <?php if($this->status_proficiency == "FILE_PENGEMASAN"){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <label>FOTO PENGEMASAN SAMPLE</label>
                                                <input type="file" class="form-control" name="FILE_PENGEMASAN[]" multiple="multiple" accept="image/*" required >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <?php if($this->proficiency->FILE_PENGEMASAN != ""){ ?>
                                                <?php
                                                    $f_pengemasan = explode(";", $this->proficiency->FILE_PENGEMASAN);
                                                    for($fp=0;$fp<count($f_pengemasan);$fp++){    
                                                ?>
                                                <img height="300px" width="300px" src="<?= base_url('assets/uploads/proficiency/').$f_pengemasan[$fp] ?>" />

                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane <?= $this->status_proficiency == "FILE_PETUNJUK_TEKNIS" ? 'active' : "" ?>" id="tab_petunjuk_teknis">
                                <div class="row">
                                    <form action="<?= base_url('sampling/action_input/').$this->input->get('id') ?>" method="POST" enctype="multipart/form-data">
                                        <?php if($this->status_proficiency == "FILE_PETUNJUK_TEKNIS"){ ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <label>FILE PETUNJUK TEKNIS</label>
                                                <input type="file" class="form-control" name="FILE_PETUNJUK_TEKNIS" accept="application/pdf" required >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="col-sm-12 clearfix">
                                                <?php if($this->proficiency->FILE_PETUNJUK_TEKNIS != ""){ ?>
                                                <?php
                                                    $f_petunjuk_teknis = explode(";", $this->proficiency->FILE_PETUNJUK_TEKNIS);
                                                    for($fp=0;$fp<count($f_petunjuk_teknis);$fp++){    
                                                ?>
                                                <br/><a class="btn btn-sm btn-info" target="_blank" href="<?= base_url('assets/uploads/proficiency/').$this->proficiency->FILE_PETUNJUK_TEKNIS ?>"">Download File Petunjuk Teknis</a>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                            <div class="tab-pane <?= $this->status_proficiency == "PENGIRIMAN" ? 'active' : "" ?>" id="tab_pengiriman">
                                <div class="row">
                                    <form action="<?= base_url('sampling/tambah_penerima/').$this->input->get('id') ?>" method="POST">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label>NAMA PENGIRIM *</label>
                                                <input type="hidden" class="form-control" id="ID_PENERIMA" name="ID_PENERIMA" />
                                                <input type="text" class="form-control" name="NAMA_PENGIRIM" id="NAMA_PENGIRIM" value="<?= $this->session->userdata('USER')->FULLNAME ?>" required readonly />
                                            </div>
                                            <div class="col-sm-6">
                                                <label>ALAMAT PENGIRIM *</label>
                                                <input type="text" class="form-control" name="ALAMAT_PENGIRIM" id="ALAMAT_PENGIRIM" required />
                                            </div><br/>&nbsp;
                                            <div class="col-sm-6"><br/>
                                                <label>NAMA PENERIMA *</label>
                                                <input id="NAMA_PENERIMA" type=text NAME="NAMA_PENERIMA" class="form-control" required />
                                                <!-- <select class="form-control select2" NAME="ID_USER_PENERIMA" id="ID_USER_PENERIMA" placeholder="Select Penerima ...">
                                                    <option value="">Select a PIC ...</option>
                                                    <?php foreach($this->list_pic as $dt_pic): ?>
                                                    <option value="<?php echo $dt_pic->ID_USER ?>" data-fullname="<?= $dt_pic->FULLNAME ?>" data-email="<?= $dt_pic->EMAIL ?>" ><?php echo $dt_pic->FULLNAME ?></option>
                                                    <?php endforeach; ?>
                                                </select>  -->
                                            </div>
                                            <div class="col-sm-6">
                                                <label>EMAIL *</label>
                                                <input type="hidden" class="form-control" name="ID_USER_PENERIMA" id="ID_USER_PENERIMA" />
                                                <input type="text" class="form-control" name="EMAIL_PENERIMA" id="EMAIL_PENERIMA" required />
                                            </div><br/>&nbsp;
                                            <div class="col-sm-12 clearfix">
                                                <label>ALAMAT YANG DITUJU *</label>
                                                <textarea class="form-control" name="ALAMAT_PENERIMA" id="ALAMAT_PENERIMA" required></textarea>
                                            </div>
                                            <div class="col-sm-12 clearfix">
                                                <label>CC PENERIMA *</label>
                                                <textarea class="form-control" name="CC_PENERIMA" id="CC_PENERIMA" required></textarea>
                                                <sub>CC Penerima digunakan untuk pemberian akses saat pengisian form hasil uji.
                                                tuliskan nama dengan pemisah titik koma (;) <b>contoh: Ahmad;Andi;Bambang;</b></sub>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>TANGGAL PENGIRIMAN *</label>
                                                <input type="text" class="form-control" name="TGL_PENGIRIMAN" id="TGL_PENGIRIMAN" value="<?= date('d/m/Y') ?>" required />
                                            </div>
                                            <div class="col-sm-6">
                                                <label>JUMLAH SAMPLE DIKIRIM *</label>
                                                <input type="text" class="form-control" name="JML_SAMPLE" id="JML_SAMPLE" required />
                                            </div><br/>&nbsp;
                                            <!-- <div class="col-sm-6 clearfix">
                                                <label>RESI PENGIRIMAN</label>
                                                <input type="text" class="form-control" name="RESI" >
                                            </div> -->
                                        </div>
                                        <?php if($this->status_proficiency == "PENGIRIMAN"){ ?>
                                        <div class="form-group">
                                            <div id="segment-btn-cancel" class="col-sm-12 clearfix">
                                                <button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        </form>
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>NO</th>
                                                            <th>NAMA PENGIRIM</th>
                                                            <th>ALAMAT PENGIRIM</th>
                                                            <th>NAMA PENERIMA</th>
                                                            <th>EMAIL PENERIMA</th>
                                                            <th>ALAMAT</th>
                                                            <th>CC PENERIMA</th>
                                                            <th>TANGGAL PENGIRIMAN</th>
                                                            <th>JUMLAH SAMPLE</th>
                                                            <th width="7%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i=1; foreach ($this->penerima as $key => $value) { ?>
                                                            <tr>
                                                                <td><?= $i ?></td>
                                                                <td><?= $value['NAMA_PENGIRIM'] ?></td>
                                                                <td><?= $value['ALAMAT_PENGIRIM'] ?></td>
                                                                <td><?= $value['NAMA_PENERIMA'] ?></td>
                                                                <td><?= $value['EMAIL_PENERIMA'] ?></td>
                                                                <td><?= $value['ALAMAT_PENGIRIMAN'] ?></td>
                                                                <td><?= $value['CC_PENERIMA']; ?></td>
                                                                <td><?= $value['TGL_PENGIRIMAN'] ?></td>
                                                                <td><?= $value['JML_SAMPLE'] ?></td>
                                                                <td>
                                                                    <button type="button" id="btn-edit" data-id_penerima="<?= $value['ID_PENERIMA'] ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></button>
                                                                    <button type="button" id="btn-delete" data-id_penerima="<?= $value['ID_PENERIMA'] ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                </td>
                                                            </tr>
                                                        <?php $i++; } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="<?= base_url('sampling') ?>" class="btn btn-success btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
			</div>
		</div>
	</div>
</section>
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"></script> 
<script>
    $(document).ready(function(){
        $("#TGL_PENGIRIMAN").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });

        // $("#ID_USER_PENERIMA").select2();
    });

    // $(document).on("change", "#ID_USER_PENERIMA", function(e){
    //     $("#NAMA_PENERIMA").val($(this).find(':selected').data("fullname"));
    //     $("#EMAIL_PENERIMA").val($(this).find(':selected').data("email"));
    // });

    $("form").on("submit", function(e) {
		e.preventDefault();
        
		var post_url = $(this).attr("action");
		var request_method = $(this).attr("method");
        var form_data = new FormData(this);
		
		$.ajax({
            processData: false,
            contentType: false,
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
				alert("Input Activity Sukses");
                location.reload();
			} else {
				alert("Input Activity Failed")
			}
		});
	});

    $(document).on("click", "#btn-edit", function(e){
        var id = $(this).data("id_penerima");
        $.ajax({
			url : '<?php echo site_url("sampling/detail_penerima") ?>/'+id,
		}).done(function(response){
            $("#ID_PENERIMA").val(response.ID_PENERIMA);
            $("#NAMA_PENGIRIM").val(response.NAMA_PENGIRIM);
            $("#ALAMAT_PENGIRIM").val(response.ALAMAT_PENGIRIM);
            $("#ID_USER_PENERIMA").val(response.ID_USER_PENERIMA).change();
            $("#NAMA_PENERIMA").val(response.NAMA_PENERIMA);
            $("#EMAIL_PENERIMA").val(response.EMAIL_PENERIMA);
            $("#ALAMAT_PENERIMA").val(response.ALAMAT_PENGIRIMAN);
            $("#CC_PENERIMA").val(response.CC_PENERIMA);
            var date = new Date(response.TGL_PENGIRIMAN);
            $('#TGL_PENGIRIMAN').datepicker("setDate", ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/'+ ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear());
            $("#JML_SAMPLE").val(response.JML_SAMPLE);

            $("#segment-btn-cancel").html('<button type="submit" id="btn-submit" class="btn btn-primary">Update</button>&nbsp;<button type="button" id="btn-cancel" class="btn btn-danger">Cancel Update</button>');
		});
    });

    $(document).on("click", "#btn-cancel", function(e){
        $("#ID_PENERIMA").val("");
        $("#NAMA_PENGIRIM").val("");
        $("#ALAMAT_PENGIRIM").val("");
        $("#ID_USER_PENERIMA").val("").change();
        $("#NAMA_PENERIMA").val("");
        $("#EMAIL_PENERIMA").val("");
        $("#ALAMAT_PENERIMA").val("");
        $("#CC_PENERIMA").val("");
        $("#TGL_PENGIRIMAN").val("");
        $("#JML_SAMPLE").val("");
        
        $("#segment-btn-cancel").html('<button type="submit" id="btn-submit" class="btn btn-primary">Submit</button>');
    });

    $(document).on("click", "#btn-delete", function(e){
        var id = $(this).data("id_penerima")
		var c = confirm("Apakah yakin ingin menghapus ?");
		if(c){
			$.ajax({
				url : '<?php echo site_url("sampling/delete_penerima") ?>'+'/'+id,
				type: "POST",
			}).done(function(response){
				if(response == 1){
					alert("Delete Penerima Sample Successfully");
					location.reload();
				} else {
					alert("Delete Penerima Sample Failed")
				}
			});
		}
    });

    /* Auto Complete */
    var options = {
        url: function(username) {
            return '<?php echo site_url("opco/get_username_registered/") ?>';
        },
        getValue: function(element) {
            return element.FULLNAME;
        },
        template: {
            type: "description",
            fields: {
                description: "COMPANY"
            }
        },
        //Set return data
        list: {
            onSelectItemEvent: function() {
                var selectedItemValue = $("#NAMA_PENERIMA").getSelectedItemData().EMAIL;
                var selectedIDValue = $("#NAMA_PENERIMA").getSelectedItemData().ID_USER;
                $("#EMAIL_PENERIMA").val(selectedItemValue).trigger("change");
                $("#ID_USER_PENERIMA").val(selectedIDValue).trigger("change");
            }
        },
        ajaxSettings: {
            dataType: "json",
            method: "POST",
            data: {
                dataType: "json"
            }
        },  
        //Sending post data
        preparePostData: function(data) {
            data.username = $("#NAMA_PENERIMA").val();
            return data;
        },
        theme: "plate-dark", //square | round | plate-dark | funky
        requestDelay: 400
    };
    
    $(function(){
        $("#NAMA_PENERIMA").easyAutocomplete(options);
    });
</script>