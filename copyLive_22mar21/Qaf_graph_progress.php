<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qaf_graph_progress extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_qaf_graph_progress");
	}

}

/* End of file Qaf_graph_progress.php */
/* Location: ./application/controllers/Qaf_graph_progress.php */
?>