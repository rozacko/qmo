<?php

class Area extends QMUser {
	
	public $list_company = array();
	public $list_plant = array();
	public $list_area = array();
	public $list_grouparea = array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $data_area;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
	}
	
	public function index(){
		
		$this->ID_COMPANY = ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$this->ID_COMPANY;
		$this->ID_PLANT   = ($this->USER->ID_PLANT)?$this->USER->ID_PLANT:$this->ID_PLANT;
		
		$this->list_company   = $this->m_company->datalist();
		$this->list_plant 	  = $this->m_plant->datalist($this->ID_COMPANY); #echo $this->m_plant->get_sql();
		$this->list_grouparea = $this->m_grouparea->datalist();
		$this->list_area 	  = $this->m_area->datalist($this->ID_COMPANY, $this->ID_PLANT); #echo $this->m_plant->get_sql();
		$this->template->adminlte("v_area");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}	

	public function by_plant($ID_PLANT=NULL){
		$this->ID_PLANT   = $ID_PLANT;
		$this->ID_COMPANY = $this->m_plant->data("ID_PLANT='$ID_PLANT'")->ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_COMPANY=NULL, $ID_PLANT=NULL){
		$this->ID_COMPANY 	= ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$ID_COMPANY;
		$this->ID_PLANT   	= ($this->USER->ID_PLANT)?$this->USER->ID_PLANT:$ID_PLANT;
		$this->list_company = $this->m_company->datalist();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist();
		$this->template->adminlte("v_area_add");
	}
	
	public function create(){
		$this->m_area->insert($this->input->post()); #die($this->m_area->get_sql());
		if($this->m_area->error()){
			$ID_PLANT	= $this->input->post("ID_PLANT");
			$this->ID_COMPANY = $this->m_plant->data("ID_PLANT='$ID_PLANT'")->ID_COMPANY;
			$this->notice->error($this->m_area->error());
			redirect("area/add/".$ID_COMPANY."/".$ID_PLANT);
		}
		else{
			$this->notice->success("Area Data Saved.");
			redirect("area/by_plant/".$this->input->post("ID_PLANT"));
		}
	}
	
	public function edit($ID_AREA, $ID_COMPANY=NULL, $ID_PLANT=NULL){
		$this->data_area 	= $this->m_area->get_data_by_id($ID_AREA);
		$this->ID_PLANT   	= ($ID_PLANT) ? $ID_PLANT:$this->data_area->ID_PLANT;
		$this->ID_COMPANY 	= ($ID_COMPANY) ? $ID_COMPANY:$this->m_plant->data("ID_PLANT='$this->ID_PLANT'")->ID_COMPANY;
		$this->ID_COMPANY 	= ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$this->ID_COMPANY;
		$this->ID_PLANT   	= ($this->USER->ID_PLANT)?$this->USER->ID_PLANT:$ID_PLANT;
		
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist();
		$this->template->adminlte("v_area_edit");
	}
	
	public function update($ID_AREA){
		$this->m_area->update($this->input->post(),$ID_AREA);
		if($this->m_area->error()){
			$this->notice->error($this->m_area->error());
			redirect("area/edit/".$ID_AREA);
		}
		else{
			$this->notice->success("Area Data Updated.");
			redirect("area/by_plant/".$this->input->post("ID_PLANT"));
		}
	}
	
	public function delete($ID_AREA,$ID_PLANT=NULL){
		$this->m_area->delete($ID_AREA);
		if($this->m_area->error()){
			$this->notice->error($this->m_area->error());
		}
		else{
			$this->notice->success("Area Data Removed.");
		}
		redirect("area/by_plant/".$ID_PLANT);
	}
	
	public function ajax_area_list($ID_PLANT=NULL){
		echo json_encode($this->m_area->list_area($ID_PLANT));
	}
}	

?>
