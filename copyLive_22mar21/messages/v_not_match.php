<style type="text/css">
  .skin-red-light .wrapper, .skin-red-light .main-sidebar, .skin-red-light .left-side {
    background-color: #ecf0f5;
  }
  .header, .breadcrumb, h4, .sidebar-toggle, .sidebar, .main-footer{
    display: none
  }
  .skin-red-light .content-wrapper, .skin-red-light .main-footer {
    border-left: 0;
  }

</style>
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      	Approval
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
       <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Approval</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            	<div class="keterangan" style="text-align: center">
            		<h5>Sorry, this request does not exist!</h5>
            	</div>
            </div>
          </div>
        </div>
      </div>
    </section>
