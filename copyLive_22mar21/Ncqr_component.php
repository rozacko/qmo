<?php

class Ncqr_component extends QMUser {
	
	public $list_config = array();
	public $list_plant = array();
	public $list_company = array();
	public $list_grouparea = array();
	public $list_component = array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $ID_GROUPAREA;
	public $DISPLAY;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("c_ncqr_component");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_component");
	}
	
	public function index(){
		$this->ID_COMPANY 	= $this->input->get("id_company");
		$this->ID_PLANT   	= $this->input->get("id_plan");
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist();
		$this->template->adminlte("v_ncqr_component");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_PLANT=NULL){
		$data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_COMPANY = $data_plant->ID_COMPANY;		
		$this->list_company = $this->m_company->datalist();
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_ncqr_component_assign");
	}
	
	public function create(){ 
		
		$ID_PLANT = $this->input->post("ID_PLANT");
		$ID_GROUPAREA = $this->input->post("ID_GROUPAREA");
		$DISPLAY = $this->input->post("DISPLAY");
		
		IF($ID_GROUPAREA){
			//clean
			$this->c_ncqr_component->clean($ID_PLANT,$ID_GROUPAREA,$this->input->post("OPT_COMPONENT"));
			
			foreach($this->input->post("OPT_COMPONENT") as $ID_COMPONENT){
				//unset data
				$this->c_ncqr_component->clean_single($ID_PLANT,$ID_GROUPAREA,$ID_COMPONENT);
				
				$data = false;
				$data['ID_PLANT'] 		= $ID_PLANT;
				$data['ID_GROUPAREA'] 	= $ID_GROUPAREA;
				$data['ID_COMPONENT'] 	= $ID_COMPONENT;
				@$this->c_ncqr_component->insert($data);# echo $this->c_ncqr_component->get_sql();
			}
			$this->notice->success("Component Assignment Success");
		}
		redirect("ncqr_component/edit/".$ID_PLANT."/".$ID_GROUPAREA);
	}
	
	public function edit($ID_PLANT=NULL,$ID_GROUPAREA=NULL){ #die("edit");
		$this->load->model("m_component");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant 	= $this->m_plant->datalist($this->data_plant->ID_COMPANY);
		$this->list_grouparea = $this->c_ncqr_component->ncqr_grouparea();
		$this->list_component = $this->m_component->datalist();
		
		$this->template->adminlte("v_ncqr_component_assign");
	}
	
	public function view($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){
		$this->load->model("m_component");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist($this->data_plant->ID_COMPANY);
		$this->list_grouparea = $this->c_ncqr_component->list_grouparea($ID_PLANT);
		$this->list_component = $this->m_component->datalist();
		
		$this->template->adminlte("v_ncqr_component_view");
	}
	
	// ajax
	public function list_config_grouparea($ID_PLANT=NULL){
		$data = $this->c_ncqr_component->list_grouparea($ID_PLANT);  
		echo json_encode($data);
	}
	
	public function async_list_grouparea($ID_PLANT=NULL){
		$data = $this->c_ncqr_component->list_grouparea($ID_PLANT);
		echo json_encode($data);
	}
	
	public function async_configuration($ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$data = $this->c_ncqr_component->configuration($ID_PLANT,$ID_GROUPAREA);# echo $this->c_ncqr_component->get_sql();
		echo json_encode($data);
	}
	
	public function ncqr_grouparea(){
		$data = $this->c_ncqr_component->ncqr_grouparea();
		echo json_encode($data);
	}
}

