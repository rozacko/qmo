<?php

class M_Menu extends DB_QM 
{
    private $post = array();
    private $table = "M_MENU";
    private $pKey = "ID_MENU";
    private $column_order = array('ID_MENU'); //set column for datatable order
    private $column_search = array('a.NM_MENU', 'a.URL_MENU', 'b.NM_GROUPMENU'); //set column for datatable search
    private $order = array("b.NO_ORDER" => 'ASC'); //default order

    public function __construct()
    {
        $this->post = $this->input->post();
    }

    public function datalist() 
    {
        $this->db->select("a.*, b.NM_GROUPMENU");
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU", "LEFT");
        $this->db->where("a.APP", $this->APP);
        $this->db->order_by("b.NO_ORDER");
        $this->db->order_by("a.NO_ORDER");
        return $this->db->get("M_MENU a")->result();
    }

    public function datalist2()   
    {
        $this->db->select("a.*, b.NM_GROUPMENU, c.NM_MENU AS NM_PARENT");
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU", "LEFT");
        $this->db->join("M_MENU c", "a.PARENT=c.ID_MENU AND c.IS_SUBMENU = 'N'", "LEFT");
        $this->db->where("a.APP", $this->APP);
        // $this->db->where("(SELECT COUNT(ID_MENU) FROM M_MENU WHERE PARENT = a.ID_MENU) = 0");
        $this->db->order_by("b.NO_ORDER");
        // $this->db->order_by("a.PARENT DESC");
        $this->db->order_by("c.NO_ORDER");
        $this->db->order_by("a.NO_ORDER");
        $a =$this->db->get("M_MENU a")->result();
        // echo $this->db->last_query();
        return $a; 
    } 

    public function search(&$keyword)
    {
        $this->db->like("Nm_menu", $keyword);
        return $this->db->get("m_menu")->result();
    }

    public function data($where)
    {
        $this->db->where($where);
        $this->db->where("APP", $this->APP);
        return $this->db->get("m_menu")->row();
    }
	public function datachild($where)
    {
        $this->db->where($where);
        $this->db->select('ID_MENU');
        return $this->db->get("m_menu")->result_array();
    }

    public function get_data_by_id($ID_MENU)
    {
        $this->db->where("ID_MENU", $ID_MENU);
        return $this->db->get("M_MENU")->row();
    }

    public function data_except_id($where, $skip_id)
    {
        $this->db->where("ID_MENU !=", $skip_id);
        $this->db->where($where);
        return $this->db->get("M_MENU")->row();
    }

    public function insert($data)
    {
        if($data['ACTIVE'] == 'on'){
            $data['ACTIVE'] = '1';
        }

        $this->db->set($data);
        $this->db->set("ID_MENU", "SEQ_ID_MENU.NEXTVAL", false);
        $this->db->set("APP", $this->APP);
        $this->db->insert("M_MENU");
    }

    public function update($data, $ID_MENU)
    {
        if($data['ACTIVE'] == 'on'){
            $data['ACTIVE'] = '1';
        }
        if($data['PARENT'] == null){
            $data['IS_SUBMENU'] = 'N';
        }else{
            $data['IS_SUBMENU'] = 'Y';
        }
        $this->db->set($data);
        $this->db->where("ID_MENU", $ID_MENU);
        $this->db->update("M_MENU");
    }

    public function delete($ID_MENU)
    {
        $this->db->where("ID_MENU", $ID_MENU);
        $this->db->delete("M_MENU");
    }

    public function list_active($ID_USER = null)
    {
        $this->db->distinct();
        $this->db->select("a.ID_MENU,a.NM_MENU,a.URL_MENU,a.NO_ORDER,a.ID_GROUPMENU,a.APP,b.NM_GROUPMENU,b.NO_ORDER, b.ICON");
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU");
        $this->db->join("M_AUTHORITY c", "a.ID_MENU=c.ID_MENU");
        $this->db->join("M_USERGROUP d", "c.ID_USERGROUP=d.ID_USERGROUP");

        //----------------------------------------------------------
        $this->db->join("M_ROLES e", "e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='" . $ID_USER . "'");
        // $this->db->join("M_USERS e","e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='".$ID_USER."'");
        //----------------------------------------------------------

        $this->db->order_by("b.NO_ORDER");
        $this->db->order_by("a.NO_ORDER");
        $this->db->order_by("a.NM_MENU");
        $this->db->order_by("a.ID_MENU");
        $this->db->order_by("a.URL_MENU");
        $this->db->order_by("a.ID_GROUPMENU");
        $this->db->order_by("a.APP");
        $this->db->order_by("b.NM_GROUPMENU");
        $this->db->where("a.ACTIVE", "1");
        $this->db->where("a.APP", $this->APP);
        return $this->db->get("M_MENU a")->result();
        // echo $this->db->last_query();

    }

    public function get_query($id_groupmenu = null, $id_parent = null)
    {
        $this->db->select("a.*, b.NM_GROUPMENU");
        $this->db->from($this->table . ' a');
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU", "LEFT");
        $this->db->where("a.APP", $this->APP);
        if ($this->post['groupmenu']) {
            $this->db->where('b.ID_GROUPMENU', $this->post['groupmenu']);
            $this->db->where('a.IS_SUBMENU', 'N');
        }
        if ($this->post['parent']) {
            $this->db->where('a.PARENT', $this->post['parent']);
            $this->db->where('a.IS_SUBMENU', 'Y');
        }
        if ($id_groupmenu) {
            $this->db->where('b.ID_GROUPMENU', $id_groupmenu);
            $this->db->where('a.IS_SUBMENU', 'N');
        }
        if ($id_parent) {
            $this->db->where('a.PARENT', $id_parent);
        }
        $this->db->order_by("b.NO_ORDER");
        $this->db->order_by("a.NO_ORDER");
        #echo $this->db->get_compiled_select();exit();
    }

    public function get_list($id_groupmenu = null, $id_parent = null)
    {
        $this->get_query($id_groupmenu, $id_parent);
        $i = 0;

        //Loop column search
        foreach ($this->column_search as $item) {
            if ($this->post['search']['value']) {
                if ($i === 0) { //first loop
                    $this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
                } else {
                    $this->db->or_like($item, strtoupper($this->post['search']['value']));
                }

                if (count($this->column_search) - 1 == $i) { //last loop
                    $this->db->group_end(); //close bracket
                }
            }
            $i++;
        }

        if (isset($this->post['order'])) { //order datatable
            $this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
            #echo $this->db->get_compiled_select();exit();
        } elseif (isset($this->order)) {
            $this->db->order_by(key($this->order), $this->order[key($this->order)]);
        }

        if ($this->post['length'] != -1) {
            $this->db->limit($this->post['length'], $this->post['start']);
            $query = $this->db->get();
        } else {
            $query = $this->db->get();
        }

        return $query->result();
    }

    /** Count query result after filtered **/
    public function count_filtered($id_groupmenu = null, $id_parent = null)
    {
        $this->get_query($id_groupmenu, $id_parent);
        $query = $this->db->get();
        return (int)$query->num_rows();
    }

    /** Count all result **/
    public function count_all($id_groupmenu = null)
    {
        $this->get_query($id_groupmenu, $id_parent);
        $query = $this->db->get();
        return (int)$query->num_rows();
    }

    public function getParent($ID_USER = null)
    {
        $this->db->distinct();
        $this->db->select("b.ID_GROUPMENU, b.NM_GROUPMENU,b.NO_ORDER, b.ICON");
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU");
        $this->db->join("M_AUTHORITY c", "a.ID_MENU=c.ID_MENU");
        $this->db->join("M_USERGROUP d", "c.ID_USERGROUP=d.ID_USERGROUP");

        //----------------------------------------------------------
        $this->db->join("M_ROLES e", "e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='" . $ID_USER . "'");
        // $this->db->join("M_USERS e","e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='".$ID_USER."'");
        //----------------------------------------------------------

        $this->db->order_by("b.NO_ORDER");
        $this->db->order_by("b.NM_GROUPMENU");
        $this->db->where("a.ACTIVE", "1");
        $this->db->where("a.APP", $this->APP);
        return $this->db->get("M_MENU a")->result_array();
        // echo $this->db->last_query();

    }
 
    public function getSubMenu($ID_USER = null, $ID_GROUPMENU = null, $ID_PARENT = null)
    {
        $this->db->distinct();
        $this->db->select("a.ID_MENU,a.NM_MENU,a.URL_MENU,a.NO_ORDER,a.ID_GROUPMENU,a.APP,b.NM_GROUPMENU,b.NO_ORDER, a.ICON, a.PARENT, a.IS_SUBMENU, c.READ, c.WRITE");
        $this->db->join("M_GROUPMENU b", "a.ID_GROUPMENU=b.ID_GROUPMENU");
        $this->db->join("M_AUTHORITY c", "a.ID_MENU=c.ID_MENU");
        $this->db->join("M_USERGROUP d", "c.ID_USERGROUP=d.ID_USERGROUP");

        //----------------------------------------------------------
        $this->db->join("M_ROLES e", "e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='" . $ID_USER . "'");
        // $this->db->join("M_USERS e","e.ID_USERGROUP=d.ID_USERGROUP and e.ID_USER='".$ID_USER."'");
        //----------------------------------------------------------

        $this->db->order_by("b.NO_ORDER");
        $this->db->order_by("a.NO_ORDER");
        $this->db->order_by("a.NM_MENU");
        $this->db->order_by("a.ID_MENU");
        $this->db->order_by("a.URL_MENU");
        $this->db->order_by("a.ID_GROUPMENU");
        $this->db->order_by("a.APP");
        $this->db->order_by("b.NM_GROUPMENU");
        $this->db->order_by("a.IS_SUBMENU");
        $this->db->order_by("c.READ");
        $this->db->order_by("c.WRITE");
        $this->db->where("a.ACTIVE", "1");
        $this->db->where("a.APP", $this->APP);
        $this->db->where("b.ID_GROUPMENU", $ID_GROUPMENU);
        if ($ID_PARENT) {
            $this->db->where("a.PARENT", $ID_PARENT);
        }
        $a = $this->db->get("M_MENU a")->result_array();
        // if ($ID_PARENT) {
        //     echo $this->db->last_query();
        // }
        return $a;

    }

}
