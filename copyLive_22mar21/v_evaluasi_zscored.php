<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Evaluasi Zscored Uji Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header with-border">
                <b>Periode Proficiency: </b> <?= $this->proficiency->TITLE_PP;?> [<?= $this->proficiency->GROUP_PP;?> - <?= $this->proficiency->YEAR_PP;?>] | <b>Commodity: </b> <?= $this->proficiency->NAMA_SAMPLE; ?> | <b>Lab: </b> <?= $this->proficiency->NAMA_LAB; ?>
                </div>
				<div class="box-body">
                    <div class="row">
                        <div id="dt_zscored"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    $("document").ready(function(){
        zscored();
    });

    function zscored(){
        $.ajax({
            processData: false,
            contentType: false,
			url : "<?= base_url('evaluasi_proficiency/count_zscored').'?proficiency='.$this->input->get('proficiency')."&komoditi=".$this->input->get('komoditi') ?>",
			type: "GET",
		}).done(function(response){
            var dt_z = "";
            var i =0;
            response.forEach(function(item, index){
                
                dt_z += "<div class='col-md-6'>";
                dt_z += "<table class='table table-bordered'>";
                dt_z += "<thead>";
                dt_z += "<tr>";
                dt_z += "<th rowspan='2' style='text-align:center;'>KODE LAB</th>";
                dt_z += "<th colspan='2' style='text-align:center;'>"+item.parameter+"</th>";
                dt_z += "<th rowspan='2' style='text-align:center;vertical-align:middle;'>Si</th>";
                dt_z += "<th rowspan='2' style='text-align:center;vertical-align:middle;'>Di</th>";
                dt_z += "<th colspan='2' style='text-align:center;'>Zscore</th>";
                dt_z += "</tr>";
                dt_z += "<tr>";
                dt_z += "<th style='text-align:center;'>UJI 1</th>";
                dt_z += "<th style='text-align:center;'>UJI 2</th>";
                dt_z += "<th style='text-align:center;'>Zbi</th>";
                dt_z += "<th style='text-align:center;'>Status</th>";
                dt_z += "</tr>";
                dt_z += "</thead>";
                var datas = item.data;
                datas.forEach(function(item, index){
                    dt_z += "<tr>";
                    dt_z += "<td style='text-align:center;'>"+item['KODE_LAB']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['UJI_1']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['UJI_2']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['SI']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['DI']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['ZBI']+"</td>";
                    dt_z += "<td style='text-align:center;'>"+item['STATUS']+"</td>";
                    dt_z += "</tr>";
                });
                dt_z += "</table>"
                dt_z += "</div>";
                dt_z += "<div id='chart_zscored"+i+"' class='col-md-6'></div>&nbsp;<hr/>&nbsp;";
                i++;
            });
            
            $("#dt_zscored").html(dt_z);

            response.forEach(function(item, index){
                Highcharts.chart('chart_zscored'+index, {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Zscore '+item.parameter
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: item.lab,
                        crosshair: true
                    },
                    yAxis: {
                        min: -4,
                        title: {
                            text: ''
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        name: 'Zscore',
                        data: item.zscore
                    }]
                });
            });
            
		});
  
    }
</script>