   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Data Sample Product
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">

          <div class="box" id="formArea" style="display: none;">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Sample Product</h3>
              </div>

              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>

              <div class="box-body">
                  <div class="form-group clearfix">
                    <div class="col-sm-4 ">
                      <label>NAME COMPETITOR</label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control hidden" name="ID_COMPETITOR" id="ID_COMPETITOR" placeholder="Id Competitor" required >
                      <input type="text" class="form-control" name="NAME_COMPETITOR" id="NAME_COMPETITOR" placeholder="Name Competitor" required readonly="true">
                    </div>

                    <div class="col-sm-2 ">
                      <label>INITIAL </label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control" name="INITIAL_COMPETITOR" id="INITIAL_COMPETITOR" placeholder="Initial Competitor" required readonly="true">
                    </div>

                    <div class="col-sm-4 ">
                      <label>MERK </label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control" name="MERK_PRODUCT" id="MERK_PRODUCT" placeholder="Merk Product" required readonly="true">
                    </div>
                    <div class="col-sm-2 ">
                      <label>GROUP </label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control" name="GROUP" id="GROUP" placeholder="Group" required readonly="true">
                    </div>


                  </div>

                  <div class="form-group clearfix">  

					<div class="box box-danger direct-chat direct-chat-danger">
						<div class="box-header with-border">
							<h3 class="box-title">Type Product <span  style="color:red; float: right; margin-left: 20px;">(*)</span></h3>

							<div class="box-tools pull-right">
							</div>
						</div>
					<!-- /.box-header -->
						<div class="box-body" style="">
						<!-- Conversations are loaded here -->
						<form id="checklistallproduct">
							<div class="form-group col-sm-3 col-md-3 col-lg-3" id="typeproduk1">
								
							</div>
							<div class="form-group col-sm-3 col-md-3 col-lg-3" id="typeproduk2">
								
							</div>
							<div class="form-group col-sm-3 col-md-3 col-lg-3" id="typeproduk3">
								
							</div>
							<div class="form-group col-sm-3 col-md-3 col-lg-3" id="typeproduk4">
								
							</div>
						</form>
						<!-- /.direct-chat-pane -->
						</div>
					<!-- /.box-body -->
						<div class="box-footer" style="">

						</div>
					<!-- /.box-footer-->
					</div>

                  </div>

              </div>

              <div class="box-footer">
                <button type="button" id="AddArea" class="btn btn-primary"><i class="fa fa-save"></i>  &nbsp;Save</button>
                <button type="button" id="UpdateArea" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-default btnCancel">Cancel</button>
              </div>
            </div>
          </div>
    
          <div class="box" id="viewArea">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Data Sample Product (<i>scm.semenindonesia.com</i>)</h3>
                <div class="input-group input-group-sm" style="width: 150px; float: right;display: none;">
                  <a id="AddNew" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">COMPANY CODE</th>
                        <th width="*">COMPANY NAME</th>
                        <th width="*">INISIAL</th>
                        <th width="*">MERK PRODUCT</th>
                        <th width="*">GROUP</th>
                        <th width="5%">TYPE PRODUCT</th>
                        <th width="5%">ACTION</th>
                      </tr>
                    </thead>
                  </table>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>

          <div class="box" id="viewProductList" style="display: none;">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Data Sample Product</h3>
                <div class="input-group input-group-sm" style="width: 150px; float: right;">
                  <a id="AddNewProduct" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadDataProduct" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables_product" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th width="5%">COMPANY CODE</th>
                      <th width="*">COMPANY NAME</th>
                      <th width="*">INISIAL</th>
                      <th width="*">MERK PRODUCT</th>
                      <th width="*">TYPE PRODUCT</th>
                      <th width="5%">GROUP</th>
                      <th width="5%">ACTION</th>
                    </tr>
                  </thead>
                </table>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>

          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>

$(document).ready(function(){


  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable;

  var glbtypeproduct;

  reloadtabel();   
  loadTypeProductList(); 

  $(document).on('click',"#AddNew",function () {
    $('#saving').show();

    $('#formArea').show();
    $('#viewArea').hide();

    $('#UpdateArea').hide();
    $('#AddArea').show();

    $('#ID_COMPETITOR').val('');
    $('#NAME_COMPETITOR').val('');
    $('#MERK_PRODUCT').val('');
    $('#TYPE_PRODUCT').val('');
    $('#INITIAL_COMPETITOR').val('');
    $('#ID_COMPANY').val('');
    $('#is_smig').prop('checked', false);

    $('#saving').hide();

  });

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });

  $(document).on('click',"#ReloadDataProduct",function () {
    reloadtabelProduct();
  });

  $(document).on('click',"#AddArea",function () {
    $('#saving').show();
    var issmig = 0;
    var componentceklist = document.getElementsByName("typeproduk[]");
    for (var i=0; i < componentceklist.length; i++) {
      if (componentceklist[i].checked == true) {
        issmig = 1;  
      }
    }
    if ($('#NAME_COMPETITOR').val() != '' && $('#MERK_PRODUCT').val() != '' && $('#TYPE_PRODUCT').val() != '' && $('#INITIAL_COMPETITOR').val() != '' ) {
      $.ajax({
        url: "<?php echo site_url("sample_product/save_product");?>",
        data: {"NAME_COMPETITOR": $('#NAME_COMPETITOR').val(), "MERK_PRODUCT": $('#MERK_PRODUCT').val(), "TYPE_PRODUCT": $('#TYPE_PRODUCT').val(), "INITIAL_COMPETITOR": $('#INITIAL_COMPETITOR').val(), "ID_COMPANY": $('#ID_COMPANY').val(), "IS_SMIG": issmig, "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }              
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
      $("#a-notice-error").click();
      if ($('#NAME_COMPETITOR').val() == '') {
        $('#NAME_COMPETITOR').focus();
      } else if ($('#MERK_PRODUCT').val() == '') {
        $('#MERK_PRODUCT').focus();
      } else if ($('#TYPE_PRODUCT').val() == '') {
        $('#TYPE_PRODUCT').focus();
      } else if ($('#INITIAL_COMPETITOR').val() == '') {
        $('#INITIAL_COMPETITOR').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',"#UpdateArea",function () {
    $('#saving').show();
    var tyeproductlist = new Array();
    var componentceklist = document.getElementsByName("typeproduk[]");
    for (var i=0; i < componentceklist.length; i++) {
      if (componentceklist[i].checked == true) {
        tyeproductlist.push(componentceklist[i].value);
      }
    }

    if ($('#ID_COMPETITOR').val() != '' && tyeproductlist ) {
      $.ajax({
        url: "<?php echo site_url("sample_product/update_product_type");?>",
        data: {"KODE_PERUSAHAAN": $('#ID_COMPETITOR').val(), "TYPE_PRODUCT": JSON.stringify(tyeproductlist), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });      
    } else {
      $("#a-notice-error").click();
      if ($('#ID_COMPETITOR').val() == '') {
        $('#ID_COMPETITOR').focus();
      } else if ($('#TYPE_PRODUCT').val() == '') {
        $('#TYPE_PRODUCT').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',".btnCancel",function () {
    $('#saving').show();
    $('#formArea').hide();
    $('#viewArea').show();

    $('#ID_COMPETITOR').val('');
    $('#NAME_COMPETITOR').val('');
    $('#NAME_PRODUCT').val('');
    $('#INITIAL_COMPETITOR').val('');
    $('#ID_COMPANY').val('');
    $('#is_smig').prop('checked', false);
    reloadtabel();
    $('#saving').hide();      
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btSetTypeProduct",function () {
    $('#saving').show();
    $('#formArea').show();
    $('#viewArea').hide();  

    $("#checklistallproduct")[0].reset();

    $('#UpdateArea').show();
    $('#AddArea').hide(); 

    var data = oTable.row($(this).parents('tr')).data();

    if (parseInt(data['ID_COMPETITOR']) == 1) {
      $('#is_smig').prop('checked', true); // Checks it
      $('#ID_COMPANY').val(data['ID_COMPANY']);
    } else {
      $('#is_smig').prop('checked', false); // Unchecks it
      $('#ID_COMPANY').val('');
    }

    $('#ID_COMPETITOR').val(data['KODE_PERUSAHAAN']);
    $('#NAME_COMPETITOR').val(data['NAMA_PERUSAHAAN']);
    $('#INITIAL_COMPETITOR').val(data['INISIAL']);
    $('#MERK_PRODUCT').val(data['PRODUK']);

    var checklistselected = data['TYPE_PRODUCT'];

    if (checklistselected) {
    	for (keyproduct in checklistselected) {
    		$('#'+keyproduct).prop('checked', true);
    	}
    }

    $('#TYPE_PRODUCT').val(data['ID_PRODUCT']);

	  $('#GROUP').val(data['KELOMPOK']);
    $('#saving').hide();
  });

  /** btDelete Click **/
  $(document).on('click',".btDelete",function () {
    var data = oTable.row($(this).parents('tr')).data();
    $.confirm({
        title: "Remove Component",
        text: "This sample area will be removed. Are you sure?",
        confirmButton: "Remove",
        confirmButtonClass: "btn-danger",
        cancelButton: "Cancel",
        confirm: function() {
          $.ajax({
            url: "<?php echo site_url("sample_product/delete_product");?>",
            data: {"ID_COMPETITOR": data['ID_COMPETITOR'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
              if (res['status'] == true) {
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
              } else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              reloadtabel();            
            },
            error: function () {
              $("#a-notice-error").click();
            }
          });
        },
        cancel: function() {
        }
    });
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("sample_product/product_list_scm");?>',
        type: "POST"
      },
      columns: [
        {"data": "KODE_PERUSAHAAN", "width": 50},
        {"data": "NAMA_PERUSAHAAN"},
        {"data": "INISIAL"},
        {"data": "PRODUK"},
        
        {"data": "KELOMPOK", "width": 100,
          "mRender": function(row, data, index){
            if (row == 'SMI') {
              return 'SMI GROUP';
            } else {              
              return 'NON SMIG';
            }

          }
        },

        {"data": "TYPE_PRODUCT", "width": 100,
          "mRender": function(row, data, index){
            if (row) {
              	var checklistproduct = '';
	            for (codeproduct in row) {
	            	checklistproduct += '<label> <input type="checkbox" disabled checked > '+codeproduct+' </label> <br> ';
	            } 

	            return checklistproduct;

            } else {     

              return 'NOT SET YET';

            }

          }
        },
        {"data": "KODE_PERUSAHAAN", "width": 100,
          "mRender": function(row, data, index){
            return '<button title="Set Type Product" class="btSetTypeProduct btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Set Type Product</button>';
          }
        },
      ]
    });

    $('#ReloadData').show();
  }

  function reloadtabelProduct() {
    // body...
    $('#ReloadDataProduct').hide();
    $('#AddNewProduct').hide();
    oTable = $('#dt_tables_product').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("sample_product/product_list_scm");?>',
        type: "POST"
      },
      columns: [
        {"data": "KODE_PERUSAHAAN", "width": 50},
        {"data": "NAMA_PERUSAHAAN"},
        {"data": "INISIAL"},
        {"data": "PRODUK"},
        {"data": "KD_PRODUCT", "width": 100,
          "mRender": function(row, data, index){
            if (row) {
              return row;
            } else {              
              return 'NOT SET YET';
            }

          }
        },
        {"data": "KELOMPOK", "width": 100,
          "mRender": function(row, data, index){
            if (row == 'SMI') {
              return 'SMI GROUP';
            } else {              
              return 'NON SMIG';
            }

          }
        },
        {"data": "KODE_PERUSAHAAN", "width": 100,
          "mRender": function(row, data, index){
            return '<button title="Set Type Product" class="btSetTypeProduct btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Set Type Product</button>';
          }
        },
      ]
    });
  
    $('#ReloadDataProduct').show();
    $('#AddNewProduct').show();
  }

  	function loadTypeProductList() {
	  var typeproduct = $('#TYPE_PRODUCT');
	  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product/all");?>', function (result) {
	    var values = result;

	    glbtypeproduct = result;

      	$('#typeproduk1').html('');
      	$('#typeproduk2').html('');
      	$('#typeproduk3').html('');
      	$('#typeproduk4').html('');


	    typeproduct.find('option').remove();
	    if (values != undefined && values.length > 0) {
			typeproduct.css("display","");
			$(values).each(function(index, element) {
				typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
			});

			for (var i = 0; i < values.length; i++) {

				var checklistelement = '<div class="checkbox">'
									+'<label>'
										+'<input type="checkbox" id="'+values[i]['KD_PRODUCT']+'" name="typeproduk[]" value="'+values[i]['KD_PRODUCT']+'">'+values[i]['KD_PRODUCT']+''
									+'</label>'
								+'</div>';

				if (i % 4 == 0) {
					document.getElementById('typeproduk1').innerHTML += checklistelement;
				} else if (i % 4 == 1) {
					document.getElementById('typeproduk2').innerHTML += checklistelement;
				} else if (i % 4 == 2) {
					document.getElementById('typeproduk3').innerHTML += checklistelement;
				} else if (i % 4 == 3) {
					document.getElementById('typeproduk4').innerHTML += checklistelement;
				}
			}

	    }else{
			typeproduct.find('option').remove();
			typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
	    }
	  });
	}

  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
