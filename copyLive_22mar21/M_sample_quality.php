<?php

class M_sample_quality Extends DB_QM {
	private $post  = array();
	private $table = "M_GROUPAREA";
	private $pKey  = "ID_GROUPAREA";
	private $column_order = array(NULL, 'ID_GROUPAREA'); //set column for datatable order
	private $column_search = array('NM_GROUPAREA', 'KD_GROUPAREA'); //set column for datatable search
	private $order = array("ID_GROUPAREA" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	public function datalist(){
		$this->db->order_by("a.ID_GROUPAREA");
		return $this->db->get("M_GROUPAREA a")->result();
	}

	public function samplearealist_scm(){
		$this->scmdb->order_by("a.NM_KOTA");
		return $this->scmdb->get("ZREPORT_M_KOTA a")->result();
	}

	public function sample_perusahaan_scm(){
	    $this->scmdb->where("a.KELOMPOK", 'SMI');
		$this->scmdb->order_by("a.NAMA_PERUSAHAAN");
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->result_array();
	}

	public function samplearealist(){
		$this->db->order_by("a.ID_SAMPLE_AREA");
		return $this->db->get("O_SAMPLE_AREA a")->result();
	}

	public function get_sample_area($key){
	    $this->db->select("a.*");
	    $this->db->where("LOWER(a.NAME_AREA)", strtolower($key));
		return $this->db->get("O_SAMPLE_AREA a")->row_array();
	}

	public function get_sample_area_scm($key){
	    $this->scmdb->select("a.*");
	    $this->scmdb->where("LOWER(a.NM_KOTA)", strtolower($key));
		return $this->scmdb->get("ZREPORT_M_KOTA a")->row_array();
	}

	public function get_competitor($key){
	    $this->db->select("a.*");
	    $this->db->where("LOWER(a.MERK_PRODUCT)", strtolower($key));
		return $this->db->get("O_COMPETITOR a")->row_array();
	}

	public function get_competitor_scm($key){
	    $this->scmdb->where("LOWER(a.PRODUK)", strtolower($key));
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->row_array();
	}

	public function get_competitor_detail_scm($key){
	    $this->scmdb->where("a.KODE_PERUSAHAAN", $key);
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->row_array();
	}

	public function type_productlist($all){

		if (!$all) {
			# code...			
			$this->db->where("LOWER(a.KD_PRODUCT)", 'ppc');
			$this->db->or_where("LOWER(a.KD_PRODUCT)", 'opc');
			$this->db->or_where("LOWER(a.KD_PRODUCT)", 'pcc');
		}

		$this->db->order_by("a.ID_PRODUCT");
		return $this->db->get("M_PRODUCT a")->result_array();
	}

	public function get_tye_product($key){
	    $this->db->where("LOWER(a.KD_PRODUCT)", strtolower($key));
		$this->db->order_by("a.ID_PRODUCT");
		return $this->db->get("M_PRODUCT a")->row_array();
	}

	public function type_produk_scm($key){
	    $this->db->select("B.*");
		$this->db->join("M_PRODUCT B","A.TYPE_PRODUCT = B.ID_PRODUCT");
	    $this->db->where("A.KODE_PERUSAHAAN", $key);
		return $this->db->get("O_TYPE_PRODUCT_SCM A")->result_array();
	}

	public function merklistb(){
		$this->db->order_by("a.ID_COMPETITOR");
		return $this->db->get("O_COMPETITOR a")->result();
	}


	public function merklist(){
		$this->db->select("A.*, B.KD_PRODUCT, B.NM_PRODUCT");
	    $this->db->join("M_PRODUCT B","A.TYPE_PRODUCT = B.ID_PRODUCT");
		return $this->db->get("O_COMPETITOR a")->result();
	}

	public function merklist_scm(){
		$this->scmdb->order_by("a.NAMA_PERUSAHAAN");
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->result();
	}

	public function merklist_scmn(){
		$this->scmdb->order_by("a.NAMA_PERUSAHAAN");
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->result_array();
	}

	public function componentlist(){
		$this->db->order_by("a.ID_COMPONENT");
		return $this->db->get("M_COMPONENT a")->result();
	}

	public function dataQM($tanggal, $typeproduct){
		# code...
		$this->db->select("B.ID_COMPONENT, COALESCE(AVG(B.NILAI),0) AS RERATA");
		$this->db->join("D_CEMENT_DAILY B","A.ID_CEMENT_DAILY = B.ID_CEMENT_DAILY");
		$this->db->join("M_PRODUCT C","C.ID_PRODUCT = A.ID_PRODUCT");
		if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(C.KD_PRODUCT) = 'ppc' OR LOWER(C.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(C.KD_PRODUCT) = 'opc'");
		}


		$startdate = $tanggal['start'];
		$enddate = $tanggal['end'];

		$between = "A.DATE_DATA BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);
		
  		$this->db->group_by("B.ID_COMPONENT, A.ID_PRODUCT, C.KD_PRODUCT");
  		$this->db->order_by("B.ID_COMPONENT");
		return $this->db->get("T_CEMENT_DAILY A")->result_array();
	}
	

  	public function component_checklist(){
		$this->db->select("A .ID_COMPONENT, A .KD_COMPONENT, A .NM_COMPONENT, COALESCE(B.IS_ACTIVE, 0) AS STATUS_CHECKLIST");
		$this->db->join("O_CHECKLIST B","A.ID_COMPONENT = B.ID_COMPONENT", 'left');
  		$this->db->order_by("A.ID_COMPONENT");
		return $this->db->get("M_COMPONENT A")->result_array();
	}

	public function component_checklist_order(){
		$this->db->select("A.ID_COMPONENT,	A.KD_COMPONENT,	A.NM_COMPONENT,	COALESCE (B.IS_ACTIVE, 0) AS STATUS_CHECKLIST,	C.URUTAN");
		$this->db->join("O_CHECKLIST B","A.ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("C_PARAMETER_ORDER C","A.ID_COMPONENT = C.ID_COMPONENT", 'left');
		$this->db->where("C.ID_GROUPAREA", 1);
		$this->db->where("C.DISPLAY", 'D');
		$this->db->where("B.IS_ACTIVE", 1);
  		$this->db->order_by("C.URUTAN,	A.ID_COMPONENT");
		return $this->db->get("M_COMPONENT A")->result_array();
	}

	public function component_min_max($periode, $idcomponent, $typeproduct){
		// $this->db->select("A .ID_COMPONENT, A.V_MIN, A.V_MAX, A .PERIODE, A .FLAG, B .ID_PRODUCT");
		// $this->db->join("M_PRODUCT B","B.ID_PRODUCT = A.ID_PRODUCT");
	 //    $this->db->where("A.ID_COMPONENT", $idcomponent);

	 //    if ( (int) $typeproduct == 0 ) {
		// 	# code...
	 //    	$this->db->where("(LOWER(B.KD_PRODUCT) = 'ppc' OR LOWER(B.KD_PRODUCT) = 'pcc')");
		// } else {
		// 	# code...
	 //    	$this->db->where("LOWER(B.KD_PRODUCT) = 'opc'");
		// }

	 //    $this->db->where("A.FLAG", 1);

	 //    $between = "TO_CHAR(A.PERIODE, 'YYYY-MM-DD') BETWEEN '".$periode['start']."' AND '".$periode['end']."'";
	 //    $this->db->where($between);
  // 		$this->db->order_by("A.PERIODE", "DESC");
		// return $this->db->get("C_RANGE_QAF A")->row_array();

		$min_max_this_periode = $this->periode_min_max($periode, $idcomponent, $typeproduct);
		$min_max_before_periode = $this->min_max_before_periode($periode, $idcomponent, $typeproduct);
		$last_min_max_std = $this->last_min_max($periode, $idcomponent, $typeproduct);

		if ($min_max_this_periode) {
			# code...
			$data = $min_max_this_periode;
		} else if ($min_max_before_periode) {
			# code...
			$data = $min_max_before_periode;
		} else {
			# code...
			$data = $last_min_max_std;
		}		

		return $data;
	}


	public function periode_min_max($periode, $idcomponent, $typeproduct){
		# code...
		$this->db->select("A .ID_COMPONENT, A.V_MIN, A.V_MAX, A .PERIODE, A .FLAG, B .ID_PRODUCT");
		$this->db->join("M_PRODUCT B","B.ID_PRODUCT = A.ID_PRODUCT");
	    $this->db->where("A.ID_COMPONENT", $idcomponent);

	    if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(B.KD_PRODUCT) = 'ppc' OR LOWER(B.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(B.KD_PRODUCT) = 'opc'");
		}

	    $this->db->where("A.FLAG", 1);

	    $between = "TO_CHAR(A.PERIODE, 'YYYY-MM-DD') BETWEEN '".$periode['start']."' AND '".$periode['end']."'";
	    $this->db->where($between);
  		$this->db->order_by("A.PERIODE", "DESC");
		return $this->db->get("C_RANGE_QAF A")->row_array();
	}

	public function min_max_before_periode($periode, $idcomponent, $typeproduct){
		# code...
		$this->db->select("A .ID_COMPONENT, A.V_MIN, A.V_MAX, A .PERIODE, A .FLAG, B .ID_PRODUCT");
		$this->db->join("M_PRODUCT B","B.ID_PRODUCT = A.ID_PRODUCT");
	    $this->db->where("A.ID_COMPONENT", $idcomponent);

	    if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(B.KD_PRODUCT) = 'ppc' OR LOWER(B.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(B.KD_PRODUCT) = 'opc'");
		}

	    // $this->db->where("A.FLAG", 1);

	    $between = "TO_CHAR(A.PERIODE, 'YYYY-MM-DD') < '".$periode['start']."'";
	    $this->db->where($between);
  		$this->db->order_by("A.PERIODE", "DESC");
		return $this->db->get("C_RANGE_QAF A")->row_array();
	}

	public function last_min_max($periode, $idcomponent, $typeproduct){
		# code...
		$this->db->select("A .ID_COMPONENT, A.V_MIN, A.V_MAX, A .PERIODE, A .FLAG, B .ID_PRODUCT");
		$this->db->join("M_PRODUCT B","B.ID_PRODUCT = A.ID_PRODUCT");
	    $this->db->where("A.ID_COMPONENT", $idcomponent);

	    if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(B.KD_PRODUCT) = 'ppc' OR LOWER(B.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(B.KD_PRODUCT) = 'opc'");
		}

	    $this->db->where("A.FLAG", 1);

	    // $between = "TO_CHAR(A.PERIODE, 'YYYY-MM-DD') BETWEEN '".$periode['start']."' AND '".$periode['end']."'";
	    // $this->db->where($between);

  		$this->db->order_by("A.PERIODE", "DESC");
		return $this->db->get("C_RANGE_QAF A")->row_array();
	}

	public function component_precission($periode, $idcomponent, $typeproduct){
		$this->db->select("A.PRECISSION");
		$this->db->where("A.ID_COMPONENT", $idcomponent);
		return $this->db->get("O_PRECISSION_CONFIG A")->row_array();
	}


	public function component_SNI_standard($periode, $idcomponent, $typeproduct){
		$this->db->select("A.MIN_VALUE, A.MAX_VALUE");
		$this->db->join("M_PRODUCT B","B.ID_PRODUCT = A.ID_TYPE_PRODUCT");
	    $this->db->where("A.ID_COMPONENT", $idcomponent);

	    if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("LOWER(B.KD_PRODUCT) = 'pcc'");
		} else {
			# code...
	    	$this->db->where("LOWER(B.KD_PRODUCT) = 'opc'");
		}

  		$this->db->order_by("A.CREATE_DATE", "DESC");
		return $this->db->get("O_SNI_STANDARD_COMP A")->row_array();
	}

	public function sample_exists($data, $tgl){
		$this->db->where("DATE_COLLECT","to_date('".$tgl."','YYYY-MM-DD')",FALSE);
	    $this->db->where("ID_COMPANY",$data['ID_COMPANY']);
	    $this->db->where("SAMPLE_AREA_NAME",$data['SAMPLE_AREA_NAME']);
	    $this->db->where("COMPETITOR_MERK_PRODUCT",$data['COMPETITOR_MERK_PRODUCT']);
		$this->db->order_by("ID_COMPONENT");
		return $this->db->get("O_SAMPLE_QUALITY")->result_array();
	}

	public function sample_insert($data, $tgl){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE","to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')",FALSE);
		$this->db->set("DATE_COLLECT","to_date('".$tgl."','YYYY-MM-DD')",FALSE);
		$query = $this->db->insert("O_SAMPLE_QUALITY");
	    if($query == true){
	      return true;
	    }else{
	      return false;
	    }
	}

	public function is_sample_exists($data, $tgl){
		$this->db->where("DATE_COLLECT","to_date('".$tgl."','YYYY-MM-DD')",FALSE);
		$this->db->where("ID_COMPONENT",$data['ID_COMPONENT']);
	    $this->db->where("ID_COMPANY",$data['ID_COMPANY']);
	    $this->db->where("ID_SAMPLE_AREA",$data['ID_SAMPLE_AREA']);
	    $this->db->where("ID_COMPETITOR",$data['ID_COMPETITOR']);
	    $this->db->where("ID_TYPE_PRODUCT",$data['ID_TYPE_PRODUCT']);
		return $this->db->get("O_SAMPLE_QUALITY")->num_rows();
	}

	public function sample_update($data, $tgl){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("COMPONENT_VALUE", $data['COMPONENT_VALUE']);
		// $this->db->set("ID_TYPE_PRODUCT", $data['ID_TYPE_PRODUCT']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')",FALSE);
		$this->db->set("MODIFIED_BY", $data['CREATE_BY']);
		$this->db->where("DATE_COLLECT", "to_date('".$tgl."','YYYY-MM-DD')",FALSE);
		$this->db->where("ID_SAMPLE_AREA", $data['ID_SAMPLE_AREA']);
		$this->db->where("ID_COMPETITOR", $data['ID_COMPETITOR']);
		$this->db->where("ID_COMPONENT", $data['ID_COMPONENT']);
		$this->db->where("ID_COMPANY", $data['ID_COMPANY']);
	    $this->db->where("ID_TYPE_PRODUCT",$data['ID_TYPE_PRODUCT']);
		$query = $this->db->update("O_SAMPLE_QUALITY");
	    if($query == true){
	      return true;
	    }else{
	      return false;
	    }
	}

	public function sample_delete_exist($data, $tgl){
		$this->db->where("DATE_COLLECT", "to_date('".$tgl."','YYYY-MM-DD')",FALSE);
		$this->db->where("ID_SAMPLE_AREA", $data['ID_SAMPLE_AREA']);
		$this->db->where("ID_COMPETITOR", $data['ID_COMPETITOR']);
		$this->db->where("ID_COMPONENT", $data['ID_COMPONENT']);
		$this->db->where("ID_COMPANY", $data['ID_COMPANY']);
	    $this->db->where("ID_TYPE_PRODUCT",$data['ID_TYPE_PRODUCT']);
		$query = $this->db->delete("O_SAMPLE_QUALITY");
	    if($query == true){
	      return true;
	    }else{
	      return false;
	    }
	}

	public function get_data_sample_group($typeproduct, $companyid, $startdate, $enddate){
		$this->db->select("DISTINCT A.DATE_COLLECT, A.ID_COMPANY, A.ID_SAMPLE_AREA, A.ID_COMPETITOR, B.NAME_AREA, B.NAME_PROVINCE, B.NAME_ISLAND, B.NAME_REGION, C.NAME_COMPETITOR, C.MERK_PRODUCT, D.NM_PRODUCT, D.KD_PRODUCT");
		$this->db->join("O_SAMPLE_AREA B", "A.ID_SAMPLE_AREA = B.ID_SAMPLE_AREA");
		$this->db->join("O_COMPETITOR C", "A.ID_COMPETITOR = C.ID_COMPETITOR");
		$this->db->join("M_PRODUCT D", "C.TYPE_PRODUCT = D.ID_PRODUCT");
		$this->db->where("A.ID_COMPANY", $companyid);
		$this->db->where("C.TYPE_PRODUCT", $typeproduct);
		$between = "A.DATE_COLLECT BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);
		$this->db->order_by("A.DATE_COLLECT");
		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}

	public function get_data_sample_group_scm($typeproduct, $companyid, $startdate, $enddate){
		$this->db->select("DISTINCT A.DATE_COLLECT, A.ID_COMPANY, A.ID_SAMPLE_AREA, A.ID_COMPETITOR, A.ID_TYPE_PRODUCT, D.NM_PRODUCT, D.KD_PRODUCT");

		$this->db->join("M_PRODUCT D", "A.ID_TYPE_PRODUCT = D.ID_PRODUCT");
		$this->db->where("A.ID_COMPANY", $companyid);
		$this->db->where_in("A.ID_TYPE_PRODUCT", $typeproduct);
		$between = "A.DATE_COLLECT BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);
		$this->db->order_by("A.DATE_COLLECT");
		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}


	public function get_data_sample_list($data, $id_component, $component_name, $tgl){
		$this->db->select("COMPONENT_VALUE, DATE_COLLECT");
	    $this->db->where("ID_COMPANY",$data['ID_COMPANY']);
	    $this->db->where("ID_COMPETITOR",$data['ID_COMPETITOR']);
	    $this->db->where("ID_SAMPLE_AREA",$data['ID_SAMPLE_AREA']);
	    $this->db->where("ID_TYPE_PRODUCT",$data['ID_TYPE_PRODUCT']);
	    $this->db->where("ID_COMPONENT", $id_component);
		return $this->db->get("O_SAMPLE_QUALITY")->result_array();
	}

	public function get_area_sample_list_scm($data){
	    $this->scmdb->where("ID_M_KOTA", $data['ID_SAMPLE_AREA']);
		return $this->scmdb->get("ZREPORT_M_KOTA")->row_array();
	}

	public function checklist_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE","to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')",FALSE);
		$this->db->insert("O_CHECKLIST");
	}

	public function is_checklist_exists($data){
		$this->db->where("ID_COMPONENT",$data['ID_COMPONENT']);
		return $this->db->get("O_CHECKLIST")->num_rows();
	}

	public function checklist_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("IS_ACTIVE",$data['IS_ACTIVE']);
		$this->db->set("MODIFIED_DATE","to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')",FALSE);
		$this->db->set("MODIFIED_BY",$data['MODIFIED_BY']);
		$this->db->where("ID_COMPONENT",$data['ID_COMPONENT']);
		$this->db->update("O_CHECKLIST");
	}

	public function datalist_auth($ID_GROUPAREA=null){
		if($ID_GROUPAREA) $this->db->where("a.ID_GROUPAREA",$ID_GROUPAREA);
		$this->db->order_by("a.ID_GROUPAREA");
		return $this->db->get("M_GROUPAREA a")->result();
	}

	public function list_by_plant($ID_PLANT){
		$this->db->select("a.*,b.NM_PLANT,c.NM_COMPANY");
		$this->db->join("M_PLANT b","a.ID_PLANT=b.ID_PLANT");
		$this->db->join("M_COMPANY c","b.ID_COMPANY=c.ID_COMPANY");
		$this->db->where("a.ID_PLANT",$ID_PLANT);
		return $this->db->get("M_GROUPAREA ")->result();
	}

	public function search(&$keyword){
		$this->db->like("NM_GROUPAREA",$keyword);
		return $this->db->get("M_GROUPAREA")->result();
	}

	public function data($where){
		$this->db->where($where);
		return  $this->db->get("M_GROUPAREA")->row();
	}

	public function get_data_by_id($ID_GROUPAREA){
		$this->db->where("ID_GROUPAREA",$ID_GROUPAREA);
		return $this->db->get("M_GROUPAREA")->row();
	}

	public function data_except_id($where,$skip_id){
		$this->db->where("ID_GROUPAREA !=",$skip_id);
		$this->db->where($where);
		return $this->db->get("M_GROUPAREA")->row();
	}

	public function insert($data){
		$this->db->set($data);
		$this->db->set("ID_GROUPAREA","SEQ_ID_GROUPAREA.NEXTVAL",FALSE);
		$this->db->insert("M_GROUPAREA");
	}

	public function update($data,$ID_GROUPAREA){
		$this->db->set($data);
		$this->db->where("ID_GROUPAREA",$ID_GROUPAREA);
		$this->db->update("M_GROUPAREA");
	}

	public function delete($ID_GROUPAREA){
		$this->db->where("ID_GROUPAREA",$ID_GROUPAREA);
		$this->db->delete("M_GROUPAREA");
	}

	public function sample_delete($data, $tgl){
		$this->db->where($data);
		$this->db->where("DATE_COLLECT", "to_date('".$tgl."','YYYY-MM-DD HH24:MI:SS')",FALSE);
		$delete_data = $this->db->delete("O_SAMPLE_QUALITY");

		if ($delete_data) {
			# code...
			return true;
		} else {
			# code...
			return false;
		}
		
	}

	public function get_query(){
		$this->db->select('*');
		$this->db->from($this->table);
	}

	public function get_list() {
		$this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

}
