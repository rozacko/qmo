<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Input Sample Quality
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>

      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <label for="oCOMPANYn">COMPANY</label>
                <select id="oCOMPANYn" name="oCOMPANYn" class="form-control select2">
                </select>
              </div>

              <div class="col-sm-1" style="float:  right;">
                <label for="delLastRow"></label>
                <button class="btn btn-block btn-danger btn-flat delLastRow" style="display: none;" name="delNewRow" id="delLastRow"><i class="fa fa-trash"></i> Row</button>
              </div>

              <div class="col-sm-1" style="float:  right;">
                <label for="addNewRow"></label>
                <button class="btn btn-block btn-success btn-flat addNewRow" style="display: none;" name="addNewRow" id="addNewRow"><i class="fa fa-plus"></i> Row</button>
              </div>
            </div>

          <div class="row col-sm-12">
            <div id="example1" class="hot handsontable htColumnHeaders" ></div>
          </div>

          <div class="form-group row">
            <div class="col-sm-1" style="float: right; margin-top: 10px;">
              <button class="btn btn-block btn-primary btn-flat" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" name="savesample" id="savesample"><i class="fa fa-save"></i> &nbsp;Save</button>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data saved successfully :)"
  ></a>
<!-- eof msg confirm -->

<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }

 table #tb_dev {
    border-collapse: collapse;
    width: 100%;
  }

  #tb_dev th, td {
      text-align: center;
      padding: 8px;
  }

  #tb_dev tr:nth-child(even){background-color: #f2f2f2}

  #tb_dev th {
      background-color: #ffaf38;
      color: white;
  }
</style>

<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- DataTable CSS -->
<link href="<?php echo base_url("plugins/dataTables/datatables.min.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- DataTable CSS -->
<script src="<?php echo base_url("plugins/dataTables/datatables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/dataTables/dataTables.select.min.js");?>"/></script>

<!-- Select2 Plugin-->
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/select2/select2.full.min.js");?>"/></script>

<script>
  $(document).ready(function(){

    loadCompany();

    var oTableComp = $("#tabel_master_competitor").DataTable();
    var oTableArea = $("#tabel_master_area").DataTable();
    var glbproduk = new Array();

    $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_produk_scm");?>', function (result) {
      glbproduk = result;

    });


    function getContentData() {
      return [
      ];
    }

    function getListArea() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_KOTA']);
          }
        }else{
        }
      });
      return datal;
    }

    function getSampleProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_merk");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['PRODUK']);

          }
        }else{
        }
      });
      return datal;
    }

    function getTypeProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['KD_PRODUCT']);
          }
        }else{
        }
      });
      
      return datal;
    }

    function getRowOptions() {
      var initial_coloptions = [
        {
          type: 'date'
        },
        {
          type: 'autocomplete',
          source: getListArea(),
          strict: true,
          allowInvalid: false,
          visibleRows: 10
        },
        {
          data: 'produk',
          type: 'autocomplete',
          source: getSampleProduct(),
          strict: true,
          allowInvalid: false,
          visibleRows: 10
        },
        {
          type: 'text',
        }
      ];

      var rowcontent = {
            type: "numeric"
      }

      for (var i = 0; i < oglbcomponent.length; i++) {
        initial_coloptions[i+4] = rowcontent;
      }
      return initial_coloptions;
    }

    function getColHeader() {
      var columnlist = ['Date', 'Area', 'Competitor', 'Type Product'];
      for (var i = 0; i < oglbcomponent.length; i++) {
        columnlist.push(oglbcomponent[i]);
      }
      return columnlist;
    }

    function getColHeader() {
      var columnlist = ['Date', 'Area', 'Competitor', 'Type Product'];
      for (var i = 0; i < oglbcomponent.length; i++) {
        columnlist.push(oglbcomponent[i]);
      }
      return columnlist;
    }

    function getTypeProduct(merk) {
      return glbproduk[merk];
    }

    function loadtabelinput() {

      if (hot1) {

        hot1.destroy();

      }   

      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_component_display");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
              oglbcomponent.push(values[i]['KD_COMPONENT']);
              oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
              oglbcomponentname.push(values[i]['KD_COMPONENT']);
            }
          }
          hot1 = new Handsontable(example1, {
            data: getContentData(),
            height: 450,
            autoColumnSize : true,
            fixedColumnsLeft: 3,
            manualColumnFreeze: true,
            manualColumnResize: true,
            colHeaders: getColHeader(),
            columns: getRowOptions(),
          });
          hot1.alter('insert_row', hot1.countRows());

          hot1.updateSettings({
            afterChange: function(changes, src) {
              var row = changes[0][0],
                col = changes[0][1],
                newVal = changes[0][3];

                if (newVal == 'OPC' || newVal == 'PPC' || newVal == 'PCC') {}


                if (col == 'produk') {

          				var cellProperties = hot1.getCellMeta(row, 3);


          				if (parseInt((glbproduk[newVal]).length) == 1 ) {

      	                	hot1.setCellMeta(row, 3, 'type', 'text');
      	                	hot1.setDataAtCell(row, 3, glbproduk[newVal][0]);
      					    cellProperties.readOnly = 'true';

          				} else if (parseInt((glbproduk[newVal]).length) > 1 ) {

          					hot1.setCellMeta(row, 3, 'type', 'autocomplete');
      					    cellProperties.source = getTypeProduct(newVal);
      					    hot1.render();

      					    var cellPropertiestmp = hot1.getCellMeta(row, 3);

          				}
                }
            }
          })

          $('#delLastRow').show();
          $('#addNewRow').show();
          $('#savesample').show();
        }else{
        }
      });

    }

    $('#ComponentSetting').on('show.bs.modal', function (event) {
      $('#formarea').hide();
      $('#view_marea').show();
      $('#btnUpdArea').hide();
      $('#formproduct').hide();
      $('#view_mproduct').show();
      $('#btnUpdProduct').hide();
    })

    $("#addNewArea").click(function(event){
      $('#formarea').show();
      $('#view_marea').hide();
      $('#btnAddArea').show();
      $('#btnUpdArea').hide();
    });

    $("#addNewProduct").click(function(event){
      $('#formproduct').show();
      $('#view_mproduct').hide();
      $('#btnAddProduct').show();
      $('#btnUpdProduct').hide();
    });

    $("#btnEditArea").click(function(event){
      $('#formarea').show();
      $('#view_marea').hide();
      $('#btnAddArea').hide();
      $('#btnUpdArea').show();
    });

    $("#btnEditProduct").click(function(event){
      $('#formproduct').show();
      $('#view_mproduct').hide();
      $('#btnAddProduct').hide();
      $('#btnUpdProduct').show();
    });

    $("#btnCancelArea").click(function(event){
      $('#formarea').hide();
      $('#view_marea').show();
    });

    $("#btnCancelProduct").click(function(event){
      $('#formproduct').hide();
      $('#view_mproduct').show();
    });

    $("#btnAddArea").click(function(event){
      $('#formarea').hide();
      $('#view_marea').show();
    });

    $("#btnAddProduct").click(function(event){
      $('#formproduct').hide();
      $('#view_mproduct').show();
    });

    $("#btnUpdArea").click(function(event){
      $('#formarea').hide();
      $('#view_marea').show();
    });

    $("#btnUpdProduct").click(function(event){
      $('#formproduct').hide();
      $('#view_mproduct').show();
    });

    var example1 = document.getElementById('example1'), hot1;
    var oglbcomponent = new Array();
    var oglbcomponentid = new Array();
    var oglbcomponentname = new Array();

    document.querySelector('.addNewRow').addEventListener('click', function() {
      hot1.alter('insert_row', hot1.countRows());
    });

    document.querySelector('.delLastRow').addEventListener('click', function() {
      hot1.alter('remove_row', hot1.countRows() - 1);
    });

    $("#btLoad").click(function(event){
      event.preventDefault();
      loadCement();
    });

    $("#savecomponent").click(function(event){

      $(".modal-body").addClass("loading");
      $(".modal-body").css("cursor", "progress");
      var arrcomponentchecked = new Array();
      var arrcomponentunchecked = new Array();
      var componentceklist = document.getElementsByName("listcomponent[]");
      for (var i=0; i < componentceklist.length; i++) {
          if (componentceklist[i].checked == true) {
              arrcomponentchecked.push(componentceklist[i].value);
          } else {
              arrcomponentunchecked.push(componentceklist[i].value);
          }
      }

      $.ajax({
        url: "<?php echo site_url("input_sample_quality/save_checklist");?>",
        data: {"datacheck": arrcomponentchecked, "datauncheck": arrcomponentunchecked, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          $(".modal-body").removeClass("loading");
          $(".modal-body").css("cursor", "default");

          if (res.msg == 'success') {
            $("#a-notice-success").click();
          }
          else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }
        },
        error: function () {
          $(".modal-body").removeClass("loading");
          $(".modal-body").css("cursor", "default");
          $("#a-notice-error").click();
        }
      });

    });

    loadtabelinput();

    loadSampleAreaList();
    loadTypeProductList();
    loadMerkList();

    $('button[name=savesample]').click(function () {
      saveSampleQuality();
    });

    function saveSampleQuality(argument) {
     	$body = $("body");
      	$body.addClass("loading");
      	$body.css("cursor", "progress");

    	$('#savesample').button('loading');

      	var companyid = $("#oCOMPANYn").val();
      	var companyname = $("#oCOMPANYn option:selected").text();
      	var handsonData = hot1.getData();
      	$.ajax({
	        url: "<?php echo site_url("input_sample_quality/save_sample_data_q");?>",
	        data: {"company_id": companyid, "companyname": companyname, "id_comp": oglbcomponentid, "name_comp": oglbcomponentname, "user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData}, //returns all cells' data
	        dataType: 'json',
	        type: 'POST',
	        success: function (res) {
	          	$body.removeClass("loading");
	          	$body.css("cursor", "default");
	          	if (res.msg == 'success') {
	            	$("#a-notice-success").click();
                var rowcount = hot1.countRows();
                for (var i = 0; i < rowcount; i++) {
                  hot1.alter('remove_row', hot1.countRows() - 1);
                }	            	
                hot1.alter('insert_row', hot1.countRows());
	          	}
	          	else {
	            	$("#a-notice-error").data("text", res.msg);
	            	$("#a-notice-error").click();
	          	}
    			$('#savesample').button('reset');

	        },
	        error: function () {
	          	$body.removeClass("loading");
	          	$body.css("cursor", "default");
	          	$("#a-notice-error").click();

    			$('#savesample').button('reset');
	        }
      	});

    }

    $('button[name=save]').click(function () {

      	saveCement();

    });

    $("#btLoad").click(function(event){
      event.preventDefault();
      loadCement();
    });

    $("#btSetting, #btSettings").click(function(event){
      event.preventDefault();
      var formData = $("#formData").serializeArray();
      var handsonData = handsontable.getData();
      var g_area = ($("#ID_GROUPAREA option:selected").text() == "FINISH MILL") ? "FM" : "OTHER";

      $.ajax({
        url: "<?php echo site_url("input_daily/preview_boxplot");?>",
        data: {"data": handsonData, "formData": formData, "comp": cols, "g_area": g_area}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
        },
        error: function () {
        }
      });
      $('#ComponentSetting').modal('show');
    });

  /** Notification **/
  $(".notice-error").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-success").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });

});

function loadSampleAreaList() {
  var area = $('#oSUB_AREA');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
    var values = result;
    area.find('option').remove();
    if (values != undefined && values.length > 0) {
      area.css("display","");
      $(values).each(function(index, element) {
        area.append($("<option></option>").attr("value", element.ID_SAMPLE_AREA).text(element.NAME_AREA));
      });
    }else{
      area.find('option').remove();
      area.append($("<option></option>").attr("value", '00').text("NO TYPE"));
    }
  });
}

function loadTypeProductList() {
  var typeproduct = $('#oTYPE_PRODUCT');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product");?>', function (result) {
    var values = result;
    typeproduct.find('option').remove();
    if (values != undefined && values.length > 0) {
      typeproduct.css("display","");
      $(values).each(function(index, element) {
        typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
      });
    }else{
      typeproduct.find('option').remove();
      typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
    }
  });
}

function loadMerkList() {
  var merk = $('#oMERK');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_merk");?>', function (result) {
    var values = result;
    merk.find('option').remove();
    if (values != undefined && values.length > 0) {
      merk.css("display","");
      $(values).each(function(index, element) {
        merk.append($("<option></option>").attr("value", element.ID_COMPETITOR).text(element.NAME_PRODUCT));
      });
    }else{
      merk.find('option').remove();
      merk.append($("<option></option>").attr("value", '00').text("NO TYPE"));
    }
  });
}

function loadCompany() {
  var merk = $('#oCOMPANYn');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_company_scm");?>', function (result) {
    var values = result;
    merk.find('option').remove();
    if (values != undefined && values.length > 0) {
      merk.css("display","");
      $(values).each(function(index, element) {
        merk.append($("<option></option>").attr("value", element.KODE_PERUSAHAAN).text(element.NAMA_PERUSAHAAN));
      });
    }else{
      merk.find('option').remove();
    }

    
    $('.select2').select2();
  });
}
</script>
