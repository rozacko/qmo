
<section class="content-header">
  <h1>
     Set up pelaksanaan (Akurasi Labor SIG)
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">

            <div class="box-header">
              <form class="form-inline">
	          <?php if($this->PERM_WRITE): ?>
	            <div class="input-group input-group-sm" style="width: 150px; ">
	              <a href="<?php echo site_url("acclab_setuppelaksanaan/add");?>" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
	            </div>
	          <?PHP endif; ?>
	          </form>
          	  <hr/>

            </div>

            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap "
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th width="1">No.</th>
					<th >Nama Akurasi Labor</th>
					<th width="1">Tahun </th>
	                <th >Jenis Pelaksanaan</th>
	                <th>Standard Evaluasi</th>
	                <th>Plant</th>
	                <th >PIC Observasi</th>
	                <th >PIC Plant</th>
                    <th >Start Date</th>
					<th >End Date</th>
					<th width="1">Status</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
					<td><?= $dt->NAMA;?></td>
					<td><?= $dt->TAHUN;?></td>
					<td><?= $dt->JENIS;?></td>
					<td><?= ($dt->FK_ID_STD != 0) ? $dt->NAMA_STANDARD.' - '.$dt->KODE_STANDARD : '-';?></td>
                    <td><?= $dt->NAMA_LAB;?> </td>
                    <td><?= $dt->FULLNAME_OBSERVASI;?> </td>
                    <td><?= $dt->FULLNAME_PLANT;?> </td>
                    <td><?= $dt->START_DATE;?> </td>
                    <td><?= $dt->END_DATE;?> </td>
                    <td><?= ($dt->STATUS == 'OPEN' ? 'APPROVED ATASAN' : $dt->STATUS );?> </td>
                    <!-- <td>
					<?php
						$fase = $this->setup->fase_proficiency($dt->ID);
						echo $fase->ACTIVITY_NAME;
					?>
					</td> -->
                    <td>
                    <?php if($this->PERM_WRITE): ?>
                      <?php if ($dt->STATUS == 'WAITING') { ?>
                      	<button title="Send Notif" class="btNotif btn btn-info btn-xs" type="button" data-toggle="modal" data-target="#notifModal<?= $dt->ID; ?>"><i class="fa fa-paper-plane-o"></i> Send Notif</button>
                      <?php }else{ ?>
                      	<a href="<?php echo site_url("acclab_setuppelaksanaan/send_notif_pic/");?><?= $dt->ID; ?>" onClick="return doconfirm2();"><button title="Send Notif" class="btNotif btn btn-info btn-xs" type="button"><i class="fa fa-paper-plane-o"></i> Send Notif</button></a>
                      <?php } ?>
                      <?php if ($dt->STATUS == 'WAITING' || $dt->STATUS == 'REJECTED') { ?>
                      	<a href="<?php echo site_url("acclab_setuppelaksanaan/edit/");?><?= $dt->ID; ?>" title="Detail" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-arrows "></i> Edit</a>
                        <a href="<?php echo site_url("acclab_setuppelaksanaan/hapus/");?><?= $dt->ID; ?>" onClick="return doconfirm();" title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</a>
                      <?php } ?>
                    <?php endif; ?>
                    </td>
                  </tr>
<div id="notifModal<?= $dt->ID; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form role="form" method="POST" action="<?php echo site_url("acclab_setuppelaksanaan/send_notif") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Send Notifikasi :</b> [<?= $dt->JENIS;?>] <?= $dt->NAMA;?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="id_setup"  value="<?= $dt->ID;?>" />
				<div class="form-group c-group after-add-more">
                  <div class="col-sm-12 clearfix">
					<label>Email Atasan</label>
					<select class="form-control select2" NAME="atasan" id="atasan<?= $dt->ID ?>" placeholder="Select a PIC ...">
						<option value="">Pilih Atasan ...</option>
						<?php  foreach($this->atasan as $dt_pic): ?>
						  <option value="<?php echo $dt_pic->ID_USER ?>"><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
						<?php endforeach; ?>
					</select>
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Send</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>
                <?php } ?>
                </tbody>
	          </table>
            </div>

          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

<script>
$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable();
      $('select').selectize({
          sortField: 'text'
      });
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
	function doconfirm2(){
	  job=confirm("Are you sure you want to send notification to PIC?");
	  if(job!=true){
		return false;
	  }
	}
</script>