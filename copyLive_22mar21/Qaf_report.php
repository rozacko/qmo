<?php

class Qaf_report extends QMUser {
	
	public $list_data = array();
	public $list_product = array();
	public $list_qaf = array();
	public $NM_PRODUCT;
	public $list_component = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");

		$this->load->model("c_range_qaf");
		$this->load->model("qaf_daily");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_product");
		$this->load->model("M_hebat_report");
	}
	
	public function index(){
		$this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_qaf_report");
	}
	
	public function get_company_list(){
		echo json_encode($this->m_company->list_company());
	}
	
	
	public function generate_company_report(){
		$this->qaf_daily->generate_report($this->input->post(),'company'); //echo $this->qaf_daily->get_sql();
	}
	
	public function get_company_report(){ #var_dump($this->input->post()); 
		$this->NM_COMPANY = $this->m_company->get_data_by_id($this->input->post("ID_COMPANY"))->NM_COMPANY;
		$this->list_qaf = $this->qaf_daily->report_company($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_report_company");
	}
	
	public function generate_report($REPORT='cement'){
		$this->qaf_daily->generate_report($this->input->post(),$REPORT); #echo $this->qaf_daily->get_sql();
	}
	
	public function get_report(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->AREA = $this->m_area->get_data_by_id_cement($this->input->post("ID_AREA"));
		// echo $this->qaf_daily->get_sql(); 
		$this->list_component = $this->qaf_daily->component3($this->input->post()); //echo $this->qaf_daily->get_sql()."<br><br>";
		//var_dump($this->list_component); 
		$this->list_qaf = $this->qaf_daily->report($this->input->post());  #echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_report");
	}
	
	
	public function get_report_cs(){ 
		#var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		//$this->list_component = $this->qaf_daily->component($this->input->post(),array(19,20,21));
		// var_dump($this->list_component);  echo $this->qaf_daily->get_sql(); exit;
		$this->list_component = $this->qaf_daily->component3($this->input->post(),array(19,20,21)); //cari component & range
		$this->list_qaf = $this->qaf_daily->report_cs($this->input->post());  //echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_cs");
	}
	
	public function get_report_st(){ 
		#var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		//$this->list_component = $this->qaf_daily->component($this->input->post(),array(22,23)); #var_dump($this->list_component);
		$this->list_component = $this->qaf_daily->component3($this->input->post(),array(22,23)); 
		//echo $this->qaf_daily->get_sql();
		$this->list_qaf = $this->qaf_daily->report_st($this->input->post());  
		// echo $this->qaf_daily->get_sql();
		// var_dump($this->list_component); 
		$this->template->nostyle("v_qaf_st");
	}
	
	public function get_report_clinker(){ 
		// var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_AREA = $this->m_area->get_data_by_id_allP($this->input->post("ID_AREA"))->NM_AREA;   #echo $this->qaf_daily->get_sql();
		$this->list_component = $this->qaf_daily->component($this->input->post()); #var_dump($this->list_component); 
		 // echo $this->qaf_daily->get_sql(); 
		$this->list_qaf = $this->qaf_daily->report_clinker($this->input->post());  #echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_clinker");
	}
	
	public function report_produksi(){ #var_dump($this->input->post());
		// $id_plant = $this->input->post("ID_PLANT");
		// // echo '<br>'.$id_plant[0]['ID_PLANT'];
		// var_dump($id_plant[0]);
		// die;
		// $id_plant = $this->input->post('ID_PLANT'); 
		// print_r($id_plant);
		// $param_in = array();
		// if($id_plant){
		// 	$param_in['ID_PLANT'] = $id_plant[];
		// }  

		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; #echo $this->qaf_daily->get_sql();
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi($this->input->post()); #echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			// echo '<br>'.$r->QAF.'/'.$r->JML_COMPONENT;
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi");
	}
	
	public function report_produksi_cs(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_cs($this->input->post());  //echo $this->qaf_daily->get_sql();
				
		foreach($this->list_qaf as $i => $r){
			$r->JML_COMPONENT = 3; //cs3, cs7, cs28
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);			
		}
		$this->template->nostyle("v_qaf_produksi_cs");
	}
	
	public function report_produksi_st(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_st($this->input->post());  //echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			$r->JML_COMPONENT = 2; //INIT, FINAL
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi_cs");
	}
	
	public function report_clinker(){ 
		// var_dump($this->input->post());die;
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_clinker1($this->input->post()); //echo $this->qaf_daily->get_sql();
		// $this->list_component = $this->qaf_daily->component2($this->input->post()); #var_dump($this->list_component); 
		// var_dump($this->list_qaf);exit;
		foreach($this->list_qaf as $i => $r){
			// echo '<br>'.$r->QAF.'/'.$r->JML_COMPONENT. ' ===='.round((float)$r->QAF/$r->JML_COMPONENT, 2);
			$this->list_qaf[$i]->QAF = (float)$r->QAF;
			$qaf_prod = (float)$r->QAF/(int)$r->JML_COMPONENT;
			$this->list_qaf[$i]->QAF_AREA = is_nan($qaf_prod) ? 0 : round($qaf_prod,2);
			$this->list_component = $this->qaf_daily->component2($this->input->post(), $r->ID_PLANT); #var_dump($this->list_component); 
			$this->list_qaf2[] = $this->qaf_daily->report_clinker2($this->input->post(), $r->ID_AREA);  #echo $this->qaf_daily->get_sql();
		}
			 //var_dump($this->list_qaf);
		// exit;
		$this->template->nostyle("v_qaf_produksi_clinker");
	}
	
	
	public function export_report(){
		$this->data = json_encode($this->m_incident->report($this->input->post()));
		$this->template->nostyle("xls_qaf_incident_report");
	}	
	
	public function grafik_qaf(){
		$this->list_company = $this->m_company->list_company_auth();
		$this->template->adminlte("v_grafik_qaf");
	}
	
	public function get_area_by_group(){

				//$ID_GROUPAREA = $this->input->post('ID_GROUPAREA');
				$id_plant = $this->input->post('id_plant'); 
				// var_dump($id_plant);
				// die;
				$ID_PRODUCT = $this->input->post('ID_PRODUCT'); 
				$ID_COMPANY = $this->input->post('ID_COMPANY'); 

				$param_in = array();
				if($id_plant){
					$param_in['ID_PLANT'] = $id_plant;
				} 
				$ID_GROUPAREA[]=1;
				$ID_GROUPAREA[]=81; //tmbah outsilo
		//reseult = $this->qaf_daily->get_area_by_group1($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT); #echo $this->qaf_daily->get_sql();
		$reseult = $this->qaf_daily->get_area_by_group2($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT); 
		

				echo json_encode($reseult);
	}
	
	/*
	public function compressive_strength(){
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post());# var_dump($this->list_component); # echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_cs");
	}
	*/ 
	
	public function compressive_strength($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_COMPANY=NULL,$ID_PRODUCT=NULL){
		$this->libExternal('select2');
		$ID_GROUPAREA=1; //cement
		switch($sm){		
			case 'get_area':	
				$ID_GROUPAREA=array(1,81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group2($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			case 'get_area_cs':	
				$ID_GROUPAREA=array(1,81);
				//print_r($ID_PLANT); 
				echo json_encode($this->qaf_daily->get_area_by_group_time($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT));
				# echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				// $this->template->adminlte("v_qaf_cs");
				$this->template->adminlte("v_qaf_cs_tab");

			break;
		}
	}
	
	
	public function setting_time($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_COMPANY=NULL,$ID_PRODUCT=NULL){
		$this->libExternal('select2');
		//$ID_GROUPAREA=1; //cement
		switch($sm){		
			case 'get_area':
				$ID_GROUPAREA=array(1,81);
				//echo json_encode($this->qaf_daily->get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY, $ID_PRODUCT));
				echo json_encode($this->qaf_daily->get_area_by_group_time($ID_PLANT,$ID_GROUPAREA,$ID_COMPANY, $ID_PRODUCT));
				
				// echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				// $this->template->adminlte("v_qaf_st");
				$this->template->adminlte("v_qaf_st_tab");

			break;
		}
	}
	
	public function clinker($sm='default',$ID_AREA=NULL){
		$ID_GROUPAREA=4; //cement
		switch($sm){		
			case 'get_area':
				$ID_COMPANY = $this->input->post('ID_COMPANY');
				$id_plant = $this->input->post('id_plant'); 

				$param_in = array();
				if($id_plant){
					$param_in['ID_PLANT'] = $id_plant;
				} 
				$reseult = $this->qaf_daily->get_area_by_group1($param_in,$ID_GROUPAREA,$ID_COMPANY,$ID_PRODUCT);
				#echo $this->qaf_daily->get_sql();
				echo json_encode($reseult);
				// print_r($reseult);
			break;
			
			default:
				$this->libExternal('select2');
				$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
				$this->list_company = $this->m_company->datalist();
				$this->template->adminlte("v_qaf_clinker");
			break;
		}
		
		
	}
	
	public function json_plant_list($ID_COMPANY){
		$this->load->model("m_plant");
		$data = $this->list_plant = $this->m_plant->datalist($ID_COMPANY);
		echo json_encode($data);
	}
	

	public function async_list_product_qaf($ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$otsilo=  array(102,101,104,103,105);
		if(in_array($ID_PLANT,$otsilo)){
			$ID_GROUPAREA=81;
		}
		$this->load->model("c_range_qaf");
		$data = $this->c_range_qaf->list_configured_product_qaf($ID_PLANT,$ID_GROUPAREA);  #echo $this->c_range_qaf->get_sql();
		echo json_encode($data);
	}

	public function async_list_product_qaf1(){
		$this->load->model("c_range_qaf");

				//$ID_GROUPAREA = $this->input->post('ID_GROUPAREA');
				$id_plant = $this->input->post('id_plant'); 

				$param_in = array();
				if($id_plant){
					$param_in['ID_PLANT'] = $id_plant;
				} 
				$ID_GROUPAREA[]=1;
				$ID_GROUPAREA[]=81; //tmbah outsilo
		//$data = $this->c_range_qaf->list_configured_product_qaf1($param_in,$ID_GROUPAREA); #echo $this->c_range_qaf->get_sql();
		$data = $this->c_range_qaf->list_configured_product_qaf2($param_in,$ID_GROUPAREA);
		echo json_encode($data);
	}
	
	public function calculate_qaf_cs(){
		// $this->M_hebat_report->calculate_qaf_cs($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}

	public function json_line($tipe=''){
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');
		if(!$_POST['opt_company']){
			echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
			exit;
		}
		if(!$_POST['tahun']){
			echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
			exit;
		}

		switch ($tipe) {
			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			default:
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;
		}

		if($query == 'qaf_clinker_progress'){
			$id_plant  = $this->input->post('id_plant');

			$param = array();
			$param_in = array();

			$param['TAHUN'] = $tahun;
			$param['ID_COMPANY'] = $opco;

			if($id_plant){ 
				$param_in['ID_PLANT'] = $id_plant;
			}

			$result = $this->M_hebat_report->qaf_clinker_progress2($param, $param_in);
		}else{
			$result = $this->M_hebat_report->{$query}($opco, $tahun);
		}
		
		$temp   = array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $res) {
				$color 			= monthColor($res->BULAN);
				$temp['x'][]  	= $res->BULAN;
				$temp['y'][]  	= $res->NILAI;
				$temp['type'] 	= 'scatter';
				$temp['line'] 	= array('dash' => 'dashdot');
				$temp['marker'] = array('symbol' => "square", "size" => 8);
				$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
				$temp['hoverinfo'] 	= 'x+y';
			}

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => 'TOTAL QAF',
								"showline" => true,
								"showgrid" => true,
								"gridwidth" => 4,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['xaxis'] 	= array(
								"title" => "MONTH",
								"showgrid" => false,
								"showline" => true,
								"type" => 'linear',
								"autorange" => false,
								"range" => array(0,12),
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.23
								)
							);
			$lay['title'] 		= $title;
			$lay['showlegend'] 	= false;
			$lay['hovermode'] 	= 'closest';
			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= array($temp);
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}
}	

?>
