<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_cement_hourly extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_input_cement_hourly", $data);
	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_component($id_plant='',$id_grouparea=''){
		//HandsonTable Column Header
		$id_comp[] = '_time';
		$header[]['title'] = 'TIME';
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'H');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->NM_COMPONENT);
		}

		//Tambahkan Status dan Remark
		#$header[]['title'] = 'Machine Status';
		$header[]['title'] = 'REMARK';

		#$id_comp[] = '_machine_status';
		$id_comp[] = '_remark';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		$data['jsonData'] 	= '';
		to_json($data);
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_hourly
		$data = array();
		$t_prod = $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND ID_PRODUCT='".$form->ID_PRODUCT."'");
		foreach ($t_prod as $key => $row) {
			$d_prod = $this->d_cement_hourly->get_by_id($row->ID_CEMENT_HOURLY);
			$data[$key][] = $row->JAM_DATA;
			foreach ($d_prod as $k => $vl) {
				$data[$key][] = $vl->NILAI;
			}
			//$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
			$data[$key][] = $row->MESIN_REMARK;
		}

		to_json($data);
	}

	public function save_table(){
		$post = $this->input->post();
			
		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;

		//T_PRODUCTION_HOURLY (1): ID_CEMENT_HOURLY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_UPDATE,JAM_UPDATE,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_HOURLY (M): ID_CEMENT_HOURLY, ID_COMPONENT, NILAI
		
		//read line by line
		foreach($data as $y => $row){ //y index
			
			//sub index
			$i_jam			= 0;
			//$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);
			
			if(!$row[$i_jam]) continue; //break null data	
			
			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= $form->ID_PRODUCT;
			//$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $row[$i_jam];
			
			#var_dump($tdata);
			#save
			//cek dulu
			$exists = null;
			
			$ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($tdata[ID_AREA],$tdata[ID_PRODUCT],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			
			if(!$ID_CEMENT_HOURLY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
			}
			else{
				$tdata['DATE_UPDATE'] = date("d/m/Y");
				$tdata['JAM_UPDATE']  = date("H");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
			}
						
			//D
			for($x=1;$x<$i_remark;$x++){
				$ddata = null;
				$ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
				$ddata['ID_COMPONENT']		= $comp[$x];
				$ddata['NILAI']				= $row[$x];
				$ddata['NO_FIELD']			= $x;
				#save 
				
				#var_dump($ddata);
				
				#exit;
				
				if(!$this->t_cement_hourly->d_exists($ddata)){
					$this->t_cement_hourly->d_insert($ddata);
				}
				else{
					$this->t_cement_hourly->d_update($ddata);
				}
			}
			
			//exit;

		}		
	}
}

/* End of file Input_cement_hourly.php */
/* Location: ./application/controllers/Input_cement_hourly.php */
?>
