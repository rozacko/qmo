<?php

class M_acclab_settingperiode Extends DB_QM {
	
	public function get_list(){
        $this->db->where("DELETE_BY IS NULL");
		$this->db->order_by("TAHUN","DESC");
		return $this->db->get("QMLAB_PERIODE")->result();
    }
	
	public function save(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$nama	 	 = $this->input->post('nama_acclab');
		$tahun	 	 = $this->input->post('tahun_acclab');
		$start_date	 = $this->input->post('start_date');
		$end_date	 = $this->input->post('end_date');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			INSERT INTO QMLAB_PERIODE (NAMA, TAHUN, START_DATE, END_DATE, CREATE_DATE, CREATE_BY, STATUS) VALUES ('{$nama}', '{$tahun}', TO_DATE('{$start_date}', 'YYYY-MM-DD'), TO_DATE('{$end_date} 23:59:59', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}', 'OPEN')
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function edit(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$id		 = $this->input->post('id_acclab');
		$nama	 	 = $this->input->post('nama_acclab');
		$tahun	 	 = $this->input->post('tahun_acclab');
		$start_date	 = $this->input->post('start_date');
		$end_date	 = $this->input->post('end_date');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			UPDATE QMLAB_PERIODE SET NAMA = '{$nama}', TAHUN = '{$tahun}', START_DATE = TO_DATE('{$start_date}', 'YYYY-MM-DD'), END_DATE = TO_DATE('{$end_date} 23:59:59', 'YYYY-MM-DD HH24:MI:SS'), UPDATE_DATE = TO_DATE('{$date_in}', 'YYYY-MM-DD'), UPDATE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function edit_status(){
		$id_pp		 = $this->input->post('id_acclab');
		$status 	 = $this->input->post('status_acclab');
		
		$sql = "
			UPDATE QMLAB_PERIODE SET STATUS = '{$status}' WHERE ID = {$id_pp}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function deleted($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$sql = "
			UPDATE QMLAB_PERIODE SET DELETE_DATE = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), DELETE_BY = '{$user_in->FULLNAME}' WHERE ID = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
}

?>