   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Third Material Configuration
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">   
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Component Settings</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <div id="example1" class="hot handsontable htColumnHeaders" ></div>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>

$(document).ready(function(){


  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable;

  var example1 = document.getElementById('example1'), hot1;

  loadtabelinput()
  // reloadtabel();  
  // loadTypeProductList(); 

  $(document).on('click',"#AddNew",function () {
    $('#saving').show();

    $('#formArea').show();
    $('#viewArea').hide();

    $('#UpdateArea').hide();
    $('#AddArea').show();

    $('#ID_MATERIAL').val('');
    $('#KODE_MATERIAL').val('');
    $('#NAME_MATERIAL').val('');

    $('#saving').hide();

  });

  $(document).on('click',"#ReloadData",function () {
    loadtabelinput()
    // reloadtabel();
  });

  $(document).on('click',"#AddArea",function () {
    $('#saving').show();

    if ($('#KODE_MATERIAL').val() != '' && $('#NAME_MATERIAL').val() ) {
      $.ajax({
        url: "<?php echo site_url("third_material/save_third_material");?>",
        data: {"KODE_MATERIAL": $('#KODE_MATERIAL').val(), "NAME_MATERIAL": $('#NAME_MATERIAL').val(), "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }              
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
      $("#a-notice-error").click();
      if ($('#KODE_MATERIAL').val() == '') {
        $('#KODE_MATERIAL').focus();
      } else if ($('#NAME_MATERIAL').val() == '') {
        $('#NAME_MATERIAL').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',"#UpdateArea",function () {
    $('#saving').show();

    if ($('#KODE_MATERIAL').val() != '' && $('#NAME_MATERIAL').val() != '') {
      $.ajax({
        url: "<?php echo site_url("third_material/update_third_material");?>",
        data: {"KODE_MATERIAL": $('#KODE_MATERIAL').val(), "NAME_MATERIAL": $('#NAME_MATERIAL').val(),  "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });      
    } else {
      $("#a-notice-error").click();
      if ($('#KODE_MATERIAL').val() == '') {
        $('#KODE_MATERIAL').focus();
      } else if ($('#NAME_MATERIAL').val() == '') {
        $('#NAME_MATERIAL').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',".btnCancel",function () {
    $('#saving').show();
    $('#formArea').hide();
    $('#viewArea').show();

    $('#ID_MATERIAL').val('');
    $('#KODE_MATERIAL').val('');
    $('#NAME_MATERIAL').val('');
    reloadtabel();
    $('#saving').hide();      
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    $('#saving').show();
    $('#formArea').show();
    $('#viewArea').hide();  

    $('#UpdateArea').show();
    $('#AddArea').hide(); 

    var data = oTable.row($(this).parents('tr')).data();
    console.log(data);

    $('#ID_MATERIAL').val(data['ID_MATERIAL']);
    $('#KODE_MATERIAL').val(data['KODE_MATERIAL']);
    $('#NAME_MATERIAL').val(data['NAME_MATERIAL']);
    $('#saving').hide();
  });

  /** btDelete Click **/
  $(document).on('click',".btDelete",function () {
    var data = oTable.row($(this).parents('tr')).data();
    console.log(data);
    $.confirm({
        title: "Remove Component",
        text: "This sample area will be removed. Are you sure?",
        confirmButton: "Remove",
        confirmButtonClass: "btn-danger",
        cancelButton: "Cancel",
        confirm: function() {
          $.ajax({
            url: "<?php echo site_url("third_material/delete_third_material");?>",
            data: {"ID_MATERIAL": data['ID_MATERIAL'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
              if (res['status'] == true) {
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
              } else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              reloadtabel();            
            },
            error: function () {
              $("#a-notice-error").click();
            }
          });
        },
        cancel: function() {
        }
    });
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      // scrollY: 270,
      // scrollX: true,
      // fixedColumns:   {
      //     leftColumns: 2
      // },
      ajax: {
        url: '<?php echo site_url("third_material/third_material_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "ID_MATERIAL", "width": 50},
        // {"data": "NAME_COMPETITOR"},
        {"data": "KODE_MATERIAL"},
        {"data": "NAME_MATERIAL"},
        {"data": "ID_MATERIAL", "width": 100,
          "mRender": function(row, data, index){
            return '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';

          }
        },
      ]
    });
    

    $('#ReloadData').show();
    $('#AddNew').show();
  }

  	function loadTypeProductList() {
	  var typeproduct = $('#TYPE_PRODUCT');
	  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product");?>', function (result) {
	    var values = result;
	    typeproduct.find('option').remove();
	    if (values != undefined && values.length > 0) {
	      typeproduct.css("display","");
	      $(values).each(function(index, element) {
	        typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
	      });
	    }else{
	      typeproduct.find('option').remove();
	      typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
	    }

	    // $('#btLoad').show();
	    // $('#saving').css('display','none');
	  });
	}



  function loadtabelinput() {

    hot1 = new Handsontable(example1, {
      data: getContentData(),
      // height: 450,
      autoColumnSize : true,
      // fixedColumnsLeft: 3,
      colWidths: [150, 70, 70, 70, 70],
      manualColumnFreeze: true,
      manualColumnResize: true,
      colHeaders: getColHeader(),
      columns: getRowOptions()
    });

    $('#ReloadData').show();

  }

  function getContentData() {
      // return [{"TRASS",0,0,0,0},{"LIMESTONE",0,0,0,0},{"GYPSUM",0,0,0,0}];
      return [["TRASS", , , , ],["LIMESTONE", , , , ],["GYPSUM", , , , ]];
  }

  function getRowOptions() {
    var initial_coloptions = [
      {
        type: 'text',
        readOnly: true
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      }
    ];

    return initial_coloptions;
  }

  function getColHeader() {
    var columnlist = ['Third Material', 'H20', 'LOi', 'Insol', 'SO3'];
    return columnlist;
  }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
