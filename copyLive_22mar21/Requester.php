<?php

class Requester extends QMUser {
	
	public $list_company = array();
	public $list_plant = array();
	public $list_user = array();
	public $comp = '';
	public $list_usergroup = array();
	public $dtController = 'requester';
	public $dtTitle = 'Requester';
	
	public $ID_COMPANY;
	public $viewer;
	public $ID_USER;
	public $data_user;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_roles");
		$this->load->model("m_plant");
		$this->load->model("m_user");
		$this->load->model("m_usergroup");
		$this->load->model("employee");
	}

	public function index(){

		$ses = $this->session->userdata('USER');
		$this->comp = $ses->ID_COMPANY;

		$this->list_user 	  = $this->m_user->datalist($this->comp); #echo $this->m_plant->get_sql();
		$this->template->adminlte("v_user");
	}

	//Admin-----------------------------
	public function approve($id, $vercode){
		$this->libExternal('select2');
		if(!$id){
			$this->response(array('status' => 'error', 'message' => 'id user not found!'));
			exit;
		}
		if(!$vercode){
			$this->response(array('status' => 'error', 'message' => 'verification code not found!'));
			exit;
		}


		$param['a.ID_USER'] = $id;
		$param['a.VERCODE'] = $vercode;
		$param['FLAG_ACTIVATION'] = '1';

		$ses = $this->session->userdata('USER');

		$access = $this->getAccess('adminuser');
		if($ses->ID_COMPANY){
			$param['a.ID_COMPANY'] = $ses->ID_COMPANY;
		}
		$this->data_user 	= $this->m_user->get_data($param);
		// echo $this->db->last_query();exit;

		if($ses->ID_COMPANY){
			if($access){
				if($this->data_user){
					$this->ID_USER   	= $id;
					$this->list_company = $this->m_company->list_company();
					$this->list_usergroup = $this->m_usergroup->datalist();
					
					$this->template->adminlte("approval/v_approve_account");
				}else{
					$this->template->adminlte("messages/v_not_match");
				}
			}else{
				$this->template->adminlte("messages/v_dont_have_authorization");
			}
		}else{
			if($this->data_user){
				$this->ID_USER   	= $id;
				$this->list_company = $this->m_company->list_company();
				$this->list_usergroup = $this->m_usergroup->datalist();
				
				$this->template->adminlte("approval/v_approve_account");
			}else{
				$this->template->adminlte("messages/v_not_match");
			}
		}
	}
	
//Requester-----------------------------
	public function activation($id, $vercode){
		$this->libExternal('select2');
		if(!$id){
			$this->response(array('status' => 'error', 'message' => 'id user not found!'));
			exit;
		}
		if(!$vercode){
			$this->response(array('status' => 'error', 'message' => 'verification code not found!'));
			exit;
		}


		$param['a.ID_USER'] = $id;
		$param['a.VERCODE'] = $vercode;
		$param['FLAG_ACTIVATION'] = '2';

		$ses = $this->session->userdata('USER');
		if($ses->ID_COMPANY){
			$param['a.ID_COMPANY'] = $ses->ID_COMPANY;
		}

		$this->data_user 	= $this->m_user->get_data($param);
		// echo $this->db->last_query();
		// exit;
		if($this->data_user){
			$this->ID_USER   	= $id;
			$this->list_company = $this->m_company->list_company();
			$this->list_usergroup = $this->m_usergroup->datalist();
			$this->roles = $this->m_roles->get_data_role_by_iduser($id);

			$str_roles = array();
			foreach ($this->roles as $i => $iv) {
				$str_roles[] = $iv->ID_USERGROUP;
			}

			$this->str_roles = join($str_roles, ',');

			$this->template->adminlte("approval/v_activation_account");
		}else{
			$this->template->adminlte("messages/v_not_match");
		}
	}

	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}	

	public function by_plant($ID_USER=NULL){
		$this->ID_USER   = $ID_USER;
		$this->ID_COMPANY = $this->m_plant->data("ID_USER='$ID_USER'")->ID_COMPANY;
		$this->index();
	}
	
	public function updateActivation($ID_USER, $typeAppoval){
		$this->update($ID_USER, $typeAppoval);
	}
	public function update($ID_USER, $typeAppoval){
		// var_dump($this->input->post());
		$status = 'error';
		if($this->m_user->data_except_id(array('USERNAME'=>$this->input->post("USERNAME")),$ID_USER)){
			$message = 'Username already exists!';
			$this->response(array('status'=>$status, 'message' => $message));
			exit;
		}
		$data_user 	= $this->m_user->get_data_by_id($ID_USER);
		if($data_user->ISACTIVE == 'Y'){
			$this->response(array('status'=>$status, 'message' => 'Sorry, this request has been approved!'));
			exit;
		}
		if($data_user->DELETED == '1'){
			$this->response(array('status'=>$status, 'message' => 'Sorry, this user has been deleted!'));
			exit;
		}
		$data = $this->input->post();
		if(!$data['PASSWORD']){
			$data['PASSWORD'] = md5(md5('semenindonesia'));
		}else{
			$data['PASSWORD'] = md5(md5($data['PASSWORD']));
		}

		// echo $this->input->post('ID_USERGROUP');
		// $usergroup = json_decode(stripslashes($_POST['ID_USERGROUP']),true);
		$rolesPost = $data['ID_USERGROUP'];
		unset($data['ID_USERGROUP']);
		// var_dump($data);
		// exit;
		if($typeAppoval == 'agree'){
			if($rolesPost == 'null'){
				$this->response(array('status'=>$status, 'message' => 'Sorry, roles cannot be empty!'));
				exit;
			}

			$this->agree($data, $ID_USER, $rolesPost);
		}else{
			$this->disagree($data, $ID_USER);
		}
	}
	

	function agree($data, $ID_USER, $rolesPost){
		$status = 'error';

		$data['ID_COMPANY'] = $this->input->post("ID_COMPANY");
		$data['ID_PLANT'] = $this->input->post("ID_PLANT");
		$data['ID_AREA'] = $this->input->post("ID_AREA");
		// $data['ISACTIVE'] = 'Y';
		$data['FLAG_ACTIVATION'] = '2';
		$key = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$encript = str_replace(array('$','/'), array('',''), password_hash((md5($key.strtotime(date('Ymd')))), PASSWORD_DEFAULT));
		$data['VERCODE'] = $encript;

		$compname = $data['NM_COMPANY'];
		unset($data['NM_COMPANY']);

		$param = array('mk_nopeg'=> trim(strtolower($data['NOPEG'])));
		$check = $this->employee->chekDataKaryawans($param);

		$nopeg = '';
		$ukname = '';
		if($check){
			$nopeg = $check->mk_nopeg;
			$ukname = $check->muk_nama;
		}

		$this->m_user->update($data,$ID_USER); #die($this->m_user->get_sql());
		// echo $this->db->last_query();
		if($this->m_user->error()){
			$message = $this->m_user->error();
		}
		else{
			$roles = array();
			$rolesPost = explode(',', $rolesPost);
			foreach ($rolesPost as $i => $v) {
				$roles[$i]['ID_USER'] = $ID_USER;
				$roles[$i]['ID_USERGROUP'] = $v;
			}
			$insBatch = $this->m_roles->insertBatch($roles); #die($this->m_user->get_sql());

			$roles = $this->m_roles->get_data_role_by_iduser($ID_USER);

			$otorisasi = array();
			foreach ($roles as $key => $value) {
				$otorisasi[] = $value->NM_USERGROUP;
			}

			$otorisasi = join($otorisasi, ', ');

			
			$url = base_url('requester/activation/'.$ID_USER.'/'.$encript);
			$dataMail = array(
				'url' => $url,
				'name' => $data['FULLNAME'], 
				'nopeg' => $nopeg, 
				'email' => $data['EMAIL'], 
				'ukname' => $ukname, 
				'compname' => $compname, 
				'otorisasi' => $otorisasi
			);

			// Menunjukkan kalau yang update adalah requester
			$status_msg = 'agreeadmin';
			if(isset($data['ISACTIVE'])){
				$status_msg = 'agreereq';
			}

			$send = $this->sendMail($dataMail, $status_msg);
			if($send){
				$status = 'success';
				$message = 'Users Data Updated';
			}else{
				$status = 'error';
				$message = 'Failed Update Data';
			}
		}

		$this->response(array('status'=>$status, 'message' => $message));

	}
	function disagree($data, $ID_USER){
		$status = 'error';

		$data['ISACTIVE'] = 'N';
		$data['DELETED'] = '1';
		$encript = 'DISAGREE-'.password_hash((md5($data['USERNAME'].$data['EMAIL'].strtotime(date('Ymd')))), PASSWORD_DEFAULT);
		$data['VERCODE'] = $encript;

		$compname = $data['NM_COMPANY'];
		unset($data['NM_COMPANY']);

		$param['mk_nopeg'] = $data['NOPEG'];
		$check = $this->employee->chekDataKaryawans($param);

		// var_dump($check);
		// exit;
		$nopeg = '';
		$ukname = '';
		if($check){
			$nopeg = $check->mk_nopeg;
			$ukname = $check->muk_nama;
		}

		$this->m_user->update($data,$ID_USER); #die($this->m_user->get_sql());
		if($this->m_user->error()){
			$message = $this->m_user->error();
		}
		else{

			$url = '';
			$dataMail = array(
				'url' => $url,
				'name' => $data['FULLNAME'], 
				'nopeg' => $nopeg, 
				'email' => $data['EMAIL'], 
				'ukname' => $ukname, 
				'compname' => $compname, 
				'id_company' => $data['ID_COMPANY'], 
				'otorisasi' => $otorisasi
			);
			// print_r($dataMail);
			// exit;
			$send = $this->sendMail($dataMail);
			if($send){
				$status = 'success';
				$message = 'Users Data Updated';
			}else{
				$status = 'error';
				$message = 'Failed Update Data';
			}
		}

		$this->response(array('status'=>$status, 'message' => $message));
	}

	private function sendMail($dataMail, $status = null){
		$this->load->library('email');
		$this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');


		$to = array();
		$cc = array();

		if($this->serverHost() == 'DEV'){
			//DEV---------------------------------------START
			$to = array('m.r.sucahyo2@gmail.com');
			$cc = array($mail_cc, '95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');
			//DEV---------------------------------------END
		}else{
			//PROD---------------------------------------START
			if(!$status == 'agreeadmin'){
				#Requester
				$to[] = $dataMail['email'];
			}else{
				#Requester admin per opco dan pak heri
				$mailAdmin = $this->m_user->getAdminEmail($dataMail['ID_COMPANY']);
				$mail_cc = $this->employee->chekKaryawans(trim(strtolower('HERI.PURNOMO')))->mk_email;

				$to[] = $mailAdmin;
				$cc[] = $mail_cc;
			}
			//PROD---------------------------------------END
		}

 
		$this->email->to($to);
		$this->email->cc($cc);
		$this->email->subject('QMO Approval');

		$this->email->message($this->templateMail($dataMail, $status));

		if($this->email->send()){
			$result = true;
		}else{
			$result = false;
		}

		return $result;
	}

	public function delete($ID_USER){
		$this->m_user->delete($ID_USER);
		if($this->m_user->error()){
			$this->notice->error($this->m_user->error());
		}
		else{
			$this->notice->success("Users Data Removed.");
		}
		redirect("user");
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}

	public function get_list($company = null){
		$list = $this->m_user->get_list($company, 'requester');
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USER;
			$row[] = $no;
			$row[] = $column->FULLNAME;
			$row[] = $column->USERNAME;
			$row[] = $column->NM_USERGROUP;
			$row[] = '<center><a href="./requester/approve/'.$column->ID_USER.'/'.$column->VERCODE.'"><i class="fa fa-check"></i> </a>';
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_user->count_all(),
            "recordsFiltered" => $this->m_user->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	function activate($id = null, $status = null){
		$this->m_user->activate($id, $status);
		if($this->m_user->error()){
			$this->response(array('status' => 'error', 'message' => $this->m_user->error()));
		}
		else{
			$this->response(array('status' => 'success', 'message' => 'Successful status change'));
		}

	}

	function templateMail($data = array('url' => '-', 'name' => '-', 'nopeg' => '-', 'email' => '-', 'ukname' => '-', 'compname' => '-', 'otorisasi' => '-'), $status = null){
		$isiemail = "";

		$alert			= "font-size: 16px; color: #fff; font-weight: 500; padding: 20px; text-align: center; border-radius: 3px 3px 0 0;";
		$body_wrap 		= "background-color: #f6f6f6; width: 100%;";
		$alert_good		= "background: #1ab394;";
		$alert_bad		= "background: #b31a1a;";
		$container 		= "display: block !important; max-width: 600px !important; margin: 0 auto !important; /* makes it centered */ clear: both !important;";
		$content 		= "max-width: 600px; margin: 0 auto; display: block; padding: 20px;";
		$content_wrap	= "padding: 20px;";
		$btn_primary 	= "text-decoration: none; color: #FFF; background-color: #1ab394; border: solid #1ab394; border-width: 5px 10px; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize;";
		$content_block	= "padding: 0 0 20px;";
		$main 			= "background: #fff; border: 1px solid #e9e9e9; border-radius: 3px;";
		$footer			= "width: 100%; clear: both; color: #999; padding: 20px;";
		
		$buttonAction = '';
		$message = 'Sorry, your request was not accepted.';
		$alert_color = $alert_bad;
		if($status == 'agreeadmin'){
			$alert_color = $alert_good;
			$buttonAction = '<a href="'.$data["url"].'" style="'.$btn_primary.'">Activation.</a>';
			$message = 'Your account creation request on qm has been successfully approved.<br>Now you can activate your account.';
		}elseif($status = 'agreereq'){
			$alert_color = $alert_good;
			$message = 'Account with this data successfully activated.';
		}

		$isiemail .= '
			<table style="'.$body_wrap.'">
				<tr>
					<td></td>
					<td style="'.$container.'" width="600">
						<div style="'.$content.'">
							<table style="'.$main.'" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="'.$alert . $alert_color.'">
										Notif: Request for creating a QM account.
									</td>
								</tr>
								<tr>
									<td style="'.$content_wrap.'">
										<table>
											<tr>
												<td colspan="3" class="'.$content_block.'">
													The following is information about people who want to have an account on QM.
												</td>
											</tr>
											<tr>
												<td width="100">Nama</td>
												<td>:</td>
												<td width="300">'.$data["name"].'</td>
											</tr>
											<tr>
												<td>Nopeg</td>
												<td>:</td>
												<td>'.$data["nopeg"].'</td>
											</tr>
											<tr>
												<td>Email</td>
												<td>:</td>
												<td>'.$data["email"].'</td>
											</tr>
											<tr>
												<td>Unit Kerja</td>
												<td>:</td>
												<td>'.$data["ukname"].'</td>
											</tr>
											<tr>
												<td>Perusahaan</td>
												<td>:</td>
												<td>'.$data["compname"].'</td>
											</tr>
											<tr>
												<td>Otorisasi</td>
												<td>:</td>
												<td>'.$data["otorisasi"].'</td>
											</tr>
										</table>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td style="'.$content_block.'"><br>
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'">
													'.$message.'
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'" align="center">
													'.$buttonAction.'
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'">
													Thank you for your time.
												</td>
											</tr>
										</table>
										
									</td>
								</tr>
							</table>
							<div style="'.$footer.'">
								<table width="100%">
									<tr>
										<td class="'.$content_block.'"><a href="http://qmo.semenindonesia.com">QMO</a> &copy; Quality Management Online.</td>
									</tr>
								</table>
							</div></div>
					</td>
					<td></td>
				</tr>
			</table>
		';
		
		return $isiemail;
	}
}	

?>
