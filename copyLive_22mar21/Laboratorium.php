<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_laboratorium","laboratorium");
    }
    
    public function index(){
		$this->list_company = $this->laboratorium->get_company();
        $this->list_data = $this->laboratorium->data_laboratorium();
        $this->template->adminlte("v_laboratorium");
    }
	
	public function do_add(){
		$save_act = $this->laboratorium->save_laboratorium();
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("laboratorium");
		} else {
			$this->notice->error($this->laboratorium->error());
			redirect("laboratorium");
		} 
	}
	
	public function do_edit(){
		$edit_act = $this->laboratorium->edit_laboratorium();
		if($edit_act){
			$this->notice->success("Data Edited.");
			redirect("laboratorium");
		} else {
			$this->notice->error($this->laboratorium->error());
			redirect("laboratorium");
		} 
	}
	
	public function do_delete($id_laboratorium){
		$del_act = $this->laboratorium->delete_laboratorium($id_laboratorium);
		if($del_act){
			$this->notice->success("Data Deleted.");
			redirect("laboratorium");
		} else {
			$this->notice->error($this->laboratorium->error());
			redirect("laboratorium");
		} 
	}

}

?>