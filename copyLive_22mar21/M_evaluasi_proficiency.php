<?php

class M_evaluasi_proficiency Extends DB_QM {

    private $column_search = array('T_PERIODE_PROFICIENCY.TITLE_PP', 'M_SAMPLE_UJI_PROFICIENCY.NAMA_SAMPLE');

    public function __construct(){
		$this->post = $this->input->post();
	}

    public function get_query(){
        $this->db->distinct();
        $this->db->select("T_PERIODE_PROFICIENCY.TITLE_PP, 
        T_PERIODE_PROFICIENCY.YEAR_PP, 
        T_PROFICIENCY.ID_PROFICIENCY, 
        T_PROFICIENCY.ID_KOMODITI, 
        M_SAMPLE_UJI_PROFICIENCY.NAMA_SAMPLE");
        $this->db->from("T_PERIODE_PROFICIENCY");
        $this->db->join("T_PROFICIENCY", "T_PERIODE_PROFICIENCY.ID_PP = T_PROFICIENCY.ID_PRIODE");
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", "T_PROFICIENCY.ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->join("T_HASIL_UJI_PROFICIENCY", "T_PROFICIENCY.ID_PROFICIENCY = T_HASIL_UJI_PROFICIENCY.ID_PROFICIENCY");
        $this->db->where("T_PERIODE_PROFICIENCY.DELETE_AT IS NULL");
        $this->db->where("T_PROFICIENCY.DELETED_AT IS NULL");
		$this->db->where("T_PROFICIENCY.FILE_PENGEMASAN IS NOT NULL");
        $this->db->where("T_PERIODE_PROFICIENCY.GROUP_PP", "Internal");
    }
    
    public function get_list() {
        $this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function get_kode_komoditi($id_komoditi = ""){
		if($id_komoditi != ""){
			$this->db->where("ID_SAMPLE", $id_komoditi);
		}
		$this->db->select("ID_SAMPLE, KODE_SAMPLE");
		$this->db->from("M_SAMPLE_UJI_PROFICIENCY");
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_hasil_uji($ID_PROFICIENCY, $QUEST){
		$sql = "SELECT
		A.ID_HASIL_UJI,
		A.KODE_LAB,
		A.ANSWER AS UJI_1,
		B.ANSWER AS UJI_2,
		round(( A.ANSWER + B.ANSWER ) / SQRT( 2 ), 4 ) AS SI,
		round(( A.ANSWER - B.ANSWER ) / SQRT( 2 ), 4) AS DI
	FROM
		(
	SELECT
		A.ID_HASIL_UJI,
		D.KODE_LAB,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
		JOIN M_LABORATORIUM D ON A.ID_LABORATORIUM = D.ID_LAB
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%1%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) A
		JOIN (
	SELECT
		A.ID_HASIL_UJI,
		D.KODE_LAB,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
		JOIN M_LABORATORIUM D ON A.ID_LABORATORIUM = D.ID_LAB
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%2%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI";

		$data = $this->db->query($sql);
		return $data->result_array();
	}

	public function get_median_uji($ID_PROFICIENCY, $QUEST){
		$sql = "SELECT MEDIAN(TO_NUMBER(UJI_1)) AS MED_UJI_1, MEDIAN(TO_NUMBER(UJI_2)) AS MED_UJI_2, MEDIAN(TO_NUMBER(SI)) AS MED_SI, MEDIAN(TO_NUMBER(DI)) AS MED_DI FROM (SELECT
		A.ID_HASIL_UJI,
		A.KODE_LAB,
		A.ANSWER AS UJI_1,
		B.ANSWER AS UJI_2,
		round(( A.ANSWER + B.ANSWER ) / SQRT( 2 ), 4 ) AS SI,
		round(( A.ANSWER - B.ANSWER ) / SQRT( 2 ), 4) AS DI
	FROM
		(
	SELECT
		A.ID_HASIL_UJI,
		D.KODE_LAB,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
		JOIN M_LABORATORIUM D ON A.ID_LABORATORIUM = D.ID_LAB
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%1%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) A
		JOIN (
	SELECT
		A.ID_HASIL_UJI,
		D.KODE_LAB,
		C.QUEST_TEXT,
		B.ANSWER 
	FROM
		T_HASIL_UJI_PROFICIENCY A
		JOIN T_DTL_HASIL_UJI_PROFICIENCY B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI
		JOIN M_DTL_FORM_PENGUJIAN C ON B.ID_QUESTION = C.ID_QUESTION 
		JOIN M_LABORATORIUM D ON A.ID_LABORATORIUM = D.ID_LAB
	WHERE
		ID_PROFICIENCY = '".$ID_PROFICIENCY."' 
		AND C.QUEST_TEXT LIKE '%".$QUEST."%' 
		AND C.QUEST_TEXT LIKE '%2%' 
	ORDER BY
		A.ID_HASIL_UJI ASC,
		C.ID_QUESTION ASC 
		) B ON A.ID_HASIL_UJI = B.ID_HASIL_UJI)";

		$data = $this->db->query($sql);
		return $data->result_array();
	}
}