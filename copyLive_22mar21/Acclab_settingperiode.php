<?php

class Acclab_settingperiode extends QMUser {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_acclab_settingperiode","acclab_periode");
		
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		// $this->list_gp = $this->acclab_periode->get_gp();
		$this->list_data = $this->acclab_periode->get_list();
		//print_r($this->list_data);
		//exit();
		
		$this->template->adminlte("v_acclab_settingperiode");
    }
	
	public function do_add(){
		$save_act = $this->acclab_periode->save();
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("acclab_settingperiode");  
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_settingperiode");
		}
	}
	
	public function do_edit(){
		$edit_act = $this->acclab_periode->edit();
		if($edit_act){
			$this->notice->success("Data Edited.");
			redirect("acclab_settingperiode");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_settingperiode");
		} 
	}
	
	public function do_edit_status(){
		$edit_act = $this->acclab_periode->edit_status();
		if($edit_act){
			$this->notice->success("Status Edited.");
			redirect("acclab_settingperiode");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_settingperiode");
		} 
	}
	
	public function do_delete($id_pp){
		$del_act = $this->acclab_periode->deleted($id_pp);
		if($del_act){
			$this->notice->success("Data Deleted.");
			redirect("acclab_settingperiode");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_settingperiode");
		} 
	}

}	

?>
