<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Report Score
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content" >
  
  <div class="row" >
    <div class="col-xs-12">
      <div class="box">
        <form method="POST">
          <!-- /.box-header -->
          <div class="box-body">
            <table  id="dt_tables"
              class="table table-striped table-bordered table-hover dt-responsive nowrap"
              cellspacing="0"
              width="100%">
              <thead>
                <tr>
                  <td colspan="<?php echo (8+$this->COUNT_COMPANY);?>">
                    <div class="form-group col-sm-12 col-sm-4 picker_daily">
                      <label for="ID_COMPANY">OPTION</label>
                      <SELECT style="width:auto;" class="form-control select2" name="ASPEK" id='id_jenis_aspek'>
                        <option value="ALL">ALL</option>
                        <?php
                          foreach ($this->JENIS_ASPEK as $aspek) {
                            $slk = ($this->ASPEK == $aspek->ID_JENIS_ASPEK) ? "selected":"";
                            echo "<option $slk value='".$aspek->ID_JENIS_ASPEK."'>" . $aspek->JENIS_ASPEK . "</option>";
                          }
                        ?>
                      </SELECT>
                    </div>
                    <div class="form-group col-sm-12 col-sm-2 picker_daily" >
                      <label for="ID_COMPANY">MONTH</label>
                      <SELECT style="width:auto;" class="form-control select2" name="MONTH" id='bulan'>
                        <?php for($i=1;$i<=12;$i++): ?>
                          <option value="<?php echo $i; ?>" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')==$i):date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                        <?php endfor; ?>
                      </SELECT>
                    </div>
                    <div class="form-group col-sm-12 col-sm-2">
                      <label for="ID_COMPANY">YEAR</label>
                      <SELECT style="width:auto;" class="form-control select2" name="YEAR" id='tahun'>
                      <?php for($i=2016;$i<=date("Y");$i++): ?>
                        <option <?php echo (($this->input->post('YEAR')!=NULL)? ($this->input->post('YEAR')==$i):date("Y")==$i) ? "selected":"";?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                      <?php endfor; ?>
                      </SELECT>
                    </div>
                    <div class="form-group col-sm-12 col-sm-2">
                      <label for="ID_COMPANY">&nbsp;</label>
                      <button type="submit" id='btLoad' class="form-control btn btn-primary"><i class="fa fa-search"> Load</i></button>
                    </div>
                    <div class="form-group col-sm-12 col-sm-2">
                      <label for="ID_COMPANY">&nbsp;</label>
                      <a id="btn-Convert-Html2Image" class="form-control btn btn-success" href="#"><i class="fa fa-picture-o"> Export</i></a>
                    </div>
                  </td>
                </tr>
             
              </thead>
              </table>
              <table style="background: #C2F1FF" class="table table-striped table-bordered table-hover dt-responsive nowrap html-content-holder"
              cellspacing="0"
              width="100%" height="100%">
               <thead>
                <tr>
                  <th colspan="11" align="center" style="text-align: center"><h2>Periode (<span id="titlePeriode">
                    <?= ($this->input->post('YEAR')!=NULL)? ($this->input->post('YEAR')==$i):''; ?> -
                    <?= ($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')==$i):''; ?>
                  </span>)</h2></th>
                </tr>
                <tr>
                  <th rowspan="2"  width="2%" style="vertical-align : middle;text-align:center;background-color:white">NO.</th>
                  <th rowspan="2"  width="30%"  style="vertical-align : middle;text-align:center;background-color:white">KRITERIA</th>
                  <th rowspan="2" width="30%" style="vertical-align : middle;text-align:center;background-color:white">BATASAN</th>
                  <?php
                    $json_builder =  array();
                    $dataChart = array();
                      foreach ($this->LIST_COMPANY as $company) {
                        $sTot  = 0;
                        $nilai = empty($this->NILAI['comp']) ? array(0):$this->NILAI['comp'][$company->ID_COMPANY];
                        $ctr   = 5*count($nilai);
                        if ($this->USER->ID_COMPANY==$company->ID_COMPANY || $this->USER->ID_COMPANY == NULL){
                            
                            $dataChart['name'] = $company->NM_COMPANY;
                            $dataChart['kode'] = $company->KD_COMPANY;
                            $dataChart['id'] = $company->ID_COMPANY;
                            
                          if ($this->ASPEK!="ALL") {
                            foreach ($nilai as $nl) {
                              $sTot +=$nl->NILAI_SKORING;
                            }
                            $color = '';
                            $total = $sTot/$ctr*$nl->BOBOT;
                            
                            $total = is_nan($total)?0:$total;
                            
                            $dataChart['color'] = $color;
                            $dataChart['nilai'] = $total;
                            $json_builder[] = $dataChart;
                          }else{
                            $vSCORE = ARRAY();
                            foreach ($this->JENIS_ASPEK as $aspek) {
                              $vSCORE[$aspek->ID_JENIS_ASPEK][BOBOT] = $aspek->BOBOT;
                              $vSCORE[$aspek->ID_JENIS_ASPEK][SCORE] = 0;
                              $vSCORE[$aspek->ID_JENIS_ASPEK][ITEM] = 0;								
                              foreach ($nilai as $nl) {
                                    if ($aspek->ID_JENIS_ASPEK==$nl->ID_JENIS_ASPEK) {
                                        $vSCORE[$aspek->ID_JENIS_ASPEK][SCORE] 	+=	$nl->NILAI_SKORING;
                                        $vSCORE[$aspek->ID_JENIS_ASPEK][ITEM] 	+=	1;
                                    }
                              }
                            }
                            $total = 0;
                            foreach($vSCORE as $r){
                              @$total += ($r[SCORE] / (5*$r[ITEM]) * $r[BOBOT]);
                            }

                            $total = is_nan($total)?0:$total;
                            
                            $color = "#";
                            if($total < 70 ) $color = "#FF6363";
                            if($total >= 70 && $total < 85) $color = "#2167FF"; 
                            if($total >= 85 && $total < 90) $color = "#8EFF80"; 
                            if($total >= 90) $color = "#FFD700"; 
                            $dataChart['color'] = $color;
                            $dataChart['nilai'] = $total;
                            $json_builder[] = $dataChart;
                            
                          }
                        }
                      }
                      
                      
                       foreach ($json_builder as $key => $row) {
                            $nama[$key]  = $row['nama'];
                            $nilai[$key] = $row['nilai'];
                            $color[$key] = $row['color'];
                        }
                        $nama  = array_column($json_builder, 'nama');
                        $nilai = array_column($json_builder, 'nilai');
                        array_multisort($nilai, SORT_DESC, $json_builder);
                      
                        // print_r($json_builder);
                        
                    $arr_color=array('#FFD700',' #C0C0C0','#CD853F','#EEEEEE','#008B8B');
                    $no =0;            
                    foreach ($json_builder as $company) {
                      // $color = company_color($company['kode']);
                      $color = $arr_color[$no];
                      $no+=1;
                      echo "<th style='background-color:$color;' colspan='2'><center style='color:black;text-shadow:1px 1px 1px #cccccc'>".$company['name'] . "</center></th>";
                    }
                  ?>
                </tr>
                <tr>
                  <?php
                    $arr_color_head=array('goldenrod',' #A5998E','tan','#CECECC','mediumseagreen');
                    $nom=0;
                    foreach ($json_builder as $company) {
                      $color = $arr_color_head[$nom];$nom+=1;
                      // $color = company_color($company['kode'],1);
                      echo "<th style='background-color:$color;'><center style='color:white;'>FIG.</center></th>";
                      echo "<th style='background-color:$color;'><center style='color:white;'>SKOR</center></th>";
                    }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 1;
                $arr_color_in=array('khaki',' #BAB2AB','lightsalmon','#DDDDD2','darkseagreen');
                foreach ($this->LIST_INPUT as $list) {
                  echo "<tr>";
                    // echo "<td style='background-color:".company_color(1).";'>$no</td>";
                    // echo "<td style='background-color:".company_color(1).";'>".$list->KRITERIA."</td>";
                    echo "<td style='background-color:lightcyan;'>$no</td>";
                    echo "<td style='background-color:lightcyan;'>".$list->KRITERIA."</td>";
                    echo "<td style='background-color:lightgreen;'>".$list->BATASAN."</td>";
                    #echo "<td>";
                      #echo $list->BATASAN;
                      echo "<input name='ID_BATASAN[]' value='".$list->ID_BATASAN."' type='hidden'>";
                    #echo "</td>";
                $urut=0;
                    foreach ($json_builder as $company) {
                        
                      // $color = company_color($company['kode'],2);
                      $color = $arr_color_in[$urut];$urut+=1;
                      $nilai = empty($this->NILAI['comp']) ? array(0):$this->NILAI['comp'][$company['id']];
                      $bts = $list->ID_BATASAN;
                      $fig = array_filter($nilai, function ($e) use ($bts) {
                        return $e->ID_BATASAN == $bts;
                      });
                      echo "<td style='background-color:$color;' align='center'>";
                        echo "<span class='opco_".$company['id']."' id='NILAI_".$list->ID_BATASAN.$company['id']."'>";
                          //Figure
                          //var_dump($fig);
                          foreach ($fig as $fNilai) {
                            if ($this->USER->ID_COMPANY==$company['id'] || $this->USER->ID_COMPANY == NULL){
                              echo ($fNilai->NILAI_ASPEK=='') ? '':round($fNilai->NILAI_ASPEK,2);
                            }
                          }
                        echo "</span>";
                      echo "</td>";
                      echo "<td style='background-color:$color;' align='center'>";
                        echo "<span class='opcox_".$company['id'].$list->ID_JENIS_ASPEK."' id='NILAIX_".$list->ID_BATASAN.$company['id']."'>";
                          //Score
                          foreach ($fig as $fNilai) {
                            if ($this->USER->ID_COMPANY==$company['id'] || $this->USER->ID_COMPANY == NULL){
                              $red = ($fNilai->NILAI_SKORING <= 2) ? "red":"blue";
                              echo "<font color='$red'>".$fNilai->NILAI_SKORING."</font>";
                            }
                          }
                        echo "</span>";
                      echo "</td>";
                    }
                  echo "</tr>";
                  $no++;
                } 
                ?>
                <tr>
                  <th colspan="3" valign="middle" width="2%">PEROLEHAN NILAI</th>
                  <?php
                       foreach ($json_builder as $key => $row) {
                                   
                            echo "<td colspan='2' align='center' bgcolor='".$row['color']."'>";
                              echo "<span class='total_".$row['id']."'>";  
                              echo "<font color=black><b>".round($row['nilai'],2)."</b></font>";
                              echo "</span>";
                            echo "</td>";
                       };
                        
                  ?>
                </tr>
              </tbody>
            </table>
            <br />
            <div style="width:100%;height:140%;display:none" >
                <div id="previewImage" >
                </div>
            </div>
          <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<script>
        
     var element = $(".html-content-holder"); // global variable
        var getCanvas; // global variable
        
  $(document).ready(function(){
      $("#btn-Convert-Html2Image").click(function(){
                var imgageData = getCanvas.toDataURL("image/png");
                var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                $("#btn-Convert-Html2Image").attr("download", "Report Score SI Ramah.png").attr("href", newData);
        });
      });
    var tahun = $('#tahun option:selected').text();
    var bulan = $('#bulan option:selected').text();

    $('#titlePeriode').text(bulan+' '+tahun);
   
         window.onload = function(e)
            {
                html2canvas(element, {
                 onrendered: function (canvas) {
                        $("#previewImage").html(canvas);
                        getCanvas = canvas; 
                     },
                     // 1085 1092
                    width:1300,
                    height:1300,
                 });
            };
  
               
                
  
</script>
