<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Blind Test Form Configuration
    <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box" id="entryBlindTest">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Form Setting Blind test</h3>
            <div class="input-group input-group-sm" style="width: 40px; float: right;">
              <!-- <a id="CancelEntry" type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a> -->
            </div>
          </div>
        </div>
        <div class="box-body">
          <div class="row col-sm-8">
            <div class="box-body" id="forminputblind" style="">
              <div class="box box-warning box-solid col-sm-12 col-md-12 col-lg-12">
                <div class="box-header with-border">
                  <h3 class="box-title"> ENTRY DATA DIBAWAH INI <i class="fa fa-hand-o-down"></i> </h3>
                </div>
                <div class="box-body" style="">
                  <div id="example1" class="hot handsontable htColumnHeaders" style="margin-left: 15px; margin-right: : 15px; max-height: 350px !important;"></div>
                  <div class="form-group row" style="margin-left: 15px; margin-right: : 15px;">
                    <div class="col-sm-2" style="">
              <?php if($this->PERM_WRITE): ?>
                      <label for="addNewRow"></label>
                      <a  style="display: none;" name="addNewRow" id="addNewRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-success btn-sm addNewRow"><i class="fa fa-plus"></i> Row</a>

              <?PHP endif; ?>
                    </div>
                    <div class="col-sm-2" style="">
              <?php if($this->PERM_WRITE): ?>
                      <label for="delLastRow"></label>
                      <a style="display: none;" name="delNewRow" id="delLastRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-danger btn-sm delLastRow"><i class="fa fa-trash"></i> Row</a>

              <?PHP endif; ?>
                    </div>
                    <div class="col-sm-2" style="float: right;">
              <?php if($this->PERM_WRITE): ?>
                      <a  name="savesample" id="savesample" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> &nbsp;Save</a>

              <?PHP endif; ?>
                    </div>
                    <!-- <div class="col-sm-2" style="float: right; margin-top: 25px;">
                      <a  name="replacesample" id="replacesample" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-primary btn-sm"><i class="fa fa-refresh"></i> &nbsp;Replace</a>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row col-sm-8">
            <div class="box-body" style="margin-left: 5px; ">
              <div class="box box-danger box-solid col-sm-12 col-md-12 col-lg-12">
                <div class="box-header with-border">
                  <h3 class="box-title"> Blind Test Form Preview </h3>
                  <div class="box-tools pull-right">
                    <!-- <a id="SubmitBlind" onclick="submitsample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a>  -->
                    <a id="ReloadDataBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                  </div>
                </div>
                <div class="box-body" style="">
                  <table  id="blind_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">URUTAN</th>
                        <th width="50%">KOMPONEN</th>
                        <th width="*">GROUP</th>
                        <th width="*">SATUAN</th>
                        <th width="5%">ACTION</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="box-footer">
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</section>
<!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<link href="https://cdn.datatables.net/rowgroup/1.1.2/css/rowGroup.dataTables.min.css" rel="stylesheet">

<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="https://cdn.datatables.net/rowgroup/1.1.2/js/dataTables.rowGroup.min.js"/></script>


<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script type="text/javascript">
  
  function receiptsample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menerima sample sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_settingblindtest/receipt_blindtest");?>",
        data: {"FK_ID_PERIODE": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }
  
  function deletesample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menghapus data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_settingblindtest/delete_formblindtest");?>",
        data: {"ID": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadDataBlind").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }   
          reloadblindtabel();
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }
  
  function submitsample() {
    // body...
    var r = confirm("Apakah anda yakin untuk submit data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_settingblindtest/submit_blindtest");?>",
        data: {"ID": $('#id_setup').text(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#CancelEntry").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }

$(document).ready(function(){

  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable; 
  var oBlindTable;
  var glbproduk = new Array();
  loadtabelinput();
              reloadblindtabel();

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });
  $(document).on('click',"#ReloadDataBlind",function () {
  	reloadblindtabel();
  });
  
  $(document).on('click',"#CancelEntry",function () {
    $('#entryBlindTest').hide();
    $('#viewArea').show();
    $("#ReloadData").click(); 
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  function reloadblindtabel(action = true) {
    // body...

    oBlindTable = $('#blind_tables').dataTable({    	
      "paging":   false,
      destroy: true,
      processing: true,
      //   serverSide: true,
      select: true,
      "ajax": {
        "url": "<?php echo site_url("acclab_settingblindtest/ajax_get_formblindtest");?>/"+action,
        "type": "POST"
      },
      rowGroup: {dataSrc: 2}
    });
  }

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    // $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("acclab_settingblindtest/blindtest_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "RNUM", "width": 50},
        {"data": "NAMA"},
        {"data": "NAMA_LAB"},
        {"data": "TAHUN"},
        {"data": "SETUP_START"},
        {"data": "SETUP_END"},
        {"data": "NAMA_PIC_PLANT"},
        {"data": "TANGGAL_TERIMA"},
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){

            if (index['STATUS_ENTRY'] == 0) {
              return '<button title="Terima Sample" class="btTerima btn btn-warning btn-xs" type="button" onclick="receiptsample('+index['ID']+')"><i class="fa fa-download"></i> Terima Blind Test</button>';
            } else if (index['STATUS_ENTRY'] == 1) {
              return '<button title="Entry Blind Test" class="btEntry btn btn-success btn-xs" type="button"><i class="fa fa-edit"></i> Entry Blind Test</button>';
            } else if (index['STATUS_ENTRY'] == 2) {
              return '<button title="Detail Blind Test" class="btDetail btn btn-primary btn-xs" type="button"><i class="fa fa-document"></i> Detail Blind Test</button>';
            } else {
              return '';
            }

          }
        },
      ]
    });
    
    $('#ReloadData').show();
  }

$(document).on('click', ".btEntry", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    $('#SubmitBlind').show();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    loadtabelinput();
    reloadblindtabel(data['ID']);
});

$(document).on('click', ".btDetail", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    // console.log(data);
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').hide();    

    $('#SubmitBlind').hide();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    // loadtabelinput();
    reloadblindtabel(data['ID'], false); 
});

  var example1 = document.getElementById('example1'), hot1;
  var oglbcomponent = new Array();
  var oglbcomponentid = new Array();
  var oglbcomponentname = new Array();

  document.querySelector('.addNewRow').addEventListener('click', function() {
    hot1.alter('insert_row', hot1.countRows());
  });

  document.querySelector('.delLastRow').addEventListener('click', function() {
    hot1.alter('remove_row', hot1.countRows() - 1);
  });

    function getListComponent() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_settingblindtest/get_component");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_COMPONENT']);

          }
        }else{
        }
      });
      return datal;
    }

  function getContentData() {
      return [
      ];
    }

    function getRowOptions() {
      var initial_coloptions = [
        {
          data: 'produk',
          type: 'autocomplete',
          source: getListComponent(),
          strict: true,
          allowInvalid: false,
          colWidths: 250,
          visibleRows: 10
        },{
          data: 'group',
          type: 'autocomplete',
          source: ['CHEMICAL TESTING', 'PHYSICAL TESTING'],
          strict: true,
          allowInvalid: false,
          colWidths: 150,
          visibleRows: 10
        },
        {
          type: 'text',
          colWidths: 100,
        },
        {
          type: 'numeric',
          colWidths: 100,
        }
      ];
      return initial_coloptions;
    }

    function getColHeader() {
      var columnlist = ['Component', 'Group', 'Satuan', 'Urutan'];
      return columnlist;
    }

    function getTypeProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_settingblindtest/ajax_get_type_product");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['KD_PRODUCT']);
          }
        }else{
        }
      });
      
      return datal;
    }

    function getTypeProduct(merk) {
      return glbproduk[merk];
    }

    function getListArea() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_KOTA']);
          }
        }else{
        }
      });
      return datal;
    }

  function loadtabelinput() {

    if (hot1) {

      hot1.destroy();

    }   

    $.getJSON('<?php echo site_url("acclab_settingblindtest/ajax_get_component_display");?>', function (result) {
      var values = result;
      if (values != undefined && values.length > 0) {
        for (var i = 0; i < values.length; i++) {
          if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
            oglbcomponent.push(values[i]['KD_COMPONENT']);
            oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
            oglbcomponentname.push(values[i]['KD_COMPONENT']);
          }
        }
        hot1 = new Handsontable(example1, {
          data: getContentData(),
          height: 250,
          autoColumnSize : true,
          fixedColumnsLeft: 3,
          manualColumnFreeze: true,
          manualColumnResize: true,
          colHeaders: getColHeader(),
          columns: getRowOptions(),
        });
        hot1.alter('insert_row', hot1.countRows());

        hot1.updateSettings({
          afterChange: function(changes, src) {
            var row = changes[0][0],
              col = changes[0][1],
              newVal = changes[0][3];
          }
        })

        $('#delLastRow').show();
        $('#addNewRow').show();
        $('#savesample').show();
        // $('#savesample, #replacesample').show();
      }else{
      }
    });

  }

   $(document).on('click',"#savesample",function () {
      saveBlindTest();
  });


    // $('button[name=savesample]').click(function () {
    //   saveBlindTest();
    // });

    function saveBlindTest() {
        $body = $("body");
        $body.addClass("loading");
        $body.css("cursor", "progress");

      $('#savesample').button('loading');

        // var companyid = $("#oCOMPANYn").val();
        // var companyname = $("#oCOMPANYn option:selected").text();
        var handsonData = hot1.getData();
        $.ajax({
          url: "<?php echo site_url("acclab_settingblindtest/save_form_blindtest");?>",
          data: {"user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData, "id_setup": $('#id_setup').text(), "id_periode": $('#id_periode').text()}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              if (res.msg == 'success') {
                // $("#a-notice-success").click();
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
                var rowcount = hot1.countRows();
                for (var i = 0; i < rowcount; i++) {
                  hot1.alter('remove_row', hot1.countRows() - 1);
                }               
                hot1.alter('insert_row', hot1.countRows());
              }
              else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              $('#savesample').button('reset');
              loadtabelinput();
              reloadblindtabel();
          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();

              $('#savesample').button('reset');
          }
        });

    }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
