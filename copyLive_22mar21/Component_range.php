<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Component_range extends QMUser {

	public $id_component;
	public $list_component = array();

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("C_range_component");
	}
	
	public function index(){
		$this->template->adminlte("v_component_range");
	}

	public function get_list(){
		$list = $this->C_range_component->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->ID_COMPONENT;
			$row[] = $no;
			$row[] = $field->KD_COMPONENT;
			$row[] = $field->NM_COMPONENT;
			$row[] = (float) $field->V_MIN;
			$row[] = (float) $field->V_MAX;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->C_range_component->count_all(),
            "recordsFiltered" => $this->C_range_component->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function edit(){
		$this->id_component		= $this->input->post('id_component');
		$this->list_component 	= (!empty($this->id_component)) ? $this->C_range_component->get_id($this->id_component):$this->C_range_component->get_list();
		$this->template->adminlte("v_component_range_edit");
	}

	public function create(){
		$id_component = $this->input->post('id_component');
		
		for ($i=0 ; $i < count($id_component); $i++ ) { 
			$v_min = $this->input->post("v_min_$id_component[$i]");
			$v_max = $this->input->post("v_max_$id_component[$i]");



			$data['ID_COMPONENT'] = $id_component[$i];
			$data['V_MIN'] = $v_min;
			$data['V_MAX'] = $v_max;
			$exists = $this->C_range_component->where_id_nojoin($data['ID_COMPONENT']);
			
			if (!$exists) {
				$this->C_range_component->insert($data);
			}else{
				unset($data['ID_COMPONENT']);
				$this->C_range_component->update($data, $id_component[$i]);
				
			}
		}

		#echo $this->db->last_query();die();
		$this->notice->success("Configuration Saved");
		redirect("component_range");
	}
}

/* End of file Component_range.php */
/* Location: ./application/controllers/Component_range.php */
?>
