<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  QAF CLINKER PROGRESS
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
    
   <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" class="col-md-12 row">
            <div class="form-group">
              <div class="col-sm-4">
                <div class="row">
                  <div class="col-sm-8 ">
                    <label class="control-label" >Company</label>
                    <select id="opt_company" name="opt_company" class="form-control select2">
                        <option value="">Choose Company...</option>
                      <?php  foreach($this->list_company as $company):?>
                        <option value="<?php echo $company->ID_COMPANY;?>"><?php echo $company->NM_COMPANY;?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <label class="control-label" >Year</label><br>
                    <select class="form-control select2" name="YEAR" style="width:100%" id='tahun'>
                    <?php for($i=2016;$i<=date("Y");$i++): ?>
                      <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                    <?php endfor; ?>
                    </select>
                </div>
              </div>
            </div>
 
            <div class="form-group">
              <div class="col-sm-4">
                <label class="control-label" >Plant</label>
                <input type="checkbox" id="checkallplant" title="Select All" class="pull-right">

                <select multiple="" class="form-control input-sm select2" style="width: 100%" name="id_plant" id="id_plant">
                    <option value="">Choose Plant...</option>
                </select>
              </div>
            </div>
           <!-- <div class="form-group">
              <div class="col-sm-4">
                <label class="control-label" >Product</label>
                <select  multiple="" id="id_product" name="id_product" class="form-control select2">
                </select>
              </div>
            </div> -->
            <div class="form-group">
              <div class="form-group col-sm-12">
                <hr/> 
                <button class="btn-primary btn btn-sm btn-icon" name="load" id="btLoad"><i class="fa fa-eye"></i> Load</button>
              </div>
            </div>
            
          </form>
          <div class="col-md-12 row">
            <div id="divTable" style="width:70%;display:none;margin:auto;">
              <div id="boxPlot_div" class="boxPlot_div"></div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<!-- /.content -->

<a  id="a-notice-error"
  class="notice-error"
  style="display:none";
  href="#"
  data-title="Alert"
  data-text=""
></a>
<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  .boxPlot_div { margin:auto;margin-bottom:10px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 ) 
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>') 
                  50% 50% 
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;   
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
  $(document).ready(function(){
  $("#checkallplant").click(function(){
    if($("#checkallplant").is(':checked') ){
        $("#id_plant > option").prop("selected","selected");
        $("#id_plant").trigger("change");
    }else{
        $("#id_plant > option").removeAttr("selected");
         $("#id_plant").trigger("change");
     }
  });


    $('.select2').select2();


  $('#opt_company').on('change', function(){
    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
  }); 

  function listPlant(idCompany = ''){

      $.ajax({
        url : '<?=base_url("login/getPlant/")?>'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#id_plant').html('');
        $.map( data, function( val, i ) {
          selected = '';
          if(val.ID_PLANT != ''){
            // selected = 'selected';
            $('#id_plant').append('<option value="'+val.ID_PLANT+'" '+selected+'>'+val.NM_PLANT+'</option>');
          }
        });
      });

      $('.select2').select2();
  }

  $('#id_plant').on('change', function(){
    idPlant = $(this).val();
    listProduct(idPlant);
  }); 

  function listProduct(idPlant = ''){
    $.post( "<?=base_url("hebat_graph/async_list_product_qaf")?>", { id_plant: idPlant})
    .done(function(data){
        data = JSON.parse(data);
        $('#id_product').html('');
        $.map( data, function( val, i ) {
          console.log(val);
          selected = '';
          if(val.ID_PRODUCT != ''){
            // selected = 'selected';
            $('#id_product').append('<option value="'+val.ID_PRODUCT+'" '+selected+'>'+val.KD_PRODUCT+'</option>');
          }
        });
      });

      $('.select2').select2();
  }
    
    $("#btLoad").click(function(event){
      event.preventDefault();

      /* Animation */
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");

      /* Initialize Vars */

      $.ajax({
          type: "POST",
          dataType: "json",
          data: {
            opt_company: $("#opt_company").val(),
            tahun: $("#tahun").val(),
            id_plant: $("#id_plant").val(),
          },
          url: '<?php echo site_url("hebat_graph/json_line/clinker");?>',
          success: function(res){
            $body.removeClass("loading");
            $body.css("cursor", "default");          

            if (res.result == 200) {
              //console.log(res);
              $("#divTable").css('display','');
              
              BoxPlot = document.getElementById('boxPlot_div');
              Plotly.purge(BoxPlot);
              Plotly.plot(BoxPlot, {
                data: res.data,
                layout: res.layout
              });              
            }else{
              $("#divTable").css('display','none');
              $("#a-notice-error").data("text", 'Oops! No data found');
              $("#a-notice-error").click();
            }


          },
          error: function(jqXHR, textStatus, errorThrown) {
            $body.removeClass("loading");
            $body.css("cursor", "default");
            $("#a-notice-error").data("text", 'Oops! Something went wrong.<br>Please check message in console');
            $("#a-notice-error").click();

            console.log("XHR: " + JSON.stringify(jqXHR));
            console.log("Status: " + textStatus);
            console.log("Error: " + errorThrown);
          }
        });
    });

    /** Notification **/
    $(".notice-error").confirm({ 
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });
  });
</script>