<?php

class Technician extends QMUser {
	
	public $list_data = array();
	public $data_technician;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_technician");
	}
	
	public function index(){
		$this->list_technician = $this->m_technician->datalist();
		$this->template->adminlte("v_technician");
	}
	
	public function add(){
		$this->template->adminlte("v_technician_add");
	}
	
	public function create(){
		$this->m_technician->insert($this->input->post());
		if($this->m_technician->error()){
			$this->notice->error($this->m_technician->error());
			redirect("technician/add");
		}
		else{
			$this->notice->success("Technician Data Saved.");
			redirect("technician");
		}
	}
	
	public function edit($ID_TECHNICIAN){
		$this->data_technician = $this->m_technician->get_data_by_id($ID_TECHNICIAN);
		$this->template->adminlte("v_technician_edit");
	}
	
	public function update($ID_TECHNICIAN){
		$this->m_technician->update($this->input->post(),$ID_TECHNICIAN);
		if($this->m_technician->error()){
			$this->notice->error($this->m_technician->error());
			redirect("technician/edit/".$ID_TECHNICIAN);
		}
		else{
			$this->notice->success("Technician Data Updated.");
			redirect("technician");
		}
	}
	
	public function delete($ID_TECHNICIAN){
		$this->m_technician->delete($ID_TECHNICIAN);
		if($this->m_technician->error()){
			$this->notice->error($this->m_technician->error());
		}
		else{
			$this->notice->success("Technician Data Removed.");
		}
		redirect("technician");
	}
}	

?>
