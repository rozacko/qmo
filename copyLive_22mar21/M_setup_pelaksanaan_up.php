<?php

class M_setup_pelaksanaan_up Extends DB_QM {
	
	public function data_comodity(){
		$this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->result();
	}
	
	public function data_pic(){
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			right join M_ROLES mr on mu.id_user = mr.id_user and mr.ID_USERGROUP = 65
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->result();
	}
	
	public function data_periode(){
		$this->db->where("DELETE_BY IS NULL");
		$this->db->where("GROUP_PP != 'Internal'");
		$this->db->order_by("YEAR_PP","DESC");
		return $this->db->get("T_PERIODE_PROFICIENCY")->result();
	}
	
	public function get_list($id_pp = false){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, MU.FULLNAME, TPP.*");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->where("TPP.GROUP_PP != 'Internal'");
		if($id_pp != null){
			$this->db->where("TP.ID_PRIODE", $id_pp);
		}
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
    }
    
    public function save_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		 = $this->input->post('ID_PP');
		$id_comodity = $this->input->post('ID_KOMODITI');
		$id_pic		 = $this->input->post('ID_PIC');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$this->db->set("ID_PIC", $id_pic);
		$this->db->set("ID_PRIODE", $id_pp);
		$this->db->set("ID_KOMODITI", $id_comodity);
		$this->db->set("CREATED_AT","TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS')", false);
		$this->db->set("CREATED_BY", $user_in->FULLNAME);
		$q = $this->db->insert("T_PROFICIENCY");

		return $q;
	}
	
	public function update_proficiency(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		 = $this->input->post('ID_PP');
		$id_proficiency = $this->input->post('ID_PROFICIENCY');
		$id_comodity = $this->input->post('ID_KOMODITI');
		$id_pic		 = $this->input->post('ID_PIC');
		
		$this->db->set("ID_PIC", $id_pic);
		$this->db->set("ID_PRIODE", $id_pp);
		$this->db->set("ID_KOMODITI", $id_comodity); 
		$this->db->set("UPDATED_AT","CURRENT_DATE", false);
		$this->db->set("UPDATED_BY", $user_in->FULLNAME);
		$this->db->where("ID_PROFICIENCY",$id_proficiency);
		$q = $this->db->update("T_PROFICIENCY");

		return $q;
	}
	
	public function hapus_proficiency($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$this->db->set("DELETED_AT","CURRENT_DATE", false);
		$this->db->set("DELETED_BY", $user_in->FULLNAME);
		$this->db->where("ID_PROFICIENCY",$id);
		$q = $this->db->update("T_PROFICIENCY");
		
		if($q){
			$this->db->set("DELETED_AT","CURRENT_DATE", false);
			$this->db->set("DELETED_BY", $user_in->FULLNAME);
			$this->db->where("ID_PROFICIENCY",$id);
			$query = $this->db->update('T_ACTIVITY');
		}
		return $query;
	}
	
	public function get_data_by_id($id){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, TPP.*");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->where("TP.ID_PROFICIENCY", $id);
		$this->db->order_by("TP.CREATED_AT");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->row();
	}
	
	// timeline get data
	public function get_data_activity($id){
		$this->db->where("ID_PROFICIENCY", $id);
		$this->db->where("DELETED_BY IS NULL");
		$this->db->order_by("PLAN_DATE_START");
		$query = $this->db->get('T_ACTIVITY');
		return $query->result();
	}
	
	public function save_activity($id_proficiency){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		
		$id = (int)$id_proficiency;
		
				$start_in 	= date("Y-m-d", strtotime($start));
			
				$date_now 	= date("Y-m-d");
				$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
				$sql = "
					INSERT INTO T_ACTIVITY (ID_PROFICIENCY, ACTIVITY_NAME, PLAN_DATE_START, CREATED_AT, CREATED_BY) VALUES ('{$id}', '{$activity}', TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}')
				";
				$q = $this->db->query($sql);
			
		return $q;
	}
	
	public function update_activity(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_activity = $this->input->post('ID_ACTIVITY');
		$activity = $this->input->post('activity');
		$start 	  = $this->input->post('start_date');
		
		$start_in 	= date("Y-m-d", strtotime($start));
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_ACTIVITY SET ACTIVITY_NAME = '{$activity}', PLAN_DATE_START = TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id_activity}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function hapus_activity($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			UPDATE T_ACTIVITY SET DELETED_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), DELETED_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
    
}

?>