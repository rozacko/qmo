<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Commodity extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_commodity","commodity");
    }
    
    public function index(){
        $this->list_data = $this->commodity->data_comodity();
        $this->template->adminlte("v_commodity");
    }
	
	public function do_add(){
		$save_act = $this->commodity->save_commodity();
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("commodity");
		} else {
			$this->notice->error($this->commodity->error());
			redirect("commodity");
		} 
	}
	
	public function do_edit(){
		$edit_act = $this->commodity->edit_commodity();
		if($edit_act){
			$this->notice->success("Data Edited.");
			redirect("commodity");
		} else {
			$this->notice->error($this->commodity->error());
			redirect("commodity");
		} 
	}
	
	public function do_delete($id_sample){
		$del_act = $this->commodity->delete_commodity($id_sample);
		if($del_act){
			$this->notice->success("Data Deleted.");
			redirect("commodity");
		} else {
			$this->notice->error($this->commodity->error());
			redirect("commodity");
		} 
	}

}

?>