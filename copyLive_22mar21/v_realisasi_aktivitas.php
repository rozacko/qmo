 <!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
        Update Realisasi Aktivitas pelaksanaan
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
		
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
			
		<?php if($notice->error): ?>

			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>

		<?php endif; ?>
		
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Periode Proficiency: </b> <?= $this->data_detil->TITLE_PP;?> [<?= $this->data_detil->GROUP_PP;?> - <?= $this->data_detil->YEAR_PP;?>] | <b>Commodity: </b> <?= $this->data_detil->NAMA_SAMPLE; ?> | <b>Lab: </b> <?= $this->data_detil->NAMA_LAB; ?> | <b>PIC: </b> <?= $this->data_detil->FULLNAME; ?></h3>
			  
            </div>
            <!-- /.box-header -->
           
              <div class="box-body" >
			  
              <!-- /.box-body -->
			  <div class="col-sm-12 ">
				<label>Activity Schedule Pelaksanaan</label>
				<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap">
					<thead>
						<tr>
							<th>No.</th>
							<th>Activity</th>
							<th>Planning</th>
							<th>Realisasi</th>
							<th></th>
						</tr>
					</thead>
					<tbody style="font-weight: normal;">
					 <?php 
						$count = 1;
						foreach ($this->data_activity as $dt) { ?>
						<tr>
							<td><?= $count++; ?></td>
							<td><?= $dt->ACTIVITY_NAME; ?></td>
							<td><?= $dt->PLAN_DATE_START; ?> s/d <?= $dt->PLAN_DATE_END; ?></td>
							<td>
								<?php 
									if($dt->ACT_DATE_START == null or $dt->ACT_DATE_END == null){
										echo "-";
										$out1 = null;
										$out2 = null;
									} else {
										echo $dt->ACT_DATE_START." s/d ".$dt->ACT_DATE_END;
										// format date start
										$date1 = $dt->ACT_DATE_START;
										$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
										$out1 = $dateObj1->format('Y-m-d');
									// format date end
										$date2 = $dt->ACT_DATE_END;
										$dateObj2 = DateTime::createFromFormat('d-M-y', $date2);
										$out2 = $dateObj2->format('Y-m-d');
									}
								?> 
							</td>
							<td><center>
							 <?php if($this->PERM_WRITE): ?>
                      <button title="Edit" class="btEdit btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID_ACTIVITY;?>"><i class="fa fa-pencil-square-o"></i> </button>
                    <?php endif; ?>
							</center></td>
						</tr>
						
<!-- Modal edit activity -->
<div id="editModal<?= $dt->ID_ACTIVITY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("aktivitas_pelaksanaan/do_edit_activity") ?>" >
    <!-- Modal content-->
	 
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Update Realisasi Activitas Pelaksanaan:</b> </h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="ID_PRO" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<input type="hidden" name="ID_ACTIVITY"  value="<?= $dt->ID_ACTIVITY;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-6 clearfix">
					<label>Activity </label>
					<input type="text" class="form-control" name="activityn" placeholder="Activity name" value="<?= $dt->ACTIVITY_NAME; ?>" disabled>
				  </div>
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date" value="<?= $out1;?>">
				  </div>
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date" value="<?= $out2; ?>">
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

					<?php } ?>
					</tbody>
				</table>
			  </div>

             
            </form>
			
			
			<div class="gantt col-md-12" style="font-size: 12px;"></div>
			</div>
          </div>

    </section>

<link href="http://taitems.github.io/jQuery.Gantt/css/style.css" type="text/css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="http://taitems.github.io/jQuery.Gantt/js/jquery.fn.gantt.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js"></script>

<script>
 $(function() {
            "use strict";
			
			 $.ajax({
                type:"GET",
                url:"<?php echo site_url("aktivitas_pelaksanaan/timeline_act/") ?><?= $this->data_detil->ID_PROFICIENCY; ?>",
                dataType: "json",   
                success:function(data_in){
                    
					$(".gantt").gantt({
						source: data_in,
						navigate: "scroll",
						scale: "weeks",
						maxScale: "months",
						minScale: "days",
						itemsPerPage: 20,
						scrollToToday: false,
						useCookie: false,
						onItemClick: function(data) {
							//alert("Item clicked - show some details");
						},
						onAddClick: function(dt, rowId) {
							//alert("Empty space clicked - add an item!");
						},
						onRender: function() {
							if (window.console && typeof console.log === "function") {
								console.log("chart rendered");
							}
						}
					});

					$(".gantt").popover({
						selector: ".bar",
						title: function _getItemText() {
							return this.textContent;
						},
						container: '.gantt',
						//content: "",
						trigger: "hover",
						placement: "auto right"
					});
					prettyPrint();
					
                },
                 error: function (xhr, ajaxOptions, thrownError) { 
                    alert(xhr.responseText);
                }
            });

        });
</script>




<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->

<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<script type="text/javascript">
    $(document).ready(function() {
	  $('select').selectize({
          sortField: 'text'
      });
		
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
		  
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>

<script>

$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>