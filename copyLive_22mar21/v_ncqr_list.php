<style type="text/css">
/*  thead {
    background: #f4e5e5;
}*/
td.btnDetail{
    -webkit-transition: width 2s;
    transition: width 2s;
}
td.btnDetail:hover {
    background: #dd4b39;
    color: #fff;
}
tbody {
    font-weight: normal;
}
span#nm_status {
    padding: 23px;
    margin: 0 40px;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    NCQR List
    <small>Information of NCQR Incident.</small>
  </h1>
</section>

<section class="content">
  <!-- SELECT2 EXAMPLE -->
  <div class="box box-default">
    <div class="box-header with-border">

      <div class="row">
        <div class="col-md-8">
          <form id="formData">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Company</label>
                  <select id="id_company" name="id_company" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                    <?php
                    echo '<option value="">Choose company...</option>';
                    foreach ($this->list_company as $i => $iv) {
                      echo '<option value="'.$iv->ID_COMPANY.'">'.$iv->NM_COMPANY.'</option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Plant</label>
                  <input type="checkbox" id="checkAllPlant" title="Select All" class="pull-right">
                  <select id="id_plant" name="id_plant" multiple="multiple" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Periode</label>
                  <input id="periode" name="periode" value="<?=date('m/Y')?>" type="text" name="periode" class="form-control datepicker pointer" readonly="" style="background-color: #fff;">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Status</label>
                  <input type="checkbox" checked="checked" id="checkAllStatus" title="Select All" class="pull-right">
                  <select id="status" name="status" multiple="multiple" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
                    <option selected="selected" value="1">In Progress</option>
                    <?php
                    if(!$this->receivAct){
                      echo '<option selected="selected" value="2">Normal by System</option>';
                    }
                    ?>
                    <option selected="selected" value="3">Resolved</option>
                    <option selected="selected" value="Y">Closed</option>
                  </select>
                </div>
              </div>
            </div>
          </form>
          <div class="row">
            <div class="col-md-10">
              <div class="form-group">
                <label></label>
                <div class="btn-group pull-right">
                  <button class=" btn btn-sm btn-warning" id="export"> <i class="fa fa-file-excel-o"></i> Export</button>
                  <button class=" btn btn-sm btn-primary" id="filter"> <i class="fa fa-eye"></i> Filter</button>
                </div>
              </div> 
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <ul class="timeline" style="display: none">
            <!-- timeline time label -->
            <li class="time-label">
              <span class="bg-red">
                Information
              </span>
            </li>
            <li>
              <i class="fa fa-info bg-blue"></i>
              <div class="timeline-item">
                <h3 class="timeline-header"><a href="#">Product</a> KILN</h3>
              </div>
            </li>
            <li>
              <i class="fa fa-info bg-aqua"></i>
              <div class="timeline-item">
                <h3 class="timeline-header no-border"><a href="#">Parameter</a> FCaO</h3>
              </div>
            </li>
          </ul>    
        </div>
      </div>
      
    </div> 
    <div class="box-body" id="contentTable">
        <?php 
        // echo $this->list_table;
        ?>
    </div>
  </div>
</section>

<div id="detailModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail NCQR</h4>
      </div>
      <div class="modal-body">
        <!-- <p>Some text in the modal.</p> -->
        <div class="callout callout-info">
            <h4>Info!</h4>
            <div class="row">
              <div class="col-md-8">
                <ul>
                  <li>Company: <span id="nm_company">-</span></li>
                  <li>Plant: <span id="nm_plant">-</span></li>
                  <li>Group Area: <span id="nm_grouparea">-</span></li>
                </ul>
                
              </div>
              <div class="col-md-4" style="font-weight: bold; font-weight: bold; padding: 10px 0px 0px 0px;">
                <span class="label label-warning" style="background-color: #5a5a5a !important;" id="nm_status"></span>
              </div>
            </div>
            <p>
            </p>
        </div>


          <div id="detailList" class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Detail List</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table style="width: 100%" id="tblDetail" class="table no-margin table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>AREA</th>
                      <th>SUBJECT</th>
                      <th>TYPE</th>
                      <th>DATE</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="tblContent">
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div id="notifList" class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Notification List</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table style="width: 100%" id="tblNotif" class="table no-margin table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>NO</th>
                      <th>NOTIFICATION</th>
                      <th>NAME</th>
                      <th>EMAIL</th>
                      <th>DATE</th>
                    </tr>
                  </thead>
                  <tbody id="tblNotifBody">

                  </tbody>
                </table>
              </div>
              <button id="notifBack" class="btn btn-sm btn-default pull-right"><i class="fa fa-long-arrow-left"></i> Back</button>
            </div>
          </div>
        
      </div>
<!--       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
 -->    </div>

  </div>
</div>


<script type="text/javascript">
  $(function(){
    datenow = moment().format('DD/MM/YYYY');

    $('.select2').select2();
    // $( ".datepicker" ).datepicker( "setDate", new Date());
    $('.datepicker').datepicker({
      // todayBtn: "linked",
      format: 'mm/yyyy',
      viewMode: 'months',
      minViewMode: 'months',
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: true,
      autoclose: true
    });

    $('#tableNcqr').DataTable();

  });

  function listPlant(idCompany = ''){
      $('#id_plant').html('');
      $.ajax({
        url : './NcqrList/getPlant/'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#id_plant').html('');
        $.map( data, function( val, i ) {
          if(val.ID_PLANT){
            $('#id_plant').append('<option value="'+val.ID_PLANT+'">'+val.NM_PLANT+'</option>');
          }
        });
      });

      $('select2').select2();
  }

  $('#id_company').on('change', function(){
    $("#checkAllPlant").removeAttr('checked');

    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
  });

  $("#checkAllPlant").click(function(){
    if($("#checkAllPlant").is(':checked') ){
        $("#id_plant > option").prop("selected","selected");
        $("#id_plant").trigger("change");
    }else{
        $("#id_plant > option").removeAttr("selected");
         $("#id_plant").trigger("change");
     }
  });
  $("#checkAllStatus").click(function(){
    if($("#checkAllStatus").is(':checked') ){
        $("#status > option").prop("selected","selected");
        $("#status").trigger("change");
    }else{
        $("#status > option").removeAttr("selected");
         $("#status").trigger("change");
     }
  }); 

  $("#filter").click(function(){

    $.ajax({
      url: "<?=base_url('NcqrList/tableNcqrList')?>",
      type: "POST",
      data: {
        PERIODE: $("#periode").val(), 
        ID_COMPANY: $("#id_company").val(),
        ID_PLANT: $("#id_plant").val(),
        STATUS: $("#status").val()
      }
    }).done(function(respon) {     
       respon = JSON.parse(respon);
        if(respon['status'] == true){
          notif(1, respon['msg']);
          $('#contentTable').html(respon['data']);

          $(".btnDetail").click(function(){
            id_plant = $(this).attr('id_plant');
            nm_plant = $(this).attr('nm_plant');
            id_grouparea = $(this).attr('id_grouparea');
            nm_grouparea = $(this).attr('nm_grouparea');
            id_area = $(this).attr('id_area');
            status = $(this).attr('status');
            if(id_grouparea == 1){
              getDetail(id_plant, nm_plant, id_grouparea, nm_grouparea, status, id_area);
            }else{
              getDetail(id_plant, nm_plant, id_grouparea, nm_grouparea, status);
            }
            // alert(id_plant + ' - ' +id_grouparea + ' - '+ id_area+ ' - '+ status);
          });

        }else{
          notif(3, respon['msg']);
        }
    }).fail(function (jqXHR, exception) {
          notif(4, 'Data Error!');
    });
  });

  function getDetail(id_plant, nm_plant, id_grouparea, nm_grouparea, status, id_area = null){
    var data = $('#id_company').select2('data');
    // alert();

    $('#nm_company').text(data[0].text);
    $('#nm_plant').text(nm_plant);
    $('#nm_grouparea').text(nm_grouparea);

    if(status == 1 || status == '1,2'){
      nm_status = 'IN PROGRESS';
    }else if(status == 2){
      nm_status = 'NORMAL BY SYSTEM';
    }else if(status == 2){
      nm_status = 'RECEIVER ACTION';
    }else{
      nm_status = 'CLOSED';
    }
    $('#nm_status').text(nm_status);

    $.ajax({
      url: "<?=base_url('NcqrList/getDetail')?>",
      type: "POST",
      data: {
        ID_PLANT: id_plant, 
        ID_GROUPAREA: id_grouparea,
        ID_AREA: id_area,
        ID_NCQR_STATUS: status,
        PERIODE: $("#periode").val(), 
      }
    }).done(function(respon) {     
       respon = JSON.parse(respon);
        if(respon['status'] == true){
          notif(1, respon['msg']);

          tbl = "";
          $.each(respon['data'], function(i, v){
            tbl += "<tr>";
              tbl += "<td>"+(i+1)+"</td>";
              tbl += "<td>"+v['NM_AREA']+"</td>";
              tbl += "<td>"+v['SUBJECT']+"</td>";
              tbl += "<td>"+v['NM_INCIDENT_TYPE']+"</td>";
              tbl += "<td>"+v['TANGGAL']+"</td>";
              tbl += "<td><button id_incident='"+v['ID_INCIDENT']+"' class='btnNotif btn btn-sm btn-info'>Notification</button></td>";
            tbl += "</tr>";
          });

          $('#tblDetail').DataTable().destroy();
          $('#tblContent').html(tbl);
          $('#tblDetail').DataTable({
             "bLengthChange": false,
             "searching": false
          });

          $('#detailModal').modal('show');
          $('#notifList').hide();
          $('#detailList').show();

          $(".btnNotif").click(function(){

            id_incident = $(this).attr('id_incident');
            getNotifList(id_incident);


            $('#notifList').show();
            $('#detailList').hide();
          });

          $("#notifBack").click(function(){
            $('#notifList').hide();
            $('#detailList').show();
          });
          
        }else{
          $('#tblContent').html('');
          notif(3, respon['msg']);
        }
    }).fail(function (jqXHR, exception) {
          notif(4, 'Data Error!');
    });


  }
  
  function getNotifList(id_incident){

    $.ajax({
      url: "<?=base_url('NcqrList/notif_sent2/')?>"+id_incident,
      type: "GET"
    }).done(function(respon) {     
       respon = JSON.parse(respon);
        if(respon['status'] == true){
          notif(1, respon['msg']);

          tbl = "";
          $.each(respon['data'], function(i, v){
            tbl += "<tr>";
              tbl += "<td>"+(i+1)+"</td>";
              tbl += "<td>"+v['NM_JABATAN']+"</td>";
              tbl += "<td>"+v['FULLNAME']+"</td>";
              tbl += "<td>"+v['EMAIL']+"</td>";
              tbl += "<td>"+v['TANGGAL_NOTIFIKASI']+"</td>";
            tbl += "</tr>";
          });

          $('#tblNotif').DataTable().destroy();
          $('#tblNotifBody').html(tbl);
          $('#tblNotif').DataTable({
             "bLengthChange": false,
             "searching": false
          });
        }else{
          $('#tblNotifBody').html('');
          notif(3, respon['msg']);
        }
    }).fail(function (jqXHR, exception) {
          notif(4, 'Data Error!');
    });
  }


  $("#export").click(function(){
      PERIODE= $("#periode").val();
      ID_COMPANY= $("#id_company").val();
      ID_PLANT= $("#id_plant").val().join('-');
      STATUS= $("#status").val().join('-');

      var win = window.open("<?=base_url('NcqrList/tableNcqrList/export?')?>"+'PERIODE='+PERIODE+'&ID_COMPANY='+ID_COMPANY+'&ID_PLANT='+ID_PLANT+'&STATUS='+STATUS, '_blank');
      win.focus();
  });


  

</script>