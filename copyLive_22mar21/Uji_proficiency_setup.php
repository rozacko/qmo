<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uji_proficiency_setup extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_uji_proficiency_setup","setup");
		
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_data = $this->setup->get_list();
		//print_r($this->db->last_query());
		//exit();
		$this->template->adminlte("v_set_up_pelaksanaan");
    }
	
	public function pelaksanaan($id_pp){
		$this->list_data = $this->setup->get_list($id_pp);
		$this->template->adminlte("v_set_up_pelaksanaan");
	}
    
    public function add(){
		$this->list_periode = $this->setup->data_periode();
		$this->list_comodity = $this->setup->data_comodity();
		$this->list_lab = $this->setup->data_lab();
		$this->list_pic = $this->setup->data_pic();
		$this->template->adminlte("v_set_up_pelaksanaan_add");
	}
	
	public function create(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		 = $this->input->post('ID_PP');
		$id_comodity = $this->input->post('ID_KOMODITI');
		$id_lab 	 = $this->input->post('ID_LAB');
		$id_pic		 = $this->input->post('ID_PIC');
		$date_now 	 = date('m-d-Y');
		$create_by_now = $user_in->FULLNAME;
		
		$save = $this->setup->save_proficiency(); 
		
		//send email
		$user = $this->setup->get_email_pic($id_pic);
		$komoditi = $this->setup->get_komoditi($id_comodity);
		$priode = $this->setup->get_priode_pro($id_pp);
		
		$pp = $priode->TITLE_PP." ".$priode->GROUP_PP." ".$priode->YEAR_PP;
		
		$this->send_email($user->EMAIL, $pp, $komoditi->NAMA_SAMPLE, $user->FULLNAME);
		
		if($save){
			$data = $this->setup->get_proficiency_now($id_pp, $id_comodity, $id_lab, $id_pic, $date_now, $create_by_now);
			$id_proficiency = $data->ID_PROFICIENCY;
			
			$save_act = $this->setup->save_activity($id_proficiency);
			if($save_act){
				$this->notice->success("Data Created.");
				redirect("Uji_proficiency_setup");
			} else {
				$this->notice->error($this->setup->error());
				redirect("Uji_proficiency_setup/add");
			} 
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/add");
		}
	}
	
	public function get_list(){
		$list = $this->setup->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $column->ID_PROFICIENCY;
			$row[] = $column->NAMA_SAMPLE;
			$row[] = $column->NAMA_LAB;
			$row[] = $column->FULLNAME;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->setup->count_all(),
            "recordsFiltered" => $this->setup->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}	
	
	public function edit($id_proficiency){
		$this->list_periode = $this->setup->data_periode();
		$this->list_comodity = $this->setup->data_comodity();
		$this->list_lab = $this->setup->data_lab();
		$this->list_pic = $this->setup->data_pic();
		$this->data_detil = $this->setup->get_data_by_id($id_proficiency);
		//print_r($this->data_detil); 
		//exit();
		$this->data_activity = $this->setup->get_data_activity($id_proficiency);
		$this->template->adminlte("v_set_up_pelaksanaan_edit");
	}
	
	public function do_edit(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_proficiency = $this->input->post('ID_PROFICIENCY');
		
		$update = $this->setup->update_proficiency(); 
		
		if($update){ 
			$this->notice->success("Data Edited.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		}
	}
	
	public function hapus($id_proficiency){
		$hapus = $this->setup->hapus_proficiency($id_proficiency); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("Uji_proficiency_setup");
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup");
		}
	}
	
	public function create_activity(){
		$id_proficiency = $this->input->post('PROFICIENCY');
		
		$save_act = $this->setup->save_activity($id_proficiency);
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} 
	}
	
	public function do_edit_activity(){
		$id_proficiency = $this->input->post('ID_PRO');
		$update_act = $this->setup->update_activity();
		//print_r($this->db->last_query());
		//exit();
		
		if($update_act){
			$this->notice->success("Data Edited.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} 
		
	}
	
	public function do_hapus_activity($id_activity, $id_proficiency){
		$hapus = $this->setup->hapus_activity($id_activity); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		}
	}
	
	public function timeline_act($id_proficiency){
		$list = $this->setup->get_data_activity($id_proficiency);
		//print_r($list);
		//exit();
		
		$data = array();
		
		foreach($list as $dt){
			
			//plan
			$date1 = $dt->PLAN_DATE_START;
			$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
			$out1 = $dateObj1->format('Y/m/d');
			$start = (strtotime("{$out1}") * 1000);
			
			$date2 = $dt->PLAN_DATE_END;
			$dateObj2 = DateTime::createFromFormat('d-M-y', $date2);
			$out2 = $dateObj2->format('Y/m/d');
			$end = (strtotime("{$out2}") * 1000);
			
			$values[] = array ("from" => $start, "to" => $end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttBlue");
			$hasil = array ("name"	 => $dt->ACTIVITY_NAME, "desc"	 => "Plan", "values" => $values);
			
			unset($values);
			array_push($data, $hasil);
			
			
			//realisasi
			if($dt->ACT_DATE_START != null and $dt->ACT_DATE_END != null){
				$date3 = $dt->ACT_DATE_START;
				$dateObj3 = DateTime::createFromFormat('d-M-y', $date3);
				$out3 = $dateObj3->format('Y/m/d');
				$real_start = (strtotime("{$out3}") * 1000);
				
				$date4 = $dt->ACT_DATE_END;
				$dateObj4 = DateTime::createFromFormat('d-M-y', $date4);
				$out4 = $dateObj4->format('Y/m/d');
				$real_end = (strtotime("{$out4}") * 1000);
				
				$values2[] = array ("from" => $real_start, "to" => $real_end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil2 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values2);
				array_push($data, $hasil2);
			} else {
				$values3[] = array ("from" => '', "to" => '', "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil3 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values3);
				array_push($data, $hasil3);
			}
			
			
		}
		
		//return json_encode($data);
		
		echo (json_encode($data));
	}
	
	public function send_email($email, $title, $komoditi, $name){
		$from = "qmo.noreply@semenindonesia.com";
		$to = $email;
		$subject = "Penunjukan PIC ".$title." - ".$komoditi;
		$message = "
Kepada Yth.
".$name."

Dengan hormat,

	Anda telah ditunjuk sebagai provider penyedia Sampel ".$title." Komoditi ".$komoditi.". Harap melakukan update terkait aktifitas sebagai Provider Penyedia sampel di qmo.semenindonesia.com serta menyampaikan petunjuk teknis pelaksanaan pengujian melalui email. Demikian terima kasih

Regards,
		
Koordinator Pelaksanaan ".$title."
		";
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}
	

}

?>