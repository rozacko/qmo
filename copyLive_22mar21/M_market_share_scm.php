<?php

class M_market_share_scm Extends DB_QM {
	private $post  = array();
	private $table = "M_GROUPAREA";
	private $pKey  = "ID_GROUPAREA";
	private $column_order = array(NULL, 'ID_GROUPAREA'); //set column for datatable order
	private $column_search = array('NM_GROUPAREA', 'KD_GROUPAREA'); //set column for datatable search
	private $order = array("ID_GROUPAREA" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	public function samplearealist_scm(){
		$this->scmdb->order_by("a.NM_KOTA");
		return $this->scmdb->get("ZREPORT_M_KOTA a")->result();
	}

	public function sample_perusahaan_scm(){
	    $this->scmdb->where("a.KELOMPOK", 'SMI');
		$this->scmdb->order_by("a.NAMA_PERUSAHAAN");
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->result_array();
	}

	public function get_sample_area_scm($key){
	    $this->scmdb->select("a.*");
	    $this->scmdb->where("LOWER(a.NM_KOTA)", strtolower($key));
		return $this->scmdb->get("ZREPORT_M_KOTA a")->row_array();
	}

	public function get_competitor_scm($key){
	    $this->scmdb->where("LOWER(a.PRODUK)", strtolower($key));
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->row_array();
	}

	public function get_competitor_detail_scm($key){
	    $this->scmdb->where("a.KODE_PERUSAHAAN", $key);
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->row_array();
	}

	public function merklist_scmn(){
		$this->scmdb->order_by("a.NAMA_PERUSAHAAN");
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN a")->result_array();
	}

	public function region_list_scmn(){
		$this->scmdb->order_by("A.ID_PULAU", "ASC");
		return $this->scmdb->get("ZREPORT_M_PROVINSI A")->result_array();
	}

	public function region_list_scm_pulau($key){
	    $this->scmdb->where("A.ID_PULAU", (int) $key);
		return $this->scmdb->get("M_PULAU A")->row_array();
	}

	public function realisasi_value($param){		
		$this->scmdb->select("QTY_REAL");
	    $this->scmdb->where($param);
		return $this->scmdb->get("ZREPORT_MS_TRANS1")->row_array();
	}

	public function dataQM($tanggal, $typeproduct){
		# code...
		$this->db->select("B.ID_COMPONENT, COALESCE(AVG(B.NILAI),0) AS RERATA");
		$this->db->join("D_CEMENT_DAILY B","A.ID_CEMENT_DAILY = B.ID_CEMENT_DAILY");
		$this->db->join("M_PRODUCT C","C.ID_PRODUCT = A.ID_PRODUCT");
		if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(C.KD_PRODUCT) = 'ppc' OR LOWER(C.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(C.KD_PRODUCT) = 'opc'");
		}


		$startdate = $tanggal['start'];
		$enddate = $tanggal['end'];

		$between = "A.DATE_DATA BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);
		
  		$this->db->group_by("B.ID_COMPONENT, A.ID_PRODUCT, C.KD_PRODUCT");
  		$this->db->order_by("B.ID_COMPONENT");
		return $this->db->get("T_CEMENT_DAILY A")->result_array();
	}
	

  	public function component_checklist(){
		$this->db->select("A .ID_COMPONENT, A .KD_COMPONENT, A .NM_COMPONENT, COALESCE(B.IS_ACTIVE, 0) AS STATUS_CHECKLIST");
		$this->db->join("O_CHECKLIST B","A.ID_COMPONENT = B.ID_COMPONENT", 'left');
  		$this->db->order_by("A.ID_COMPONENT");
		return $this->db->get("M_COMPONENT A")->result_array();
	}

	public function component_checklist_order(){
		$this->db->select("A.ID_COMPONENT,	A.KD_COMPONENT,	A.NM_COMPONENT,	COALESCE (B.IS_ACTIVE, 0) AS STATUS_CHECKLIST,	C.URUTAN");
		$this->db->join("O_CHECKLIST B","A.ID_COMPONENT = B.ID_COMPONENT", 'left');
		$this->db->join("C_PARAMETER_ORDER C","A.ID_COMPONENT = C.ID_COMPONENT", 'left');
		$this->db->where("C.ID_GROUPAREA", 1);
		$this->db->where("C.DISPLAY", 'D');
		$this->db->where("B.IS_ACTIVE", 1);
  		$this->db->order_by("C.URUTAN,	A.ID_COMPONENT");
		return $this->db->get("M_COMPONENT A")->result_array();
	}

}
