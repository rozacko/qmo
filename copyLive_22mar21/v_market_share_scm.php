<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Market Share SCM Report
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>

      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
            <div class="form-group row">
              <div class="form-group col-sm-3 col-sm-3">
                <label for="datepicker">Select Periode</label>
                <div class="input-group">
                  <!-- /btn-group -->
                  <input type="text" id="datepicker" type="text" class="form-control" data-date="" data-date-format="YYYY-MM" value="<?php echo date("Y-m");?>">
                  <div class="input-group-btn">
                    <button type="button" class="btn btn-danger" id="search_scm_report" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>"><i class="fa fa-search"></i>&nbsp; Search</button>
                    <button type="button" class="btn btn-success" id="exp_scm_report" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>"><i class="fa fa-file-o"></i>&nbsp; Export</button>
                  </div>
                </div>                
              </div>

            </div>
        </div>
        <div class="box-body" >

          <div class="rowspan" id="Parent_Table_Report_MS_SCM_e" style="margin: 10px;">
            <table class="table table-bordered" id="Table_Report_MS_SCM_e">  
            </table>
          </div>

        </div>
        <div class="box-footer clearfix"></div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data saved successfully :)"
  ></a>
<!-- eof msg confirm -->

<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }

 table #tb_dev {
    border-collapse: collapse;
    width: 100%;
  }

  #tb_dev th, td {
      text-align: center;
      padding: 8px;
  }

  #tb_dev tr:nth-child(even){background-color: #f2f2f2}

  #tb_dev th {
      background-color: #ffaf38;
      color: white;
  }

  #Parent_Table_Report_MS_SCM_e {
    max-height: 500px;
  }
  
  #Table_Report_MS_SCM_e {
    width: 1800px !important;
  }
  
  #Parent_Table_Report_MS_SCM {
    max-height: 500px;
  }
  
  #Table_Report_MS_SCM {
    width: 1800px !important;
  }

</style>

<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- DataTable CSS -->
<link href="<?php echo base_url("plugins/dataTables/datatables.min.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- DataTable CSS -->
<script src="<?php echo base_url("plugins/dataTables/datatables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/dataTables/dataTables.select.min.js");?>"/></script>

<!-- Select2 Plugin-->
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/select2/select2.full.min.js");?>"/></script>

<!-- bootstrap datepicker -->
<script src="<?php echo base_url("plugins/datepicker/bootstrap-datepicker.js");?>"></script>

<!-- Tabel Column Freezer -->
<script src="<?php echo base_url("plugins/TabelFreezeColumn/js/tableHeadFixer.js");?>"></script>


<script>
$(document).ready(function(){

  //Date picker
  $('#datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm'
  });

  loadDataMarketShare();

  jQuery(document).on('click', '#search_scm_report', function() {
    loadDataMarketShare();
  });

  jQuery(document).on('click', '#exp_scm_report', function() {
    var oDate = $('#datepicker').val();
    var tDate = oDate.split('-');
    $('#exp_scm_report').button('loading');  
      Export_Excel('Table_Report_MS_SCM_e', tDate[0], tDate[1]);
    $('#exp_scm_report').button('reset');
  }); 

  /** Notification **/
  $(".notice-error").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-success").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });



});


function Export_Excel(table, tahun, bulan) {
  // body...
  var uri = 'data:application/vnd.ms-excel;base64,'
      , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
      , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
      , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }

      if (!table.nodeType) table = document.getElementById(table)
  var ctx = {worksheet: 'Market Share' || 'Worksheet', table: table.innerHTML}
  var v = new MouseEvent("click", {
          view: window,
          bubbles: false,
          cancelable: true
      });
  var u = document.createElement("a");
  u.setAttribute("download", "Market Share Periode : "+tahun+"-"+bulan+".xls");
  u.setAttribute("href", uri + base64(format(template, ctx)));
  u.setAttribute("target", "_blank");
  u.dispatchEvent(v);

$('#exportExcelBtne').button('reset');

}

function loadDataMarketShare() {

  $('#search_scm_report').button('loading');
  $('#exp_scm_report').button('loading'); 

  var oDate = $('#datepicker').val();
  var tDate = oDate.split('-');
  var urlget = "market_share_scm_report/Get_MarketShare_SCM/"+tDate[0]+"/"+tDate[1];
  $.ajax({
    url: "<?php echo site_url("market_share_scm_report/Get_MarketShare_SCM");?>",
    data: {"tahun": tDate[0], "bulan": tDate[1]},
    dataType: 'json',
    type: 'POST',
    success: function (result) {

      console.log(result);

      if (result['Status'] == true) {
        var company_list = result['Data']['Company'];
        var col_list = '';
        var sub_col_list = '';

        var header_tabel = '';
        var body_tabel = '';

        col_list += '<th rowspan="2" width="3%" style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;">No</th>';
        col_list += '<th rowspan="2" width="250px" style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle; white-space: nowrap;">Nama Daerah</th>';

        for (indx in company_list) {   
          col_list += '<th colspan="2" width="*" style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;">'+company_list[indx]['INISIAL']+'</th>';
          sub_col_list += '<th style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><i>Bag</i></th>';
          sub_col_list += '<th style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><i>Bulk</i></th>';
        }

        col_list += '<th colspan="3" width="*" style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><b>Total Supply</b></th>';
        sub_col_list += '<th style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><b>Bag</b></th>';
        sub_col_list += '<th style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><b>Bulk</b></th>';
        sub_col_list += '<th style="background-color: rgb(255, 156, 0) !important; text-align: center; vertical-align: middle;"><b>Total</b></th>';

        header_tabel = '<thead style=" background-color: rgb(255, 156, 0) !important; color: white;"><tr>'+col_list+'</tr><tr>'+sub_col_list+'</tr></thead>';
        var n_total_bag_nasional = new Array();
        var n_total_curah_nasional = new Array();
          var v_bag_comp_nasional = 0 ;
          var v_curah_comp_nasional = 0 ;
        var region_list = result['Data']['Region Data'];
        body_tabel += '<tbody>';
        for (indx in region_list) {          
          var n_total_bag = new Array();
          var n_total_curah = new Array();
          var v_bag_comp = 0 ;
          var v_curah_comp = 0 ;
          for (var i = 0; i < region_list[indx].length; i++) {
            var v_bag = 0 ;
            var v_curah = 0 ;
            body_tabel += '<tr style="font-size: smaller; font-weight: 200;">';
              body_tabel += '<td>'+(i+1)+'.</td>';
              body_tabel += '<td style="white-space: nowrap;">'+region_list[indx][i]['NM_PROV']+'</td>';
              var realisasi = region_list[indx][i]['REALISASI_MS'];
              for (comp_code in realisasi) {

                if (!n_total_bag[comp_code]) {
                  n_total_bag[comp_code] = new Array();
                }

                n_total_bag[comp_code].push(realisasi[comp_code]['BAG']);

                if (!n_total_curah[comp_code]) {
                  n_total_curah[comp_code] = new Array();
                }

                n_total_curah[comp_code].push(realisasi[comp_code]['CURAH']);

                body_tabel += '<td>'+(realisasi[comp_code]['BAG']).toFixed(2)+'</td>';
                body_tabel += '<td>'+(realisasi[comp_code]['CURAH']).toFixed(2)+'</td>';
                v_bag = v_bag + realisasi[comp_code]['BAG'];
                v_curah = v_curah + realisasi[comp_code]['CURAH'];
              }
              body_tabel += '<td>'+(v_bag).toFixed(2)+'</td>';
              body_tabel += '<td>'+(v_curah).toFixed(2)+'</td>';
              body_tabel += '<td>'+((v_bag + v_curah)).toFixed(2)+'</td>';
            body_tabel += '</tr>';
          }          

          body_tabel += '<tr style="background-color: #eeef90;">';
            body_tabel += '<td colspan="2" style="white-space: nowrap;"><b>Total '+indx+'</b></td>';
            for (indx_comp in company_list) {   

            	if (!n_total_bag_nasional[indx_comp]) {
                	n_total_bag_nasional[indx_comp] = new Array();
                }
				if (!n_total_curah_nasional[indx_comp]) {
                	n_total_curah_nasional[indx_comp] = new Array();
                }
                
              body_tabel += '<td><b>'+(n_total_bag[indx_comp].reduce(getSum)).toFixed(2)+'</b></td>';
              n_total_bag_nasional[indx_comp].push(n_total_bag[indx_comp].reduce(getSum));
              body_tabel += '<td><b>'+(n_total_curah[indx_comp].reduce(getSum)).toFixed(2)+'</b></td>';
              n_total_curah_nasional[indx_comp].push(n_total_curah[indx_comp].reduce(getSum));

              v_bag_comp = v_bag_comp + parseFloat(n_total_bag[indx_comp].reduce(getSum));
              v_curah_comp = v_curah_comp + parseFloat(n_total_curah[indx_comp].reduce(getSum));
            }
            
            body_tabel += '<td><b>'+(v_bag_comp).toFixed(2)+'</b></td>';
            body_tabel += '<td><b>'+(v_curah_comp).toFixed(2)+'</b></td>';
            body_tabel += '<td><b>'+((v_bag_comp+v_curah_comp)).toFixed(2)+'</b></td>';

          body_tabel += '</tr>';
        }

        body_tabel += '<tr style="background-color: #d1d266;">';
            body_tabel += '<td colspan="2" style="white-space: nowrap;"><b>Total Nasional</b></td>';
            for (indx_comp in company_list) {   

            	body_tabel += '<td><b>'+(n_total_bag_nasional[indx_comp].reduce(getSum)).toFixed(2)+'</b></td>';
            	body_tabel += '<td><b>'+(n_total_curah_nasional[indx_comp].reduce(getSum)).toFixed(2)+'</b></td>';

            	v_bag_comp_nasional = v_bag_comp_nasional + parseFloat(n_total_bag_nasional[indx_comp].reduce(getSum));
            	v_curah_comp_nasional = v_curah_comp_nasional + parseFloat(n_total_curah_nasional[indx_comp].reduce(getSum));
            }
            
            body_tabel += '<td><b>'+(v_bag_comp_nasional).toFixed(2)+'</b></td>';
            body_tabel += '<td><b>'+(v_curah_comp_nasional).toFixed(2)+'</b></td>';
            body_tabel += '<td><b>'+((v_bag_comp_nasional + v_curah_comp_nasional)).toFixed(2)+'</b></td>';

        body_tabel += '</tr>';

        body_tabel += '</tbody>';

        $('#Table_Report_MS_SCM_e').html(header_tabel+body_tabel);
        $("#Table_Report_MS_SCM_e").tableHeadFixer({"left" : 2}); 

      }
    },
    error: function () {
    }
  });     

  $('#search_scm_report').button('reset'); 
  $('#exp_scm_report').button('reset');

}

function getSum(total, num) {
    return total + num;
}

</script>
