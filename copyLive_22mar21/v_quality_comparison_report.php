<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Quality Comparison
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>

      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <!-- <form id="formData"> -->
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-3" id="DIVAREA_GROUP">
                <label for="AREA_GROUP">GROUP AREA</label>
                <select id="AREA_GROUP" name="AREA_GROUP" class="form-control select2">
                  <option value="Province">Province</option>
                  <option value="National">National</option>
                </select>
              </div>
              <div class="form-group col-sm-6 col-sm-3">
                <label for="TANGGAL">DATE RANGE</label>
                <input name="TANGGAL" id="datepicker" type="text" class="form-control" value="<?php echo date("m/Y");?>">
              </div>
              <div class="form-group col-sm-6 col-sm-2" id="DIVID_PRODUCT">
                <label for="ID_PRODUCT">TYPE</label>
                <select id="ID_PRODUCT" name="ID_PRODUCT" class="form-control select2">
                  <option value="1">OPC</option>
                  <option value="0">Non OPC</option>

                </select>
              </div>
              <div class="form-group col-sm-6 col-sm-2" id="selectCompany">
                <label for="ID_COMPANY">COMPANY</label>
                <select id="ID_COMPANY" name="ID_COMPANY" class="form-control select2">
                </select>
              </div>

              <div class="form-group col-sm-6 col-sm-2" id="selectCompany">
                <label for="BTN_SEARCH"> </label>
                <button style="display: none" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-primary btn-flat" name="load" id="btLoad" ><i class="fa fa-search"></i> &nbsp; Search</button>
              </div>

            </div>

              <span id="saving" style="">
                    <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>

            <div class="form-group row">
                <div class="form-group col-sm-3 col-sm-2">
                    <button type="button" id="exportExcelBtne" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-success btn-sm btn-flat pull-left" style="margin: 10px; display: none;"><i class="fa fa-file-excel-o"></i> &nbsp; Export Data</button>
                </div> 
                <div class="form-group col-sm-3 col-sm-2">
                <button type="button" id="convert" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" class="btn btn-block btn-warning btn-sm btn-flat pull-left" style="margin: 10px; display: none;"><i class="fa fa-file-image-o"></i> &nbsp; Convert Charts</button>
                </div>             
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="nav-tabs-custom" id="dataqomparison">
              </div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-12">
            </div>

            <div class="col-sm-12 col-sm-12 hidden" id="reportview">

            </div>

            <hr/>

          <div class="row col-sm-12">
              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>
            <div id="example1" class="hot handsontable htColumnHeaders" ></div>
          </div>

          <div id="divTable" style="display:none;">
            <div class="form-group row">
              <div class="col-sm-12">
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
                <div id="handsonTable"></div>
              </div>
            </div>
            <div class="form-group row">
            </div>

          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Data saved successfully :)"
  ></a>
<!-- eof msg confirm -->

<div class="modal_load"><!-- Loading modal --></div>


<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }

  /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }

 table #tb_dev {
    border-collapse: collapse;
    width: 100%;
  }

  #tb_dev th, td {
      text-align: center;
      padding: 8px;
  }

  .comparison > thead > tr > th,
  .comparison > tbody > tr > th,
  .comparison > tfoot > tr > th,
  .comparison > thead > tr > td,
  .comparison > tbody > tr > td,
  .comparison > tfoot > tr > td {
    border-top: 2px solid black !important;
  }

  #tb_dev tr:nth-child(even){background-color: #f2f2f2}

  #tb_dev th {
      background-color: #ffaf38;
      color: white;
  }

  @import 'https://code.highcharts.com/css/highcharts.css';

  /*.highcharts-background {
    fill: #efefef;
    stroke: #dd4b39;
    stroke-width: 2px;
  }*/

</style>

<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- DateRangePicker CSS -->
<link href="<?php echo base_url("plugins/daterangepicker/daterangepicker.css");?>" rel="stylesheet">

<!-- DateRangePicker JS -->
<script src="<?php echo base_url("plugins/daterangepicker/moment.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/daterangepicker/daterangepicker.min.js");?>"/></script>

<!-- Highchart JS -->
<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.svg.js"></script>

<!-- svgConvert JS -->
<!-- <script src="https://rawgit.com/madebyshape/svg-convert/master/dist/svgConvert.js"></script> -->


<!-- SVG to Canvas JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.4/rgbcolor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stackblur-canvas/1.4.1/stackblur.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/canvg/dist/browser/canvg.min.js"></script>

<script>

  var glbtagchart = new Array();
  var glbidtable = new Array();

  var glbselectedarea = 'Province';
  var glbselectedperiode = '';
  var glbselectedtype = 'OPC';
  var glbselectedid = 1 ;
  var glbselectedcomp = '';
  var glbselectedcompid = 0;

  $(document).ready(function(){

    jQuery(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) { // on tab selection event
        jQuery(".container-for-chart").each(function() {
            var chart = jQuery(this).highcharts(); // target the chart itself
            // chart.reflow() // reflow that chart
        });
    })
    
    $('#converter-bt').bind('click', function() {
        var svg = document.getElementById('legendContainer');

        var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        var data = (new XMLSerializer()).serializeToString(svg);
        var DOMURL = window.URL || window.webkitURL || window;

        var img = new Image();
        var svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        var url = DOMURL.createObjectURL(svgBlob);

        img.onload = function () {
          ctx.drawImage(img, 0, 0);
          DOMURL.revokeObjectURL(url);

          var imgURI = canvas
              .toDataURL('image/png')
              .replace('image/png', 'image/octet-stream');
        };

        img.src = url;

    });

    function triggerDownload (imgURI) {
      var evt = new MouseEvent('click', {
        view: window,
        bubbles: false,
        cancelable: true
      });

      var a = document.createElement('a');
      a.setAttribute('download', 'MY_COOL_IMAGE.png');
      a.setAttribute('href', imgURI);
      a.setAttribute('target', '_blank');

      a.dispatchEvent(evt);
    }

    $('#convert').bind('click', function() {
            var national = 'National';
            $('#convert').button('loading');
            $('#ExportImageChart').show();
      
        html2canvas($("#ExportImageChart"), {
          onrendered: function(canvas) {
             var imgsrc = canvas.toDataURL("image/png");

          var v = new MouseEvent("click", {
                  view: window,
                  bubbles: false,
                  cancelable: true
              });
          var u = document.createElement("a");
          u.setAttribute("download", "National Charts Comparison.png");
          u.setAttribute("href", imgsrc);
          u.setAttribute("target", "_blank");
          u.dispatchEvent(v);

            $('#convert').button('reset');
            $('#ExportImageChart').hide();

          }
        });
    });

    $('#datepicker').daterangepicker();

    $("[id$=exportExcelBtne]").click(function(e) {

        var area = $('#AREA_GROUP option:selected').text();
        var areaval = $('#AREA_GROUP').val();

        var typename = $('#ID_PRODUCT option:selected').text();
        var typeval = $('#ID_PRODUCT').val();

        var periode = $('#datepicker').val();

        var company = $('#ID_COMPANY option:selected').text();
        var companyval = $('#ID_COMPANY').val();


        var tablecontent = new Array();

        for (var listid in glbidtable) {
          var tempdata = new Array();
          tempdata[0] = new Array();
          tempdata[1] = new Array();
          tempdata[2] = new Array();
          if (glbselectedarea == 'National') {
            tempdata[3] = new Array();
            tempdata[4] = new Array();
          }

          var myTableHeadArray = new Array();
          var myTableDataArray = new Array();
          $("table#"+glbidtable[listid]+" tr").each(function() { 
              var arrayOfThisRow = new Array();

              var tableHead = $(this).find('th');
              if (tableHead.length > 0) {
                  tableHead.each(function() { arrayOfThisRow.push($(this).text()); });
                  myTableHeadArray.push(arrayOfThisRow);
              }

              var tableData = $(this).find('td');
              if (tableData.length > 0) {
                  tableData.each(function() { arrayOfThisRow.push($(this).text()); });
                  myTableDataArray.push(arrayOfThisRow);
              }
          });

          tempdata[0] = myTableHeadArray;
          tempdata[1] = myTableDataArray;

          if (glbselectedarea == 'National') {
            tempdata[2] = myTableDataArray[parseInt(myTableDataArray.length) - 3];
            tempdata[3] = myTableDataArray[parseInt(myTableDataArray.length) - 2];
            tempdata[4] = myTableDataArray[parseInt(myTableDataArray.length) - 1];
            myTableDataArray.pop();
            myTableDataArray.pop();
            myTableDataArray.pop();
          } else {
            tempdata[2] = myTableDataArray[parseInt(myTableDataArray.length) - 1];
            myTableDataArray.pop();
          }

          tablecontent[0] = new Array();
          tablecontent[0] = tempdata;

        }


        glbselectedperiode = glbselectedperiode.replace(/\ - /g, "~");
        glbselectedperiode = glbselectedperiode.replace(/\//g, "-");
        var textarr = JSON.stringify(tablecontent);
        var base_url = '<?php echo site_url();?>';

        textarr = textarr.replace(/\[/g, " ++~=+ ");
        textarr = textarr.replace(/\]/g, " +=~++ ");
        textarr = textarr.replace(/\,/g, " +..+ ");

        var url = "quality_comparison_report/excel_create/"+glbselectedcompid+"/"+glbselectedperiode+"/"+glbselectedid+"/"+glbselectedarea;

        location.assign(base_url+url);

    });

    loadCompany();

    $("#AREA_GROUP").change(function(){
        var area_group_val = $('#AREA_GROUP').val();
        var area_group_name = $('#AREA_GROUP option:selected').val();
        if (area_group_val == 'National') {
          $('#selectCompany').hide();
        } else {
          $('#selectCompany').show();
        }
    });

    $('#saving').css('display','none');

    //Pika month change
    jQuery(document).on('change', '.pika-select-month', function() {
      picker.setDate(new Date((1900+picker.getDate().getYear()), this.value));
    });

    //pika year change
    jQuery(document).on('change', '.pika-select-year', function() {
      picker.setDate(new Date((this.value), picker.getDate().getMonth()));
    });

    // loadTypeProductList();

    $('#btLoad').show();

    $("#btLoad").click(function(event){
      loadSampleData();
    });

    function getContentData() {
      return [];
    }

    function getRowOptions(data) {
      var initial_coloptions = [
        {type: 'date'},
        {type: 'text'},
        {type: 'text'},
        {type: 'text'},
      ];

      var rowcontent = {
            // type: "numeric" // Got some problem in /PROD/qmonline
            type: "text"
      }

      for (var i = 0; i < (data.length)-4; i++) {
        initial_coloptions[i+4] = rowcontent;
      }

      return initial_coloptions;
    }

    function getColHeader(data) {
      var columnlist = new Array();
      for (var i = 0; i < data.length; i++) {
        columnlist.push(data[i]['title']);
      }

      return columnlist;
    }

    function Export_Excel(table, area, periode, company) {
      // body...
      var uri = 'data:application/vnd.ms-excel;base64,'
          , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
          , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
          , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }

          if (!table.nodeType) table = document.getElementById(table)
      var ctx = {worksheet: company || 'Worksheet', table: table.innerHTML}
      var v = new MouseEvent("click", {
              view: window,
              bubbles: false,
              cancelable: true
          });
      var u = document.createElement("a");
      u.setAttribute("download", area + "_"+company+" : "+periode+".xls");
      u.setAttribute("href", uri + base64(format(template, ctx)));
      u.setAttribute("target", "_blank");
      u.dispatchEvent(v);

    $('#exportExcelBtne').button('reset');

    }

    var example1 = document.getElementById('example1'), hot1;

    /** Notification **/
  $(".notice-error").confirm({
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
  });

  $(".notice-success").confirm({
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-success"
  });

});

function loadTypeProductList() {
  var typeproduct = $('#ID_PRODUCT');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product");?>', function (result) {
    var values = result;
    typeproduct.find('option').remove();
    if (values != undefined && values.length > 0) {
      typeproduct.css("display","");
      $(values).each(function(index, element) {
        typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
      });
    }else{
      typeproduct.find('option').remove();
      typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
    }

    $('#saving').css('display','none');
  });
}

function loadSampleData() {
  $('#convert').hide();
    $('#exportExcelBtne').hide();
  $('#reportview').html('');
  $('#dataqomparison').html('');
  $('#saving').css('display','');
  $('#btLoad').button('loading');
  var id_company = $('#ID_COMPANY').val();
  var name_company = $('#ID_COMPANY option:selected').text();
  var tanggal = $('#datepicker').val();
  var id_typeproduct = $('#ID_PRODUCT').val();
  var type_product = $('#ID_PRODUCT option:selected').text();
  var areagroup = $('#AREA_GROUP').val();

  var sni_name_product = 'OPC';

  if (parseInt(id_typeproduct) == 0) {
  	sni_name_product = 'PCC';
  }

  glbselectedarea = areagroup;
  glbselectedperiode = tanggal;
  glbselectedtype = type_product;
  glbselectedid = parseInt(id_typeproduct);
  glbselectedcomp = name_company;
  glbselectedcompid = parseInt(id_company);

  var listtable = '';
  var listtableexp = '';
  var tmplegend = new Array();
  var imgidhighchart = new Array();

    glbidtable = new Array();

  if (areagroup == 'Province') {

    $.ajax({
        url: "<?php echo site_url("quality_comparison_report/load_tabel_report");?>",
        data: {"id_company": id_company, "tanggal": tanggal, "id_typeproduct": id_typeproduct, "areagroup": areagroup},
        dataType: 'json',
        type: 'POST',
        success: function (result) {
            if (result['Status'] == true) {
              var tdata = result['Data'];
                var tgroupareaarr = new Array();
                var tgroupareaarrsplit = new Array();
                var tgroupchartarr = new Array();
                var smigmin = new Array();
                var smigmax = new Array();
                var tmpsmigmin = new Array();
                var tmpsmigmax = new Array();
                var tmpavgind = new Array();                
                var tmpsniproduct = new Array();
                var tmpcompe = new Array();
                var valseries = new Array();
                var componentlist = result['Checklist'];
                for (var i = 0; i < componentlist.length; i++) {
                  if (componentlist[i]['Component Name'] == 'CS1D' || componentlist[i]['Component Code'] == 'CS1D') {
                    smigmax[0] = componentlist[i]['V_Max'];
                    smigmin[0] = componentlist[i]['V_Min'];
                  } else if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                    smigmax[1] = componentlist[i]['V_Max'];
                    smigmin[1] = componentlist[i]['V_Min'];
                  } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                    smigmax[2] = componentlist[i]['V_Max'];
                    smigmin[2] = componentlist[i]['V_Min'];
                  } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                    smigmax[3] = componentlist[i]['V_Max'];
                    smigmin[3] = componentlist[i]['V_Min'];
                  }
                }

                var avgindval = new Array();

                var tabview = '';
                var htabview = '';
                var ctabview = '';
                var inddextab = 0;

            for (var grouparea in tdata) {
                  var listtablecontent = '';
                  var listtablecontente = '';
                  var isactivetab = '';
                  var tmpseries = new Array();
                  tmpseries['CS3D'] = new Array();
                  tmpseries['CS7D'] = new Array();
                  tmpseries['CS28D'] = new Array();

              if (inddextab == 0 ) {
                isactivetab = 'active';
              }

              htabview += '<li class="'+isactivetab+'"><a href="#tab_'+ grouparea.replace(" ", "_") +'" data-toggle="tab" aria-expanded="false">'+grouparea+'</a></li>';

                  inddextab++;

                    var contentscatter =  new Array();

                    var tdatachartscatter = {
                                                type: 'scatter',
                                                marker: {
                                                    radius: 4
                                                }
                                            };

                    var listbodytable = '';

                listtablecontent += '<div class="table box-body table-responsive no-padding" >'
                +'<table class="tblreportview table-responsive comparison" id="TableContent'+grouparea.replace(" ", "_")+'" border="1" style="width: 100%; font-size: 12px;">';
                listtablecontente += '<thead>'
                +'<tr>'
                +'<th width="10%" style=" background-color: deepskyblue; ">'+ grouparea +'</th>';

                    listtablecontente += '<th width="*" rowspan="2" style=" background-color: deepskyblue; vertical-align: middle;">&nbsp;'
                                + name_company + '<br> (<i>Data QM</i>) </th>';
                    listtablecontente += '<th width="*" rowspan="2" style=" background-color: deepskyblue; vertical-align: middle;">&nbsp;'
                                + name_company + '<br> (<i>Data SemNas</i>) </th>';

            glbidtable.push('TableContent'+grouparea.replace(" ", "_"));

            for (var competitorid in tdata[grouparea]['Competitor']) {

              listtablecontente += '<th width="*" rowspan="2" style=" background-color: deepskyblue; vertical-align: middle;">&nbsp;'
                      + tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']+ '</th>';

            }

                  for (var i = 0; i < componentlist.length; i++) {

                      var tvaluearr = new Array();

                      listbodytable += '<tr>';
                      listbodytable += '<td style="text-align: left;">&nbsp;<i>'+componentlist[i]['Component Name']+'</i>&nbsp;</td>';

                      if (result['Data QM'][componentlist[i]['Id Component']]) {
                          listbodytable += '<td> '+ converse_value(result['Data QM'][componentlist[i]['Id Component']]) +' </td>';
                          if (tdata[grouparea]['SMIG']) {
                            if (tdata[grouparea]['SMIG'][id_company]['Market Share (%)'] && tdata[grouparea]['SMIG'][id_company]['Market Share (%)'] > 0) {
                              tvaluearr.push(parseFloat((result['Data QM'][componentlist[i]['Id Component']])) * parseFloat((tdata[grouparea]['SMIG'][id_company]['Market Share (%)'])));
                            } else {
                              tvaluearr.push(0 * parseFloat((tdata[grouparea]['SMIG'][id_company]['Market Share (%)'])));
                            }
                          } else {
                              tvaluearr.push(0);
                          }
                      } else {
                          listbodytable += '<td> - </td>';
                          tvaluearr.push(0);
                      }

                      if (tdata[grouparea]['SMIG']) {
                        for (var competitorid in tdata[grouparea]['SMIG']) {

                            if (!contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']]) {
                              contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']] = new Array();
                            }

                            var datasample = tdata[grouparea]['SMIG'][competitorid]['Data'];
                            if (datasample[componentlist[i]['Id Component']]) {
                              listbodytable += '<td> '+ (datasample[componentlist[i]['Id Component']]['ReRata']) +' </td>';
                              if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][0] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                                  tmpseries['CS3D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});

                              } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][1] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                                  tmpseries['CS7D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});

                              } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][2] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                                  tmpseries['CS28D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});

                              }

                            } else {
                              listbodytable += '<td>&nbsp;-&nbsp;</td>';
                              if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][0] = 0;
                                  tmpseries['CS3D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});
                              } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][1] = 0;
                                  tmpseries['CS7D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});
                              } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                                  contentscatter[tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']][2] = 0;
                                  tmpseries['CS28D'].push({name: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id: tdata[grouparea]['SMIG'][competitorid]['Competitor Inisial']}]});
                              }

                            }

                        }
                      } else {

                        if (!contentscatter[name_company]) {
                            contentscatter[name_company] = new Array();
                          }

                        listbodytable += '<td>&nbsp;-&nbsp;</td>';
                          if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                              contentscatter[name_company][0] = 0;
                          tmpseries['CS3D'].push({name: name_company, data: [{ y: parseFloat(0), id: name_company}]});
                          } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                              contentscatter[name_company][1] = 0;
                              tmpseries['CS7D'].push({name: name_company, data: [{ y: parseFloat(0), id: name_company}]});
                          } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                              contentscatter[name_company][2] = 0;
                          tmpseries['CS28D'].push({name: name_company, data: [{ y: parseFloat(0), id: name_company}]});
                          }
                      }

                      for (var competitorid in tdata[grouparea]['Competitor']) {

                          if (!contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']]) {
                            contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']] = new Array();
                          }

                          var datasample = tdata[grouparea]['Competitor'][competitorid]['Data'];
                          if (datasample[componentlist[i]['Id Component']]) {
                            listbodytable += '<td> '+ (datasample[componentlist[i]['Id Component']]['ReRata']) +' </td>';
                            if (tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'] && tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'] > 0) {
                              tvaluearr.push(parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])) * parseFloat((tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'])));
                            } else {
                              tvaluearr.push(0 * parseFloat((tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'])));
                            }
                            if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][0] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                                tmpseries['CS3D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][1] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                                tmpseries['CS7D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][2] = parseFloat((datasample[componentlist[i]['Id Component']]['ReRata']));
                              tmpseries['CS28D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat((datasample[componentlist[i]['Id Component']]['ReRata'])), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            }

                          } else {
                            listbodytable += '<td>&nbsp;-&nbsp;</td>';
                            tvaluearr.push(0);
                            if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][0] = 0;
                            tmpseries['CS3D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][1] = 0;
                            tmpseries['CS7D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                                contentscatter[tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']][2] = 0;
                            tmpseries['CS28D'].push({name: tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial'], data: [{ y: parseFloat(0), id:  tdata[grouparea]['Competitor'][competitorid]['Competitor Inisial']}]});
                            }

                          }

                      }

                      var sumvalue = 0;

                      for (var k = 0; k < tvaluearr.length; k++) {
                          sumvalue += (tvaluearr[k]);
                      }

                      var avgvalue = sumvalue;
                      if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                          avgindval[0] = parseFloat(avgvalue.toFixed(2));
                          tmpavgind.push(parseFloat(avgvalue.toFixed(2)));
                      } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                          avgindval[1] = parseFloat(avgvalue.toFixed(2));
                          tmpavgind.push(parseFloat(avgvalue.toFixed(2)));
                      } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                          avgindval[2] = parseFloat(avgvalue.toFixed(2));
                          tmpavgind.push(parseFloat(avgvalue.toFixed(2)));
                      }

                      listbodytable += '<td>&nbsp;'+ avgvalue.toFixed(2) +'&nbsp;</td>';
                      if (parseInt(componentlist[i]['V_Min']) > 0) {
                        listbodytable += '<td>&nbsp;'+ componentlist[i]['V_Min'] +'&nbsp;</td>';
                        if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                        tmpsmigmin.push(parseFloat(componentlist[i]['V_Min']));
                        } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                        tmpsmigmin.push(parseFloat(componentlist[i]['V_Min']));
                        } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                        tmpsmigmin.push(parseFloat(componentlist[i]['V_Min']));
                        }
                      } else {
                        listbodytable += '<td>&nbsp; - &nbsp;</td>';
                        if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                        tmpsmigmin.push(parseFloat(0));
                        } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                        tmpsmigmin.push(parseFloat(0));
                        } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                        tmpsmigmin.push(parseFloat(0));
                        }

                      }
                      if (parseInt(componentlist[i]['V_Max']) > 0) {
                        listbodytable += '<td>&nbsp;'+ componentlist[i]['V_Max'] +'&nbsp;</td>';
                        if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                        	tmpsmigmax.push(parseFloat(componentlist[i]['V_Max']));
                        } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                        	tmpsmigmax.push(parseFloat(componentlist[i]['V_Max']));
                        } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                        	tmpsmigmax.push(parseFloat(componentlist[i]['V_Max']));
                        }
                      } else {
                        listbodytable += '<td>&nbsp; - &nbsp;</td>';
                      	if (componentlist[i]['Component Name'] == 'CS3D' || componentlist[i]['Component Code'] == 'CS3D') {
                        	tmpsmigmax.push(parseFloat(0));
                        } else if (componentlist[i]['Component Name'] == 'CS7D' || componentlist[i]['Component Code'] == 'CS7D') {
                        	tmpsmigmax.push(parseFloat(0));
                        } else if (componentlist[i]['Component Name'] == 'CS28D' || componentlist[i]['Component Code'] == 'CS28D') {
                        	tmpsmigmax.push(parseFloat(0));
                        }
                      }

                      if (parseFloat(componentlist[i]['SNI_Min']) > 0) {
                        listbodytable += '<td>&nbsp; '+ parseFloat((componentlist[i]['SNI_Min']).toFixed(1)) +' &nbsp;</td>';
                        tmpsniproduct.push(parseFloat((componentlist[i]['SNI_Min']).toFixed(1)));
                      } else {
                        listbodytable += '<td>&nbsp; - &nbsp;</td>';
                      }

                      if (parseFloat(componentlist[i]['SNI_Max']) > 0) {
                      	listbodytable += '<td>&nbsp; '+ parseFloat((componentlist[i]['SNI_Max']).toFixed(1)) +' &nbsp;</td>';
                      } else {
                      	listbodytable += '<td>&nbsp; - &nbsp;</td>';
                      }

                      listbodytable += '</tr>';

                  }

                  listbodytable += '<tr style="background-color: #56ff0c;">';
                  listbodytable += '<td style="text-align: left;"><b> MARKET SHARE </b></td>';

                    if (tdata[grouparea]['SMIG']) {
                      if (tdata[grouparea]['SMIG'][id_company]['Market Share (%)'] && tdata[grouparea]['SMIG'][id_company]['Market Share (%)'] > 0) {
                        listbodytable += '<td colspan="2"> '+ parseFloat((tdata[grouparea]['SMIG'][id_company]['Market Share (%)'] * 100).toFixed(2)) +' % </td>';
                      } else {
                        listbodytable += '<td colspan="2"> '+0+' % </td>';
                      }
                    } else {
                      listbodytable += '<td colspan="2"> '+0+' % </td>';
                    }

                    for (var competitorid in tdata[grouparea]['Competitor']) {
                      if (tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'] && tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'] > 0) {
                        listbodytable += '<td > '+ parseFloat((tdata[grouparea]['Competitor'][competitorid]['Market Share (%)'] * 100).toFixed(2)) +' % </td>';
                      } else {
                        listbodytable += '<td > '+0+' % </td>';
                      }
                    }

                  listbodytable += '<td>'+ 100 +'%</td>'
                                  +'<td colspan="2"> </td>'
                                  +'<td colspan="2"> </td>'
                                  +'</tr>';

            listtablecontente += '<th width="*" rowspan="2" style=" background-color: yellow; vertical-align: middle;">&nbsp;AVG IND</th>'
                    +'<th width="10%" colspan="2" style=" background-color: yellow; ">SMIG STD</th>'
                    +'<th width="10%" colspan="2" style=" background-color: green; color: white;" >SNI '+sni_name_product+'</th>'
                    +'</tr>'
                    +'<tr>'
                    +'<th style=" background-color: deepskyblue; vertical-align: middle;">PARAMETER</th>'
                    +'<th style=" background-color: yellow; vertical-align: middle;">MIN</th>'
                    +'<th style=" background-color: yellow; vertical-align: middle;">MAX</th>'

                    +'<th style=" background-color: green; vertical-align: middle; color: white;" >MIN</th>'
                    +'<th style=" background-color: green; vertical-align: middle; color: white;" >MAX</th>'

                    +'</tr>'
                    +'</thead>'
                    +'<tbody>'+listbodytable
                    +'</tbody>';
            
            listtablecontent += listtablecontente + '</table>'
                    +'</div><div class="col-sm-2 col-md-2 col-lg-2 pull right"><button type="button" id="exp-'+grouparea.replace(" ", "_")+'" onclick="exportSVGChart(\'exp-'+grouparea.replace(" ", "_")+'\',\''+grouparea.replace(" ", "_")+'ExportImageChart\')" data-loading-text="<i class=\'fa fa-spinner fa-spin\'></i> &nbsp;<font class=\'lowercase\'></font>" class="btn btn-block btn-warning btn-sm btn-flat pull-left" style="margin: 10px;"><i class="fa fa-file-image-o"></i> &nbsp; Convert Charts</button></div><div class="col-sm-12 col-sm-12" style="margin-top: 20px; margin-bottom: 20px;" id="'+grouparea.replace(" ", "_")+'Container" >'
                    +'<div class="row col-sm-12 col-sm-12" id="'+grouparea.replace(" ", "_")+'chartCS">'
                    +'<div class="form-group container-for-chart col-sm-4 col-md-4 col-lg-4" style="min-height: 650px; border: solid 1px red;background-color: #efefef;"><div id="'+grouparea.replace(" ", "_")+'cs3D" style="width: 295px; height: 648px; margin-top: 1em"></div></div>'
                    +'<div class="form-group container-for-chart col-sm-4 col-md-4 col-lg-4" style="min-height: 650px; border: solid 1px red;background-color: #efefef;"><div id="'+grouparea.replace(" ", "_")+'cs7D" style="width: 295px; height: 648px; margin-top: 1em"></div></div>'  
                    +'<div class="form-group container-for-chart col-sm-4 col-md-4 col-lg-4" style="min-height: 650px; border: solid 1px red;background-color: #efefef;"><div id="'+grouparea.replace(" ", "_")+'cs28D" style="width: 295px; height: 648px; margin-top: 1em"></div></div>' 
                    +'</div>'
                    +'<div class="col-sm-12 col-sm-12" id="'+grouparea.replace(" ", "_")+'chartCSLegend" style="height: 105px;">'
                    +'<svg id="'+grouparea.replace(" ", "_")+'legendContainer" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" class="highcharts-container" style="width: 100%;overflow:scroll;margin:0;"></svg></div>'
                    +'</div>'
                    +'<div id="'+grouparea.replace(" ", "_")+'compressivestrength" class="form-group col-sm-12 col-md-12 col-lg-12" style="display: none;"></div>'

                    +'<div id="'+grouparea.replace(" ", "_")+'ExportImageChart" class="form-group col-sm-12 col-md-12 col-lg-12" style="min-height: 850px;">'
          					+'<div class="form-group col-sm-12 col-md-12 col-lg-12">'
          					+'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
          					+'<canvas id="'+grouparea.replace(" ", "_")+'cs3Dcanvas" width="319" height="648"></canvas>'
          					+'</div>'
          					+'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
          					+'<canvas id="'+grouparea.replace(" ", "_")+'cs7Dcanvas" width="319" height="648"></canvas>'
          					+'</div>'
          					+'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
          					+'<canvas id="'+grouparea.replace(" ", "_")+'cs28Dcanvas" width="319" height="648"></canvas>'
          					+'</div>'
          					+'</div>'
          					+'<div class="form-group col-sm-12 col-md-12 col-lg-12" id="'+grouparea.replace(" ", "_")+'chartCSLegendcanvas">'
          					+'<canvas id="'+grouparea.replace(" ", "_")+'legendContainercanvas" width="1100" height="105"></canvas>'
          					+'</div>'
          					+'</div>';

              glbtagchart.push('#'+grouparea.replace(" ", "_"));

              listtable += listtablecontent;
              valseries.push(tmpseries['CS3D']);
              valseries.push(tmpseries['CS7D']);
              valseries.push(tmpseries['CS28D']);


              tmplegend.push({tmpchart: ''+grouparea.replace(" ", "_")+'compressivestrength', legendname: '#'+grouparea.replace(" ", "_")+'legendContainer'});
              tmplegend.push({tmpchart: ''+grouparea.replace(" ", "_")+'compressivestrength', legendname: '#'+grouparea.replace(" ", "_")+'legendContainer'});
              tmplegend.push({tmpchart: ''+grouparea.replace(" ", "_")+'compressivestrength', legendname: '#'+grouparea.replace(" ", "_")+'legendContainer'});

                    tgroupareaarr.push(grouparea.replace(" ", "_"));

                  tgroupareaarrsplit.push(grouparea.replace(" ", "_")+'cs3D');
                  tgroupareaarrsplit.push(grouparea.replace(" ", "_")+'cs7D');
                  tgroupareaarrsplit.push(grouparea.replace(" ", "_")+'cs28D');

                    var avgind = {
                                  type: 'spline',
                                  name: 'Average Ind',
                                  color: 'yellow',
                                  data: [{ y: avgindval, id: 'Average Ind'}] ,
                                  marker: {
                                      lineWidth: 1,
                                      lineColor: 'yellow',
                                      fillColor: 'white'
                                  }
                              };

                    var tdatachartset = [{
                                          type: 'spline',
                                          name: 'SMIG Min',
                                          color: 'red',
                                          data: [{ y: smigmin, id: 'SMIG Min'}] ,
                                          marker: {
                                              lineWidth: 1,
                                              lineColor: 'red',
                                              fillColor: 'white'
                                          }
                                      },{
                                          type: 'spline',
                                          name: 'SMIG Max',
                                          data: [{ y: smigmax, id: 'SMIG Max'}] ,
                                          color: 'green',
                                          marker: {
                                              lineWidth: 1,
                                              lineColor: 'green',
                                              fillColor: 'white'
                                          }
                                      }];

                    for (var competitorname in contentscatter) {

                      var tcontentscatter = {
                                            type: 'scatter',
                                            name: competitorname,
                                            data: [{ y: contentscatter[competitorname], id: competitorname}] ,
                                            marker: {
                                                radius: 4
                                            }
                                        };

                      tdatachartset.push(tcontentscatter);

                    }

                    tdatachartset.push(avgind);
                    tgroupchartarr.push(tdatachartset);


                  ctabview += '<div class="tab-pane '+isactivetab+'" id="tab_'+ grouparea.replace(" ", "_") +'">'+listtablecontent+'</div>';

                  listtableexp += listtablecontente + '<tbody><tr>'
                  +'<td></td>'
                  +'</tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr></tbody>'; 

                imgidhighchart.push('Image'+grouparea.replace(" ", "_")+'CS3D');
                imgidhighchart.push('Image'+grouparea.replace(" ", "_")+'CS7D');
                imgidhighchart.push('Image'+grouparea.replace(" ", "_")+'CS8D');

              }

            if (htabview != '') {
               $('#exportExcelBtne').show(); 
            }

            tabview = '<ul class="nav nav-tabs">'+htabview+'</ul><div class="tab-content">'+ctabview+'</div>';
            $('#dataqomparison').html(tabview);

            $('#reportview').html('<table id="tblreportviewexp" class="tblreportview table table-responsive" border="1" style="width: 100%;font-size: 11px;">'+listtableexp+'</table>'); 

                loadsplitchart(tgroupareaarrsplit, tgroupchartarr, valseries, tmpavgind, tmpsmigmin, tmpsmigmax, tmplegend, imgidhighchart, tmpsniproduct);

            } else {
            	$('#dataqomparison').html('<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-info"></i> Alert!</h4>'+result['Message']+'.</div>');
            }

            for (var indextag in glbtagchart) {
              $(glbtagchart[indextag]+'ExportImageChart').hide();
            }

            $('#saving').css('display','none');
            $('#btLoad').button('reset');

        },
        error: function () {
            $('#saving').css('display','none');
            $('#btLoad').button('reset');
        }
    });

  } else {
    var national = '';
    var listtablecontent = '';
    var listtablecontente = '';
  var listbodytable = '';
  var listbodytablems = '';
  var listbodytableclinker = '';
    var listtableexp = '';
  var listbodytablehho = '';
  var listcompany = new Array();
  var clincker_factor = new Array();
  var temp_component = new Array();
  var compressive_chart = new Array();
  var tmpavgind = new Array();
  var tmpsmigmin = new Array();
  var tmpsmigmax = new Array();
  var tmpseries = new Array();
  var tmplegend = new Array();           
  var tmpsniproduct = new Array();

    tmpseries['CS3D'] = new Array();
    tmpseries['CS7D'] = new Array();
    tmpseries['CS28D'] = new Array();

  var tmpdatasplitchart = new Array();

    $('#dataqomparison').html(national);

    $.ajax({
        url: "<?php echo site_url("quality_comparison_report/load_tabel_report_national");?>",
        data: {"id_company": id_company, "tanggal": tanggal, "id_typeproduct": id_typeproduct, "areagroup": areagroup},
        dataType: 'json',
        type: 'POST',
        success: function (result) {

            if (result['Status'] == true && result['Data']) {

              var listdata = result['Data'];
                  listtablecontent += '<div class="box-body table-responsive no-padding" style=" display: -webkit-inline-box;">'
                  +'<table class="tblreportview comparison" id="TableContentNational" border="1" style="width: 100%;font-size: 11px;">';

              glbidtable.push('TableContentNational');

                  listtablecontente +='<thead>'
                  +'<tr>'
                  +'<th width="10%" style=" background-color: deepskyblue; ">National</th>';

                  listbodytablems = '<tr style="background-color: #cdf3ff;"><td> Market Share </td>';
                  listbodytableclinker = '<tr style="background-color: #ffddd9;color: #bd0000;"><td> Clinker Factor Predicted </td>';
                  listbodytablehho = '<tr style=""><td> H<sub>2</sub>O in Cement Predicted </td>';

                      for (var competitorname in listdata['SMI']) {
                        listtablecontente += '<th width="*" rowspan="2" style=" background-color: deepskyblue; vertical-align: middle;">&nbsp;'+listdata['SMI'][competitorname]['INISIAL']+'&nbsp;</th>';
                        listcompany.push(listdata['SMI'][competitorname]['INISIAL']);
                        clincker_factor.push(parseFloat((listdata['SMI'][competitorname]['CLINKER FACTOR PREDICTED (%)']).toFixed(2)));

                        temp_component = listdata['SMI'][competitorname]['Component Data'];

                        listbodytablems += '<td > '+ converse_value(parseFloat(parseFloat(listdata['SMI'][competitorname]['Prosentase MS']) * 100).toFixed(2)) +' % </td>';

                      listbodytableclinker += '<td > '+ converse_value(parseFloat((listdata['SMI'][competitorname]['CLINKER FACTOR PREDICTED']).toFixed(2))) +' </td>';
                      listbodytablehho += '<td > '+ converse_value(parseFloat((listdata['SMI'][competitorname]['H2O IN CEMENT PREDICTED']).toFixed(2))) +' </td>';

                      }

                      for (var competitorname in listdata['Competitor']) {
              listtablecontente += '<th width="*" rowspan="2" style=" background-color: deepskyblue; vertical-align: middle;">&nbsp;'+listdata['Competitor'][competitorname]['INISIAL']+'&nbsp;</th>';
                        listcompany.push(listdata['Competitor'][competitorname]['INISIAL']);
                        clincker_factor.push(parseFloat((listdata['Competitor'][competitorname]['CLINKER FACTOR PREDICTED (%)']).toFixed(2)));

                        temp_component = listdata['Competitor'][competitorname]['Component Data'];

                        listbodytablems += '<td > '+ converse_value(parseFloat(parseFloat(listdata['Competitor'][competitorname]['Prosentase MS']) * 100).toFixed(2)) +' % </td>';
                      listbodytableclinker += '<td > '+ converse_value(parseFloat((listdata['Competitor'][competitorname]['CLINKER FACTOR PREDICTED (%)']).toFixed(2))) +' </td>';
                      listbodytablehho += '<td > '+ converse_value(parseFloat((listdata['Competitor'][competitorname]['H2O IN CEMENT PREDICTED']).toFixed(2))) +' </td>';

                      }

                  listbodytablems += '<td colspan="5" style="background-color: yellow;"> 100 % </td></tr>';
                  listbodytableclinker += '<td colspan="5"> </td></tr>';
                  listbodytablehho += '<td colspan="5"> </td></tr>';

          for (var componentname in temp_component) {

            var tmpavgindval = 0;

            listbodytable += '<tr><td>' + temp_component[componentname]['Component Name'] + '</td>';

            for (var competitorname in listdata['SMI']) {
              listbodytable += '<td>'+ converse_value(listdata['SMI'][competitorname]['Component Data'][componentname]['ReRata']) +'</td>';

              if (temp_component[componentname]['Component Name'] == 'CS3D' || temp_component[componentname]['Component Code'] == 'CS3D') {
                            tmpseries['CS3D'].push({name: listdata['SMI'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['SMI'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['SMI'][competitorname]['INISIAL']}]});
                          } else if (temp_component[componentname]['Component Name'] == 'CS7D' || temp_component[componentname]['Component Code'] == 'CS7D') {
                            tmpseries['CS7D'].push({name: listdata['SMI'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['SMI'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['SMI'][competitorname]['INISIAL']}]});
                          } else if (temp_component[componentname]['Component Name'] == 'CS28D' || temp_component[componentname]['Component Code'] == 'CS28D') {
                            tmpseries['CS28D'].push({name: listdata['SMI'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['SMI'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['SMI'][competitorname]['INISIAL']}]});
                          }

                          tmpavgindval +=  parseFloat((listdata['SMI'][competitorname]['Prosentase MS'])) * parseFloat((listdata['SMI'][competitorname]['Component Data'][componentname]['ReRata']));

                      }

                      for (var competitorname in listdata['Competitor']) {
              listbodytable += '<td>'+ converse_value(listdata['Competitor'][competitorname]['Component Data'][componentname]['ReRata']) +'</td>';

                    if (temp_component[componentname]['Component Name'] == 'CS3D' || temp_component[componentname]['Component Code'] == 'CS3D') {
                              tmpseries['CS3D'].push({name: listdata['Competitor'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['Competitor'][competitorname]['INISIAL']}]});
                          } else if (temp_component[componentname]['Component Name'] == 'CS7D' || temp_component[componentname]['Component Code'] == 'CS7D') {
                              tmpseries['CS7D'].push({name: listdata['Competitor'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['Competitor'][competitorname]['INISIAL']}]});
                          } else if (temp_component[componentname]['Component Name'] == 'CS28D' || temp_component[componentname]['Component Code'] == 'CS28D') {
                              tmpseries['CS28D'].push({name: listdata['Competitor'][competitorname]['INISIAL'], data: [{ y: parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['ReRata'])), id: listdata['Competitor'][competitorname]['INISIAL']}]});
                          }

                          tmpavgindval +=  parseFloat((listdata['Competitor'][competitorname]['Prosentase MS'])) * parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['ReRata']));

                      }

            listbodytable += '<td>'+ converse_value(tmpavgindval.toFixed(2)) +'</td>';
            listbodytable += '<td>'+ converse_value(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['V_Min']).toFixed(2))) +'</td>';
            listbodytable += '<td>'+ converse_value(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['V_Max']).toFixed(2))) +'</td>';

            if (temp_component[componentname]['Component Name'] == 'CS3D' || temp_component[componentname]['Component Code'] == 'CS3D' || temp_component[componentname]['Component Name'] == 'CS7D' || temp_component[componentname]['Component Code'] == 'CS7D' || temp_component[componentname]['Component Name'] == 'CS28D' || temp_component[componentname]['Component Code'] == 'CS28D') {

              tmpavgind.push(parseFloat((tmpavgindval).toFixed(2)));
              tmpsmigmin.push(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['V_Min']).toFixed(2)));
              tmpsmigmax.push(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['V_Max']).toFixed(2)));

              tmpsniproduct.push(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['SNI_Min']).toFixed(1)));

            }

            listbodytable += '<td>&nbsp;'+converse_value(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['SNI_Min']).toFixed(1)))+'&nbsp;</td>';

            listbodytable += '<td>&nbsp;'+converse_value(parseFloat((listdata['Competitor'][competitorname]['Component Data'][componentname]['SNI_Max']).toFixed(1)))+'&nbsp;</td></tr>';

          }

              listtablecontente += '<th width="*" rowspan="2" style=" background-color: yellow; vertical-align: middle;">&nbsp;AVG IND</th>'
                      +'<th width="10%" colspan="2" style=" background-color: yellow; ">SMIG STD</th>'

                      +'<th width="10%" colspan="2" style=" background-color: green; color: white;" >SNI '+sni_name_product+'</th>'

                      +'</tr>'
                      +'<tr>'
                      +'<th style=" background-color: deepskyblue; vertical-align: middle;">PARAMETER</th>'
                      +'<th style=" background-color: yellow; vertical-align: middle;">MIN</th>'
                      +'<th style=" background-color: yellow; vertical-align: middle;">MAX</th>'

                      +'<th style=" background-color: green; vertical-align: middle; color: white;" >MIN</th>'
                      +'<th style=" background-color: green; vertical-align: middle; color: white;" >MAX</th>'

                      +'</tr>'
                      +'</thead>'
                      +'<tbody>'+listbodytable+listbodytablems+listbodytableclinker+listbodytablehho
                      +'</tbody>';
              listtablecontent += listtablecontente +'</table>'
                      +'</div>'
                      +'<div id="NationaCharts" class="form-group col-sm-12 col-md-12 col-lg-12">'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px; margin-bottom: 20px;border: solid 1px red;background-color: #efefef;"><div id="NationalU" style="width: 950px; height: 400px; margin-top: 1em"></div></div>'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12" id="nationalchartCS">'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;"><div id="nationalcs3D"  style="width: 295px; height: 648px; margin-top: 1em"></div></div>'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;"><div id="nationalcs7D"  style="width: 295px; height: 648px; margin-top: 1em"></div></div>'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;"><div id="nationalcs28D"  style="width: 295px; height: 648px; margin-top: 1em"></div></div>'
                      +'</div>'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12" id="nationalchartCSLegend" style="height: 105px;">'
                      +'<svg id="legendContainer" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" class="highcharts-container" style="width: 100%;overflow:scroll;margin:0;"></svg></div>'
                      +'</div>'
                      +'<div id="nationalcompressivestrength" class="form-group col-sm-12 col-md-12 col-lg-12" style="display: none;"></div>'

                      +'<div id="ExportImageChart" class="form-group col-sm-12 col-md-12 col-lg-12" style="min-height: 1250px;">'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12" style="margin-top: 20px; margin-bottom: 20px;border: solid 1px red;background-color: #efefef;">'
                      +'<canvas id="NationalUcanvas" width="1050" height="400" ></canvas>'
                      +'</div>'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12">'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
                      +'<canvas id="nationalcs3Dcanvas" width="319" height="648"></canvas>'
                      +'</div>'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
                      +'<canvas id="nationalcs7Dcanvas" width="319" height="648"></canvas>'
                      +'</div>'
                      +'<div class="form-group col-sm-4 col-md-4 col-lg-4" style="min-height: 650px;border: solid 1px red;background-color: #efefef;">'
                      +'<canvas id="nationalcs28Dcanvas" width="319" height="648"></canvas>'
                      +'</div>'
                      +'<div class="form-group col-sm-12 col-md-12 col-lg-12" id="nationalchartCSLegend">'
                      +'<canvas id="legendContainercanvas" width="1100" height="105"></canvas>'
                      +'</div>'
                      +'</div>';

              compressive_chart.push('nationalcs3D');
              compressive_chart.push('nationalcs7D');
              compressive_chart.push('nationalcs28D');



              imgidhighchart.push('ImageNationalCS3D');
              imgidhighchart.push('ImageNationalCS7D');
              imgidhighchart.push('ImageNationalCS8D');


              tmplegend.push({tmpchart: 'nationalcompressivestrength', legendname: '#legendContainer'});
              tmplegend.push({tmpchart: 'nationalcompressivestrength', legendname: '#legendContainer'});
              tmplegend.push({tmpchart: 'nationalcompressivestrength', legendname: '#legendContainer'});

              for (var componentname in tmpseries) {

                tmpdatasplitchart.push(tmpseries[componentname]);

              }


          $('#exportExcelBtne').show();
          $('#convert').show();
          

          $('#dataqomparison').html(listtablecontent);
          loadChartNational(clincker_factor, listcompany);

          loadsplitchart(compressive_chart, null , tmpdatasplitchart, tmpavgind, tmpsmigmin, tmpsmigmax, tmplegend, imgidhighchart, tmpsniproduct);


              $('#reportview').html('<table id="tblreportviewexp" class="tblreportview table table-responsive" border="1" style="width: 100%;">'+listtablecontente
                +'<tbody><tr>'
                    +'</tr><tr><td></td></tr><tr><td></td></tr><tr><td></td></tr></tbody></table>'); 

            } else {
            	$('#dataqomparison').html('<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-info"></i> Alert!</h4>'+result['Message']+'.</div>');
            }

            $('#saving').css('display','none');
            $('#btLoad').button('reset');
            $('#ExportImageChart').hide();


        },
        error: function () {

            $('#saving').css('display','none');
            $('#btLoad').button('reset');
        }
    });

  }

}

function converse_value(val) {
  // body...
  if (parseFloat(val) == 0) {
    return '-';
  } else {
    return val;
  }
}


function loadChartNational(data, series) {
  // body...

  var options = {

      chart: {
        backgroundColor: '#efefef'
      },
      exporting: {
          url: 'http://export.highcharts.com/'
      },
      title: {
          text: 'CLINKER FACTOR PREDICTION'
      },

      credits: {
            enabled: false
        },

      xAxis: {
          categories: series
      },

      yAxis: {
        title: {
          enabled : true,
          text : 'Values (%)',
          style : {
              fontWeight:'bold'
          }
        },
      },

      plotOptions: {
          series: {
              dataLabels: {
                  enabled: true,
                  formatter: function() {
                      if (this.y === 0) {
                          return null;
                      } else {
                          return this.y+' %';
                      }
                  }
              }
          }
      },

      series: [{
          type: 'column',
          colorByPoint: true,
          name: 'Clicker Factor',
          data: data,
            tooltip: {
              headerFormat: '<b>{series.name}</b> : ',
              pointFormat: '{point.y} %'
            },
          showInLegend: false
      }]

  };

  var chart = Highcharts.chart('NationalU', {

      chart: {
        backgroundColor: '#efefef'
      },
      title: {
          text: 'CLINKER FACTOR PREDICTION'
      },

      credits: {
            enabled: false
        },

      xAxis: {
          categories: series
      },

       yAxis: {
        title: {
          enabled : true,
          text : 'Values (%)',
          style : {
              fontWeight:'bold'
          }
        },
      },

        plotOptions: {
            series: {
                dataLabels: {
                  enabled: true,
                  formatter: function() {
                      if (this.y === 0) {
                          return null;
                      } else {
                          return this.y+' %';
                      }
                  }
                }
            } 
        },

      series: [{
          type: 'column',
          colorByPoint: true,
            name: 'Clicker Factor',
          data: data,
            tooltip: {
              headerFormat: '<b>{series.name}</b> : ',
              pointFormat: '{point.y} %'
            },
          showInLegend: false
      }]

  });

  var svg = chart.getSVG();
  canvg(document.getElementById('NationalUcanvas'), svg);

  var obj = {},  
      exportUrl = 'http://export.highcharts.com/';
      obj.options = JSON.stringify(options);
      obj.type = 'image/png';
      obj.async = true;
}

function loadChart(data, series) {
  // body...
  for (var i = 0; i < data.length; i++) {
    var chart = Highcharts.chart(data[i], {

        title: {
            text: (data[i]).replace("_", " ")
        },

        credits: {
            enabled: false
        },

        exporting: {
            enabled: false
        },

        subtitle: {
            text: 'Compressive Strength Chart'
        },

        xAxis: {
            categories: ['3 Days', '7 Days', '28 Days']
        },

        series: series[i]
    });

  }
}

function loadsplitchart(data, series, valseries, tmpavgind, tmpsmigmin, tmpsmigmax, tmplegend, imgidhighchart, tmpsniproduct) {
  // body...

  var chart;
  // Define a custom symbol path
  Highcharts.SVGRenderer.prototype.symbols.minus = function (x, y, w, h) {
      return ['M', x, y + h / 2, 'L', x + w, y + h / 2, 'z'];
  };
  if (Highcharts.VMLRenderer) {
      Highcharts.VMLRenderer.prototype.symbols.minus = Highcharts.SVGRenderer.prototype.symbols.minus;
  }

  var tmplegendchart = new Array();
  var ind = 0;

  for (var i = 0; i < data.length; i++) {

    var csdays = 3;
    var legend = true;

    if (i % 3 == 1) {
      csdays = 7;
      legend = false;
    } else if (i % 3 == 2) {
      csdays = 28;
      legend = false;
    }

    var nval = 350;

    var list = valseries[i];

      var tsortarray = new Array();

      for (var a = 0; a < valseries[i].length; a++) {
        tsortarray.push(valseries[i][a]['data'][0]);
      }

    list.push({
              name: 'SMIG Min',
              type: 'scatter',
              marker: {
                // enabled: true
                symbol: 'minus',
                /* fillColor: 'red', */
                lineColor:'red',
                radius:4,
                lineWidth: 1
              },
              tooltip: {
                headerFormat: '<b>{series.name}</b> : ',
              pointFormat: '{point.y}'
                },
              color: 'red',
              data: [{ y: tmpsmigmin[i], id: 'SMIG Min'}]
          });

    list.push({
              name: 'AVG Ind',
              type: 'scatter',
              marker: {                
                // enabled: true
                symbol: 'minus',
                /* fillColor: 'red', */
                lineColor:'yellow',
                radius:4,
                lineWidth: 1
              },
              tooltip: {
                headerFormat: '<b>{series.name}</b> : ',
              pointFormat: '{point.y}'
                },
              color: 'yellow',
              data: [{ y: tmpavgind[i], id: 'AVG Ind'}]
          });

    list.push({
              name: 'SMIG Max',
              type: 'scatter',
              marker: {
                // enabled: true
                symbol: 'minus',
                /* fillColor: 'red', */
                lineColor:'green',
                radius:4,
                lineWidth: 1
              },
              tooltip: {
                headerFormat: '<b>{series.name}</b> : ',
              pointFormat: '{point.y}'
                },
              color: 'green',
              data: [{ y: tmpsmigmax[i], id: 'SMIG Max'}]
        });

    list.push({
            name: 'SNI Product',
            type: 'scatter',
            marker: {
              // enabled: true
              symbol: 'minus',
              /* fillColor: 'red', */
              lineColor:'blue',
              radius:4,
              lineWidth: 1
            },
            tooltip: {
              headerFormat: '<b>{series.name}</b> : ',
            pointFormat: '{point.y}'
              },
            color: 'blue',
            data: [{ y: tmpsniproduct[i], id: 'SNI Product'}]
      });

      tsortarray.sort(function (a, b) {
          return a.y - b.y;
      });

      var tindex = 0;
      var tlabelsarray = new Array();

      var yaxislabelright = 5 ;
      var yaxislaberleft = 5 ;

      for (var index in tsortarray) {
        if (tindex % 2 == 0) {
            // tlabelsarray.push({ point: tsortarray[index]['id'], align: 'right', x: -20, y: yaxislabelright, text: tsortarray[index]['id']+' : {point.y}', backgroundColor: 'white', style: { fontSize: '8px' } });
            tlabelsarray.push({ point: tsortarray[index]['id'], align: 'right', x: -20, y: yaxislabelright, text: tsortarray[index]['id'] , backgroundColor: 'white', style: { fontSize: '12px' } });
            yaxislabelright = yaxislabelright-25;

        } else {
            // tlabelsarray.push({ point: tsortarray[index]['id'], align: 'left', x: 20, y: yaxislaberleft, text: tsortarray[index]['id']+' : {point.y}', backgroundColor: 'white', style: { fontSize: '8px' } });
            tlabelsarray.push({ point: tsortarray[index]['id'], align: 'left', x: 20, y: yaxislaberleft, text: tsortarray[index]['id'] , backgroundColor: 'white', style: { fontSize: '12px' } });
            yaxislaberleft = yaxislaberleft-25;

        }
        tindex++;
      }

      var options = {

        chart: {
          backgroundColor: '#efefef'
        },

        exporting: {
            url: 'http://export.highcharts.com/'
        },
        title: {
              text: 'Compressive Strenght <br> '+ csdays +' Days'
          },
          credits: {
              enabled: false
          },
          xAxis: {
              categories: [csdays + ' Days']
          },
          yAxis: {
          min: 100,
          max: 500,
          tickInterval: 50,
              plotLines:[{
                  value: tmpsmigmin[i],
                  color: 'red',
                  width: 2,
                  zIndex: 4,
                  label: {
                  text: ''
                  }
              },{
                  value: tmpavgind[i],
                  color: 'yellow',
                  width: 2,
                  zIndex: 4,
                  label: {
                  text: ''
                   }
              },{
                  value: tmpsmigmax[i],
                  color: 'green',
                  width: 2,
                  zIndex: 4,
                  label: {
                  text: ''
                   }
              },{
                  value: tmpsniproduct[i],
                  color: 'blue',
                  dashStyle: 'DashDot',
                  width: 2,
                  zIndex: 4,
                  label: {
                  text: ''
                   }
              }]

          },

        series: list,

          annotations: [{
              labels: tlabelsarray,
              labelOptions: {
                  allowOverlap: true
              }
          }]
    };

    var chart = Highcharts.chart(data[i], {

      chart: {
        backgroundColor: '#efefef'
      },
      title: {
              text: 'Compressive Strenght <br> '+ csdays +' Days'
          },
          credits: {
              enabled: false
          },
          xAxis: {
              categories: [csdays + ' Days']
          },
          yAxis: {
            min: 100,
            max: 500,
            tickInterval: 50,
              plotLines:[{
                  value: tmpsmigmin[i],
                  color: 'red',
                  width: 2,
                  zIndex: 4,
                  label: {
                    text: ''
                  }
              },{
                  value: tmpavgind[i],
                  color: 'yellow',
                  width: 2,
                  zIndex: 4,
                  label: {
                    text: ''
                   }
              },{
                  value: tmpsmigmax[i],
                  color: 'green',
                  width: 2,
                  zIndex: 4,
                  label: {
                    text: ''
                   }
              },{
                  value: tmpsniproduct[i],
                  color: 'blue',
                  dashStyle: 'DashDot',
                  width: 2,
                  zIndex: 4,
                  label: {
                  text: ''
                   }
              }]
          },

            legend: {
              enabled: false,
            },

          series: list,

            annotations: [{
                labels: tlabelsarray,
                labelOptions: {
                    allowOverlap: true
                }
            }]

    });

    setCanvas(chart, data[i]);

    var obj = {},  
      exportUrl = 'http://export.highcharts.com/';
      obj.options = JSON.stringify(options);
      obj.type = 'image/png';
      obj.async = true;

      var tagid = "#"+imgidhighchart[i];

      if (i % 3 == 2) {

        var tmplegendcharte = new Array();

        var chartlegends = Highcharts.chart(tmplegend[i]['tmpchart'], {
          chart: {
                  events: {
                      load: function (event) {
                          tmplegendcharte = $(".highcharts-legend").get();
                          tmplegendchart.push(tmplegendcharte[ind]);

                          $(tmplegend[i]['legendname']).append(tmplegendcharte[(tmplegendcharte.length) - 1]);
                          $(".highcharts-legend").removeAttr('transform');

                          var svg = document.getElementById((tmplegend[i]['legendname']).replace("#", ""));

                          var canvas = document.getElementById((tmplegend[i]['legendname']).replace("#", "")+'canvas');
                          var ctx = canvas.getContext('2d');
                          var data = (new XMLSerializer()).serializeToString(svg);
                          var DOMURL = window.URL || window.webkitURL || window;

                          var img = new Image();
                          var svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
                          var url = DOMURL.createObjectURL(svgBlob);

                          img.onload = function () {
                            ctx.drawImage(img, 0, 0);
                            DOMURL.revokeObjectURL(url);

                            var imgURI = canvas
                                .toDataURL('image/png');

                          };

                          img.src = url;

                      },
                      redraw: function(){
                          $(".highcharts-legend").removeAttr('transform');
                      }
                  },
                  backgroundColor: '#efefef'
              },

          title: {
                  text: 'Compressive Strenght <br> '+ csdays +' Days'
              },
              credits: {
                  enabled: false
              },
              xAxis: {
                  categories: [csdays + ' Days']
              },
              yAxis: {
                min: 50,
                max: 500,
                tickInterval: 10,
                  plotLines:[{
                      value: tmpsmigmin[i],
                      color: 'red',
                      width: 2,
                      zIndex: 4,
                      label: {
                        text:'SMIG Min'
                      }
                  },{
                      value: tmpavgind[i],
                      color: 'yellow',
                      width: 2,
                      zIndex: 4,
                      label: {
                        text:'AVG Ind'
                       }
                  },{
                      value: tmpsmigmax[i],
                      color: 'green',
                      width: 2,
                      zIndex: 4,
                      label: {
                        text:'SMIG Max'
                       }
                  },{
                      value: tmpsniproduct[i],
                      color: 'blue',
                      dashStyle: 'DashDot',
                      width: 2,
                      zIndex: 4,
                      label: {
                      text: ''
                       }
                  }]
              },

              series: list

        });

      }

  }

}

function setCanvas(chart, tagid) {
	// body...
  var svg = chart.getSVG();
  canvg(document.getElementById(tagid+"canvas"), svg);
}

function setImageSRC(exportUrl, obj, tagid) {
  // body...
  $.ajax({
        type: 'post',
        url: exportUrl,
        data: obj,
        success: function (data) {
        }
    });
}

function loadCompany() {
  var merk = $('#ID_COMPANY');
  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_company_scm");?>', function (result) {
    var values = result;
    merk.find('option').remove();
    if (values != undefined && values.length > 0) {
      merk.css("display","");
      $(values).each(function(index, element) {
        merk.append($("<option></option>").attr("value", element.KODE_PERUSAHAAN).text(element.NAMA_PERUSAHAAN));
      });
    }else{
      merk.find('option').remove();
      merk.append($("<option></option>").attr("value", '00').text("NO COMPANY"));
    }
  });
}

function importCanvas(sourceCanvas, targetSVG, imgURI) {
    // get base64 encoded png data url from Canvas
    var img_dataurl = imgURI;


    var svg_img = document.createElementNS(
        "http://www.w3.org/2000/svg", "image");

    svg_img.setAttributeNS(
        "http://www.w3.org/1999/xlink", "xlink:href", img_dataurl);

    targetSVG.appendChild(svg_img);
}

function exportSVGChart(btnid,tagid) {
  // body...

  $('#'+btnid).button('loading');
  $("#"+tagid).show();

  html2canvas($("#"+tagid), {
    onrendered: function(canvas) {
      var imgsrc = canvas.toDataURL("image/png");
      var name = tagid.replace("Container", "");
      var area = $('#AREA_GROUP option:selected').text();
      var company = $('#ID_COMPANY option:selected').text();
      var v = new MouseEvent("click", {
              view: window,
              bubbles: false,
              cancelable: true
          });
      var u = document.createElement("a");
      u.setAttribute("download", company+" "+area+" "+name+".png");
      u.setAttribute("href", imgsrc);
      u.setAttribute("target", "_blank");
      u.dispatchEvent(v);

      $('#'+btnid).button('reset');
      $("#"+tagid).hide();

    }
  });

}

function exportSVG(divContainer, title) {
  // body...
  // first create a clone of our svg node so we don't mess the original one
  var svgContainer = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  $(divContainer).find("svg").clone().appendTo(svgContainer)
  // parse the styles
  // get the data
  var svgData = svgContainer.outerHTML;

  var v = new MouseEvent("click", {
                view: window,
                bubbles: false,
                cancelable: true
            });

  // here I'll just make a simple a with download attribute
  var a = document.createElement('a');

  a.setAttribute("download", title+'ChartCS.svg');
  a.setAttribute("href", 'data:image/svg+xml; charset=utf8, ' + encodeURIComponent(svgData));
  a.setAttribute("target", "_blank");
  a.dispatchEvent(v);
}

</script>
