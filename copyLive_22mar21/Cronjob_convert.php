<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob_convert extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_parameter_order");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_daily"); 
		$this->load->model("d_production_daily");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily"); 
		$this->load->model("C_range_component");
		
        $this->load->model("m_cronjob");
        $this->load->model("t_cement_daily");
        $this->load->model("t_production_daily");
	}

	public function index(){ 
        $this->libExternal('datepicker');
        $this->libExternal('select2'); 
		$this->template->adminlte("v_manual_cronjob_hourly");
	} 
	
	
	 public function get_data()
    { 
        $detail_data = $this->m_cronjob->get_data();
		echo json_encode($detail_data);

    }
	 
    
	public function simpan() {
		if(isset($_POST["tgl_data_start"]) ){ 
		// $cek_data = $this->m_cronjob->cek_data($_POST["tgl_data_start"], '');
			// if(count($cek_data)<1){
				
				$tgl_data_start = $this->input->post('tgl_data_start');
				$tgl_data_end = $this->input->post('tgl_data_end');
				$tgl_run = $this->input->post('tgl_run'); 
				$column = array(
					'TGL_DATA_START','TGL_DATA_END','TGL_JADWAL','JAM_JADWAL', 'CREATE_BY','CREATE_DATE','DELETE_MARK','STATUS', 'ID_JADWAL'
				); 
				
				$data = array(
					"to_date('".$tgl_data_start."','dd/mm/yyyy')","to_date('".$tgl_data_end."','dd/mm/yyyy')","to_date('".$tgl_run."','dd/mm/yyyy')","'".$_POST["jam_run"]."'", "'".$this->USER->ID_USER."'","SYSDATE", "0", "0","SEQ_ID_JADWAL.NEXTVAL"
				);
				 
				$q = $this->m_cronjob->simpan($column, $data);
				 
				// $q = $this->m_cronjob->simpan($data);
				 
				if($q) {
					echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
				} else {
					echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
				}
			// }else{
				// echo json_encode(array('notif' => '0', 'message' => "Data Tanggal ".$_POST['tgl_data']." Sudah Terjadwal "));
			// }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->m_cronjob->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	public function insert_from_daily(){   
	
	// $begin = new DateTime('2010-05-01');
	// $end = new DateTime('2010-05-10');
	// $end = $end->modify( '+1 day' );

	// $interval = DateInterval::createFromDateString('1 day');
	// $period = new DatePeriod($begin, $interval, $end);

	// foreach ($period as $dt) {
		// echo $dt->format("d/m/Y")."<br>";
	// }
	// exit();
	
		 $tanggal = ISSET($_POST['TANGGAL']) ? $_POST['TANGGAL'] :  date('d/m/Y'); 
		 $jam = date('H');
		$cek_data = $this->m_cronjob->cek_data($tanggal, $jam);
		if(count($cek_data)>0){ 
			foreach($cek_data as $cd){
				$begin = new DateTime($cd['TGL_DATA_START']);
				$end_asli = new DateTime($cd['TGL_DATA_END']);
				$end = $end_asli->modify( '+1 day' );
				// print_r($begin) ;
				// print_r($end) ;
					// exit();
				$interval = DateInterval::createFromDateString('1 day');
				$period = new DatePeriod($begin, $interval, $end);
							
				foreach ($period as $dt) { 
				
					$tanggal = $dt->format("d/m/Y"); 
					echo $tanggal." -- ";
					// $tanggal = date('d/m/Y', strtotime($cd['TGL_DATA']));
					 $get_area = $this->m_cronjob->get_data_area($tanggal); 
					$arrInputData = array();
					$arrInputHeader = array();
					$arrInputDataUpdate = array();
					$arrInputHeaderUpdate = array();
					$tot_insert = 0;
					$tot_update = 0;
					foreach($get_area as $val){
						// if($val['ID_AREA']=="88"){  untuk testing clinker rembang 
							$get_data_hourly =  $this->m_cronjob->get_data_daily($val['ID_AREA'], $tanggal);
							$cek_ada_header = 1; // Hanya Data awal perulangan
							$no_field = 0; // DI CEMENT DAILY MULAI DARI 0
							foreach($get_data_hourly as $nil){
								if($cek_ada_header==1){ 
									$tdata = array();
									// PROSES INSERT HEADER T_CEMENT_DAILY
									$tdata['ID_AREA'] = $val['ID_AREA'];
									// $tdata['ID_PRODUCT'] = $nil['ID_PRODUCT'];
									$tdata['ID_PRODUCT'] = ""; // KILN DATA PRODUCT NUll
									$tdata['ID_MESIN_STATUS'] = "";
									$tdata['MESIN_REMARK'] =  "";
									$tdata['DATE_DATA'] =  $tanggal;  
									$ID_CEMENT_DAILY = $this->t_cement_daily->get_id_daily($val['ID_AREA'],"",$tanggal); 
									if(!$ID_CEMENT_DAILY){  
										$tot_insert += 1;
										$tdata['DATE_ENTRY'] = date("d/m/Y");
										$tdata['USER_ENTRY'] = "1234567890";
										$tdata['FROM_HOURLY'] = "Y";
										$ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
										array_push($arrInputHeader, $tdata);
									}
									else{
										$tot_update += 1;
										$tdata['DATE_ENTRY'] = date("d/m/Y");
										 $tdata['USER_UPDATE'] = "1234567890";
										$this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);
										array_push($arrInputHeaderUpdate, $tdata);

									}
								}
								$cek_ada_header +=1;
								
								 if($ID_CEMENT_DAILY!=""){
									// PROSES INSERT DETAIL D_CEMENT_DAILY
									$ddata = array();
									// $ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
									$ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
									$ddata['ID_COMPONENT']		= $nil['ID_COMPONENT'];
									$ddata['NILAI']				= $nil['NILAI_AVG'];
									$ddata['NO_FIELD']			= $no_field;
									$no_field  += 1 ;
									if(!$this->t_cement_daily->d_exists($ddata)){ // CEK DETAIL ADA TIDAK
										array_push($arrInputData, $ddata); // PUSH ARRAY JIKA ADA, 
									}
									else{
										 $this->t_cement_daily->d_update($ddata); // JIKA DATA ADA
										array_push($arrInputDataUpdate, $ddata);  
									}
									 
								 }
							}
						// }
					}
					
					
					if(count($arrInputData)>0){ 
						$this->t_cement_daily->insert_dcement($arrInputData); // SIMPAN ARRAY HASIL PUSH DIATAS
						echo json_encode("Sukses Tambah Data"."<br>");
						// echo json_encode(implode(" ",$arrInputData));
					}else{
						echo json_encode("Sukses Update Data"."<br>");
						// echo json_encode(implode(" ",$arrInputDataUpdate));
					}
				}
				 
				 
				$this->m_cronjob->update_jadwal(date('d/m/Y', strtotime($cd['TGL_DATA_START'])), date('d/m/Y', strtotime($cd['TGL_DATA_END'])), $tot_insert, $tot_update);
				// exit();
			}
			// echo "-------------- START DATA HEADER INSERT --------------";
			// echo json_encode($arrInputHeader); 
			// echo "-------------- END DATA HEADER INSERT --------------";
			// echo "<br>";
			// echo "-------------- START DATA HEADER UPDATE --------------";
			// echo json_encode($arrInputHeaderUpdate); 
			// echo "-------------- END DATA HEADER UPDATE --------------";
			// echo "<br>";
			
			
			// echo "-------------- START DATA DETAIL INSERT --------------";
			// echo json_encode($arrInputData); 
			// echo "-------------- END DATA DETAIL INSERT --------------";
			// echo "<br>";
			
			// echo "-------------- START DATA DETAIL UPDATE --------------";
			// echo json_encode($arrInputDataUpdate); 
			// echo "-------------- END DATA HEADER UPDATE --------------";
			// echo "<br>";
			}
		
	}

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
