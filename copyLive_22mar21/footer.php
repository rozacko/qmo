
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">

    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2017 <a href="http://www.semenindonesia.com">PT. Semen Indonesia</a>.</strong> All rights reserved.
  </footer>


</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- AdminLTE App -->
<script src="<?php echo base_url("templates/adminlte/dist/js/app.min.js") ?>"></script>
<script type="text/javascript">

  $(function(){
    var currentLoc = window.location;
    var last_url = localStorage.getItem('last_url');
    if(last_url){
      if(currentLoc == last_url){
        localStorage.removeItem('last_url');
      }else{
        window.location.href = last_url;
      }
    }
  });

  $(document).ajaxStart(function() {
    $body = $("body");
    $body.addClass("loading");
    $body.css("cursor", "progress");
  });
  $(document).ajaxComplete(function() {
    $body = $("body");
    $body.removeClass("loading");
    $body.css("cursor", "default");
  });

  // $(".btnMenu").click(function(){
  //   menu_parent = $(this).parent().parent().attr('menu_parent');
  //   menu_child = $(this).attr('menu_child');
  //   // alert(menu_parent);
  //   // alert(menu_child);
  //   $.ajax({
  //     url: "<?=base_url('menu/setMenu/')?>",
  //     type: "POST",
  //     data: {
  //       parent: menu_parent,
  //       child: menu_child
  //     },
  //     cache: false,
  //     global: false
  //   }).done(function(respon) {     
  //    // respon = JSON.parse(respon);

  //   }).fail(function (jqXHR, exception) {
  //     notif(4, 'Get Url Error!');
  //   });

  // });
</script>
<!-- JS External-->
<?php
  foreach ($this->jsExt as $k => $v) {
    echo '<script src="'.base_url('assets/'.$v).'"/></script>';
  }
?>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
     
<style>
.table-fixed {
  table-layout: auto !important;
  margin: 10px;
  min-width: 400px;
}

.table-fixed td, .table-fixed th {
  padding: 10px;
  padding-left: 25px;
  padding-right: 25px;
  border-bottom: 1px solid #eee;
}

.table-fixed th {
  border-top: 1px solid #ddd;
  border-bottom: 2px solid #ddd;
}

.tool-fixed {
  float: right !important;
  margin-right: 25px !important;
}

</style>
</body>
</html>
