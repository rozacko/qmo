<?php

class Sample_area extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_samplearea");
	}
	
	public function index(){
		$this->list_component = $this->m_samplearea->datalist();
		$this->template->adminlte("v_sample_area");
	}

	public function save_area(){
		
		$result['msg'] = 'Cannot Insert New Area ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['NAME_AREA'] = $post['NAME_AREA'];
		$data['NAME_PROVINCE'] = $post['NAME_PROVINCE'];
		$data['NAME_ISLAND'] = $post['NAME_ISLAND'];
		$data['NAME_REGION'] = $post['NAME_REGION'];
		$data['CREATE_BY'] = $post['user'];

		$isexsist_area = $this->m_samplearea->is_sample_exists($data);

		if ($isexsist_area <= 0) {
			# code...

			$isertd_area = $this->m_samplearea->sample_insert($data);
			if ($isertd_area) {
				// code...
				$result['msg'] = 'Insert New Area Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Area Already Exsist';
			$result['status'] = false;
		}
		
		to_json($result);
	}

	public function update_area(){
		
		$result['msg'] = 'Cannot Update Area ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_SAMPLE_AREA'] = $post['ID_SAMPLE_AREA'];
		$data['NAME_AREA'] = $post['NAME_AREA'];
		$data['NAME_PROVINCE'] = $post['NAME_PROVINCE'];
		$data['NAME_ISLAND'] = $post['NAME_ISLAND'];
		$data['NAME_REGION'] = $post['NAME_REGION'];
		$data['MODIFIED_BY'] = $post['user'];

		$isexsist_area = $this->m_samplearea->is_sample_exists($data);

		if ($isexsist_area <= 0) {
			# code...

			$isertd_area = $this->m_samplearea->sample_update($data);

			if ($isertd_area) {
				// code...
				$result['msg'] = 'Update Area Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Area Already Exsist';
			$result['status'] = false;
		}

		to_json($result);
	}

	public function delete_area(){
		
		$result['msg'] = 'Cannot Delete Area ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_SAMPLE_AREA'] = $post['ID_SAMPLE_AREA'];
		$data['NAME_AREA'] = $post['NAME_AREA'];
		$data['DELETE_BY'] = $post['user'];

		$isertd_area = $this->m_samplearea->sample_delete($data);
		if ($isertd_area) {
			// code...
			$result['msg'] = 'Delete Area Success ...';
			$result['status'] = true;
		}

		to_json($result);
	}

	public function area_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

      	$data	= $this->m_samplearea->get_data($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_samplearea->recFil($key),
  			'recordsTotal'		=>	$this->m_samplearea->recTot($key)
  		);

  		echo json_encode($return);
    }

	public function area_list_scm(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

      	$data	= $this->m_samplearea->get_data_scm($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_samplearea->recFil_scm($key),
  			'recordsTotal'		=>	$this->m_samplearea->recTot_scm($key)
  		);

  		echo json_encode($return);
    }

}	

?>
