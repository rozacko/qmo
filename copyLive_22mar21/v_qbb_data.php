
<section class="content-header">
  <h1>
     Data Kualitas Batubara
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	<?php
	//var_dump($this->list_data);
	?>
	
       <div class="row">
		<div class="col-xs-12">
		  <div class="box">
			 <form id="formData" method="post" action="<?php echo site_url("qbb_data/index");?>">
                        <div class="form-group row" style="margin: 1em;">
							<div class="col-sm-3 clearfix">
								<label>From Date </label>
								<input type="date" class="form-control" name="start_load"   required>
							</div>
							<div class="col-sm-3 ">
								<label>To Date </label>
								<input type="date" class="form-control" name="end_load"  required>
							</div> 
							<div class=" col-sm-2">
								<br>
								<button type="submit" class="form-control btn-primary" name="load" id="btLoad">Load Data</button>
							</div>
							<div class="col-sm-2">
								<br>
								<button data-toggle="modal" data-target="#addSync"  type="button" class="form-control btn-success" name="sync" id="btSync">Sync Data</button>
							</div>
                        </div>

                    </form> 
			</div>
		</div> 
        <div class="col-xs-12">

          <div class="box">
			<div class="table-responsive">
			<label style="padding: 0.5em; ">Show data: <?= $this->start_data; ?> s/d <?= $this->end_data;?></label>
            <div class="box-body ">
			
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap"
	            cellspacing="0"
	            width="100%">
	            <thead >
					
	              <tr style="text-align: center;">
	                <th rowspan="2" width="5%">No.</th>
					<th rowspan="2">Create at</th>
					<th rowspan="2">Vendor</th>
					<th >Volume</th>
					<th>Total Moisture</th>
                    <th>Ash Content </th>
	                <th>Inherent Moisture</th>
					<th>Calorie</th>
					<th>Calorie</th>
					<th rowspan="2">Plant</th>
	              </tr>
				  <tr>
					<th>(Ton)</th>
					<th>TM (ar)</th>
                    <th>AC (ar)</th>
	                <th>IM (adb)</th>
					<th>CAL (ar)</th>
					<th>CAL (adb)</th>
				  </tr>
				 
	            </thead>

                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
					<td><?= $dt['CREATE_AT'];?></td>
					<td><?= $dt['NAMA_VENDOR'];?></td>
                    <td><?= number_format($dt['TONASE'],2,",",".");?></td>
					<td><?= number_format($dt['TOTAL_MOISTURE'],2,",",".");?></td>
					<td><?= number_format($dt['INHERENT_MOISTURE'],2,",",".");?></td> 
					<td><?= number_format($dt['ASH_CONTENT'],2,",",".");?></td> 
                    <td><?= number_format( $dt['CALORI_AR'],2,",",".");?></td> 
                    <td><?= number_format($dt['CALORI_ADB'],2,",",".");?></td> 
				    <td><?= $dt['NAMA_PLANT'];?></td>
                  </tr>
                <?php } ?>
                </tbody>
				<tfoot>
				 <tr>
						<td></td>
						<th class="as"></th>
						<th class="as"></th>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<th class="as"></th>
					</tr>
				</tfoot>
	          </table>
			  </div>
            </div>
            
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- Modal Sync -->
<div id="addSync" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form role="form" method="POST" action="<?php echo site_url("qbb_data/data_sap_qbb") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Sync Data From SAP:</b> </h4>
      </div>
      <div class="modal-body">
				<div class="form-group c-group after-add-more" id="utama">
                   <div class="col-sm-6 clearfix">
					<label>From Date </label>
					<input type="date" class="form-control" name="start_sync"   required>
				  </div>
				  <div class="col-sm-6 ">
					<label>To Date </label>
					<input type="date" class="form-control" name="end_sync"  required>
				  </div> 
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Sync</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<!--script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script -->
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<!-- data table print -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
<style>
  div.dt-buttons {
   float: right;
   margin-left:10px;
}
</style>
<script>
$(document).ready(function(){

	/** DataTables Init **/
     var table = $("#dt_tables").DataTable(
		{
            dom: 'Blfrtip',
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            buttons: [
			{
                extend: 'print',
                autoPrint: false
            }, 'excel', 'pdf', 'csv', 'copy'
			],
			initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                var select = $('<select><option value=""></option></select>').appendTo( 
					
					$(this.footer()).empty()).on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
		}
		
	); 
	  
});
</script>
