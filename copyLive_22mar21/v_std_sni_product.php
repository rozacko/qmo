   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Standard SNI Product
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">


          <div class="box" id="formArea" style="display: none;">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Standard SNI Product</h3>
              </div>

              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>

              <div class="box-body">
                  <div class="form-group clearfix">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>KODE MATERIAL</label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control hidden" name="ID_MATERIAL" id="ID_MATERIAL" placeholder="Id Material" required >
                      <input type="text" class="form-control" name="KODE_MATERIAL" id="KODE_MATERIAL" placeholder="Kode Material" required >
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>NAME MATERIAL </label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control" name="NAME_MATERIAL" id="NAME_MATERIAL" placeholder="Name Material" required >
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>Component Non - H<sub>2</sub>O </label><span  style="color:red; float: right;">(*)</span>                    
                      <select id="COMP_RELATION" name="COMP_RELATION" class="form-control select2">
                        <?php  foreach($this->list_component as $component): ?>
                          <option value="<?php echo $component->ID_COMPONENT;?>"><?php echo '<b>'.$component->KD_COMPONENT.'</b> ( <i>'.$component->NM_COMPONENT.'</i> )';?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group clearfix">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>H<sub>2</sub>O Percentage  (%) </label><span  style="color:red; float: right;">(*)</span>
                      <div class="input-group">
                        <input type="number" id="H20_PERCENT" name="H20_PERCENT" min="0" placeholder="Input Value (%)" class="form-control">
                        <span class="input-group-addon">%</span>
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>Percentage Component Non - H<sub>2</sub>O (%) </label><span  style="color:red; float: right;">(*)</span>
                      <div class="input-group">
                        <input type="number" id="COMP_PERCENT" name="COMP_PERCENT" min="0" placeholder="Input Value (%)" class="form-control">
                        <span class="input-group-addon">%</span>
                      </div>
                    </div>
                  </div>
              </div>

              <div class="box-footer">
                <button type="button" id="AddArea" class="btn btn-primary"><i class="fa fa-save"></i>  &nbsp;Submit</button>
                <button type="button" id="UpdateArea" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-default btnCancel">Cancel</button>
              </div>
            </div>
          </div>
    
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Data Standard SNI Product</h3>
                
              </div>

              <div class="box-body">

                <div class="form-group clearfix">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>Type Product </label>
                      <div class="input-group">
                        <select id="ID_PRODUCT" name="ID_PRODUCT" class="form-control select2" data-placeholder="Select a Type Product" style="width: 100%;">
                          <option value="">Please wait...</option>
                        </select>
                      </div>
                    </div>
                  </div>

                <div class="box box-danger box-solid col-sm-4 col-md-4 col-lg-4">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Standard SNI Product Preview </h3>

                    <div class="input-group input-group-sm" style="width: 150px; float: right;">
                      <a id="btn-Save-STD" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-save"></i> &nbsp; Save</a>
                    </div>
                    <div class="input-group input-group-sm" style="width: 40px; float: right;">
                      <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                  <div class="box-body" style="">                  	
	                <div id="progress-saving" style="display:none;">
	                  	<img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
	                </div>
                    <div class="row col-sm-12">                      
                      <div id="example1" class="hot handsontable htColumnHeaders" style="width: 100%;" ></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- Select2 Plugin-->
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/select2/select2.full.min.js");?>"/></script>


<script>

$(document).ready(function(){


  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();


  $("#progress-saving").show();

  loadTypeProductList();

  var oTable;

  var example1 = document.getElementById('example1'), hot1, glbcomponent;

  reloadtabel();  

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
    loadtabelinput();
  });

  $(document).on('click',"#btn-Save-STD",function () {
    
    $("#progress-saving").show();

      	var handsonData = hot1.getData();

      $.ajax({
        url: "<?php echo site_url("third_material/save_sni_standard_component");?>",
        data: {"ID_TYPE_PRODUCT": $('#ID_PRODUCT').val(), "SNI_DATA": handsonData, "COMPONENT_DATA": glbcomponent, "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          loadTypeProductList();
        },
        error: function () {
          loadTypeProductList();
        }
      });
    
      $("#progress-saving").hide();
  });

  $(document).on('click',"#UpdateArea",function () {
    
    $("#progress-saving").show();

    if ($('#ID_MATERIAL').val() != '' && $('#KODE_MATERIAL').val() != '' && $('#NAME_MATERIAL').val() != '' && $('#H20_PERCENT').val() != '' && $('#COMP_RELATION').val() != '' && $('#COMP_PERCENT').val() != '') {
      $.ajax({
        url: "<?php echo site_url("third_material/update_third_material");?>",
        data: {"ID_MATERIAL": $('#ID_MATERIAL').val(), "KODE_MATERIAL": $('#KODE_MATERIAL').val(), "NAME_MATERIAL": $('#NAME_MATERIAL').val(), "H2O_PERCENTAGE": $('#H20_PERCENT').val(), "ID_COMPONENT": $('#COMP_RELATION').val(), "COMP_PERCENTAGE": $('#COMP_PERCENT').val(),  "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();
            reloadtabel();      
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });      
    } else {
      $("#a-notice-error").click();
      if ($('#ID_MATERIAL').val() == '') {
        $('#ID_MATERIAL').focus();
      } else if ($('#KODE_MATERIAL').val() == '') {
        $('#KODE_MATERIAL').focus();
      } else if ($('#NAME_MATERIAL').val() == '') {
        $('#NAME_MATERIAL').focus();
      } else if ($('#H20_PERCENT').val() == '') {
        $('#H20_PERCENT').focus();
      } else if ($('#COMP_RELATION').val() == '') {
        $('#COMP_RELATION').focus();
      } else if ($('#COMP_PERCENT').val() == '') {
        $('#COMP_PERCENT').focus();
      }
    }
    
      $("#progress-saving").hide();
  });

  $(document).on('click',".btnCancel",function () {
    
    $("#progress-saving").show();
    $('#formArea').hide();
    $('#viewArea').show();

    $('#ID_MATERIAL').val('');
    $('#KODE_MATERIAL').val('');
    $('#NAME_MATERIAL').val('');
    $('#H20_PERCENT').val('');
    $('#COMP_RELATION').val('');
    $('#COMP_PERCENT').val('');
    reloadtabel();
    
      $("#progress-saving").hide();      
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  	$( "#ID_PRODUCT" ).change(function() {
    	loadtabelinput();

	});

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    
    $("#progress-saving").show();
    $('#formArea').show();
    $('#viewArea').hide();  

    $('#UpdateArea').show();
    $('#AddArea').hide(); 

    var data = oTable.row($(this).parents('tr')).data();

    $('#ID_MATERIAL').val(data['ID_MATERIAL']);
    $('#KODE_MATERIAL').val(data['KODE_MATERIAL']);
    $('#NAME_MATERIAL').val(data['NAME_MATERIAL']);
    $('#H20_PERCENT').val(data['H2O_PERCENTAGE']);
    $('#COMP_RELATION').val(data['ID_COMPONENT']);
    $('#COMP_PERCENT').val(data['COMP_PERCENTAGE']);
    
      $("#progress-saving").hide();
  });

  /** btDelete Click **/
  $(document).on('click',".btDelete",function () {
    var data = oTable.row($(this).parents('tr')).data();
    $.confirm({
        title: "Remove Component",
        text: "This sample area will be removed. Are you sure?",
        confirmButton: "Remove",
        confirmButtonClass: "btn-danger",
        cancelButton: "Cancel",
        confirm: function() {
          $.ajax({
            url: "<?php echo site_url("third_material/delete_third_material");?>",
            data: {"ID_MATERIAL": data['ID_MATERIAL'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
              if (res['status'] == true) {
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
              } else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              reloadtabel();            
            },
            error: function () {
              $("#a-notice-error").click();
            }
          });
        },
        cancel: function() {
        }
    });
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    $('#btn-Save-STD').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("third_material/third_material_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "ID_MATERIAL", "width": 50},
        {"data": "KODE_MATERIAL"},
        {"data": "NAME_MATERIAL"},
        {"data": "ID_MATERIAL", "width": 100,
          "mRender": function(row, data, index){
            return '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';

          }
        },
      ]
    });
    

    $('#ReloadData').show();
    $('#btn-Save-STD').show();
  }

  function loadTypeProductList() {
    var typeproduct = $('#ID_PRODUCT');
    $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product/all");?>', function (result) {
      var values = result;
      typeproduct.find('option').remove();
      if (values != undefined && values.length > 0) {
        typeproduct.css("display","");
        $(values).each(function(index, element) {
          typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
        });
      }else{
        typeproduct.find('option').remove();
        typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
      }

      loadtabelinput();
    });
  }


  function loadtabelinput() {

    if (hot1) {

      hot1.destroy();

    }  

    $.ajax({
		url: "<?php echo site_url("third_material/get_componen_sni_standard_product/");?>",
		data: {"ID_TYPE_PRODUCT": $("#ID_PRODUCT").val(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
		dataType: 'json',
		type: 'POST',
		success: function (result) { 
			if (result && result['Status'] && result['Data']) {

				hot1 = new Handsontable(example1, {
					data: result['Data']['Body'],
					// height: 450,
					autoColumnSize : true,
					className: "htCenter",
					fixedColumnsLeft: 1,
					filters: true,
					dropdownMenu: ['alignment','filter_by_condition', 'filter_by_value', 'filter_action_bar'],
					columnSorting: true,
					colWidths: result['Data']['ColWidhts'],
					manualColumnFreeze: true,
					manualColumnResize: true,
					colHeaders: result['Data']['Header'],
					columns: getRowOptions()
				});

				glbcomponent = result['Data']['ComponentID'];

				$('#ReloadData').show();

      			$("#progress-saving").hide();

			} else {        

				$('#ReloadData').show();
				
      			$("#progress-saving").hide();
			}   
		},
		error: function () {

			$('#ReloadData').show();	
      		$("#progress-saving").hide();

		}
	});

  }

  function getContentData() {
      return [["TRASS", , , , ],["LIMESTONE", , , , ],["GYPSUM", , , , ]];
  }

  function getRowOptions() {
    var initial_coloptions = [
      {
        type: 'text',
        readOnly: true
      },
      {
        type: "text"
      },
      {
        type: "text"
      }
    ];

    return initial_coloptions;
  }

  function getColHeader() {
    var columnlist = ['Third Material', 'H20', 'LOi', 'Insol', 'SO3'];
    return columnlist;
  }

  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
