<?php

class Sampling extends QMUser {

    public $status_proficiency = "";
    public $proficiency = array();
    public $penerima = array();

    public function __construct(){
		parent::__construct();
        $this->load->helper("string");
        $this->load->model("m_sampling");
        $this->load->library('email');
    }

    public function index(){
        $this->template->adminlte("v_sampling");
    }

    public function input(){
        $this->libExternal('datepicker');
        $this->libExternal('select2');
        $id = $this->input->get("id");

        $data = $this->m_sampling->detail($id);
        $this->penerima = $this->m_sampling->list_penerima($id);
        $this->proficiency = $data;
        $this->list_pic = $this->m_sampling->data_pic();

        $this->status_proficiency = "FILE_SAMPLE";
        if($data->FILE_SAMPLE == ""){
            $this->status_proficiency = "FILE_SAMPLE";
        } else if($data->FILE_HOMOGEN_STABIL_1 == "" && $data->FILE_HOMOGEN_STABIL_2 == ""){
            $this->status_proficiency = "FILE_HOMOGEN";
        } else if($data->FILE_PENGEMASAN == ""){
            $this->status_proficiency = "FILE_PENGEMASAN";
        } else if($data->FILE_PETUNJUK_TEKNIS == ""){
            $this->status_proficiency = "FILE_PETUNJUK_TEKNIS";
        } else {
            $this->status_proficiency = "PENGIRIMAN";
        }


        $this->template->adminlte("v_sampling_input");
    }
    
    public function get_list(){
        $user_role = $this->session->userdata("ROLE");
        $role = $this->recursive_array_search("ADMINISTRATOR", $user_role);

        $akses = "none";
        if($role == true){
            $akses = "all";
        } else {
            $role = $this->recursive_array_search("Opco (Sample Provider)", $user_role);
            if($role == true){
                $akses = "sample_provider";
            }
        }

        $list = $this->m_sampling->get_list($akses, $this->session->userdata("USER")->ID_USER);
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["TITLE_PP"] = $column->TITLE_PP;
			$row["YEAR_PP"] = $column->YEAR_PP;
			$row["ID_PROFICIENCY"] = $column->ID_PROFICIENCY;
			$row["ID_KOMODITI"] = $column->ID_KOMODITI;
			$row["NAMA_SAMPLE"] = $column->NAMA_SAMPLE;
			$row["ID_LAB"] = $column->ID_LAB;
			$row["NAMA_LAB"] = $column->NAMA_LAB;
			$row["ID_PIC"] = $column->ID_PIC;
			$row["FULLNAME"] = $column->FULLNAME;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_sampling->count_all(),
            "recordsFiltered" => $this->m_sampling->count_filtered(),
            "data" => $data,
        );

		to_json($output);
    }

    public function recursive_array_search($category,$allProjects) {
        foreach ($allProjects as $projectName => $categories) {
            $categoryIndex = array_search($category, $categories);
            if ($categoryIndex !== false) {
                return true;
            }
        }
        return false;
    }

    public function action_input($id){
        $data = array();

        $detail = $this->m_sampling->detail($id);
        $config['upload_path']= './assets/uploads/proficiency/';
        $config['allowed_types'] = 'jpg|jpeg|png|xls|xlsx|csv|pdf';
        $config['max_size'] = 10240;

        if(isset($_FILES['FILE_SAMPLE'])){
            $file_sample_upload = array();
            for($i=0;$i<count($_FILES['FILE_SAMPLE']['name']);$i++){
                $_FILES['file']['name'] = $_FILES['FILE_SAMPLE']['name'][$i];
                $_FILES['file']['type'] = $_FILES['FILE_SAMPLE']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['FILE_SAMPLE']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['FILE_SAMPLE']['error'][$i];
                $_FILES['file']['size'] = $_FILES['FILE_SAMPLE']['size'][$i];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')){
                    $FILE_SAMPLE = $this->upload->data();
                    array_push($file_sample_upload, $FILE_SAMPLE["file_name"]);
                }
            }

            if($detail->FILE_SAMPLE == ""){
                $data["FILE_SAMPLE"] = implode(";", $file_sample_upload);
            } else {
                $upload = implode(";", $file_sample_upload);
                $data["FILE_SAMPLE"] = $detail->FILE_SAMPLE.";".$upload; 
            }
           
            $data["FILE_SAMPLE_AT"] = date("Y-m-d H:i:s");
            
        }

        if(isset($_FILES['FILE_HOMOGEN_STABIL_1'])){
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('FILE_HOMOGEN_STABIL_1')){
                $FILE_HOMOGEN_STABIL_1 = $this->upload->data();
                $data["FILE_HOMOGEN_STABIL_1"] = $FILE_HOMOGEN_STABIL_1["file_name"];
                $data["FILE_HOMOGEN_STABIL_1_AT"] = date("Y-m-d H:i:s");
            }
        }

        if(isset($_FILES['FILE_HOMOGEN_STABIL_2'])){
            $file_homogen = array();
            for($i=0;$i<count($_FILES['FILE_HOMOGEN_STABIL_2']['name']);$i++){
                $_FILES['file']['name'] = $_FILES['FILE_HOMOGEN_STABIL_2']['name'][$i];
                $_FILES['file']['type'] = $_FILES['FILE_HOMOGEN_STABIL_2']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['FILE_HOMOGEN_STABIL_2']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['FILE_HOMOGEN_STABIL_2']['error'][$i];
                $_FILES['file']['size'] = $_FILES['FILE_HOMOGEN_STABIL_2']['size'][$i];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')){
                    $FILE_HOMOGEN_STABIL_2 = $this->upload->data();
                    array_push($file_homogen, $FILE_HOMOGEN_STABIL_2["file_name"]);    
                }
            }

            if($detail->FILE_HOMOGEN_STABIL_2 == ""){
                $data["FILE_HOMOGEN_STABIL_2"] = implode(";", $file_homogen);
            } else {
                $upload = implode(";", $file_homogen);
                $data["FILE_HOMOGEN_STABIL_2"] = $detail->FILE_HOMOGEN_STABIL_2.";".$upload;
            }
            
            $data["FILE_HOMOGEN_STABIL_2_AT"] = date("Y-m-d H:i:s");
        }

        if(isset($_FILES['FILE_PENGEMASAN'])){
            $file_pengemasan_upload = array();
            for($i=0;$i<count($_FILES['FILE_PENGEMASAN']['name']);$i++){
                $_FILES['file']['name'] = $_FILES['FILE_PENGEMASAN']['name'][$i];
                $_FILES['file']['type'] = $_FILES['FILE_PENGEMASAN']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['FILE_PENGEMASAN']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['FILE_PENGEMASAN']['error'][$i];
                $_FILES['file']['size'] = $_FILES['FILE_PENGEMASAN']['size'][$i];

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')){
                    $FILE_PENGEMASAN = $this->upload->data();
                    array_push($file_pengemasan_upload, $FILE_PENGEMASAN["file_name"]);
                }
            }
            $data["FILE_PENGEMASAN"] = implode(";", $file_pengemasan_upload);
            $data["FILE_PENGEMASAN_AT"] = date("Y-m-d H:i:s");
        }

        if(isset($_FILES['FILE_PETUNJUK_TEKNIS'])){
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('FILE_PETUNJUK_TEKNIS')){
                $FILE_PETUNJUK_TEKNIS = $this->upload->data();
                $data["FILE_PETUNJUK_TEKNIS"] = $FILE_PETUNJUK_TEKNIS["file_name"];
                $data["FILE_PETUNJUK_TEKNIS_AT"] = date("Y-m-d H:i:s");
            }
        }
        
        $this->m_sampling->update($data, $id);
        if($this->m_sampling->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
    }

    public function tambah_penerima($id){
        $data["ID_PROFICIENCY"] = $id;
        $data["ID_USER_PENERIMA"] = $this->input->post("ID_USER_PENERIMA");
        $data["NAMA_PENERIMA"] = $this->input->post("NAMA_PENERIMA");
        $data["EMAIL_PENERIMA"] = $this->input->post("EMAIL_PENERIMA");
        $data["ALAMAT_PENGIRIMAN"] = $this->input->post("ALAMAT_PENERIMA");
        $data["TGL_PENGIRIMAN"] = $this->input->post("TGL_PENGIRIMAN");
        $data["JML_SAMPLE"] = $this->input->post("JML_SAMPLE");
        $data["NAMA_PENGIRIM"] = $this->input->post("NAMA_PENGIRIM");
        $data["ALAMAT_PENGIRIM"] = $this->input->post("ALAMAT_PENGIRIM");
        $data["CC_PENERIMA"] = $this->input->post("CC_PENERIMA");
        

        if($this->input->post("ID_PENERIMA") == ""){
            $this->m_sampling->add_penerima($data, $id);
            $priode = $this->m_sampling->detail($id);

            $title = $priode->TITLE_PP." ".$priode->GROUP_PP." ".$priode->YEAR_PP;
            $this->send_email($title, $data["EMAIL_PENERIMA"], $data["NAMA_PENERIMA"], $data["JML_SAMPLE"], $data["TGL_PENGIRIMAN"]);
        } else {
            unset($data["ID_PROFICIENCY"]);
            $this->m_sampling->edit_penerima($data, $this->input->post("ID_PENERIMA"));
        }
        
        if($this->m_sampling->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
    }

    public function detail_penerima($ID_PENERIMA){
        $data = $this->m_sampling->get_detail_penerima($ID_PENERIMA);
        to_json($data);
    }
    

    public function delete_penerima($ID_PENERIMA){
		$this->m_sampling->delete_penerima($ID_PENERIMA);
		if($this->m_sampling->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
	}

    public function send_email($title, $to, $name, $jumlah, $tanggal){
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        if($uri['1'] == 'DEV'){
            $to = "muh.rudi.hariyanto@gmail.com";
        }
        $from = "qmo@sig.id";
        $subject = "Pengiriman Sample ".$title;
        $message = "Kepada Yth.
        ".$name."
        
        Dengan hormat,
        Telah dikirim sample ".$title." ke alamat saudara sejumlah ".$jumlah." kemasan pada tanggal ".$tanggal." melalui jasa ekspedisi.
        Harap melakukan konfirmasi aktifitas di qmo.semenindonesia.com sebagai berikut:
            1. Konfirmasi menerima sample
            2. Update schedule aktivitas pengujian
            3. Melaporkan hasil uji melalui form yang telah disediakan
        Demikian terima kasih
        
        Regards,
        Koordinator Pelaksanaan ".$title;
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}
}