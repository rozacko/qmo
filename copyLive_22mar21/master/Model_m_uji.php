<?php

class Model_m_uji extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM 
                ELAB_M_UJI A
                -- join ELAB_M_COMPONENT R ON A.ID_COMPONENT = R.ID_COMPONENT 
                LEFT JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI 
                WHERE A.ID_PRODUCT IS NULL and A.IS_DELETE = '0'
                ORDER BY ID_UJI ASC
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM 
                ELAB_M_UJI A
                -- join ELAB_M_COMPONENT R ON A.ID_COMPONENT = R.ID_COMPONENT 
                LEFT JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI  
                WHERE A.IS_DELETE = '0' and a.id_product is null
                ORDER BY ID_UJI ASC
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_UJI (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_UJI SET ".implode(",", $data)." WHERE ID_UJI={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE ELAB_M_UJI SET IS_DELETE = 1  WHERE ID_UJI={$id}");
	}
    
    
	public function getKategori() {
		return $this->db->query("SELECT * FROM ELAB_M_KATEGORI")->result_array();
	}
	public function getCompany() {
		return $this->db->query("SELECT * FROM M_COMPANY")->result_array();
	}
	public function getComponent() {
		return $this->db->query("SELECT * FROM ELAB_M_COMPONENT")->result_array();
	}

}
?>