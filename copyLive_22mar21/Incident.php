<?php

class Incident extends QMUser {

	public $data_user = array();
	public $list_data = array();
	public $list_company = array();
	public $list_type = array();
	public $data_incident;
	public $incident;
	public $solution;
	public $receiver;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_incident");
		$this->load->model("m_plant");
		$this->load->model("m_company");
		$this->load->model("c_ncqr_product");
	}

	public function index($current = NULL, $id_company = NULL){
		$this->libExternal('select2');
		// if($current){
		// 	$curr = $current;
		// }else{
		// 	$curr = 0;
		// }

		$this->CURRENT = $current;
		$this->ID_COMPANY = $id_company;
      	$this->receivAct = $this->getAccess('NCQR-TEKNISI');
		  $this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_incident = $this->m_incident->datalist();#echo $this->m_incident->get_sql();
		$this->template->adminlte("v_incident");
	}

	public function assigned(){
		$this->list_incident = $this->m_incident->assigned();
		$this->template->adminlte("v_incident_assigned");
	}


	public function add(){
		$this->template->adminlte("v_incident_add");
	}

	public function create(){
		$this->m_incident->insert($this->input->post());
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/add");
		}
		else{
			$this->notice->success("Incident Data Saved.");
			redirect("incident");
		}
	}

	public function edit($ID_INCIDENT){
		$this->load->model("m_technician");
		$this->list_technician = $this->m_technician->datalist();
		$this->data_incident = $this->m_incident->get_data_by_id($ID_INCIDENT);
		$this->template->adminlte("v_incident_edit");
	}

	public function update($ID_INCIDENT){
		$this->m_incident->update($this->input->post(),$ID_INCIDENT);
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/edit/".$ID_INCIDENT);
		}
		else{
			$this->notice->success("Incident Data Updated.");
			redirect("incident");
		}
	}

	public function assign($ID_INCIDENT=NULL){
		$data['ID_TEKNISI']  = $this->input->post("ID_TEKNISI");
		$data['ASSIGN_NOTE']  = $this->input->post("ASSIGN_NOTE");
		$this->m_incident->assign($data,$ID_INCIDENT);
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/edit/".$ID_INCIDENT);
		}
		else{
			$this->notice->success("Incident Assigned.");
			redirect("incident");
		}
	}

	public function export($ID_INCIDENT){	
		$param['ID_INCIDENT'] = $ID_INCIDENT;
		$receiver['M'] = ($this->m_incident->ncqr_incident($param))[0];
		$receiver['DATA'] = $this->m_incident->receiver_incident($param);
		$receiver['INCIDENT'] = $this->m_incident->get_data_by_id($param['ID_INCIDENT']);
		
		if ($receiver['M']['ID_COMPANY'] == '8') {
			$receiver['M']['TXT_COMPANY'] = 'SP';
		}else if ($receiver['M']['ID_COMPANY'] == '7') {
			$receiver['M']['TXT_COMPANY'] = 'SG';
		}else if ($receiver['M']['ID_COMPANY'] == '9') {
			$receiver['M']['TXT_COMPANY'] = 'ST';
		}else if ($receiver['M']['ID_COMPANY'] == '10') {
			$receiver['M']['TXT_COMPANY'] = 'SIBU';
		}

		$this->load->library('Pdf');
	 	$pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
	    $pdf->SetTitle('TINDAK PENCEGAHAN DAN KOREKSI -'.$receiver['M']['TXT_COMPANY']);
	    $pdf->SetTopMargin(20);
	    $pdf->setFooterMargin(20);
	    $pdf->SetAutoPageBreak(true);
	    $pdf->SetAuthor('baday');
	    $pdf->SetDisplayMode('real', 'default');

	    // set default header data
		// $pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	    // create some HTML content
		$html = $this->load->view('nostyle/xls_ncqr_solving_report',$receiver , true);
	    // $pdf->Write(5, 'Contoh Laporan PDF dengan CodeIgniter + tcpdf');
		$html2 = $this->load->view('nostyle/xls_ncqr_solving_report_2',$receiver , true);
		$html3 = "Tanggal cetak : ".date('d-m-Y');
	    // $pdf->Write(5, 'Contoh Laporan PDF dengan CodeIgniter + tcpdf');
	    $pdf->AddPage();
		$pdf->writeHTML($html, true, false, true, false, '');
		if (isset($receiver['DATA']->VERIFIKASI)) {
			
		    $pdf->AddPage();
			$pdf->writeHTML($html2, true, false, true, false, '');
		}
		$pdf->writeHTML($html3, true, false, true, false, '');
		
		// echo "$html";
	    $pdf->Output('TKP-'.date("d-m-Y").'-report.pdf', 'D');
		
	}

	public function solve($ID_INCIDENT, $current, $ID_COMPANY){
		// print_r($_POST);
		// exit;
		$this->CURRENT 			= $current;
		$this->ID_COMPANY 		= $ID_COMPANY;
		$param['ID_INCIDENT'] 	= $ID_INCIDENT;
		$incident = $this->m_incident->ncqr_incident($param);
		// echo $this->m_incident->get_sql();
		$this->receiver = $this->m_incident->receiver_incident($param); #echo $this->m_incident->get_sql();
		// echo '<pre>';
		// print_r($this->receiver);
		// echo '</pre>';

		$this->incident = $incident[0];

		
		// if(count($incident) > 0){
			$id_action = $this->receiver->ID_ACTION;
			// if (!$id_action) {
				
		      	$administrator = $this->getAccess('ADM');
		      	if($administrator){
			      	$this->access = true;
			      	$this->accessQc = true;
		      	}else{
			      	$this->access = $this->getAccess('NCQR-TEKNISI');
			      	$this->accessQc = $this->getAccess('QC-VERIF');
		      	}

				if($this->input->post("JUDUL_SOLUTION")){
					$this->solving($ID_INCIDENT);
				}
				else{
					$this->load->model("employee");
					$this->data_user = $this->employee->chekKaryawans($this->USER->USERNAME);
					$this->load->model("m_technician");
					$this->list_technician = $this->m_technician->datalist();
					$this->data_incident = $this->m_incident->get_data_by_id($ID_INCIDENT); #echo $this->m_incident->get_sql();

					if($this->data_incident[0]->ID_SOLUTION){
						$this->load->model("m_solution");
						$this->solution = $this->m_solution->get_data_by_id($this->data_incident[0]->ID_SOLUTION);
						#echo $this->m_incident->get_sql();
					}
					// echo "string";
					$this->template->adminlte("v_incident_solving2");
				}
			// }else{
			// 	redirect("incident");
			// }
		// }else{
		// 	$incidentCheck = $this->m_incident->ncqr_incidentCheck($param);
		// // echo $this->db->last_query();
		// 	if($incidentCheck > 0){
		// 		$this->template->adminlte("messages/v_incident_fixed");
		// 	}else{ 
		// 		$this->load->view("errors/cli/error_404");
		// 	}
		// }

	}

	public function solving($ID_INCIDENT, $ID_COMPANY, $current = NULL){
		// print_r($_POST);

		// die;
		// echo $this->getAccess('NCQR-TEKNISI');
		// print_r($_POST);
		// die;
		// print_r($_SESSION[USER]);

		// exit;
		if (empty($_POST['ID_ACTION'])) {
			// echo 'lakjs';die;
			$data['ID_INCIDENT'] = $ID_INCIDENT;
			$data['TYPE_TKP'] = 'QUALITY';
			$data['UNIT_KERJA'] = $_POST['UK_NAMA'];		
			$data['AREA'] = $_POST['AREA'];		
			$data['ACUAN'] = $_POST['REFERENCE'];		
			$data['KLAUSUL'] = $_POST['CLAUSE'];		
			$data['CREATE_BY'] = $_SESSION['USER']->FULLNAME;		
			$data['ANALISA'] = $_POST['ANALISA'];		
			$data['TINDAKAN'] = $_POST['TINDAKAN'];		
			$data['PENCEGAHAN'] = $_POST['PENCEGAHAN'];		
			// echo 'aaaa';die;

			$this->m_incident->insert_action($data);
		// 	print_r($_POST);
		// exit;
		}else {
			// echo 'kjydfh';die;
			$ID_ACTION = $_POST['ID_ACTION'];		
			$REJECT = $_POST['tombol_reject'];		
			// $data['TANGGAL_VERIFIKASI'] = $_POST['TANGGAL_VERIFIKASI'];
				if($this->getAccess('NCQR-TEKNISI')){
					$data['RESOLVED_BY'] = $_SESSION['USER']->FULLNAME;	
				}else{
					$data['VERIFIKASI'] = $_POST['VERIFIKASI'];		
					$data['VERIFIKATOR'] = $_SESSION['USER']->FULLNAME;
				}

			$this->m_incident->update_action($data, $ID_INCIDENT, $ID_ACTION);
			$data = [];
			if($REJECT == 'reject'){
				$data['ID_NCQR_STATUS'] = 1;		
				$data['CLOSED_STATUS'] = 'N';	
			}else{	
				if($this->getAccess('NCQR-TEKNISI')){
					$data['ID_NCQR_STATUS'] = 3;		
					$data['CLOSED_STATUS'] = 'N';
					// $data['RESOLVED_BY'] = $_SESSION['USER']->FULLNAME;	
				}else{
					$data['ID_NCQR_STATUS'] = 3;		
					$data['CLOSED_STATUS'] = 'Y';	
				}
			}
				
			$this->m_incident->update($data, $ID_INCIDENT);
		}
		// $this->load->model("m_solution");
		// $sol['JUDUL_SOLUTION'] = $this->input->post("JUDUL_SOLUTION");
		// $sol['DETAIL_SOLUTION'] = $this->input->post("DETAIL_SOLUTION");
		// $sol['MASALAH'] = $this->input->post("MASALAH");
		// //CHECK SOLTION ID
		// $ID_SOLUTION = $this->m_incident->get_data_by_id($ID_INCIDENT)->ID_SOLUTION;
		// if($ID_SOLUTION){
		// 		//UPDATE SOLUTION
		// 	$this->m_solution->update($sol,$ID_SOLUTION);
		// 	die($this->m_solution->get_sql());
		// }
		// else{
		// 	//new
	 	//      	$accessRa = $this->getAccess('NCQR-TEKNISI');
	 	//      	$accessQc = $this->getAccess('QC-VERIF');
		// 	$ID_SOLUTION = $this->m_solution->insert($sol);
		// 	$this->m_incident->solved($ID_SOLUTION,$ID_INCIDENT, $access);
		// }

		if($_POST['tombol_reject']){
			$pesan = 'Reject'; 
		}else{
			$pesan = 'Solved';
		}


		if(!$this->m_incident->error()){
			$this->notice->success("Incident ".$pesan);
			redirect("incident/index/".$current.'/'.$ID_COMPANY);
		}
		else{
			$this->notice->error("Something error while solving incident");
			redirect("incident/solve/".$ID_INCIDENT.'/'.$current.'/'.$ID_COMPANY);
		}

	}

	public function solved($ID_INCIDENT=NULL){
		if($ID_INCIDENT){
			$this->solved_incident($ID_INCIDENT);
		}
		else{
			$this->list_incident = $this->m_incident->solved_list();
			$this->template->adminlte("v_incident_solved");
		}
	}

	public function solved_incident($ID_INCIDENT){
		$this->data_incident = $this->m_incident->get_solved_by_id($ID_INCIDENT);
		$this->template->adminlte("v_incident_solved_detail");
	}

	public function report(){
		$this->libExternal('datepicker'); 
		$this->load->model("m_company");
		$this->list_company = $this->m_company->datalist();
		$this->list_type = $this->m_incident->type();
		#$this->list_incident = $this->i->datalist();
		$this->template->adminlte("v_incident_report");
	}

	public function get_report(){
		echo json_encode($this->m_incident->report($this->input->post())); # echo $this->m_incident->get_sql();
	}

	public function export_report(){
		$this->data = json_encode($this->m_incident->report($this->input->post()));
		$this->template->nostyle("xls_ncqr_incident_report");
	}

	public function dashboard($is_notifikasi=null){
		$this->libExternal('datepicker');
		
		$this->list_company = $this->m_company->list_company_auth();
		if($is_notifikasi == 'notifikasi'){
			$this->template->adminlte("v_incident_dashboard_notif");
		}
		elseif($is_notifikasi == 'notifikasi_company'){
			$this->template->adminlte("v_incident_notif_company");
		}
		elseif($is_notifikasi == 'score'){
			$this->template->adminlte("v_score_ncqr");
		}
		else{
			$this->template->adminlte("v_incident_dashboard");
		}
	}

	public function get_dashboard(){
		echo json_encode($this->m_incident->get_dashboard($this->input->post()));# echo $this->m_incident->get_sql();
	}

	public function get_dashboard_notifikasi(){
		echo json_encode($this->m_incident->get_dashboard_notifikasi($this->input->post()));
	}

	public function get_notifikasi_company(){
		echo json_encode($this->m_incident->get_notifikasi_company($this->input->post()));
	}

	public function get_score_company(){
		echo json_encode($this->m_incident->avg_score_ncqr($this->input->post()));# echo $this->m_incident->get_sql();
	}

	public function grafik_ncqr(){
		$this->list_company = $this->m_company->list_company_auth();
		$this->template->adminlte("v_grafik_ncqr");
	}

	public function notif_sent($ID_INCIDENT){
		$this->list_notif = $this->m_incident->list_notif($ID_INCIDENT); # echo $this->m_incident->get_sql();
		$this->template->nostyle("v_notif_ncqr");
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		// $area= $this->c_ncqr_product->ncqr_area($ID_COMPANY,$ID_PLANT);# echo $this->m_area->get_sql();
		// echo json_encode($area);

		$ID_COMPANY   = $this->input->post('id_company');
        $ID_PLANT   = $this->input->post('plant');
		$area= $this->c_ncqr_product->ncqr_area($ID_COMPANY,$ID_PLANT);# echo $this->m_area->get_sql();
		echo json_encode($area);
	}

	public function get_list(){

		// print_r($this->input->post());
		// exit;
      	$this->receivAct = $this->getAccess('NCQR-TEKNISI');
		$list = array();
		if(is_array($this->input->post('j_incident'))){
			$data_status = $this->input->post('j_incident');

			$status = array();
			$closed = array();
			foreach ($data_status as $i => $v) {
				if(is_numeric($v)){
					if($this->receivAct){
						if($v == 1){
							$status[] = 1;
							$status[] = 2;
						}else{
							$status[] = $v;
						}
					}else{
						$status[] = $v;
					}
				}else{
					$closed[] = $v;
				}
			}

			// echo join($status, ',');
			// echo '<br>'.join($closed, ',');

			$where['status'] = join($status, ',');
			$where['closed'] = join($closed, ',');
			$where['id_company'] = $this->input->post('id_company');

			$list = $this->m_incident->get_list($where);
		}
		// echo $this->db->last_query();
		
		// exit;
		$data = [];
		$no   = $this->input->post('start');
		if ($list) {
			
			foreach ($list as $column) {
				$no++;
				$row = array();
				$row[] = $column->ID_INCIDENT;
				$row[] = $no;
				$row[] = $column->NM_PLANT;
				$row[] = $column->NM_AREA;
				$row[] = $column->SUBJECT;
				$row[] = $column->TANGGAL;
				$row[] = $column->TYPE;
				if($column->CLOSED_STATUS == 'Y'){
					$row[] = 'Closed';
				}else{
					if($column->ID_NCQR_STATUS == 1){
						$row[] = 'In Progress';
					}elseif ($column->ID_NCQR_STATUS == 2) {
						if($this->receivAct){
							$row[] = 'In Progress';
						}else{
							$row[] = 'Normal By System';
						}
					}else{
						$row[] = 'Resolved';
					}
				}
				$ID_ACTION =$this->m_incident->receiver_incident(['ID_INCIDENT'=>$column->ID_INCIDENT]);
				$row[] = (isset($ID_ACTION))?'Y':'N';
				$data[] = $row;

			}
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_incident->count_all($where),
            "recordsFiltered" => $this->m_incident->count_filtered($where),
            "data" => $data,
        );

		to_json($output);
	}



	//FASE 2-----------------------------------

	public function get_list2(){

      	$this->receivAct = $this->getAccess('NCQR-TEKNISI');
		$list = array();
		// echo '<pre>';
		// print_r($this->input->post('j_incident'));
		if(is_array($this->input->post('j_incident'))){
			$data_status = $this->input->post('j_incident');

			$status = array();
			$closed = array();

			foreach ($data_status as $i => $v) {
				if(is_numeric($v)){
					if($this->receivAct){
						if($v == 1){
							$status[] = 1;
							$status[] = 2;
						}else{ 
							$status[] = $v;
						}
					}else{ 
						$status[] = $v;
					}
				}else{
					$status[] = '3';
					$closed[] = $v;
				}
			}

			$where['status'] = join($status, ',');
			$where['closed'] = join($closed, ',');
			$where['id_company'] = $this->input->post('id_company');
			$list = $this->m_incident->get_list2($where);
			// echo $this->input->post('id_company');
			// echo $this->db->last_query();
		}
		// echo $this->db->last_query();


		$data = [];
		$no   = $this->input->post('start'); 
		if ($list) {
			foreach ($list as $column) {
				$no++;
				$row = array();
				$row[] = $column->ID_INCIDENT;
				$row[] = $no;
				$row[] = $column->NM_PLANT;
				$row[] = $column->NM_AREA;
				$row[] = $column->SUBJECT;
				$row[] = $column->TANGGAL;
				$row[] = $column->TYPE;
				
				$cek_list = $this->m_incident->get_inci($column->ID_INCIDENT);
				// echo '<br>'.$this->m_incident->get_sql();
// echo $this->db->last_query();
				if($column->CLOSED_STATUS == 'Y' AND $cek_list != ''){
					$row[] = 'Closed';
				}else{
					if($column->ID_NCQR_STATUS == 1){
						$row[] = 'In Progress';
					}elseif ($column->ID_NCQR_STATUS == 2) {
						if($this->receivAct){
							$row[] = 'In Progress';
						}else{
							$row[] = 'Normal By System';
						}
					}elseif ($column->ID_NCQR_STATUS == 3) {
						$row[] = 'Resolved';
					}
				}
				// $ID_ACTION =$this->m_incident->receiver_incident(['ID_INCIDENT'=>$column->ID_INCIDENT]);
				$row[] = $column->CLOSED_STATUS;
				$data[] = $row;

			}
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_incident->count_all2($where),
            "recordsFiltered" => $this->m_incident->count_filtered2($where),
            "data" => $data,
        );

		to_json($output);
	}

	public function getTest(){
		echo "string";
	}

	public function getIncident($param = array()){
        $this->load->library('libmail');
 		
 		$post = $this->input->post();

        // $post = array(
        //     'ID_AREA' => 88,
        //     'ID_GROUPAREA' => 4,
        //     'ID_PRODUCT' => 00
        // ); 
 
        $param['ID_AREA'] = $post['ID_AREA'];
        if($post['ID_GROUPAREA'] == 1){
            $param['ID_COMPONENT'] = '15,13,7';
        }else{
            $param['ID_COMPONENT'] = '12';
            unset($param['ID_PRODUCT']);
        }

        $param['ID_NCQR_STATUS'] = 1;
        
        $incident = $this->m_incident->ncqr_incident($param);

        foreach ($incident as $i => $iv) {
            $detail = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']);

            // $incident[$i]['DETAIL'] = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']); # echo $this->m_incident->get_sql();
            $subject = $iv['NM_COMPANY'].' ('.$iv['NM_PLANT'].' - '.$iv['NM_AREA'].')';
        
            switch ($iv['ID_INCIDENT_TYPE']) {
                case '1':
                    $listJabatan = '1';
                    break;
                case '2':
                    $listJabatan = '1,2';
                    break;
                case '3':
                    $listJabatan = '1,2,3';
                    break;
                default:
                    $listJabatan = '1,2,3,21';
            }
            $paramMail['b.ID_AREA'] = $param['ID_AREA'];
            $paramMail['ID_JABATAN'] = $listJabatan;
            $listMail = $this->m_incident->mail_incident($paramMail);
            // echo $this->db->last_query();
            
            $to = array();
            $cc = array();
            if($this->serverHost() == 'DEV'){
                $to[] = 'm.r.sucahyo2@gmail.com';
                // $cc = array('95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');
            }else{
                foreach ($listMail as $j => $jv) {
                    if($iv['TEMBUSAN'] == 1){
                        $cc[] = $jv['EMAIL'];
                    }else{
                        $to[] = $jv['EMAIL'];
                    }
                }
            }

            $testTO = array();
            $testCC = array();
            foreach ($listMail as $j => $jv) {
                if($jv['TEMBUSAN'] == 1){
                    $testCC[] = $jv['EMAIL'];
                }else{
                    $testTO[] = $jv['EMAIL'];
                }
            }

            $output = array(
                'URL'       => base_url('incident/solve/'.$iv['ID_INCIDENT']),
                'INCIDENT'  => $iv,
                'DETAIL'    => $detail,
                'MAIL'      => array('to'=>$to,'cc'=>$cc),
                'TO'        => $testTO,
                'CC'        => $testCC,
                'SUBJECT'   => $subject
            );

            $msg = $this->libmail->ncqr($output);

            echo $msg.'<hr>';
            // exit;
        
            $this->load->library('email');
            $this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');

            $this->email->to($to); 
            $this->email->cc($cc);
            
            $this->email->subject($subject);
            $this->email->message($msg);

            if($this->email->send()){
                $status = 'OK';
                foreach ($listMail as $j => $jv) {
                    $this->m_t_notifikasi->insert($jv['ID_OPCO'], $iv['ID_INCIDENT'], $jv['ID_JABATAN'], $iv['ID_INCIDENT_TYPE']);
                }

            }else{
                $status = 'FAIL';
            }

        }
		echo json_encode(array('result'=>$status));
    }


}

?>
