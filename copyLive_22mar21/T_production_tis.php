<?php defined('BASEPATH') OR exit('No direct script access allowed');

class T_production_tis Extends DB_QM {

	public function data_where_prod($where){
		$this->db->select("a.*, to_char(DATE_DATA,'DD-MM-YYYY') AS TANGGAL, b.KD_PRODUCT, x.NM_AREA, y.NM_PLANT");
		$this->db->from('T_PRODUCTION_TIS a');
		$this->db->join('M_PRODUCT b', 'a.ID_PRODUCT = b.ID_PRODUCT', 'left');
		$this->db->join('M_AREA x', 'x.ID_AREA = a.ID_AREA');
		$this->db->join('M_PLANT y', 'y.ID_PLANT = x.ID_PLANT');
		$this->db->where($where);
		$this->db->order_by('a.ID_PRODUCT', 'asc');
		$this->db->order_by('x.ID_PLANT', 'asc');
		$this->db->order_by('a.ID_AREA', 'asc');
		$this->db->order_by('a.DATE_DATA', 'asc');
		
        
		$data=  $this->db->get();
		return  $data->result();
	}
}