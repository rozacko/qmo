<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode_proficiency extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_periode_proficiency","periode");
		
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_gp = $this->periode->get_gp();
		$this->list_data = $this->periode->get_list();
		//print_r($this->list_data);
		//exit();
		
		$this->template->adminlte("v_periode_proficiency");
    }
	
	public function do_add(){
		$save_act = $this->periode->save();
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("periode_proficiency");  
		} else {
			$this->notice->error($this->periode->error());
			redirect("periode_proficiency");
		}
	}
	
	public function do_edit(){
		$edit_act = $this->periode->edit();
		if($edit_act){
			$this->notice->success("Data Edited.");
			redirect("periode_proficiency");
		} else {
			$this->notice->error($this->periode->error());
			redirect("periode_proficiency");
		} 
	}
	
	public function do_edit_status(){
		$edit_act = $this->periode->edit_status();
		if($edit_act){
			$this->notice->success("Status Edited.");
			redirect("periode_proficiency");
		} else {
			$this->notice->error($this->periode->error());
			redirect("periode_proficiency");
		} 
	}
	
	public function do_delete($id_pp){
		$del_act = $this->periode->deleted($id_pp);
		if($del_act){
			$this->notice->success("Data Deleted.");
			redirect("periode_proficiency");
		} else {
			$this->notice->error($this->periode->error());
			redirect("periode_proficiency");
		} 
	}
	
}

?>