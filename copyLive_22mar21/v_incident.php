   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        NCQR Incident
        <small></small> 
      </h1>

    </section>
    <input type="hidden" id="pageInfo" value="<?php echo $this->CURRENT;?>"> 
    <!-- Main content -->
    <section class="content">
     <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label>Status</label>
          <input type="checkbox" checked="checked" id="checkAllStatus" title="Select All" class="pull-right">
          <select id="status" name="status" multiple="multiple" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
            <option selected="selected" value="1">In Progres</option>
            <?php
            if(!$this->receivAct){
              echo '<option selected="selected" value="2">Normal by System</option>';
            }
            ?>
            <option selected="selected" value="3">Resolved</option>
            <option selected="selected" value="closed">Closed</option>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label>Company</label>
          <select id="id_company" name="id_company" class="form-control select2">
            <option value="" SELECTED >All Company</option> <!-- izza 2021.03.03 -->
            <?php  foreach($this->list_company as $company):  
              ?>
              <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="col-md-2">
        <div class="form-group">
          <label style="width: 100%">&nbsp;</label>
            <button class=" btn btn-sm btn-primary" id="btnFilter"> <i class="fa fa-search"></i></button>
        </div>
      </div>
      </div>
       <div class="row">
        <div class="col-xs-12">
          <div class="box">
			       <table>
               <tr>
                <td>
                  <div class="box-header">
                  </div>
                </td>
              </tr>
              <!-- /.box-header -->
              <tr>
                <td>
                  <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover table-responsive dt-responsive nowrap"
                            cellspacing="0"
                            width="100%">
                      <thead>
                        <tr>
                          <th width="5%">ID</th>
                          <th width="5%">No.</th>
                          <th width="5%">Plant</th>
                          <th width="5%">Area</th>
                          <th width="30%">Subject</th>
                          <th width="30%">Date</th>
                          <!-- <th width="30%">Solved</th> -->
                          <!-- <th width="30%">Solution</th> -->
                          <th width="30%">Type</th>
                          <th width="30%">Status</th>
                          <th width="20%"></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
	
	
<!-- msg confirm -->
<?php if($notice->error): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>

<?php endif; ?>

<?php if($notice->success): ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
<?php endif; ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>

<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>


$(document).ready(function(){

	$(".select2").select2(); 
  
  $("#checkAllStatus").click(function(){
    if($("#checkAllStatus").is(':checked') ){
        $("#status > option").prop("selected","selected");
        $("#status").trigger("change");
    }else{
        $("#status > option").removeAttr("selected");
         $("#status").trigger("change");
     }
  }); 

  $(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });	
  
	<?php if($notice->error): ?>
	$("#a-notice-error").click();
	<?php endif; ?>
	
	<?php if($notice->success): ?>
	$("#a-notice-success").click();
	<?php endif; ?>
	
  /** DataTables Init **/
  // alert($('#pageInfo').val());

  var table = $("#dt_tables").DataTable({
      language: {
        searchPlaceholder: ""
      },
      "paging": true,
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
              "<'row'<'col-sm-12'rt>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": [
      {
        extend: "pageLength",
        className: "btn-sm bt-separ"
      },
      {
        text: "<i class='fa fa-refresh'></i> Reload",
        className: "btn-sm",
          action: function(){
            dtReload(table);
          }
        }
      ],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url" : '<?php echo site_url("incident/get_list2");?>',
      "type": 'post',
      "data": function(data){ 
          // console.log(data);
          data.j_incident = $("#status").val();
          data.id_company = $("#id_company").val();
        }
      },
      "columnDefs": [{
        "targets": 0,
        "visible": false,
        "searchable": false
        },
        { responsivePriority: 1, targets: -1 },
        {
        "render" : function(data){
          console.log(data);
          var btn = '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button">View Incident Detail</button>';
          if (data[8]=='Y') {
             btn += '<button title="Export" class="btExport btn btn-primary btn-xs" type="button" >Export</button>';
          }else{
             btn += '<button title="Export" class="btExport btn btn-primary btn-xs" type="button" disabled>Export</button>';
          }
          return btn;
          // console.log(data[6]);
        },
        "targets": -1,
        "data": null
        }
      ],

      initComplete: function () {
        setTimeout( function () {
          // alert(infopage);
          if($('#pageInfo').val() != ''){
             infopage = parseInt($('#pageInfo').val());
          }else{
             infopage = parseInt(0);
          }
          table.page(infopage).draw(false);
        }, 10 );
      }
    })

$('#dt_tables').on( 'page.dt', function () {
  var info = table.page.info();
  $('#pageInfo').val( info.page);
} );

    /** DataTable Ajax Reload **/
    function dtReload(table,time) {
      var time = (isNaN(time)) ? 100:time;
      setTimeout(function(){ table.search('').draw(); }, time);
    }

    $("#btnFilter").click(function(){
      $('#dt_tables').DataTable().ajax.reload();
    });

    //DataTable search on enter
    $('#dt_tables_filter input').unbind();
    $('#dt_tables_filter input').bind('keyup', function(e) {
      var val = this.value;
      var len = val.length;

      if(len==0) table.search('').draw();
      if(e.keyCode == 13) {
        table.search(this.value).draw();
      }
    });

    /** btEdit Click **/
    $(document).on('click',".btEdit",function () {
     linkinfopage = $('#pageInfo').val();
     ID_COMPANY = $('#id_company').val();
      // alert(linkinfopage);
      var data = table.row($(this).parents('tr')).data();
      var url = '<?php echo site_url("incident/solve/");?>' + data[0] +'/'+ID_COMPANY+'/'+linkinfopage;
      window.open(url,'_blank');
    });    /** btEdit Click **/
    $(document).on('click',".btExport",function () {
      var data = table.row($(this).parents('tr')).data();
      var url = '<?php echo site_url("incident/export/");?>' + data[0];
      window.location.target = '_blank';
      window.location.href = url;
    });  	
});


</script>
