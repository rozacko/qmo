 <!-- Content Header (Page header) -->
 <section class="content-header">
      <h1>
        Aktivitas pelaksanaan (Uji Proficiency Nasional / Internasional)
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
		
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
			
		<?php if($notice->error): ?>

			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>

		<?php endif; ?>
		
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Periode Proficiency: </b> <?= $this->data_detil->TITLE_PP;?> [<?= $this->data_detil->GROUP_PP;?> - <?= $this->data_detil->YEAR_PP;?>] | <b>Commodity: </b> <?= $this->data_detil->NAMA_SAMPLE; ?></h3>
			  
			  
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body" >
			  
              <!-- /.box-body -->
			  <div class="col-sm-12 ">
				<label>Activitas pelaksanaan </label><button title="Add Activity" data-toggle="modal" data-target="#addModal" style="float: right; margin-bottom: 5px;" type="button" class="btn btn-primary">+ Tambah Aktivitas</button>
				<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap">
					<thead>
						<tr>
							<th>No.</th>
							<th>Activity</th>
							<th>Date</th>
							
							<th></th>
						</tr>
					</thead>
					<tbody style="font-weight: normal;">
					 <?php 
						$count = 1;
						foreach ($this->data_activity as $dt) { ?>
						<tr>
							<td><?= $count++; ?></td>
							<td><?= $dt->ACTIVITY_NAME; ?></td>
							<td><?= $dt->PLAN_DATE_START; ?></td>
							
							<td><center>
							 <?php if($this->PERM_WRITE): ?>
                      <button title="Edit" class="btEdit btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID_ACTIVITY;?>"><i class="fa fa-pencil-square-o"></i> </button>
                      <a href="<?php echo site_url("setup_pelaksanaan_up/do_hapus_activity/");?><?= $dt->ID_ACTIVITY; ?>/<?= $this->data_detil->ID_PROFICIENCY;?>" onClick="return doconfirm();"><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> </button></a>
                    <?php endif; ?>
							</center></td>
						</tr>
						
<!-- Modal edit activity -->
<div id="editModal<?= $dt->ID_ACTIVITY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("setup_pelaksanaan_up/do_edit_activity") ?>" >
    <!-- Modal content-->
	<?php 
	// format date start
		$date1 = $dt->PLAN_DATE_START;
		$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
		$out1 = $dateObj1->format('Y-m-d');
	
	?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Edit Activity:</b> <?= $dt->ACTIVITY_NAME; ?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="ID_PRO" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<input type="hidden" name="ID_ACTIVITY"  value="<?= $dt->ID_ACTIVITY;?>" />
				<div class="form-group">
                  <div class="col-sm-8 clearfix">
					<label>Activity </label>
					
					<select class="form-control select2" NAME="activity" placeholder="Select a activity ...">
						<option value="<?= $dt->ACTIVITY_NAME; ?>" ><?= $dt->ACTIVITY_NAME; ?></option>
						
					</select>          
				  </div>
				  <div class="col-sm-4 ">
					<label>Date </label>
					<input type="date" class="form-control" name="start_date"  value="<?= $out1; ?>">
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

					<?php } ?>
					</tbody>
				</table>
			  </div>

             
            </form>
			
			
			
          </div>

    </section>

	
<!-- Modal add activity -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	<form role="form" method="POST" action="<?php echo site_url("setup_pelaksanaan_up/create_activity") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Activity</h4>
      </div>
      <div class="modal-body">
			<input type="hidden" name="PROFICIENCY" id="PROFICIENCY" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<div class="form-group">
                  <div class="col-sm-8 clearfix">
					<label>Activity </label>
					
					<select class="form-control select2" name="activity" >
						<option value="" disabled="true">Select a activity ...</option>
						<option value="Menerima sample">Menerima sample</option>
						<option value="Pengujian sample">Pengujian sample</option>
						<option value="Pengiriman hasil uji">Pengiriman hasil uji</option>
					</select>          
				  </div>
				  <div class="col-sm-4 ">
					<label>Date </label>
					<input type="date" class="form-control" name="start_date"  >
				  </div>
				  
				  
                </div>
			
     
	 </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>



<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->

<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script type="text/javascript">
    $(document).ready(function() {
	 
		
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
		  
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>

<script>

$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>