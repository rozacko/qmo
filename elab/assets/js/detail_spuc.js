   
	function detail_spuc(id_transaksi, hidden) {
		$.post("kupp/detail_spuc", {"id_transaksi": id_transaksi, "hidden": hidden  }, function(data) {
			$("#detail_spuc").html(data); 
            $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Detaill SPUC</b>"); 
            $("#dlg_detail").modal("show"); 
		}).fail(function() {
			// Nope
		}).always(function() {
			// $(".selectpicker").selectpicker("refresh");
		});
	} 
    
	function detail_spuc_folder(id_transaksi, hidden) {
		$.post("../kupp/detail_spuc", {"id_transaksi": id_transaksi, "hidden": hidden }, function(data) {
			$("#detail_spuc").html(data); 
            $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Detaill SPUC</b>"); 
            $("#dlg_detail").modal("show"); 
		}).fail(function() {
			// Nope
		}).always(function() {
			// $(".selectpicker").selectpicker("refresh");
		});
	} 