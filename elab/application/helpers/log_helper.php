<?php 

if(!defined('BASEPATH')) exit('No direct script access allowed');

	function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "update"){
        $log_tipe  = 4;
    }
    elseif(strtolower($tipe) == "delete"){
        $log_tipe  = 5;
    }
    elseif(strtolower($tipe) == "download"){
        $log_tipe  = 6;
    }
 
    // paramter

    $param['LOG_TIME']				= date('d-m-y H:i:s');
    $param['ID_USER']     		 	= $CI->session->userdata("USER")->ID_USER;
    $param['LOG_ACTIVITY']      	= $log_tipe;
    $param['KETERANGAN']      		= $str;
 
    //load model log
    $CI->load->model('coq/M_log');
 
    //save to database
    $CI->M_log->save_log($param);
 
}

?>