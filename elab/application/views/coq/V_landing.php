<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                  
                    <div class="col-md-6">
                      <h2><i class="fa fa-list"></i> Master Biaya</h2>
                    </div>
                    <div class="col-md-6">
                       <div style="text-align:right;">
                          <button class="btn-sm btn-success" onclick="add()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                      </div>
                    </div>
                  
                    <div class="ibox-content">
                      <div class="row">
                    <table id="Tabel" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Type Product</center>  </th> 
                            <th> <center> Standart</center>  </th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center>Type Product</center> </td>
                            <td> <center> Standart</center> </td> 
                           
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


 <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Tambah Data</h4>
             </div>
             <div class="modal-body form">
                <form action="" method="POST" id="form">
                  <div class="form-group">
                    <input type="hidden" name="id_biaya">
                  </div>
                  <div class="form-group">
                    <label>Parameter Uji</label>
                    <select class="form-control selectpicker" data-live-search="true" id="par_uji"  name="par_uji" >
                                                             <option value="">--Pilih--</option>
                                                        <?php 
                                                        foreach ($tampilkan->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->ID_UJI ?>"><?php echo $key->NAMA_UJI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                  </div>

                  <div class="form-group">
                    <label>Standart Uji</label>
                    <select class="form-control selectpicker" data-live-search="true"   name="standart_uji" id="standart">
                                                              <option value="">--Pilih--</option>
                                                        <?php 
                                                        foreach ($standart->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->ID_STANDART ?>"><?php echo $key->NAMA_STANDART ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                  </div>
                  <div class="form-group">
                    <label>Kategori</label>
                    <select class="form-control selectpicker" data-live-search="true"   name="metode" id="metode">
                                                        <option value="">--Pilih--</option>
                                                        <?php 
                                                        foreach ($kategori->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->ID_KATEGORI ?>"><?php echo $key->NM_KATEGORI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                  </div>

                  <div class="form-group">
                    <label>Laboratorium</label>
                    <select class="form-control selectpicker" data-live-search="true"   name="lab" id="lab" >
                                                             <option value="">--Pilih--</option>
                                                        <?php 
                                                        foreach ($lab->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->ID_LAB ?>"><?php echo $key->NM_LAB ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                  </div>
                  <div class="form-group">
                    <label>Biaya</label>
                    <input type="number" name="biaya" class="form-control">
                  </div>
                  </form>
               
             </div>
             <div class="modal-footer">
              <button class="btn btn-primary" onclick="save()" id="submit">Simpan</button>
              
             </div>
            </div>

          </div>
        </div>

<script>
   


       var dataTable;
          $(document).ready(function(){
           
    TabelData = $('#tabel').DataTable({ 
        "processing": true,
        "serverSide": true,
        "lengthMenu": [10, 25, 50, 100],
        "sDom": 'Rfrtlip',
        'lengthChange': true,
        "searching": true,
        "ordering": false,
        "autoWidth": false,
        "ajax": {
            "url": "<?php echo site_url('coq/std_product/get_data')?>",
            "type": "POST"
        },
    
  });
     });

        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        var save_method;
          

          function add()
          {
            save_method ='add';
            $('#form')[0].reset();
            $('.modal-title').text('Tambah Data');
            $(".selectpicker").selectpicker("refresh");
            $('#myModal').modal('show');
          }
          function save()
          {
            var url;
             
            if(save_method == 'add'){
              url = '<?php echo site_url('master/Biaya/tambah') ;?>';
            }
            else{
              url = '<?php echo site_url('master/Biaya/update') ;?>';
            }
            $.ajax({
              url:url,
              type:'POST',
              data:$('form').serialize(),
              dataType:'JSON',
              success:function(data){
                informasi(BootstrapDialog.TYPE_SUCCESS, "Sukses menyimpan data.");
                tableData.ajax.reload();
                $('#dlg').modal('hide');
              },
              error:function(jqXHR, textStatus, errorThrown)
              {
               informasi(BootstrapDialog.TYPE_DANGER, "Harap Periksa data.");
              }
            });
          }
          function edit(id){
            save_method= 'update';
            $('#form')[0].reset();

            $.ajax({
              url : '<?php echo site_url('master/Biaya/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('[name="id_biaya"]').val(data.ID_BIAYA);
                $('[name="par_uji"]').val(data.ID_UJI);
                $('[name="standart_uji"]').val(data.STANDART);
                $('[name="metode"]').val(data.ID_KATEGORI);
                $('[name="lab"]').val(data.LAB);
                $('[name="biaya"]').val(data.BIAYA);
                

                $('#dlg').modal('show');
                $('.modal-title').text('UPDATE');
                $(".selectpicker").selectpicker("refresh");
                $('#submit').text('UPDATE');
              }
            });
          }
    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Standart</b>",
            "message": "Anda yakin ingin menghapus Standart \"" + kolom['standart'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_standart'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/standart/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
