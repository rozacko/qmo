<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }
    .dropzone {
        min-height: 200px;
    margin-top: 100px;
    border: 2px dashed #0087F7; 
    }
    
    .dz-details{
        height : 50px;
    }
    .dz-filename{
        height : 100%;
    }  
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins" id='dataRequest'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> List Request Sales</h2>
                </div>
                <div class="col-lg-6">
                    <button type="button" class="btn-sm btn-success btn_tambah pull-right" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Kode Request</center>  </th> 
                            <th> <center> Name</center>  </th> 
                            <th> <center> Standart</center>  </th> 
                            <th> <center> Type</center>  </th>
                            <th> <center> Brand</center>  </th> 
                            <th> <center> Packing</center>  </th> 
                            <th> <center> Product</center>  </th> 
                            <th> <center> Request</center>  </th> 
                            <th> <center> Plant</center>  </th> 
                            <th> <center> Status</center>  </th> 
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th> 
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Kode Request</center>  </td> 
                            <td> <center> Name</center>  </td> 
                            <td> <center> Standart</center>  </td> 
                            <td> <center> Type</center>  </td>
                            <td> <center> Brand</center>  </td> 
                            <td> <center> Packing</center>  </td> 
                            <td> <center> Product</center>  </td> 
                            <td> <center> Request</center>  </td> 
                            <td> <center> Plant</center>  </td> 
                            <td> <center> Status</center>  </td> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="ibox float-e-margins"  id='detailRequest' style='display:none'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Detail Spec and Quality</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-secondary"  onclick='tombol_back();'  ><span
                                    class="fa fa-arrow-left"></span>&nbsp;Back
                        </button>&nbsp;&nbsp;&nbsp; 
                    </div>
                </div>
            </div>


            <div class="ibox-content"> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Type Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_trequest'>Type Request</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Nama Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_nrequest'>Nama Request</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_standart'>Standart</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Product</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_produk'>Product</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Type Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_tipe'>Type Produk</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Brands</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_brand'>Brands</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Plant</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_plant'>Plant</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Date</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_date'>Date</p></div>
                        </div></div> 
                </div>  
           <div id='form_kapal_c'>
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Packing Produk</label><div class="col-lg-1" style=' width : 0px;'><p>:</p></div> 
                            <div class="col-lg-8"><p id='v_packing'>Packing Produk</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Buyer Ident (Trade/perush)</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_buyer'>Buyer Ident (Trade/perush)</p></div>
                        </div></div> 
                </div>  
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Export Destination</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_country'>Export Destination</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Total shipment & shipment size</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_total'>Total shipment & shipment size</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Vessel Name</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_kapal'>Vessel Name</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Bill Of Lading Number</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_bill_number'>Bill Of Lading Number</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Bill Of Lading Date</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_bill_date'>Bill Of Lading Date</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Port Of Loading</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_port_loading'>Port Of Loading</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Port Of Discharge</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_port_discharge'>Port Of Discharge</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Notify Party</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_notify'>Notify Party</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Description Sample</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_desc_sample'>Description Sample</p></div>
                        </div></div> 
                </div>  
                <!-- <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Date</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_date'>Date</p></div>
                        </div></div> 
                </div>   -->
                <!-- <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Shipment Period Delivery</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_date'>Shipment Period Delivery</p></div>
                        </div></div> 
                </div>  --> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Note</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><textarea name="v_note" id='v_note' style="width:100%" readonly></textarea></div>
                        </div></div> 
                </div>   
                
                <!-- <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">File</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_file'>File</p></div>
                        </div></div> 
                </div>  --> 
                
          </div>
                
                
    <div class="row" id='chatRoom'>
        <div class="col-lg-12">

                <div class="ibox chat-view">

                    <div class="ibox-title">
                        <small class="pull-right text-muted"> </small>
                         Chat room panel
                    </div>


                    <div class="ibox-content">

                        <div class="row"> 
                            <div class="col-md-12 ">
                                <div class="chat-discussion">
                                    <input type='hidden' id='id_request_chat'>
                                    <div id='chatting'> 
                                    <div> 
                                </div>

                            </div> 

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form"> 
                                    <div class="form-group"> 
                                        <textarea class="form-control message-input" name="message" id="message" placeholder="Enter message text"></textarea>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-12" style='    margin-top: 10px;'> 
                                    <div class="form-group">
                                            <button type="button" class="btn-lg btn-success pull-right" id="add_chat"><span class="fa fa-paper-plane"></span>&nbsp;Kirim</button>
                                    </div> 
                            </div>
                        </div>


                    </div>

                </div>
        </div>

    </div>

                 
            </div>
        </div>
    </div>
    
    
</div>
</div>


    
    <div class="ibox float-e-margins"  id='formTemplate' style='display:none'>
            <div class="ibox-title" > 
                <div class="col-lg-6">
                    <h2 id='title_tipe'><i class="fa fa-list"></i></h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-secondary" onclick='tombol_back();'  ><span
                                    class="fa fa-arrow-left"></span>&nbsp;Back
                        </button>&nbsp;&nbsp;&nbsp; 
                    </div>
                </div>  

            <div class="ibox-content"> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Nama</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            <input type='hidden' class='form-control input-xs' id='id_spec' >
                            <!-- <input type='hidden' class='form-control input-xs' id='id_plant' > -->
                            <input type='hidden' class='form-control input-xs' id='tipe_request_c' >
                            <!-- <input type='hidden' class='form-control input-xs' id='prints' > -->
                            <input type='text' class='form-control input-xs' id='nm_spec'   >
                             </div>
                        </div></div> 
                </div>  
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='standart_c' name="standart" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Product</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='produk_c' name="produk" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Type Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='tipe_c' name="tipe" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div>
                <div class="row" >
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Brand</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='brand_c' name="brand_c" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Brand --</option>
                            </select>
                            </div>
                        </div></div> 
                </div>
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Plant</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='plant_c' name="plant" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Plant --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <!-- <div class="row" >
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Print Page</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='prints' name="prints">
                                <option value='1' selected='selected'>1</option>
                                    <option value='2'>2</option>
                            </select>
                            </div>
                        </div></div> 
                </div>  -->
                
                <div class="row">
                    <div class='col-lg-6'>  <div class="form-group row"><label class="col-lg-6 col-form-label">Date Produce</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                              <input type='text' style='background-color:white' class='form-control input-xs' id="start" >
                            </div>
                        </div>
                     </div> 
                     <div class='col-lg-6'>  <div class="form-group row"><label class="col-lg-1 col-form-label">To</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                              <input type='text' style='background-color:white' class='form-control input-xs' id="end" >
                            </div>
                        </div>
                     </div> 
                </div>  
                <div class="row" style="display: none">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5" style='color:black'>
                            <select class='form-control selectpicker' data-live-search='true' id='request_c' name="request" disabled  >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>
                            </select>
                            </div>
                        </div></div> 
                </div>
                
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Formulas</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <textarea class="form-control" rows="7" id="formulas"></textarea>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Print Page</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5" style='color:black'>
                            <select class='form-control'  id="prints" name="prints">
                                <option value='1'  selected>1</option>
                                <option value='2' >2</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row" id="vessel_name">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Vessel Name</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='nm_kapal_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="bill_number_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Bill Of Lading Number</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='bill_number_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="bill_date_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Bill Of Lading Date</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='bill_date_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="port_loading_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Port Of Loading</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='port_loading_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="port_discharge_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Port Of Discharge</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='port_discharge_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="notify_party_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Notify Party</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='notify_party_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="desc_sample_2">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Description Sample</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='desc_sample_c'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row" id="quantity_1">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Quantity</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            
                            <input type='text' class='form-control input-xs' id='quantity'   >
                             </div>
                        </div></div> 
                </div>
                <div class="row">
                    <div class='col-lg-8'> 
                    </div>
                    <div class='col-lg-2'> 
                        <button type='button' class='btn btn-warning btn-md pull-right' onclick='list_template();' ><i
                                    class='fa fa-plus'></i>&nbsp;Add From Template
                        </button>  
                   </div>
                    <div class='col-lg-2'> 
                        <button type='button' class='btn btn-warning btn-md pull-left' onclick='list_parameter();' ><i
                                    class='fa fa-plus'></i>&nbsp;Add Parameter
                        </button>  
                   </div> 
                </div>
                
                
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%; " id="tabel_template" >
                        <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>Simbol</th> 
                                <th>Satuan</th> 
                                <th>Metode</th> 
                                <th>Mark</th> 
                                <th style='width:100px'>Nilai Spesifikasi</th> 
                                <th style='width:100px'>Nilai Result</th> 
                                <th style="width:20px">Hapus</th> 
                            </tr>
                        </thead>
                        <tbody id="isi_tabel_template">
                        </tbody>
                    </table> 
                </div>
            </div>
                
                 <div class="row">
                            <div class="col-lg-12" > 
                                    <div class="form-group"> 
                                           <button type='button' class='btn btn-primary btn-lg btn-simpan pull-right' onclick='simpan();' id='input_simpans'><i
                                                        class='fa fa-save'></i>&nbsp;Simpan
                                            </button>  
                                            <button type='button' class='btn btn-success btn-lg btn-edit pull-right' onclick='update();' id='input_update'><i
                                                        class='fa fa-save'></i>&nbsp;Update
                                            </button>
                                    </div> 
                            </div>
               </div>
                 
    
        </div>
    </div>
</div>



<div class='modal fade ' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
             
                <div class="row">
                    <div class='col-lg-3'>
                    </div>
                    <div class='col-lg-6'>
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Market</label> 
                            <div class="col-lg-10">
                                <select class='form-control input-xs selectpicker' data-live-search='false' id='kapal' name="kapal" > 
                                        <option value='0' selected >Domestic</option>
                                        <option value='1' >Export</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Name Request:</label>
                            <input type='hidden' class='form-control input-xs' id='id_request' name="id_request" >
                            <input type='text' class='form-control input-xs' id='nm_request' name="nm_request" >
                        </div> 
                    </div>
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Type Request:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='tipe_request' name="tipe_request" >
                                <option value='' data-hidden='true' selected='selected'>-- Type Request --</option>
                                <option value='0' >Specification</option>
                                <option value='1' >Certificated</option>
                            </select>
                        </div> 
                    </div>
                </div> 
                <div class="row">
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Standart:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='standart' name="standart" >
                                <option value='' data-hidden='true' selected='selected'>-- Choose Standart --</option>
                            </select>
                        </div> 
                    </div>
                    <div class='col-lg-6'>  
                        <div class='form-group'>
                            <label>Product:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='produk' name="produk" >
                                <option value='' data-hidden='true' selected='selected'>-- Choose Product --</option>
                            </select>
                        </div>
                    </div>  
                </div>  
                <div class="row"> 
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Type Product:</label> 
                            <select class='form-control selectpicker' data-live-search='true' id='tipe' name="tipe" >
                                <option value='' data-hidden='true' selected='selected'>-- Choose Type Product --</option>
                            </select>
                        </div>
                    </div>
                    
                 
                
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Brand:</label> 
                            <select class='form-control selectpicker' data-live-search='true' id='brand' name="brand" >
                                <option value='' data-hidden='true' selected='selected'>-- Choose Brand --</option>
                            </select>
                        </div>
                    </div>
                    
                </div> 

                <div class="row"> 
                    <!-- <div id='form_kapal1'>  
                     </div>   -->   
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Plant:</label> 
                                <select class='form-control selectpicker' data-live-search='true' id='plant' name="plant"  multiple>
                                    <option value='' data-hidden='true' >-- Choose Plant --</option>
                                </select>
                            </div> 
                        </div> 

                         <div class='col-lg-6'>
                             <div class='form-group'>
                                <label>Packing Product:</label> 
                                <select class='form-control selectpicker' data-live-search='false' id='packing' name="packing" >
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Packing Product --</option>
                                </select>
                            </div>
                        </div>
                        
                </div>
                
            <div id='form_kapal2'>     
                
                <div class="row">
                    <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Vessel Name:</label> 
                                <input type='text' class='form-control input-xs' id='nm_kapal' name="nm_kapal" >
                            </div> 
                        </div> 
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Bill Of Lading Number:</label> 
                                <input type='text' class='form-control input-xs' id='bill_number' name="bill_number" >
                            </div> 
                        </div> 
                    </div>
                    <div class="row">
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Bill Of Lading Date:</label> 
                                <input type='text' class='form-control input-xs' id='bill_date' name="bill_date" >
                            </div> 
                        </div> 
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Port Of Loading:</label> 
                                <input type='text' class='form-control input-xs' id='port_loading' name="port_loading" >
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Port Of Discharge:</label> 
                                <input type='text' class='form-control input-xs' id='port_discharge' name="port_discharge" >
                            </div> 
                        </div> 
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Description Of Sample:</label> 
                                <input type='text' class='form-control input-xs' id='desc_sample' name="desc_sample" >
                            </div> 
                        </div>
                    </div> 
                    <div class="row">
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Notify Party:</label> 
                                <input type='text' class='form-control input-xs' id='notify_party' name="notify_party" >
                            </div> 
                        </div> 
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Buyer ident (Trade/perush):</label> 
                             <input type='text' class='form-control input-xs' id='buyer' name="buyer" >
                        </div>
                        </div>
                        </div>
                        <div class="row"> 
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Export Destination:</label> 
                            <select class='form-control selectpicker' data-live-search='true' name="country"  id='country'>
                                <option value='' data-hidden='true' selected='selected'>-- Choose Negara --</option>
                            </select>
                        </div>  
                    </div>
                    <div class='col-lg-6'>  
                        <div class='form-group'>
                            <label>Total Shipment & shipment size:</label> 
                             <input type='text' class='form-control input-xs' name="total"  id='total'>
                        </div>
                    </div>
                </div> 
                
                <div class="row">
                    <div class='col-lg-3'>  
                    </div>
                    <div class='col-lg-6'>  
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Start Period Delivery :</label>  
                                 <input type='text' class='form-control input-xs date-picker' name="start_1" value='<?php echo date('m-Y') ?>' id='start_1'>
                            </div>  
                        </div>
                        <div class='col-lg-6'> 
                            <div class='form-group'>
                                <label>End Period Delivery :</label> 
                                 <input type='text' class='form-control input-xs date-picker' value='<?php $startDate = time();
                                       $final= date('m-Y', strtotime('+2 month', $startDate)); echo $final ?>'  name="end_1"  id='end_1'>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class='form-group'>
                    <label>Note:</label>
                   <textarea name="note" id='note' style="width:100%;height:150px"></textarea>
                </div>   
                    <div class="dropzone" style='margin-top:10px;'>

                      <div class="dz-message">
                       <center><h3> Drop files here or click to upload.</h3></center>
                      </div>

                    </div>
         </div>
                <!--<div  action="#" class="dropzone" id="dropzoneForm">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </div> -->
            </div>
            <div class='modal-footer'> 
               <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>  
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>
</div>



<div class='modal fade ' id='dlg_template' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;'> List Template</div>
            </div>
            <div class='modal-body'>
                 <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example" style="font-size:100%;cursor: pointer; " id="tabel_list_template" >
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Nama Template</th>
                                    <th>Standart</th> 
                                    <th>Type</th> 
                                    <th>Produk</th>    
                                    <th> </th>    
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table> 
                    </div>
            </div>
               
            </div>
            <div class='modal-footer'>  
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>


<div class='modal fade ' id='dlg_parameter' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                 <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example" style="font-size:100%;cursor: pointer; " id="tabel_list" >
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Parameter</th>
                                    <th>Simbol</th> 
                                    <th>Satuan</th> 
                                    <th>Mark</th>  
                                    <th>Metode</th>  
                                    <th>Parent</th>  
                                    <th> </th>  
                                    <th> </th>  
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table> 
                    </div>
            </div>
               
            </div>
            <div class='modal-footer'>  
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>



<div class='modal fade' id='dlg_tolak' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'> 
                            <h2 style='text-align:center;margin-top: -11px;'>Approval New <span id='v_tipe'></span></h2>
                            <h5 style='text-align:center'>No.  <span id='v_id'></span></h5>
                            <hr /> 
                            <input type='hidden' id='id_join'>
                            <input type='hidden' id='id_tipe'>
                            <input type='hidden' id='v_plantE'>
                            <table  border="1" style='width:100%;font-family:arial;font-size:12px'>  
                                <tbody>
                                        <tr>
                                            <td colspan='2' height='30px' style='text-align:center;font-weight:bold'>General Information</td> 
                                        </tr>
                                        <tr>
                                            <td width='30%'>Name</td>
                                            <td height='30px' id="v_nama"></td>
                                        </tr>
                                        <tr>
                                            <td>Standart</td>
                                            <td height='30px' id="v_standartM"></td>
                                        </tr>
                                        <tr>
                                            <td>Produk</td>
                                            <td height='30px' id="v_produkM"></td>
                                        </tr>
                                        <tr>
                                            <td>Type</td>
                                            <td height='30px' id="v_tipe1"></td>
                                        </tr>
                                        <tr>
                                            <td>Plant</td>
                                            <td height='30px' id="v_plantM"></td>
                                        </tr> 
                                </tbody>
                            </table>  
                                        <p>Keterangan :</p>
                                        <div class="row">  <div class="col-lg-12"> 
                                            <textarea name="syarat" id="syarat" style="width:100%"></textarea>
                                        </div>
                                        </div>
            </div>
            <div class='modal-footer'> 
                <button type='button' class='btn btn-danger btn-xs btn-edit' onclick='tolakRequest();'  id='input_update1'><i
                            class='fa fa-thumbs-down'></i>&nbsp;Reject
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>



<script>
<?php
        $sess_user = $this->session->userdata("USERLAB"); 
         $session_role = ''; 
        foreach($sess_user as $v){ 
            $session_role .= $v['ID_ROLE'].",";
        }  
        $role = rtrim($session_role, ",");
?>

    var  roleUser = <?php echo $role ?>; 
    var TabelData;
    var tokenFile = [];
    Dropzone.autoDiscover = false;

        var foto_upload= new Dropzone(".dropzone",{
        url: "<?php echo base_url('coq/request/fileUpload') ?>",
        maxFilesize: 2,
        method:"post",
        acceptedFiles:"image/*, .pdf, .docx, .xlsx, .doc, .xls",
        paramName:"userfile",
        dictInvalidFileType:"Type file ini tidak dizinkan",
        addRemoveLinks:true,
        });


        //Event ketika Memulai mengupload
        foto_upload.on("sending",function(a,b,c){
            a.token=Math.random();
            c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
            tokenFile.push(a.token) 
        });
        
        //Event ketika foto dihapus
        foto_upload.on("removedfile",function(a){
            var token=a.token;
            $.ajax({
                type:"post",
                data:{token:token},
                url:"<?php echo base_url('coq/request/remove_foto') ?>",
                cache:false,
                dataType: 'json',
                success: function(){
                    console.log("Foto terhapus");
                },
                error: function(){
                    console.log("Error");

                }
            });
        });
          $(".date-picker").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months", autoclose: true
        });

    $(document).ready(function () {
        
    if(roleUser=='30'){ 
        $("#tambah_data").hide();
    }
         $('#form_kapal1, #form_kapal2').hide();
          
        $('#kapal').change(function () {
            var kapal = $(this).val();
            if(kapal=='0'){ 
                $('#form_kapal1, #form_kapal2').hide();
            }else{
                $('#form_kapal1, #form_kapal2').show(); 
            }
        });
 
        $("#tabel_template tbody").sortable({
          cursor: "move",
          placeholder: "sortable-placeholder",
          helper: function(e, tr)
          {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index)
            {
            // Set helper cell sizes to match the original sizes
            $(this).width($originals.eq(index).width());
            });
            return $helper;
          }
        }).disableSelection(); 
                
                
        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [
            
                {"visible": false, "targets": 6},     
                {"visible": false, "targets": 11}, 
                {"visible": false, "targets": 12}, 
                {"visible": false, "targets": 13}, 
                {"visible": false, "targets": 14},
                {"visible": false, "targets": 15}, 
                {"visible": false, "targets": 16}, 
                {"visible": false, "targets": 17}, 
                {"visible": false, "targets": 18}, 
                {"visible": false, "targets": 19}, 
                {"visible": false, "targets": 20}, 
                {"visible": false, "targets": 21}, 
                {"visible": false, "targets": 22}, 
                {"visible": false, "targets": 23},
                {"visible": false, "targets": 24}, 
                {"visible": false, "targets": 25}, 
                {"visible": false, "targets": 26},  
                 {"visible": false, "targets": 27}, 
                 {"visible": false, "targets": 28}, 
                 {"visible": false, "targets": 29}, 
                 {"visible": false, "targets": 30}, 
                 {"visible": false, "targets": 31}, 
                 {"visible": false, "targets": 32}, 
                 {"visible": false, "targets": 33},
                 {"visible": false, "targets": 34},
                 
                {"orderable": false, "targets": 35}, 
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/request/get_data',
                dataType: 'JSON',
                dataSrc: function (json) { 
                console.log(write_menu)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.data.length; i++) { 
                    var aksi = '';
                    
                       var request = (json.data[i].TIPE_REQUEST == 0 ? 'SPECIFICATION' : 'CERTIFICATED') 
                        var download = "style='display:none'";
                       if(json.data[i].STATUS==0){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Submit</span></button>';
                       }else if(json.data[i].STATUS==1){
                           var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Create Admin</span></button>';
                       }else if(json.data[i].STATUS==11){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Admin</span></button>';
                       }else if(json.data[i].STATUS==2){
                           var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Approve 1</span></button>';
                       }else if(json.data[i].STATUS==21){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 1</span></button>';
                       }else if(json.data[i].STATUS==3){
                           var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Approve 2</span></button>';
                           
                       }else if(json.data[i].STATUS==31){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 2</span></button>';
                       }else if(json.data[i].STATUS==4){
                           var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Done</span></button>';
                           var download = '';
                       }else if (json[i].STATUS == 41){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 3</span></button>';
                       }
                       
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            if(json.data[i].STATUS=='0' && json.role=='31'){
                                var aksi = '<center> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button>' 
                            }
                            else if(json.data[i].STATUS=='4' && json.role =='31'){
                                var aksi = '<center><button class="btn btn-primary btn-xs waves-effect" '+download+' onclick="print(\'' + json.data[i].ID_JOIN + '\', \'' + json.data[i].TIPE_REQUEST + '\')" title="Print"><span class="fa fa-print"></span> </button></center>' 
                            }else if(json.data[i].STATUS=='4' && json.role =='30'){
                                var aksi = '<center><button class="btn btn-primary btn-xs waves-effect" '+download+' onclick="print(\'' + json.data[i].ID_JOIN + '\', \'' + json.data[i].TIPE_REQUEST + '\')" title="Print"><span class="fa fa-print"></span> </button></center>' 
                            }else if(json.role !='31' && json.data[i].STATUS!='4'){
                                var baru = (json.data[i].NEW==0 ? 'Create' : 'Update')
                                var warna_button = (json.data[i].NEW==0 ? ' btn-success ' : ' btn-primary ')
                                var icon_button = (json.data[i].NEW==0 ? ' fa-plus ' : ' fa-pencil ')
                                var button_reject = (json.data[i].NEW==0 ? ' <button  onclick="tolak(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect"><span class="btn-labelx"><i class="fa  fa-thumbs-o-down"></i></span></button>' : '  ')
                                var aksi = (json.data[i].STATUS==11 || json.data[i].STATUS==2 || json.data[i].STATUS==3  ? '': '<center><button onclick="edit(\'' + i + '\')" title="'+baru+' '+request+'" class="btn '+warna_button+' btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa '+icon_button+'"></i></span></button> '+button_reject+'</center>');
                            }
                            // var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        } 
                      
                       
                       
                        return_data.push({
                            'id': no,
                            'kd_request'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_request(\'' + i + '\')"><u>'+json.data[i].KD_REQUEST+'</u></a>',  
                            'request'   :   json.data[i].NM_REQUEST,  
                            'standart': json.data[i].NM_STANDART, 
                            'tipe': json.data[i].KD_PRODUCT_TYPE,
                            'brand': json.data[i].NM_BRAND,
                            'packing': json.data[i].NM_PACK,
                            'produk': json.data[i].NM_PRODUCT,
                            'tipe_request': request,
                            'plant': json.data[i].NM_PLANT,
                            'status': "<center>"+status+"</center>",
                            'id_request': json.data[i].ID_REQUEST,
                            'id_standart': json.data[i].ID_STANDART,
                            'id_tipe': json.data[i].ID_PRODUCT_TYPE,
                            'id_packing': json.data[i].ID_PACK,
                            'id_product': json.data[i].ID_PRODUCT,
                            'id_tipe_request': json.data[i].TIPE_REQUEST,
                            'note': json.data[i].NOTE,
                            'country': json.data[i].NM_COUNTRY,
                            'buyer': json.data[i].BUYER,
                            'total_ship': json.data[i].TOTAL,
                            'date': json.data[i].STARTDATE+' s.d '+json.data[i].ENDDATE,
                            'baru': json.data[i].NEW,
                            'id_spec': json.data[i].ID_SPEC,
                            'spec': json.data[i].NM_SPEC,
                            'kapal': json.data[i].IS_KAPAL,
                            'id_plant': json.data[i].ID_PLANT,
                            'nm_kapal': json.data[i].NM_KAPAL,
                            'bill_number_1': json.data[i].BILL_NUMBER,
                            'bill_date_1': json.data[i].BILL_DATE,
                            'port_loading_1': json.data[i].PORT_LOADING,
                            'port_discharge_1': json.data[i].PORT_DISCHARGE,
                            'desc_sample_1': json.data[i].DESC_SAMPLE,
                            'notify_party_1': json.data[i].NOTIFY_PARTY,
                            'id_brand': json.data[i].ID_BRAND,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'kd_request'}, 
                {data: 'request'}, 
                {data: 'standart'}, 
                {data: 'tipe'}, 
                {data: 'brand'}, 
                {data: 'packing'},
                {data: 'produk'},
                {data: 'tipe_request'},
                {data: 'plant'},
                {data: 'status'}, 
                {data: 'id_request'}, 
                {data: 'id_standart'},
                {data: 'id_tipe'},
                {data: 'id_packing'},
                {data: 'id_product'},
                {data: 'id_tipe_request'},
                {data: 'note'},
                {data: 'country'},
                {data: 'buyer'},
                {data: 'total_ship'},
                {data: 'date'},
                {data: 'baru'},
                {data: 'id_spec'},
                {data: 'spec'},
                {data: 'kapal'},
                {data: 'id_plant'},
                {data: 'nm_kapal'},
                {data: 'bill_number_1'},
                {data: 'bill_date_1'},
                {data: 'port_loading_1'},
                {data: 'port_discharge_1'},
                {data: 'desc_sample_1'},
                {data: 'notify_party_1'},
                {data: 'id_brand'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
        
        

        refreshStandart()
        refreshTipe()
        // refreshPacking()
        refreshProduk()
        refreshPlant()
        // refreshCountry()
        refreshRequest() 
        /// Button Action
        
        
        
        $('#tambah_data').click(function () {
            
            // var myDropzone = Dropzone.forElement("#dropzoneForm");
            // myDropzone.removeAllFiles(true); 
            tokenFile = [];
            refreshStandart()
            refreshTipe()
            refreshPacking()
            refreshProduk()
            refreshPlant()
            refreshCountry()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_request, #nm_request, #tipe_request, #standart, #tipe, #produk, #note,#nm_kapal,#bill_number,#bill_date,#port_loading ,#port_discharge,#desc_sample,#notify_party ").val('');
            $("#dlg").modal("show");
        });

        $('#add_chat').click(function () {
            if ($("#message").val() == ""  ) {
                show_toaster(3, '', "Message harus terisi.")
                // informasi(BootstrapDialog.TYPE_WARNING, "Message harus terisi.");
                return;
            }
             $.post("<?php echo base_url(); ?>coq/request/simpanMessage", {
                    "message": $("#message").val() , 
                    "id_request": $("#id_request_chat").val() , 
                    
                }, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.notif == "1") {  
                        refreshChat($("#id_request_chat").val())
                        $("#message").val('') 
                         show_toaster(1, '', 'Chat Terkirim.')
                    } else {
                         show_toaster(2, '', data.message)
                        // informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                     show_toaster(2, '',"Gagal menyimpan data. Server sedang bermasalah.")
                    // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
                }).always(function () {
                });
        }); 
        

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

         $('#tabel_list tbody').on( 'click', 'tr', function () { 
             var tes = ListParameter.row( this ).data();
             var parameter =  tes['parameter'] ;
             var simbol =  tes['simbol'] ;
             var satuan =  tes['satuan'] ;
             var mark =  tes['mark'] ;
             var metode =  tes['metode'] ;
             var id_parameter =  tes['id_parameter'] ; 
             var id_standart_uji =  tes['id_standart_uji'] ; 
             var tipe_request = $("#tipe_request").val();
             $("#tabel_template").find("tbody").append(
                "<tr style='cursor: move;'>"+ 
                
                "<td><span id='"+id_parameter+"'>"+parameter+"</span></td>"+
                "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                "<td>"+(metode==null ? '' : metode)+"</td>"+  
                "<td>"+(mark==null ? '' : mark)+"</td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result"+id_parameter+"'></td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+id_parameter+"\',\'"+parameter+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+id_parameter+"</td>"+
                "<td style='display:none;'>"+id_standart_uji+"</td>"+ 
                "</tr>");   
                
                $("#dlg_parameter").modal("hide");
           } );
            $('#tabel_list_template tbody').on( 'click', 'tr', function () { 
             var tes = ListTemplate.row( this ).data(); 
                $.post("<?php echo base_url(); ?>coq/spesification/get_template", {"id_template": tes['id_template']}, function(datas) {
                    var data = JSON.parse(datas);
                     for(var i=0;i< data.length; i++){  
                                $("#tabel_template").find("tbody").append(
                                     "<tr>"+ 
                            
                                    "<td><span id='"+data[i].ID_UJI+"'>"+data[i].NAMA_UJI+"<span></td>"+
                                    "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                                    "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                                    "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                                    "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                                    "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_TEMPLATE==null ? '' : data[i].NILAI_TEMPLATE)+"' id='nilai_template'></td>"+  
                                    "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_RESULT==null ? '' : data[i].NILAI_RESULT)+"' id='nilai_result"+data[i].ID_UJI+"'></td>"+  
                                    "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+data[i].ID_UJI+"\',\'"+data[i].NAMA_UJI+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                                    "<td style='display:none;'> </td>"+
                                    "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                                    "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                                    "</tr>");      
                           
                     }
                         
                         
                }).fail(function() {
                    show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")    
                }).always(function() {  
                });
                
                $("#dlg_template").modal("hide");
           } );


    });

        function tombol_back(){ 
              $("#dataRequest").show(500)
              $("#detailRequest").hide(500) 
              $("#formTemplate").hide(500)
        }
        
    function print(code, tipe)
    {
        if(tipe==0){
            window.open("<?=base_url()?>coq/Spesification/template/"+code);  
        }else{
            window.open("<?=base_url()?>coq/Quality/template/"+code);
        }
        
    }
         
    function tolak(baris) {   
        var kolom = TabelData.row(baris).data(); 
        $("#v_nama").html(kolom['request']);
        $("#v_standartM").html(kolom['standart']); 
        $("#v_produkM").html(kolom['produk']); 
        $("#v_tipe, #v_tipe1").html(kolom['tipe_request']); 
        $("#id_join").val(kolom['id_request']);  
        $("#v_id").html(kolom['kd_request']);  
        $("#v_plantM").html(kolom['plant']);  
        $("#v_plantE").val(kolom['id_plant']);  
        $("#v_company").html(kolom['company']);  
        var tipe = (kolom['tipe_request']=='Specification' ? '0' : '1')
        $("#id_tipe").val(tipe);  
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Approval</b>");
        $("#dlg_tolak").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }
    
    
    function tolakRequest() {
        var id = $("#id_join").val();
        var syarat = $("#syarat").val();
        var plant = $("#v_plantE").val();   
        if (syarat == ""  
        ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Keterangan  harus terisi.");
            return;
        }
        $('input_update1').attr('disabled', true);
        $('#input_update1').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        $.post("<?php echo base_url(); ?>coq/request/tolakRequest", {
            "id": id,
            "syarat": syarat,  
            "plant": plant,   
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg_tolak").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil melakukan Reject dan mengirim Email.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
            $('#input_update1').html("<i class='fa fa-thumbs-down'></i>&nbsp;Reject");
            $('#input_update1').attr('disabled', false);
        }).always(function () {
 
            $('#input_update1').html("<i class='fa fa-thumbs-down'></i>&nbsp;Reject");
            $('#input_update1').attr('disabled', false);
        });
    }

        
    function detail_request(baris) {
        var kolom = TabelData.row(baris).data();    
        $("#v_trequest").html(kolom['tipe_request']);
        $("#v_nrequest").html(kolom['request']);
        $("#v_standart").html(kolom['standart']);
        $("#v_tipe").html(kolom['tipe']);
        $("#v_brand").html(kolom['brand']);
        $("#v_packing").html(kolom['packing']);
        $("#v_buyer").html(kolom['buyer']);
        $("#v_country").html(kolom['country']);
        $("#v_bill_number").html(kolom['bill_number_1']);
        $("#v_total").html(kolom['total_ship']);
        $("#v_kapal").html(kolom['nm_kapal']);
        $("#v_bill_date").html(kolom['bill_date_1']);
        $("#v_port_loading").html(kolom['port_loading_1']);
        $("#v_port_discharge").html(kolom['port_discharge_1']);
        $("#v_notify").html(kolom['notify_party_1']);
        $("#v_desc_sample").html(kolom['desc_sample_1']);
        $("#v_date").html(kolom['date']); 
        $("#v_note").html(kolom['note']); 
        $("#v_produk").html(kolom['produk']); 
        if(kolom['kapal']=='0'){
           $("#form_kapal_c").hide()
        }else{
           $("#form_kapal_c").show() 
        }
        
         $("#id_request_chat").val(kolom['id_request']); 
        
        $.post("<?php echo base_url(); ?>coq/request/detail_request", {'id_request' : kolom['id_request']}, function (datas) {
             var data = JSON.parse(datas); 
            $("#v_plant").html(data.plant); 
            $("#v_file").html(data.file); 
            refreshChat($("#id_request_chat").val())
            
             $("#dataRequest").hide('2000')
              $("#detailRequest").show('2000')
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshChat(id_request) {
        $.post("<?php echo base_url(); ?>coq/request/refreshChat", {'id_request' : id_request}, function (datas) {
              var data = JSON.parse(datas); 
                $("#chatting").html(data.chat);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshPacking() {
        $.post("<?php echo base_url(); ?>coq/request/refreshPacking", function (data) {
            $("#packing").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshStandart() {
        $.post("<?php echo base_url(); ?>coq/request/refreshStandart", function (data) {
            $("#standart").html(data); $("#standart_c").html(data);
            
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshTipe() {
        $.post("<?php echo base_url(); ?>coq/request/refreshTipe", function (data) {
            $("#tipe").html(data);
            $("#tipe_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshProduk() {
        $.post("<?php echo base_url(); ?>coq/request/refreshProduk", function (data) {
            $("#produk").html(data);
            $("#produk_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshPlant() {
        $.post("<?php echo base_url(); ?>coq/request/refreshPlant", function (data) {
            $("#plant").html(data);
             $("#plant_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    
    function refreshCountry() {
        $.post("<?php echo base_url(); ?>coq/request/refreshCountry", function (data) {
            $("#country").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    
    function refreshRequest(id_request) {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshRequest", function (data) { 
                $("#request").html(data);
                $("#request_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    } 
    $(function () {
        $('#tipe_c').change(function () {
            refreshBrand_c($("#tipe_c option:selected").val())
        });

        function refreshBrand_c(id) {
            $.post("<?php echo base_url(); ?>coq/spesification/refreshBrand", {
                "id": id
            }, function (data) {
                $("#brand_c").html(data);


            }).fail(function () {
                // Nope
            }).always(function () {
                $(".selectpicker").selectpicker("refresh");
            });
        }
        $('#tipe').change(function () {
            refreshBrand($("#tipe option:selected").val())
        });

        function refreshBrand(id) {
            $.post("<?php echo base_url(); ?>coq/spesification/refreshBrand", {
                "id": id
            }, function (data) {
                $("#brand").html(data);


            }).fail(function () {
                // Nope
            }).always(function () {
                $(".selectpicker").selectpicker("refresh");
            });
        }
    });
 
    function simpan() {
        var notify_party = $("#notify_party").val();
        var desc_sample = $("#desc_sample").val();
         var port_discharge = $("#port_discharge").val();
        var port_loading = $("#port_loading").val();
        var bill_date = $("#bill_date").val(); 
        var bill_number = $("#bill_number").val(); 
        var nm_kapal = $("#nm_kapal").val(); 
        var nm_request = $("#nm_request").val(); 
        var tipe_request = $("#tipe_request").val(); 
        var standart = $("#standart").val(); 
        var tipe = $("#tipe").val(); 
        var brand = $("#brand").val(); 
        var produk = $("#produk").val(); 
        var note = $("#note").val(); 
        var buyer = $("#buyer").val(); 
        var country = $("#country").val(); 
        var total = $("#total").val(); 
        var start = $("#start_1").val(); 
        var end = $("#end_1").val(); 
        var plant = $("#plant").val();
        var packing = $("#packing").val(); 
        var kapal = $("#kapal").val(); 
        if (nm_request == "" ) {
            
            show_toaster(3, '', "Form harus terisi.")
            // informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
            return;
        }
        
        $('#input_simpan').attr('disabled', true);
        $('#input_simpan').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        $.post("<?php echo base_url(); ?>coq/request/simpan", {
            "nm_request": nm_request ,
            "nm_kapal": nm_kapal ,
            "bill_number": bill_number ,
            "bill_date": bill_date ,
            "port_loading": port_loading ,
            "port_discharge": port_discharge ,
            "desc_sample": desc_sample ,
            "notify_party": notify_party ,
            "tipe_request": tipe_request ,
            "standart": standart ,
            "tipe": tipe ,
            "brand": brand ,
            "produk": produk ,
            "note": note , 
            "buyer": buyer , 
            "country": country , 
            "total": total , 
            "start": start , 
            "end": end ,  
            "plant": plant ,
            "packing": packing ,
            "kapal": kapal ,
            "tokenFile": tokenFile ,
            
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                
                show_toaster(1, '', data.message)
                // informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)
                // informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
                show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")
            // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
            
            $('#input_simpan').html("<i class='fa fa-save'></i>&nbsp;Simpan"); 
            $('#input_simpan').attr('disabled', false);
        });
    }

    
        function hapus_role(elm) { 
            var cld = $(elm).parent().parent(); //tr 
            cld.remove(); 
        }
        $("#quantity_1").hide();
        $("#desc_sample_2").hide();
        $("#notify_party_2").hide();
        $("#port_discharge_2").hide();
        $("#port_loading_2").hide();
        $("#bill_date_2").hide();
        $("#bill_number_2").hide();
        $("#vessel_name").hide();
        $(function(){
            $('#prints').change(function(data){
            if ($('#prints option:selected').val() == '1') {
        $("#quantity_1").hide();
        $("#desc_sample_2").hide();
        $("#notify_party_2").hide();
        $("#port_discharge_2").hide();
        $("#port_loading_2").hide();
        $("#bill_date_2").hide();
        $("#bill_number_2").hide();
        $("#vessel_name").hide();
            }else if($('#prints option:selected').val() == '2'){
        $("#quantity_1").show();
        $("#desc_sample_2").show();
        $("#notify_party_2").show();
        $("#port_discharge_2").show();
        $("#port_loading_2").show();
        $("#bill_date_2").show();
        $("#bill_number_2").show();
        $("#vessel_name").show();
            }
        });
        });
    function edit(baris) { 
       
        var kolom = TabelData.row(baris).data();    
        $('#isi_tabel_template').html('');
        $("#id_spec").val(kolom['id_spec']);
        $("#nm_spec").val(kolom['request']); 
        /*$("#standart_c").val(kolom['id_standart']); 
        $("#tipe_c").val(kolom['id_tipe']);
        $("#brand_c").val(kolom['id_brand']);   
        $("#produk_c").val(kolom['id_product']);*/ 
        /*$("#request_c").val(kolom['id_request']);*/ 
        $("#tipe_request_c").val(kolom['id_tipe_request']); 
        /*$("#id_plant").val(kolom['id_plant']); */
        /*$("#start").val(kolom['date']); 
        $("#end").val(kolom['date']);*/ 
        $("#formulas").val(kolom['formulas']); 
        /*$("#prints").val(kolom['prints']); */
        $("#nm_kapal_c").val(kolom['nm_kapal']); 
        $("#bill_number_c").val(kolom['bill_number_1']);
        $("#bill_date_c").val(kolom['bill_date_1']); 
        $("#port_loading_c").val(kolom['port_loading_1']);
        $("#port_discharge_c").val(kolom['port_discharge_1']); 
        $("#notify_party_c").val(kolom['notify_party_1']); 
        $("#desc_sample_c").val(kolom['desc_sample_1']);
        $("#quantity").val(kolom['total_ship']);
        
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
        var baru = (kolom['baru']==0 ? 'Create ' : 'Update ')
        var titel = (kolom['id_tipe_request']==0 ? ''+baru+' Specification' : ''+baru+' Certificated')
        $("#title_tipe").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;"+titel+"</b>");
        
                var link1 = (kolom['id_tipe_request']==0 ? 'spesification' : 'quality') 
                var link2 = (kolom['id_tipe_request']==0 ? 'id_spec' : 'id_quality') 
                console.log(link2)
        $.post("<?php echo base_url(); ?>coq/"+link1+"/get_spec", {'id_spec': kolom['id_spec'], 'id_quality': kolom['id_spec'], }, function(datas) {
            var data = JSON.parse(datas);
             for(var i=0;i< data.length; i++){  
                        $("#tabel_template").find("tbody").append(
                            "<tr>"+ 
                            
                            "<td>"+data[i].NAMA_UJI+"</td>"+
                            "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                            "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                            "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                            "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_STD==null ? '' : data[i].NILAI_STD)+"' id='nilai_template'></td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_SPEC==null ? '' : data[i].NILAI_SPEC)+"' id='nilai_result'></td>"+  
                            "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                            "<td style='display:none;'> </td>"+
                            "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                            "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                            "</tr>");   
                   
             }
                 
                 
        }).fail(function() {
                show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")      
        }).always(function() { 
          $('#input_update').attr('disabled',false) ;
          $('#input_update').html('<i class="fa fa-save"> </i> Update') ;
        });
        
        
        
                  $("#dataRequest").hide(500)
                  $("#formTemplate").show(500)
        $(".selectpicker").selectpicker("refresh");
    }


  
    function update() {
        var id = $("#id_spec").val();
        var nm_spec = $("#nm_spec").val();  
        var standart = $("#standart_c").val(); 
        var tipe = $("#tipe_c").val();
        var brand = $("#brand_c").val(); 
        var produk = $("#produk_c").val();  
        var request = $("#request_c").val(); 
        var tipe_request = $("#tipe_request_c").val();  
        var plant = $("#plant_c").val();  
        var quantity_1 = $("#quantity").val();
        var desc_sample_2 = $("#desc_sample_c").val();  
        var notify_party_2 = $("#notify_party_c").val(); 
        var port_discharge_2 = $("#port_discharge_c").val();
        var port_loading_2 = $("#port_loading_c").val(); 
        var bill_date_2 = $("#bill_date_c").val();  
        var bill_number_2 = $("#bill_number_c").val(); 
        var vessel_name = $("#nm_kapal_c").val();
        var start = $("start").val();  
        var end = $("end").val();
        var formulas = $("#formulas").val();  
        var prints = $("#prints").val();  
        if (nm_spec == "") {
            show_toaster(3, '', "Form harus terisi.")    
            return;
        }
        
        $('#input_update').attr('disabled', true);
        $('#input_update').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        
        var isi_data = "";
        var i = 0;
        isi_data += "header[id]=" + encodeURI($("#id_spec").val());
        isi_data += "&header[nm_spec]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[nm_quality]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[standart]=" + encodeURI($("#standart_c").val());
        isi_data += "&header[tipe]=" + encodeURI($("#tipe_c").val());
        isi_data += "&header[brand]=" + encodeURI($("#brand_c").val());
        isi_data += "&header[produk]=" + encodeURI($("#produk_c").val()); 
        isi_data += "&header[request]=" + encodeURI($("#request_c").val());    
        isi_data += "&header[plant]=" + encodeURI($("#plant_c").val());    
        isi_data += "&header[start]=" + encodeURI($("#start").val()); 
        isi_data += "&header[end]=" + encodeURI($("#end").val());
        isi_data += "&header[formulas]=" + encodeURI($("#formulas").val());    
        isi_data += "&header[quantity_1]=" + encodeURI($("#quantity").val());
        isi_data += "&header[desc_sample_2]=" + encodeURI($("#desc_sample_c").val());
        isi_data += "&header[notify_party_2]=" + encodeURI($("#notify_party_c").val());
        isi_data += "&header[port_discharge_2]=" + encodeURI($("#port_discharge_c").val());
        isi_data += "&header[port_loading_2]=" + encodeURI($("#port_loading_c").val());
        isi_data += "&header[bill_date_2]=" + encodeURI($("#bill_date_c").val());
        isi_data += "&header[bill_number_2]=" + encodeURI($("#bill_number_c").val()); 
        isi_data += "&header[vessel_name]=" + encodeURI($("#nm_kapal_c").val()); 
        isi_data += "&header[prints]=" + encodeURI($("#prints").val()); 

         var tbl_list = $("#tabel_template tbody");  
        tbl_list.find('tr').each(function(i, obj) {
            isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
            isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html()); 
            isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(5).find('input').val()); 
            isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(6).find('input').val()); 
            i++;
        });   
                var link1 = (tipe_request==0 ? 'spesification' : 'quality') 
                var link2 = (id=='' ? 'simpan' : 'update') 
        $.post("<?php echo base_url(); ?>coq/"+link1+"/"+link2, isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                show_toaster(1, '', data.message)    
                  $("#dataRequest").show()
                  $("#formTemplate").hide()
                  
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)    
            }
        }).fail(function () {
                show_toaster(2, '', "Gagal mengubah data. Server sedang bermasalah.")    
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }


    function list_parameter(){
         $("#dlg_parameter").modal("show");
        ListParameter = $('#tabel_list').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 8},  
                {"visible": false, "targets": 7},    
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listParameter',
                data : {
                    'id_standart' : $("#standart_c").val(),
                    'id_produk' : $("#produk_c").val() 
                },
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {   
                        return_data.push({
                            'id': no, 
                            'parameter'   :  json[i].NAMA_UJI,  
                            'simbol': json[i].SIMBOL, 
                            'satuan': json[i].SATUAN, 
                            'mark': json[i].MARK, 
                            'metode': json[i].NM_METODE, 
                            'parent': json[i].NAMA_PARENT, 
                            'id_parameter': json[i].ID_UJI,   
                            'id_standart_uji': json[i].ID_STANDART_UJI,   
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'parameter'},  
                {data: 'simbol'}, 
                {data: 'satuan'},  
                {data: 'mark'},  
                {data: 'metode'},   
                {data: 'parent'},   
                {data: 'id_parameter'},   
                {data: 'id_standart_uji'},   
            ],
        }); 
    }

    function list_template(){
         $("#dlg_template").modal("show");
        ListTemplate = $('#tabel_list_template').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 5},    
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listTemplate',
                data : {
                     
                },
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {  
                        return_data.push({
                            'id': no, 
                            'template'   :  json[i].NM_TEMPLATE,  
                            'standart': json[i].NM_STANDART, 
                            'tipe': json[i].KD_PRODUCT_TYPE, 
                            'produk': json[i].NM_PRODUCT, 
                            'id_template': json[i].ID_TEMPLATE_HEADER,  
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'template'},  
                {data: 'standart'}, 
                {data: 'tipe'},  
                {data: 'produk'},  
                {data: 'id_template'},    
            ],
        }); 
    }
    
    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Metode</b>",
            "message": "Anda yakin ingin menghapus Request \"" + kolom['kd_request'] + "\" - \"" + kolom['request'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_request'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/request/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                 show_toaster(1, '', "Berhasil menghapus data.")
                // informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else { 
                 show_toaster(2, '', data.message)
                // informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
                 show_toaster(2, '', "Gagal menghapus data. Server sedang bermasalah.")
            // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }
    $(function(){
             $('#cara_1').hide();
             $('#cara_2').hide();
             $('#cara_3').hide();
    $('#rumus').change(function(){
        if ($('#rumus option:selected').val() == 'rumus_1') {
            $('#cara_1').show();
            $('#cara_2').hide();
            $('#cara_3').hide();
        }else if($('#rumus option:selected').val() == 'rumus_2'){
             $('#cara_2').show();
             $('#cara_1').hide();
             $('#cara_3').hide();
        }else if($('#rumus option:selected').val() == 'rumus_3'){
             $('#cara_3').show();
             $('#cara_1').hide();
             $('#cara_2').hide();
        }
    });
        });
      
 //   });
 $(function(){
    $('#btn-update').click(function(){
    
      
         /*var _id = $('#edit-name').val();

        $(_tr).find('td:eq(0)').text(_id);*/
        
        $('#modal-edit').modal('hide');
        var cara_1 = 4.071 * ($('#2').val()- $('#3').val()) - 7.600 * $('#5').val() - 6.718 * $('#7').val() - 1.430 * $('#9').val();
        var cara_2 = 4.071 * ($('#12').val()- $('#13').val()) - 7.600 * $('#15').val() - 6.718 * $('#17').val() - 1.430 * $('#19').val() - 2.582 * $('#21').val();
        var cara_3 = 4.071 * $('#32').val() - 7.600 * $('#34').val() - 6.718 * $('#36').val() - 1.430 * $('#38').val();
        /*$("#"+$('#id-name').val()).html($('#edit-name').val());*/
        /*$("#"+$('#id-coba').val()).html($('#name').val());*/
       if ($('#rumus option:selected').val() == 'rumus_1') {
            $("#nilai_result"+$('#id-name').val()).val(cara_1);
        }else if ($('#rumus option:selected').val() == 'rumus_2') {
            $("#nilai_result"+$('#id-name').val()).val(cara_2);
        }else if($('#rumus option:selected').val() == 'rumus_3')  {
           $("#nilai_result"+$('#id-name').val()).val(cara_3); 
        }  
        
        


    


 });
 });

    function updateparam(a, b){
        $('#cara_1').hide();
             $('#cara_2').hide();
             $('#cara_3').hide();
        // var _tr = $(this).closest('tr');
        // var _id = _tr.find('td:eq(0)').text();
        $('#rumus').val('');
        $('#rumus').selectpicker('refresh');

        $('#form-edit')[0].reset();
        $('#modal-edit').modal('show');
        $('#id-name').val(a);
        $('#name_param').val(b);        
        /*$('#edit-name').val(b);*/
        
 }
</script>
<!-- Dialog "Edit Record" -->
   <div id="modal-edit" class="modal" tabindex="-1" role="dialog">
      <form id="form-edit" action="">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Rumus Perhitungan</h5>
            </div>
            <div class="modal-body">
               <div class="form-group">
                <div class="form-group">
                    <input id="name_param" class="form-control" type="text" name="name" value="" readonly="">
                </div>
                  <label>Rumus: </label>                  
                  <input id="id-name" class="form-control" type="text" name="name" value="" style="display: none;">
                  
                  <select class='form-control selectpicker' data-live-search='true' id="rumus">
                                <option value="" data-hidden='true' selected='selected'></option>
                                <option value='rumus_1'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3</option>
                                <option value='rumus_2'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3 - 2.582 x SO3</option>
                                <option value='rumus_3'>C3S = 4.071 x CaO - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3 </option>
                            </select>
                            <br><br>
                            <div id="cara_1">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="1" class="form-control"  value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="2" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="3" class="form-control" placeholder="FCaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="4" class="form-control" value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="5" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="6" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="7" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="8" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="9" class="form-control" placeholder="Fe2O3"> 
                                   </div>
                                </div>
                            </div>

                            <!-- cara 2 -->
                            <div id="cara_2">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="11" class="form-control" value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="12" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="13" class="form-control" placeholder="FCaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="14" class="form-control" value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="15" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="16" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="17" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="18" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="19" class="form-control" placeholder="Fe2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="20" class="form-control" value="2,582" readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="21" class="form-control" placeholder="SO3"> 
                                   </div>
                                </div>
                            </div>
                            <!-- cara 3 -->
                            
                            <div id="cara_3">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="31" class="form-control" value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="32" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="33" class="form-control"  value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   
                                   
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="34" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="35" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="36" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="37" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="38" class="form-control" placeholder="Fe2O3"> 
                                   </div>
                                </div>
                            </div>
                 <!--  <input id="name" class="form-control" type="text" name="name" value="" required> -->
               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="btn-update">Save</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
      </form>
   </div>


