<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Master Company</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
                        <!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
                        <!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
                        <!--                            Excel-->
                        <!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'>
                                <center> No</center>
                            </th>
                            <th>
                                <center> Company</center>
                            </th>
                            <th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                      id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>
                                <center></center>
                            </th>
                            <td>
                                <center> Company</center>
                            </td>
                            <td></td>
                            <th style='widtd:120px;'>
                                <center></center>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='form-group'>
                    <label>Name Company:</label>
                    <input type='hidden' class='form-control input-xs' id='id_company'>
                    <input type='text' class='form-control input-xs' id='nm_company'>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {


        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy', title: 'Master_Company', exportOptions: {columns: [0, 1]}},
                {extend: 'excel', title: 'Master_Company', exportOptions: {columns: [0, 1]}},
                {extend: 'pdf', title: 'Master_Company', exportOptions: {columns: [0, 1]}},
                {extend: 'print', title: 'Master_Company', exportOptions: {columns: [0, 1]}},

            ],
            "columnDefs": [
                {"visible": false, "targets": 2},
                {"orderable": false, "targets": 3},
                {"orderable": false, "targets": 0},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/company/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        }
                        return_data.push({
                            'id': no,
                            'company': json[i].NM_COMPANY,
                            'id_company': json[i].ID_COMPANY,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'company'},
                {data: 'id_company'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        refreshKelompok()
        /// Button Action
        $('#tambah_data').click(function () {

            refreshKelompok()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_company, #nm_company, #kelompok, #lokasi").val('');
            $("#dlg").modal("show");
        });

        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>coq/company/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })


        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });


    });

    function refreshKelompok() {
        $.post("<?php echo base_url(); ?>coq/company/refreshKelompok", function (data) {
            $("#kelompok").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function simpan() {
        var nm_company = $("#nm_company").val();
        if (nm_company == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/company/simpan", {
            "nm_company": nm_company
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#id_company").val(kolom['id_company']);
        $("#nm_company").val(kolom['company']);
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function update() {
        var id = $("#id_company").val();
        var nm_company = $("#nm_company").val();
        $('#input_update').attr('disabled', true);
        $('#input_update').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        if (nm_company == ""
        ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama  harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/company/update", {
            "id": id,
            "nm_company": nm_company,
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Company</b>",
            "message": "Anda yakin ingin menghapus Company \"" + kolom['company'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_company'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/company/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
