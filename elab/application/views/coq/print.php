

    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    </style>
    

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i>Print SPEC/QUALITY</h2>  
            <div class="ibox-content">

                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabelData">
                        <thead>
                            <tr>
                               <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Kode Request</center>  </th> 
                            <th> <center> Nama Product</center>  </th> 
                            <th> <center> Tanggal</center>  </th> 
                          <th style='width:120px;'>
                                <center> Print <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                           
                            </tr>
                            <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Kode Request</center>  </td> 
                            <td> <center> Nama Product</center>  </td> 
                            <td> <center> Tanggal</center>  </td> 
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>
 



<script>
   var tableData;
          $(document).ready(function(){
           
      
    tableData = $('#tabelData').DataTable({ 
        "processing": false,
        "serverSide": true,
        "lengthMenu": [10, 25, 50, 100],
        "sDom": 'Rfrtlip',
        'lengthChange': true,
        "searching": true,
        "ordering": false,
        "autoWidth": false,
        "info": true,
        "scrollCollapse": true,
        "ajax": {
            "url": "<?php echo site_url('index.php/coq/Display/ajax_list')?>",
            "type": "POST"
        },
    
  });
}); 
 $('#tabelData thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        

	
 
   function detail_request(id){
          
        
            $.ajax({
              url : '<?php echo site_url('coq/Display/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('#id_request').val(data.ID_REQUEST);
                $('#v_trequest').val(data.KD_REQUEST);
                $('#v_nrequest').val(data.NM_REQUEST);
                $('#v_standart').val(data.NM_STANDART);
                $('#v_tipe').val(data.KD_PRODUCT_TYPE);
                $('#v_packing').val(data.NM_PACK);
                $('#v_buyer').val(data.BUYER);
                $('#v_country').val(data.NM_COUNTRY);
                $('#v_total').val(data.TOTAL);
                $('#v_date').val(data.ENDDATE);
                $('#v_note').val(data.NOTE);
                

                
              $("#exampleModal").modal('show');
              }
            });
          }
        
    function print(code)
    {
      window.open("<?=base_url()?>coq/Display/template/"+code);
        
    }
   

</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Detail Spec/Quality</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Tipe Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <input type="hidden" id="id_request">
                            <div class="col-lg-8"><input type="text" id='v_trequest' readonly=""  style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Nama Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_nrequest' readonly=""  style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_standart' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Tipe Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_tipe' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Packing Produk</label><div class="col-lg-1" style=' width : 0px;'><p>:</p></div> 
                            <div class="col-lg-8"><input type="text" id='v_packing' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Buyer Ident (Trade/perush)</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_buyer' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Plant</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_plant' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Export Destination</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_country' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Total shipment & shipment size</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_total' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div>  
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Shipment Period Delivery</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><input type="text" id='v_date' readonly=""   style="width:100%"></div>
                        </div></div> 
                </div>  
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Note</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><textarea name="v_note" id='v_note' style="width:100%" readonly rows='5'></textarea></div>
                        </div></div> 
                </div>   
                
                
     
    </div>
  </div>
</div>