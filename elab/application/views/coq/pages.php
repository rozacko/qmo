 <style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

   

</style>
 <div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins" id='dataHeader'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Pages Portal</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data" onclick="add()"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
<!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
<!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
<!--                            Excel-->
<!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Menu</center>  </th> 
                            <th> <center> Link</center>  </th> 
                            
                            
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Menu</center>  </td> 
                            <td> <center> Link</center>  </td> 
                             
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

       var tableData;
          $(document).ready(function(){
             tableData = $('#tabel').DataTable({ 
                 "processing": true,
        "serverSide": true,
        "lengthMenu": [10, 25, 50, 100],
        'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            ],
        'lengthChange': true,
        "searching": true,
        "ordering": false,
        "autoWidth": false,
        "ajax": {
            "url": "<?php echo site_url('coq/pages/ajax_list')?>",
            "type": "POST"
        },
              
            });
            });
          $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(tableData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            tableData.column($(this).data('index')).search(this.value).draw();
        });

        tableData.on('order.dt search.dt', function () {
            tableData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
            var save_method;  
              function add()
          {
            save_method ='add';
            $('#form')[0].reset();
            $('.modal-title').text('Tambah Data');
            
            $('#myModal').modal('show');
          }
          function save()
          {
            var url;
             
            if(save_method == 'add'){
              
                url = '<?php echo site_url('coq/pages/tambah') ;?>';
              }
              
            
            else{
              
                url = '<?php echo site_url('coq/pages/update') ;?>';
            }
            $.ajax({
              url:url,
              type:'POST',
              data:$('form').serialize(),
              dataType:'JSON',
              success:function(data){
                 show_toaster(1, '','Berhasil Menyimpan Data')
                tableData.ajax.reload();
                $('#myModal').modal('hide');
                
              },
              error:function(jqXHR, textStatus, errorThrown)
              {
               show_toaster(2, '',"Gagal menyimpan data. Server sedang bermasalah.")  
              }
            });
          }
          function edit(id){
          $('#form')[0].reset();
         save_method ='edit';
         
         
         $.ajax({
           url : '<?php echo site_url('coq/pages/edit') ?>/'+id,
           type : 'GET',
           dataType : 'JSON',
            success:function(data){
              
              $('[name="id_menu"]').val(data.ID_MENU);
              $('[name="menu"]').val(data.MENU);
              $('[name="link"]').val(data.LINK);
              $('[name="flag"]').val(data.FLAG_ACTIVE);
              

              
              $('.modal-title').text('Update Menu');
              $('#myModal').modal('show');
              //$(".selectpicker").selectpicker("refresh");
          }
        });
    }
           function konfirmasi(id) {
        
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Menu</b>",
            "message": "Anda yakin ingin menghapus Menu Ini ?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(id, dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/pages/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.status == "true") {
                dialog.close();
                show_toaster(1, '', "Sukses menyimpan data.");
                tableData.ajax.reload();
            } else {
                dialog.close();
                show_toaster(1, '', "Sukses menyimpan data.");
                tableData.ajax.reload();
            }
        }).fail(function () {
            show_toaster(2, '', "Gagal Harap Periksa Data.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }
     
        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }

        });        
    </script>
    <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
              <h4 class="modal-title">Tambah Data</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               
             </div>
             <div class="modal-body form">
                <form action="" method="POST" id="form">
                  <div class="form-group">
                    <input type="hidden" name="id_menu">
                     <input type="hidden" name="flag">
                    
                  </div>
                  
                  <div class="form-group" id="menu">
                    <label >Menu</label>
                    <input type="text" name="menu" class="form-control" >
                  </div>
                  <div class="form-group" id="link">
                    <label >Link</label>
                    <input type="text" name="link" class="form-control" >
                  </div>
                 
                  </form>
               
             </div>
             <div class="modal-footer">
              <button class="btn btn-primary" onclick="save()" id="submit">Simpan</button>
              
             </div>
            </div>

          </div>
        </div>
         

    

