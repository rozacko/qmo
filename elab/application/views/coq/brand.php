<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">

            <div class="col-md-6">
                <h2><i class="fa fa-list"></i> Master Brand</h2>
            </div>
            <div class="col-md-6">
                <div style="text-align:right;">
                    <button class="btn-sm btn-success" onclick="add()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah
                    </button>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <table id="Tabel" class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'>
                                <center> No</center>
                            </th>
                            <th>
                                <center> Type Product</center>
                            </th>
                            <th>
                                <center> Nama Brand</center>
                            </th>
                            <th>
                                <center> Kode Brand</center>
                            </th>

                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                      id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>
                                <center></center>
                            </th>
                            <td>
                                <center>Type Product</center>
                            </td>
                            <td>
                                <center> Nama Brand</center>
                            </td>
                            <td>
                                <center> Kode Brand</center>
                            </td>

                            <th style='widtd:120px;'>
                                <center></center>
                            </th>
                        </tr>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body form'>
                <form action="" method="POST" id="form">
                    <div class='form-group'>
                        <label>Type Product:</label>
                        <input type='hidden' class='form-control input-xs' name="id_brand">
                        <select class='form-control selectpicker' data-live-search='true' name="type_product"
                                data-max-options="10">
                            <option value='' data-hidden='true' selected='selected'>-- Pilih Type Product --</option>
                            <?php
                            foreach ($product->result() as $a) {
                                ?>
                                <option value="<?php echo $a->ID_PRODUCT_TYPE ?>"><?php echo $a->KD_PRODUCT_TYPE ?>
                                    - <?php echo $a->NM_PRODUCT_TYPE ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>

                    <div class='form-group'>
                        <label>Name Brand:</label>
                        <input type="text" name="brand" class="form-control">
                    </div>
                    <div class='form-group'>
                        <label>Kode Brand:</label>
                        <input type="text" name="kode" class="form-control">
                    </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='save();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>

            </div>
        </div>
    </div>
</div>


<script>


    var TabelData;
    $(document).ready(function () {

        TabelData = $('#Tabel').DataTable({
            "processing": true,
            "serverSide": true,
            "lengthMenu": [10, 25, 50, 100],
            "sDom": 'Rfrtlip',
            'lengthChange': true,
            "searching": true,
            "ordering": false,
            "autoWidth": false,
            "ajax": {
                "url": "<?php echo site_url('coq/brand/get_data')?>",
                "type": "POST"
            },

        });


        $('#Tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
    var save_method;


    function add() {
        save_method = 'add';
        $('#form')[0].reset();
        $('.modal-title').text('Tambah Data');
        $(".selectpicker").selectpicker("refresh");
        $('#dlg').modal('show');
    }

    function save() {
        var url;

        if (save_method == 'add') {
            url = '<?php echo site_url('coq/brand/tambah');?>';
        }
        else {
            url = '<?php echo site_url('coq/brand/update');?>';
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: $('form').serialize(),
            dataType: 'JSON',
            success: function (data) {
                show_toaster(1, '', "Sukses menyimpan data.");
                TabelData.ajax.reload();
                $('#dlg').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                show_toaster(2, '', "Gagal Harap Periksa Data.");
            }
        });
    }

    function edit(id) {
        save_method = 'update';
        $('#form')[0].reset();

        $.ajax({
            url: '<?php echo site_url('coq/brand/edit'); ?>/' + id,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                $('[name="id_brand"]').val(data.ID_BRAND);
                $('[name="type_product"]').val(data.ID_PRODUCT_TYPE);
                $('[name="brand"]').val(data.NM_BRAND);
                $('[name="kode"]').val(data.KODE_BRAND);


                $('#dlg').modal('show');
                $('.modal-title').text('UPDATE');
                $(".selectpicker").selectpicker("refresh");
                $('#submit').text('UPDATE');
            }
        });
    }

    function konfirmasi(id) {

        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Brand</b>",
            "message": "Anda yakin ingin menghapus Standart Product Ini ?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(id, dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/brand/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.status == "true") {
                dialog.close();
                show_toaster(1, '', "Sukses menyimpan data.");
                TabelData.ajax.reload();
            } else {
                dialog.close();
                show_toaster(1, '', "Sukses menyimpan data.");
                TabelData.ajax.reload();
            }
        }).fail(function () {
            show_toaster(2, '', "Gagal Harap Periksa Data.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

    $('.filter_table').click(function () {
        var status = $('#filtering').val();
        if (status == '1') {
            $('#filtering').val('0')
            $('.filtering').hide(500)
        } else {
            $('#filtering').val('1')
            $('.filtering').show(500)
        }
    });

    // $('#Table_filter').remove();


    $(document).on('click', '#btn_filtering', function () {
        var data = $(this).data('filtering');
        if (data == 1) {
            $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
            $('#filtering').hide(500);

        } else {
            $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
            $('#filtering').show(500);
        }
    });
</script>
