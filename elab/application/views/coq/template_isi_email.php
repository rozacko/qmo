<!DOCTYPE html>
<html>
<head>
    <meta name='viewport' content='width=device-width'>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <title>Simple Transactional Email</title>
    <style type='text/css'>
        /* -------------------------------------
                    INLINED WITH https://putsmail.com/inliner
                ------------------------------------- */

        /* -------------------------------------
                    RESPONSIVE AND MOBILE FRIENDLY STYLES
                ------------------------------------- */
        .garis {
            border: 1px solid black;
        }

        .garistitik {
            border-bottom: 1px dotted black;
            border-right: 1px solid black;
        }

        .hanyaTitik {
            border-bottom: 1px dotted black;
        }

        .kanan {
            border-right: 1px solid black;
        }

        .atas {
            border-top: 1px solid black;
        }

        table #utama {
            border: 1px solid black;
        }

        #utama th {
            height: 20px;
        }

        #utama tr td {
            height: 18px;
            /*border: 1px solid red;*/
        }

        .kotak {
            width: 300px;
            height: 25px;
            /*background-color: lightgrey;*/
            border: 1px solid black;
            margin: 2px;
            text-align: center;
            font-size: 8px;
            align-content: right;
            position: absolute;
            right: 50px;
            bottom: 10px;
        }

        @media only screen and (max-width: 890px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important;
            }

            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important;
            }

            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important;
            }

            table[class=body] .content {
                padding: 0 !important;
            }

            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important;
            }

            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important;
            }

            table[class=body] .btn table {
                width: 100% !important;
            }

            table[class=body] .btn a {
                width: 100% !important;
            }

            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important;
            }
        }

        /* -------------------------------------
                    PRESERVE THESE STYLES IN THE HEAD
                ------------------------------------- */

        @media all {
            .ExternalClass {
                width: 100%;
            }

            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%;
            }

            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important;
            }

            .btn-primary table td:hover {
                background-color: #34495e !important;
            }

            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important;
            }
        }
    </style>
</head>

<body class=''
      style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;'>
<table border='0' cellpadding='0' cellspacing='0' class='body'
       style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
    <tr>
        <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
        <td class='container'
            style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:1024px;padding:10px;width: 1024px;Margin:0 auto !important;'>
            <div class='content'
                 style='box-sizing:border-box;display:block;Margin:0 auto;max-width:1024px;padding:10px;'>
                <!-- START CENTERED WHITE CONTAINER -->
                <span class='preheader'
                      style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
                <table border='0' cellpadding='0' cellspacing='0' class='body'
                       style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                    <tr>
                        <th width='20%' align='center'><img src='" . base_url() . "assets/image/SEMEN.png'
                                                            style='width:50%'></th>
                        <td width='60%' align='center'>
                            <h4>
                                E-COQ
                                <br/>
                                Electronic Certificate of Quality Semen Indonesia
                            </h4>
                        </td>
                        <th></th>
                        <!-- <th width='20%' align='center'><img src='../../img/k3.png' style='width:50%'></th> -->
                    </tr>
                </table>
                <table class='main'
                       style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class='wrapper'
                            style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
                            <table border='0' cellpadding='0' cellspacing='0'
                                   style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                                <tr>
                                    <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>
                                        <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                        <center>
                                            <h4><u>Approval {$param['title']}</u></center>
                                        </p>

                                        <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                                    <center><h4 style='text-decoration:underline'>Kode ORDER</h4></center>
                                                  </p> -->
                                        <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                            Kepada Yth,
                                            <br> {$param['KEPADA']}<br> Di tempat<br>
                                        </p>
                                        <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                            Terdapat permintaan approval untuk pengajuan {$param['title']}, detail
                                            rincian sebagai berikut:</p>


                                        <hr style="height: 2px;border: 0; box-shadow: inset 0 12px 12px -12px; padding: 0px;margin: 0px;">
                                        <!--<hr style="border: 10px solid black;padding: 0px; margin: 0px;">-->
                                        <br><br>

                                        <table width="100%"
                                               style="border-collapse: collapse; border: none; margin-top: 6px;">
                                            <tr>


                                                <!-- <td align="center"><h4><b>SPECIFICATION OF <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b></h4> -->
                                                <td align="center" style="padding-left: 30px"><h2><b><u>SPECIFICATION OF
                                                                QUALITY (SOQ)</u></b></h2>
                                                </td>

                                            </tr>
                                            <tr>
                                                <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                                                    ?>
                                                    <td></td>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <td align="center"><h4>
                                                            <b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                                                                (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4>
                                                    </td>
                                                    <?php
                                                } ?>

                                            </tr>


                                        </table>
                                        <table width="80%" align="center"
                                               style="border-collapse: collapse; border: none;">
                                            <tr>

                                                <td style="text-align: center;">
                                                    <small style="font-weight: bold">Certificate No :</small>
                                                    <small style="font-weight: bold;font-size: 12px;color: midnightblue;"><?php echo $data['detail'][0]['KD_SPEC']; ?>
                                                        /<?php echo date('m') ?>
                                                        /<?php echo $data['detail'][0]['KODE_BRAND']; ?></small>
                                                </td>

                                            </tr>
                                            <tr>

                                                <td style="text-align: center; font-weight: bold">
                                                    <small>Brand : <?php
                                                        if ($data['detail'][0]['NM_BRAND'] == '') {
                                                            echo '&nbsp;&nbsp;  -';
                                                        } else {
                                                            echo $data['detail'][0]['NM_BRAND'];

                                                        }; ?></small>
                                                </td>


                                            </tr>
                                        </table>
                                        <br>
                                        <table width="200px"
                                               style="border-collapse: collapse; border: none; max-width: 200px">
                                            <tr>
                                                <td style="text-align: right">
                                                    <small style="font-size: 10px;">Date Produce</small>
                                                </td>
                                                <td>
                                                    <small style="font-size: 10px;">:</small>
                                                </td>
                                                <td>
                                                    <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['START_PRODUCE']; ?></small>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td style="text-align: right;">
                                                    <small style="font-size: 10px; align-content: right">To</small>
                                                </td>
                                                <td>
                                                    <small style="font-size: 10px;">:</small>
                                                </td>
                                                <td>
                                                    <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['END_PRODUCE']; ?></small>
                                                </td>

                                            </tr>
                                        </table>

                                        <table id="utama" width="100%"
                                               style="border-collapse: collapse;border: 1px solid black;">
                                            <thead>
                                            <tr>
                                                <!--        <th rowspan='2' colspan="4" width="52%" class="garis">-->
                                                <th colspan="4" width="52%" class="garis">

                                                    <center style="font-size: 12px;"><p>QUALITY PARAMETERS</p></center>
                                                </th>
                                                <!--            <th align='center' rowspan='2' colspan="3" width='40%'>-->
                                                <!--            </th>-->

                                                <th align='center' colspan='3' width='48%' class="garis">
                                                    <center>
                                                        <small
                                                                style="font-size: 12px;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></small>
                                                    </center>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>

                                                <!--            <td></td>-->
                                                <th colspan='3' width="44%" class="garis" style="text-align: left">
                                                    <small style="font-size: 10px;">&nbsp;&nbsp;<b>I.CHEMICAL
                                                            COMPOSITION : </b></small>
                                                </th>

                                                <th width="8%" class="garis">
                                                    <center>
                                                        <small style="font-size: 10px;"><b>UNIT</b></small>
                                                    </center>
                                                </th>
                                                <th align='center' width='17%' class="garis">
                                                    <center>
                                                        <small style="font-size: 10px;"><b>TESTING METHOD</b></small>
                                                    </center>
                                                </th>
                                                <th align='center' width='17%' class="garis">
                                                    <center>
                                                        <small style="font-size: 10px;"><b>SPECIFICATION</b></small>
                                                    </center>
                                                </th>
                                                <th align='center' width='14%' class="garis">
                                                    <center>
                                                        <small style="font-size: 10px;"><b>TEST RESULT</b></small>
                                                    </center>
                                                </th>
                                            </tr>
                                            <!-- INI SOQ DATA LOOPING-->
                                            <?php $no = 0;
                                            foreach ($list_par as $value) {

                                                if (empty($value['NILAI_STD'])) {
                                                    $value['NILAI_STD'] = '-';
                                                    $value['MARK'] = '';
                                                }
                                                if (empty($value['NILAI_SPEC'])) {
                                                    $value['NILAI_SPEC'] = '-';
                                                    $value['MARK'] = '';
                                                }
                                                if ($value['ID_KATEGORI'] == '1') {
                                                    ?>
                                                    <tr>

                                                        <?php if (count($value['NAMA_UJI']) == '0') {
                                                            ?>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <?php
                                                        } else {
                                                            $no++;
                                                            ?>
                                                            <td width="5%" class="garistitik">
                                                                <small style="font-size: 9px;">
                                                                    <center><?php echo $no; ?></center>
                                                                </small>
                                                            </td>
                                                            <?php if (count($p['NAMA_PARENT']) == '0') {
                                                                ?>
                                                                <td class="hanyaTitik">
                                                                    <small style="font-size: 9px;">
                                                                        &nbsp; <?php echo $value['NAMA_UJI']; ?></small>
                                                                </td>
                                                                <td style="border-left-width: 0px;border-left-style: solid;"
                                                                    class="hanyaTitik kanan">
                                                                    <small style=" font-size: 9px;">
                                                                        (<?php echo $value['SIMBOL']; ?>)
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['SATUAN']; ?></center>
                                                                    </small>
                                                                </td>

                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['NM_METODE']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['MARK']; ?>
                                                                            &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;color: midnightblue;">
                                                                        <center><?php echo $value['NILAI_SPEC']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <td class="hanyaTitik">
                                                                    <small style="font-size: 9px;"><?php echo $value['NAMA_PARENT']; ?>
                                                                        <br><?php echo $value['NAMA_UJI']; ?></small>
                                                                </td>
                                                                <td style="border-left-width: 0px;border-left-style: solid;">
                                                                    <small style="font-size: 9px;"></small>
                                                                    <center><?php echo $value['SIMBOL']; ?></center>
                                                                    </small></td>
                                                                <td>
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['SATUAN']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td>
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['NM_METODE']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td>
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $value['MARK']; ?>
                                                                            &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td>
                                                                    <small style="font-size: 9px; color: midnightblue;">
                                                                        <center><?php echo $value['NILAI_SPEC']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <?php
                                                            }

                                                        } ?>

                                                    </tr>
                                                    <?php
                                                }
                                            } ?>
                                            <tr>
                                                <td class="kanan"></td>
                                                <td colspan="2" class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                            </tr>
                                            <tr>
                                                <th colspan='3' class="garis" style="text-align: left">
                                                    <small style="font-size: 10px;">&nbsp;&nbsp;<b>II.PHYSICAL
                                                            PROPERTIES : </b></small>
                                                </th>
                                                <th class="garis"></th>
                                                <th class="garis"></th>
                                                <th class="garis"></th>
                                                <th class="garis"></th>
                                            </tr>

                                            <?php $no = 1;
                                            $tmp = array();
                                            foreach ($list_par as $value) {
                                                $tmp[$value['NAMA_PARENT']][] = $value;
                                            }
                                            foreach ($tmp as $tmp_valuw) {
                                                for ($i = 0; $i < sizeof($tmp_valuw); $i++) {
                                                    if ($tmp_valuw[$i]['ID_KATEGORI'] == '2') {
                                                        if ($tmp_valuw[$i]['NAMA_PARENT'] == '') {
                                                            ?>
                                                            <tr>
                                                                <td class="garistitik">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $no++; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik">
                                                                    <small style="font-size: 9px;">
                                                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                                                            &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;color: midnightblue;">
                                                                        <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                        <?php } else {
                                                            if ($i == 0) { ?>
                                                                <tr>
                                                                    <td class="garistitik">
                                                                        <small style="font-size: 9px;">
                                                                            <center><?php echo $no++; ?></center>
                                                                        </small>
                                                                    </td>
                                                                    <td colspan="2" class="hanyaTitik kanan">
                                                                        <small style="font-size: 9px;">
                                                                            &nbsp;<?php echo $tmp_valuw[$i]['NAMA_PARENT']; ?></small>
                                                                    </td>
                                                                    <!--                                <td>-->
                                                                    <?php //echo $tmp_valuw[$i]['NAMA_PARENT']
                                                                    ?><!--</td>-->
                                                                    <!--<td class="hanyaTitik kanan">
                                                                        <small style="font-size: 9px;">
                                                                            <center>
                                                                    </td>-->
                                                                    <td class="hanyaTitik kanan">
                                                                        <small style="font-size: 9px;">
                                                                            <center></center>
                                                                        </small>
                                                                    </td>

                                                                    <td class="hanyaTitik kanan">
                                                                        <small style="font-size: 9px;">
                                                                            <center></center>
                                                                        </small>
                                                                    </td>
                                                                    <td class="hanyaTitik kanan">
                                                                    </td>
                                                                    <td class="hanyaTitik kanan">
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <td class="garistitik"></td>
                                                                <td colspan="2" class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                                                    </small>
                                                                </td>

                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;">
                                                                        <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                                                            &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                                                    </small>
                                                                </td>
                                                                <td class="hanyaTitik kanan">
                                                                    <small style="font-size: 9px;color: midnightblue;">
                                                                        <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                                                    </small>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            };

                                            ?>
                                            <tr>
                                                <td class="kanan"></td>
                                                <td colspan="2" class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                                <td class="kanan"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <br>
                                        <table width='100%'>
                                            <tr>
                                                <td colspan="3"><p style='font-size:10px;'>
                                                        <?php if ($data['detail'][0]['NM_PRODUCT'] == 'CLINKER') { ?>
                                                            <i>We certifying that the clinker described above is
                                                                suitable for production of cement conforming
                                                                to <?php echo $data['detail'][0]['NM_STANDART']; ?></i>
                                                        <?php } else {
                                                            ; ?>
                                                            <i>We certifying that the cement described above is
                                                                compliance with specification of SNI 2049 : 2015
                                                                (<?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?>
                                                                )</i>
                                                        <?php }; ?>
                                                        <br>
                                                </td>
                                            </tr>
                                        </table>


                                        <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                            Surat permintaan ini sebagai informasi, Klik link berikut untuk melihat dan
                                            melakukan tindak lanjut.</p>
                                        <table border='0' cellpadding='0' cellspacing='0' class='btn btn-primary'
                                               style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;box-sizing:border-box;width:100%;'>
                                            <tbody>
                                            <tr>
                                                <td align='center'
                                                    style='font-family:sans-serif;font-size:14px;vertical-align:top;padding-bottom:15px;'>

                                                    <!-- ================================== BUTTON ======================================= -->
                                                    <table border='0' cellpadding='0' cellspacing='0'
                                                           style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;width:auto;'>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <center>
                                                                    <a href='" . base_url() . "coq/approval/prosesApproval/{$param['
                                                                       ID']}/{$param['id_tipe']}/{$param['STATUS1']}'
                                                                    target='_blank'
                                                                    style='text-decoration:underline;background-color:#ffffff;border:solid
                                                                    1px
                                                                    #3498db;border-radius:5px;box-sizing:border-box;color:#3498db;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px
                                                                    25px;text-decoration:none;text-transform:capitalize;background-color:#3498db;border-color:#3498db;color:#ffffff;'>Approve</a>
                                                                </center>
                                                            </td>
                                                            <td>
                                                                <center>
                                                                    <a href='" . base_url() . "coq/approval/prosesApproval/{$param['
                                                                       ID']}/{$param['id_tipe']}/{$param['STATUS2']}'
                                                                    target='_blank'
                                                                    style='text-decoration:underline;background-color:red;border:solid
                                                                    1px
                                                                    red;border-radius:5px;box-sizing:border-box;color:red;cursor:pointer;display:inline-block;font-size:14px;font-weight:bold;margin:0;padding:12px
                                                                    25px;text-decoration:none;text-transform:capitalize;background-color:red;border-color:red;color:#ffffff;'>Reject</a>
                                                                </center>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- ================================== BUTTON ======================================= -->

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                            Hormat saya,<br/>{$param['sender']}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- END MAIN CONTENT AREA -->
                </table>
                <!-- START FOOTER -->
                <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
                    <table border='0' cellpadding='0' cellspacing='0'
                           style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                        <tr>
                            <td class='content-block'
                                style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
							                  <span class='apple-link'
                                                    style='color:#999999;font-size:12px;text-align:center;'>
							                              E-COQ, <a style='color:#999999;font-size:12px;'
                                                                    href='http://quality.semenindonesia.com'>Electronic Certificate of Quality</a>
							                              <br>
							                              Copyright by <a style='color:#999999;font-size:12px;'
                                                                          href='http://sisi.id/'>PT.SISI</a>
							                            </span>
                                </span>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- END FOOTER -->
                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
    </tr>
</table>
</body>

</html>