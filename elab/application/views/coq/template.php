<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }
    .dropzone {
	margin-top: 100px;
	border: 2px dashed #0087F7; 
} 
 #tabel_list.dataTable tbody tr:hover {
  background-color: #ffa;
}
 
 #tabel_list.dataTable tbody tr:hover > .sorting_1 {
  background-color: #ffa;
}

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins" id='dataHeader'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Template Specification And Certificated</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
<!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
<!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
<!--                            Excel-->
<!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Template</center>  </th> 
                            <th> <center> Standart</center>  </th> 
                            <th> <center> Type</center>  </th>  
                            <th> <center> Product</center>  </th> 
                            <th> <center> Type Sertifikat</center>  </th> 
                            <th> <center> Request</center>  </th>  
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th> 
                            <th></th> 
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Template</center>  </td> 
                            <td> <center> Standart</center>  </td> 
                            <td> <center> Type</center>  </td>  
                            <td> <center> Product</center>  </td> 
                            <td> <center> Type Sertifikat</center>  </td> 
                            <td> <center> Request</center>  </td>  
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th>  
                            <th></th>  
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="ibox float-e-margins"  id='formTemplate' style='display:none'>
            <div class="ibox-title" > 
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Template Specification And Certificated</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-secondary" id='tombol_back' ><span
                                    class="fa fa-arrow-left"></span>&nbsp;Back
                        </button>&nbsp;&nbsp;&nbsp; 
                    </div>
                </div>  

            <div class="ibox-content"> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Nama Template</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            <input type='hidden' class='form-control input-xs' id='id_template' >
                            <input type='text' class='form-control input-xs' id='nm_template'   >
                             </div>
                        </div></div> 
                </div>  
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Tipe Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='tipe_request' name="tipe_request" >
                                <option value='0' selected='selected'>Specification</option>
                                <option value='1' >Certificated</option> 
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='standart' name="standart" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Tipe Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='tipe' name="tipe" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='produk' name="produk" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row" style='display:none'>
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='request' name="request" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>
                        <button type='button' class='btn btn-warning btn-md pull-right' onclick='list_parameter();' ><i
                                    class='fa fa-plus'></i>&nbsp;Add Parameter
                        </button>  
                   </div>
                </div>
                
                
			<div class="row">
                <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%; " id="tabel_template" >
						<thead>
							<tr>
								<th>Parameter</th>
								<th>Simbol</th> 
								<th>Satuan</th> 
								<th>Metode</th> 
								<th>Mark</th> 
								<th style='width:100px'>Nilai Spesifikasi</th> 
								<th style='width:100px'>Nilai Result</th> 
								<th style="width:20px">Hapus</th> 
							</tr>
						</thead>
						<tbody id="isi_tabel_template">
						</tbody>
					</table> 
					<table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%;display:none " id="tabel_template_quality" >
						<thead>
							<tr>
								<th>Parameter</th>
								<th>Simbol</th> 
								<th>Satuan</th> 
								<th>Metode</th> 
								<th>Mark</th> 
								<th style='width:100px'>Nilai Result</th> 
								<th style='width:100px'>Nilai Spesifikasi</th> 
								<th style="width:20px">Hapus</th> 
							</tr>
						</thead>
						<tbody id="isi_tabel_template_quality">
						</tbody>
					</table> 
				</div>
            </div>
                
                 <div class="row">
                            <div class="col-lg-12" > 
                                    <div class="form-group"> 
                                           <button type='button' class='btn btn-primary btn-md btn-simpan pull-right' onclick='simpan();' id='input_simpan'><i
                                                        class='fa fa-save'></i>&nbsp;Simpan
                                            </button>  
                                            <button type='button' class='btn btn-success btn-md btn-edit pull-right' onclick='update();' id='input_update'><i
                                                        class='fa fa-save'></i>&nbsp;Update
                                            </button>
                                    </div> 
                            </div>
               </div>
                 
    
        </div>
    </div>
</div>


<div class='modal fade ' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                 <div class="row">
                    <div class="table-responsive">
                        
                            <button class="btn btn-success" id="tombol"><span
                                    class="fa fa-plus"></span>&nbsp;Tambah Parameter
                        </button><br><br>
                        <table class="table table-striped table-bordered dataTables-example" style="font-size:100%;cursor: pointer; " id="tabel_list" >
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Parameter</th>
                                    <th>Simbol</th> 
                                    <th>Satuan</th> 
                                    <th>Mark</th>  
                                    <th>Metode</th>  
                                    <th>Parent</th>  
                                    
                                    <th> </th>  
                                    <th> </th>  
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table> 
                        
                    </div>
            </div>
               
            </div>
            <div class='modal-footer'>  
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>


<script>
    var TabelData;
    
          $(".date-picker").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months", autoclose: true
        });

    $(document).ready(function () {
        
                $("#tabel_template tbody").sortable({
                  cursor: "move",
                  placeholder: "sortable-placeholder",
                  helper: function(e, tr)
                  {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                    });
                    return $helper;
                  }
                }).disableSelection(); 
                $("#tabel_template_quality tbody").sortable({
                  cursor: "move",
                  placeholder: "sortable-placeholder",
                  helper: function(e, tr)
                  {
                    var $originals = tr.children();
                    var $helper = tr.clone();
                    $helper.children().each(function(index)
                    {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                    });
                    return $helper;
                  }
                }).disableSelection(); 
        $('#tombol').on('click', function(){
     

      var rows_selected = ListParameter.column(0).checkboxes.selected();
      
      
      $.each(rows_selected, function(){
        
        
         var tes =ListParameter.row(this).data();
         var tipe_request = $("#tipe_request").val()
             var parameter =  tes['parameter'] ;
             var simbol =  tes['simbol'] ;
             var satuan =  tes['satuan'] ;
             var mark =  tes['mark'] ;
             var metode =  tes['metode'] ;
             var id_parameter =  tes['id_parameter'] ; 
             var id_metode =  tes['id_standart'] ;
         if(tipe_request==0){
                $("#tabel_template").append(
                "<tr>"+  
                "<td><span id='"+id_parameter+"'>"+parameter+"</span></td>"+
                "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                "<td>"+(metode==null ? '' : metode)+"</td>"+  
                "<td>"+(mark==null ? '' : mark)+"</td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result"+id_parameter+"'></td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+id_parameter+"\',\'"+parameter+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+id_parameter+"</td>"+
                "<td style='display:none;'>"+id_metode+"</td>"+ 
                "</tr>");   
             
         }else{
                $("#tabel_template_quality").append(
                "<tr>"+  
                "<td><span id='"+id_parameter+"'>"+parameter+"</span></td>"+
                "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                "<td>"+(metode==null ? '' : metode)+"</td>"+  
                "<td>"+(mark==null ? '' : mark)+"</td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result"+id_parameter+"'></td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+id_parameter+"\',\'"+parameter+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+id_parameter+"</td>"+
                "<td style='display:none;'>"+id_metode+"</td>"+ 
                "</tr>");   
             
         }


                
                $("#dlg").modal("hide");
         
      });
         });
        
        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [ 
                {"visible": false, "targets": 6},  
                {"visible": false, "targets": 7},  
                {"visible": false, "targets": 8}, 
                {"visible": false, "targets": 9}, 
                {"visible": false, "targets": 10},
                {"visible": false, "targets": 11},  
                {"visible": false, "targets": 12},  
                {"orderable": false, "targets": 13}, 
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/template/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) { 
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else { 
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        } 
                      
                       var request = (json[i].TIPE_REQUEST == 0 ? 'SPECIFICATION' : 'CERTIFICATED')
                       var status = (json[i].STATUS == 0 ? ' <button  class="btn btn-primary btn-xs waves-effect "><span class="btn-labelx">Active</span></button>' : ' <button  class="btn btn-danger btn-xs waves-effect "><span class="btn-labelx"> Inactive</span></button>')
                        return_data.push({
                            'id': no, 
                            'template'   :  json[i].NM_TEMPLATE,  
                            'standart': json[i].NM_STANDART, 
                            'tipe': json[i].KD_PRODUCT_TYPE, 
                            'produk': json[i].NM_PRODUCT,
                            'nm_tipe': request,
                            'request': json[i].NM_REQUEST, 
                            'id_template': json[i].ID_TEMPLATE_HEADER,
                            'id_standart': json[i].ID_STANDART,
                            'id_tipe': json[i].ID_PRODUCT_TYPE,
                            'id_product': json[i].ID_PRODUCT,
                            'id_request': json[i].ID_REQUEST, 
                            'tipe_request': json[i].TIPE_REQUEST, 
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'template'},  
                {data: 'standart'}, 
                {data: 'tipe'},  
                {data: 'produk'},
                {data: 'nm_tipe'},
                {data: 'request'}, 
                {data: 'id_template'},
                {data: 'id_standart'},
                {data: 'id_tipe'}, 
                {data: 'id_product'},
                {data: 'id_request'}, 
                {data: 'tipe_request'},  
                {data: 'aksi'},
            ],
        });
        
        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        refreshStandart()
        refreshTipe() 
        refreshProduk()
        refreshRequest() 
        /// Button Action
        $('#tambah_data').click(function () {
              $('#isi_tabel_template, #isi_tabel_template_quality').html('');
            refreshStandart()
            refreshTipe() 
            refreshProduk()  
            refreshRequest() 
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show(); 
            $("#id_request, #nm_template, #tipe_request, #standart, #tipe, #produk, #note ").val('');
              $("#dataHeader").hide()
              
                $(".btn-simpan").show();
                $(".btn-edit").hide();
                
              $("#formTemplate").show()
            
            // $("#dlg").modal("show");
        });
 
          $('#tombol_back').click(function () {
              $("#dataHeader").show(500)
              $("#formTemplate").hide(500)
          });
          
          $('#tipe_request').change(function () {
              if($('#tipe_request').val()==0){
                  $("#tabel_template").show(500)
                  $("#tabel_template_quality").hide(500) 
              }else{
                  $("#tabel_template").hide(500)
                  $("#tabel_template_quality").show(500)  
              }
          });
        
        

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }

        });

         $('#tabel_list tbody').on( 'click', 'tr', function () { 
             var tes = ListParameter.row( this ).data();
             var parameter =  tes['parameter'] ;
             var simbol =  tes['simbol'] ;
             var satuan =  tes['satuan'] ;
             var mark =  tes['mark'] ;
             var metode =  tes['metode'] ;
             var id_parameter =  tes['id_parameter'] ; 
             var id_metode =  tes['id_standart'] ;  
             console.log(metode);
             
             var tipe_request = $("#tipe_request").val();
             if(tipe_request==0){
                 $("#tabel_template").find("tbody").append(
                    "<tr>"+  
                    "<td><span id='"+id_parameter+"'>"+parameter+"</span></td>"+
                    "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                    "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                    "<td>"+(metode==null ? '' : metode)+"</td>"+  
                    "<td>"+(mark==null ? '' : mark)+"</td>"+  
                    "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                    "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result"+id_parameter+"'></td>"+  
                    "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+id_parameter+"\',\'"+parameter+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                    "<td style='display:none;'> </td>"+
                    "<td style='display:none;'>"+id_parameter+"</td>"+
                    "<td style='display:none;'>"+id_metode+"</td>"+ 
                    "</tr>");   
                 
             }else{ 
                 $("#tabel_template_quality").find("tbody").append(
                    "<tr>"+  
                    "<td><span id='"+id_parameter+"'>"+parameter+"</span></td>"+
                    "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                    "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                    "<td>"+(metode==null ? '' : metode)+"</td>"+  
                    "<td>"+(mark==null ? '' : mark)+"</td>"+  
                    "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result"+id_parameter+"'></td>"+  
                    "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                    "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+id_parameter+"\',\'"+parameter+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                    "<td style='display:none;'> </td>"+
                    "<td style='display:none;'>"+id_parameter+"</td>"+
                    "<td style='display:none;'>"+id_metode+"</td>"+ 
                    "</tr>");   
             }
                
                $("#dlg").modal("hide");
           } );

    });
    
    function list_parameter(){
         $("#dlg").modal("show");
        ListParameter = $('#tabel_list').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_parameter', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_parameter', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_parameter', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_parameter', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 8},  
                {"visible": false, "targets": 7},    
                {
         'targets': 0,
         'checkboxes': {
            'selectRow': true
         }
     }
      ],
           'select': {
         'style': 'multi'
      },
        
              
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/template/get_listParameter',
                /*data : {
                    'id_standart' : $("#standart").val(),
                    'id_produk' : $("#produk").val() 
                },*/
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 0;
                    for (var i = 0; i < json.length; i++) {   
                         
                        return_data.push({
                            'id': no, 
                            'parameter'   :  json[i].NAMA_UJI,  
                            'simbol': json[i].SIMBOL, 
                            'satuan': json[i].SATUAN, 
                            'mark': json[i].MARK, 
                            'metode': json[i].NM_METODE, 
                            'parent': json[i].NAMA_PARENT,
                            'id_parameter': json[i].ID_UJI,   
                            'id_standart': json[i].ID_STANDART_UJI,   
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'parameter'},  
                {data: 'simbol'}, 
                {data: 'satuan'},  
                {data: 'mark'},  
                {data: 'metode'},   
                {data: 'parent'},
                {data: 'id_parameter'},   
                {data: 'id_standart'},   
            ],
        });
        
   
    }

    function check(){
        var tes = ListParameter.row( this ).data();
        alert('berhasil');
    }
    

        function hapus_role(elm) { 
			var cld = $(elm).parent().parent(); //tr 
            cld.remove(); 
		}
    function refreshRequest(id_request) {
        $.post("<?php echo base_url(); ?>coq/template/refreshRequest", function (data) { 
                $("#request").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    } 
    function refreshStandart() {
        $.post("<?php echo base_url(); ?>coq/template/refreshStandart", function (data) {
            $("#standart").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshTipe() {
        $.post("<?php echo base_url(); ?>coq/template/refreshTipe", function (data) {
            $("#tipe").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshProduk() {
        $.post("<?php echo base_url(); ?>coq/template/refreshProduk", function (data) {
            $("#produk").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }  
 
    function simpan() {
        var nm_template = $("#nm_template").val();  
        var standart = $("#standart").val(); 
        var tipe = $("#tipe").val(); 
        var produk = $("#produk").val();  
        var request = $("#request").val();  
        var tipe_request = $("#tipe_request").val();  
        if (nm_template == "" && standart == "" && tipe == "" && produk == ""  ) {
                         show_toaster(3, '',"Form harus terisi.")  
            return;
        }
        
        
         var isi_data = "";
            var i = 0;
            isi_data += "header[id]=" + encodeURI($("#id_template").val());
            isi_data += "&header[nm_template]=" + encodeURI($("#nm_template").val());
            isi_data += "&header[standart]=" + encodeURI($("#standart").val());
            isi_data += "&header[tipe]=" + encodeURI($("#tipe").val());
            isi_data += "&header[produk]=" + encodeURI($("#produk").val()); 
            isi_data += "&header[request]=" + encodeURI($("#request").val());   
            isi_data += "&header[tipe_request]=" + encodeURI($("#tipe_request").val());   
             
              if(tipe_request==0){  
              var tbl_list = $("#tabel_template tbody");  
                    tbl_list.find('tr').each(function(i, obj) {
                        isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
                        isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html());  
                        isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(5).find('input').val()); 
                        isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(6).find('input').val()); 
                        i++;
                    });   
                }else{
                var tbl_list = $("#tabel_template_quality tbody");  
                    tbl_list.find('tr').each(function(i, obj) {
                        isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
                        isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html());  
                        isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(5).find('input').val()); 
                        isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(6).find('input').val()); 
                        i++;
                    });   
                    
                }
          
        
        
        $.post("<?php echo base_url(); ?>coq/template/simpan", isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                  show_toaster(1, '',data.message)   
                  $("#dataHeader").show()
                  $("#formTemplate").hide()
                TabelData.ajax.reload();
            } else {
                  show_toaster(2, '',data.message) 
            }
        }).fail(function () {
                  show_toaster(2, '',"Gagal menyimpan data. Server sedang bermasalah.")  
        }).always(function () {
        });
    }

    function edit(baris) { 
        var kolom = TabelData.row(baris).data();  
        
        $('#isi_tabel_template').html('');
        $('#isi_tabel_template_quality').html('');
        $("#id_template").val(kolom['id_template']);
        $("#nm_template").val(kolom['template']); 
        $("#standart").val(kolom['id_standart']); 
        $("#tipe").val(kolom['id_tipe']);   
        $("#produk").val(kolom['id_product']); 
        $("#request").val(kolom['id_request']); 
        $("#tipe_request").val(kolom['tipe_request']); 
           if(kolom['tipe_request']==0){
                  $("#tabel_template").show(500)
                  $("#tabel_template_quality").hide(500) 
              }else{
                  $("#tabel_template").hide(500)
                  $("#tabel_template_quality").show(500)  
              }
        
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        
        $.post("<?php echo base_url(); ?>coq/template/get_template", {"id_template": kolom['id_template']}, function(datas) {
            var data = JSON.parse(datas);
             for(var i=0;i< data.length; i++){  
                if(kolom['tipe_request']==0){
                        $("#tabel_template").find("tbody").append(
                            "<tr>"+ 
                            
                            "<td><span id='"+data[i].ID_UJI+"'>"+data[i].NAMA_UJI+"<span></td>"+
                            "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                            "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                            "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                            "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_TEMPLATE==null ? '' : data[i].NILAI_TEMPLATE)+"' id='nilai_template'></td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_RESULT==null ? '' : data[i].NILAI_RESULT)+"' id='nilai_result"+data[i].ID_UJI+"'></td>"+  
                            "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+data[i].ID_UJI+"\',\'"+data[i].NAMA_UJI+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                            "<td style='display:none;'> </td>"+
                            "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                            "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                            "</tr>");   
                    
                }else{
                        $("#tabel_template_quality").find("tbody").append(
                            "<tr>"+ 
                            
                            "<td><span id='"+data[i].ID_UJI+"'>"+data[i].NAMA_UJI+"<span></td>"+
                            "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                            "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                            "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                            "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_RESULT==null ? '' : data[i].NILAI_RESULT)+"' id='nilai_result"+data[i].ID_UJI+"'></td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_TEMPLATE==null ? '' : data[i].NILAI_TEMPLATE)+"' id='nilai_template'></td>"+  
                            "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'"+data[i].ID_UJI+"\',\'"+data[i].NAMA_UJI+"\')\"><i class='fa fa-pencil fa-lg'></i></a></td>"+
                            "<td style='display:none;'> </td>"+
                            "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                            "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                            "</tr>");   
                    
                }
                   
             }
                 
                 
        }).fail(function() {
                  show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")  
        }).always(function() { 
          $('#input_update').attr('disabled',false) ;
          $('#input_update').html('<i class="fa fa-save"> </i> Update') ;
        });
        
        
        
                  $("#dataHeader").hide(500)
                  $("#formTemplate").show(500)
        $(".selectpicker").selectpicker("refresh");
    }

    function update() {
        var id = $("#id_template").val();
         var nm_template = $("#nm_template").val();  
        var standart = $("#standart").val(); 
        var tipe = $("#tipe").val(); 
        var produk = $("#produk").val();  
        var request = $("#request").val();  
        var tipe_request = $("#tipe_request").val();  
        if (nm_template == "" && standart == "" && tipe == "" && produk == ""  ) {
                  show_toaster(3, '', "Form harus terisi.")   
            return;
        }
        
        
        var isi_data = "";
        var i = 0;
        isi_data += "header[id]=" + encodeURI($("#id_template").val());
        isi_data += "&header[nm_template]=" + encodeURI($("#nm_template").val());
        isi_data += "&header[standart]=" + encodeURI($("#standart").val());
        isi_data += "&header[tipe]=" + encodeURI($("#tipe").val());
        isi_data += "&header[produk]=" + encodeURI($("#produk").val()); 
        isi_data += "&header[request]=" + encodeURI($("#request").val());   
            isi_data += "&header[tipe_request]=" + encodeURI($("#tipe_request").val());   
         
       
              if(tipe_request==0){  
              var tbl_list = $("#tabel_template tbody");  
                    tbl_list.find('tr').each(function(i, obj) {
                        isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
                        isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html());  
                        isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(5).find('input').val()); 
                        isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(6).find('input').val()); 
                        i++;
                    });   
                }else{
                var tbl_list = $("#tabel_template_quality tbody");  
                    tbl_list.find('tr').each(function(i, obj) {
                        isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
                        isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html());  
                        isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(5).find('input').val()); 
                        isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(6).find('input').val()); 
                        i++;
                    });   
                    
                }
            
        $.post("<?php echo base_url(); ?>coq/template/update", isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                  show_toaster(1, '', data.message)    
                  $("#dataHeader").show()
                  $("#formTemplate").hide()
                TabelData.ajax.reload();
            } else {
                  show_toaster(2, '', data.message)     
            }
        }).fail(function () {
                  show_toaster(2, '', "Gagal mengubah data. Server sedang bermasalah.")     
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Template</b>",
            "message": "Anda yakin ingin menghapus Template \"" + kolom['template'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_template'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/template/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                  show_toaster(1, '', "Berhasil menghapus data.")      
                TabelData.ajax.reload();
            } else {
                  show_toaster(2, '', data.message)    
            }
        }).fail(function () {
                  show_toaster(2, '', "Gagal menghapus data. Server sedang bermasalah.")  
        }).always(function () {
            dialog.setClosable(true);
        });
    }
    
 // $('#tabel_template').on('click', 'tbody .btn-edit', function(){

 //        var _tr = $(this).closest('tr');
 //        var _id = _tr.find('td:eq(0)').text();
        
 //        $('#modal-edit').modal('show');
 //        $('#edit-name').val(_id);
 //        /*console.log(_id);*/
   
        $(function(){
             $('#cara_1').hide();
             $('#cara_2').hide();
             $('#cara_3').hide();
    $('#rumus').change(function(){
        if ($('#rumus option:selected').val() == 'rumus_1') {
            $('#cara_1').show();
            $('#cara_2').hide();
            $('#cara_3').hide();
        }else if($('#rumus option:selected').val() == 'rumus_2'){
             $('#cara_2').show();
             $('#cara_1').hide();
             $('#cara_3').hide();
        }else if($('#rumus option:selected').val() == 'rumus_3'){
             $('#cara_3').show();
             $('#cara_1').hide();
             $('#cara_2').hide();
        }
    });
        });
      
 //   });
 $(function(){
    $('#btn-update').click(function(){
    
      
         /*var _id = $('#edit-name').val();

        $(_tr).find('td:eq(0)').text(_id);*/
        
        $('#modal-edit').modal('hide');
        var cara_1 = 4.071 * ($('#2').val()- $('#3').val()) - 7.600 * $('#5').val() - 6.718 * $('#7').val() - 1.430 * $('#9').val();
        var cara_2 = 4.071 * ($('#12').val()- $('#13').val()) - 7.600 * $('#15').val() - 6.718 * $('#17').val() - 1.430 * $('#19').val() - 2.582 * $('#21').val();
        var cara_3 = 4.071 * $('#32').val() - 7.600 * $('#34').val() - 6.718 * $('#36').val() - 1.430 * $('#38').val();
        /*$("#"+$('#id-name').val()).html($('#edit-name').val());*/
        /*$("#"+$('#id-coba').val()).html($('#name').val());*/
       if ($('#rumus option:selected').val() == 'rumus_1') {
            $("#nilai_result"+$('#id-name').val()).val(cara_1);
        }else if ($('#rumus option:selected').val() == 'rumus_2') {
            $("#nilai_result"+$('#id-name').val()).val(cara_2);
        }else if($('#rumus option:selected').val() == 'rumus_3')  {
           $("#nilai_result"+$('#id-name').val()).val(cara_3); 
        }  
        
        


    


 });
 });

 function updateparam(a, b){
        $('#cara_1').hide();
             $('#cara_2').hide();
             $('#cara_3').hide();
        // var _tr = $(this).closest('tr');
        // var _id = _tr.find('td:eq(0)').text();
        $('#rumus').val('');
        $('#rumus').selectpicker('refresh');

        $('#form-edit')[0].reset();
        $('#modal-edit').modal('show');
        $('#id-name').val(a);
        $('#name_param').val(b);        
        /*$('#edit-name').val(b);*/
        
 }
 
   
  
    </script>
<!-- Dialog "Edit Record" -->
   <div id="modal-edit" class="modal" tabindex="-1" role="dialog">
      <form id="form-edit" action="">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Rumus Perhitungan</h5>
            </div>
            <div class="modal-body">
               <div class="form-group">
                <div class="form-group">
                    <input id="name_param" class="form-control" type="text" name="name" value="" readonly="">
                </div>
                  <label>Rumus: </label>                  
                  <input id="id-name" class="form-control" type="text" name="name" value="" style="display: none;">
                  
                  <select class='form-control selectpicker' data-live-search='true' id="rumus">
                                <option value="" data-hidden='true' selected='selected'></option>
                                <option value='rumus_1'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3</option>
                                <option value='rumus_2'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3 - 2.582 x SO3</option>
                                <option value='rumus_3'>C3S = 4.071 x CaO - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3 </option>
                            </select>
                            <br><br>
                            <div id="cara_1">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="1" class="form-control"  value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="2" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="3" class="form-control" placeholder="FCaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="4" class="form-control" value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="5" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="6" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="7" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="8" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="9" class="form-control" placeholder="Fe2O3"> 
                                   </div>
                                </div>
                            </div>

                            <!-- cara 2 -->
                            <div id="cara_2">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="11" class="form-control" value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="12" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="13" class="form-control" placeholder="FCaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="14" class="form-control" value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="15" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="16" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="17" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="18" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="19" class="form-control" placeholder="Fe2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="20" class="form-control" value="2,582" readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="21" class="form-control" placeholder="SO3"> 
                                   </div>
                                </div>
                            </div>
                            <!-- cara 3 -->
                            
                            <div id="cara_3">
                              <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="31" class="form-control" value='4,071' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="32" class="form-control" placeholder="CaO"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="33" class="form-control"  value='7,600' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   
                                   
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="34" class="form-control" placeholder="SiO2"> 
                                   </div><div class="col-sm-1">-</div></div>
                                   <div class="row">
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="35" class="form-control" value='6,718' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="36" class="form-control" placeholder="Al2O3"> 
                                   </div><div class="col-sm-1">-</div>
                                   <div class="col-sm-2" style="padding-bottom: 5px;">
                                        <input type="text"  id="37" class="form-control" value='1,430' readonly> 
                                   </div><div class="col-sm-1">x</div>
                                   <div class="col-sm-2">
                                        <input type="text"  id="38" class="form-control" placeholder="Fe2O3"> 
                                   </div>
                                </div>
                            </div>
                 <!--  <input id="name" class="form-control" type="text" name="name" value="" required> -->
               </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="btn-update">Save</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
         </div>
      </div>
      </form>
   </div>

