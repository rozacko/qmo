<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Master Plant</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
<!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
<!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
<!--                            Excel-->
<!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Kode Plant</center>  </th> 
                            <th> <center>  Name Plant</center>  </th> 
                            <th> <center>  Company</center>  </th> 
                            <th></th>
                            <th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Product</center> </td> 
                            <td> <center> Name Plant</center> </td> 
                            <td> <center> Company</center> </td> 
                            <td></td> 
                            <td></td> 
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='form-group'>
                    <label>Kode Plant:</label>
                    <input type='hidden' class='form-control input-xs' id='id_plant'>
                    <input type='text' class='form-control input-xs' id='kd_plant'>
                </div> 
                <div class='form-group'>
                    <label>Name Plant:</label> 
                    <input type='text' class='form-control input-xs' id='nm_plant'>
                </div> 
               
                <div class='form-group'>
                    <label>Company:</label>
                    <select class='form-control selectpicker' data-live-search='false' id='company'>
                        <option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>
                    </select>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {


        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Plant', exportOptions: {columns: [0,1,2,3]}},
                // {extend: 'excel',title: 'Master_Plant', exportOptions: {columns: [0,1,2,3]}},
                // {extend: 'pdf',title: 'Master_Plant', exportOptions: {columns:[0,1,2,3]}},
                // {extend: 'print',title: 'Master_Plant', exportOptions: {columns:[0,1,2,3]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 1}, 
                {"visible": false, "targets": 4}, 
                {"visible": false, "targets": 5}, 
                {"orderable": false, "targets": 6},
                {"orderable": false, "targets": 0},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/plant/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        }
                        return_data.push({
                            'id': no,
                            'kd_plant': json[i].KD_PLANT, 
                            'nm_plant': json[i].NM_PLANT, 
                            'company': json[i].NM_COMPANY, 
                            'id_plant': json[i].ID_PLANT, 
                            'id_company': json[i].ID_COMPANY,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'kd_plant'}, 
                {data: 'nm_plant'},
                {data: 'company'},
                {data: 'id_plant'}, 
                {data: 'id_company'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        refreshCompany()
        /// Button Action
        $('#tambah_data').click(function () {

            refreshCompany()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_plant, #kd_plant, #nm_plant, #company, #kelompok, #lokasi").val('');
            $("#dlg").modal("show");
        });

        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>coq/plant/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })


        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });


    });

    function refreshCompany() {
        $.post("<?php echo base_url(); ?>coq/plant/refreshCompany", function (data) {
            $("#company").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
 
    function simpan() {
        var kd_plant = $("#kd_plant").val(); 
        var nm_plant = $("#nm_plant").val(); 
        var company = $("#company").val(); 
        if (nm_plant == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/plant/simpan", {
            "kd_plant": kd_plant,"nm_plant": nm_plant,"company": company, 
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#id_plant").val(kolom['id_plant']);
        $("#nm_plant").val(kolom['nm_plant']);
        $("#kd_plant").val(kolom['kd_plant']);
        $("#company").val(kolom['id_company']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function update() {
        var id = $("#id_plant").val();
        var kd_plant = $("#kd_plant").val(); 
        var nm_plant = $("#nm_plant").val(); 
        var company = $("#company").val(); 
        $('#input_update').attr('disabled', true);
        $('#input_update').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        if (nm_plant == ""
        ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama  harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/plant/update", {
            "id": id,
            "nm_plant": nm_plant,
            "kd_plant": kd_plant,
            "company": company, 
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Plant</b>",
            "message": "Anda yakin ingin menghapus Plant \"" + kolom['nm_plant'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_plant'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/plant/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
