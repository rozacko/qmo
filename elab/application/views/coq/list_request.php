<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }
    .dropzone {
        min-height: 200px;
	margin-top: 100px;
	border: 2px dashed #0087F7; 
    }
    
    .dz-details{
        height : 50px;
    }
    .dz-filename{
        height : 100%;
    }  
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins" id='dataRequest'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> List Request Sales</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
<!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
<!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
<!--                            Excel-->
<!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Kode Request</center>  </th> 
                            <th> <center> Name</center>  </th> 
                            <th> <center> Standart</center>  </th> 
                            <th> <center> Tipe</center>  </th> 
                            <th> <center> Packing</center>  </th> 
                            <th> <center> Product</center>  </th> 
                            <th> <center> Request</center>  </th> 
                            <th> <center> Status</center>  </th> 
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Kode Request</center>  </td> 
                            <td> <center> Name</center>  </td> 
                            <td> <center> Standart</center>  </td> 
                            <td> <center> Tipe</center>  </td> 
                            <td> <center> Packing</center>  </td> 
                            <td> <center> Product</center>  </td> 
                            <td> <center> Request</center>  </td> 
                            <td> <center> Status</center>  </td> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    
    <div class="ibox float-e-margins"  id='detailRequest' style='display:none'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Detail Spec and Quality</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-secondary"  onclick='tombol_back();'  ><span
                                    class="fa fa-arrow-left"></span>&nbsp;Back
                        </button>&nbsp;&nbsp;&nbsp; 
                    </div>
                </div>
            </div>


            <div class="ibox-content"> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Tipe Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_trequest'>Tipe Request</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Nama Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_nrequest'>Nama Request</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_standart'>Standart</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_produk'>Produk</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Tipe Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_tipe'>Tipe Produk</p></div>
                        </div></div> 
                </div> 
           <div id='form_kapal_c'>
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Packing Produk</label><div class="col-lg-1" style=' width : 0px;'><p>:</p></div> 
                            <div class="col-lg-8"><p id='v_packing'>Packing Produk</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Buyer Ident (Trade/perush)</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_buyer'>Buyer Ident (Trade/perush)</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Plant</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_plant'>Plant</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Export Destination</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_country'>Export Destination</p></div>
                        </div></div> 
                </div> 
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Total shipment & shipment size</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_total'>Total shipment & shipment size</p></div>
                        </div></div> 
                </div>  
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Shipment Period Delivery</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_date'>Shipment Period Delivery</p></div>
                        </div></div> 
                </div>  
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">Note</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><textarea name="v_note" id='v_note' style="width:100%" readonly></textarea></div>
                        </div></div> 
                </div>   
                
                <div class="row"  style="height: 0px;">
                    <div class='col-lg-12'>  <div class="form-group row"  style="height: 0px;"><label class="col-lg-3 col-form-label">File</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-8"><p id='v_file'>File</p></div>
                        </div></div> 
                </div>  
                
          </div>
                
                
    <div class="row" id='chatRoom'>
        <div class="col-lg-12">

                <div class="ibox chat-view">

                    <div class="ibox-title">
                        <small class="pull-right text-muted"> </small>
                         Chat room panel
                    </div>


                    <div class="ibox-content">

                        <div class="row"> 
                            <div class="col-md-12 ">
                                <div class="chat-discussion">
                                    <input type='hidden' id='id_request_chat'>
                                    <div id='chatting'> 
                                    <div> 
                                </div>

                            </div> 

                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="chat-message-form"> 
                                    <div class="form-group"> 
                                        <textarea class="form-control message-input" name="message" id="message" placeholder="Enter message text"></textarea>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-12" style='    margin-top: 10px;'> 
                                    <div class="form-group">
                                            <button type="button" class="btn-lg btn-success pull-right" id="add_chat"><span class="fa fa-paper-plane"></span>&nbsp;Kirim</button>
                                    </div> 
                            </div>
                        </div>


                    </div>

                </div>
        </div>

    </div>

                 
            </div>
        </div>
    </div>
    
    
</div>
</div>


    
    <div class="ibox float-e-margins"  id='formTemplate' style='display:none'>
            <div class="ibox-title" > 
                <div class="col-lg-6">
                    <h2 id='title_tipe'><i class="fa fa-list"></i></h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-secondary" onclick='tombol_back();'  ><span
                                    class="fa fa-arrow-left"></span>&nbsp;Back
                        </button>&nbsp;&nbsp;&nbsp; 
                    </div>
                </div>  

            <div class="ibox-content"> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Nama</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5"> 
                            <input type='hidden' class='form-control input-xs' id='id_spec' >
                            <input type='hidden' class='form-control input-xs' id='tipe_request_c' >
                            <input type='text' class='form-control input-xs' id='nm_spec'   >
                             </div>
                        </div></div> 
                </div>  
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Standart</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='standart_c' name="standart" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='false' id='produk_c' name="produk" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Tipe Produk</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                            <select class='form-control selectpicker' data-live-search='true' id='tipe_c' name="tipe" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Request</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5" style='color:black'>
                            <select class='form-control selectpicker' data-live-search='true' id='request_c' name="request" disabled  >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>
                            </select>
                            </div>
                        </div></div> 
                </div> 
                <div class="row">
                    <div class='col-lg-8'> 
                    </div>
                    <div class='col-lg-2'> 
                        <button type='button' class='btn btn-warning btn-md pull-right' onclick='list_template();' ><i
                                    class='fa fa-plus'></i>&nbsp;Add From Template
                        </button>  
                   </div>
                    <div class='col-lg-2'> 
                        <button type='button' class='btn btn-warning btn-md pull-left' onclick='list_parameter();' ><i
                                    class='fa fa-plus'></i>&nbsp;Add Parameter
                        </button>  
                   </div> 
                </div>
                
                
			<div class="row">
                <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%; " id="tabel_template" >
						<thead>
							<tr>
								<th>Parameter</th>
								<th>Simbol</th> 
								<th>Satuan</th> 
								<th>Metode</th> 
								<th>Mark</th> 
								<th style='width:100px'>Nilai Spesifikasi</th> 
								<th style='width:100px'>Nilai Result</th> 
								<th style="width:20px">Hapus</th> 
							</tr>
						</thead>
						<tbody id="isi_tabel_template">
						</tbody>
					</table> 
				</div>
            </div>
                
                 <div class="row">
                            <div class="col-lg-12" > 
                                    <div class="form-group"> 
                                           <button type='button' class='btn btn-primary btn-lg btn-simpan pull-right' onclick='simpan();' id='input_simpan'><i
                                                        class='fa fa-save'></i>&nbsp;Simpan
                                            </button>  
                                            <button type='button' class='btn btn-success btn-lg btn-edit pull-right' onclick='update();' id='input_update'><i
                                                        class='fa fa-save'></i>&nbsp;Update
                                            </button>
                                    </div> 
                            </div>
               </div>
                 
    
        </div>
    </div>
</div>



<div class='modal fade ' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
             
                <div class="row">
                    <div class='col-lg-3'>
                    </div>
                    <div class='col-lg-6'>
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Kapal</label> 
                            <div class="col-lg-10">
                                <select class='form-control input-xs selectpicker' data-live-search='false' id='kapal' name="kapal" > 
                                        <option value='0' selected >Non Kapal</option>
                                        <option value='1' >Kapal</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Nama Request:</label>
                            <input type='hidden' class='form-control input-xs' id='id_request' name="id_request" >
                            <input type='text' class='form-control input-xs' id='nm_request' name="nm_request" >
                        </div> 
                    </div>
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Tipe Request:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='tipe_request' name="tipe_request" >
                                <option value='' data-hidden='true' selected='selected'>-- Tipe Request --</option>
                                <option value='0' >Spec</option>
                                <option value='1' >Quality</option>
                            </select>
                        </div> 
                    </div>
                </div> 
                <div class="row">
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Standart:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='standart' name="standart" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>
                            </select>
                        </div> 
                    </div>
                    <div class='col-lg-6'>  
                        <div class='form-group'>
                            <label>Produk:</label> 
                            <select class='form-control selectpicker' data-live-search='false' id='produk' name="produk" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>
                            </select>
                        </div>
                    </div>  
                </div>  
                <div class="row"> 
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Tipe Produk:</label> 
                            <select class='form-control selectpicker' data-live-search='true' id='tipe' name="tipe" >
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>
                            </select>
                        </div>
                    </div>
                    <div id='form_kapal1'>     
                        <div class='col-lg-6'>
                             <div class='form-group'>
                                <label>Packing Produk:</label> 
                                <select class='form-control selectpicker' data-live-search='false' id='packing' name="packing" >
                                    <option value='' data-hidden='true' selected='selected'>-- Pilih Packing Produk --</option>
                                </select>
                            </div>
                        </div>
                     </div>   
                </div>
                
            <div id='form_kapal2'>     
                <div class="row">
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Buyer ident (Trade/perush):</label> 
                             <input type='text' class='form-control input-xs' id='buyer' name="buyer" >
                        </div> 
                        
                    </div>
                    <div class='col-lg-6'>
                        <div class='form-group'>
                            <label>Plant:</label> 
                            <select class='form-control selectpicker' data-live-search='true' id='plant' name="plant"  multiple>
                                <option value='' data-hidden='true' >-- Pilih Plant --</option>
                            </select>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-6'> 
                        <div class='form-group'>
                            <label>Export Destination:</label> 
                            <select class='form-control selectpicker' data-live-search='true' name="country"  id='country'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Negara --</option>
                            </select>
                        </div>  
                    </div>
                    <div class='col-lg-6'>  
                        <div class='form-group'>
                            <label>Total Shipment & shipment size:</label> 
                             <input type='text' class='form-control input-xs' name="total"  id='total'>
                        </div>
                    </div>
                </div> 
                
                <div class="row">
                    <div class='col-lg-3'>  
                    </div>
                    <div class='col-lg-6'>  
                        <div class='col-lg-6'>
                            <div class='form-group'>
                                <label>Start Period Delivery :</label>  
                                 <input type='text' class='form-control input-xs date-picker' name="start" value='<?php echo date('m-Y') ?>' id='start'>
                            </div>  
                        </div>
                        <div class='col-lg-6'> 
                            <div class='form-group'>
                                <label>End Period Delivery :</label> 
                                 <input type='text' class='form-control input-xs date-picker' value='<?php $startDate = time();
                                       $final= date('m-Y', strtotime('+2 month', $startDate)); echo $final ?>'  name="end"  id='end'>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class='form-group'>
                    <label>Note:</label>
                   <textarea name="note" id='note' style="width:100%;height:150px"></textarea>
                </div>   
                    <div class="dropzone" style='margin-top:10px;'>

                      <div class="dz-message">
                       <center><h3> Drop files here or click to upload.</h3></center>
                      </div>

                    </div>
         </div>
                <!--<div  action="#" class="dropzone" id="dropzoneForm">
                    <div class="fallback">
                        <input name="file" type="file" multiple />
                    </div>
                </div> -->
            </div>
            <div class='modal-footer'> 
               <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>  
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>



<div class='modal fade ' id='dlg_template' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;'> List Template</div>
            </div>
            <div class='modal-body'>
                 <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example" style="font-size:100%;cursor: pointer; " id="tabel_list_template" >
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Nama Template</th>
                                    <th>Standart</th> 
                                    <th>Tipe</th> 
                                    <th>Produk</th>    
                                    <th> </th>    
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table> 
                    </div>
            </div>
               
            </div>
            <div class='modal-footer'>  
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>


<div class='modal fade ' id='dlg_parameter' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                 <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example" style="font-size:100%;cursor: pointer; " id="tabel_list" >
                            <thead>
                                <tr>
                                    <th style='width:20px'>No</th>
                                    <th>Parameter</th>
                                    <th>Simbol</th> 
                                    <th>Satuan</th> 
                                    <th>Mark</th>  
                                    <th>Metode</th>  
                                    <th>Parent</th>  
                                    <th> </th>  
                                    <th> </th>  
                                </tr>
                            </thead>
                            <tbody >
                            </tbody>
                        </table> 
                    </div>
            </div>
               
            </div>
            <div class='modal-footer'>  
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div> 
        </div>
    </div>
</div>


<script>
    var TabelData;
    var tokenFile = [];
    Dropzone.autoDiscover = false;

        var foto_upload= new Dropzone(".dropzone",{
        url: "<?php echo base_url('coq/request/fileUpload') ?>",
        maxFilesize: 2,
        method:"post",
        acceptedFiles:"image/*, .pdf, .docx, .xlsx, .doc, .xls",
        paramName:"userfile",
        dictInvalidFileType:"Type file ini tidak dizinkan",
        addRemoveLinks:true,
        });


        //Event ketika Memulai mengupload
        foto_upload.on("sending",function(a,b,c){
            a.token=Math.random();
            c.append("token_foto",a.token); //Menmpersiapkan token untuk masing masing foto
            tokenFile.push(a.token) 
        });
        
        //Event ketika foto dihapus
        foto_upload.on("removedfile",function(a){
            var token=a.token;
            $.ajax({
                type:"post",
                data:{token:token},
                url:"<?php echo base_url('coq/request/remove_foto') ?>",
                cache:false,
                dataType: 'json',
                success: function(){
                    console.log("Foto terhapus");
                },
                error: function(){
                    console.log("Error");

                }
            });
        });
          $(".date-picker").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months", autoclose: true
        });

    $(document).ready(function () {
         $('#form_kapal1, #form_kapal2').hide();
          
        $('#kapal').change(function () {
            var kapal = $(this).val();
            if(kapal=='0'){ 
                $('#form_kapal1, #form_kapal2').hide();
            }else{
                $('#form_kapal1, #form_kapal2').show(); 
            }
        });
 
        $("#tabel_template tbody").sortable({
          cursor: "move",
          placeholder: "sortable-placeholder",
          helper: function(e, tr)
          {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index)
            {
            // Set helper cell sizes to match the original sizes
            $(this).width($originals.eq(index).width());
            });
            return $helper;
          }
        }).disableSelection(); 
                
                
        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            ],
            "columnDefs": [
                {"visible": false, "targets": 6},    
                {"visible": false, "targets": 9}, 
                {"visible": false, "targets": 10}, 
                {"visible": false, "targets": 11}, 
                {"visible": false, "targets": 12}, 
                {"visible": false, "targets": 13},
                {"visible": false, "targets": 14}, 
                {"visible": false, "targets": 15}, 
                {"visible": false, "targets": 16}, 
                {"visible": false, "targets": 17}, 
                {"visible": false, "targets": 18}, 
                {"visible": false, "targets": 19}, 
                {"visible": false, "targets": 20}, 
                {"visible": false, "targets": 21}, 
                {"visible": false, "targets": 22},
                {"visible": false, "targets": 23}, 
                {"orderable": false, "targets": 24}, 
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/request/get_data',
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.data.length; i++) { 
                    var aksi = '';
                    
                       var request = (json.data[i].TIPE_REQUEST == 0 ? 'SPEC' : 'QUALITY') 
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            if(json.data[i].STATUS=='0' && json.role =='31'){
                                var aksi = '<center> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>' 
                            }else if(json.role !='31'){
                                var baru = (json.data[i].NEW==0 ? 'Create' : 'Update')
                                var warna_button = (json.data[i].NEW==0 ? ' btn-success ' : ' btn-primary ')
                                var aksi = '<center><button onclick="edit(\'' + i + '\')" title="'+baru+' '+request+'" class="btn '+warna_button+' btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button></center>';
                            }
                            // var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        } 
                      
                       if(json.data[i].STATUS==0){
                           var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Submit</span></button>';
                       }else if(json.data[i].STATUS==1){
                           var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Process</span></button>';
                       }else{
                           var status = '<button  class="btn btn-primary btn-xs waves-effect "><span class="btn-labelx">Done</span></button>';
                       }
                       
                       
                       (json.data[i].STATUS == 0 ? ' <button  class="btn btn-primary btn-xs waves-effect "><span class="btn-labelx">Active</span></button>' : ' <button  class="btn btn-danger btn-xs waves-effect "><span class="btn-labelx"> Inactive</span></button>')
                        return_data.push({
                            'id': no,
                            'kd_request'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_request(\'' + i + '\')"><u>'+json.data[i].KD_REQUEST+'</u></a>',  
                            'request'   :   json.data[i].NM_REQUEST,  
                            'standart': json.data[i].NM_STANDART, 
                            'tipe': json.data[i].KD_PRODUCT_TYPE,
                            'packing': json.data[i].NM_PACK,
                            'produk': json.data[i].NM_PRODUCT,
                            'tipe_request': request,
                            'status': "<center>"+status+"</center>",
                            'id_request': json.data[i].ID_REQUEST,
                            'id_standart': json.data[i].ID_STANDART,
                            'id_tipe': json.data[i].ID_PRODUCT_TYPE,
                            'id_packing': json.data[i].ID_PACK,
                            'id_product': json.data[i].ID_PRODUCT,
                            'id_tipe_request': json.data[i].TIPE_REQUEST,
                            'note': json.data[i].NOTE,
                            'country': json.data[i].NM_COUNTRY,
                            'buyer': json.data[i].BUYER,
                            'total_ship': json.data[i].TOTAL,
                            'date': json.data[i].STARTDATE+' s.d '+json.data[i].ENDDATE,
                            'baru': json.data[i].NEW,
                            'id_spec': json.data[i].ID_SPEC,
                            'spec': json.data[i].NM_SPEC,
                            'kapal': json.data[i].IS_KAPAL,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'kd_request'}, 
                {data: 'request'}, 
                {data: 'standart'}, 
                {data: 'tipe'}, 
                {data: 'packing'},
                {data: 'produk'},
                {data: 'tipe_request'},
                {data: 'status'}, 
                {data: 'id_request'}, 
                {data: 'id_standart'},
                {data: 'id_tipe'},
                {data: 'id_packing'},
                {data: 'id_product'},
                {data: 'id_tipe_request'},
                {data: 'note'},
                {data: 'country'},
                {data: 'buyer'},
                {data: 'total_ship'},
                {data: 'date'},
                {data: 'baru'},
                {data: 'id_spec'},
                {data: 'spec'},
                {data: 'kapal'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
        
        

        refreshStandart()
        refreshTipe()
        // refreshPacking()
        refreshProduk()
        // refreshPlant()
        // refreshCountry()
        refreshRequest() 
        /// Button Action
        
        
        
        $('#tambah_data').click(function () {
            
            // var myDropzone = Dropzone.forElement("#dropzoneForm");
            // myDropzone.removeAllFiles(true); 
            tokenFile = [];
            refreshStandart()
            refreshTipe()
            refreshPacking()
            refreshProduk()
            refreshPlant()
            refreshCountry()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_request, #nm_request, #tipe_request, #standart, #tipe, #produk, #note ").val('');
            $("#dlg").modal("show");
        });

        $('#add_chat').click(function () {
            if ($("#message").val() == ""  ) {
                show_toaster(3, '', "Message harus terisi.")
                // informasi(BootstrapDialog.TYPE_WARNING, "Message harus terisi.");
                return;
            }
             $.post("<?php echo base_url(); ?>coq/request/simpanMessage", {
                    "message": $("#message").val() , 
                    "id_request": $("#id_request_chat").val() , 
                    
                }, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.notif == "1") {  
                        refreshChat($("#id_request_chat").val())
                        $("#message").val('') 
                         show_toaster(1, '', 'Chat Terkirim.')
                    } else {
                         show_toaster(2, '', data.message)
                        // informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                     show_toaster(2, '',"Gagal menyimpan data. Server sedang bermasalah.")
                    // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
                }).always(function () {
                });
        }); 
        

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

         $('#tabel_list tbody').on( 'click', 'tr', function () { 
             var tes = ListParameter.row( this ).data();
             var parameter =  tes['parameter'] ;
             var simbol =  tes['simbol'] ;
             var satuan =  tes['satuan'] ;
             var mark =  tes['mark'] ;
             var metode =  tes['metode'] ;
             var id_parameter =  tes['id_parameter'] ; 
             var id_metode =  tes['id_metode'] ; 
             
             $("#tabel_template").find("tbody").append(
                "<tr style='cursor: move;'>"+ 
                
                "<td>"+parameter+"</td>"+
                "<td>"+(simbol==null ? '' : simbol)+"</td>"+
                "<td>"+(satuan==null ? '' : satuan)+"</td>"+  
                "<td>"+(metode==null ? '' : metode)+"</td>"+  
                "<td>"+(mark==null ? '' : mark)+"</td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>"+  
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result'></td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+id_parameter+"</td>"+
                "<td style='display:none;'>"+id_metode+"</td>"+ 
                "</tr>");   
                
                $("#dlg_parameter").modal("hide");
           } );
            $('#tabel_list_template tbody').on( 'click', 'tr', function () { 
             var tes = ListTemplate.row( this ).data(); 
                $.post("<?php echo base_url(); ?>coq/spesification/get_template", {"id_template": tes['id_template']}, function(datas) {
                    var data = JSON.parse(datas);
                     for(var i=0;i< data.length; i++){  
                                $("#tabel_template").find("tbody").append(
                                    "<tr>"+  
                                    "<td style='color:blue'>"+data[i].NAMA_UJI+"</td>"+
                                    "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                                    "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                                    "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                                    "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                                    "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_TEMPLATE==null ? '' : data[i].NILAI_TEMPLATE)+"' id='nilai_template'></td>"+
                                    "<td><input type='text' class='form-control input-xs' style='width: 150px'  id='nilai_result'></td>"+  
                                    "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                                    "<td style='display:none;'> </td>"+
                                    "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                                    "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                                    "</tr>");   
                           
                     }
                         
                         
                }).fail(function() {
                    show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")    
                }).always(function() {  
                });
                
                $("#dlg_template").modal("hide");
           } );


    });

        function tombol_back(){ 
              $("#dataRequest").show(500)
              $("#detailRequest").hide(500) 
              $("#formTemplate").hide(500)
        }
    function detail_request(baris) {
        var kolom = TabelData.row(baris).data();    
        $("#v_trequest").html(kolom['tipe_request']);
        $("#v_nrequest").html(kolom['request']);
        $("#v_standart").html(kolom['standart']);
        $("#v_tipe").html(kolom['tipe']);
        $("#v_packing").html(kolom['packing']);
        $("#v_buyer").html(kolom['buyer']);
        $("#v_country").html(kolom['country']);
        $("#v_total").html(kolom['total_ship']);
        $("#v_date").html(kolom['date']); 
        $("#v_note").html(kolom['note']); 
        $("#v_produk").html(kolom['produk']); 
        if(kolom['kapal']=='0'){
           $("#form_kapal_c").hide()
        }else{
           $("#form_kapal_c").show() 
        }
        
         $("#id_request_chat").val(kolom['id_request']); 
        
        $.post("<?php echo base_url(); ?>coq/request/detail_request", {'id_request' : kolom['id_request']}, function (datas) {
             var data = JSON.parse(datas); 
            $("#v_plant").html(data.plant); 
            $("#v_file").html(data.file); 
            refreshChat($("#id_request_chat").val())
            
             $("#dataRequest").hide('2000')
              $("#detailRequest").show('2000')
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshChat(id_request) {
        $.post("<?php echo base_url(); ?>coq/request/refreshChat", {'id_request' : id_request}, function (datas) {
              var data = JSON.parse(datas); 
                $("#chatting").html(data.chat);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshPacking() {
        $.post("<?php echo base_url(); ?>coq/request/refreshPacking", function (data) {
            $("#packing").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshStandart() {
        $.post("<?php echo base_url(); ?>coq/request/refreshStandart", function (data) {
            $("#standart").html(data); $("#standart_c").html(data);
            
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshTipe() {
        $.post("<?php echo base_url(); ?>coq/request/refreshTipe", function (data) {
            $("#tipe").html(data);
            $("#tipe_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshProduk() {
        $.post("<?php echo base_url(); ?>coq/request/refreshProduk", function (data) {
            $("#produk").html(data);
            $("#produk_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshPlant() {
        $.post("<?php echo base_url(); ?>coq/request/refreshPlant", function (data) {
            $("#plant").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshCountry() {
        $.post("<?php echo base_url(); ?>coq/request/refreshCountry", function (data) {
            $("#country").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    
    function refreshRequest(id_request) {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshRequest", function (data) { 
                $("#request").html(data);
                $("#request_c").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    } 
 
    function simpan() {
        var nm_request = $("#nm_request").val(); 
        var tipe_request = $("#tipe_request").val(); 
        var standart = $("#standart").val(); 
        var tipe = $("#tipe").val(); 
        var produk = $("#produk").val(); 
        var note = $("#note").val(); 
        var buyer = $("#buyer").val(); 
        var country = $("#country").val(); 
        var total = $("#total").val(); 
        var start = $("#start").val(); 
        var end = $("#end").val(); 
        var plant = $("#plant").val();
        var packing = $("#packing").val(); 
        var kapal = $("#kapal").val(); 
        if (nm_request == "" || tipe_request == ""  ||standart == ""  || tipe == ""  ) {
            
            show_toaster(3, '', "Form harus terisi.")
            // informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/request/simpan", {
            "nm_request": nm_request ,
            "tipe_request": tipe_request ,
            "standart": standart ,
            "tipe": tipe ,
            "produk": produk ,
            "note": note , 
            "buyer": buyer , 
            "country": country , 
            "total": total , 
            "start": start , 
            "end": end ,  
            "plant": plant ,
            "packing": packing ,
            "kapal": kapal ,
            "tokenFile": tokenFile ,
            
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                
                show_toaster(1, '', data.message)
                // informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)
                // informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
                show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")
            // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    
        function hapus_role(elm) { 
			var cld = $(elm).parent().parent(); //tr 
            cld.remove(); 
		}
    function edit(baris) { 
        var kolom = TabelData.row(baris).data();    
        $('#isi_tabel_template').html('');
        $("#id_spec").val(kolom['id_spec']);
        $("#nm_spec").val(kolom['spec']); 
        $("#standart_c").val(kolom['id_standart']); 
        $("#tipe_c").val(kolom['id_tipe']);   
        $("#produk_c").val(kolom['id_product']); 
        $("#request_c").val(kolom['id_request']); 
        $("#tipe_request_c").val(kolom['id_tipe_request']); 
        
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
        var baru = (kolom['baru']==0 ? 'Create ' : 'Update ')
        var titel = (kolom['tipe_request']=='SPEC' ? ''+baru+' Spesification' : ''+baru+' Quality')
        $("#title_tipe").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;"+titel+"</b>");
        
                var link1 = (kolom['id_tipe_request']==0 ? 'spesification' : 'quality') 
                var link2 = (kolom['id_tipe_request']==0 ? 'id_spec' : 'id_quality') 
                console.log(link2)
        $.post("<?php echo base_url(); ?>coq/"+link1+"/get_spec", {'id_spec': kolom['id_spec'], 'id_quality': kolom['id_spec'], }, function(datas) {
            var data = JSON.parse(datas);
             for(var i=0;i< data.length; i++){  
                        $("#tabel_template").find("tbody").append(
                            "<tr>"+ 
                            
                            "<td>"+data[i].NAMA_UJI+"</td>"+
                            "<td>"+(data[i].SIMBOL==null ? '' : data[i].SIMBOL)+"</td>"+
                            "<td>"+(data[i].SATUAN==null ? '' : data[i].SATUAN)+"</td>"+  
                            "<td>"+(data[i].NM_METODE==null ? '' : data[i].NM_METODE)+"</td>"+
                            "<td>"+(data[i].MARK==null ? '' : data[i].MARK)+"</td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_STD==null ? '' : data[i].NILAI_STD)+"' id='nilai_template'></td>"+  
                            "<td><input type='text' class='form-control input-xs' style='width: 150px' value='"+(data[i].NILAI_SPEC==null ? '' : data[i].NILAI_SPEC)+"' id='nilai_result'></td>"+  
                            "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                            "<td style='display:none;'> </td>"+
                            "<td style='display:none;'>"+data[i].ID_UJI+"</td>"+
                            "<td style='display:none;'>"+data[i].ID_STANDART_UJI+"</td>"+ 
                            "</tr>");   
                   
             }
                 
                 
        }).fail(function() {
                show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")      
        }).always(function() { 
          $('#input_update').attr('disabled',false) ;
          $('#input_update').html('<i class="fa fa-save"> </i> Update') ;
        });
        
        
        
                  $("#dataRequest").hide(500)
                  $("#formTemplate").show(500)
        $(".selectpicker").selectpicker("refresh");
    }


  
    function update() {
        var id = $("#id_spec").val();
        var nm_spec = $("#nm_spec").val();  
        var standart = $("#standart_c").val(); 
        var tipe = $("#tipe_c").val(); 
        var produk = $("#produk_c").val();  
        var request = $("#request_c").val(); 
        var tipe_request = $("#tipe_request_c").val();  
        if (nm_spec == "" && standart == "" && tipe == "" && produk == ""  ) {
            show_toaster(3, '', "Form harus terisi.")    
            return;
        }
        
        
        var isi_data = "";
        var i = 0;
        isi_data += "header[id]=" + encodeURI($("#id_spec").val());
        isi_data += "&header[nm_spec]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[nm_quality]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[standart]=" + encodeURI($("#standart_c").val());
        isi_data += "&header[tipe]=" + encodeURI($("#tipe_c").val());
        isi_data += "&header[produk]=" + encodeURI($("#produk_c").val()); 
        isi_data += "&header[request]=" + encodeURI($("#request_c").val());    
         
         var tbl_list = $("#tabel_template tbody");  
        tbl_list.find('tr').each(function(i, obj) {
            isi_data += "&isi_data["+i+"][parameter]=" + encodeURI($(this).children().eq(9).html());
            isi_data += "&isi_data["+i+"][standart]=" + encodeURI($(this).children().eq(10).html()); 
            isi_data += "&isi_data["+i+"][nilai]=" + encodeURI($(this).children().eq(5).find('input').val()); 
            isi_data += "&isi_data["+i+"][result]=" + encodeURI($(this).children().eq(6).find('input').val()); 
            i++;
        });   
                var link1 = (tipe_request==0 ? 'spesification' : 'quality') 
                var link2 = (id=='' ? 'simpan' : 'update') 
        $.post("<?php echo base_url(); ?>coq/"+link1+"/"+link2, isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                show_toaster(1, '', data.message)    
                  $("#dataRequest").show()
                  $("#formTemplate").hide()
                  
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)    
            }
        }).fail(function () {
                show_toaster(2, '', "Gagal mengubah data. Server sedang bermasalah.")    
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }


    function list_parameter(){
         $("#dlg_parameter").modal("show");
        ListParameter = $('#tabel_list').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            ],
            "columnDefs": [
                {"visible": false, "targets": 8},  
                {"visible": false, "targets": 7},    
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listParameter',
                data : {
                    'id_standart' : $("#standart_c").val(),
                    'id_produk' : $("#produk_c").val() 
                },
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {   
                        return_data.push({
                            'id': no, 
                            'parameter'   :  json[i].NAMA_UJI,  
                            'simbol': json[i].SIMBOL, 
                            'satuan': json[i].SATUAN, 
                            'mark': json[i].MARK, 
                            'metode': json[i].NM_METODE, 
                            'parent': json[i].NAMA_PARENT, 
                            'id_parameter': json[i].ID_UJI,   
                            'id_metode': json[i].ID_METODE,   
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'parameter'},  
                {data: 'simbol'}, 
                {data: 'satuan'},  
                {data: 'mark'},  
                {data: 'metode'},   
                {data: 'parent'},   
                {data: 'id_parameter'},   
                {data: 'id_metode'},   
            ],
        }); 
    }

    function list_template(){
         $("#dlg_template").modal("show");
        ListTemplate = $('#tabel_list_template').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Metode', exportOptions: {columns: [0,1,2]}},

            ],
            "columnDefs": [
                {"visible": false, "targets": 5},    
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listTemplate',
                data : {
                     
                },
                dataType: 'JSON',
                dataSrc: function (json) { 
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {  
                        return_data.push({
                            'id': no, 
                            'template'   :  json[i].NM_TEMPLATE,  
                            'standart': json[i].NM_STANDART, 
                            'tipe': json[i].KD_PRODUCT_TYPE, 
                            'produk': json[i].NM_PRODUCT, 
                            'id_template': json[i].ID_TEMPLATE_HEADER,  
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'template'},  
                {data: 'standart'}, 
                {data: 'tipe'},  
                {data: 'produk'},  
                {data: 'id_template'},    
            ],
        }); 
    }
    
    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Metode</b>",
            "message": "Anda yakin ingin menghapus Request \"" + kolom['kd_request'] + "\" - \"" + kolom['request'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_request'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/request/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                 show_toaster(1, '', "Berhasil menghapus data.")
                // informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else { 
                 show_toaster(2, '', data.message)
                // informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
                 show_toaster(2, '', "Gagal menghapus data. Server sedang bermasalah.")
            // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
