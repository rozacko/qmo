<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }
    #watermark
{
 position:fixed;
 
 opacity:0.3;
 z-index:99;
 color:#DAA520;
}
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Approval</h2> 
                </div> 
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Name</center>  </th>  
                            <th> <center> Standart</center>  </th> 
                            <th> <center> Type Product</center>  </th> 
                            <th> <center> Type</center>  </th> 
                            <th> <center> Plant</center>  </th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th> 
                            <th></th>  
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Name</center> </td> 
                            <td> <center> Standart</center> </td> 
                            <td> <center> Type Product</center> </td> 
                            <td> <center> Type</center> </td> 
                            <td> <center> Plant</center> </td> 
                            <td></td>  
                            <td></td>  
                            <td></td>  
                            <td></td>  
                            <td></td>   
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true' id="tombol"><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'> 
                            <h2 style='text-align:center;margin-top: -11px;'>Approval New <span id='v_tipe'></span></h2>
                            <h5 style='text-align:center'>No.  <span id='v_id'></span></h5>
                            <hr /> 
                            <input type='hidden' id='id_join'>
                            <input type='hidden' id='id_tipe'>
                            <input type='hidden' id='id_plant'>
                            <input type='hidden' id='id_request'>
                            <table  border="1" style='width:100%;font-family:arial;font-size:12px'>  
                                <tbody>
                                        <tr>
                                            <td colspan='2' height='30px' style='text-align:center;font-weight:bold'>General Information</td> 
                                        </tr>
                                        <tr>
                                            <td width='30%'>Name</td>
                                            <td height='30px' id="v_nama"></td>
                                        </tr>
                                        <tr>
                                            <td>Standart</td>
                                            <td height='30px' id="v_standart"></td>
                                        </tr>
                                        <tr>
                                            <td>Produk</td>
                                            <td height='30px' id="v_produk"></td>
                                        </tr>
                                        <tr>
                                            <td>Tipe</td>
                                            <td height='30px' id="v_tipe1"></td>
                                        </tr>
                                        <tr>
                                            <td>Plant</td>
                                            <td height='30px' id="v_plant"></td>
                                        </tr>
                                        <tr>
                                            <td>Company</td>
                                            <td height='30px' id="v_company"></td>
                                        </tr>
                                </tbody>
                            </table>  
                                        <p>Keterangan :</p>
                                        <div class="row">  <div class="col-lg-12"> 
                                            <textarea name="syarat" id="syarat" style="width:100%"></textarea>
                                        </div>
                                        </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update("1");' id='input_update'><i
                            class='fa fa-thumbs-up'></i>&nbsp;Approve
                </button>
                <button type='button' class='btn btn-danger btn-xs btn-edit' onclick='update("2");'  id='input_update1'><i
                            class='fa fa-thumbs-down'></i>&nbsp;Reject
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>
<div id='detail_spuc'></div>

<!-- Modal -->

        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog" style="width:70% ">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header bg-primary">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Reviewer</h4>
             </div>
             <div class="modal-body form">
                <div >
                    <!-- <h1 style="margin-left: 250px;">Accessed By : </h1>
                    <h1 style="margin-left: 240px;"><?php echo $this->session->userdata("USER")->FULLNAME; ?> </h1>
                    <h1 style="margin-left: 240px;">(<?php echo $this->session->userdata("USER")->EMAIL; ?>)</h1> -->
                    <table id="watermark" style="margin-top: 150px;margin-left: 300px;">
                        <tr><th align="right"><p>Viewed By : </p></th></tr>
                        <tr><th align="right"><p><?php echo $this->session->userdata("USER")->FULLNAME; ?></p></th></tr>
                        <tr><th align="right"><p>(<?php echo $this->session->userdata("USER")->EMAIL; ?>)</p></th></tr>
                        
                    </table>
                </div>
                <form action="" method="POST" id="form">
                    
                  <div class="form-group">
                    <input type="hidden" name="id_join">
                  </div>
                  <div class="form-group">
                    <label>Already the review : </label>
                    <input type="text" name="nmasa" class="form-control" id="masa" readonly="" style="background-color: white;border:none">
                  </div>
                  <div class="form-group">
                    <label>Role : </label>
                    <input type="text" name="role" class="form-control" id="role" readonly="" style="background-color: white;border:none">
                  </div>
                  <div class="form-group">
                    <label>The made for plant : </label>
                    <input type="text" name="plant" class="form-control" id="plant" readonly="" style="background-color: white;border:none">
                  </div>
                  <div class="form-group">
                    <label>Approve date : </label>
                    <input type="text" name="no_Sert" class="form-control" id="no_Sert" readonly="" style="background-color: white;border:none">
                  </div>
                  <div class="form-group">
                    <label>Status : </label>
                    <input type="text"  class="form-control" value="Done Approve" readonly="" style="background-color: white;border:none">
                  </div>

                  
                  
                  </form>

             </div>
             <div class="modal-footer">
              <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
             </div>
            </div>

          </div>
        </div>


<script>
    var TabelData;
    $(document).ready(function () {
    var url = $(location).attr('href'),
    parts = url.split("/"),
    last_part = parts[parts.length-1];
    console.log(last_part)

        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Approval', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Approval', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Approval', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Approval', exportOptions: {columns: [0,1,2]}},

            ],
            "columnDefs": [   
                {"visible": false, "targets": 6},  
                {"visible": false, "targets": 7}, 
                {"visible": false, "targets": 8},  
                {"visible": false, "targets": 9},  
                {"visible": false, "targets": 10},  
                {"orderable": false, "targets": 10}, 
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/approval/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        // if (write_menu == '') {
                            // var aksi = '- no access -';
                        // } else {
                            /*var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-success btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-thumbs-up"></i></span></button> <button onclick="detail(\'' +json[i].ID + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-eye"></i></span></button> </center>'*/
                             var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-success btn-xs waves-effect btn_edit" title="Approve"><span class="btn-labelx"><i class="fa fa-thumbs-up"></i></span></button> <button onclick="informasi_2(\'' + json[i].ID + '\')" class="btn btn-warning btn-xs waves-effect btn_info" title="Information"><span class="btn-labelx"><i class="fa fa-info"></i></span></button></center>'
                        // }
                        return_data.push({
                            'id': no, 
                            'nama': json[i].NAMA, 
                            'standart': json[i].NM_STANDART,
                            'produk': json[i].NM_PRODUCT_TYPE,
                            'tipe': json[i].TIPE,
                            'plant': json[i].NM_PLANT,
                            'id_join': json[i].ID,
                            'kode': json[i].KODE,
                            'id_request': json[i].ID_REQUEST,
                            'id_plant': json[i].ID_PLANT,
                            'company': json[i].NM_COMPANY,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'nama'}, 
                {data: 'standart'}, 
                {data: 'produk'},
                {data: 'tipe'},
                {data: 'plant'},
                {data: 'id_join'},
                {data: 'kode'},
                {data: 'id_request'},
                {data: 'id_plant'},
                {data: 'company'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        refreshStandart()
        /// Button Action
        $('#tambah_data').click(function () {

            refreshStandart()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_standart, #id_metode, #nm_metode").val('');
            $("#dlg").modal("show");
        });

        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>coq/product/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })


        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });


    });

    function refreshStandart() {
        $.post("<?php echo base_url(); ?>coq/approval/refreshStandart", function (data) {
            $("#id_standart").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
 
    function simpan() {
        var nm_metode = $("#nm_metode").val(); 
        var id_standart = $("#id_standart").val(); 
        if (nm_metode == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama harus terisi.");
            return;
        }
        
       $.ajax({
            type: 'GET',
            url: "<?php echo base_url(); ?>coq/approval/simpan",
            success:function(data){
             alert(data);
            }
        }); 
        // $.post("<?php echo base_url(); ?>coq/approval/simpan", {
            // "nm_metode": nm_metode ,
            // "id_standart": id_standart ,
        // }, function (datas) {
            // var data = JSON.parse(datas);
            // if (data.notif == "1") {
                // $("#dlg").modal("hide");
                // informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                // TabelData.ajax.reload();
            // } else {
                // informasi(BootstrapDialog.TYPE_DANGER, data.message);
            // }
        // }).fail(function () {
            // informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        // }).always(function () {
        // });
    }

    function edit(baris) {  
        var kolom = TabelData.row(baris).data(); 
        $("#v_nama").html(kolom['nama']);
        $("#v_standart").html(kolom['standart']); 
        $("#v_produk").html(kolom['produk']); 
        $("#v_tipe, #v_tipe1").html(kolom['tipe']); 
        $("#id_join").val(kolom['id_join']);  
        $("#id_plant").val(kolom['id_plant']);  
        $("#id_request").val(kolom['id_request']);  
        $("#v_id").html(kolom['kode']);  
        $("#v_plant").html(kolom['plant']);  
        $("#v_company").html(kolom['company']);  
        var tipe = (kolom['tipe']=='Specification' ? '0' : '1')
        $("#id_tipe").val(tipe);  
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Approval</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }


        function informasi_2(id){
                
                $('#form')[0].reset();

                $.ajax({
                  url : '<?php echo site_url('coq/approval/informasi'); ?>/'+id,
                  type : 'GET',
                  dataType:'JSON',
                  success:function(data){
                    $('[name="id_join"]').val(data.ID_JOIN);
                    $('[name="no_Sert"]').val(data.TANGGAL);
                    $('[name="role"]').val(data.NAMA_ROLE);
                    $('[name="masa"]').val(data.STATUS);
                    $('[name="nmasa"]').val(data.FULLNAME);
                    $('[name="plant"]').val(data.NM_PLANT);
                    


                    $('#myModal').modal('show');
                    
                  }
                });
              }

    function update(tipe) {
        var id = $("#id_join").val();
        var syarat = $("#syarat").val(); 
        var id_tipe = $("#id_tipe").val();   
        var id_plant = $("#id_plant").val(); 
        var id_request = $("#id_request").val();   
        if (syarat == "" && tipe == '2'
        ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Keterangan  harus terisi.");
            return;
        }
        $('#input_update, #input_update1,#tombol,.btn-tutup').attr('disabled', true);
        $('#input_update, #input_update1,.btn-tutup').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        $.post("<?php echo base_url(); ?>coq/approval/update", {
            "id": id,
            "syarat": syarat, 
            "id_tipe": id_tipe,  
            "tipe": tipe,  
            "id_plant": id_plant,  
            "id_request": id_request,  
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil melakukan Approval dan mengirim Email.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {

            $('#input_update').html("<i class='fa fa-thumbs-up'></i>&nbsp;Approve");
            $('#input_update1').html("<i class='fa fa-thumbs-down'></i>&nbsp;Reject");
            $('#input_update, #input_update1').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Metode</b>",
            "message": "Anda yakin ingin menghapus Metode \"" + kolom['metode'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_metode'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/approval/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }
    function detail(id) {
        $.post("<?php echo base_url(); ?>coq/approval/get_detail1", {"id": id}, function (data) {
            $("#detail_spuc").html(data); 
            $("#dlg_detail").modal("show"); 
        }).fail(function() {
            // Nope
        }).always(function() {
            // $(".selectpicker").selectpicker("refresh");
        });
    } 
</script>
