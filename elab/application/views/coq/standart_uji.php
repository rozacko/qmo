<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Master Standart Uji</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
<!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
<!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
<!--                            Excel-->
<!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'> <center> No</center>  </th>
                            <th> <center> Parameter</center>  </th> 
                            <th> <center> Metode</center>  </th> 
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>  <center></center> </th>
                            <td> <center> Parameter</center> </td> 
                            <td> <center> Metode</center> </td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <th style='widtd:120px;'> <center></center>  </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='form-group'>
                    <label>Name Metode:</label>
                    <input type='hidden' class='form-control input-xs' id='id_standart_uji'> 
                    <select class='form-control selectpicker' data-live-search='true' id='parameter' data-max-options="10">
                        <option value='' data-hidden='true' selected='selected'>-- Pilih Parameter --</option>
                    </select>
                </div> 
                <div class='form-group'>
                    <label>Metode:</label> 
                    <select class='form-control selectpicker' data-live-search='true' id='metode' data-max-options="10">
                        <option value='' data-hidden='true' selected='selected'>-- Pilih Metode --</option>
                    </select>
                </div> 
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {


        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Standart_Uji', exportOptions: {columns: [0,1,2]}},
                // {extend: 'excel',title: 'Master_Standart_Uji', exportOptions: {columns: [0,1,2]}},
                // {extend: 'pdf',title: 'Master_Standart_Uji', exportOptions: {columns: [0,1,2]}},
                // {extend: 'print',title: 'Master_Standart_Uji', exportOptions: {columns: [0,1,2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 3}, 
                {"visible": false, "targets": 4}, 
                {"visible": false, "targets": 5}, 
                {"orderable": false, "targets": 6}, 
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/standart_uji/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        }
                        return_data.push({
                            'id': no,
                            'parameter': json[i].NAMA_UJI, 
                            'metode': json[i].NM_METODE, 
                            'id_standart_uji': json[i].ID_STANDART_UJI,
                            'id_parameter': json[i].ID_UJI,
                            'id_metode': json[i].ID_METODE,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'parameter'}, 
                {data: 'metode'}, 
                {data: 'id_standart_uji'},
                {data: 'id_parameter'},
                {data: 'id_metode'},
                {data: 'aksi'},
            ],
        });


        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        refreshParameter()
        refreshMetode()
        /// Button Action
        $('#tambah_data').click(function () {

                refreshParameter()
                refreshMetode()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#parameter, #id_standart_uji, #metode").val('');
            $("#dlg").modal("show");
        });

        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>coq/product/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })


        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });


    });

    function refreshParameter() {
        $.post("<?php echo base_url(); ?>coq/standart_uji/refreshParameter", function (data) {
            $("#parameter").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
    function refreshMetode() {
        $.post("<?php echo base_url(); ?>coq/standart_uji/refreshMetode", function (data) {
            $("#metode").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }
 
    function simpan() {
        var id_parameter = $("#parameter").val(); 
        var id_metode = $("#metode").val(); 
        if (id_parameter == "" ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/standart_uji/simpan", {
            "id_parameter": id_parameter ,
            "id_metode": id_metode ,
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#id_standart_uji").val(kolom['id_standart_uji']);
        $("#metode").val(kolom['id_metode']); 
        $("#parameter").val(kolom['id_parameter']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function update() {
        var id = $("#id_standart_uji").val();
        var id_parameter = $("#parameter").val(); 
        var id_metode = $("#metode").val();
        $('#input_update').attr('disabled', true);
        $('#input_update').html('<i class="fa fa-spinner fa-spin" ></i> Processing'); 
        if (id_parameter == "" || id_metode == '') {
            informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/standart_uji/update", {
            "id": id,
            "id_parameter": id_parameter, 
            "id_metode": id_metode, 
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Standart Uji</b>",
            "message": "Anda yakin ingin menghapus Standart Uji \"" + kolom['parameter'] + "\" \"" + kolom['metode'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_standart_uji'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/standart_uji/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                TabelData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
