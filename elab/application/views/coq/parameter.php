<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-top: 0.2em;
        margin-right: 0.5em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">


            <div class="">
                <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Parameter Uji</h2>
                </div>
                <div class="col-md-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success  btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Tambah
                        </button>&nbsp;&nbsp;&nbsp;
                        <!-- <button type="button" id='btn_xls' class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel </button> -->
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                               style="font-size:95%" id="tabel" width="100%">
                            <thead>
                            <tr>
                                <th style='width:10px;'>No</th>
                                <th>Parameter Uji</th>
                                <th>Symbol</th>
                                <th>Kategori</th>
                                <th>Product</th>
                                <th>Parent</th>
                                <th>Satuan</th>
                                <th>Mark</th> 
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style='width:120px;'>
                                    <center>Aksi <span class="fa fa-filter pull-right" data-filtering="1"
                                                       id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                    </center>
                                </th>
                            </tr>
                            <tr id="filtering">
                                <th style='widtd:10px;'></th>
                                <td>Parameter Uji</td>
                                <td>Symbol</td>
                                <td>Kategori</td>
                                <td>Product</td>
                                
                                <td>Parent</td> 
                                <td>Satuan</td> 
                                <td>Mark</td> 
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <th style='widtd:120px;'></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='row'>
                    <div class='col-md-4'>
                         <div class='form-group'>
                            <label>Produk:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='produk'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>
                            </select>
                        </div>  
                    </div>
                    <div class='col-md-4'>
                         <div class='form-group'>
                            <label>Tipe:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='tipe'>
                                <option value='0' selected='selected'>Parent</option>
                                <option value='1'>Sub Parameter</option>
                            </select>
                        </div> 
                    </div>
                    <div class='col-md-4'>
                        <div class='form-group' id='form_existing'>
                            <label>Parent Parameter:</label>
                            <select class='form-control selectpicker' data-live-search='true' id='existing' style='line-height: 17px !important;'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Parameter --</option>
                            </select>
                        </div> 
                    </div>
                </div>
                    
                             <div class='form-group' >
                                <label>Parameter:</label>
                                <input type='hidden' class='form-control input-xs' id='id_parameter'>
                                <input type='text' class='form-control input-xs' id='nm_parameter'>
                            </div>
                       
                             <div class='form-group' >
                                <label>Symbol:</label> 
                                <input type='text' class='form-control input-xs' id='simbol'>
                            </div> 
                            
                             <div class='form-group'>
                                <label>Kategori:</label>
                                <select class='form-control selectpicker' data-live-search='false' id='kategori'>
                                    
                                    <option value='1'>Kimia</option>
                                    <option value='2'>Fisika</option>
                                </select>
                            </div> 

                             <div class='form-group' >
                                <label>Satuan:</label> 
                                <input type='text' class='form-control input-xs' id='satuan'>
                            </div> 
                       
                             <div class='form-group' >
                                <label>Mark:</label> 
                                <input type='text' class='form-control input-xs' id='mark'>
                            </div> 
                     
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {

        $('#tabel thead td').each(function (i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="' + title + '" data-index="' + (i + 1) + '" />');
        });
        tabel_view()
        refreshExisting()
        refreshProduk()
        refreshComponent()

        /// Button Action
        $('#tambah_data').click(function () {
            refreshExisting()
            refreshProduk()
            refreshComponent()
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#form_insert").show();
            $('#form_existing').hide()
            $("#tipe").val('0')
            $("#component ,#existing ,#produk, #tipe, #id_parameter, #nm_parameter, #simbol, #satuan, #mark,#kategori").val('');
            $("#dlg").modal("show");
        });

        $('#tipe').change(function () {
            var tipe = $('#tipe').val();
            if (tipe == '0') {
                $('#form_uji').show()
                $('#form_existing').hide()
                $("#component ,#existing ,#id_uji, #kd_uji, #nm_uji, #company, #kategori, #biaya, #satuan, #simbol").val('');
            } else {
                $('#form_uji').hide()
                $('#form_existing').show()
            }

        });

       


        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>coq/parameter/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

    });

            $('#produk').change(function() {   
             refreshExisting($("#produk option:selected").val()) 
          });  
    function refreshComponent() {
        $.post("<?php echo base_url(); ?>coq/parameter/refreshComponent", function (data) {
            $("#component").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshExisting(id) {
        $.post("<?php echo base_url(); ?>coq/parameter/refreshExisting", {"id": id}, function (data) {
            $("#existing").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshProduk() {
        $.post("<?php echo base_url(); ?>coq/parameter/refreshProduk", function (data) {
            $("#produk").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function tabel_view() {
        TabelData = $('#tabel').DataTable({
            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4,5,6 ]}},
                // {extend: 'excel',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4,5,6 ]}},
                // {extend: 'pdf',title: 'Master_Parameter_uji', exportOptions: {columns:  [0,1,2,3,4,5,6 ]}},
                // {extend: 'print',title: 'Master_Parameter_uji', exportOptions: {columns:[0,1,2,3,4,5,6 ]}},
            // ],
            "columnDefs": [
                {"visible": false, "targets": 8},
                {"visible": false, "targets": 9},
                {"visible": false, "targets": 10}, 
                {"visible": false, "targets": 11}, 
                {"orderable": false, "targets": 12},
            ],
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "orderCellsTop": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            // "sDom": 'Rfrtlip',
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "fixedColumns": {
                "leftColumns": 0,
                "rightColumns": 1
            },
            // "scrollY": '50vh',
            "scrollCollapse": true, 
            columns: [
                {data: 'no'},
                {data: 'param'},
                {data: 'simbol'},
                {data: 'kategori'},
                {data: 'produk'},
                {data: 'parent'},
                {data: 'satuan'},
                {data: 'mark'}, 
                {data: 'id_param'},
                {data: 'id_produk'},
                {data: 'id_parent'},
                {data: 'id_kategori'},
                {data: 'aksi'},
            ],
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>coq/parameter/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        // if(json[i].NAMA_UJI=='Ya'){
                        // var component = json[i].NM_COMPONENT;
                        // }else{
                        // var component = json[i].NM_COMPONENT+" "+json[i].NAMA_UJI;
                        // }
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect  btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        } 
                        return_data.push({
                            'no': no,
                            'param': json[i].NAMA_UJI,
                            'simbol': json[i].SIMBOL,
                            'kategori': json[i].NM_KATEGORI,
                            'produk': json[i].NM_PRODUCT,
                            'parent': json[i].PARENT_PARAMETER  ,
                            'satuan': json[i].SATUAN,
                            'mark': json[i].MARK, 
                            'id_param': json[i].ID_UJI,
                            'id_produk': json[i].ID_PRODUCT,
                            'id_parent': json[i].PARENT,
                            'id_kategori': json[i].ID_KATEGORI,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            }
        });
        // Apply the search
        TabelData.columns().every(function () {
            var that = this;

            $('input', this.header()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });
    }


    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#id_parameter").val(kolom['id_param']); 
        $("#produk").val(kolom['id_produk']); 
        $("#simbol").val(kolom['simbol']);
        
        if(kolom['id_parent']==null){
            var parent_menu = '';
            var tipe = '0';
            $("#form_existing").hide()
        }else{
             var parent_menu = kolom['id_parent'];
            var tipe = '1';
            $("#form_existing").show()
        } 
        $("#tipe").val(tipe); 
        $("#existing").val(parent_menu); 
        $("#nm_parameter").val(kolom['param']); 
        $("#satuan").val(kolom['satuan']); 
        $("#mark").val(kolom['mark']); 
         $("#kategori").val(kolom['id_kategori']);  
        // $("#unit_sni").val(kolom['unit_sni']); 
        // $("#test_astm").val(kolom['test_astm']); 
        // $("#test_en").val(kolom['test_en']); 
        // $("#test_sni").val(kolom['test_sni']); 
        // $("#mark_astm").val(kolom['mark_astm']); 
        // $("#mark_en").val(kolom['mark_en']); 
        // $("#mark_sni").val(kolom['mark_sni']);  
        
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function simpan() {
        var produk = $("#produk").val();
        var tipe = $("#tipe").val();
        var existing = $("#existing").val();
        var parameter = $("#nm_parameter").val();
        var simbol = $("#simbol").val();
        var kategori = $("#kategori").val();
        var satuan = $("#satuan").val();
        var mark = $("#mark").val();
        // var unit_si = $("#unit_si").val();
        // var unit_en = $("#unit_en").val();
        // var unit_sni = $("#unit_sni").val();
        // var test_astm = $("#test_astm").val();
        // var test_en = $("#test_en").val();
        // var test_sni = $("#test_sni").val();
        // var mark_astm = $("#mark_astm").val();
        // var mark_en = $("#mark_en").val();
        // var mark_sni = $("#mark_sni").val();  
        if (parameter == "" || produk == "" ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Paremeter Uji dan Produk harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/parameter/simpan", { 
            "produk": produk,
            "tipe": tipe,
            "existing": existing,
            "parameter": parameter,
            "simbol": simbol,
            "kategori": kategori,
            "satuan": satuan,
            "mark": mark,
            // "unit_sni": unit_sni,
            // "test_astm": test_astm,
            // "test_en": test_en,
            // "test_sni": test_sni,
            // "mark_astm": mark_astm,
            // "mark_en": mark_en,
            // "mark_sni": mark_sni,
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function update() {
        var id = $("#id_parameter").val();
        var produk = $("#produk").val();
        var tipe = $("#tipe").val();
        var existing = $("#existing").val();
        var parameter = $("#nm_parameter").val();
        var simbol = $("#simbol").val();
        var satuan = $("#satuan").val();
        var mark = $("#mark").val();
        var kategori = $("#kategori").val();
 
         if (parameter == "" || produk == "" ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Produk dan Paremeter Uji harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>coq/parameter/update", {
            "id": id,
             "produk": produk,
            "tipe": tipe,
            "existing": existing,
            "parameter": parameter,
            "simbol": simbol,
            "kategori": kategori,
            "satuan": satuan,
            "mark": mark, 
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Parameter</b>",
            "message": "Anda yakin ingin menghapus Parameter Uji \"" + kolom['param'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Hapus",
                "action": function (dialog) {
                    hapus(kolom['id_param'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/parameter/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
