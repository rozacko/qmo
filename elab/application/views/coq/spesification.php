<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

    .dropzone {
        margin-top: 100px;
        border: 2px dashed #0087F7;
    }

    #tabel_list.dataTable tbody tr:hover {
        background-color: #ffa;
    }

    #tabel_list.dataTable tbody tr:hover > .sorting_1 {
        background-color: #ffa;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins" id='dataHeader'>
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> List Specification</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <?php $sess_user = $this->session->userdata("USERLAB");
                    $session_role = '';
                    foreach ($sess_user as $v) {
                        $session_role .= $v['ID_ROLE'] . ",";
                    }
                    $role = rtrim($session_role, ",");
                    
                     if ($role == "30" || $role == "38") {
                            ?>
                            <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Add
                        </button>&nbsp;&nbsp;&nbsp;
                            <?php
                        }else{
                            ?>
                            
                            <?php
                        } ?>
                        
                        <!--                        <button type="button" id='btn_xls' class="btn-sm btn-warning"-->
                        <!--                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export-->
                        <!--                            Excel-->
                        <!--                        </button>-->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:95%" id="tabel" width="100%">
                        <thead>
                        <tr>
                            <th style='width:30px;'>
                                <center> No</center>
                            </th>
                            <th>
                                <center> Kode</center>
                            </th>
                            <th>
                                <center> Spec</center>
                            </th>
                            <th>
                                <center> Standart</center>
                            </th>
                            <th>
                                <center> Type</center>
                            </th>
                            <th>
                                <center> Product</center>
                            </th>
                            <th>
                                <center> Plant</center>
                            </th>
                            <th>
                                <center> Brand</center>
                            </th>
                            <th>
                                <center> Request</center>
                            </th>
                            <th>
                                <center> Status</center>
                            </th>
                            <th>
                                <center> Formulas</center>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
							<th></th>
                            <th style='width:120px;'>
                                <center> Action <span class="fa fa-filter pull-right" data-filtering="1"
                                                      id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                </center>
                            </th>
                        </tr>
                        <tr id="filtering">
                            <th style='widtd:30px;'>
                                <center></center>
                            </th>
                            <td>
                                <center> Kode</center>
                            </td>
                            <td>
                                <center> Template</center>
                            </td>
                            <td>
                                <center> Standart</center>
                            </td>
                            <td>
                                <center> Type</center>
                            </td>
                            <td>
                                <center> Product</center>
                            </td>
                            <td>
                                <center> Plant</center>
                            </td>
                            <td>
                                <center> Brand</center>
                            </td>
                            <td>
                                <center> Request</center>
                            </td>
                            <td>
                                <center> Status</center>
                            </td>
                            <th>
                                <center> Formulas</center>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
							<th></th>
                            <th style='widtd:120px;'>
                                <center></center>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="ibox float-e-margins" id='formTemplate' style='display:none'>
        <div class="ibox-title">
            <div class="col-lg-6">
                <h2><i class="fa fa-list"></i> Specification</h2>
            </div>
            <div class="col-lg-6">
                <div style="text-align:right;">
                    <button type="button" class="btn-sm btn-secondary" id='tombol_back'><span
                                class="fa fa-arrow-left"></span>&nbsp;Back
                    </button>&nbsp;&nbsp;&nbsp;
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Name Specification</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <input type='hidden' class='form-control input-xs' id='id_spec'>
                                <input type='text' class='form-control input-xs' id='nm_spec'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Standart</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='false' id='standart'
                                        name="standart">
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Standart --
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Type Product</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='true' id='tipe' name="tipe">
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Type Product --
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Product</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='false' id='produk'
                                        name="produk">
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Product --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Plant</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='true' id='plant'
                                        name="plant">
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Plant --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class='col-lg-6'>
                        <div class="form-group row"><label class="col-lg-6 col-form-label">Date Produce</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <input type='text' style='background-color:white' class='form-control input-xs'
                                       id="start">
                            </div>
                        </div>
                    </div>
                    <div class='col-lg-6'>
                        <div class="form-group row"><label class="col-lg-1 col-form-label">To</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <input type='text' style='background-color:white' class='form-control input-xs'
                                       id="end">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style='display:none'>
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Request</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='true' id='request'
                                        name="request">
                                    <option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Brands</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='true' id='brand'
                                        name="brand">
                                    <option value='' data-hidden='true' selected='selected'>-- Choose Brands --</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Formulas</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <textarea class="form-control" rows="7" id="formulas"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class='col-lg-12'>
                        <div class="form-group row"><label class="col-lg-3 col-form-label">Print Page</label>
                            <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                            <div class="col-lg-5">
                                <select class='form-control selectpicker' data-live-search='true' id='prints'
                                        name="prints">
                                    <option value='1' selected='selected'>1</option>
                                    <option value='2'>2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="vessel_name_2">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Vessel Name</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='vessel_name'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="bill_number">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Bill Of Lading Number</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='bill_number_2'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="bill_date">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Bill Of Lading Date</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='bill_date_2'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="port_loading">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Port Of Loading</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='port_loading_2'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="port_discharge">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Port Of Discharge</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='port_discharge_2'   >
                                 </div>
                            </div></div> 
                    </div> 
                    <div class="row" id="desc_sample">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Description Sample</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='desc_sample_2'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="notify_party">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Notify Party</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='notify_party_2'   >
                                 </div>
                            </div></div> 
                    </div>  
                    <div class="row" id="quantity">
                        <div class='col-lg-12'>  <div class="form-group row"><label class="col-lg-3 col-form-label">Quantity</label> <div class="col-lg-1" style=' width : 0px;'><p>:</p></div>
                                <div class="col-lg-5"> 
                                <input type='text' class='form-control input-xs' id='quantity_1'   >
                                 </div>
                            </div></div> 
                    </div>
                    


            </div>
            <div class="row">
                <div class='col-lg-8'>
                </div>

                <div class='col-lg-2'>
                    <button type='button' class='btn btn-warning btn-md pull-right' onclick='list_template();'><i
                                class='fa fa-plus'></i>&nbsp;Add From Template
                    </button>
                </div>
                <div class='col-lg-2'>
                    <button type='button' class='btn btn-warning btn-md pull-left' onclick='list_parameter();'><i
                                class='fa fa-plus'></i>&nbsp;Add Parameter
                    </button>
                </div>
            </div>


            <div class="row">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example"
                           style="font-size:100%; " id="tabel_template">
                        <thead>
                        <tr>
                            <th>Parameter</th>
                            <th>Simbol</th>
                            <th>Satuan</th>
                            <th>Metode</th>
                            <th>Mark</th>
                            <th style='width:100px'>Nilai Spesifikasi</th>
                            <th style='width:100px'>Nilai Result</th>
                            <th style="width:20px">Hapus</th>
                        </tr>
                        </thead>
                        <tbody id="isi_tabel_template">
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <button type='button' class='btn btn-primary btn-lg btn-simpan pull-right'
                                onclick='simpan();' id='input_simpan'><i
                                    class='fa fa-save'></i>&nbsp;Simpan
                        </button>
                        <button type='button' class='btn btn-success btn-lg btn-edit pull-right' onclick='update();'
                                id='input_update'><i
                                    class='fa fa-save'></i>&nbsp;Update
                        </button>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i
                            class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example"
                               style="font-size:100%;cursor: pointer; " id="tabel_list">
                            <thead>
                            <tr>
                                <th style='width:20px'>No</th>
                                <th>Parameter</th>
                                <th>Simbol</th>
                                <th>Satuan</th>
                                <th>Mark</th>
                                <th>Metode</th>
                                <th>Parent</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<div class='modal fade ' id='dlg_template' role='dialog' aria-hidden='true' data-backdrop='static'
     data-keyboard='false'>
    <div class='modal-dialog  modal-lg'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i
                            class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;'> List Template</div>
            </div>
            <div class='modal-body'>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered dataTables-example"
                               style="font-size:100%;cursor: pointer; " id="tabel_list_template">
                            <thead>
                            <tr>
                                <th style='width:20px'>No</th>
                                <th>Nama Template</th>
                                <th>Standart</th>
                                <th>Tipe</th>
                                <th>Produk</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>

<div id='detail_spuc'></div>
<script>

    <?php
    $sess_user = $this->session->userdata("USERLAB");
    $session_role = '';
    foreach ($sess_user as $v) {
        $session_role .= $v['ID_ROLE'] . ",";
    }
    $role = rtrim($session_role, ",");
    ?>

    var roleUser = <?php echo $role ?>;

    var TabelData;

    $(".date-picker").datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months", autoclose: true
    });

    /*$('#expired').daterangepicker({
      opens: 'left',
    }, function(start, end, label) {
        $('#expired').val(start.format('DD-MM-YYYY')+' - '+end.format('DD-MM-YYYY'))
      // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    }); */


    $(document).ready(function () {
        $("#quantity").hide();
        $("#desc_sample").hide();
        $("#notify_party").hide();
        $("#port_discharge").hide();
        $("#port_loading").hide();
        $("#bill_date").hide();
        $("#bill_number").hide();
        $("#vessel_name_2").hide();
        $(function(){
            $('#prints').change(function(data){
            if ($('#prints option:selected').val() == '1') {
        $("#quantity").hide();
        $("#desc_sample").hide();
        $("#notify_party").hide();
        $("#port_discharge").hide();
        $("#port_loading").hide();
        $("#bill_date").hide();
        $("#bill_number").hide();
        $("#vessel_name_2").hide();
            }else if($('#prints option:selected').val() == '2'){
        $("#quantity").show();
        $("#desc_sample").show();
        $("#notify_party").show();
        $("#port_discharge").show();
        $("#port_loading").show();
        $("#bill_date").show();
        $("#bill_number").show();
        $("#vessel_name_2").show();
            }
        });
        });

        if (roleUser == 31) {
            $("#tambah_data").hide()
        }

        $("#tabel_template tbody").sortable({
            cursor: "move",
            placeholder: "sortable-placeholder",
            helper: function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                });
                return $helper;
            }
        }).disableSelection();


        TabelData = $('#tabel').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'excel', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'pdf', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'print', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},

            // ],
            "columnDefs": [

                {"visible": false, "targets": 10},
                {"visible": false, "targets": 11},
                {"visible": false, "targets": 12},
                {"visible": false, "targets": 13},
                {"visible": false, "targets": 14},
                {"visible": false, "targets": 15},
                {"visible": false, "targets": 16},
                {"visible": false, "targets": 17},
                {"visible": false, "targets": 18},
				{"visible": false, "targets": 19},
                {"orderable": false, "targets": 20},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        var edit = "style='display:none'";
                        var download = "style='display:none'";
                        var hapus = "style='display:none'";
                        if (json[i].STATUS == 0) {
                            var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Request Sales</span></button>';
                        } else if (json[i].STATUS == 1) {
                            if (json[i].ROLE == 31 || json[i].ROLE == 32 || json[i].ROLE == 34 || json[i].ROLE == 35 ) {
                                var edit = "style='display:none'";
                                var hapus = "style='display:none'";
                            } else {
                                if (json[i].ROLE == 30) {
                                     var edit = '';
                                    var hapus = "style='display:none'";
                                }else if(json[i].ROLE == 38){
                                    var edit = '';
                                    var hapus = "";
                                }
                                
                            }
                            var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Create Admin</span></button>';
                        } else if (json[i].STATUS == 11) {
                            var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Admin</span></button>';
								if (json[i].ROLE == 31 || json[i].ROLE == 32 || json[i].ROLE == 34 || json[i].ROLE == 35) {
                                var edit = "style='display:none'";
                                var hapus = "style='display:none'";
                            } else {
                                var edit = '';
                                var hapus = "style='display:none'";
                            }
                        } else if (json[i].STATUS == 2) {
                            var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Approve 1</span></button>';
                        } else if (json[i].STATUS == 21) {
                            var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 1</span></button>';
                             if (json[i].ROLE == 31 || json[i].ROLE == 32 || json[i].ROLE == 34 || json[i].ROLE == 35) {
                                var edit = "style='display:none'";
                                var hapus = "style='display:none'";
                            } else {
                                var edit = '';
                                var hapus = "style='display:none'";
                            }
                        } else if (json[i].STATUS == 3) {
                            var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Approve 2</span></button>';
                        } else if (json[i].STATUS == 31) {
                            var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 2</span></button>';
                             if (json[i].ROLE == 31 || json[i].ROLE == 32 || json[i].ROLE == 34 || json[i].ROLE == 35) {
                                var edit = "style='display:none'";
                                var hapus = "style='display:none'";
                            } else {
                                var edit = '';
                                var hapus = "style='display:none'";
                            }
                        } else if (json[i].STATUS == 4) {
                            var status = '<button  class="btn btn-success btn-xs waves-effect "><span class="btn-labelx">Done</span></button>';
                            var download = '';
                        } else if (json[i].STATUS == 41) {
                            var status = '<button  class="btn btn-warning btn-xs waves-effect "><span class="btn-labelx">Reject Approve 3</span></button>';
                             if (json[i].ROLE == 31 || json[i].ROLE == 32 || json[i].ROLE == 34 || json[i].ROLE == 35) {
                                var edit = "style='display:none'";
                                var hapus = "style='display:none'";
                            } else {
                                var edit = '';
                                var hapus = "style='display:none'";
                            }
                        }

                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" ' + edit + ' class="btn btn-warning btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus" ' + hapus + ' ><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button> <button class="btn btn-primary btn-xs waves-effect" ' + download + ' onclick="print(\'' + json[i].ID_SPEC_HEADER + '\')" title="Print"><span class="fa fa-print"></span> </button></center>'
                            // var aksi = '<center><button onclick="edit(\'' + i + '\')" ' + edit + ' class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus" ' + hapus + ' ><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button> <button class="btn btn-primary btn-xs waves-effect" ' + download + ' onclick="print(\'' + json[i].ID_SPEC_HEADER + '\')" title="Print"><span class="fa fa-print"></span> </button><button class="btn btn-success btn-xs waves-effect" title="View"><span class="fa fa-eye"></span></button></center>'
                        }


                        return_data.push({
                            'id': no,
                            'kode': '<u><a onclick="detail_spec(\'' + json[i].ID_SPEC_HEADER + '\')" class="btn-labelx success" title="Click For Detail">' + json[i].KD_SPEC + ' </a></u> ',
                            //'kode'   :  json[i].KD_SPEC,
                            'spec': json[i].NM_SPEC,
                            'standart': json[i].NM_STANDART,
                            'tipe': json[i].KD_PRODUCT_TYPE,
                            'plant': json[i].NM_PLANT,
                            'produk': json[i].NM_PRODUCT,
                            'brand': json[i].NM_BRAND,
                            'request': json[i].NM_REQUEST,
                            'status': "<center>" + status + "</center>",
                            'id_spec': json[i].ID_SPEC_HEADER,
                            'id_standart': json[i].ID_STANDART,
                            'id_tipe': json[i].ID_PRODUCT_TYPE,
                            'id_product': json[i].ID_PRODUCT,
                            'id_request': json[i].ID_REQUEST,
                            'id_plant': json[i].ID_PLANT,
                            'start': json[i].START_PRODUCE,
                            'end': json[i].END_PRODUCE,
                            'formulas': json[i].FORMULAS,
							'id_brand': json[i].ID_BRAND,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'kode'},
                {data: 'spec'},
                {data: 'standart'},
                {data: 'tipe'},
                {data: 'produk'},
                {data: 'plant'},
                {data: 'brand'},
                {data: 'request'},

                {data: 'status'},
                {data: 'id_spec'},
                {data: 'id_standart'},
                {data: 'id_tipe'},
                {data: 'id_product'},
                {data: 'id_request'},
                {data: 'id_plant'},
                {data: 'start'},
                {data: 'end'},
                {data: 'formulas'},
				{data: 'id_brand'},
                {data: 'aksi'},
            ],
        });

        $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();


        refreshStandart()
        refreshTipe()
        refreshProduk()
        refreshRequest()
        refreshPlant()
        /// Button Action
        $('#tambah_data').click(function () {
            $('#isi_tabel_template').html('');
            refreshStandart()
            refreshTipe()
            refreshProduk()
            refreshPlant()
            refreshRequest()
            $("#judul_input").html('Form Add');
            $(".btn-simpan").show();
            $("#id_request, #nm_spec, #tipe_request, #standart, #tipe, #produk, #note,#brand ").val('');
            $("#dataHeader").hide()

            $(".btn-simpan").show();
            $(".btn-edit").hide();

            $("#formTemplate").show(500)

            // $("#dlg").modal("show");
        });

        $('#tombol_back').click(function () {
            $("#dataHeader").show(500)
            $("#formTemplate").hide(500)
        });


        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

        $('#tabel_list tbody').on('click', 'tr', function () {
            var tes = ListParameter.row(this).data();
            var parameter = tes['parameter'];
            var simbol = tes['simbol'];
            var satuan = tes['satuan'];
            var mark = tes['mark'];
            var metode = tes['metode'];
            var id_parameter = tes['id_parameter'];
            var id_standart_uji = tes['id_standart_uji'];

            $("#tabel_template").find("tbody").append(
                "<tr style='cursor: move;'>" +

                "<td><span id='" + id_parameter + "'>" + parameter + "</span></td>" +
                "<td>" + (simbol == null ? '' : simbol) + "</td>" +
                "<td>" + (satuan == null ? '' : satuan) + "</td>" +
                "<td>" + (metode == null ? '' : metode) + "</td>" +
                "<td>" + (mark == null ? '' : mark) + "</td>" +
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_template'></td>" +
                "<td><input type='text' class='form-control input-xs' style='width: 150px' id='nilai_result" + id_parameter + "'></td>" +
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>" +
                "<td style='display:none;'> </td>" +
                "<td style='display:none;'>" + id_parameter + "</td>" +
                "<td style='display:none;'>" + id_standart_uji + "</td>" +
                "</tr>");

            $("#dlg").modal("hide");
        });
        $('#tabel_list_template tbody').on('click', 'tr', function () {
            var tes = ListTemplate.row(this).data();

            $.post("<?php echo base_url(); ?>coq/spesification/get_template", {"id_template": tes['id_template']}, function (datas) {
                var data = JSON.parse(datas);

                for (var i = 0; i < data.length; i++) {

                    refreshHitung(data[i].ID_TEMPLATE_HEADER)
                    $("#tabel_template").find("tbody").append(
                        "<tr>" +
                        "<td><span id='" + data[i].ID_UJI + "'>" + data[i].NAMA_UJI + "<span></td>" +
                        "<td>" + (data[i].SIMBOL == null ? '' : data[i].SIMBOL) + "</td>" +
                        "<td>" + (data[i].SATUAN == null ? '' : data[i].SATUAN) + "</td>" +
                        "<td>" + (data[i].NM_METODE == null ? '' : data[i].NM_METODE) + "</td>" +
                        "<td>" + (data[i].MARK == null ? '' : data[i].MARK) + "</td>" +
                        "<td><input type='text' class='form-control input-xs' style='width: 150px' value='" + (data[i].NILAI_TEMPLATE == null ? '' : data[i].NILAI_TEMPLATE) + "' id='nilai_template'></td>" +
                        "<td><input type='text' class='form-control input-xs' style='width: 150px'  value='" + (data[i].NILAI_RESULT == null ? '' : data[i].NILAI_RESULT) + "' id='nilai_result" + data[i].ID_UJI + "'></td>" +
                        "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'" + data[i].ID_UJI + "\',\'" + data[i].NAMA_UJI + "\')\"><i class='fa fa-pencil fa-lg'></i></a></td>" +
                        "<td style='display:none;'> </td>" +
                        "<td style='display:none;'>" + data[i].ID_UJI + "</td>" +
                        "<td style='display:none;'>" + data[i].ID_STANDART_UJI + "</td>" +

                        "</tr>");

                }


            }).fail(function () {
                show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")
            }).always(function () {
            });

            $("#dlg_template").modal("hide");
        });

    });

    function add() {


        $('#myModal').modal('show');
    }

    $(function () {
        $('#tipe').change(function () {
            refreshBrand($("#tipe option:selected").val())
        });

        function refreshBrand(id) {
            $.post("<?php echo base_url(); ?>coq/spesification/refreshBrand", {
                "id": id
            }, function (data) {
                $("#brand").html(data);


            }).fail(function () {
                // Nope
            }).always(function () {
                $(".selectpicker").selectpicker("refresh");
            });
        }
    });

    function list_parameter() {
        $("#dlg").modal("show");
        ListParameter = $('#tabel_list').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'excel', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'pdf', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'print', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 8},
                {"visible": false, "targets": 7},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listParameter',
                data: {
                    'id_standart': $("#standart").val(),
                    'id_produk': $("#produk").val()
                },
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        return_data.push({
                            'id': no,
                            'parameter': json[i].NAMA_UJI,
                            'simbol': json[i].SIMBOL,
                            'satuan': json[i].SATUAN,
                            'mark': json[i].MARK,
                            'metode': json[i].NM_METODE,
                            'parent': json[i].NAMA_PARENT,
                            'id_parameter': json[i].ID_UJI,
                            'id_standart_uji': json[i].ID_STANDART_UJI,
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'parameter'},
                {data: 'simbol'},
                {data: 'satuan'},
                {data: 'mark'},
                {data: 'metode'},
                {data: 'parent'},
                {data: 'id_parameter'},
                {data: 'id_standart_uji'},
            ],
        });
    }

    function print(code) {
        window.open("<?=base_url()?>coq/Spesification/template/" + code);

    }

    function list_template() {
        $("#dlg_template").modal("show");
        ListTemplate = $('#tabel_list_template').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Dasta"},
            "orderCellsTop": true,
            // 'dom': 'Bfrtip',
            // 'buttons': [
                // {extend: 'copy', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'excel', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'pdf', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},
                // {extend: 'print', title: 'Master_Metode', exportOptions: {columns: [0, 1, 2]}},

            // ],
            "columnDefs": [
                {"visible": false, "targets": 5},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "destroy": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/coq/spesification/get_listTemplate',
                data: {},
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        return_data.push({
                            'id': no,
                            'template': json[i].NM_TEMPLATE,
                            'standart': json[i].NM_STANDART,
                            'tipe': json[i].KD_PRODUCT_TYPE,
                            'produk': json[i].NM_PRODUCT,
                            'id_template': json[i].ID_TEMPLATE_HEADER,
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'template'},
                {data: 'standart'},
                {data: 'tipe'},
                {data: 'produk'},
                {data: 'id_template'},
            ],
        });
    }


    function hapus_role(elm) {
        var cld = $(elm).parent().parent(); //tr
        cld.remove();
    }

    function refreshPlant() {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshPlant", function (data) {
            $("#plant").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshRequest(id_request) {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshRequest", function (data) {
            $("#request").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshStandart() {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshStandart", function (data) {
            $("#standart").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshTipe() {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshTipe", function (data) {
            $("#tipe").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshProduk() {
        $.post("<?php echo base_url(); ?>coq/spesification/refreshProduk", function (data) {
            $("#produk").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function simpan() {
        var nm_spec = $("#nm_spec").val();
        var standart = $("#standart").val();
        var tipe = $("#tipe").val();
        var produk = $("#produk").val();
        var request = $("#request").val();
        var plant = $("#plant").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var formulas = $("#formulas").val();
        var brand = $("#brand").val();
        var vessel_name = $("#vessel_name").val(); 
        var bill_number_2 = $("#bill_number_2").val(); 
        var bill_date_2 = $("#bill_date_2").val(); 
        var port_loading_2 = $("#port_loading_2").val(); 
        var port_discharge_2 = $("#port_discharge_2").val(); 
        var desc_sample_2 = $("#desc_sample_2").val(); 
        var notify_party_2 = $("#notify_party_2").val(); 
        var quantity_1 = $("#quantity_1").val();
        var prints = $("#prints").val();  
        if (nm_spec == "" && standart == "" && tipe == "" && produk == "" && plant == "" && formulas == "" && brand == "") {
            show_toaster(3, '', "Form harus terisi.")
            return;
        }

        $('#input_simpan').attr('disabled', true);
        
        $('#input_simpan').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        
        var isi_data = "";
        var i = 0;
        isi_data += "&header[nm_spec]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[standart]=" + encodeURI($("#standart").val());
        isi_data += "&header[tipe]=" + encodeURI($("#tipe").val());
        isi_data += "&header[produk]=" + encodeURI($("#produk").val());
        isi_data += "&header[request]=" + encodeURI($("#request").val());
        isi_data += "&header[plant]=" + encodeURI($("#plant").val());
        isi_data += "&header[start]=" + encodeURI($("#start").val());
        isi_data += "&header[end]=" + encodeURI($("#end").val());
        isi_data += "&header[formulas]=" + encodeURI($("#formulas").val());
        isi_data += "&header[brand]=" + encodeURI($("#brand").val());
        isi_data += "&header[vessel_name]=" + encodeURI($("#vessel_name").val());
        isi_data += "&header[bill_number_2]=" + encodeURI($("#bill_number_2").val());
        isi_data += "&header[bill_date_2]=" + encodeURI($("#bill_date_2").val());
        isi_data += "&header[port_loading_2]=" + encodeURI($("#port_loading_2").val());
        isi_data += "&header[port_discharge_2]=" + encodeURI($("#port_discharge_2").val());
        isi_data += "&header[desc_sample_2]=" + encodeURI($("#desc_sample_2").val());
        isi_data += "&header[notify_party_2]=" + encodeURI($("#notify_party_2").val());
        isi_data += "&header[quantity_1]=" + encodeURI($("#quantity_1").val());
        isi_data += "&header[prints]=" + encodeURI($("#prints").val());

        var tbl_list = $("#tabel_template tbody");
        tbl_list.find('tr').each(function (i, obj) {
            isi_data += "&isi_data[" + i + "][parameter]=" + encodeURI($(this).children().eq(9).html());
            isi_data += "&isi_data[" + i + "][standart]=" + encodeURI($(this).children().eq(10).html());
            isi_data += "&isi_data[" + i + "][nilai]=" + encodeURI($(this).children().eq(5).find('input').val());
            isi_data += "&isi_data[" + i + "][result]=" + encodeURI($(this).children().eq(6).find('input').val());
            i++;
        });

        $.post("<?php echo base_url(); ?>coq/spesification/simpan", isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {

                show_toaster(1, '', data.message)
                $("#dataHeader").show()
                $("#formTemplate").hide()
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)
            }
        }).fail(function () {
            show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
            $('#input_simpan').html("<i class='fa fa-save'></i>&nbsp;Simpan"); 
            $('#input_simpan').attr('disabled', false);
        
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $('#isi_tabel_template').html('');
        $("#id_spec").val(kolom['id_spec']);
        $("#nm_spec").val(kolom['spec']);
        $("#standart").val(kolom['id_standart']);
        $("#tipe").val(kolom['id_tipe']);
        $("#produk").val(kolom['id_product']);
        $("#request").val(kolom['id_request']);
        $("#plant").val(kolom['id_plant']);
        $("#start").val(kolom['start']);
        $("#end").val(kolom['end']);
        $("#formulas").val(kolom['formulas']);
        $("#brand").val(kolom['id_brand']);
        $("#vessel_name").val(); 
        $("#bill_number_2").val(); 
        $("#bill_date_2").val(); 
        $("#port_loading_2").val(); 
        $("#port_discharge_2").val(); 
        $("#desc_sample_2").val(); 
        $("#notify_party_2").val(); 
        $("#quantity_1").val();
        $("#prints").val();   
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");

        $.post("<?php echo base_url(); ?>coq/spesification/get_spec", {"id_spec": kolom['id_spec']}, function (datas) {
            var data = JSON.parse(datas);
            for (var i = 0; i < data.length; i++) {
                refreshSpec(data[i].ID_SPEC_HEADER)
                $("#tabel_template").find("tbody").append(
                    "<tr>" +

                    "<td><span id='" + data[i].ID_UJI + "'>" + data[i].NAMA_UJI + "<span></td>" +
                    "<td>" + (data[i].SIMBOL == null ? '' : data[i].SIMBOL) + "</td>" +
                    "<td>" + (data[i].SATUAN == null ? '' : data[i].SATUAN) + "</td>" +
                    "<td>" + (data[i].NM_METODE == null ? '' : data[i].NM_METODE) + "</td>" +
                    "<td>" + (data[i].MARK == null ? '' : data[i].MARK) + "</td>" +
                    "<td><input type='text' class='form-control input-xs' style='width: 150px' value='" + (data[i].NILAI_STD == null ? '' : data[i].NILAI_STD) + "' id='nilai_template'></td>" +
                    "<td><input type='text' class='form-control input-xs' style='width: 150px'  value='" + (data[i].NILAI_SPEC == null ? '' : data[i].NILAI_SPEC) + "' id='nilai_result" + data[i].ID_UJI + "'></td>" +
                    "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a><a  class='btn-xs btn btn-edit' onclick=\"updateparam(\'" + data[i].ID_UJI + "\',\'" + data[i].NAMA_UJI + "\')\"><i class='fa fa-pencil fa-lg'></i></a></td>" +
                    "<td style='display:none;'> </td>" +
                    "<td style='display:none;'>" + data[i].ID_UJI + "</td>" +
                    "<td style='display:none;'>" + data[i].ID_STANDART_UJI + "</td>" +
                    "</tr>");

            }


        }).fail(function () {
            show_toaster(2, '', "Gagal menyimpan data. Server sedang bermasalah.")
        }).always(function () {
            $('#input_update').attr('disabled', false);
            $('#input_update').html('<i class="fa fa-save"> </i> Update');
        });


        $("#dataHeader").hide(500)
        $("#formTemplate").show(500)
        $(".selectpicker").selectpicker("refresh");
    }

    function update() {
        var id = $("#id_spec").val();
        var nm_spec = $("#nm_spec").val();
        var standart = $("#standart").val();
        var tipe = $("#tipe").val();
        var produk = $("#produk").val();
        var request = $("#request").val();
        var plant = $("#plant").val();
        var start = $("#start").val();
        var end = $("#end").val();
        var formulas = $("#formulas").val();
        var brand = $("#brand").val();
        var vessel_name = $("#vessel_name").val(); 
        var bill_number_2 = $("#bill_number_2").val(); 
        var bill_date_2 = $("#bill_date_2").val(); 
        var port_loading_2 = $("#port_loading_2").val(); 
        var port_discharge_2 = $("#port_discharge_2").val(); 
        var desc_sample_2 = $("#desc_sample_2").val(); 
        var notify_party_2 = $("#notify_party_2").val(); 
        var quantity_1 = $("#quantity_1").val();  
        var prints = $("#prints").val();  
        if (nm_spec == "" && standart == "" && tipe == "" && produk == "" && plant == "" && formulas == "" && brand == "") {
            show_toaster(3, '', "Form harus terisi.")
            return;
        }
        $('#input_update').attr('disabled', true);
        
        $('#input_update').html('<i class="fa fa-spinner fa-spin" ></i> Processing');
        

        var isi_data = "";
        var i = 0;
        isi_data += "header[id]=" + encodeURI($("#id_spec").val());
        isi_data += "&header[nm_spec]=" + encodeURI($("#nm_spec").val());
        isi_data += "&header[standart]=" + encodeURI($("#standart").val());
        isi_data += "&header[tipe]=" + encodeURI($("#tipe").val());
        isi_data += "&header[produk]=" + encodeURI($("#produk").val());
        isi_data += "&header[request]=" + encodeURI($("#request").val());
        isi_data += "&header[plant]=" + encodeURI($("#plant").val());
        isi_data += "&header[start]=" + encodeURI($("#start").val());
        isi_data += "&header[end]=" + encodeURI($("#end").val());
        isi_data += "&header[formulas]=" + encodeURI($("#formulas").val());
        isi_data += "&header[brand]=" + encodeURI($("#brand").val());
        isi_data += "&header[vessel_name]=" + encodeURI($("#vessel_name").val());
        isi_data += "&header[bill_number_2]=" + encodeURI($("#bill_number_2").val());
        isi_data += "&header[bill_date_2]=" + encodeURI($("#bill_date_2").val());
        isi_data += "&header[port_loading_2]=" + encodeURI($("#port_loading_2").val());
        isi_data += "&header[port_discharge_2]=" + encodeURI($("#port_discharge_2").val());
        isi_data += "&header[desc_sample_2]=" + encodeURI($("#desc_sample_2").val());
        isi_data += "&header[notify_party_2]=" + encodeURI($("#notify_party_2").val());
        isi_data += "&header[quantity_1]=" + encodeURI($("#quantity_1").val());
        isi_data += "&header[prints]=" + encodeURI($("#prints").val());

        var tbl_list = $("#tabel_template tbody");
        tbl_list.find('tr').each(function (i, obj) {
            isi_data += "&isi_data[" + i + "][parameter]=" + encodeURI($(this).children().eq(9).html());
            isi_data += "&isi_data[" + i + "][standart]=" + encodeURI($(this).children().eq(10).html());
            isi_data += "&isi_data[" + i + "][nilai]=" + encodeURI($(this).children().eq(5).find('input').val());
            isi_data += "&isi_data[" + i + "][result]=" + encodeURI($(this).children().eq(6).find('input').val());
            i++;
        });

        $.post("<?php echo base_url(); ?>coq/spesification/update", isi_data, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                show_toaster(1, '', data.message)
                $("#dataHeader").show()
                $("#formTemplate").hide()
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)
            }
        }).fail(function () {
            show_toaster(2, '', "Gagal mengubah data. Server sedang bermasalah.")
        }).always(function () {

            $('#input_update').html("<i class='fa fa-save'></i>&nbsp;Update");
            $('#input_update').attr('disabled', false);
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom);
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Delete Spesification</b>",
            "message": "Anda yakin ingin menghapus Spesification \"" + kolom['spec'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Delete",
                "action": function (dialog) {
                    hapus(kolom['id_spec'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>coq/spesification/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                show_toaster(1, '', "Berhasil menghapus data.")
                TabelData.ajax.reload();
            } else {
                show_toaster(2, '', data.message)
            }
        }).fail(function () {
            show_toaster(2, '', "Gagal menghapus data. Server sedang bermasalah.")
        }).always(function () {
            dialog.setClosable(true);
        });
    }

    function detail_spec(id) {
        $.post("<?php echo base_url(); ?>coq/spesification/detail", {"id": id}, function (data) {
            $("#detail_spuc").html(data);
            $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Detail Specification</b>");
            $("#dlg_detail").modal("show");
        }).fail(function () {
            // Nope
        }).always(function () {
            // $(".selectpicker").selectpicker("refresh");
        });
    }

    $(function () {
        $('#cara_1').hide();
        $('#cara_2').hide();
        $('#cara_3').hide();
        $('#rumus').change(function () {
            if ($('#rumus option:selected').val() == 'rumus_1') {
                $('#cara_1').show();
                $('#cara_2').hide();
                $('#cara_3').hide();
            } else if ($('#rumus option:selected').val() == 'rumus_2') {
                $('#cara_2').show();
                $('#cara_1').hide();
                $('#cara_3').hide();
            } else if ($('#rumus option:selected').val() == 'rumus_3') {
                $('#cara_3').show();
                $('#cara_1').hide();
                $('#cara_2').hide();
            }
        });
    });

    function refreshHitung(id) {
        $.post("<?php echo base_url(); ?>coq/Spesification/ambil_nilai", {
            "id": id
        }, function (data) {
            $('[name="ambil"]').html(data);


        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshSpec(id) {
        $.post("<?php echo base_url(); ?>coq/Spesification/ambil_spec", {
            "id": id
        }, function (data) {
            $('[name="ambil"]').html(data);


        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    $(function () {
        $('#btn-update').click(function () {


            /*var _id = $('#edit-name').val();

           $(_tr).find('td:eq(0)').text(_id);*/

            $('#modal-edit').modal('hide');
            var cara_1 = 4.071 * ($('#2').val() - $('#3').val()) - 7.600 * $('#5').val() - 6.718 * $('#7').val() - 1.430 * $('#9').val();
            var cara_2 = 4.071 * ($('#12').val() - $('#13').val()) - 7.600 * $('#15').val() - 6.718 * $('#17').val() - 1.430 * $('#19').val() - 2.582 * $('#21').val();
            var cara_3 = 4.071 * $('#32').val() - 7.600 * $('#34').val() - 6.718 * $('#36').val() - 1.430 * $('#38').val();
            /*$("#"+$('#id-name').val()).html($('#edit-name').val());*/
            /*$("#"+$('#id-coba').val()).html($('#name').val());*/
            if ($('#rumus option:selected').val() == 'rumus_1') {
                $("#nilai_result" + $('#id-name').val()).val(cara_1);
            } else if ($('#rumus option:selected').val() == 'rumus_2') {
                $("#nilai_result" + $('#id-name').val()).val(cara_2);
            } else if ($('#rumus option:selected').val() == 'rumus_3') {
                $("#nilai_result" + $('#id-name').val()).val(cara_3);
            }


        });
    });

    function updateparam(a, b) {
        $('#cara_1').hide();
        $('#cara_2').hide();
        $('#cara_3').hide();
        // var _tr = $(this).closest('tr');
        // var _id = _tr.find('td:eq(0)').text();
        $('#rumus').val('');
        $('#rumus').selectpicker('refresh');

        $('#form-edit')[0].reset();
        $('#modal-edit').modal('show');
        $('#id-name').val(a);
        $('#name_param').val(b);

        /*$('#edit-name').val(b);*/

    }


</script>
<!-- Dialog "Edit Record" -->
<div id="modal-edit" class="modal" tabindex="-1" role="dialog">
    <form id="form-edit" action="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rumus Perhitungan</h5>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">
                            <input id="name_param" class="form-control" type="text" name="name" value="" readonly="">
                        </div>
                        <label>Rumus: </label>
                        <input id="id-name" class="form-control" type="text" name="name" value=""
                               style="display: none;">

                        <select class='form-control selectpicker' data-live-search='true' id="rumus">
                            <option value="" data-hidden='true' selected='selected'></option>
                            <option value='rumus_1'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x
                                Fe2O3
                            </option>
                            <option value='rumus_2'>C3S = 4.071 x (CaO-FCaO) - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x
                                Fe2O3 - 2.582 x SO3
                            </option>
                            <option value='rumus_3'>C3S = 4.071 x CaO - 7.600 x SiO2 - 6.718 x Al2O3 - 1.430 x Fe2O3
                            </option>
                        </select>
                        <br><br>
                        <div id="cara_1">
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="1" class="form-control" value='4,071' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="2" class="form-control" placeholder="CaO" name="ambil">  -->
                                    <select class="form-control selectpicker" id="2" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="3" class="form-control" placeholder="FCaO"> -->
                                    <select class="form-control selectpicker" id="3" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="4" class="form-control" value='7,600' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- input type="text"  id="5" class="form-control" placeholder="SiO2">  -->
                                    <select class="form-control selectpicker" id="5" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="6" class="form-control" value='6,718' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="7" class="form-control" placeholder="Al2O3">  -->
                                    <select class="form-control selectpicker" id="7" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="8" class="form-control" value='1,430' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2">
                                    <!-- <input type="text"  id="9" class="form-control" placeholder="Fe2O3">  -->
                                    <select class="form-control selectpicker" id="9" name="ambil">

                                    </select>
                                </div>
                            </div>
                        </div>

                        <!-- cara 2 -->
                        <div id="cara_2">
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="11" class="form-control" value='4,071' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="12" class="form-control" placeholder="CaO" name="ambil">  -->
                                    <select class="form-control selectpicker" id="12" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="13" class="form-control" placeholder="FCaO" name="ambil">  -->
                                    <select class="form-control selectpicker" id="13" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="14" class="form-control" value='7,600' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="15" class="form-control" placeholder="SiO2" name="ambil"> -->
                                    <select class="form-control selectpicker" id="15" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="16" class="form-control" value='6,718' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="17" class="form-control" placeholder="Al2O3" name="ambil">  -->
                                    <select class="form-control selectpicker" id="17" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="18" class="form-control" value='1,430' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2">
                                    <!-- <input type="text"  id="19" class="form-control" placeholder="Fe2O3" name="ambil">  -->
                                    <select class="form-control selectpicker" id="19" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="20" class="form-control" value="2,582" readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2">
                                    <!-- <input type="text"  id="21" class="form-control" placeholder="SO3" name="ambil">  -->
                                    <select class="form-control selectpicker" id="21" name="ambil">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-- cara 3 -->

                        <div id="cara_3">
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="31" class="form-control" value='4,071' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="32" class="form-control" placeholder="CaO" name="ambil"> -->
                                    <select class="form-control selectpicker" id="32" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="33" class="form-control" value='7,600' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="34" class="form-control" placeholder="SiO2" name="ambil">  -->
                                    <select class="form-control selectpicker" id="34" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="35" class="form-control" value='6,718' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <!-- <input type="text"  id="36" class="form-control" placeholder="Al2O3" name="ambil">  -->
                                    <select class="form-control selectpicker" id="36" name="ambil">

                                    </select>
                                </div>
                                <div class="col-sm-1">-</div>
                                <div class="col-sm-2" style="padding-bottom: 5px;">
                                    <input type="text" id="37" class="form-control" value='1,430' readonly>
                                </div>
                                <div class="col-sm-1">x</div>
                                <div class="col-sm-2">
                                    <!-- <input type="text"  id="38" class="form-control" placeholder="Fe2O3" name="ambil">  -->
                                    <select class="form-control selectpicker" id="38" name="ambil">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--  <input id="name" class="form-control" type="text" name="name" value="" required> -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-update">Save</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

