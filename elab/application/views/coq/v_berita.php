  <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }

    
 </style>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                  
                    <div class="col-md-6">
                      <h2><i class="fa fa-list"></i> Master Berita</h2>
                    </div>
                    <div class="col-md-6">
                       <div style="text-align:right;">
                          <button class="btn-sm btn-success" onclick="add()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                      </div>
                    </div>
                  
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Content Of The News</th>
                            <th>Images</th>
                            <th>Created Date</th>
                            <th><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>
                            <tr id="filtering">
                            <th></th>
                            <td>Title</td>
                            <td>Content Of The News</td>
                            <td>Images</td>
                            <td>Created Date</td>
                            <th></th>
                          </tr>
                          
                        </thead>
                       
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          var tableData;
          $(document).ready(function(){
             tableData = $('#Table').DataTable({ 
            "processing": true,
            "serverSide": true,
            "lengthMenu": [10, 25, 50, 100],
            "sDom": 'Rfrtlip',
            'lengthChange': true,
            "searching": true,
            "ordering": false,
            "order":[],
            "ajax": {
                "url": "<?php echo base_url('coq/master_berita/ajax_list')?>",
                "type": "POST",

            },

        });

            $('#Table thead td').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" data-index='+(i+1)+' />' );
                  } );

                  $( tableData.table().container() ).on( 'keyup', 'thead input', function () {
                      console.log($(this).data('index')+"-"+this.value);
                      tableData.column( $(this).data('index') ).search( this.value ).draw();
                   });

                  tableData.on( 'order.dt search.dt', function () {
                  tableData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                      cell.innerHTML = i+1;
                  } );
              } ).draw();

            $(document).on('click','#btn_filtering', function(){
                var data = $(this).data('filtering');
                if(data==1){
                  $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                  $('#filtering').hide(500);

                }else{
                  $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                  $('#filtering').show(500);
                }
             });
          
          

          });
          $(function(){
            $('#content').summernote({
        placeholder: '. . . .',
        tabsize: 2,
        height: 100,
        callbacks: {
            onImageUpload: function(image) {
                        uploadImage(image[0]);
            },
            onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }           
        }
      });
         function uploadImage(image) {
            data = new FormData();
            data.append("image", image);
            $.ajax({
                data: data,
                type: "POST",
                url: "<?php echo base_url('coq/master_berita/upload_image') ?>",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                   $('#content').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
            });
        }
        function deleteImage(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo base_url('coq/master_berita/delete_image') ?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
 
        
          $('#content_e').summernote({
        
        tabsize: 2,
        height: 100,
        callbacks: {
            onImageUpload: function(image) {
                        uploadImage_e(image[0]);
            },
            onMediaDelete : function(target) {
                        deleteImage_e(target[0].src);
                    }           
        }
      });
          function uploadImage_e(image) {
            data = new FormData();
            data.append("image", image);
            $.ajax({
                data: data,
                type: "POST",
                url: "<?php echo base_url('coq/master_berita/upload_image_e') ?>",
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                   $('#content_e').summernote("insertImage", url);
                    },
                    error: function(data) {
                        console.log(data);
                    }
            });
        }
        function deleteImage_e(src) {
                $.ajax({
                    data: {src : src},
                    type: "POST",
                    url: "<?php echo base_url('coq/master_berita/delete_image_e') ?>",
                    cache: false,
                    success: function(response) {
                        console.log(response);
                    }
                });
            }
          function postForm() {
    var content = $('textarea[name="content"]').html($('#content').code());
    }
    function postForm_e() {
    var content_e = $('textarea[name="content_e"]').html($('#summernote').code());
    }
          });
           
          var save_method;
          var table;

          function add()
          {
            save_method ='add';
            $('#activity-form')[0].reset();
            $('.modal-title').text('Tambah Data');
            $(".selectpicker").selectpicker("refresh");
            $('#modal-tambah-activity').modal('show');
          }
          
          function hapus(id){
         if (confirm("Apakah yakin ingin menghapus?")) {
             $.ajax({
                 url:'<?php echo base_url() ?>coq/master_berita/hapus',
                 type: 'post',
                 data: {id:id},
                 success: function () {
                  
                     tableData.ajax.reload();
                 },
                 error: function () {
                     alert('Gagal');
                 }
             });
         } 

      }
          function edit(id){
            $('#activity-form-edit')[0].reset();
            save_method ='edit';

            $.ajax({
              url : '<?php echo site_url('coq/master_berita/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('[name="id_e"]').val(data.ID_NEWS);
                $('[name="title_e"]').val(data.TITLE);
                $('[name="content_e"]').summernote('code', data.ISI_BERITA);
                $('[name="foto_e"]').val(data.IMAGES);
               
                

                $('#modal-edit-activity').modal('show');
                
                $('.modal-title').text('Update Menu');
              }
            });
          }
          
        </script>
       
        <!-- Modal -->
        <div id="modal-tambah-activity" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Tambah Data</h4>
             </div>
             <div class="modal-body form">
                <form id="activity-form" action="<?php echo base_url('coq/master_berita/tambah') ?>" method="POST" enctype="multipart/form-data" onsubmit="return postForm()">
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="id" name="id">
                  </div>
                  

                  
                  <div class="row">
                                <div class="col-lg-12">  
                                    <div class="form-activity" role="form" data-toggle="validator">  
                                        <div class="form-group">
                                            <label for="plant">Title</label>
                                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter your title ..." required>
                                        </div>     
                                        <div class="form-group">
                                            <label for="judulact">Content of the news</label>
                                            <textarea class="form-control" id="content" name="content" placeholder="Enter Content ..." rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsiact">Images</label>
                                            <input type="file" class="form-control" id="foto" name="foto" required>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                  
                  
                  
               
             </div>
             <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <input type="submit" class="btn btn-primary" id="save" name="save" value="Simpan"></i> &nbsp;<font class='lowercase'></font>
                        </form>
                    </div>
            </div>

          </div>
        </div>
        <!-- modal edit-->
        <div class="modal fade" id="modal-edit-activity">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form News</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="activity-form-edit" action="<?php echo base_url('coq/master_berita/update') ?>" method="POST" enctype="multipart/form-data" onsubmit="return postForm_e()">
                            <input type="hidden" class="form-control" id="id" name="id_e">

                            <div class="row">
                                <div class="col-lg-12">  
                                    <div class="form-activity" role="form" data-toggle="validator">  
                                        <div class="form-group">
                                            <label for="plant">Title</label>
                                            <input type="text" class="form-control" id="title" name="title_e" placeholder="Enter your title ..." required>
                                        </div>     
                                        <div class="form-group">
                                            <label for="judulact">Content of the news</label>
                                            <textarea class="form-control" id="content_e" name="content_e" placeholder="Enter Content ..." rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="deskripsiact">Images</label>
                                            <input type="file" class="form-control" id="foto" name="foto_e" required>
                                        </div> 
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                            <input type="submit" class="btn btn-primary" id="save" name="update" value="Simpan"></i> &nbsp;<font class='lowercase'></font>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
         