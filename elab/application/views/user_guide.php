

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                  
                    <div class="col-md-6">
                      <h2><i class="fa fa-list"></i> User Guide</h2>
                    </div>
                    
                  
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>File</th>
                            <th><center>Aksi</center></th>
                          </tr>
                            
                          
                        </thead>
                       <tbody>
                         <tr>
                           <td>1</td>
                           <td>User Guide Rebuild E-Laboratory</td>
                           <td><button onclick="download()" class="btn btn-primary btn-sm waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-download"></i></span> Download</button></td>
                         </tr>
                       </tbody>
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
           function download()
    {
        window.open("<?=base_url()?>user_guide/download");
    }
        </script>