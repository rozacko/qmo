
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    .form-group {
        margin-bottom : 0px;
    }
 </style>
<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script>
 <div id='detail_spuc'></div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			     <div class="">
            <div class="col-md-6">
              <h2><i class="fa fa-list"></i> Hasil Pengujian</h2>
            </div>
            <div class="col-md-6">
              <div style="text-align:right">
                  &nbsp;&nbsp;&nbsp;
                  <!-- <button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel</button>  -->
              </div>
            </div>
           </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th  style='width:10px;'>No</th>
                                <!-- <th   style='width:20px;'>Detail SPUC  </th> -->
                                <th  >NO SPUC  </th>
                                <th  >Tanggal Terima  </th>
                                <th  >Nama Contoh   </th>
                                <th  >  LHU </th>
                                <th  ><center>  SHU <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span></center></th>
                                <th  >   </th>
                                <th  >   </th>
                            </tr>
                            <tr id="filtering">
                                <th  style='widtd:10px;'></th>
                                <td >NO SPUC </td>
                                <td >Tanggal Terima </td>
                                <td >Nama Contoh</td>
                                <th ></th>
                                <th ></th>
                                <th ></th>
                                <th ></th>
                            </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){
    // $('#tabel thead td').each( function () {
        // var title = $(this).text();
        // $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // } );
        tabel_view()

});

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "orderCellsTop": true,
                "columnDefs": [
                    { "visible": false, "targets": 6 },
                    { "visible": false, "targets": 7 },
                    {'orderable' : false, 'targets': 5}
                  ],
                // "destroy": true,
                'dom': 'Bfrtip',
                'buttons': [
                    {extend: 'copy', exportOptions: {columns: [0,2,3,4]}},
                    {extend: 'excel', exportOptions: {columns: [0,2,3,4]}},
                    {extend: 'pdf', exportOptions: {columns: [0,2,3,4]}},
                    {extend: 'print', exportOptions: {columns: [0,2,3,4]}},
                ],
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                // "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                
                columns : [
                  { data : 'no' },
                  // { data : 'spuc' },
                  { data : 'no_spuc' },
                  { data : 'tgl_terima' },
                  { data : 'contoh' },
                  { data : 'lhu' },
                  { data : 'shu' },
                  { data : 'id_transaksi' },
                  { data : 'id_kategori' },
                ],
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/hasil_uji/get_data',
                     dataType: 'JSON',
                     dataSrc : function (json) {
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){
                            return_data.push({
                              'no'	    : no ,
                              // 'spuc'	    :   '<center><button onclick="detail_spuc(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-warning btn-xs waves-effect btn_edit" title="Detail KKPP"><span class="btn-labelx"><i class="fa fa-check"></i></span> </button> </center>',
                              'no_spuc'      :  '<a href="#" onclick="detail_spuc(\''+ json[i].ID_TRANSAKSI +'\')" class="" title="Detail KKPP"/><span class=""></span> '+json[i].NO_BAPPUC+'',
                              'tgl_terima'      :   json[i].TANGGAL,
                              'contoh'          :  json[i].NAMA_CONTOH ,
                              'lhu'          :  json[i].LHU ,
                              'shu'          :  json[i].SHU ,
                              'id_transaksi'          :  json[i].ID_TRANSAKSI ,
                              'id_kategori'          :  json[i].ID_KATEGORI ,
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});

    // Apply the search
             $('#tabel thead td').each(function (i) {
                var title = $('#Table thead th').eq($(this).index()).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
            });

            $(TabelData.table().container()).on('keyup', 'thead input', function () {
                console.log($(this).data('index') + "-" + this.value);
                TabelData.column($(this).data('index')).search(this.value).draw();
            });

            TabelData.on('order.dt search.dt', function () {
                TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();
            
            
        $(document).on('click', '#btn_filtering', function () { 
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

    }

	function edit(baris) {
		var kolom = TabelData.row(baris).data();
		$("#no_spuc").html(kolom['no_spuc']);

		$("#tgl_terima").html(kolom['tgl_terima']);
		$("#peralatan").html(kolom['peralatan']=="Y" ? "YA" : "TIDAK");
		$("#personil").html(kolom['personil']=="Y" ? "YA" : "TIDAK");
		$("#metode").html(kolom['metode']=="Y" ? "YA" : "TIDAK");
		$("#waktu").html(kolom['waktu']=="Y" ? "YA" : "TIDAK");
		$("#keterangan").html('" '+kolom['keterangan']+' "');
        $(".btn-simpan").hide();
        $(".btn-edit").show();
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;KAJI ULANG PERMINTAAN PENGUJIAN</b>");
		$("#dlg").modal("show");
	}


     function lhu(id_transaksi, id_kategori) {
        var base_url    =  "<?php echo base_url(); ?>";
        // if(id_kategori=='1'||id_kategori=='2'){
            var link = 'lhu_old';
        // }else{
              // var link = 'lhu';
        // }
        if(id_kategori=='3' || id_kategori=='5')
        {
          link ='lhu';
        }
        var page    =   base_url + 'hasil_uji/'+link+'/?id_transaksi=' +id_transaksi+ '&&id_kategori=' +id_kategori;
                // e.preventDefault();
                $.ajax({
                    url: page,
                    type: 'GET',
                    // data: {
                       // 'id_kategori': id_kategori,
                       // 'id_transaksi': id_transaksi
                   // },
                    success: function() {
                         // window.open(page,'_blank' );
                        // window.location = page;
                    }
                });
     }
     function shu(id_transaksi, id_kategori) {
        var base_url    =  "<?php echo base_url(); ?>";
        var link = 'shu';
        if(id_kategori=='3' || id_kategori=='5')
        {
          link ='newshu';
        }
        var page    =   base_url + 'hasil_uji/'+link+'/?id_transaksi=' +id_transaksi;
                // e.preventDefault();
                $.ajax({
                    url: page,
                    type: 'GET',
                    data: {
                       'id_transaksi': id_transaksi
                   },
                    success: function() {
                         // window.open(page,'_blank' );
                        // window.location = page;
                    }
                });
     }

</script>
