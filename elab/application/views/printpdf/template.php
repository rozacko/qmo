<?php if ($data['detail'][0]['ID_QUALITY_HEADER']) {
    ?>
    <!--=========================================================-->
    <!--=========================================================-->
    <!--================  TEMPLATE COQ  ----- ===================-->
    <!--=========================================================-->
    <!--=========================================================-->
    <!DOCTYPE html>
    <html>
    <head>
        <title>Certificate of Quality <?php echo $data['detail'][0]['NM_COMPANY']; ?></title>
        <style type="text/css">
            .garis {
                border: 1px solid black;
            }

        </style>

    </head>
    <body>
    <table width="100%" style="border-collapse: collapse; border: none; margin-bottom: 5px;">
        <tr>
            <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                ?>
                <td align="left"><img src="assets/image/padang.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                ?>
                <td align="left"><img src="assets/image/logo_sgja.jpg" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                ?>
                <td align="left"><img src="assets/image/tonasa.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                ?>
                <td align="left"><img src="assets/image/gresik.png" width="80" height="68" class="rounded-circle"/></td>
                <?php

            } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                ?>
                <td align="left"><img src="assets/image/tanglong.png" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                ?>
                <td align="left"><img src="assets/image/SBI.PNG" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } ?>


            <td align="right" style="padding-top: 10px;"><img src="assets/image/smig.png" width="130"
                /><img src="assets/image/Loog-kualitas.png" width="70"
                       height="60"/></td>
        </tr>
    </table>
    <hr style="border: 1px solid black" style="padding: 0px; margin: 0px;">
    <br><br>
    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 6px;">
        <tr>


            <!-- <td align="center"><h4><b>CERTIFICATE OF QUALITY <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b> -->
            <td align="center" style="padding-left: 30px"><h3><b><u>CERTIFICATE OF QUALITY (COQ)</u></b></h3>
                </h4></td>

        </tr>
        <tr>
            <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                ?>
                <td></td>
                <?php
            } else {
                ?>
                <td align="center"><h4><b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                            (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4></td>
                <?php
            } ?>

        </tr>

    </table>
    <table width="40%" align="center" style="border-collapse: collapse; border: none;">
        <tr>

            <td style="text-align: center;">
                <small style="font-weight: bold">Certificate No :</small>
                <small style="font-weight: bold;font-size: 12px;color: midnightblue;"><?php echo $data['detail'][0]['KD_QUALITY']; ?>/<?php echo date('m') ?>/<?php echo $data['detail'][0]['NM_PRODUCT']; ?></small>
            </td>
        </tr>
        <!--<tr>

            <td>
                <small>Certificate No</small>
            </td>


            <td>:</td>


            <td style="color: midnightblue;">
                <small><?php /*echo $data['detail'][0]['KD_QUALITY']; */ ?>/<?php /*echo date('m') */ ?>
                    /<?php /*echo $data['detail'][0]['NM_PRODUCT']; */ ?></small>
            </td>
        </tr>-->
        <tr>

            <td style="text-align: center; font-weight: bold">
                <small>Brand : <?php echo $data['detail'][0]['NM_BRAND']; ?></small>
            </td>

            <!--            <td>:</td>-->

            <!--            <td style="color: blue;">-->
            <!--                <small>--><?php //echo $data['detail'][0]['NM_BRAND']; ?><!--</small>-->
            <!--            </td>-->

        </tr>
    </table>
    <br>
    <table width="130px" style="border-collapse: collapse; border: none;">
        <tr>
            <td style="text-align: right">
                <small style="font-size: 10px;">Date Produce</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['START_PRODUCE']; ?></small>
            </td>

        </tr>
        <tr>
            <td style="text-align: right;">
                <small style="font-size: 10px;">To</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['END_PRODUCE']; ?></small>
            </td>

        </tr>
    </table>


    <table width='100%' border="1" style="border-collapse: collapse;">
        <thead>
        <tr>
            <th width='4%' rowspan='2'></th>
            <th align='center' rowspan='2' colspan="3" width='40%'>
                <center style="font-size: 12px;"><p>QUALITY PARAMETERS</p></center>
            </th>

            <th align='center' rowspan='1' colspan='3' width='40%'>
                <center>
                    <small
                            style="font-size: 10px;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></small>
                </center>
            </th>
        </tr>
        <!--<tr>
            <th width='4%' rowspan='2'></th>
            <th align='center' rowspan='3' colspan="3" width='40%'>
                <center><p style="font-size: 12px;">QUALITY PARAMETERS</p></center>
            </th>

            <th align='center' rowspan='1' colspan='3' width='40%'>
                <center>
                    <small
                            style="font-size: 10px;"><?php /*echo $data['detail'][0]['NM_PRODUCT_TYPE']; */ ?><?php /*echo $data['detail'][0]['KD_PRODUCT_TYPE']; */ ?></small>
                </center>
            </th>
        </tr>-->
        <tr>
            <?php if ($data['detail'][0]['NM_STANDART'] != 'ASTM') {
                ?>
                <th align='center' rowspan='1' colspan='3' width='40%'>
                    <center>
                        <small
                                style="font-size: 10px;"><?php echo $data['detail'][0]['NM_STANDART_PRODUCT']; ?></small>
                    </center>
                </th>
                <?php
            } else {
                ?>
                <th align='center' rowspan='1' colspan='3' width='40%'>
                    <center>
                        <small
                                style="font-size: 10px;"><?php echo $data['detail'][0]['NM_STANDART_PRODUCT']; ?></small>
                    </center>
                </th>
                <?php
            } ?>
        </tr>
        <!--<tr>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TESTING METHOD</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TEST RESULT</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>SPECIFICATION</small>
                </center>
            </th>
        </tr>-->
        </thead>
        <tbody>
        <tr>
            <td></td>
            <td colspan='2'>
                <small style="font-size: 10px;"><b>I.CHEMICAL COMPOSITION : </b></small>
            </td>

            <td>
                <center>
                    <small style="font-size: 8px;"><b>UNIT</b></small>
                </center>
            </td>
            <td align='center' width='13%'>
                <center>
                    <small style="font-size: 8px;"><b>TESTING METHOD</b></small>
                </center>
            </td>
            <td align='center' width='13%'>
                <center>
                    <small style="font-size: 8px;"><b>TEST RESULT</b></small>
                </center>
            </td>
            <td align='center' width='13%'>
                <center>
                    <small style="font-size: 8px;"><b>SPECIFICATION</b></small>
                </center>
            </td>
        </tr>


        <?php $no = 0;
        foreach ($list_par as $value) {
            if (empty($value['NILAI_STD'])) {
                $value['NILAI_STD'] = '-';
                $value['MARK'] = '';
            }
            if (empty($value['NILAI_SPEC'])) {
                $value['NILAI_SPEC'] = '-';
                $value['MARK'] = '';
            }

            if ($value['ID_KATEGORI'] == '1') {
                ?>
                <tr>

                    <?php if (count($value['NAMA_UJI']) != '1') {
                        ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php
                    } else {
                        $no++;
                        ?>
                        <td>
                            <small style="font-size: 8px;">
                                <center><?php echo $no; ?></center>
                            </small>
                        </td>
                        <?php if (count($p['NAMA_PARENT']) == '0') {
                            ?>
                            <td width="120px" style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;">&nbsp; <?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;">
                                <small style=" font-size: 8px;">
                                    (<?php echo $value['SIMBOL']; ?>)
                                </small>
                            </td>
                            <td width='8%'>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['MARK']; ?><?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;"><?php echo $value['NAMA_PARENT']; ?>
                                    <br><?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td width='8%'>
                                <small style="font-size: 8px;"></small>
                                >
                                <center><?php echo $value['SIMBOL']; ?></center>
                                </small></td>
                            <td width='8%'>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['MARK']; ?><?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        }
                    } ?>

                </tr>
                <?php
            }
        } ?>
        <tr>
            <td>&nbsp;</td>
            <td style="border-right-width: 0px;border-right-style: solid;">&nbsp;</td>
            <td style="border-left-width: 0px;border-left-style: solid;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>

            <td></td>
            <td colspan='2'>
                <small style="font-size: 10px;"><b>II.PHYSICAL PROPERTIES : </b></small>
            </td>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php $no = 1;
        $tmp = array();
        foreach ($list_par as $value) {
            $tmp[$value['NAMA_PARENT']][] = $value;
        }
        foreach ($tmp as $tmp_valuw) {
            for ($i = 0; $i < sizeof($tmp_valuw); $i++) {
                if ($tmp_valuw[$i]['ID_KATEGORI'] == '2') {
                    if ($tmp_valuw[$i]['NAMA_PARENT'] == '') {
                        ?>
                        <tr>
                            <td style="border-bottom-style: dotted">
                                <small style="font-size: 8px;">
                                    <center><?php echo $no++; ?></center>
                                </small>
                            </td>
                            <td style="border-right-width: 0px;border-right-style: solid; border-bottom-style: dotted">
                                <small style="font-size: 8px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td width='8%' style="border-left-width: 0px;border-left-style: solid;">
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td width='8%'>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                        </tr>
                    <?php } else {
                        if ($i == 0) { ?>
                            <tr>
                                <td>
                                    <small style="font-size: 8px;">
                                        <center><?php echo $no++; ?></center>
                                    </small>
                                </td>
                                <td style="border-right-width: 0px;border-right-style: solid;">
                                    <small style="font-size: 8px;">
                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_PARENT']; ?></small>
                                </td>
                                <!--                                <td>--><?php //echo $tmp_valuw[$i]['NAMA_PARENT']
                                ?><!--</td>-->
                                <td width='8%' style="border-left-width: 0px;border-left-style: solid;">
                                    <small style="font-size: 8px;">
                                        <center>
                                </td>
                                <td width='8%'>
                                    <small style="font-size: 8px;">
                                        <center></center>
                                    </small>
                                </td>

                                <td>
                                    <small style="font-size: 8px;">
                                        <center></center>
                                    </small>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td></td>
                            <td style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td width='8%' style="border-left-width: 0px;border-left-style: solid;">
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td width='8%'>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
        } ?>
        </tbody>
    </table>
    <br>
    <table width='100%'>
        <tr>
            <td width='60%' style="padding-top: 5px;"><p style="font-size:8px;">
                    THE <?php echo $data['detail'][0]['NM_PRODUCT']; ?> IS SUITABLE FOR THE PRODUCTION OF CEMENT
                    CONFORMING TO <?php echo $data['detail'][0]['NM_STANDART']; ?></p><br><b style="font-size:8px;"><i>Formulas
                        for mineral specification of the <?php echo $data['detail'][0]['NM_PRODUCT']; ?></i></b><br>
                <p style="font-size: 8px;white-space: pre-wrap;"><?php echo $data['detail'][0]['FORMULAS']; ?> </p></td>
            <td width='5%'></td>
            <td width='35%' align='center'></td>
        </tr>
        <tr>
            <td width='60%'></td>
            <td width='5%'></td>
            <td width='35%' align='center'>
                <small style="font-size: 10px;"><?php echo $data['detail'][0]['NM_PLANT']; ?>
                    , <?php echo date('d F Y') ?> </small>
                <br><br>
                <small><img
                            src='https://api.qrserver.com/v1/create-qr-code/?data=Nama Quality :<?php echo $approve['Approval'][0]['NM_QUALITY']; ?>%20Kode Quality :<?php echo $approve['Approval'][0]['KD_QUALITY']; ?>%20Diapprove oleh :<?php echo $approve['Approval'][0]['FULLNAME']; ?>%20Tanggal:<?php echo $approve['Approval'][0]['TANGGAL']; ?>%20Manager approve oleh:<?php echo $approve['Approval'][1]['FULLNAME']; ?>%20Tanggal:<?php echo $approve['Approval'][1]['TANGGAL']; ?>%20Senior Manager approve oleh:<?php echo $approve['Approval'][2]['FULLNAME']; ?>%20Tanggal:<?php echo $approve['Approval'][2]['TANGGAL']; ?>&amp;size=80x80'
                            alt='' title=''/></small>
                <br><br>
                <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SEMEN PADANG</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SEMEN INDONESIA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SEMEN TONASA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SEMEN GRESIK</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SEMEN THANGLONG</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                    ?>
                    <small style="font-size: 10px;"> SM OF SOLUSI BANGUN INDONESIA</small>
                    <?php

                } ?>


            </td>
        </tr>
    </table>


    </body>
    </html>
    <?php
} elseif ($data['detail'][0]['ID_SPEC_HEADER']) {
    ?>

    <!--=========================================================-->
    <!--=========================================================-->
    <!--================  TEMPLATE SOQ  ----- ===================-->
    <!--=========================================================-->
    <!--=========================================================-->
    <!DOCTYPE html>
    <html>
    <head>
        <title>Specification of Quality <?php echo $data['detail'][0]['NM_COMPANY']; ?></title>
        <style type="text/css">
            .garis {
                border: 1px solid black;
            }
        </style>

    </head>
    <body>
    <table width="100%" style="border-collapse: collapse; border: none; margin-bottom: 5px;">
        <tr>
            <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                ?>
                <td align="left"><img src="assets/image/padang.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                ?>
                <td align="left"><img src="assets/image/logo_sgja.jpg" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                ?>
                <td align="left"><img src="assets/image/tonasa.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                ?>
                <td align="left"><img src="assets/image/gresik.png" width="80" height="68" class="rounded-circle"/></td>
                <?php

            } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                ?>
                <td align="left"><img src="assets/image/tanglong.png" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                ?>
                <td align="left"><img src="assets/image/SBI.PNG" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } ?>


            <td align="right" style="padding-top: 10px;"><img src="assets/image/smig.png" width="130"
                /><img src="assets/image/Loog-kualitas.png" width="70"
                       height="60"/></td>


        </tr>

    </table>
    <hr style="border: 1px solid black;" style="padding: 0px; margin: 0px;">
    <br><br>
    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 6px;">
        <tr>


            <!-- <td align="center"><h4><b>SPECIFICATION OF <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b></h4> -->
            <td align="center" style="padding-left: 30px"><h2><b><u>SPECIFICATION OF QUALITY (SOQ)</u></b></h2>
            </td>

        </tr>
        <tr>
            <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                ?>
                <td></td>
                <?php
            } else {
                ?>
                <td align="center"><h4><b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                            (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4></td>
                <?php
            } ?>

        </tr>


    </table>
    <table width="40%" align="center" border="0" style="border-collapse: collapse; border: none;">
        <tr>

            <td style="text-align: center;">
                <small style="font-weight: bold">Certificate No :</small>
                <small style="font-weight: bold;font-size: 12px;color: midnightblue;"><?php echo $data['detail'][0]['KD_SPEC']; ?>
                    /<?php echo date('m') ?>/<?php echo $data['detail'][0]['NM_PRODUCT']; ?></small>
            </td>


            <!--            <td>:</td>-->


            <!--            <td style="color: blue;">-->
            <!--                <small>--><?php //echo $data['detail'][0]['KD_SPEC']; ?><!--/--><?php //echo date('m') ?>
            <!--                    /--><?php //echo $data['detail'][0]['NM_PRODUCT']; ?><!--</small>-->
            <!--            </td>-->


        </tr>
        <tr>

            <td style="text-align: center; font-weight: bold">
                <small>Brand : <?php echo $data['detail'][0]['NM_BRAND']; ?></small>
            </td>

            <!--            <td>:</td>-->

            <!--            <td style="color: blue;">-->
            <!--                <small>--><?php //echo $data['detail'][0]['NM_BRAND']; ?><!--</small>-->
            <!--            </td>-->

        </tr>
    </table>
    <br>
    <table width="130px" style="border-collapse: collapse; border: none;">
        <tr>
            <td style="text-align: right">
                <small style="font-size: 10px;">Date Produce</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['START_PRODUCE']; ?></small>
            </td>

        </tr>
        <tr>
            <td style="text-align: right;">
                <small style="font-size: 10px; align-content: right">To</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['END_PRODUCE']; ?></small>
            </td>

        </tr>
    </table>

    <table class="style2" border="1" width='100%' style="border-collapse: collapse;border: 1px solid black;">
        <thead>
        <tr>
            <th rowspan='2' colspan="4" width="49%">

                <center style="font-size: 12px;"><p>QUALITY PARAMETERS</p></center>
            </th>
            <!--            <th align='center' rowspan='2' colspan="3" width='40%'>-->
            <!--            </th>-->

            <th align='center' rowspan='1' colspan='3' width='51%'>
                <center>
                    <small
                            style="font-size: 10px;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></small>
                </center>
            </th>
        </tr>
        <tr>
            <?php if ($data['detail'][0]['NM_STANDART'] != 'ASTM') {
                ?>
                <th align='center' rowspan='1' colspan='3' width='51%'>
                    <center>
                        <small
                                style="font-size: 10px;"><?php echo $data['detail'][0]['NM_STANDART_PRODUCT']; ?></small>
                    </center>
                </th>
                <?php
            } else {
                ?>
                <th align='center' rowspan='1' colspan='3' width='51%'>
                    <center>
                        <small
                                style="font-size: 10px;"><?php echo $data['detail'][0]['NM_STANDART_PRODUCT']; ?></small>
                    </center>
                </th>
                <?php
            } ?>

        </tr>
        </thead>
        <tbody>
        <tr>

            <!--            <td></td>-->
            <td colspan='3' width="49%">
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>I.CHEMICAL COMPOSITION : </b></small>
            </td>

            <td>
                <center>
                    <small style="font-size: 8px;"><b>UNIT</b></small>
                </center>
            </td>
            <td align='center' width='17%'>
                <center>
                    <small style="font-size: 8px;"><b>TESTING METHOD</b></small>
                </center>
            </td>
            <td align='center' width='17%'>
                <center>
                    <small style="font-size: 8px;"><b>SPECIFICATION</b></small>
                </center>
            </td>
            <td align='center' width='17%'>
                <center>
                    <small style="font-size: 8px;"><b>TEST RESULT</b></small>
                </center>
            </td>
        </tr>
        <!-- INI SOQ DATA LOOPING-->
        <?php $no = 0;
        foreach ($list_par as $value) {

            if (empty($value['NILAI_STD'])) {
                $value['NILAI_STD'] = '-';
                $value['MARK'] = '';
            }
            if (empty($value['NILAI_SPEC'])) {
                $value['NILAI_SPEC'] = '-';
                $value['MARK'] = '';
            }
            if ($value['ID_KATEGORI'] == '1') {
                ?>
                <tr>

                    <?php if (count($value['NAMA_UJI']) == '0') {
                        ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php
                    } else {
                        $no++;
                        ?>
                        <td>
                            <small style="font-size: 8px;" width="30">
                                <center><?php echo $no; ?></center>
                            </small>
                        </td>
                        <?php if (count($p['NAMA_PARENT']) == '0') {
                            ?>
                            <td style="border-right-width: 0px;border-right-style: solid;" width="40px">
                                <small style="font-size: 8px;">&nbsp; <?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;">
                                <small style=" font-size: 8px;">
                                    (<?php echo $value['SIMBOL']; ?>)
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;color: midnightblue;">
                                    <center><?php echo $value['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;"><?php echo $value['NAMA_PARENT']; ?>
                                    <br><?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;">
                                <small style="font-size: 8px;"></small>
                                <center><?php echo $value['SIMBOL']; ?></center>
                                </small></td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px; color: midnightblue;">
                                    <center><?php echo $value['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <?php
                        }

                    } ?>

                </tr>
                <?php
            }
        } ?>
        <tr>
            <td width="30">&nbsp;</td>
            <td style="border-right-width: 0px;border-right-style: solid;">&nbsp;</td>
            <td style="border-left-width: 0px;border-left-style: solid;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>

            <!--            <td></td>-->
            <td colspan='3'>
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>II.PHYSICAL PROPERTIES : </b></small>
            </td>

            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>


        <?php $no = 1;
        //        echo '<pre>';
        //        print_r($list_par);
        $tmp = array();
        foreach ($list_par as $value) {
            $tmp[$value['NAMA_PARENT']][] = $value;
        }

        //        print_r($tmp);

        foreach ($tmp as $tmp_valuw) {
            for ($i = 0; $i < sizeof($tmp_valuw); $i++) {
                if ($tmp_valuw[$i]['ID_KATEGORI'] == '2') {
                    if ($tmp_valuw[$i]['NAMA_PARENT'] == '') {
                        ?>
                        <tr>
                            <td width="30">
                                <small style="font-size: 8px;">
                                    <center><?php echo $no++; ?></center>
                                </small>
                            </td>
                            <td style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;">
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                        </tr>
                    <?php } else {
                        if ($i == 0) { ?>
                            <tr>
                                <td width="30">
                                    <small style="font-size: 8px;">
                                        <center><?php echo $no++; ?></center>
                                    </small>
                                </td>
                                <td style="border-right-width: 0px;border-right-style: solid;">
                                    <small style="font-size: 8px;">
                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_PARENT']; ?></small>
                                </td>
                                <!--                                <td>--><?php //echo $tmp_valuw[$i]['NAMA_PARENT']
                                ?><!--</td>-->
                                <td style="border-left-width: 0px;border-left-style: solid;">
                                    <small style="font-size: 8px;">
                                        <center>
                                </td>
                                <td>
                                    <small style="font-size: 8px;">
                                        <center></center>
                                    </small>
                                </td>

                                <td>
                                    <small style="font-size: 8px;">
                                        <center></center>
                                    </small>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td width="30"></td>
                            <td style="border-right-width: 0px;border-right-style: solid;">
                                <small style="font-size: 8px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;">
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <td>
                                <small style="font-size: 8px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
        };

        ?>
        <tr>
            <td>&nbsp;</td>
            <td style="border-right-width: 0px;border-right-style: solid;">&nbsp;</td>
            <td style="border-left-width: 0px;border-left-style: solid;">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>
    <br>
    <table width='75%'>
        <!-- <tr>
            <td width='60%' style="padding-top: 5px;white-space: pre-wrap;"><p style="font-size:8px;">
                    THE <?php echo $data['detail'][0]['NM_PRODUCT']; ?> IS SUITABLE FOR THE PRODUCTION OF CEMENT
                    CONFORMING TO <?php echo $data['detail'][0]['NM_STANDART']; ?></p><br><b style="font-size:8px;"><i>Formulas
                        for mineral specification of the <?php echo $data['detail'][0]['NM_PRODUCT']; ?></i></b><br>
                <p style="font-size: 8px;white-space: pre-wrap;"><?php echo $data['detail'][0]['FORMULAS']; ?> </p></td>
            <td width='5%'></td>
            <td width='35%' align='center'></td>
        </tr> -->
        <tr>
            <td><p style='font-size:8px;'>THE <?php echo $data['detail'][0]['NM_PRODUCT']; ?> IS SUITABLE FOR THE
                    PRODUCTION OF CEMENT CONFORMING TO <?php echo $data['detail'][0]['NM_STANDART']; ?></p></td>
        </tr>
        <tr>
            <td><b style='font-size:8px;'><i>Formulas for mineral specification of
                        the <?php echo $data['detail'][0]['NM_PRODUCT']; ?></i></b></td>
        </tr>
        <tr>
            <td><p style='font-size:8px;white-space:pre-line;'><i><?php echo $data['detail'][0]['FORMULAS']; ?> </i></p>
            </td>

        </tr>
    </table>
    <table width="100%">
        <tr>
            <td width='60%'></td>
            <td width='5%'></td>
            <td width='35%' align='center'>
                <small><?php echo $data['detail'][0]['NM_PLANT']; ?>
                    , <?php echo date('d F Y') ?> </small>
                <br><br>
                <small><img
                            src='https://api.qrserver.com/v1/create-qr-code/?data=Nama Spec :<?php echo $approve['Approval'][0]['NM_SPEC']; ?> Kode Spec :<?php echo $approve['Approval'][0]['KD_SPEC']; ?>\n Diapprove oleh :<?php echo $approve['Approval'][0]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][0]['TANGGAL']; ?>\n Manager approve oleh:<?php echo $approve['Approval'][1]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][1]['TANGGAL']; ?>\n Senior Manager approve oleh:<?php echo $approve['Approval'][2]['FULLNAME']; ?>\nTanggal:<?php echo $approve['Approval'][2]['TANGGAL']; ?>&amp;size=80x80'
                            alt='' title=''/></small>
                <br><br>
                <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN PADANG</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN INDONESIA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN TONASA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN GRESIK</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN THANGLONG</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                    ?>
                    <small style="font-size:10px;"> SM OF SOLUSI BANGUN INDONESIA</small>
                    <?php

                } ?>

            </td>
        </tr>
    </table>


    </body>
    </html>
    <?php
} ?>

