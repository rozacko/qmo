<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
       .garis{
              border: 1px solid black;
            }
        table {
                  border-collapse: collapse;
                }
        table.fonts tr td { font-size: 11px; }
    </style>
</head>
<body>

    <table width="100%" style="border-collapse: collapse; border: none;">
        <tr>
            <td rowspan="2" width="10%"> <img src="assets/image/logo_sgja.jpg"/></td>
            <td  width="55%" style="font-size:11px">PT SEMEN INDONESIA (PERSERO) Tbk.</td>
            <td  width="35%" style="font-size:11px;text-align:right">F/5062/006   </td>
        </tr>
        <tr>
            <td style="font-size:11px;">DEPTARTEMEN LITBANG PRODUK & APLIKASI</td>
            <td style="font-size:11px;text-align:right">Edisi : 1, Revisi : 1, Tanggal : 13-06-2002</td>
        </tr>
    </table>

    <br>

    <table width='100%' class="fonts" border="1">
        <tr>
            <td colspan="2" align='center' style="font-size:14px;font-weight:bold;">LAPORAN PENGUJIAN <?php echo $data['title']; ?> </td>
        </tr>
         <tr>
            <td width="30%">Nama Contoh </td>
            <td width="70%">: <?php echo $data['header']['NAMA_CONTOH'] ?></td>
         </tr>
         <tr>
            <td width="30%">Jumlah Contoh </td>
            <td width="70%">: <?php echo $data['header']['JUMLAH'] ?></td>
         </tr>
         <tr>
            <td width="30%">Diterima tanggal</td>
            <td width="70%">: <?php echo $data['header']['TANGGAL'] ?> </td>
         </tr>
         <tr>
            <td width="30%">Dikerjakan tanggal </td>
            <td width="70%">:<?php echo $data['header']['TGL_AWAL'] ?> </td>
         </tr>
         <tr>
            <td width="30%">SPUC </td>
            <td width="70%">: <?php echo $data['header']['NO_BAPPUC'] ?></td>
         </tr>
         <tr>
            <td width="30%">Metode Uji </td>
            <td width="70%">: - </td>
         </tr>
    </table>

    <br>

    <?php
$count = 1;
foreach ($data['print']['page'] as $key): ?>

        Pengujian Beton
    <table width="100%" border="1" class="fonts">
        <tr>
            <td rowspan="2">NO</td>
            <td rowspan="2" width="30%">PENGUJIAN</td>
            <td rowspan="2"><center>SATUAN</center></td>
            <td rowspan="2" width="13%">STADART UJI</td>
            <td colspan="6"> <center>Kode Contoh</center></td>
        </tr>
        <tr>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo "<td><center>" . ($i + (($count - 1) * 6)) . "</center></td>";
}?>
        </tr>
        <!-- Pengujian Kuat Tekan Beton -->
        <tr>
            <td>1</td>
            <td>Pengujian Kuat Tekan Beton</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Pengujian Kuat Tekan Beton -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>1 Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['168'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td></td>
            <td>3 Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['169'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>7 Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['170'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td></td>
            <td>14 Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['171'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>21 Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['172'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td></td>
            <td>28Hari</td>
            <td><center>N/mm2</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . $key['kuat']['173'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->


        <!-- Slump -->
        <tr>
            <td>2</td>
            <td>Slump</td>
           <td><center>cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['slump']['161'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Slump -->

        <!-- Kuat Lentur 3 Titik Pembebanan -->
        <tr>
            <td>3</td>
            <td>Kuat Lentur 3 Titik Pembebanan</td>
           <td><center>kg/cm2</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['beban3']['162'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Lentur 3 Titik Pembebanan -->
        <!-- Kuat Lentur 1  Titik Pembebanan -->
        <tr>
            <td>4</td>
            <td>Kuat Lentur 1 Titik Pembebanan</td>
           <td><center>kg/cm2</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['beban1']['163'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Lentur 1  Titik Pembebanan -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td>5</td>
            <td>Kuat Belah Tarik Selinder </td>
           <td><center>kg/cm2</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['selinder']['164'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
         <!-- Permeability   -->
        <tr>
            <td>6</td>
            <td>Permeability </td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Permeability   -->
        <!-- Kedalaman Peneterasi   -->
        <tr>
            <td></td>
            <td>Kedalaman Peneterasi </td>
           <td><center>cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['permeability']['165'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kedalaman Peneterasi   -->

        <!-- Setting Time Beton   -->
        <tr>
            <td>7</td>
            <td>Setting Time Beton </td>
           <td><center>Jam:mnt</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo "<td></td>";
}?>
        </tr>
        <!-- Setting Time Beton   -->

         <!-- Setting Time Awal   -->
        <tr>
            <td></td>
            <td>Setting Time Awal </td>
           <td><center>Jam:mnt</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['awal']['166'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Setting Time Awal   -->
         <!-- Setting Time Akhir   -->
        <tr>
            <td></td>
            <td>Setting Time Akhir </td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['akhir']['167'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Setting Time Akhir   -->
    </table>

    <br>

    Pengujian Agregat
    <table width="100%" border="1" class="fonts">
        <tr>
            <td rowspan="2">NO</td>
            <td rowspan="2" width="30%">PENGUJIAN</td>
            <td rowspan="2"><center>SATUAN</center></td>
            <td rowspan="2" width="13%">STADART UJI</td>
            <td colspan="6"><center>Kode Contoh</center></td>
        </tr>
        <tr>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td><center>" . ($i + (($count - 1) * 6)) . "</center></td>";
}?>
        </tr>
        <!-- Berat isi dan rongga udara dalam agregat -->
        <tr>
            <td>1</td>
            <td>Berat isi dan rongga udara dalam agregat</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Berat isi dan rongga udara dalam agregat -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>Berat Isi</td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['beratisi']['192'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td></td>
            <td>Berat Isi SSD</td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['beratisissd']['192'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>Rongga Udara Void Content</td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['ronggaudaravoid']['192'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td>2</td>
            <td><b>Keausan Agregat dengan Los Angles</b></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Hari -->

         <!-- Hari -->
        <tr>
            <td></td>
            <td>Material Kecil</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Hari -->
        <!-- Hari -->
        <tr>
            <td></td>
            <td>Keausan Agregat</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['keasusankecil']['193'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Hari -->


        <!-- Slump -->
        <tr>
            <td></td>
            <td>Material Besar</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['keasusanbesar']['193'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Slump -->

        <!-- Kuat Lentur 3 Titik Pembebanan -->
        <tr>
            <td></td>
            <td>Keausan Agregat</td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Lentur 3 Titik Pembebanan -->
        <!-- Kuat Lentur 1  Titik Pembebanan -->
        <tr>
            <td>3</td>
            <td>Berat jenis dan penyerapan agregat halus</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Lentur 1  Titik Pembebanan -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td>Berat Jenis Relatif (spesific gravity) :</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->

        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ DO</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjdorelatif']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ SSD</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjssdrelatif']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ R</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjnyatarelatif']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td>Berat Jenis (Density)</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ DO</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjdodensity']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ SSD</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjssddensity']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ R</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjnyatadensity']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>Abs</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['penyerapan']['194'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->




         <!-- Berat jenis dan penyerapan agregat kasar -->
        <tr>
            <td>4</td>
            <td>Berat jenis dan penyerapan agregat kasar</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Berat jenis dan penyerapan agregat kasarx-->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td>Berat Jenis Relatif (spesific gravity) :</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->

        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ DO</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjdorelatif']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ SSD</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjssdrelatif']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ R</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjnyatarelatif']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td>Berat Jenis (Density)</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ DO</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjdodensity']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ SSD</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjssddensity']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
        <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>BJ R</center></td>
           <td><center>gr/cm</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['bjnyatadensity']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->
         <!-- Kuat Belah Tarik Selinder   -->
        <tr>
            <td></td>
            <td><center>Abs</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['penyerapan']['195'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kuat Belah Tarik Selinder   -->


        <!-- Analisa Ayakan -->
        <!-- Analisa Ayakan   -->
        <tr>
            <td>5</td>
            <td>Analisa Ayakan</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Analisa Ayakan   -->
        <!-- Analisa Ayakan   -->
        <tr>
            <td></td>
            <td>Agregat halus (persen lolos komulatif)</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>9.5 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus95']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>4,75 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus475']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>2,36 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus236']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>1,18 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus118']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>0,6 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus06']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>0,3 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus03']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>0,15 mm</center></td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['halus015']['196'][$i] . "</center></td>";
}?>
        </tr>

        <tr>
            <td></td>
            <td>Agregat Kasar (persen lolos komulatif)</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>50 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar50']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>37,5 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar37']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>25 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar25']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>19 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar19']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>12,5 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar12']['196'][$i] . "</center></td>";
}?>
        </tr>
        <tr>
            <td></td>
            <td><center>9,5 mm</center></td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['kasar9']['196'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Analisa Ayakan   -->

        <!-- kadar butir halus lebih kecil dari 75 mikro -->
        <tr>
            <td>6</td>
            <td>kadar butir halus lebih kecil dari 75 mikro</td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['butirhalus75']['197'][$i] . "</center></td>";

}?>
        </tr>
        <!-- kadar butir halus lebih kecil dari 75 mikro -->
        <!-- Kadar Organik agregat halus -->
        <tr>
            <td>7</td>
            <td>Kadar Organik agregat halus</td>
            <td><center>Plat No</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['198']['198'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kadar Organik agregat halus -->

        <!-- Total Kadarair agregat yang dapat menguap dg pengeringan -->
        <tr>
            <td>8</td>
            <td>Total Kadarair agregat yang dapat menguap dg pengeringan</td>
           <td><center>-</center></td>
            <td></td>
            <?php for ($i = 1; $i <= 6; $i++) {
    # code...
    echo " <td></td>";
}?>
        </tr>
        <!-- Total Kadarair agregat yang dapat menguap dg pengeringan -->

        <!-- Kadar air -->
        <tr>
            <td></td>
            <td>Kadar air </td>
           <td><center>%</center></td>
            <td></td>
            <?php for ($i = 0; $i < 6; $i++) {
    # code...
    echo " <td><center>" . $key['199']['199'][$i] . "</center></td>";
}?>
        </tr>
        <!-- Kadar air -->
    </table>

    <?php if (count($data['print']['page']) != 1 && $count < count($data['print']['page'])): ?>
        <pagebreak />
    <?php endif?>

    <?php
$count += 1;
?>


    <?php endforeach?>





    <!-- Signed -->
    <br>
    <br>
    <table width="100%">
        <tr>
            <td width="70%"></td>
            <td>Gresik </td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td>LAB QC Gresik</td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td>
                <img src=<?php echo $data['barcode']; ?> alt='' title='' style="width:75px;height:75px;"/>
                <br>
                <?php echo $data['approveArr']['FULLNAME']; ?>
            </td>
        </tr>
    </table>

</body>
</html>>
