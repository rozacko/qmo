<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
       .garis{
              border: 1px solid black;
            }
    </style>
</head>
<body>
    <table width="100%" style="border-collapse: collapse; border: none;"> 
        <tr>
            <td rowspan="2" width="10%"> <img src="assets/image/logo_sgja.jpg"/></td> 
            <td  width="55%" style="font-size:16px">PT SEMEN INDONESIA (PERSERO) Tbk.</td>   
            <td  width="35%" style="font-size:11px;text-align:right">F/5062/006   </td>
        </tr> 
        <tr>
            <td style="font-size:16px;">DEPTARTEMEN LITBANG PRODUK & APLIKASI</td>   
            <td style="font-size:9px;text-align:right"></td>
        </tr> 
    </table>

    <br>
    <br>
    <br>
    <table width="100%">
        <tr>
            <td style="text-align: center;font-size: 24px;"><u>SURAT PERINTAH UJI CONTOH <?php echo date('Y') ?></u></td>
        </tr>
        <tr>
            <td style="text-align: center;font-size: 14px;"><b>(SPUC)</b></td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%">
        <tr>
            <td style="width: 30%;">No</td>
            <td>:  <?php echo $data['elab_t_bppuc'][0]['NO_BAPPUC'] ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">Tanggal</td>
            <td>: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['TANGGAL']  :  "-" ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">Jumlah Contoh</td>
            <td>: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['JUMLAH']  :  "-" ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">Jenis Contoh</td>
            <td>: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['NAMA_CONTOH']  :  "-" ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">Metode Uji</td>
            <td>: <?php echo $data['metode'][0]['NAMA_STANDART'] ;?> dan <?php echo $data['metode'][1]['NAMA_STANDART'] ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">Batas Waktu Uji</td>
            <td>: <?php echo count($data['elab_t_bppuc'])==1 ? 
                 $data['elab_t_bppuc'][0]['TANGGAL_MULAI']
                 ." - ".
                 $data['elab_t_bppuc'][0]['TANGGAL_SELESAI']  
                 :  "-" ;?>
            </td>
        </tr>
    </table>

    <br>
    <br>


    <table width="100%" border="1" style="border-collapse: collapse;">
        <tr >
            <?php foreach ($data['uniq'] as $key): ?>
                <td  style="width: <?php echo (100/count($data['uniq'])) ?>%;text-align: center;"><?php echo $key; ?></td>
            <?php endforeach ?>
        </tr>
        <tr >
            <?php foreach ($data['uniq'] as $key): ?>
                <td style="vertical-align: top; border-bottom: none;"><?php echo $data[$key.'text']; ?></td>
            <?php endforeach ?>
        </tr>
    </table>

    <?php if (count($data['uniq'])!=0) {  ?>
        <table width="100%" style="padding-top: 10px;" border="1" style="border-collapse: collapse;">
            <tr>
                <td style="border-right: none;"></td>
                <td colspan="3"  style="text-align: center; border-left: none;"> 
                    <br>Laboratory Of Management Office<br><br><br><br>
                    Yudi Darmawan<br><br><br>
                </td>
            </tr>
            <tr>
                <td width="50%">Keterangan</td>
                <td colspan="2" width="30%">Diteruskan Kepada</td>
                <td width="20%">Paraf</td>
            </tr>
            <tr>
                <td><br><br><br><br><br><br><br><br></td>
                <td colspan="2" style="font-size: 10pt;">Penyelia Fisika  Penyelia Kimia</td>
                <td></td>
            </tr>
            <tr>
                <td style="font-size: 11px;">Kembali ke Administrasi....( dilampiri konsep hasil uji ) </td>
                <td width ="15%">Administrasi</td>
                <td width="15%" ><br><br><br><br> </td>
                <td></td>
            </tr>
        </table>
    <?php 
    } 
     ?>
    
   
</body>
</html>>