<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
       .garis{
              border: 1px solid black;
            }
    </style>
</head>
<body>
    <table width="100%" style="border-collapse: collapse; border: none;"> 
        <tr>
            <td rowspan="2" width="10%"> <img src="assets/image/logo_sgja.jpg"/></td> 
            <td  width="55%" style="font-size:16px">PT SEMEN INDONESIA (PERSERO) Tbk.</td>   
            <td  width="35%" style="font-size:11px;text-align:right">F/5062/006   </td>
        </tr> 
        <tr>
            <td style="font-size:16px;">DEPTARTEMEN LITBANG PRODUK & APLIKASI</td>   
            <td style="font-size:9px;text-align:right"></td>
        </tr> 
    </table>

    <br>
    <br>
    <br>
    <table width="100%">
        <tr>
            <td style="text-align: center;font-size: 20px;">BERITA ACARA PEMERIKSAAN & PENERIMAAN</td>
        </tr>
        <tr>
            <td style="text-align: center;font-size: 20px;">CONTOH UJI</td>
        </tr>
    </table>
    <br>
    <br>
    <table width="100%" style="font-size: 16px;">
        <tr>
            <td style="width: 30%;">NAMA CONTOH</td>
            <td>:  <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['NAMA_CONTOH']  :  "-" ;?></td>
        </tr>
        <tr>
            <td style="width: 30%;">NO. SPUC</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['NO_BAPPUC']  :  "-" ;?></td>            
        </tr>
        <tr>
            <td style="width: 30%;">TANGGAL PENERIMAAN</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['TANGGAL']  :  "-" ;?></td>            
        </tr>
        <tr>
            <td style="width: 30%;">KEMASAN CONTOH UJI</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['KEMASAN']  :  "-" ;?></td>            
        </tr>
        <tr>
            <td style="width: 30%;">WARNA CONTOH UJI</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['WARNA']  :  "-" ;?></td>            
        </tr>
        <tr>
            <td style="width: 30%;">BENTUK CONTOH UJI</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['NAMA_CONTOH']  :  "-" ;?></td>            
        </tr>
        <tr>
            <td style="width: 30%; vertical-align: top;">KETERANGAN</td>
            <td style="width: 70%;">: <?php echo count($data['elab_t_bppuc'])==1 ?  $data['elab_t_bppuc'][0]['KETERANGAN']  :  "-" ;?>
            </td>            
        </tr>
    </table>

    <br>
    <br>
    <br>
    <br>
    <br>


   
    <table width="100%">
        <tr>
            <td width="30%" style="text-align: center;">Diserahkan Oleh</td>
            <td width="40%" style="text-align: center;"></td>
            <td width="30%" style="text-align: center;">Diterima 0leh</td>
        </tr>
        <tr>
            <td><br><br><br><br><br><br></td>
            <td><br><br><br><br><br><br></td>
            <td><br><br><br><br><br><br></td>
        </tr>
         <tr>
            <td style="text-align: center;">(....................................)</td>
            <td style="text-align: center;"></td>
            <td style="text-align: center;">(....................................)</td>
        </tr>
        <tr>
            <td style="text-align: center;">Nama dan tanda tangan</td>
            <td style="text-align: center;"></td>
            <td style="text-align: center;">Nama dan tanda tangan</td>
        </tr>
    </table>
   
</body>
</html>>