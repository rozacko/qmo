<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
       .garis{
              border: 1px solid black;
            }
        table {
                  border-collapse: collapse;
                }

        table.fonts tr td { font-size: 11px; }
    </style>
</head>
<body>
    
    <table width="100%" style="border-collapse: collapse; border: none;">
        <tr>
            <td rowspan="2" width="10%"> <img src="assets/image/logo_sgja.jpg"/></td>
            <td  width="55%" style="font-size:11px">PT SEMEN INDONESIA (PERSERO) Tbk.</td>
            <td  width="35%" style="font-size:11px;text-align:right">F/5062/006   </td>
        </tr>
        <tr>
            <td style="font-size:11px;">DEPTARTEMEN LITBANG PRODUK & APLIKASI</td>
            <td style="font-size:11px;text-align:right">Edisi : 1, Revisi : 1, Tanggal : 13-06-2002</td>
        </tr>
    </table>

    <br>

    <table width='100%' border="1" class="fonts">
        <tr>
            <td colspan="2" align='center' style="font-size:14px;font-weight:bold;">LAPORAN PENGUJIAN <?php echo $data['title']; ?> </td>
        </tr>
         <tr>
            <td width="30%">Nama Contoh </td>
            <td width="70%">: <?php  echo $data['header']['NAMA_CONTOH'] ?></td>
         </tr>
         <tr>
            <td width="30%">Jumlah Contoh </td>
            <td width="70%">: <?php  echo $data['header']['JUMLAH'] ?></td>
         </tr>
         <tr>
            <td width="30%">Diterima tanggal</td>
            <td width="70%">: <?php  echo $data['header']['TANGGAL'] ?> </td>
         </tr>
         <tr>
            <td width="30%">Dikerjakan tanggal </td>
            <td width="70%">:<?php  echo $data['header']['TGL_AWAL'] ?> </td>
         </tr>
         <tr>
            <td width="30%">SPUC </td>
            <td width="70%">: <?php  echo $data['header']['NO_BAPPUC'] ?></td>
         </tr>
         <tr>
            <td width="30%">Metode Uji </td>
            <td width="70%">: - </td>
         </tr>
    </table>

    <br>

    <table width="100%" border="1" class="fonts">
        <tr>
            <td rowspan="2">
                <center><b>NO</b></center>
            </td>
            <td rowspan="2">
                <center><b>PARAMETER</b></center>
            </td>
            <td colspan="2">
                <center><b>PT SMI REQUIREMENT</b></center>
            </td>
            <td rowspan="2">
                <center><b>HASIL UJI</b></center>
            </td>
             <td rowspan="2">
                <center><b>METHOD</b></center>
            </td>
        </tr>
        <tr>
            <td >
                <center><b>ALT. MATERIAL</b></center>
            </td>
            <td >
                <center><b>ALT. FUEL</b></center>
            </td>
        </tr>

        <?php $no=1; ?>
        <?php foreach ($data['data']as $value): ?>
            <tr>
                <td><?php echo $no;?></td>
                <td><?php echo $value['KETERANGAN']; ?></td>
                <td><?php echo "-"; ?></td>
                <td><?php echo "-"; ?></td>

                <?php 
                if (in_array($value['ID_UJI'], $data['param'])) {
                    # code...
                    echo " <td><center>".$value['NILAI']."</center></td>";
                }else{
                    echo " <td><center>-</center></td>";
                }
                ?>
               
                <td></td>
            </tr>
            <?php $no+=1; ?>
        <?php endforeach ?>

    </table>

    <!-- Signed -->
    <br>
    <br>
    <table width="100%">
        <tr>
            <td width="70%"></td>
            <td>Gresik </td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td>LAB QC Gresik</td>
        </tr>
        <tr>
            <td width="70%"></td>
            <td>
                <img src=<?php echo $data['barcode']; ?> alt='' title='' style="width:75px;height:75px;"/>
                <br>
                <?php echo $data['approveArr']['FULLNAME']; ?> 
            </td>
        </tr>
    </table>

</body>
</html>>
