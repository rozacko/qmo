<!--=========================================================-->
<!--=========================================================-->
<!--================  TEMPLATE SOQ  ----- ===================-->
<!--=========================================================-->
<!--=========================================================-->
<?php if (substr($data['detail'][0]['KD_QUALITY'], 0, 2) == 'CE') {

    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Certificate of Quality <?php echo $data['detail'][0]['NM_COMPANY']; ?></title>
        <style type="text/css">
            .garis {
                border: 1px solid black;
            }

            .garistitik {
                border-bottom: 1px dotted black;
                border-right: 1px solid black;
            }

            .hanyaTitik {
                border-bottom: 1px dotted black;
            }

            .kanan {
                border-right: 1px solid black;
            }

            .atas {
                border-top: 1px solid black;
            }

            table #utama {
                border: 1px solid black;
            }

            #utama th {
                height: 20px;
            }

            #utama tr td {
                height: 18px;
                /*border: 1px solid red;*/
            }

            .kotak {
                z-index: 1;
                width: 300px;
                height: 25px;
                /*background-color: lightgrey;*/
                border: 1px solid black;
                margin: 2px;
                text-align: center;
                font-size: 8px;
                align-content: right;
                position: absolute;
                right: 50px;
                bottom: 35px;
            }

            .page {
                width: 300px;
                height: 25px;
                /*background-color: lightgrey;*/
                /*border: 1px solid black;*/
                margin: 2px;
                text-align: center;
                font-size: 8px;
                align-content: right;
                position: absolute;
                right: 10px;
                bottom: 10px;
            }

            .kotak1 {
                width: 300px;
                /*height: 25px;*/
                margin: 2px;
                text-align: left;
                font-size: 8px;
                align-content: right;
                position: absolute;
                left: 50px;
                bottom: 10px;
            }

            .kotakbg {
                z-index: 1;
                position: absolute;
                right: 0px;
                bottom: 0px;
            }
        </style>
    </head>
    <body>
    <table width="100%" style="border-collapse: collapse; border: none; margin-bottom: 5px;">
        <tr>
            <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                ?>
                <td align="left"><img src="assets/image/padang.png" width="80" height="68" class="rounded-circle"/>
                    <br><img src="assets/image/Logo SIG.png" width="20" style="padding-left: 30px;"/>
                </td>
                <td align="right" style="padding-top: 14px; padding-bottom: 0px"><img src="assets/image/kan_logo.png"
                                                                                      width="100" height="42"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                ?>
                <td align="left"><img src="assets/image/logo_sgja.jpg" width="80" height="68" class="rounded-circle"/>
                    <br><img src="assets/image/Logo SIG.png" width="20" style="padding-left: 30px;"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                ?>
                <td align="left"><img src="assets/image/tonasa.png" width="80" height="68" class="rounded-circle"/>
                    <br><img src="assets/image/Logo SIG.png" width="20" style="padding-left: 30px;"/>
                </td>
                <td align="right" style="padding-top: 14px; padding-bottom: 0px"><img src="assets/image/kan_logo.png"
                                                                                      width="100" height="42"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                ?>
                <td align="left"><img src="assets/image/gresik.png" width="80" height="68" class="rounded-circle"/>

                </td>
                <!-- <td align="right" style="padding-top: 14px; padding-bottom: 0px"><img src="assets/image/kan_logo.png" width="100" height="42" /> -->

                </td>

                <?php

            } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                ?>
                <td align="left"><img src="assets/image/tanglong.png" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                ?>
                <td align="left"><img src="assets/image/SBI.PNG" width="80" height="68" class="rounded-circle"/></td>
                <td align="right" style="padding-top: 14px; padding-bottom: 0px"><img src="assets/image/kan_logo.png"
                                                                                      width="100" height="42"/></td>
                <?php
            } ?>


        </tr>
        <tr>
            <td align="left"><img src="assets/image/Logo SIG.png" width="20"
                                  style="padding-left: 30px;padding-top: 10px;"/></td>
            <!-- <td align="center" style="padding-left: 250px;"> <p style="font-size: 6px;color: grey;">Laboratorium Penguji</p>
                <p  style="font-size: 6px;color: grey;">LP-280-IDN</p></td> -->
        </tr>
    </table>
    <hr style="height: 2px;border: 0; box-shadow: inset 0 12px 12px -12px; padding: 0px;margin: 0px;">
    <br><br>
    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 6px;">
        <tr>


            <!-- <td align="center"><h4><b>CERTIFICATE OF QUALITY <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b> -->
            <td align="center" style="padding-left: 30px"><h2><b><u>CERTIFICATE OF QUALITY (COQ)</u></b></h2>
            </td>

        </tr>
        <tr>
            <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                ?>
                <td></td>
                <td></td>
                <?php
            } else {
                ?>
                <td align="center"><h4><b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                            (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4></td>

                <?php
            } ?>

        </tr>
        <tr>
            <td align="center"><h5>(ISSUED BY MANUFACTURER)</h5></td>
        </tr>
    </table>

    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 20px; padding-top: 15px;">
        <tr>
            <td style="padding-top: 1em;width: 45%;">Description of Goods</td>
            <td style="padding-top: 1em;width: 5%;">:</td>
            <td style="padding-top: 1em;width: 50%;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Quantity</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['QUANTITY']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Shipper</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em">PT. <?php echo $data['detail'][0]['NM_COMPANY']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Manufacturer</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em">PT. <?php echo $data['detail'][0]['NM_COMPANY']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Notify Party</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['NOTIFY_PARTY']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Vessel Name</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['NM_KAPAL']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Bill of Lading Number</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['BILL_NUMBER']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Bill of Lading Date</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['BILL_DATE']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Port of Loading</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['PORT_LOADING']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Port of Discharge</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['PORT_DISCHARGE']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Method of Test</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['NM_STANDART']; ?></td>
        </tr>

        <tr>
            <td style="padding-top: 1em">Description of Sample</td>
            <td style="padding-top: 1em">:</td>
            <td style="padding-top: 1em"><?php echo $data['detail'][0]['DESC_SAMPLE']; ?></td>
        </tr>
    </table>
    <!-- <span style="width: 100%;height: 25px;background-color: black;border: 1px solid"></span> -->

    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 20px; padding-top: 15px;">

        <div class="kotak1">

            <img src="assets/image/gobeyondnext.PNG" width="100"/>
        </div>
        <div class="page">
            <p>Page 1 of 2</p>
        </div>

    </table>
    <div class="kotakbg">
        <img src="assets/image/footer-bg.png" height="35px" width="793.70088739539px;"/>
    </div>
    <!--  -->
    <div style="page-break-after:always;">
    </div>

    <!DOCTYPE html>
    <html>
    <head>

        <style type="text/css">
            .garis {
                border: 1px solid black;
            }

            .garistitik {
                border-bottom: 1px dotted black;
                border-right: 1px solid black;
            }

            .hanyaTitik {
                border-bottom: 1px dotted black;
            }

            .kanan {
                border-right: 1px solid black;
            }

            .atas {
                border-top: 1px solid black;
            }

            table #utama {
                border: 1px solid black;
            }

            #utama th {
                height: 20px;
            }

            #utama tr td {
                height: 18px;
                /*border: 1px solid red;*/
            }

            .page {
                width: 300px;
                height: 25px;
                /*background-color: lightgrey;*/
                /*border: 1px solid black;*/
                margin: 2px;
                text-align: center;
                font-size: 8px;
                align-content: right;
                position: absolute;
                right: 10px;
                bottom: 10px;
            }

            .kotak {
                z-index: 1;
                width: 300px;
                height: 25px;
                /*background-color: lightgrey;*/
                border: 1px solid black;
                margin: 2px;
                text-align: center;
                font-size: 8px;
                align-content: right;
                position: absolute;
                right: 50px;
                bottom: 35px;
            }

            .kotakbg {
                z-index: 1;
                position: absolute;
                right: 0px;
                bottom: 0px;
            }
        </style>
    </head>
    <body>
    <table width="100%" style="border-collapse: collapse; border: none; margin-bottom: 5px;">
        <tr>
            <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                ?>
                <td align="left"><img src="assets/image/padang.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                ?>
                <td align="left"><img src="assets/image/logo_sgja.jpg" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                ?>
                <td align="left"><img src="assets/image/tonasa.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                ?>
                <td align="left"><img src="assets/image/gresik.png" width="80" height="68" class="rounded-circle"/></td>
                <?php

            } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                ?>
                <td align="left"><img src="assets/image/tanglong.png" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                ?>
                <td align="left"><img src="assets/image/SBI.PNG" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } ?>


            <td align="right" style="padding-top: 10px;"><img src="assets/image/smig.png" width="150"
                /><img src="assets/image/Loog-kualitas.png" width="70"
                       height="60"/></td>
        </tr>
    </table>
    <hr style="height: 2px;border: 0; box-shadow: inset 0 12px 12px -12px; padding: 0px;margin: 0px;">
    <br><br>
    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 6px;">
        <tr>


            <!-- <td align="center"><h4><b>CERTIFICATE OF QUALITY <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b> -->
            <td align="center" style="padding-left: 30px"><h2><b><u>CERTIFICATE OF QUALITY (COQ)</u></b></h2>
            </td>

        </tr>
        <tr>
            <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                ?>
                <td></td>
                <?php
            } else {
                ?>
                <td align="center"><h4><b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                            (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4></td>
                <?php
            } ?>

        </tr>

    </table>
    <table width="40%" align="center" style="border-collapse: collapse; border: none;">
        <tr>

            <td style="text-align: center;">
                <small style="font-weight: bold">Certificate No :</small>
                <small style="font-weight: bold;font-size: 12px;color: midnightblue;"><?php echo $data['detail'][0]['KD_QUALITY']; ?>
                    /<?php echo date('m') ?>/<?php echo $data['detail'][0]['KODE_BRAND']; ?></small>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-weight: bold">
                <small>Brand : <?php
                    if ($data['detail'][0]['NM_BRAND'] == '') {
                        echo '&nbsp;&nbsp;  -';
                    } else {
                        echo $data['detail'][0]['NM_BRAND'];

                    }; ?></small>
            </td>
        </tr>
    </table>
    <table width="200px" style="border-collapse: collapse; border: none; max-width: 200px">
        <tr>
            <td width="70px" style="text-align: right">
                <small style="font-size: 10px;">Date Produce</small>
            </td>
            <td width="5px">
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['START_PRODUCE']; ?></small>
            </td>

        </tr>
        <tr>
            <td style="text-align: right;">
                <small style="font-size: 10px; align-content: right">To</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['END_PRODUCE']; ?></small>
            </td>

        </tr>
    </table>

    <table id="utama" width="100%" style="border-collapse: collapse;border: 1px solid black;">
        <thead>
        <tr>
            <!--        <th rowspan='2' colspan="4" width="52%" class="garis">-->
            <th colspan="4" width="52%" class="garis">

                <center style="font-size: 12px;"><p>QUALITY PARAMETERS</p></center>
            </th>
            <!--            <th align='center' rowspan='2' colspan="3" width='40%'>-->
            <!--            </th>-->

            <th align='center' colspan='3' width='48%' class="garis">
                <center>
                    <small
                        style="font-size: 12px;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></small>
                </center>
            </th>
        </tr>
        <!--<tr>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TESTING METHOD</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TEST RESULT</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>SPECIFICATION</small>
                </center>
            </th>
        </tr>-->
        </thead>
        <tbody>
        <tr>

            <!--            <td></td>-->
            <th colspan='3' width="44%" class="garis" style="text-align: left">
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>I.CHEMICAL COMPOSITION : </b></small>
            </th>

            <th width="8%" class="garis">
                <center>
                    <small style="font-size: 10px;"><b>UNIT</b></small>
                </center>
            </th>
            <th align='center' width='17%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>TESTING METHOD</b></small>
                </center>
            </th>
            <th align='center' width='14%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>TEST RESULT</b></small>
                </center>
            </th>
            <th align='center' width='17%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>SPECIFICATION</b></small>
                </center>
            </th>
        </tr>

        <?php $no = 0;
        foreach ($list_par as $value) {

            if (empty($value['NILAI_STD'])) {
                $value['NILAI_STD'] = '-';
                $value['MARK'] = '';
            }
            if (empty($value['NILAI_SPEC'])) {
                $value['NILAI_SPEC'] = '-';
                $value['MARK'] = '';
            }
            if ($value['ID_KATEGORI'] == '1') {
                ?>
                <tr>

                    <?php if (count($value['NAMA_UJI']) == '0') {
                        ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php
                    } else {
                        $no++;
                        ?>
                        <td width="5%" class="garistitik">
                            <small style="font-size: 9px;">
                                <center><?php echo $no; ?></center>
                            </small>
                        </td>
                        <?php if (count($p['NAMA_PARENT']) == '0') {
                            ?>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;">&nbsp; <?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;" class="hanyaTitik kanan">
                                <small style=" font-size: 9px;">
                                    (<?php echo $value['SIMBOL']; ?>)
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px; color: midnightblue;">

                                    <center><?php echo $value['NILAI_SPEC']; ?></center>

                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;"><?php echo $value['NAMA_PARENT']; ?>
                                    <br><?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;" class="hanyaTitik kanan">
                                <small style="font-size: 9px;"></small>
                                <center><?php echo $value['SIMBOL']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px; color: midnightblue;">

                                    <center><?php echo $value['NILAI_SPEC']; ?></center>

                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        }

                    } ?>

                </tr>
                <?php
            }
        } ?>
        <tr>
            <td class="kanan"></td>
            <td colspan="2" class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
        </tr>
        <tr>
            <th colspan='3' class="garis" style="text-align: left">
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>II.PHYSICAL PROPERTIES : </b></small>
            </th>
            <th class="garis"></th>
            <th class="garis"></th>
            <th class="garis"></th>
            <th class="garis"></th>
        </tr>
        <?php $no = 1;
        $tmp = array();
        foreach ($list_par as $value) {
            $tmp[$value['NAMA_PARENT']][] = $value;
        }
        foreach ($tmp as $tmp_valuw) {
            for ($i = 0; $i < sizeof($tmp_valuw); $i++) {
                if ($tmp_valuw[$i]['ID_KATEGORI'] == '2') {
                    if ($tmp_valuw[$i]['NAMA_PARENT'] == '') {
                        ?>
                        <tr>
                            <td class="garistitik">
                                <small style="font-size: 9px;">
                                    <center><?php echo $no++; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                        </tr>
                    <?php } else {
                        if ($i == 0) { ?>
                            <tr>
                                <td class="garistitik">
                                    <small style="font-size: 9px;">
                                        <center><?php echo $no++; ?></center>
                                    </small>
                                </td>
                                <td colspan="2" class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_PARENT']; ?></small>
                                </td>
                                <!--                                <td>--><?php //echo $tmp_valuw[$i]['NAMA_PARENT']
                                ?><!--</td>-->
                                <!--<td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center>
                                        </td>-->
                                <td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center></center>
                                    </small>
                                </td>

                                <td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center></center>
                                    </small>
                                </td>
                                <td class="hanyaTitik kanan">
                                </td>
                                <td class="hanyaTitik kanan">
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td class="garistitik"></td>
                            <td colspan="2" class="hanyaTitik kanan">
                                <small style="font-size: 9px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <!-- <td class="hanyaTitik kanan">
                            <small style="font-size: 9px;">
                                <center><?php /*echo $tmp_valuw[$i]['SIMBOL']; */ ?></td>-->
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
        };
        ?>
        </tbody>
    </table>
    <br>
    <table width='100%'>
        <tr>
            <td colspan="3"><p style='font-size:10px;'>
                    <?php if ($data['detail'][0]['NM_PRODUCT'] == 'CLINKER') { ?>
                        <i>We certifying that the clinker described above is suitable for production of cement
                            conforming to <?php echo $data['detail'][0]['NM_STANDART']; ?></i>
                    <?php } else {
                        ; ?>
                        <i>We certifying that the cement described above is compliance with specification of SNI 2049 :
                            2015
                            (<?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?>)</i>
                    <?php }; ?>
                    <!--                THE -->
                    <?php //echo $data['detail'][0]['NM_PRODUCT']; ?><!-- IS SUITABLE FOR THE-->
                    <!--                PRODUCTION OF CEMENT CONFORMING TO -->
                    <?php //echo $data['detail'][0]['NM_STANDART']; ?><!--</p>-->
                    <br>
            </td>
        </tr>
        <br>
        <tr>
            <td width="67%" style="vertical-align: top">
                <p style="font-size: 10px"><br>Note :<br><br>
                </p>
                <p style="font-size: 10px">
                    <i><?php
                        if ($data['detail'][0]['FORMULAS'] == '') {
                            echo '';
                        } else {
                            echo $data['detail'][0]['FORMULAS'];
                        }; ?> </i>
                    <?php if ($data['detail'][0]['NM_PRODUCT'] == 'CEMENT') { ?>
                        <i>*) Additional chemical requirements <br>**) Additional physical requirements</i>
                    <?php } else {
                        ; ?>
                    <?php }; ?>
                </p>
            </td>
            <td width="3%"></td>
            <td width="30%" style="text-align: center">
                <small style="font-size: 10px;"><?php echo $data['detail'][0]['NM_PLANT']; ?>
                    , <?php echo date('d F Y') ?> </small>
                <br><br>
                <small style="padding-bottom: 2px"><img
                        src='https://api.qrserver.com/v1/create-qr-code/?data=Nama Spec :<?php echo $approve['Approval'][0]['NM_SPEC']; ?> Kode Spec :<?php echo $approve['Approval'][0]['KD_SPEC']; ?>\n Diapprove oleh :<?php echo $approve['Approval'][0]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][0]['TANGGAL']; ?>\n Manager approve oleh:<?php echo $approve['Approval'][1]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][1]['TANGGAL']; ?>\n Senior Manager approve oleh:<?php echo $approve['Approval'][2]['FULLNAME']; ?>\nTanggal:<?php echo $approve['Approval'][2]['TANGGAL']; ?>&amp;size=80x80'
                        alt='' title=''/></small>
                <br>
                <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN PADANG</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN INDONESIA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN TONASA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                    ?>
                    <small style="font-size:10px;"> MANAGER OF SEMEN GRESIK</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN THANGLONG</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                    ?>
                    <small style="font-size:10px;"> SM OF SOLUSI BANGUN INDONESIA</small>
                    <?php

                } ?>
            </td>
        </tr>
        <tr>
            <div class="kotak1">

                <img src="assets/image/gobeyondnext.PNG" width="100"/>
            </div>
            <td style="text-align: right">
                <div class="kotak"> In accordance to the regulation applied in PT. Semen Indonesia (Persero), Tbk,<br>
                    all digitally signed documents no longer need manual signature verification
                </div>
            </td>
        </tr>

    </table>
    <div class="kotakbg">
        <img src="assets/image/footer-bg.png" height="35px" width="793.70088739539px;"/>
    </div>
    </body>
    </html>
    </body>
    </html>
    <?php
} else {
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Certificate of Quality <?php echo $data['detail'][0]['NM_COMPANY']; ?></title>
        <style type="text/css">
            .garis {
                border: 1px solid black;
            }

            .garistitik {
                border-bottom: 1px dotted black;
                border-right: 1px solid black;
            }

            .hanyaTitik {
                border-bottom: 1px dotted black;
            }

            .kanan {
                border-right: 1px solid black;
            }

            .atas {
                border-top: 1px solid black;
            }

            table #utama {
                border: 1px solid black;
            }

            #utama th {
                height: 20px;
            }

            #utama tr td {
                height: 18px;
                /*border: 1px solid red;*/
            }

            .kotak {
                width: 300px;
                height: 25px;
                /*background-color: lightgrey;*/
                border: 1px solid black;
                margin: 2px;
                text-align: center;
                font-size: 8px;
                align-content: right;
                position: absolute;
                right: 50px;
                bottom: 10px;
            }
        </style>
    </head>
    <body>
    <table width="100%" style="border-collapse: collapse; border: none; margin-bottom: 5px;">
        <tr>
            <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                ?>
                <td align="left"><img src="assets/image/padang.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                ?>
                <td align="left"><img src="assets/image/logo_sgja.jpg" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                ?>
                <td align="left"><img src="assets/image/tonasa.png" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                ?>
                <td align="left"><img src="assets/image/gresik.png" width="80" height="68" class="rounded-circle"/></td>
                <?php

            } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                ?>
                <td align="left"><img src="assets/image/tanglong.png" width="80" height="68" class="rounded-circle"/>
                </td>
                <?php
            } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                ?>
                <td align="left"><img src="assets/image/SBI.PNG" width="80" height="68" class="rounded-circle"/></td>
                <?php
            } ?>


            <td align="right" style="padding-top: 10px;"><img src="assets/image/smig.png" width="150"
                /><img src="assets/image/Loog-kualitas.png" width="70"
                       height="60"/></td>
        </tr>
    </table>
    <hr style="height: 2px;border: 0; box-shadow: inset 0 12px 12px -12px; padding: 0px;margin: 0px;">
    <br><br>
    <table width="100%" style="border-collapse: collapse; border: none; margin-top: 6px;">
        <tr>


            <!-- <td align="center"><h4><b>CERTIFICATE OF QUALITY <?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?></b> -->
            <td align="center" style="padding-left: 30px"><h2><b><u>CERTIFICATE OF QUALITY (COQ)</u></b></h2>
            </td>

        </tr>
        <tr>
            <?php if (count($data['detail'][0]['NM_COMPANY']) != 1) {
                ?>
                <td></td>
                <?php
            } else {
                ?>
                <td align="center"><h4><b>PT <?php echo $data['detail'][0]['NM_COMPANY']; ?>
                            (<?php echo $data['detail'][0]['NM_PLANT']; ?>)</b></h4></td>
                <?php
            } ?>

        </tr>

    </table>
    <table width="40%" align="center" style="border-collapse: collapse; border: none;">
        <tr>

            <td style="text-align: center;">
                <small style="font-weight: bold">Certificate No :</small>
                <small style="font-weight: bold;font-size: 12px;color: midnightblue;"><?php echo $data['detail'][0]['KD_QUALITY']; ?>
                    /<?php echo date('m') ?>/<?php echo $data['detail'][0]['KODE_BRAND']; ?></small>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; font-weight: bold">
                <small>Brand : <?php
                    if ($data['detail'][0]['NM_BRAND'] == '') {
                        echo '&nbsp;&nbsp;  -';
                    } else {
                        echo $data['detail'][0]['NM_BRAND'];

                    }; ?></small>
            </td>
        </tr>
    </table>
    <table width="200px" style="border-collapse: collapse; border: none; max-width: 200px">
        <tr>
            <td width="70px" style="text-align: right">
                <small style="font-size: 10px;">Date Produce</small>
            </td>
            <td width="5px">
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['START_PRODUCE']; ?></small>
            </td>

        </tr>
        <tr>
            <td style="text-align: right;">
                <small style="font-size: 10px; align-content: right">To</small>
            </td>
            <td>
                <small style="font-size: 10px;">:</small>
            </td>
            <td>
                <small style="font-weight: normal;font-size: 10px;color: midnightblue;"><?php echo $data['detail'][0]['END_PRODUCE']; ?></small>
            </td>

        </tr>
    </table>

    <table id="utama" width="100%" style="border-collapse: collapse;border: 1px solid black;">
        <thead>
        <tr>
            <!--        <th rowspan='2' colspan="4" width="52%" class="garis">-->
            <th colspan="4" width="52%" class="garis">

                <center style="font-size: 12px;"><p>QUALITY PARAMETERS</p></center>
            </th>
            <!--            <th align='center' rowspan='2' colspan="3" width='40%'>-->
            <!--            </th>-->

            <th align='center' colspan='3' width='48%' class="garis">
                <center>
                    <small
                        style="font-size: 12px;"><?php echo $data['detail'][0]['NM_PRODUCT_TYPE']; ?><?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?></small>
                </center>
            </th>
        </tr>
        <!--<tr>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TESTING METHOD</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>TEST RESULT</small>
                </center>
            </th>
            <th align='center' width='13%'>
                <center>
                    <small style='font-size: 8px;'>SPECIFICATION</small>
                </center>
            </th>
        </tr>-->
        </thead>
        <tbody>
        <tr>

            <!--            <td></td>-->
            <th colspan='3' width="44%" class="garis" style="text-align: left">
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>I.CHEMICAL COMPOSITION : </b></small>
            </th>

            <th width="8%" class="garis">
                <center>
                    <small style="font-size: 10px;"><b>UNIT</b></small>
                </center>
            </th>
            <th align='center' width='17%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>TESTING METHOD</b></small>
                </center>
            </th>
            <th align='center' width='14%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>TEST RESULT</b></small>
                </center>
            </th>
            <th align='center' width='17%' class="garis">
                <center>
                    <small style="font-size: 10px;"><b>SPECIFICATION</b></small>
                </center>
            </th>
        </tr>

        <?php $no = 0;
        foreach ($list_par as $value) {

            if (empty($value['NILAI_STD'])) {
                $value['NILAI_STD'] = '-';
                $value['MARK'] = '';
            }
            if (empty($value['NILAI_SPEC'])) {
                $value['NILAI_SPEC'] = '-';
                $value['MARK'] = '';
            }
            if ($value['ID_KATEGORI'] == '1') {
                ?>
                <tr>

                    <?php if (count($value['NAMA_UJI']) == '0') {
                        ?>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php
                    } else {
                        $no++;
                        ?>
                        <td width="5%" class="garistitik">
                            <small style="font-size: 9px;">
                                <center><?php echo $no; ?></center>
                            </small>
                        </td>
                        <?php if (count($p['NAMA_PARENT']) == '0') {
                            ?>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;">&nbsp; <?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;" class="hanyaTitik kanan">
                                <small style=" font-size: 9px;">
                                    (<?php echo $value['SIMBOL']; ?>)
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px; color: midnightblue;">

                                    <center><?php echo $value['NILAI_SPEC']; ?></center>

                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        } else {
                            ?>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;"><?php echo $value['NAMA_PARENT']; ?>
                                    <br><?php echo $value['NAMA_UJI']; ?></small>
                            </td>
                            <td style="border-left-width: 0px;border-left-style: solid;" class="hanyaTitik kanan">
                                <small style="font-size: 9px;"></small>
                                <center><?php echo $value['SIMBOL']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px; color: midnightblue;">

                                    <center><?php echo $value['NILAI_SPEC']; ?></center>

                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $value['MARK']; ?>
                                        &nbsp;<?php echo $value['NILAI_STD']; ?></center>
                                </small>
                            </td>
                            <?php
                        }

                    } ?>

                </tr>
                <?php
            }
        } ?>
        <tr>
            <td class="kanan"></td>
            <td colspan="2" class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
            <td class="kanan"></td>
        </tr>
        <tr>
            <th colspan='3' class="garis" style="text-align: left">
                <small style="font-size: 10px;">&nbsp;&nbsp;<b>II.PHYSICAL PROPERTIES : </b></small>
            </th>
            <th class="garis"></th>
            <th class="garis"></th>
            <th class="garis"></th>
            <th class="garis"></th>
        </tr>
        <?php $no = 1;
        $tmp = array();
        foreach ($list_par as $value) {
            $tmp[$value['NAMA_PARENT']][] = $value;
        }
        foreach ($tmp as $tmp_valuw) {
            for ($i = 0; $i < sizeof($tmp_valuw); $i++) {
                if ($tmp_valuw[$i]['ID_KATEGORI'] == '2') {
                    if ($tmp_valuw[$i]['NAMA_PARENT'] == '') {
                        ?>
                        <tr>
                            <td class="garistitik">
                                <small style="font-size: 9px;">
                                    <center><?php echo $no++; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik">
                                <small style="font-size: 9px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SIMBOL']; ?></td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                        </tr>
                    <?php } else {
                        if ($i == 0) { ?>
                            <tr>
                                <td class="garistitik">
                                    <small style="font-size: 9px;">
                                        <center><?php echo $no++; ?></center>
                                    </small>
                                </td>
                                <td colspan="2" class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        &nbsp;<?php echo $tmp_valuw[$i]['NAMA_PARENT']; ?></small>
                                </td>
                                <!--                                <td>--><?php //echo $tmp_valuw[$i]['NAMA_PARENT']
                                ?><!--</td>-->
                                <!--<td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center>
                                        </td>-->
                                <td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center></center>
                                    </small>
                                </td>

                                <td class="hanyaTitik kanan">
                                    <small style="font-size: 9px;">
                                        <center></center>
                                    </small>
                                </td>
                                <td class="hanyaTitik kanan">
                                </td>
                                <td class="hanyaTitik kanan">
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td class="garistitik"></td>
                            <td colspan="2" class="hanyaTitik kanan">
                                <small style="font-size: 9px;">&nbsp;<?php echo $tmp_valuw[$i]['NAMA_UJI']; ?></small>
                            </td>
                            <!-- <td class="hanyaTitik kanan">
                            <small style="font-size: 9px;">
                                <center><?php /*echo $tmp_valuw[$i]['SIMBOL']; */ ?></td>-->
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['SATUAN']; ?></center>
                                </small>
                            </td>

                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['NM_METODE']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;color: midnightblue;">
                                    <center><?php echo $tmp_valuw[$i]['NILAI_SPEC']; ?></center>
                                </small>
                            </td>
                            <td class="hanyaTitik kanan">
                                <small style="font-size: 9px;">
                                    <center><?php echo $tmp_valuw[$i]['MARK']; ?>
                                        &nbsp;<?php echo $tmp_valuw[$i]['NILAI_STD']; ?></center>
                                </small>
                            </td>
                        </tr>
                        <?php
                    }
                }
            }
        };
        ?>
        </tbody>
    </table>
    <br>
    <table width='100%'>
        <tr>
            <td colspan="3"><p style='font-size:10px;'>
                    <?php if ($data['detail'][0]['NM_PRODUCT'] == 'CLINKER') { ?>
                        <i>We certifying that the clinker described above is suitable for production of cement
                            conforming to <?php echo $data['detail'][0]['NM_STANDART']; ?></i>
                    <?php } else {
                        ; ?>
                        <i>We certifying that the cement described above is compliance with specification of SNI 2049 :
                            2015
                            (<?php echo $data['detail'][0]['KD_PRODUCT_TYPE']; ?>)</i>
                    <?php }; ?>
                    <!--                THE -->
                    <?php //echo $data['detail'][0]['NM_PRODUCT']; ?><!-- IS SUITABLE FOR THE-->
                    <!--                PRODUCTION OF CEMENT CONFORMING TO -->
                    <?php //echo $data['detail'][0]['NM_STANDART']; ?><!--</p>-->
                    <br>
            </td>
        </tr>
        <br>
        <tr>
            <td width="67%" style="vertical-align: top">
                <p style="font-size: 10px"><br>Note :<br><br>
                </p>
                <p style="font-size: 10px">
                    <i><?php
                        if ($data['detail'][0]['FORMULAS'] == '') {
                            echo '';
                        } else {
                            echo $data['detail'][0]['FORMULAS'];
                        }; ?> </i>
                    <?php if ($data['detail'][0]['NM_PRODUCT'] == 'CEMENT') { ?>
                        <i>*) Additional chemical requirements <br>**) Additional physical requirements</i>
                    <?php } else {
                        ; ?>
                    <?php }; ?>
                </p>
            </td>
            <td width="3%"></td>
            <td width="30%" style="text-align: center">
                <small style="font-size: 10px;"><?php echo $data['detail'][0]['NM_PLANT']; ?>
                    , <?php echo date('d F Y') ?> </small>
                <br><br>
                <small style="padding-bottom: 2px"><img
                        src='https://api.qrserver.com/v1/create-qr-code/?data=Nama Spec :<?php echo $approve['Approval'][0]['NM_SPEC']; ?> Kode Spec :<?php echo $approve['Approval'][0]['KD_SPEC']; ?>\n Diapprove oleh :<?php echo $approve['Approval'][0]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][0]['TANGGAL']; ?>\n Manager approve oleh:<?php echo $approve['Approval'][1]['FULLNAME']; ?>\n Tanggal:<?php echo $approve['Approval'][1]['TANGGAL']; ?>\n Senior Manager approve oleh:<?php echo $approve['Approval'][2]['FULLNAME']; ?>\nTanggal:<?php echo $approve['Approval'][2]['TANGGAL']; ?>&amp;size=80x80'
                        alt='' title=''/></small>
                <br>
                <?php if ($data['detail'][0]['ID_COMPANY'] == '8') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN PADANG</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '10') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN INDONESIA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '9') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN TONASA</small>
                    <?php
                } elseif ($data['detail'][0]['ID_COMPANY'] == '7') {
                    ?>
                    <small style="font-size:10px;"> MANAGER OF SEMEN GRESIK</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '11') {
                    ?>
                    <small style="font-size:10px;"> SM OF SEMEN THANGLONG</small>
                    <?php

                } elseif ($data['detail'][0]['ID_COMPANY'] == '12') {
                    ?>
                    <small style="font-size:10px;"> SM OF SOLUSI BANGUN INDONESIA</small>
                    <?php

                } ?>
            </td>
        </tr>
        <tr>
            <td>
                <div class="kotak1">

                    <!--                    <img src="assets/image/gobeyondnext.PNG" width="100"/>-->
                </div>
            </td>
            <td style="text-align: right">
                <div class="kotak"> In accordance to the regulation applied in PT. Semen Indonesia (Persero), Tbk,<br>
                    all digitally signed documents no longer need manual signature verification
                </div>

            </td>
        </tr>
    </table>
    <div class="kotakbg">
        <img src="assets/image/footer-bg.png" height="35px" width="793.70088739539px;"/>
    </div>
    </body>
    </html>
    </body>
    </html>
    <?php
} ?>
