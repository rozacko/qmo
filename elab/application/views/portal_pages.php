<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>QA - Landing Page</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Animation CSS -->
    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <style>
        .feature {
            margin-bottom: 20px;
            text-align: center;
            border: 1px solid #e1e8ec;
            border-radius: 10px;
            padding: 20px;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }

        .feature .feature-image {
            margin-bottom: 0px;
        }

        .feature .feature-title {
            font-size: 30px;
            font-size: 2em;
            font-weight: 300;
        }

        .feature p {
            font-size: 16px;
            font-size: 1.0666666667em;
            margin-bottom: 30px;
        }
    </style>
</head>
<body id="page-top" class="landing-page">
<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url() ?>" style="background-color: #F1F1F1">
                    <img src="<?php echo base_url(); ?>assets/image/Logo SIG.png" width="100" alt="">

                    <br>
                    <!-- SHE -->
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
<!--                    <li><img class="nav-icon" src="--><?php //echo base_url(); ?><!--assets/images/Logo-Semen-Tonasa.png"-->
<!--                             width="60">-->
<!--                    </li>-->
<!--                    <li><img class="nav-icon" src="--><?php //echo base_url(); ?><!--assets/images/Logo-Semen-Padang.png"-->
<!--                             width="60">-->
<!--                    </li>-->
<!--                    <li><img class="nav-icon" src="--><?php //echo base_url(); ?><!--assets/images/Logo-Semen-Gresik.png"-->
<!--                             width="60">-->
<!--                    </li>-->
<!--                    <li><img class="nav-icon" src="--><?php //echo base_url(); ?><!--assets/images/sbi-logo.png"-->
<!--                             width="60" height="58">-->
<!--                    </li>-->
                    <!-- <li><a class="page-scroll" href="home.html">Beranda</a></li> -->
                    <!--                    <li><a class="page-scroll menu_login" href="Portal_pages" style="margin: -2px 0; right: -30px;"><i-->
                    <!--                                    class="fa fa-lock"></i>Login</a></li>-->
                </ul>
            </div>
        </div>
    </nav>

</div>
<section id="features" class="container services" style="padding-bottom: -10px">
</section>
<section id="features" class="container services" style="padding-bottom: -10px">
</section>
<div class="row">
   <a href="http://qmo.semenindonesia.com/">
        <div class="col-md-6">
            <div class="banner1">
            </div>
    </a></div>
<div class="col-md-6"><a href="<?php echo base_url() ?>login">
        <div class="banner2"></div>
    </a></div>
</div>

<section id="features" class="container services" style="padding-bottom: -10px">
    <div class="row">
        <div class="col-md-12">
            <div class="feature">
                <img src="<?php echo base_url(); ?>assets/images/Logo-Semen-Tonasa.png" width="80" alt=""
                     class="feature-image">
                <img src="<?php echo base_url(); ?>assets/image/LOGO SP.png" width="75"  alt=""
                     class="feature-image" style='padding-top: 15px;'>
                <img src="<?php echo base_url(); ?>assets/images/Logo-Semen-Gresik.png" width="80" alt=""
                     class="feature-image">
                <img src="<?php echo base_url(); ?>assets/images/sbi-kotak.png" height="58" alt=""
                     class="feature-image">
                <img src="<?php echo base_url(); ?>assets/images/andallas.png" height="60" alt="" class="feature-image">
                <!--                <h2 class="feature-title">Medicice</h2>-->
                <!--                <p>Laborum et dolorum fuga harum quidem rerum facilis et expedita distinctio nam libero tempore.</p>-->
            </div>
        </div>
    </div>
</section>

<section id="contact" class="gray-section contact" style="margin-top: 0px;">
    <div class="container">
        <div class="row m-b-lg">
            <div class="col-lg-12 text-center">
                <div class="navy-line"></div>
                <h1>Contact Us</h1>
                <p>PT. Semen Gresik Pabrik Tuban, </p>
            </div>
        </div>
        <div class="row m-b-lg" align="center">
            <div class="col-lg-3 col-lg-offset-3">
                <address>
                    <strong><span class="navy">Kantor Pusat (KPSG),</span></strong><br/>
                    Desa Sumberarum, Kec. Kerek,
                    <br/>
                    Kabupaten Tuban,
                    Jawa Timur 62356<br/>
                    <abbr title="Phone">P:</abbr> + 62-356-325-001/2/3
                    <br><abbr title="Phone">E:</abbr> + 62-356-323-80
                </address>
            </div>
            <div class="col-lg-4">
                <p class="text-color">

                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
                <p><strong>&copy; 2019 PT. Semen Indonesia (Persero) Tbk.</strong><br/> Designed by PT SiSi (Sinergi
                    Informatika Semen Indonesia).</p>
            </div>
        </div>
    </div>
</section>

<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/wow/wow.min.js"></script>


<style>
    .banner1 {
        height: 470px;
        width: 100%;
        /*background: url('../assets/images/header_one.jpg') 50% 0 no-repeat;*/
        background: url(<?= base_url()?>assets/images/banner2.PNG) 50% 0 no-repeat;
    }

    .banner2 {
        height: 470px;
        width: 100%;
        /*background: url('../assets/images/header_one.jpg') 50% 0 no-repeat;*/
        background: url(<?= base_url()?>assets/images/banner1.PNG) 50% 0 no-repeat;
    }

    .banner3 {
        height: 470px;
        width: 100%;
        /*background: url('../assets/images/header_one.jpg') 50% 0 no-repeat;*/
        background: url(<?= base_url()?>assets/images/lab3.jpg) 50% 0 no-repeat;
    }

    .banner4 {
        height: 470px;
        width: 100%;
        /*background: url('../assets/images/header_one.jpg') 50% 0 no-repeat;*/
        background: url(<?= base_url()?>assets/images/lab2.jpg) 50% 0 no-repeat;
    }
</style>

<script>

    $(document).ready(function () {

        $('body').scrollspy({
            target: '.navbar-fixed-top',
            offset: 80
        });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function (event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
    });

    var cbpAnimatedHeader = (function () {
        var docElem = document.documentElement,
            header = document.querySelector('.navbar-default'),
            didScroll = false,
            changeHeaderOn = 200;

        function init() {
            window.addEventListener('scroll', function (event) {
                if (!didScroll) {
                    didScroll = true;
                    setTimeout(scrollPage, 250);
                }
            }, false);
        }

        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                $(header).addClass('navbar-scroll')
            }
            else {
                $(header).removeClass('navbar-scroll')
            }
            didScroll = false;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        init();

    })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();

</script>

</body>
</html>
