
		</div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.js"></script>
     <script> 
            $('.load-screen').delay(1000).fadeOut(function(){$(this).remove()});
        $(document).ajaxStart(function(){ 
          $(".load-screen").show(); 
        });
        $(document).ajaxComplete(function(){ 
          $(".load-screen").hide(); 
        });
          
            if(write_menu==""){ 
                $(".btn_tambah").hide(); 
            }else{
                $(".btn_tambah").show(); 
            } 
            console.log(write_menu)
        $(document).ready(function(){
             
        
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000,
                //positionClass: 'toast-top-center'
            };
            $(".nav a").on("click", function(){
               $(".nav").find(".active").removeClass("active"); 
               $(this).parent().addClass("active");
            });
            
         
        });
        
        
        function show_toaster($type, $title, $mess)
        {
            if($type == 2)
                toastr.error($title,$mess);
            else if($type == 1)
                toastr.success($title,$mess);
            else if($type == 3)
                toastr.warning($title,$mess);
        }
        

        function informasi(tipe, pesan) {
            var judul = "<b><i class='fa fa-info-circle'></i>&nbsp;Informasi</b>";
            if(tipe == BootstrapDialog.TYPE_WARNING) {
                judul = "<b><i class='fa fa-exclamation-circle'></i>&nbsp;Kesalahan</b>";
            } else if(tipe == BootstrapDialog.TYPE_DANGER) {
                judul = "<b><i class='fa fa-times-circle'></i>&nbsp;Gagal</b>";
            } else if(tipe == BootstrapDialog.TYPE_SUCCESS) {
                judul = "<b><i class='fa fa-check-circle'></i>&nbsp;Berhasil</b>";
            }
            BootstrapDialog.show({
                "type": tipe,
                "title": judul,
                "message": pesan,
                "buttons": [{
                    "cssClass": "btn btn-default btn-xs",
                    "icon": "fa fa-check",
                    "label": "OK",
                    "action": function(dialogRef) {
                        dialogRef.close();
                    }
                }]
            });
        }

     </script>
</body>
</html>
