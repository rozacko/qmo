
<div class="load-screen"></div>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" style="    width: 50px;    height: 50px;" src="https://cdn.pixabay.com/photo/2016/04/15/18/05/computer-1331579_960_720.png">
                    <!-- <a data-toggle="dropdown" class="dropdown-toggle" href="#" >-->
                    <a  href="#" >
                        <span class="block m-t-xs font-bold"><?php echo (isset($this->session->userdata("USER")->FULLNAME) ? $this->session->userdata("USER")->FULLNAME : "Belum Login") ?></span>
                        <!--<span class="text-muted text-xs block"><?php echo (isset($this->session->userdata("USER")->NAMA_ROLE) ? $this->session->userdata("USER")->NAMA_ROLE : "Role") ?> <b class="caret"></b></span> -->
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="profile.html">Profile</a></li>
                        <li><a class="dropdown-item" href="contacts.html">Contacts</a></li>
                        <li><a class="dropdown-item" href="mailbox.html">Mailbox</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="login.html">Logout</a></li>
                    </ul>
                    <!-- <input type="checkbox"  data-toggle="toggle"> -->
                </div>
            </li>
            <!--
            <li  >
                <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="index.html">Dashboard v.1</a></li>
                    <li><a href="dashboard_5.html">Dashboard v.5 <span class="label label-primary pull-right">NEW</span></a></li>
                </ul>
            </li> 
            <li>
                <a href="#"><i class="fa fa-table"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url(); ?>master/menu">Menu</a></li>
                    <li><a href="<?php echo base_url(); ?>master/role">Role</a></li>
                    <li><a href="<?php echo base_url(); ?>master/kelompok">Kelompok</a></li>
                    <li><a href="<?php echo base_url(); ?>master/standart">Standart Uji</a></li>
                    <li><a href="<?php echo base_url(); ?>master/contoh">Contoh</a></li> 
                    <li><a href="<?php echo base_url(); ?>master/c_m_kemasan">Kemasan</a></li>
                    <li><a href="<?php echo base_url(); ?>master/c_m_uji">Uji</a></li>
                    <li><a href="<?php echo base_url(); ?>master/c_m_uji_limbah">Uji Limbah</a></li> 
                </ul>
            </li> 
            -->
            
            
            <?php
						$arr = array();
						foreach($menu as $m) {
							$m["controller"] = explode("/", $m["CONTROLLER_MENU"]);
							$m["controller"] = end($m["controller"]);
							if($m["PARENT_MENU"] == "") {
								$arr[$m["ID_MENU"]] = array($m, array());
								if(isset($arr[$m["ID_MENU"]])) {
									$arr[$m["ID_MENU"]][0] = $m;
								} else {
									$arr[$m["ID_MENU"]] = array($m, array());
								}
							} else {
								if(isset($arr[$m["PARENT_MENU"]])) {
									$arr[$m["PARENT_MENU"]][1][] = $m;
								} else {
									$arr[$m["PARENT_MENU"]] = array(null, array());
									$arr[$m["PARENT_MENU"]][1][] = $m;
								}
							}
						}
                        // echo json_encode($arr);
                        
						$akses = $this->session->userdata("AKSES"); 
						$read = $this->session->userdata("READ"); 
						$write = $this->session->userdata("WRITE");
                        $read_menu = ''; $write_menu = '';                        
						foreach($arr as $r) {
							if(count($r[1]) == 0) { 
								echo isset($akses[$r[0]["ID_MENU"]]) ? "<li class='" . activate_menu($r[0]["controller"]) . "'><a href='" . base_url() . $r[0]["LINK_MENU"] . "'><i class='material-icons text-{$r[0]["ICON_COLOR"]}'><i class='{$r[0]["ICON_MENU"]} icon-menu'></i></i> <span>{$r[0]["NAMA_MENU"]}</span></a></li>" : "";
                                  if($r[0]["LINK_MENU"]==uri_string()){
                                       $read_menu = isset($akses[$r[0]["ID_MENU"]]) && $read[$r[0]["ID_MENU"]]=="Y" ? "Y" : "";
                                        $write_menu = isset($akses[$r[0]["ID_MENU"]]) && $write[$r[0]["ID_MENU"]]=="Y" ? "Y" : "";
                                  } 
							} else {
								$sub = "";
								$utama = "<li class='";
								$total_sub = 0;
								foreach($r[1] as $sm) {  
                                        if($sm["LINK_MENU"]==uri_string()){
                                            $aktif= 'active';
                                            $read_menu = isset($akses[$sm["ID_MENU"]]) && $read[$sm["ID_MENU"]]=="Y" ? "Y" : "";
                                            $write_menu = isset($akses[$sm["ID_MENU"]]) && $write[$sm["ID_MENU"]]=="Y" ? "Y" : "";
                                            $this->session->set_userdata("tes",$write_menu);
                                        }else{
                                            $aktif= '';
                                        }
									// $utama .= activate_menu($sm["controller"]); 
									$utama .= $aktif; 
									$sub .= isset($akses[$sm["ID_MENU"]]) ? "<li class='".$aktif."'><a href='".base_url().$sm["LINK_MENU"]."'>{$sm["NAMA_MENU"]}</a></li>" :"";
									$total_sub += isset($akses[$sm["ID_MENU"]]) ? 1 : 0;
								}
								$utama .= "
									'>
                                        <a href='#'><i class='material-icons text-{$r[0]["ICON_COLOR"]}'><i class='{$r[0]["ICON_MENU"]}'></i></i> <span class='nav-label'>{$r[0]["NAMA_MENU"]}</span><span class='fa arrow'></span></a>
									 <ul class='nav nav-second-level'>{$sub}</ul>
									</li>
								";
								echo $total_sub > 0 ? $utama : "";
							}
						}
                        //<a href='javascript:;'><span class='menu-caret'><i class='material-icons'>arrow_drop_down</i> </span><i class='material-icons text-{$r[0]["ICON_COLOR"]}'><i class='{$r[0]["ICON_MENU"]} icon-menu'></i></i> <span>{$r[0]["NAMA_MENU"]}</span></a>
								 	
               ?>
        </ul>
    </div>
</nav>

            <script>
            var read_menu = "<?php echo $read_menu  ?>";
            var write_menu = "<?php echo $write_menu  ?>";
            
            </script>
<div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message"><?php echo (isset($this->session->userdata("USER")->USERNAME) ? $this->session->userdata("USER")->USERNAME : "Belum Login") ?></span>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>login/doLogout" class="tooltip-btn" data-toggle="tooltip" data-placement="bottom" title="Logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>