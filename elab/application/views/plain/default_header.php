
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?> - E-Lab</title>
    <meta name="description" content="Develop wep apps programming language">
    <meta name="author" content="PT. Sisi"> 
    <?php 
        $this->ci = &get_instance();
        $this->ci->load->library('Htmllib');
        $this->ci->htmllib->declare_css();
        $this->ci->htmllib->declare_font_awesome();
        $this->ci->htmllib->declare_js(); 

    ?> 

    <style>
   .load-screen {
        width: 100%;
        height: 100%;
        background: url("https://cormullion.github.io/assets/images/slackmojif/slackanimation.gif") no-repeat center center #fff;
        position: fixed;
        opacity: 0.7;
        z-index : 999;
    }

    </style>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.15/dist/summernote.min.css" rel="stylesheet">
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.1/js/dataTables.fixedColumns.min.js"></script>-->
</head>
<body>
<div id="wrapper">