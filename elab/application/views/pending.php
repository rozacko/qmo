
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    .form-group {
        margin-bottom : 0px;
    }
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script>
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> SPUC Pending Penyelia</h2>
            <div style="text-align:left">
                &nbsp;&nbsp;&nbsp;<button style='display:none' type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><i class="fa fa-file-excel-o">&nbsp;Export Excel</i></button>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:75%" id="tabel">
                        <thead>
                            <tr>
                                <th rowspan='2' style='width:10px;'>No</th>
                                <th rowspan='2' >Detail SPUC  </th>
                                <th rowspan='2' >NO SPUC  </th>
                                <th rowspan='2' >CITO   </th>
                                <th rowspan='2' >Tanggal Terima  </th>
                                <th rowspan='2' >Nama Contoh   </th>
                                <th rowspan='2' >Keterangan   </th>
                                <th colspan='2' >SURAT PERINTAH UJI CONTOH   </th>
                                <th rowspan='2' >Analis    </th>
                                <th colspan='2' >HASIL PENGUJIAN   </th>
                                <th rowspan='2' ></th> 
                                <th rowspan='2' ></th> 
                                <th rowspan='2' >Aksi</th> 
                            </tr>
                            <tr>
                                <th   >MT    </th>
                                <th  >Penyelia    </th>
                                <th  >Penyelia    </th>
                                <th   >MT    </th>

                            </tr>
                            <tr>
                              <th></th>
                              <th></th>
                              <td>NO SPUC</td>
                              <td>CITO</td>
                              <td>Tanggal Terima</td>
                              <td>Nama Contoh</td>
                              <td>Keterangan</td>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>

                            </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>
                 <hr>
                  <center><p> </p> </center>
                  <center> <label class="check-label"><i class=" "></i><input type="radio" name="approve"  id="approve" value="Y">&nbsp;Setuju</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="approve" id="approve" value="N">&nbsp;Tolak</label>
                  </center>
                  <input type="hidden" name="id_transaksi" id="id_transaksi" >
                  <input type="hidden" name="id_kategori" id="id_kategori" >
                  <input type="hidden" name="id_status" id="id_status" >
                  <p>Keterangan :</p>
                   <div class="row">  <div class="col-lg-12">
                        <textarea name="syarat" id='syarat' style="width:100%"></textarea>
                    </div>
                    </div>
			</div>
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-check'></i>&nbsp;OK</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){
    $('#tabel thead td').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
        tabel_view()

});

	function refreshKelompok() {
		$.post("<?php echo base_url(); ?>master/contoh/refreshKelompok", function(data) {
			$("#kelompok").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 13 },
                    { "visible": false, "targets": 12 },
                  ],
                "destroy": true,
                'dom': 'Bfrtip',
                'buttons': [
                    {extend: 'copy', exportOptions: {columns: [0, 2, 3, 4, 5, 6]}},
                    {extend: 'excel', exportOptions: {columns: [0, 2, 3, 4, 5, 6]}},
                    {extend: 'pdf', exportOptions: {columns: [0, 2, 3, 4, 5, 6]}},
                    {extend: 'print', exportOptions: {columns: [0, 2, 3, 4, 5, 6]}},
                ],
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "orderCellsTop": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'spuc' },
                  { data : 'no_spuc' },
                  { data : 'cito' },
                  { data : 'tgl_terima' },
                  { data : 'contoh' },
                  { data : 'keterangan' },
                  { data : 'spuc_mt' },
                  { data : 'spuc_penyelia' },
                  { data : 'analis' },
                  { data : 'uji_penyelia' },
                  { data : 'uji_mt' },
                  { data : 'id' }, 
                  { data : 'id_kategori' },
                  { data : 'aksi' },
                ],
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/pending/get_data',
                     dataType: 'JSON',
                     dataSrc : function (json) {
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){
                              if(json[i].KETERANGAN_TOLAK == null ){
                                  return_data.push({
                                      'no'	    : no ,
                                      'spuc'	    :   '<center><button onclick="detail_spuc(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-warning btn-xs waves-effect btn_edit" title="Detail KKPP"><span class="btn-labelx"><i class="fa fa-check"></i></span> </button> </center>',
                                      'no_spuc'      :  json[i].NO_BAPPUC,
                                      'cito'          :  json[i].CITO,
                                      'tgl_terima'      :   json[i].TANGGAL,
                                      'contoh'          :  json[i].NAMA_CONTOH ,
                                      'keterangan'          :  json[i].KETERANGAN ,
                                      'spuc_mt'      :   json[i].SPUC_MT,
                                      'spuc_penyelia'          :  json[i].SPUC_PENYELIA ,
                                      'analis'      :   json[i].ANALIS,
                                      'uji_penyelia'          :  json[i].HASIL_PENYELIA ,
                                      'uji_mt'          :  json[i].HASIL_MT, 
                                      'id'          :  json[i].ID_TRANSAKSI ,  
                                      'id_kategori'          : json[i].ID_KATEGORI ,  
                                      'aksi' : '<center><button onclick="edit(\''+ i +'\' )" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-check"></i></span> Pending</button> </center>'
                                    })
                                    no+=1; 
                              }
                            
                          }
                          return return_data;
                    }
                  }
		});
    // Apply the search
            // TabelData.columns().every( function () {
            //     var that = this;
            //
            //     $( 'input', this.header() ).on( 'keyup change clear', function () {
            //         if ( that.search() !== this.value ) {
            //             that
            //                 .search( this.value )
            //                 .draw();
            //         }
            //     } );
            // } );

            $(TabelData.table().container()).on('keyup', 'thead input', function () {
                console.log($(this).data('index') + "-" + this.value);
                TabelData.column($(this).data('index')).search(this.value).draw();
            });

            TabelData.on('order.dt search.dt', function () {
                TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

    }
    
    
     
	function edit(baris, tipe) { 
        $('#input_update').html('<i class="fa fa-check"></i>&nbsp;OK'); 
				$('#input_update').removeAttr("disabled");
		var kolom = TabelData.row(baris).data(); 
        if(tipe=='SPUC'){
            var isi = "<b><i class='fa fa-pencil-alt'></i>&nbsp;Persetujuan SPUC "+kolom['no_spuc']+"</b>";
            var status = 'a1'
        }else{ 
            var isi = "<b><i class='fa fa-pencil-alt'></i>&nbsp;Persetujuan Pending "+kolom['no_spuc']+"</b>";
            var status = 't1'
        }
        
		$("#id_transaksi").val(kolom['id']);
		$("#id_kategori").val(kolom['id_kategori']);
		$("#id_status").val(status);
		$("#spuc").val(kolom['no_spuc']);
		$("#tgl_terima").val(kolom['tgl_terima']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html(isi); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_transaksi").val(); 
		var id_kategori = $("#id_kategori").val(); 
		var approve = $("input[name='approve']:checked").val(); 
		var syarat = $("#syarat").val();  
		var id_status = $("#id_status").val();  
		if( approve == "" 
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
			return;
		} else if( approve == "N"  && syarat == ''
		) {
           informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
			return;
        }

            $('#input_update').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Proses'); 
			$('#input_update').attr("disabled", "disabled");  
		$.post("<?php echo base_url(); ?>pending/update", {"id_kategori": id_kategori, "id": id, "approve": approve, "syarat": syarat, "id_status": id_status }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
                 
				$('#input_update').removeAttr("disabled");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}



</script>
