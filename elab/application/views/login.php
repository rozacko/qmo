<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>E-Laboratory | Login</title>

    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <style type='text/css'>
		@media (min-width: 850px) {
			html {
				background: url("<?php echo base_url(); ?>assets/image/bg-login.jpg") no-repeat top left fixed;
				background-size: 100% 100%;
				min-height: 100%;
				height: 100%;
				overflow: hidden;
			}
		}
        </style>
</head>

<body class="gray-bg" style="margin-top: 50px;background: url("<?php echo base_url(); ?>assets/image/bg-login.jpg") no-repeat top left fixed;opacity:0.1;">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name" style="    font-size: 120px;    color: #c9ffd0;">ELAB</h1>

            </div>
            <h3 style="color:white">Welcome to E-Laboratory</h3>
            
                <form action="<?php echo site_url("login/verification") ?>" method="post">
                <div class="form-group">
                    <input type="text" name="USERNAME" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="PASSWORD" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" style="    background-color: #006947;">Login</button>
                <a href="landing" class="btn btn-danger block full-width m-b" style="    background-color: #006947;">Back To Portal</a>
                
            </form>
            
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

</body>

</html>
