
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    div.dataTables_length{
      margin-right: 0.5em;
      margin-top: 0.2em;
    }
 </style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			 <div class="">
          <div class="col-lg-6">
            <h2><i class="fa fa-list"></i> Master User</h2>
          </div>   
          <div class="col-lg-6">
            <div style="text-align:right;">
              <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>
            </div>      
          </div>
       </div>
  		
        <div class="ibox-content">
            <div class="row">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:89%" id="tabel" width="100%">
                    <thead>
                        <tr>
                            <th  style='width:30px;'>No</th>
                            <th>Name </th>
                            <th>Username </th>
                            <th>Email </th>
                            <th>LDAP </th>
                            <th>Lab / Plant </th>
                            <th>Role </th>
                            <th> </th>
                            <th> </th>
                            <th> </th>
                            <th  style='width:180px;'><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                        </tr>
                        <tr id="filtering">
                            <td  style='widtd:10px;'>No</td>
                            <td>Name </td>
                            <td>Username </td>
                            <td>Email </td>
                            <td>LDAP </td>
                            <td>Lab / Plant </td>
                            <td>Role </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <th  style='widtd:180px;'></th>
                        </tr>
                    </thead>
                    <tbody> 			
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
              <div class="row">
                <div class="col-lg-6">
                    <div class='form-group'>
                        <label>Full Name:</label>
                        <input type='hidden' class='form-control input-xs' id='id_user' >
                        <input type='text' class='form-control input-xs' id='fullname' >
                    </div> 
                </div>
                <div class="col-lg-6">
                    <div class='form-group'>
                        <label>Username:</label> 
                        <input type='text' class='form-control input-xs' id='username' >
                    </div> 
                </div>
              </div>
				
              <div class="row">
                <div class="col-lg-6">
                    <div class='form-group'>
                        <label>Email:</label> 
                        <input type='email' class='form-control input-xs' id='email' >
                    </div> 
                </div>
                <div class="col-lg-6">
                    <div class='form-group'>
                        <label>LDAP:</label>
                        <select class='form-control selectpicker' data-live-search='false' id='ldap'>
                            <option value='' data-hidden='true'  >-- Pilih --</option>
                            <option value='Y'> YES </option>
                            <option value='N'>  NO  </option>
                        </select>
                    </div>   
                </div>
              </div>
              <div class="row">
              <div id="elab" style='display:none'>
                    <div class="col-lg-4"> 
                        <div class='form-group'>
                            <label>Laboratorium:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='lab'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-3"> 
                        <div class='form-group'>
                            <label>Kategori:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='kategori'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Kategori --</option>
                            </select>
                        </div>  
                    </div>
                </div>
              <div id="coq" style='display:none'>
                    <div class="col-lg-4"> 
                        <div class='form-group'>
                            <label>Company:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='company'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>
                            </select>
                        </div>  
                    </div>
                    <div class="col-lg-3"> 
                        <div class='form-group'>
                            <label>Plant:</label>
                            <select class='form-control selectpicker' data-live-search='false' id='plant'>
                                <option value='' data-hidden='true' selected='selected'>-- Pilih Plant --</option>
                            </select>
                        </div>  
                    </div>
                </div>
                <div class="col-lg-3"> 
                    <div class='form-group'>
                        <label>Role:</label>
                        <select class='form-control selectpicker' data-live-search='false' id='role'>
                            <option value='' data-hidden='true' selected='selected'>-- Pilih Role --</option>
                        </select>
                    </div>  
                </div>
                <div class="col-lg-2">
                
                    <div class='form-group'>
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;</label> <br />
                        <a  class='btn btn-warning btn-sm btn-tutup' id='add_rolelab'><i class='fa fa-plus'></i>&nbsp;Add Role</a>
                    </div>  
                </div>
                
              </div>
				
			<div class="row">
                <div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%;display:none" id="tabel_rolelab" >
						<thead>
							<tr>
								<th>Laboratorium</th>
								<th>Role</th> 
								<th>Kategori</th> 
								<th style="width:20px">Hapus</th> 
							</tr>
						</thead>
						<tbody id="isi_tabel_rolelab">
						</tbody>
					</table>
					<table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:100%;display:none" id="tabel_roleplant">
						<thead>
							<tr>
								<th>Company</th>
								<th>Plant</th> 
								<th>Role</th> 
								<th style="width:20px">Hapus</th> 
							</tr>
						</thead>
						<tbody id="isi_tabel_roleplant">
						</tbody>
					</table>
				</div>
            </div>
				
			
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
    var tipeUser = <?php echo json_encode($this->session->userdata("USER")->TYPE) ?>;
$(document).ready(function(){   
    if(tipeUser==0){
        $("#tabel_rolelab, #elab").show()
    }else{
         $("#tabel_roleplant, #coq").show()
    }
    $('#tabel thead td').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+i+'"/>' );
    } );
    
        tabel_view()
        refreshCompany() 
        refreshRole()
        refreshLab()
        refreshKategori() 
     $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
        console.log($(this).data('index')+"-"+this.value);
        TabelData.column( $(this).data('index') ).search( this.value ).draw();
     });
     // $('#tabel_filter').remove();

     // TabelData.on( 'order.dt search.dt', function () {
     //    TabelData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
     //        cell.innerHTML = i+1;
     //    } );
       /// Button Action
	$('#tambah_data').click(function() { 
		$('#isi_tabel_rolelab').html('');
		$('#isi_tabel_roleplant').html('');
        refreshCompany() 
        refreshRole()
        refreshLab()
        refreshKategori() 
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_user, #fullname, #username, #email, #ldap, #lab, #role, #company, #plant").val('');
		$("#dlg").modal("show");
    });
	$('#add_rolelab').click(function() {  
    
        var lab = $("#lab").val()
        var role = $("#role").val()
        var kategori = $("#kategori").val()
        var nm_lab = $("#lab option:selected").attr('nama');  
        var nm_role = $("#role option:selected").attr('nama'); 
        var nm_kategori = $("#kategori option:selected").attr('nama'); 
        if(tipeUser==0){
            $("#tabel_rolelab").find("tbody").append(
                "<tr>"+ 
                
                "<td>"+nm_lab+"</td>"+
                "<td>"+nm_role+"</td>"+  
                "<td>"+nm_kategori+"</td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+lab+"</td>"+
                "<td style='display:none;'>"+role+"</td>"+
                "<td style='display:none;'>"+kategori+"</td>"+
                "</tr>");   
        }else{ 
            var company = $("#company").val()
            var plant = $("#plant").val()
            var nm_company = $("#company option:selected").attr('nama'); 
            var nm_plant = $("#plant option:selected").attr('nama');  
            $("#tabel_roleplant").find("tbody").append(
                "<tr>"+ 
                
                "<td>"+nm_company+"</td>"+
                "<td>"+nm_plant+"</td>"+  
                "<td>"+nm_role+"</td>"+  
                "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                "<td style='display:none;'> </td>"+
                "<td style='display:none;'>"+company+"</td>"+
                "<td style='display:none;'>"+plant+"</td>"+
                "<td style='display:none;'>"+role+"</td>"+
                "</tr>");   
        }
        
    });
    
    $(document).on('click','#btn_filtering', function(){
          var data = $(this).data('filtering');
          if(data==1){
            $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
            $('#filtering').hide(500);

          }else{
            $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
            $('#filtering').show(500);
          }
       });
        
});        
        function hapus_role(elm) { 
			var cld = $(elm).parent().parent(); //tr 
            cld.remove(); 
		}
	function refreshCompany() {
		$.post("<?php echo base_url(); ?>master/user/refreshCompany", function(data) {
			$("#company").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
      $('#company').change(function() {   
             refreshPlant($("#company option:selected").val()) 
          });  
	function refreshPlant(id) {
		$.post("<?php echo base_url(); ?>master/user/refreshPlant", {"id": id}, function(data) {
			$("#plant").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
	function refreshKategori() {
		$.post("<?php echo base_url(); ?>master/user/refreshKategori", function(data) {
			$("#kategori").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
	function refreshLab() {
		$.post("<?php echo base_url(); ?>master/user/refreshLab", function(data) {
			$("#lab").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
	function refreshRole() {
		$.post("<?php echo base_url(); ?>master/user/refreshRole", function(data) {
			$("#role").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 0 },
                    { "visible": false, "targets": 7 },
                    { "visible": false, "targets": 8 },
                    { "visible": false, "targets": 9 },
                    { "orderable": false, "targets": 10 },
                  ],
                "destroy": true,
                "orderCellsTop":true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": true,
                'lengthMenu' : [10,25,50,100],
                "searching": true,
                "sDom": 'Rfrtlip',
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'fullname' },
                  { data : 'username' },
                  { data : 'email' },
                  { data : 'ldap' },
                  { data : 'lab' },
                  { data : 'role' },
                  { data : 'id_user' },
                  { data : 'id_role' },
                  { data : 'id_ldap' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/user/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) {
                        // console.log(json)
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){
                             // var status = (json[i].LDAP == 'Y' ? 'YES' : 'NO');
                             
                              if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  // var aksi =  '<center><button title="Edit" style="font-size: 10px;" onclick="edit(\''+ i +'\')" class="btn btn-primary btn-sm waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button title="approve" style="font-size: 10px;" onclick="approve(\''+ i +'\')" class="btn btn-warning btn-sm waves-effect btn_approve"><span class="btn-labelx"><i class="fa fa-gear "></i></span> </button> <button title="Non Aktif" style="font-size: 10px;" onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-sm waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-ban"></i></span> </button></center>'
                                  var aksi =  '<center><button title="Edit" style="font-size: 10px;" onclick="edit(\''+ i +'\')" class="btn btn-primary btn-sm waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button>  <button title="Non Aktif" style="font-size: 10px;" onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-sm waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-ban"></i></span> </button></center>'
                              }
                              var wilayah = (tipeUser==0 ? json[i].NM_LAB :json[i].NM_PLANT )
                            return_data.push({
                              'no'	    :  no ,
                              'fullname'	    :  json[i].FULLNAME ,
                              'username'      :   json[i].USERNAME, 
                              'email'	    :  json[i].EMAIL ,
                              'ldap'          : json[i].STATUS ,  
                              'lab'    :  wilayah , 
                              'role'    :  json[i].NAMA_ROLE , 
                              'id_user'   :  json[i].ID_USER , 
                              'id_role'   :  json[i].ID_ROLE , 
                              'id_ldap'   :  json[i].LDAP ,   
                              'id_lab'   :  json[i].ID_LAB ,  
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
    // Apply the search
            TabelData.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    }

	function simpan() {
		var fullname = $("#fullname").val(); 
		var username = $("#username").val(); 
		var email = $("#email").val(); 
		var lab = $("#lab").val(); 
		var ldap = $("#ldap").val(); 
		var role = $("#role").val();  
		var kategori = $("#kategori").val();  
    $('#input_simpan').attr('disabled',true) ;
    $('#input_simpan').html('<i class="fa fa-spinner fa-spin"> </i> Processing') ;
		if(fullname == "" && username == ""   && ldap == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		} 
        
        
			var isi_data = "";
			isi_data += "header[fullname]=" + encodeURI($("#fullname").val());
            isi_data += "&header[username]=" + encodeURI($("#username").val());
            isi_data += "&header[email]=" + encodeURI($("#email").val());
            isi_data += "&header[ldap]=" + encodeURI($("#ldap").val()); 
			
			var i = 0;
			    isi_data += "header[id]=" + encodeURI($("#id_user").val());
                isi_data += "&header[fullname]=" + encodeURI($("#fullname").val());
                isi_data += "&header[username]=" + encodeURI($("#username").val());
                isi_data += "&header[email]=" + encodeURI($("#email").val());
                isi_data += "&header[ldap]=" + encodeURI($("#ldap").val()); 
                isi_data += "&header[tipe]=" + encodeURI(tipeUser); 
            if(tipeUser==0){
                var tbl_list = $("#tabel_rolelab tbody");  
                tbl_list.find('tr').each(function(i, obj) {
                    isi_data += "&isi_data["+i+"][lab]=" + encodeURI($(this).children().eq(5).html());
                    isi_data += "&isi_data["+i+"][role]=" + encodeURI($(this).children().eq(6).html()); 
                    isi_data += "&isi_data["+i+"][kategori]=" + encodeURI($(this).children().eq(7).html()); 
                    i++;
                }); 
            }else{ 
                var tbl_list = $("#tabel_roleplant tbody");   
                tbl_list.find('tr').each(function(i, obj) {
                    isi_data += "&isi_data["+i+"][company]=" + encodeURI($(this).children().eq(5).html());
                    isi_data += "&isi_data["+i+"][plant]=" + encodeURI($(this).children().eq(6).html()); 
                    isi_data += "&isi_data["+i+"][role]=" + encodeURI($(this).children().eq(7).html()); 
                    i++;
                }); 
                
            }
        
		$.post("<?php echo base_url(); ?>master/user/simpan", isi_data, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
      $('#input_simpan').attr('disabled',false) ;
      $('#input_simpan').html('<i class="fa fa-save"> </i> Simpan') ;
		});
	}
      
	function edit(baris) { 
      $("#company, #plant").val('');
		$('#isi_tabel_rolelab').html(''); 
		$('#isi_tabel_roleplant').html('');
		var kolom = TabelData.row(baris).data();    
		$("#id_user").val(kolom['id_user']); 
		$("#fullname").val(kolom['fullname']); 
		$("#username").val(kolom['username']); 
		$("#email").val(kolom['email']); 
		$("#lab").val(''); 
		$("#ldap").val(kolom['id_ldap']); 
		$("#role").val(''); 
		$("#kategori").val(''); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>"); 
    $('#input_update').attr('disabled',true) ;
    $('#input_update').html('<i class="fa fa-spinner fa-spin"> </i> Processing') ;
        
        $.post("<?php echo base_url(); ?>master/user/get_userlab", {"id_user": kolom['id_user']}, function(datas) {
            var data = JSON.parse(datas);
             for(var i=0;i< data.length; i++){ 
                    if(tipeUser==0){
                        $("#tabel_rolelab").find("tbody").append(
                        "<tr>"+ 
                        
                        "<td>"+data[i].NM_LAB+"</td>"+
                        "<td>"+data[i].NAMA_ROLE+"</td>"+  
                        "<td>"+data[i].NM_KATEGORI+"</td>"+  
                        "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                        "<td style='display:none;'> </td>"+
                        "<td style='display:none;'>"+data[i].ID_LAB+"</td>"+
                        "<td style='display:none;'>"+data[i].ID_ROLE+"</td>"+
                        "<td style='display:none;'>"+data[i].ID_KATEGORI+"</td>"+
                        "</tr>");   
                    }else{
                          $("#tabel_roleplant").find("tbody").append(
                        "<tr>"+ 
                        
                        "<td>"+data[i].NM_COMPANY+"</td>"+
                        "<td>"+data[i].NM_PLANT+"</td>"+  
                        "<td>"+data[i].NAMA_ROLE+"</td>"+  
                        "<td><a href='javascript:void(0);' class='btn-xs btn' onclick=\"hapus_role(this);\"><i class='fa fa-remove fa-lg'></i></a></td>"+
                        "<td style='display:none;'> </td>"+
                        "<td style='display:none;'>"+data[i].ID_COMPANY+"</td>"+
                        "<td style='display:none;'>"+data[i].ID_PLANT+"</td>"+
                        "<td style='display:none;'>"+data[i].ID_ROLE+"</td>"+
                        "</tr>");   
                    }
             }
                 
                 
        }).fail(function() {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function() { 
          $('#input_update').attr('disabled',false) ;
          $('#input_update').html('<i class="fa fa-save"> </i> Update') ;
        });
        
        
        
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_user").val();
		var fullname = $("#fullname").val();
		var username = $("#username").val(); 
		var email = $("#email").val(); 
		var lab = $("#lab").val(); 
		var ldap = $("#ldap").val(); 
		var role = $("#role").val(); 
		var kategori = $("#kategori").val(); 
    $('#input_update').attr('disabled',true) ;
    $('#input_update').html('<i class="fa fa-spinner fa-spin"> </i> Processing') ;
		
		if(fullname == "" && username == ""   && ldap == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		}  
        
        
        var isi_data = "";
			var i = 0;
                isi_data += "header[id]=" + encodeURI($("#id_user").val());
                isi_data += "&header[fullname]=" + encodeURI($("#fullname").val());
                isi_data += "&header[username]=" + encodeURI($("#username").val());
                isi_data += "&header[email]=" + encodeURI($("#email").val());
                isi_data += "&header[ldap]=" + encodeURI($("#ldap").val()); 
                isi_data += "&header[tipe]=" + encodeURI(tipeUser); 
            if(tipeUser==0){
                var tbl_list = $("#tabel_rolelab tbody");  
                tbl_list.find('tr').each(function(i, obj) {
                    isi_data += "&isi_data["+i+"][lab]=" + encodeURI($(this).children().eq(5).html());
                    isi_data += "&isi_data["+i+"][role]=" + encodeURI($(this).children().eq(6).html()); 
                    isi_data += "&isi_data["+i+"][kategori]=" + encodeURI($(this).children().eq(7).html()); 
                    i++;
                }); 
            }else{ 
                var tbl_list = $("#tabel_roleplant tbody");   
                tbl_list.find('tr').each(function(i, obj) {
                    isi_data += "&isi_data["+i+"][company]=" + encodeURI($(this).children().eq(5).html());
                    isi_data += "&isi_data["+i+"][plant]=" + encodeURI($(this).children().eq(6).html()); 
                    isi_data += "&isi_data["+i+"][role]=" + encodeURI($(this).children().eq(7).html()); 
                    i++;
                }); 
                
            }
            
		$.post("<?php echo base_url(); ?>master/user/update", isi_data, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
          $('#input_update').attr('disabled',false) ;
          $('#input_update').html('<i class="fa fa-save"> </i> Update') ;
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Nonaktif User</b>",
			"message": "Anda yakin ingin Menonaktifkan User \"" + kolom['username']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_user'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/user/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>