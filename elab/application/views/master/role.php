 
<style type="text/css">
div.div-akses {
    width: 100%;
    padding: 15px 15px 0px 15px;
    color: #555;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
label.check-label, label.check-label input {
    font-weight: normal;
    cursor: pointer;
}
label.check-label:hover {
    font-weight: bold;
}
 div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    

</style> 


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="">
                	<div class="col-md-6">
                		<h2><i class="fa fa-list"></i> Master Role</h2>
                	</div>
                	<div class="col-md-6">
                		<div style="text-align:right;">
		                    <button type="button" class="btn-sm btn-success  btn_tambah" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>
		                </div>		
                	</div>
                </div>
                
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel" width="100%">
                            <thead>
                                <tr>
                                    <th style='width:30px;'>No</th>
                                    <th>Name</th>
                                    <th>Permission</th>
                                    <th> </th>
                                    <th><center> Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center> </th>
                                </tr>
                                <tr id="filtering">
                                    <th style='width:30px;'></th>
                                    <td>Name</td>
                                    <td>Permission</td>
                                    <td> </td>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody></tbody> 
                        </table>
                    </div>
                    </div>
                </div>
            </div>  
    </div>
</div> 

<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' id='judul_input'></div>
			</div>
			<div class='modal-body'>
				<input type='hidden' id='input_id_role' style='display:none;'>
				<div class='form-group'>
					<label>Nama Role:</label>
					<input type='text' class='form-control input-xs' id='input_nama_role' maxlength='64'>
				</div>
				<div class='form-group'  style="display:none">
					<label>Permission:</label>
					
                        <div class="i-checks"><label> <input type="radio" checked=""   class='write' value="RW" id="permission" name="permission"> <i></i> Read Write </label> <label style="margin-left: 50px;"> <input type="radio"  class='read'  value="R" id="permission" name="permission"> <i></i>  ReadOnly </label></div>
                          
				</div>
			</div>
            
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>
<div class='modal fade' id='dlg_hak_akses' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header modal-warning'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<h4 class='modal-title'><b><i class='fa fa-wrench'></i>&nbsp;Edit Hak Akses</b></h4>
			</div>
			<div class='modal-body'>
				<div class='form-group'>
					<label>Nama Role:</label>
					<input type='text' class='form-control input-xs' id='input_nama_role_akses' disabled='disabled'>
				</div>
				<div class='form-group'>
					<label>Hak Akses:</label>
					<div class='div-akses' id='edit_hak_akses'></div>
				</div>
			</div>
			<div class='modal-footer'>
				<button type='button' class='btn btn-warning btn-xs btn-simpan' onclick='simpan_akses();'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	var TabelData;
	$(document).ready(function() { 

		 $('#tabel thead td').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+(1+i)+'" />' );
    } );

        tabel_view()
    
        $('#tambah_data').click(function() { 
             // $(".read").attr('checked','checked')
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#input_id_role, #input_nama_role").val(''); 
            $("#dlg").modal("show");
        });
        
         $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
        console.log($(this).data('index')+"-"+this.value);
        TabelData.column( $(this).data('index') ).search( this.value ).draw();
     });
        
        
	      $('.i-checks').iCheck({
	        checkboxClass: 'icheckbox_square-green',
	        radioClass: 'iradio_square-green',
	    });

	    $(document).on('click','#btn_filtering', function(){
       		var data = $(this).data('filtering');
       		if(data==1){
       			$('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
       			$('#filtering').hide(500);

       		}else{
       			$('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
       			$('#filtering').show(500);
       		}
       });
    
	});
    
    
    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },  
                "columnDefs": [
                    { "visible": false, "targets": 2 }, 
                    { "visible": false, "targets": 3 }, 
                    { "orderable": false, "targets": 4 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": true,
                "lengthMenu":[10,25,50,100],
                "searching": true,
                "sDom":'Rfrtlip',
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' }, 
                  { data : 'role' }, 
                  { data : 'permission' }, 
                  { data : 'id_role' }, 
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>master/role/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){  
                           if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  var aksi = '<center><button title="Edit" onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect  btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span></button> <button title="Akses" onclick="akses(\''+ i +'\')" class="btn  btn-xs waves-effect" style="background-color:#4A148C;"><span class="btn-labelx"><i class="fa fa-cog" style="color:white;"></i></span></button> <button title="Hapus" onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                              }
                            return_data.push({ 
                              'no'     :  no ,  
                              'role'     :  json[i].NAMA_ROLE , 
                              'permission'     :  json[i].PERMISSION ,  
                              'id_role'   :  json[i].ID_ROLE ,  
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
		$(TabelData.table().container()).on('keyup', 'thead input', function () {
            TabelData.column($(this).data('index')).search(this.value).draw();
        });
    }
	function edit(baris) {
		var kolom = TabelData.row(baris).data();
		$("#input_id_role").val(kolom['id_role']);
		$("#input_nama_role").val(kolom['role']);
        if(kolom['permission']=='RW'){
            
            $(".write").iCheck('check');
            $(".read").iCheck('uncheck');
            // $(".write").attr('checked',true);
            // $(".read").attr('checked',false);
        }else{ 
            $(".write").iCheck('uncheck');
            $(".read").iCheck('check');
            // $(".write").attr('checked',false);
            // $(".read").attr('checked','checked')
            
        }
		$("#input_simpan").hide();
		$("#input_update").show();
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Edit Role User</b>");
		$("#dlg_header").removeClass("modal-primary").addClass("modal-success");
		$("#dlg").modal("show");
	}
	function update() {
		var id_role = $("#input_id_role").val();
		var nama_role = $("#input_nama_role").val(); 
		var permission = $("#permission").val(); 
		if(nama_role == "") {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/role/update", {"id_role": id_role, "nama_role": nama_role, "permission": permission }, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() {
            
		});
	}
	function konfirmasi(baris) {
		var kolom = TabelData.row(baris).data();
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Role User</b>",
			"message": "Anda yakin ingin menghapus role user \"" + kolom['role'] + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_role'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
	function hapus(id_role, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/role/hapus", {"id_role": id_role}, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}
	function tambah() {
		$("#input_nama_role").val("");
		$("#input_company").val("");
		$(".selectpicker").selectpicker("refresh");
		$("#input_simpan").show();
		$("#input_update").hide();
		$("#judul_input").html("<b><i class='fa fa-plus'></i>&nbsp;Tambah Role User</b>");
		$("#dlg_header").removeClass("modal-success").addClass("modal-primary");
		$("#dlg").modal("show");
	}
	function simpan() {
		var nama_role = $("#input_nama_role").val(); 
		var permission = $("#permission").val(); 
		if(nama_role == "") {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/role/simpan", {"nama_role": nama_role,"permission": permission }, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menyimpan data.");
				tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
	function akses(baris) {
		var kolom = TabelData.row(baris).data();
		$("#input_nama_role_akses").val($("<div/>").html(kolom['role']).text());
		$.post("<?php echo base_url(); ?>master/role/ambil_akses", {"id_role": kolom['id_role']}, function(data) {
			$("#edit_hak_akses").html(data);
		}).fail(function() {
			$("#edit_hak_akses").html("<div style='font-size:18px;font-weight:bold;color:#ff0000;padding-bottom:15px;'><i class='fa fa-times-circle'></i>&nbsp;Gagal mengambil data. Server sedang bermasalah.</div>");
		}).always(function() {
			$("#dlg_hak_akses").modal("show");
		});
	}
	function simpan_akses() { 
		$.post("<?php echo base_url(); ?>master/role/simpan_akses", $("#form_akses").serialize(), function(data) {
			 // var data = JSON.parse(datas);
			if(data) {
				$("#dlg").modal("hide");
				$("#dlg_hak_akses").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menyimpan data.");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
</script>