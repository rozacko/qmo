  <style>
 thead input {
        width: 60%;
        padding: 3px;
        box-sizing: border-box;

    }
    div.dataTables_length{
      margin-top: 0.2em;
      margin-right: 0.5em;
    }

 </style>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <div class="">
                  <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Kategori</h2>
                  </div>
                  <div class="col-md-6">
                    <div style="text-align:right;">
                        <button class="btn-sm btn-success" onclick="add()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                    </div>
                  </div>
                </div>
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th width="25px;">No</th>
                            <th>Nama Kategori</th>
                            <th width="10%"><center>Aksi  <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>
                            <tr id="filtering">
                            <th></th>
                            <td>Nama Kategori</td>
                            <th></th>
                          </tr>
                          
                        </thead>
                       
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">
          var Table,tableData;
          $(document).ready(function(){
             tableData = $('#Table').DataTable({ 
                  "processing": true,
                  "serverSide": false,
                  "lengthMenu": [10, 25, 50, 100],
                  "pageLength": 10,
                  'dom': 'Bfrtip',
                  'buttons': [
                      {extend: 'copy',title: 'Master_Kategori', exportOptions: {columns: [0,1]}},
                      {extend: 'excel',title: 'Master_Kategori', exportOptions: {columns: [0,1]}},
                      {extend: 'pdf',title: 'Master_Kategori', exportOptions: {columns: [0,1]}},
                      {extend: 'print',title: 'Master_Kategori', exportOptions: {columns: [0,1]}},

                  ],
                  "columnDefs" : [
                    {'orderable' : false, 'targets' : 2},
                  ],
                  "info":true,
                  "searching":true,
                  "orderCellsTop":true,
                  "ordering": true,
                  "ajax": {
                      "url": "<?php echo site_url('master/Kategori/ajax_list')?>",
                      "type": "POST"
                  },
                  columns:[
                    {data:'ID_KATEGORI', name:'ID_KATEGORI'},
                    {data : 'NM_KATEGORI', name:'NM_KATEGORI'},
                    {data: 'ID_KATEGORI', render:function(d,t,f){
                      return '<button class="btn btn-primary btn-xs waves-effect" onclick="edit('+f.ID_KATEGORI+')"><span class="fa fa-pencil"></span></button> &nbsp;<button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\'' + d + '\')"><span class="fa fa-trash-o"></span></button>'
                    }},
                  ]
              
            });

            $('#Table thead td').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" style="width : 100%;" placeholder="Search '+title+'" data-index="'+(i+1)+'" />' );
            } );
            $( tableData.table().container() ).on( 'keyup', 'thead input', function () {
                tableData.column( $(this).data('index') ).search( this.value ).draw();
             });

             tableData.on( 'order.dt search.dt', function () {
                  tableData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                      cell.innerHTML = i+1;
                  } );
              } ).draw();

             $(document).on('click','#btn_filtering', function(){
                var data = $(this).data('filtering');
                if(data==1){
                  $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                  $('#filtering').hide(500);

                }else{
                  $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                  $('#filtering').show(500);
                }
             });
            
          });
           
          var save_method;
          var table;

          function add()
          {
            save_method ='add';
            $('#form')[0].reset();
            $('.modal-title').text('Tambah Data');
            $('#myModal').modal('show');
          }
          function save()
          {
            var url;
             
            if(save_method == 'add'){
              url = '<?php echo site_url('master/Kategori/tambah') ;?>';
            }
            else{
              url = '<?php echo site_url('master/Kategori/update') ;?>';
            }
            $.ajax({
              url:url,
              type:'POST',
              data:$('form').serialize(),
              dataType:'JSON',
              success:function(data){
                informasi(BootstrapDialog.TYPE_SUCCESS, "Sukses menyimpan data.");
                tableData.ajax.reload();
                $('#myModal').modal('hide');
              },
              error:function(jqXHR, textStatus, errorThrown)
              {
               informasi(BootstrapDialog.TYPE_DANGER, "Harap Periksa data.");
              }
            });
          }
          function konfirmasi(baris) {
                var kolom = tableData.row(baris).data();
                console.log(baris)
                BootstrapDialog.show({
                    "type": BootstrapDialog.TYPE_DANGER,
                    "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Kategori</b>",
                    "message": "Anda yakin ingin menghapus ?",
                    "closeByBackdrop": false,
                    "closeByKeyboard": false,
                    "buttons": [{
                        "cssClass": "btn btn-danger btn-xs btn-hapus",
                        "icon": "fa fa-trash",
                        "label": "Hapus",
                        "action": function (dialog) {
                            hapus(baris, dialog);
                        }
                    }, {
                        "cssClass": "btn btn-default btn-xs btn-tutup",
                        "icon": "fa fa-times",
                        "label": "Tutup",
                        "action": function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            }
          function hapus(id, dialog) {
                dialog.setClosable(false);
                $.post("<?php echo base_url(); ?>master/Kategori/hapusData", {"id": id}, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.status == true) {
                        dialog.close();
                        informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                        tableData.ajax.reload();
                    } else {
                        informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                    informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
                }).always(function () {
                    dialog.setClosable(true);
                });
            }
          function edit(id){
            save_method= 'update';
            $('#form')[0].reset();

            $.ajax({
              url : '<?php echo site_url('master/Kategori/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('[name="id_kategori"]').val(data.ID_KATEGORI);
                $('[name="nm_kategori"]').val(data.NM_KATEGORI);
                $('#myModal').modal('show');
                $('.modal-title').text('UPDATE');
                $('#submit').text('UPDATE');
              }
            });
          }
          function excel(){
            $.ajax({
              url : '<?php echo site_url('master/Kategori/excel') ?>',
              type:'POST',
              success:function(data){
                window.open('<?php echo site_url('master/Kategori/excel') ?>','_blank' );
              }
            });
          }
        </script>
       
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Tambah Data</h4>
             </div>
             <div class="modal-body form">
                <form action="" method="POST" id="form">
                  <div class="form-group">
                    <input type="hidden" name="id_kategori">
                  </div>
                  
                  <div class="form-group">
                    <label>Nama Kategori</label>
                    <input type="text" name="nm_kategori" class="form-control">
                  </div>
                  </form>
               
             </div>
             <div class="modal-footer">
              <button class="btn btn-primary" onclick="save()" id="submit">Simpan</button>
              
             </div>
            </div>

          </div>
        </div>
         