
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }
 </style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
        	<div class="">
        		<div class="col-lg-6">
        			<h2><i class="fa fa-list"></i> Master Standar Uji</h2>		
        		</div>
        		<div class="col-lg-6">
        			<div style="text-align:right;">
						<button type="button" class="btn-sm btn-success  btn_tambah" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>&nbsp;&nbsp;&nbsp;
                        
                        <!--<button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel</button> -->
					</div>			
				</div>
        	</div>
			
		
        <div class="ibox-content">
            <div class="row">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel" width="100%">
                    <thead>
                        <tr>
                            <th  style='width:30px;'>No</th>
                            <th>Standar Metode Uji </th>
                            <th>Status </th>
                            <th>Kategori </th>
                            <th>Keterangan </th>
                            <th> </th>
                            <th> </th>
                            <th> </th>
                            <th  style='width:120px;'>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></th>
                        </tr>
                        <tr id="filtering">
                            <th  style='widtd:30px;'></th>
                            <td>Standar Metode Uji</td>
                            <td>Status </td>
                            <td>Kategori </td>
                            <td>Keterangan </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <th  style='widtd:120px;'></th>
                        </tr>
                    </thead>
                    <tbody> 			
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
				<div class='form-group'>
					<label>Standar Uji:</label>
					<input type='hidden' class='form-control input-xs' id='id_standart' >
					<input type='text' class='form-control input-xs' id='standart_uji' >
				</div> 
				<div class='form-group'>
					<label>Status:</label>
					<select class='form-control selectpicker' data-live-search='false' id='status'>
						<option value='' data-hidden='true'  >-- Pilih Status --</option>
						<option value='Y'> ON </option>
						<option value='N'>  OFF  </option>
					</select>
				</div>   
				<div class='form-group'>
					<label>Jenis Pengujian:</label>
					<select class='form-control selectpicker' data-live-search='false' id='jenis'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Jenis --</option>
					</select>
				</div>  
				<div class='form-group'>
					<label>Keterangan:</label> 
					<input type='text' class='form-control input-xs' id='keterangan' >
				</div> 
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){  
    $('#tabel thead td').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+(1+i)+'" />' );
    } );
        tabel_view();
        refreshJenis();
       /// Button Action
	$('#tambah_data').click(function() { 
        refreshJenis()
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#standart_uji, #id_standart, #status, #jenis").val('');
		$("#dlg").modal("show");
    });
    
        
    $("#btn_xls").on("click",function (){
      var link = '<?php echo base_url(); ?>master/standart/export_xls/';
      var buka = window.open(link,'_blank');
      buka.focus();
    })

   $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
        console.log($(this).data('index')+"-"+this.value);
        TabelData.column( $(this).data('index') ).search( this.value ).draw();
     });

   $(document).on('click','#btn_filtering', function(){
      var data = $(this).data('filtering');
      if(data==1){
        $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
        $('#filtering').hide(500);

      }else{
        $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
        $('#filtering').show(500);
      }
   });

});        

	function refreshJenis() {
		$.post("<?php echo base_url(); ?>master/standart/refreshJenis", function(data) {
			$("#jenis").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
        "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Standart_uji',exportOptions: {columns: [3, 1, 4]}},
                {extend: 'excel',title: 'Master_Standart_uji',exportOptions: {columns: [3, 1, 4]}},
                {extend: 'pdf',title: 'Master_Standart_ujit',exportOptions: {columns: [3, 1, 4]}},
                {extend: 'print',title: 'Master_Standart_uji',exportOptions: {columns: [3, 1, 4]}},
                ],

                "columnDefs": [
                    { "visible": false, "targets": 7 },
                    { "visible": false, "targets": 5 },
                    { "visible": false, "targets": 6 },
                    { "orderable": false, "targets": 8 },
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthMenu": [10, 25, 50, 100],
                "lengthChange": true,
                "searching": true,
                // "sDom":'Rfrtlip',
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'nama' },
                  { data : 'status' },
                  { data : 'jenis' },
                  { data : 'keterangan' },
                  { data : 'id' },
                  { data : 'id_jenis' },
                  { data : 'id_status' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/standart/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) {
                        console.log(json)
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){
                             var status = (json[i].STATUS == 'Y' ? 'ON' : 'OFF');
                             
                              if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="fa fa-pencil"></span></button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                              }
                            return_data.push({
                              'no'	    :  no ,
                              'nama'	    :  json[i].NAMA_STANDART ,
                              'status'      :  status, 
                              'jenis'	    :  json[i].NM_KATEGORI ,
                              'keterangan'	    :  json[i].KETERANGAN ,
                              'id'          :  json[i].ID_STANDART ,  
                              'id_jenis'    :  json[i].ID_KATEGORI , 
                              'id_status'   :  json[i].STATUS , 
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
    // Apply the search
            
            $(TabelData.table().container()).on('keyup', 'thead input', function () {
            TabelData.column($(this).data('index')).search(this.value).draw();
        });
    }

	function simpan() {
		var standart_uji = $("#standart_uji").val(); 
		var status = $("#status").val(); 
		var jenis = $("#jenis").val();  
		var keterangan = $("#keterangan").val();  
		if(standart_uji == "" && status == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Standart Uji dan Status harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/standart/simpan", {"standart_uji": standart_uji,"status": status,"jenis": jenis   ,"keterangan": keterangan    
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
     
	function edit(baris) { 
		var kolom = TabelData.row(baris).data();  
        console.log(kolom['id_jenis']);
		$("#id_standart").val(kolom['id']);
		$("#standart_uji").val(kolom['nama']);
		$("#status").val(kolom['id_status']);
		$("#jenis").val(kolom['id_jenis']);
		$("#keterangan").val(kolom['keterangan']);
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>"); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_standart").val();
		var standart_uji = $("#standart_uji").val();
		var jenis = $("#jenis").val(); 
		var status = $("#status").val(); 
		var keterangan = $("#keterangan").val(); 
		if(standart_uji == "" && status == ""  
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Standart Uji dan Status harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/standart/update", {"id": id, "standart_uji": standart_uji, "jenis": jenis, "status": status, "keterangan": keterangan}, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Standart Uji</b>",
			"message": "Anda yakin ingin menghapus Standart Uji \"" + kolom['nama']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/standart/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>