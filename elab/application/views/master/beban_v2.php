

    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    </style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Master Beban Kerja</h2>  
            <div style="text-align:left; display:none">
                <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><i class="fa fa-plus">&nbsp;Tambah</i></button>
            </div>
            <div class="ibox-content">
            
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date : </label> 
                            <div class="col-sm-8">   <input class="form-control input-sm datepicker" style="cursor:pointer;background-color:#fff;" value="<?php echo date('m-Y')?>" id='filter_periode' readonly >
                            </div>
                        </div> 
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">&nbsp;</label> 
                            <div class="col-sm-8">    <button type="button" class="btn btn-success btn-block btn_cari" ><i class="fa fa-search"></i> Cari</button>
                            </div> 
                        </div>
                    </div>
                   	<br>
                </div>
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th  style='width:50px;'>No</th>
                                <th>Tanggal </th>
                                <th>Hari Ke </th>
                                <th>Hari</th> 
                                <th>Libur/Tidak</th> 
                                <th>Sisa</th> 
                                <th>Kerja Masuk</th> 
                                <th>Kerja Selesai</th> 
                            </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
				<div class='form-group'  >
					<label>Laboratorium:</label>
					<input type='hidden' class='form-control input-xs' id='id_beban' >
                    	<select class='form-control selectpicker' data-live-search='false' id='lab'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>
					</select>
				</div> 
				<div class='form-group'>
					<label>Beban Kerja Maksimal:</label>
					<input type='number' class='form-control input-xs' id='beban_kerja' >
				</div> 
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
var TabelData;

var tables;
    
$(document).ready(function(){  
	var tables = $('#tabel').DataTable({ 
            "serverSide": false,
            "processing": true,
            "searching": true,
          	"autoWidth": false,
          	"dom": 'lBfrtip',
	        'buttons': [
	            'excel', 'pdf'
	        ],
          	"aLengthMenu": [[10,25, 50, 75, -1], [10,25, 50, 75, "All"]],
            "ajax": {
	              'type': 'POST',
	              'url': '<?php echo site_url(); ?>index.php/master/beban/get_data',
	        },
	        columns: [
		          { data : 'no', name: 'no' },
		          { data : 'tanggal',
		          	'render': function(d,t,f){
		          		let text ="";
		          		if(f.libur=="Libur"){
		          			text="<p><font color='red'>"+d+"</font></p>";
		          		}else{
		          			text="<p><font color='black'>"+d+"</font></p>"
		          		}
		          		return text;
		          	} 
		          },
		          { data : 'hari',
		          'render': function(d,t,f){
		          		let text ="";
		          		if(f.libur=="Libur"){
		          			text="<p><font color='red'>"+d+"</font></p>";
		          		}else{
		          			text="<p><font color='black'>"+d+"</font></p>"
		          		}
		          		return text;
		          	} 
		          },
		          { data : 'nhari', name: 'nhari' },
		          { data : 'libur', 
		          	'render': function(d,t,f){
		          		let text ="";
		          		if(f.libur=="Libur"){
		          			text="<p><font color='red'>"+d+"</font></p>";
		          		}else{
		          			text="<p><font color='black'>"+d+"</font></p>"
		          		}
		          		return text;
		          	} 
		      	  },
		          { data : 'sisa', name: 'sisa' },
		          { data : 'beban', name: 'beban' },
		          { data : 'selesai', name: 'selesai' },
	        ]
             
	});

	$('#tambah_data').click(function() {  
        refreshLab()
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    }); 
    
    $("#filter_periode").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        autoclose : 'true'
    });
       /// Button Action

	$(document).on('click','.btn_cari', function(){
		var month=$('#filter_periode').val();
        tables.ajax.url("beban/get_data/"+month).load();
	});         
});         
</script>