  <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    div.dataTables_length{
      margin-top: 0.2em;
      margin-right: 0.5em;
    }

 </style>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <div class="">
                  <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Pelanggan</h2>
                  </div>
                  <div class="col-md-6">
                    <div style="text-align:right;">
                       <button class="btn-sm btn-success" onclick="add()"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                    </div>
                  </div>
                </div>
                    <div class="ibox-content">
                      <div class="row">
                   
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Nama instansi</th>
                            <th>NPWP</th>
                            <th>Alamat instansi</th>
                            <th>Telepon instansi</th>
                            <th>Contact person</th>
                            <th>Penanggung Jawab</th>
                            <th>Email</th>
                            <th width="60px"><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>
                           <tr id="filtering">
                            <th></th>
                            <td>Nama instansi</td>
                            <td>NPWP</td>
                            <td>Alamat instansi</td>
                            <td>Telepon instansi</td>
                            <td>Contact person</td>
                            <td>Penanggung Jawab</td>
                            <td>Email</td>
                            <th width="60px"></th>
                          </tr>
                          
                        </thead>
                        <tbody>
                          
                        </tbody>
                        
                      </table>
                 
                </div>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          var tableData;
          $(document).ready(function(){
            $('#Table thead td').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search" '+title+'" data-index="'+(i+1)+'"/>' );
            } );
             tableData = $('#Table').DataTable({ 
                  "processing": true,
                  "serverSide": false,
                  "lengthMenu": [10, 25, 50, 100],
                  'dom': 'Bfrtip',
                  'buttons': [
                      {extend: 'copy',title: 'Data Pelanggan', exportOptions: {columns: [0,1,2,3,4,5,6,7]}},
                      {extend: 'excel',title: 'Data Pelanggan', exportOptions: {columns: [0,1,2,3,4,5,6,7]}},
                      {extend: 'pdf',title: 'Data Pelanggan', exportOptions: {columns: [0,1,2,3,4,5,6,7]}},
                      {extend: 'print',title: 'Data Pelanggan', exportOptions: {columns: [0,1,2,3,4,5,6,7]}},
                      

                  ],
                  "columnDefs":[
                    {'orderable':false,'targets': 8},
                  ],
                  "pageLength": 10,
                  "info":true,
                  "searching":true,
                  "ordering": true,
                  "orderCellsTop":true,
                  "scrollX":true,
                  "ajax": {
                      "url": "<?php echo site_url('master/Pelanggan/ajax_list')?>",
                      "type": "POST"
                  },
                  columns :[
                  
                  {data:'ID_PELANGGAN', name:'ID_PELANGGAN'},
                  {data:'NAMA_INSTANSI', name:'NAMA_INSTANSI'},
                  {data:'NPWP', name:'NPWP'},
                  {data:'ALAMAT', name:'ALAMAT'},
                  {data:'TELEPON', name:'TELEPON'},
                  {data:'CP', name:'CP'},
                  {data:'PENANGGUNG_JAWAB', name:'PENANGGUNG_JAWAB'},
                  {data:'EMAIL', name:'EMAIL'},
                  {data:'ID_PELANGGAN', render:function(d,t,f){
                   return '<button class="btn btn-primary btn-xs waves-effect" onclick="edit('+f.ID_PELANGGAN+')"><span class="fa fa-pencil"></span></button> &nbsp;<button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\'' + d + '\')"><span class="fa fa-trash-o"></span></button>'
            
                  }},
                  ]
              
            });            

            $( tableData.table().container() ).on( 'keyup', 'thead input', function () {
                console.log($(this).data('index')+"-"+this.value);
                tableData.column( $(this).data('index') ).search( this.value ).draw();
             });

           tableData.on( 'order.dt search.dt', function () {
              tableData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                  cell.innerHTML = i+1;
              } );
          } ).draw();

           $(document).on('click','#btn_filtering', function(){
              var data = $(this).data('filtering');
              if(data==1){
                $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

              }else{
                $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
              }
           });
          });
          
          var save_method;
          var table;

          function add()
          {
            save_method ='add';
            $('#form')[0].reset();
            $('.modal-title').text('Tambah Data');
            $('#myModal').modal('show');
          }
          function save()
          {
            var url;
           var form = $('#nama_instansi').val();
            if(form == "" ) {
              informasi(BootstrapDialog.TYPE_WARNING, "Nama Instansi wajib diisi.");
              return;
            }
            if(save_method == 'add'){
              url = '<?php echo site_url('master/Pelanggan/tambah') ;?>';
            }
            else{
              url = '<?php echo site_url('master/Pelanggan/update') ;?>';
            }
            $.ajax({
              url:url,
              type:'POST',
              data:$('form').serialize(),
              dataType:'JSON',
              success:function(data){
                informasi(BootstrapDialog.TYPE_SUCCESS, "Sukses menyimpan data.");
               tableData.ajax.reload();
                $('#myModal').modal('hide');
              },
              error:function(jqXHR, textStatus, errorThrown)
              {
               informasi(BootstrapDialog.TYPE_DANGER, "Harap Periksa data.");
              }
            });
          }
          function konfirmasi(baris) {
                var kolom = tableData.row(baris).data();
                console.log(baris)
                BootstrapDialog.show({
                    "type": BootstrapDialog.TYPE_DANGER,
                    "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Pelanggan</b>",
                    "message": "Anda yakin ingin menghapus ?",
                    "closeByBackdrop": false,
                    "closeByKeyboard": false,
                    "buttons": [{
                        "cssClass": "btn btn-danger btn-xs btn-hapus",
                        "icon": "fa fa-trash",
                        "label": "Hapus",
                        "action": function (dialog) {
                            hapus(baris, dialog);
                        }
                    }, {
                        "cssClass": "btn btn-default btn-xs btn-tutup",
                        "icon": "fa fa-times",
                        "label": "Tutup",
                        "action": function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            }
          function hapus(id, dialog) {
                dialog.setClosable(false);
                $.post("<?php echo base_url(); ?>master/Pelanggan/hapusData", {"id": id}, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.status == true) {
                        dialog.close();
                        informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                        tableData.ajax.reload();
                    } else {
                        informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                    informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
                }).always(function () {
                    dialog.setClosable(true);
                });
            }
          function edit(id){
            save_method= 'update';
            $('#form')[0].reset();

            $.ajax({
              url : '<?php echo site_url('master/Pelanggan/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('[name="id_pelanggan"]').val(data.ID_PELANGGAN);
                $('[name="nama_instansi"]').val(data.NAMA_INSTANSI);
                $('[name="NPWP"]').val(data.NPWP);
                $('[name="alamat"]').val(data.ALAMAT);
                $('[name="telepon"]').val(data.TELEPON);
                $('[name="cp"]').val(data.CP);
                $('[name="penanggung"]').val(data.PENANGGUNG_JAWAB);
                $('[name="email"]').val(data.EMAIL);
                

                $('#myModal').modal('show');
                $('.modal-title').text('UPDATE');
                $('#submit').text('UPDATE');
              }
            });
          }
          function excel(){
            $.ajax({
              url : '<?php echo site_url('master/Pelanggan/excel') ?>',
              type:'POST',
              success:function(data){
                window.open('<?php echo site_url('master/Pelanggan/excel') ?>','_blank' );
              }
            });
          }
        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Tambah Data</h4>
             </div>
             <div class="modal-body form">
                <form action="" method="POST" id="form">
                  <div class="form-group">
                    <input type="hidden" name="id_pelanggan">
                  </div>
                  <div class="form-group">
                    <label>Nama Instansi</label>
                    <input type="text" id="nama_instansi" name="nama_instansi" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>NPWP</label>
                    <input type="text" name="NPWP" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Alamat</label>
                    <textarea class="form-control" name="alamat" rows="5"></textarea>
                  </div>

                  <div class="form-group">
                    <label>Telepon Instansi</label>
                    <input type="text" name="telepon" class="form-control" >
                  </div>
                  <div class="form-group">
                    <label>Contact Person</label>
                    <input type="number" name="cp" class="form-control" >
                  </div>
                  <div class="form-group">
                    <label>Penanggung Jawab</label>
                    <input type="text" name="penanggung" class="form-control" >
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" >
                  </div>
                  </form>
               
             </div>
             <div class="modal-footer">
              <button class="btn btn-primary" onclick="save()" id="submit">Simpan</button>
              
             </div>
            </div>

          </div>
        </div>