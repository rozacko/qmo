

    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    </style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Master Beban Kerja</h2>  
            <div style="text-align:left; display:none">
                <button type="button" class="btn-sm btn-success" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>
            </div>
            <div class="ibox-content">
            
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date : </label> 
                            <div class="col-sm-8">   <input class="form-control input-sm datepicker" style="cursor:pointer;background-color:#fff;" value="<?php echo date('m-Y')?>" id='filter_periode' readonly >
                            </div>
                        </div> 
                    </div> 
                    <div class="col-md-2">
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">&nbsp;</label> 
                            <div class="col-sm-8">    <button type="button" class="btn btn-success btn-block btn_cari" ><i class="fa fa-search"></i> Cari</button>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th  style='width:50px;'>No</th>
                                <th>Laboratorium </th>
                                <th>Beban Kerja Maksimal </th>
                                <th>Rekap SPUC </th> 
                                <th>Overload </th> 
                                <th>SPUC belum selesai analisa </th> 
                                <th> </th> 
                                <th> </th> 
                                <th  style='width:120px;'>Aksi</th>
                            </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
				<div class='form-group'  >
					<label>Laboratorium:</label>
					<input type='hidden' class='form-control input-xs' id='id_beban' >
                    	<select class='form-control selectpicker' data-live-search='false' id='lab'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>
					</select>
				</div> 
				<div class='form-group'>
					<label>Beban Kerja Maksimal:</label>
					<input type='number' class='form-control input-xs' id='beban_kerja' >
				</div> 
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
    
$(document).ready(function(){  
        tabel_view() 
        refreshLab()
       /// Button Action
	$('#tambah_data').click(function() {  
        refreshLab()
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    }); 
    
    $("#filter_periode").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        autoclose : 'true'
    });
       /// Button Action
	$('.btn_cari').click(function() {    
		tabel_view()
    }); 
   
   $('#Table_filter').remove();
        
});         

    function tabel_view(){
		var tanggal = $("#filter_periode").val()
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 6 }, 
                    { "visible": false, "targets": 7 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'lab' },
                  { data : 'beban' },
                  { data : 'rekap' }, 
                  { data : 'overload' },
                  { data : 'belum' },
                  { data : 'id_beban' },
                  { data : 'id_lab' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/beban/get_data', 
		                data: {
		                    "tanggal" : tanggal
		                },
		                       
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){ 
                             
                              if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span>Edit</button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span>Hapus</button></center>'
                              }
                            return_data.push({
                              'no'	    :  no ,
                              'lab'	    :  json[i].NM_LAB ,
                              'beban'      :   json[i].BEBAN_KERJA, 
                              'rekap'	    :  json[i].TOTAL ,
                              'overload'          :  json[i].OVERLOAD ,  
                              'belum'          :  json[i].BELUM ,  
                              'id_beban'    :  json[i].ID_BEBAN ,  
                              'id_lab'    :  json[i].ID_LAB ,  
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
    }
	function refreshLab() {
		$.post("<?php echo base_url(); ?>master/beban/refreshLab", function(data) {
			$("#lab").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

	function simpan() {
		var lab = $("#lab").val(); 
		var beban_kerja = $("#beban_kerja").val();  
		if(lab == "" && beban_kerja == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/beban/simpan", {"lab": lab,"beban_kerja": beban_kerja 
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
     
	function edit(baris) { 
		var kolom = TabelData.row(baris).data(); 
        
		$("#id_beban").val(kolom['id_beban']);
		$("#lab").val(kolom['id_lab']);
		$("#lab").prop( "disabled", true );
        $("#lab").css("color", "black");
		$("#beban_kerja").val(kolom['beban']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>"); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_beban").val(); 
		var lab = $("#lab").val(); 
		var beban_kerja = $("#beban_kerja").val();  
		if(lab == "" && beban_kerja == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/beban/update", {"id": id, "lab": lab,"beban_kerja": beban_kerja }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Beban</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_beban'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/beban/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>