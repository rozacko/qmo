<style type="text/css">
  div.dataTables_length {
    margin-right: 0.5em;
    margin-top: 0.2em;
    
  }

</style>
<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <div class="">
                  <div class="col-md-6"><h2><i class="fa fa-list"></i> Transaksi BAPPUC</h2></div>
                  <div class="col-md-6">
                     <div style="text-align:right;">
                        <button class="btn btn-sm btn-success" onclick="add()" style="margin-top: 10px; margin-bottom: 20px;"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                    </div>
                  </div>
                </div>
                
                   
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="dataTable" class="table table-striped table-bordered table-hover" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th></th>
                            <th><center>NO BAPPUC</center></th>
                            <th><center>Tanggal Terima</center></th>
                            <th><center>Nama Contoh</center></th>
                            <th><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>
                          <tr id="filtering">
                            <th></th>
                            <td><center>NO BAPPUC</center></td>
                            <td><center>Tanggal Terima</center></td>
                            <td><center>Nama Contoh</center></td>
                            <th><center></center></th>
                          </tr>
                        </thead>
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
            
<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
      
        <script type="text/javascript">
          var dataTable;
          $(document).ready(function(){
           
    tableData = $('#dataTable').DataTable({ 
        "processing": true,
        "serverSide": true,
        "lengthMenu": [10, 25, 50, 100],
        "sDom": 'Rfrtlip',
        'lengthChange': true,
        "searching": true,
        "ordering": false,
        "autoWidth": false,
        "ajax": {
            "url": "<?php echo site_url('master/transaksi/ajax_list')?>",
            "type": "POST"
        },
    
  });


      $('#dataTable thead td').each( function (i) {
          var title = $('#example thead th').eq( $(this).index() ).text();
          $(this).html( '<input type="text" width="100%" placeholder="Search '+title+'" data-index="'+(i+1)+'" />' );
      });

     $( tableData.table().container() ).on( 'keyup', 'thead input', function () {
        tableData.column( $(this).data('index') ).search( this.value ).draw();
     });

     $(document).on('click','#btn_filtering', function(){
        var data = $(this).data('filtering');
        if(data==1){
          $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
          $('#filtering').hide(500);

        }else{
          $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
          $('#filtering').show(500);
        }
     });
    
           $("#wizard").steps();

            $("#form").steps({

                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {

                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");

                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);
                    
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";


                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);
                    //$("#warna").select2({
                          //placeholder:"--Pilih--"
                        //});
                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
             

            
              //edit
              $("#wizard").steps();
            $("#form1").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });


            

          
          });
          $(function(){
            $('#contoh').hide();
            $('#contoh1').hide();
            $('#uji_kim').hide();
            $('#uji_fis').hide();
            $('#uji_beton').hide();
            $('#peminta').hide();
           

           
            
            $('#nama').change(function() {   
             refreshNama($("#nama option:selected").val()) 
          });

            function refreshNama(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view", {"id": id   
              }, function(data) {
                $("#contoh1").html(data);
                $('#contoh').slideDown();
                $('#contoh1').slideDown();
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
           
           $('#checkbox').hide();
            $('#nama').change(function(data){
              refreshCheckbox($("#nama option:selected").val()) 
             

            });
            function refreshCheckbox(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/checkbox",{"id": id   
              }, function(data) {
                $("#checkbox").html(data);
                $('#checkbox').slideDown();
              $('#peminta').slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            //menampilkan metode uji
            $('#nama').change(function() {   
             refreshUji($("#nama option:selected").val()) 
          });

            function refreshUji(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/uji", {"id": id   
              }, function(data) {
                $("#semua_uji").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            $('#plant').change(function() {   
             refreshLab($("#plant option:selected").val()) 
          });

            function refreshLab(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/lab", {"id": id   
              }, function(data) {
                $("#lab").html(data);
                // $("#lab_edit").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            //

            
            $('#semua_uji').change(function(data){
              refreshTombol($("#semua_uji option:selected").val()) 
             

            });
            function refreshTombol(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/change",{"id": id   
              }, function(data) {
                $("#tombol").html(data);
                $("#tombol").show();
                
              
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
  

            
          
           //edit====================================================
           
           
            $('#semua_uji_edit').change(function(data){
              refreshTombol_e($("#semua_uji_edit option:selected").val()) 
             

            });
            
           

            $('#nama_edit').change(function() {   
             refreshLokasi_edit($("#nama_edit option:selected").val()) 
          });

            $('#nama_edit').change(function() {   
             refreshUji_edit($("#nama_edit option:selected").val()) 
          });
           
            
           
           
            $('#nama_edit').change(function(){
              refreshCheckbox_edit($("#nama_edit option:selected").val()) 
             

            });
           
            $('#plant_edit').change(function() {   
             refreshLab_edit($("#plant_edit option:selected").val()) 
          });


            
            });
            

          function add(){
            $('#myModal').modal('show');
            $('#form')[0].reset();
            $('#nama').val('');
          }
          function edit(id){
          $('#form1')[0].reset();
         $("#id_transaksi_edit").val(id)
         $.ajax({
           url : '<?php echo site_url('master/Transaksi/edit') ?>/'+id,
           type : 'GET',
           dataType : 'JSON',
            success:function(data){
                
                pilih_eks(data.KODE_UJI, data.PEMINTA)
              $('[name="id_transaksi"]').val(data.ID_TRANSAKSI); 
                // pilih_eks(data.KODE_UJI)
              $('[name="1"]').val(data.KODE_UJI);
              $('[name="2"]').val(data.ID_PLANT);
              
              refreshLab_edit(data.ID_PLANT, data.ID_LAB) 
              
              $('[name="4"]').val(data.NO_SPUC_4);
              
              $('[name="tanggal"]').val(data.TANGGAL);
              $('[name="nama_kelompok1"]').val(data.NAMA_KELOMPOK);
              $('[name="jumlah"]').val(data.JUMLAH);
              $('[name="kemasan"]').val(data.KEMASAN);
              $('[name="berat"]').val(data.BERAT);
              $('[name="warna"]').val(data.WARNA);
              $('[name="ket_warna"]').val(data.KETERANGAN_WARNA);
              $('[name="asal_contoh"]').val(data.ASAL_CONTOH);
              $('[name="bentuk_contoh"]').val(data.BENTUK_CONTOH);
              $('[name="bentuk"]').val(data.BENTUK);
              $('[name="kondisi"]').val(data.KONDISI);
              $('[name="tanggal_mulai"]').val(data.TANGGAL_MULAI);
              $('[name="tanggal_selesai"]').val(data.TANGGAL_SELESAI);
              $('[name="no_spk"]').val(data.NO_SPK);
              select_peminta(data.PEMINTA)
              $('[name="namapemintaedit"]').val(data.PEMINTA);
              $('[name="alamat"]').val(data.ALAMAT); 
              $('[name="npwp"]').val(data.NPWP); 
              $('[name="penanggung_jawab"]').val(data.PENANGGUNG_JAWAB); 
              $('[name="keterangan"]').val(data.KETERANGAN);
              var myStr = data.SAMPEL;
              var strArray = myStr.split(",");
              
              var bd = 1;
              for(var i = 0; i < strArray.length; i++){
                  

                    $("#data_k_edit").append("<div class='form-group'>"
                                +"<label class='col-sm-2'>"+bd+"</label>"
                                +"<div class='col-sm-4'>"
                                +"<input type='text' class='form-control' name='sampel_e[]' id='sampel[]' value='"+strArray[i]+"'>"
                                +"<br>"
                                +"</div>" 
                        +"</div>"   
                        
                       );   
                    bd++;
            }
              
              refreshLokasi_edit($("#nama_edit option:selected").val(),data.ID_CONTOH)
              
              // $(".selectpicker").selectpicker("refresh");
             
               
              

              $('#myEdit').modal('show');
          }
        });
    }

   
            function refreshLab_edit(id, kd_lab) {
              $.post("<?php echo base_url(); ?>master/Transaksi/lab_e", {"id": id   
              }, function(data) {
                $("#lab_edit").html(data);
                if(kd_lab!=''){
                    $('[name="KD_LAB"]').val(kd_lab); 
                }
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
          
           
            function refreshLokasi_edit(id,contoh) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view_edit", {"id": id   
              }, function(data) {
               
                $("#contoh1_edit").html(data);
                $('#contoh1_edit').val(contoh);
                refreshCheckbox_edit($("#nama_edit option:selected").val(),data.ID_UJI)

              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
              function refreshCheckbox_edit(id,checkbox) {
              $.post("<?php echo base_url(); ?>master/Transaksi/checkbox_edit", {"id": id, "id_transaksi" : $("#id_transaksi_edit").val()  
              }, function(data) {
                $("#checkbox_edit").html(data);
                $("#checkbox_edit").val(checkbox);
                refreshUji_edit($("#nama_edit option:selected").val(), data.ID_STANDART)
              
              }).fail(function() {
                // Nope
              });
            }
              function refreshUji_edit(id,uji) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view_uji", {"id": id , 'uji':uji, "id_transaksi" : $("#id_transaksi_edit").val()    
              }, function(data) {
                 $("#semua_uji_edit").html(data);
                 $("#semua_uji_edit").val(uji);
                refreshTombol_e($("#semua_uji_edit option:selected").val(), data.ID_STANDART) 
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            function refreshTombol_e(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/change_edit",{"id": id   
              }, function(data) {
                $("#tombol_edit").html(data);
                $("#tombol_edit").show();
                
              
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            
            function konfirmasi(id, spuc) { 
                BootstrapDialog.show({
                    "type": BootstrapDialog.TYPE_DANGER,
                    "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Transaksi</b>",
                    "message": "Anda yakin ingin menghapus Transaksi \"" + spuc + "\"?",
                    "closeByBackdrop": false,
                    "closeByKeyboard": false,
                    "buttons": [{
                        "cssClass": "btn btn-danger btn-xs btn-hapus",
                        "icon": "fa fa-trash",
                        "label": "Hapus",
                        "action": function (dialog) {
                            hapus(id, dialog);
                        }
                    }, {
                        "cssClass": "btn btn-default btn-xs btn-tutup",
                        "icon": "fa fa-times",
                        "label": "Tutup",
                        "action": function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            }
            
            
            function hapus(id, dialog) {
                dialog.setClosable(false);
                $.post("<?php echo base_url(); ?>master/Transaksi/hapus", {"id": id}, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.notif == "1") {
                        dialog.close();
                        informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                        location.reload();
                    } else {
                        informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                    informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
                }).always(function () {
                    dialog.setClosable(true);
                });
            }

          // function hapus(id){
            // if (confirm("Apakah yakin ingin menghapus?")) {
        // $.ajax({
            // url:'<?php echo base_url() ?>master/Transaksi/hapus',
            // type: 'post',
            // data: {id:id},
            // success: function () {
                // alert('Berhasil Menghapus');
                // location.reload();
            // },
            // error: function () {
                // alert('Gagal');
            // }
             // });
            // } 
            
            // }
         function baca(id){
    
         
         $.ajax({
           url : '<?php echo base_url() ?>master/Transaksi/detail',
           type : 'GET',
           data: {id:id},
           
            success:function(data){
              
            

              $('#myDetail').modal('show');
          }
        });
    }
        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-lg">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Tambah</h4>
             </div>
             <div class="modal-body">
                 
                    <div class="ibox">
                        
                        <div class="ibox-content">
                            <form id="form" action="<?php echo base_url() ?>master/Transaksi/tambah" class="wizard-big" method="POST">
                                <h1>Halaman 1</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                             <input type="hidden" id="id_transaksi_edit"   class="form-control">
                                                <label>NO SPUC *</label>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select class="form-control" required="" style='background-color:white'  name="1" id="kd_uji" onchange="pilih_eks(this);">
                                                              <option value="">--KODE UJI--</option>
                                                        <?php 
                                                        foreach ($kode->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->KODE_UJI ?>"><?php echo $key->NAMA_UJI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control"  name="2" id="plant">
                                                              
                                                        <option value=""></option>
                                                              <?php 
                                                              for($a=0;$a<count($plant);$a++)
                                                              {
                                                                ?>
                                                                <option value="<?php echo $plant[$a]['ID_PLANT']; ?>" ><?php echo $plant[$a]['NM_PLANT']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control" name="3" id="lab" readonly style='background-color: white;'>
                                                             
                                                             <option value=""></option>
                                                        
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                         <input type="text" name="4"  value="<?php echo date('m.Y') ?>" readonly style='background-color: white;'  class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label style="padding-right: 38px;">Tanggal Terima</label>
                                               
                                                  <input type="text" name="tanggal" id="tanggal" class="form-control" style="width: 400px;background-color: white;" required="" placeholder="dd-mm-yyyy" readonly  >
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label style="padding-right: 30px;">Nama Kelompok</label>
                                                <select class="form-control" style="width: 400px;background-color:white"  id="nama" name="nama_kelompok" required="">
                                                      
                                                      <?php 
                                                        foreach ($lihat->result() as $a) {
                                                          ?>
                                                          <option value="<?php echo $a->ID_KELOMPOK ?>"><?php echo $a->NAMA_KELOMPOK ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                    </select>
                                              </div>
                                              <div class="form-group" >
                                                <label style="padding-right: 48px;" id="contoh">Nama Contoh</label>
                                                <select class="form-control" style="width: 400px;  background-color:white "  id="contoh1" name="nama_contoh" required="">
                                                      <option selected="selected"></option>
                                                     
                                                    </select>
                                              </div>
                                              
                                              <div id="checkbox">
                                                 
                                               
                                              </div>
                                        </div>
                                        
                                    </div>
                                  </fieldset>
                  
                                   <h1>Halaman2</h1>
                                  <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                            <div class="form-group">
                                                <label>Jumlah Contoh</label>
                                                <input type="text" name="jumlah" id="jumlah"  style='background-color:white'  class="form-control required">
                                                
                                            </div>
                                            </div>
                                             
                                        </div>
                                      <div id="data_k"></div>
                                          
                                          <div id="semua_uji">
                                          </div>

                                    
                                </fieldset>

                                <h1>Halaman 3</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                        
                                          <div class="row">
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Kemasan Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="kemasan">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($tampilkan->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->NAMA_KEMASAN ?>"><?php echo $a->NAMA_KEMASAN ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                </select>
                                              </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Berat Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="berat">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($berat->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->ID_BERAT ?>"><?php echo $a->NAMA_BERAT ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                  
                                                </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Warna Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                 
                                                  
                                                  
                                                  <input type="text" name="warna" list="warna" class="form-control"  style="width: 330px;" placeholder="Ketik bila ada warna lain">
                                                  <datalist id="warna">
                                                  <?php 
                                                    foreach ($warna->result() as $a) {
                                                      ?>
                                                      
                                                      <option value="<?php echo $a->NAMA_WARNA ?>"><?php echo $a->NAMA_WARNA ?></option>
                                                      
                                                      <?php
                                                    }
                                                    ?>
                                                    </datalist>
                                                      
                                                
                                                  
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Keterangan Warna</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  
                                                  <select class="form-control"  style="width: 330px;" name="ket_warna">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KET_WARNA ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    
                                                  </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Asal Contoh</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <input type="text" name="asal_contoh" class="form-control" style="width: 330px;">
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Bentuk Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk_contoh">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Wujud Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH_UJI_2 ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Kondisi Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control"   style="width: 330px;" name="kondisi">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KONDISI_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>Tanggal Mulai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_mulai" class="form-control" style="width: 330px;">
                                                  </div>
                                                  
                                                
                                                
                                                   <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label >Tanggal Selesai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_selesai" class="form-control" style="width: 330px;">
                                                  </div>
                                                
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>NO SPK/SPP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                      <input type="text" name="no_spk" class="form-control" style="width: 330px;" >
                                                    </div>
                                                    
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                    <label>Nama Peminta</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="peminta">
                                                      <div id="divhtmlpeminta"></div>
                                                    </div>
													     
                                                  <div id='peminta_add'>                    
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="ALAMAT">
                                                    <label>Alamat</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlalamat"></div>
                                                     
                                                      
                                                    </div>
                                                  
                                                  
												 <div class="col-md-6" style="margin-bottom: 10px;" id="NPWP">
                                                    <label>NPWP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlidentitas"></div>
                                                      
                                                      
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="penanggung_jawab">
                                                    <label>Penanggung Jawab</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlperson"></div>
                                                        
                                                      
                                                    </div>
                                                    
													                         <div id="keterangan_peminta">
                                                    <div class="col-md-6" >
                                                    <label>Keterangan</label>
                                                  </div>
                                                    <div class="col-md-6">
                                                      <textarea class="form-control" rows="5" name="keterangan" style="width: 330px;"></textarea>
                                                    </div>
                                                  </div>
                                                  </div>
                                                
                                        </div>
                                      
                                        
                                </fieldset>

                  </form>

                
             
             
             
            
          </div>
          </div>
        </div>
      </div>
    </div>

       
        <!-- Modal EDIT-->
        <div id="myEdit" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Update</h4>
             </div>
             <div class="modal-body">
                 
                     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        
                        <div class="ibox-content">
                            

                            <form id="form1" action="<?php echo base_url() ?>master/Transaksi/update" class="wizard-big" method="POST">
                                <h1>Halaman 1</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                          </div>
                                            <div class="form-group">
                                                <label>NO SPUC *</label>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                    
                                                        <input type="hidden" name="id_transaksi">
                                                        <select class="form-control" required=""  style='background-color:white'   name="1"  id='kd_uji_edit' onchange="pilih_eks(this);">
                                                              <option value="">--KODE UJI--</option>
                                                        <?php 
                                                        foreach ($kode->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->KODE_UJI ?>"><?php echo $key->NAMA_UJI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                                                    </div>
                                                       <div class="col-md-3">
                                                        <select class="form-control"  name="2" id="plant_edit">
                                                              
                                                       <?php 
                                                              for($a=0;$a<count($plant);$a++)
                                                              {
                                                                ?>
                                                                <option value="<?php echo $plant[$a]['ID_PLANT']; ?>" ><?php echo $plant[$a]['NM_PLANT']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control"   name="KD_LAB" id="lab_edit" readonly style='background-color: white;' > 
                                                             <option value=""></option>  
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                         <input type="text" name="4"  value="<?php echo date('M.Y') ?>" readonly  style='background-color: white;' class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="padding-right: 38px;">Tanggal Terima</label>
                                                <input type="text" name="tanggal" id="tanggal1" class="form-control" style="width: 400px; background-color: white;" required="" readonly>
                                            </div>
                                           
                                            <div class="form-group">
                                                <label style="padding-right: 30px;">Nama Kelompok</label>
                                                <select class="form-control" style="width: 400px;"  id="nama_edit" name="nama_kelompok1">
                                                      
                                                      <?php 
                                                        foreach ($lihat->result() as $a) {
                                                          ?>
                                                          <option value="<?php echo $a->ID_KELOMPOK ?>"><?php echo $a->NAMA_KELOMPOK ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                    </select>
                                              </div>
                                              <div class="form-group" >
                                                <label style="padding-right: 48px;" id="contoh_edit">Nama Contoh</label>
                                                <select class="form-control" style="width: 400px;"  id="contoh1_edit" name="nama_contoh1">
                                                       <option selected="selected"></option>
                                                     
                                                    </select>
                                              </div>
                                              <div id="checkbox_edit">

                                              </div>
                                        </div>
                                        
                                    </div>
                                  </fieldset>
                  
                                  <h1>Halaman2</h1>
                                  <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                          
                                            <div class="form-group">
                                                <label>Jumlah Contoh</label>
                                                <input type="text" name="jumlah" id="jumlah_edit" class="form-control" required="" style='background-color:white' >
                                            </div>
                                            
                                            </div>

                                        </div>
                                        <div id="data_k_edit">
                                          
                                        </div>
                                        
                                         <div id="semua_uji_edit">
                                              
                                            
                                       
                                        
                                          
                                        
                                      </div>
                                </fieldset>

                                <h1>Halaman 3</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                        
                                          <div class="row">
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Kemasan Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="kemasan">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($tampilkan->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->NAMA_KEMASAN ?>"><?php echo $a->NAMA_KEMASAN ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                </select>
                                              </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Berat Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="berat">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($berat->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->ID_BERAT ?>"><?php echo $a->NAMA_BERAT ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                  
                                                </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Warna Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                 
                                                  
                                                  
                                                  <input type="text" name="warna" list="warna" class="form-control"  style="width: 330px;" placeholder="Ketik bila ada warna lain">
                                                  <datalist id="warna">
                                                  <?php 
                                                    foreach ($warna->result() as $a) {
                                                      ?>
                                                      
                                                      <option value="<?php echo $a->NAMA_WARNA ?>"><?php echo $a->NAMA_WARNA ?></option>
                                                      
                                                      <?php
                                                    }
                                                    ?>
                                                    </datalist>
                                                      
                                                
                                                  
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Keterangan Warna</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  
                                                  <select class="form-control"  style="width: 330px;" name="ket_warna">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KET_WARNA ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    
                                                  </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Asal Contoh</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <input type="text" name="asal_contoh" class="form-control" style="width: 330px;">
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Bentuk Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk_contoh">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Wujud Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH_UJI_2 ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Kondisi Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control"   style="width: 330px;" name="kondisi">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KONDISI_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>Tanggal Mulai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_mulai" class="form-control" style="width: 330px;">
                                                  </div>
                                                   
                                                
                                                   <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label >Tanggal Selesai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_selesai" class="form-control" style="width: 330px;">
                                                  </div>
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>NO SPK/SPP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                      <input type="text" name="no_spk" class="form-control" style="width: 330px;" >
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                    <label>Nama Peminta</label>
                                                    </div>
                                                   
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="peminta">
                                                      <div id="divhtmlpemintaedit"></div>
                                                    </div>
                                                  
                                                <div id="peminta_edit" style='display:none'>
                                                     <div class="col-md-6" style="margin-bottom: 10px;" id="ALAMAT_edit">
                                                    <label>Alamat</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlalamatedit"></div> 
                                                    </div> 
												 <div class="col-md-6" style="margin-bottom: 10px;" id="NPWP_edit">
                                                    <label>NPWP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlidentitasedit"></div> 
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="penanggung_jawab_edit">
                                                    <label>Penanggung Jawab</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlpersonedit"></div> 
                                                    </div>
                                                    
												 <div id="keterangan_peminta_edit">
                                                    <div class="col-md-6" >
                                                    <label>Keterangan</label>
                                                  </div>
                                                    <div class="col-md-6">
                                                      <textarea class="form-control" rows="5" name="keteranganedit" style="width: 330px;"></textarea>
                                                    </div>
                                                  </div>
                                                </div>
                                        </div>
                                            
                                        
                                </fieldset>
                
             </div>
             
             </form>
            
          </div>
          </div>
        </div>

        
       
       
<script type="text/javascript">
 
  
  $(function(){
  $('#jumlah').keyup(function(){
        var vpengikut = $("#jumlah").val();
            var bd = 1;
            // alert(bd);
            if(vpengikut < 1){
                $("#data_k").empty();
                
            }
            else if(isNaN(vpengikut)){
                $("#data_k").empty();
                
            }
            else{               
                $("#data_k").empty();
                
                $("#data_k").append("<p class=\"sub-info\"> <u>Kode Sampel</u> </p>");
                while(bd <= vpengikut){
                    $("#data_k").append("<div class='form-group'>"
                                +"<label class='col-sm-2'>"+bd+"</label>"
                                +"<div class='col-sm-4'>"
                                +"<input type='text' class='form-control' name='sampel[]' id='sampel[]'>"
                                +"<br>"
                                +"</div>" 
                        +"</div>"   
                        
                       );

                    bd++;
                }
            }
    });
  });


  $(function(){
  $('#jumlah_edit').keyup(function(){
        var vpengikut = $("#jumlah_edit").val();
            var bd = 1;
            // alert(bd);
            if(vpengikut < 1){
                $("#data_k_edit").empty();
                
            }
            else if(isNaN(vpengikut)){
                $("#data_k_edit").empty();
                
            }
            else{               
                $("#data_k_edit").empty();
                
                $("#data_k_edit").append("<p class=\"sub-info\"> <u>Kode Sampel</u> </p>");
                while(bd <= vpengikut){
                    $("#data_k_edit").append("<div class='form-group'>"
                                +"<label class='col-sm-2'>"+bd+"</label>"
                                +"<div class='col-sm-4'>"
                                +"<input type='text' class='form-control' name='sampel_e[]' id='sampel[]'>"
                                +"<br>"
                                +"</div>" 
                        +"</div>"   
                        
                       );

                    bd++;
                }
            }
    });
  });
$(function(){
  $('#tanggal').datepicker({
    format:'dd-mm-yyyy',
    autoclose: true,
      todayHighlight: true
  });
 $('#tanggal1').datepicker({
    format:'dd-mm-yyyy',
    autoclose: true
  });

    $("#ALAMAT").hide();
    $("#NPWP").hide();
    $("#penanggung_jawab").hide();
    $("#keterangan_peminta").hide();

  
});  

function pilih_eks(val, id_user )
{
	kodeuji = val.value;
	if(kodeuji=='ISO'||val=='ISO'){ 
        $("#penanggung_jawab, #identitas, #alamat").val('')
        $("#peminta_edit, #peminta_add").show(); 
        $("#peminta_edit").hide()
		$('#namapeminta').val(kodeuji);
		$.ajax({
           url : '<?php echo base_url() ?>master/Transaksi/peminta',
           type : 'GET',
		   dataType : 'JSON',
           data: {'status':kodeuji},
            success:function(data){
			   htmlpeminta = "<select id='namapeminta' name='namapeminta' class='form-control selectpicker' data-live-search='true'>";
			   htmlpemintaedit = "<select id='namapemintaedit' name='namapemintaedit' class='form-control selectpicker' data-live-search='true'>";
                htmlpeminta += "<option value=''>--Pilih--</option>";
                htmlpemintaedit += "<option value=''>--Pilih--</option>";
			   for(a=0;a<data.length;a++)
			   {
          
					htmlpeminta += "<option value='"+data[a].ID_PELANGGAN+"'>"+data[a].NAMA_INSTANSI+"</option>";
					htmlpemintaedit += "<option value='"+data[a].ID_PELANGGAN+"'>"+data[a].NAMA_INSTANSI+"</option>";
			   }
			   htmlpeminta += "</select>";
			   htmlpemintaedit += "</select>";
			   $('#divhtmlpeminta').html(htmlpeminta); 
			   $('#divhtmlpemintaedit').html(htmlpemintaedit);  
               
                $('#namapeminta').selectpicker('refresh');
                $('#namapemintaedit').selectpicker('refresh');
                    // refreshPeminta($("#namapemintaedit option:selected").val()) 
                    // refreshPeminta2($("#namapemintaedit option:selected").val()) 
                    // refreshPenanggung($("#namapemintaedit option:selected").val()) 
             $('#namapeminta,#namapemintaedit').change(function() {   
                    var idu = $("#namapemintaedit").val(); 
                    if(idu==''){
                        idu = $("#namapeminta").val(); 
                    }else{
                        idu = $("#namapemintaedit").val(); 
                    }
                    refreshPeminta(idu) 
                    refreshPeminta2(idu) 
                    refreshPenanggung(idu) 
                    $("#peminta_edit").show()
                });
                
                
              $('[name="namapemintaedit"]').val(id_user);
        
          }

		});
	}
	else {
          
        $("#peminta_edit, #peminta_add").hide(); 
         
		$('#namapeminta, #namapemintaedit').val(kodeuji);
        $('#divhtmlpeminta').html(' <select class="form-control selectpicker" data-live-search="true" data-size="10" id="namapeminta"  name="namapeminta" > <option value="" selected="selected">-- namapeminta --</option></select>'); 
        var pelanggan = '';
        
        console.log(id_user)
        if(id_user==''){
            pelanggan = '-- Pilih Peminta --'; 
             $('#divhtmlpemintaedit').html(' <select class="form-control selectpicker" data-live-search="true" data-size="10" id="namapemintaedit"  name="namapemintaedit" > <option value="'+id_user+'" selected="selected"> '+pelanggan+' </option></select>'); 
        }else{
             $.post("<?php echo base_url(); ?>master/Transaksi/pemintaInternal", {"id": id_user   
              }, function(data) {
                  pelanggan = '( '+id_user+' )- ( '+data+' )' ;
                  $('#divhtmlpemintaedit').html(' <select class="form-control selectpicker" data-live-search="true" data-size="10" id="namapemintaedit"  name="namapemintaedit" > <option value="'+id_user+'" selected="selected"> '+pelanggan+' </option></select>'); 
                  select_peminta(id_user)
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
          
        }
        
        // $('#divhtmlpemintaedit').html(' <select class="form-control selectpicker" data-live-search="true" data-size="10" id="namapemintaedit"  name="namapemintaedit" > <option value="'+id_user+'" selected="selected"> '+pelanggan+' </option></select>'); 
        // select_peminta(id_user)
      
        
        
		// $.ajax({
           // url : '<?php echo base_url() ?>master/Transaksi/peminta2',
           // type : 'GET',
		   // dataType : 'JSON',
           // data: {'status':kodeuji},
            // success:function(data){
			   // htmlpeminta = "<select id='namapeminta' name='namapeminta' class='form-control selectpicker' data-live-search='true'>";
         // htmlpeminta += "<option value=''>--Pilih--</option>";
			   // for(a=0;a<data.length;a++)
			   // {
          
					// htmlpeminta += "<option value='"+data[a].mk_nopeg+"'>"+data[a].mk_nama+"</option>";
			   // }
			   // htmlpeminta += "</select>";
			   // $('#divhtmlpeminta').html(htmlpeminta); 
            // $('#namapeminta').selectpicker('refresh');
          // }
		// });
	}
}

  function select_peminta(id_user){
              $(".selectpicker").selectpicker().filter("#namapeminta, #namapemintaedit").ajaxSelectPicker({
                "ajax": {
                    "url": '<?php echo base_url() ?>master/Transaksi/peminta2',
                    "type": "POST",
                    "dataType": "json",
                    "data": {
                        "q": "{{{q}}}",
                        "id_user": id_user,
                    }
                },
                "log": false,
                "preprocessData": function(data) {
                    var i, l = data.length, array = [];
                    if(l) {
                        for(i = 0; i < l; i++) {
                            array.push($.extend(true, data[i], data[i]));
                        }
                    }
                    return array;
                }
            });
        }
      
  // $('#divhtmlpeminta').change(function() {   
               // refreshPeminta($("#divhtmlpeminta option:selected").val()) 
          // });
            
            function refreshPeminta(id) {

              $.post("<?php echo base_url(); ?>master/Transaksi/peminta3", {"id": id   
              }, function(data) {
                
                $("#divhtmlalamat").html(data);
                $("#divhtmlalamatedit").html(data);
                $("#ALAMAT").slideDown();
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }

       //===================================     
  // $('#divhtmlpeminta').change(function() {   
             // refreshPeminta2($("#divhtmlpeminta option:selected").val()) 
          // });

            function refreshPeminta2(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/peminta4", {"id": id   
              }, function(data) {
               
                $("#divhtmlidentitas").html(data);
                $("#divhtmlidentitasedit").html(data);
                $("#NPWP").slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
  // $('#divhtmlpeminta').change(function() {   
             // refreshPenanggung($("#divhtmlpeminta option:selected").val()) 
          // });

            function refreshPenanggung(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/penanggung", {"id": id   
              }, function(data) {
                $("#divhtmlperson").html(data);
                $("#divhtmlpersonedit").html(data);

                $("#penanggung_jawab").slideDown();
                $("#keterangan_peminta").slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            function getNamepeminta(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/pemintaInternal", {"id": id   
              }, function(data) {
                
              }).fail(function() {
                // Nope
              }).always(function() {
                // $(".selectpicker").selectpicker("refresh");
              });
            }
            //==============================
            

</script>

      