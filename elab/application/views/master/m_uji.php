<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-top: 0.2em;
        margin-right: 0.5em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">


            <div class="">
                <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Parameter Uji</h2>
                </div>
                <div class="col-md-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success  btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Tambah
                        </button>&nbsp;&nbsp;&nbsp;
                        <!-- <button type="button" id='btn_xls' class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel </button> -->
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                               style="font-size:95%" id="tabel" width="100%">
                            <thead>
                            <tr>
                                <th style='width:10px;'>No</th>
                                <th>Parameter Uji</th>
                                <th>Kategori</th>
                                <th>Satuan</th>
                                <th>Symbol</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style='width:120px;'>
                                    <center>Aksi <span class="fa fa-filter pull-right" data-filtering="1"
                                                       id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                    </center>
                                </th>
                            </tr>
                            <tr id="filtering">
                                <th style='widtd:10px;'></th>
                                <td>Parameter Uji</td>
                                <td>Kategori</td>
                                <td>Satuan</td>
                                <td>Symbol</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <th style='widtd:120px;'></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>

                <div id="form_insert">
                    <div class='form-group'>
                        <label>Tipe Insert:</label>
                        <select class='form-control selectpicker' data-live-search='false' id='tipe'>
                            <option value='0' selected='selected'>New</option>
                            <option value='1'>Existing</option>
                        </select>
                    </div>

                    <div class='form-group' id='form_existing'>
                        <label>Existing:</label>
                        <select class='form-control selectpicker' data-live-search='true' id='existing'>
                            <option value='' data-hidden='true' selected='selected'>-- Pilih Data --</option>
                        </select>
                    </div>
                </div>
                <div id="form_uji">

                    <div class='form-group' style='display:none'>
                        <label>Component:</label>
                        <select class='form-control selectpicker' data-live-search='true' id='component'>
                            <option value='' data-hidden='true' selected='selected'>-- Pilih Coponent --</option>
                        </select>
                    </div>
                    <div class='form-group'>
                        <label>Parameter Uji:</label>
                        <input type='hidden' class='form-control input-xs' id='id_uji'>
                        <input type='text' class='form-control input-xs' id='nm_uji'>
                    </div>
                    <div class='form-group'>
                        <label>Kategori Uji:</label>
                        <select class='form-control selectpicker' data-live-search='false' id='Kategori'>
                            <option value='' data-hidden='true' selected='selected'>-- Pilih Kategori --</option>
                        </select>
                    </div>
                    <div class='form-group'>
                        <label>Simbol:</label>
                        <input type='text' class='form-control input-xs' id='simbol'>
                    </div>
                    <div class='form-group'>
                        <label>Satuan:</label>
                        <input type='text' class='form-control input-xs' id='satuan'>
                    </div>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {

        $('#tabel thead td').each(function (i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="' + title + '" data-index="' + (i + 1) + '" />');
        });
        tabel_view()
        refreshExisting()
        refreshKategori()
        refreshComponent()

        /// Button Action
        $('#tambah_data').click(function () {
            refreshExisting()
            refreshKategori()
            refreshComponent()
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#form_insert").show();
            $('#form_existing').hide()
            $("#tipe").val('0')
            $("#component ,#existing ,#id_uji, #kd_uji, #nm_uji, #company, #Kategori, #biaya, #satuan, #simbol").val('');
            $("#dlg").modal("show");
        });

        $('#tipe').change(function () {
            var tipe = $('#tipe').val();
            if (tipe == '0') {
                $('#form_uji').show()
                $('#form_existing').hide()
                $("#component ,#existing ,#id_uji, #kd_uji, #nm_uji, #company, #Kategori, #biaya, #satuan, #simbol").val('');
            } else {
                $('#form_uji').hide()
                $('#form_existing').show()
            }

        });

        $('#existing').change(function () {


            $("#component").val($("#existing").val());
            $("#nm_uji").val($("#existing option:selected").attr('nama'));
            $("#Kategori").val($("#existing option:selected").attr('kategori'));
            // $("#biaya").val($("#existing option:selected").attr('biaya'));
            $("#satuan").val($("#existing option:selected").attr('satuan'));
            $("#simbol").val($("#existing option:selected").attr('simbol'));

            $(".selectpicker").selectpicker("refresh");

            $('#form_uji').show();


        });


        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>master/c_m_uji/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

    });


    function refreshComponent() {
        $.post("<?php echo base_url(); ?>master/c_m_uji/refreshComponent", function (data) {
            $("#component").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshExisting() {
        $.post("<?php echo base_url(); ?>master/c_m_uji/refreshExisting", function (data) {
            $("#existing").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshKategori() {
        $.post("<?php echo base_url(); ?>master/c_m_uji/refreshKategori", function (data) {
            $("#Kategori").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function tabel_view() {
        TabelData = $('#tabel').DataTable({
            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'excel',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'pdf',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'print',title: 'Master_Parameter_uji', exportOptions: {columns: [0,1,2,3,4]}},
            ],
            "columnDefs": [
                {"visible": false, "targets": 5},
                {"visible": false, "targets": 6},
                {"visible": false, "targets": 7},
                {"visible": false, "targets": 8},
                {"orderable": false, "targets": 9},
            ],
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "orderCellsTop": true,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            // "sDom": 'Rfrtlip',
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "fixedColumns": {
                "leftColumns": 0,
                "rightColumns": 1
            },
            // "scrollY": '50vh',
            "scrollCollapse": true,
            columns: [
                {data: 'no'},
                {data: 'component'},
                {data: 'Kategori'},
                {data: 'satuan'},
                {data: 'simbol'},
                {data: 'id'},
                {data: 'id_component'},
                {data: 'id_kategori'},
                {data: 'nama'},
                {data: 'aksi'},
            ],
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>master/c_m_uji/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        // if(json[i].NAMA_UJI=='Ya'){
                        // var component = json[i].NM_COMPONENT;
                        // }else{
                        // var component = json[i].NM_COMPONENT+" "+json[i].NAMA_UJI;
                        // }
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect  btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        }

                        return_data.push({
                            'no': no,
                            'component': json[i].NAMA_UJI,
                            'Kategori': json[i].NM_KATEGORI,
                            'satuan': json[i].SATUAN,
                            'simbol': json[i].SIMBOL,
                            'id': json[i].ID_UJI,
                            'id_component': json[i].ID_COMPONENT,
                            'id_kategori': json[i].ID_KATEGORI,
                            'nama': json[i].NAMA_UJI,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            }
        });
        // Apply the search
        TabelData.columns().every(function () {
            var that = this;

            $('input', this.header()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });
    }


    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#id_uji").val(kolom['id']);
        $("#kd_uji").val(kolom['kode']);
        $("#nm_uji").val(kolom['nama']);
        $("#component").val(kolom['id_component']);
        $("#Kategori").val(kolom['id_kategori']);
        $("#satuan").val(kolom['satuan']);
        $("#simbol").val(kolom['simbol']);
        $("#form_insert").hide();
        $("#form_uji").show();
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function simpan() {
        var component = $("#component").val();
        var nm_uji = $("#nm_uji").val();
        var satuan = $("#satuan").val();
        var simbol = $("#simbol").val();
        var Kategori = $("#Kategori").val();
        // if(component == "" && nm_uji == "" ) {
        if (nm_uji == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Paremeter Uji harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/c_m_uji/simpan", {
            // "component": component,
            "nm_uji": nm_uji,
            "satuan": satuan,
            "simbol": simbol,
            "Kategori": Kategori
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function update() {
        var id = $("#id_uji").val();
        var component = $("#component").val();
        var nm_uji = $("#nm_uji").val();
        var Kategori = $("#Kategori").val();
        var simbol = $("#simbol").val();
        var satuan = $("#satuan").val();

        // if(component == "" && nm_uji == "" ) {
        if (nm_uji == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Component dan Paremeter Uji harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/c_m_uji/update", {
            "id": id,
            // "component": component,
            "nm_uji": nm_uji,
            "satuan": satuan,
            "simbol": simbol,
            "Kategori": Kategori
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
            "message": "Anda yakin ingin menghapus Parameter Uji \"" + kolom['nama'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Hapus",
                "action": function (dialog) {
                    hapus(kolom['id'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>master/c_m_uji/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
