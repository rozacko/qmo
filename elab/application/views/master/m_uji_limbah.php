
          <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h2><i class="fa fa-list"></i> Master Uji Limbah</h2>
              
               
                    
                    <div style="text-align:left">
                        <button class="btn btn-sm btn-success  btn_tambah" onclick="add()" style="margin-top: 10px; margin-bottom: 20px;"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                    </div>
                    <div class="ibox-content">
                      <div class="row">
                    <div class="table-responsive">
                    <table id="dataTable" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>Kode Uji</th>
                            <th>Nama Uji</th>
                            <th>Kelompok</th>
                            <th>Satuan</th>
                            <th>Simbol</th>
                            <th></th>
                          </tr>
                        </thead>
                        <?php 
                        $no=0;
                        foreach ($tampilkan->result() as $key) {
                         $no++;
                        

                        ?>
                        <tbody>
                            <tr>
                              <td><?php echo $no; ?></td>
                              <td><?php echo $key->KODE_UJI; ?></td>
                              <td><?php echo $key->NAMA_UJI; ?></td>
                              <td><?php echo $key->KELOMPOK; ?></td>
                              <td><?php echo $key->SATUAN; ?></td>
                              <td><?php echo $key->SIMBOL; ?></td>
                            <?php  error_reporting(0); if($write_menu=="Y"){ ?>
                              <td><button class="btn btn-primary btn-xs waves-effect  btn_edit" onclick="edit(<?php echo $key->ID_UJI ?>)"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                <button class="btn btn-danger btn-xs waves-effect btn_hapus" onclick="hapus()"><span class="glyphicon glyphicon-trash"></span> Hapus</button></td>
                            <?php }else{
                                echo "<td>  - no access -  </td>";
                            } ?>
                            </tr>
                            
                        </tbody>
                        <?php
                        }
                        ?>
                      </table>
                        
                  </div>
                </div>
              </div>
            </div>
          </div>
        
        <script type="text/javascript">
          $(document).ready(function(){
            $('#dataTable').DataTable();
          });

          function add(){
            $('#myModal').modal('show');
          }

          function edit(id){
    
         
         $.ajax({
           url : '<?php echo site_url('master/c_m_uji_limbah/edit/') ?>/'+id,
           type : 'GET',
           dataType : 'JSON',
            success:function(data){
              $('[name="ID_UJI"]').val(data.ID_UJI);
              $('[name="KODE_UJI"]').val(data.KODE_UJI);
              
              $('[name="NAMA_UJI"]').val(data.NAMA_UJI);
              $('[name="KELOMPOK"]').val(data.KELOMPOK);
              $('[name="SATUAN"]').val(data.SATUAN);
              $('[name="SIMBOL"]').val(data.SIMBOL);

              $('#myEdit').modal('show');
          }
        });
    }
          function hapus(){
            $('#hapusData').modal('show');
          }
          function hapusDa(id){
            $.ajax({
              url : '<?php echo site_url('master/c_m_uji_limbah/hapus') ?>/'+id,
              type : 'POST',
              dataType : 'JSON',
              success:function(data){
                alert('Data Berhasil Dihapus');
                location.reload();
              },
              error:function(jqXHR, textStatus, errorThrown){
                alert('Data gagal dihapus');
                
              }
            });
            }
        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Tambah</h4>
             </div>
             <div class="modal-body">
                <form action="<?php echo base_url() ?>master/c_m_uji_limbah/tambah" method="POST">
                  <div class="form-group">
                    <?php 
                    $no =0;
                    foreach ($tampilkan->result() as $key) {
                      $no++;
                    }
                    
                    ?>
                    <input type="hidden" name="ID_UJI" value="<?php echo $no; ?>">
                  </div>
                  <div class="form-group">
                    <label>Kode Uji</label>
                    <input type="text" name="KODE_UJI" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <label>Nama Uji</label>
                    <input type="text" name="NAMA_UJI" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Kelompok</label>
                    <select class="form-control" name="KELOMPOK">
                      <option value="Kimia">Kimia</option>
                      <option value="Fisika">Fisika</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Satuan</label>
                    <input type="text" name="SATUAN" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Simbol</label>
                    <input type="text" name="SIMBOL" class="form-control" required="">
                  </div>
                  <input type="submit" name="tambah" class="btn btn-primary pull-right" value="Simpan"><br>
                </form>
             </div>
             <div class="modal-footer">
                
             </div>
            </div>

          </div>
        </div>

         <!-- Modal Edit-->
        <div id="myEdit" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Edit</h4>
             </div>
             <div class="modal-body">
                <form action="<?php echo base_url() ?>master/c_m_uji_limbah/update" method="POST">
                  <div class="form-group">
                    <input type="hidden" name="ID_UJI">
                  </div>
                  <div class="form-group">
                    <label>Kode Uji</label>
                    <input type="text" name="KODE_UJI" class="form-control" required="">
                  </div>

                  <div class="form-group">
                    <label>Nama Uji</label>
                    <input type="text" name="NAMA_UJI" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Kelompok</label>
                    <input type="text" name="KELOMPOK" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Satuan</label>
                    <input type="text" name="SATUAN" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Simbol</label>
                    <input type="text" name="SIMBOL" class="form-control" required="">
                  </div>
                  <input type="submit" name="update" class="btn btn-primary pull-right" value="Simpan"><br>
                </form>
             </div>
             <div class="modal-footer">
                
             </div>
            </div>

          </div>
        </div>
        <!--modal hapus-->
          <div class="modal fade" id="hapusData">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button class="close" data-dismiss="modal">&times;</button>
                  <h3 class="modal-title text-primary">Hapus Data</h3>
                </div>

                <div class="modal-body">
                  <h3 class="text-center">Apakah Yakin ingin menghapus ?</h3>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-success pull-left" onclick="hapusDa(<?php echo $key->ID_UJI ?>)">Yes</button>
                  <button class="btn btn-danger pull-right" data-dismiss="modal">Tidak</button>
                </div>
              </div>
            </div>
          </div>