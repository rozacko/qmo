<style>
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-lg-6">
                    <h2><i class="fa fa-list"></i> Master Kelompok</h2>
                </div>
                <div class="col-lg-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Tambah
                        </button>&nbsp;&nbsp;&nbsp;
                        <!-- <button type="button" id='btn_xls' class="btn-sm btn-warning"><span
                                    class="fa fa-file-excel-o"></span>&nbsp;Export Excel
                        </button> -->
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                               style="font-size:95%" id="tabel" width="100%">
                            <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th>Nama Kelompok</th>
                                <th>
                                    <center>Aksi <span class="fa fa-filter pull-right" data-filtering="1"
                                                       id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                    </center>
                                </th>
                                <th></th>
                            </tr>
                            <tr id="filtering">
                                <th></th>
                                <td>Nama Kelompok</td>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='form-group id_primary'>
                    <label>ID Kelompok:</label>
                    <input type='text' class='form-control input-xs' id='id_kelompok'>
                </div>
                <div class='form-group'>
                    <label>Nama Kelompok:</label>
                    <input type='text' class='form-control input-xs' id='nm_kelompok'>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {

        $('#tabel thead td').each(function (i) {
            var title = $(this).text();
            $(this).html('<input type="text" style="width : 100%;" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        tabel_view();

        /// Button Action
        $('#tambah_data').click(function () {
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#id_kelompok, #nm_kelompok").val('');
            $("#dlg").modal("show");
        });
        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>master/kelompok/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        })

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });


    function tabel_view() {
        TabelData = $('#tabel').DataTable({
            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "destroy": true,
            // "sDom":'Rfrtlip',
            'dom': 'Bfrtip',
            // 'buttons': [
            // 'copy', 'excel', 'pdf', 'print'
            // ],
            'buttons': [
                {extend: 'copy',title: 'Master_Kelompok',exportOptions: {columns: [3, 1]}},
                {extend: 'excel',title: 'Master_Kelompok',exportOptions: {columns: [3, 1]}},
                {extend: 'pdf',title: 'Master_Kelompok',exportOptions: {columns: [3, 1]}},
                {extend: 'print',title: 'Master_Kelompok',exportOptions: {columns: [3, 1]}},
                ],

            "columnDefs": [
                {'orderable': false, 'targets': 2},
                {'visible': false, 'targets': 3},
            ],
            "serverSide": false,
            "processing": false,
            "orderCellsTop": true,
            "paging": true,
            "lengthChange": true,
            'lengthMenu': [10, 25, 50, 100],
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            // "scrollY": '50vh',
            "scrollCollapse": true,
            columns: [
                {data: 'id'},
                {data: 'nama'},
                {data: 'aksi'},
                {data: 'no'},
            ],
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/master/kelompok/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    console.log(json)
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="fa fa-pencil"></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o" title="Hapus"></i></span></button></center>'
                        }
                        return_data.push({
                            'id': json[i].ID_KELOMPOK,
                            'nama': json[i].NAMA_KELOMPOK,
                            'aksi': aksi,
                            'no': no,
                        })
                        no+=1
                    }
                    return return_data;
                }
            }
        });

        // Apply the search
        TabelData.columns().every(function () {
            var that = this;

            $('input', this.header()).on('keyup change clear', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });


    }

    function simpan() {
        var nama = $("#nm_kelompok").val();
        $('#input_simpan').attr('disabled', true);
        $('#input_simpan').html('<i class="fa fa-spinner fa-spin"> </i> Processing');
// s
        if (nama == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/kelompok/simpan", {
            "nama": nama
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
            $('#input_simpan').attr('disabled', false);
            $('#input_simpan').html('<i class="fa fa-save"> </i> Simpan');
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();
        $("#id_kelompok").val(kolom['id']);
        $("#nm_kelompok").val(kolom['nama']);
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#id_kelompok").prop('readonly', 'readonly');
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
    }

    function update() {
        var id = $("#id_kelompok").val();
        var nama = $("#nm_kelompok").val();
        $('#input_update').attr('disabled', true);
        $('#input_update').html('<i class="fa fa-spinner fa-spin"> </i> Processing');
        if (nama == ""
        ) {
            informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/kelompok/update", {"id": id, "nama": nama}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {
            $('#input_update').attr('disabled', false);
            $('#input_update').html('<i class="fa fa-save"> </i> Update');
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Kelompok</b>",
            "message": "Anda yakin ingin menghapus kelompok \"" + kolom['nama'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Hapus",
                "action": function (dialog) {
                    hapus(kolom['id'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>master/kelompok/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
