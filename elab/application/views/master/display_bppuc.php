

    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    </style>
    
<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i>Display BAPPUC/SPUC</h2>  
            <div style="text-align:left; display:none">
                <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><i class="fa fa-plus">&nbsp;Tambah</i></button>
                <div class="col-sm-8" style="text-align:left; display:none">   <input class="form-control input-sm datepicker" style="cursor:pointer;background-color:#fff;display:none;" value="" id='filter_periode' readonly >
                  </div>
            </div>
            <div class="ibox-content">
                
                
                    
                
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th style='width:10px;'>No</th> 
                                <th>No. BAPPUC </th>
                                <th>Tanggal Terima </th> 
                                <th>Nama Contoh </th> 
                                <th>Print BAPPUC </th> 
                                <th>Print SPUC </th> 
                                <th>Print KUPP </th>
                                <th>Print SPB <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span></th> 
                            </tr>
                            <tr id="filtering">
                            <th style='width:30px;'>
                                <center></center>
                            </th>
                            <td>
                                <center> No. BAPPUC</center>
                            </td>
                            <td>
                                <center> Tanggal Terima </center>
                            </td>
                            <td>
                                <center> Nama Contoh </center>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            
                        </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
				<div class='form-group'  >
					<label>Laboratorium:</label>
					<input type='hidden' class='form-control input-xs' id='id_beban' >
                    	<select class='form-control selectpicker' data-live-search='false' id='lab'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>
					</select>
				</div> 
				<div class='form-group'>
					<label>Beban Kerja Maksimal:</label>
					<input type='number' class='form-control input-xs' id='beban_kerja' >
				</div> 
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
    
$(document).ready(function(){  
        tabel_view() 
        refreshLab()

    $('#tabel thead td').each(function (i) {
            var title = $('#example thead th').eq($(this).index()).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });

        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            console.log($(this).data('index') + "-" + this.value);
            TabelData.column($(this).data('index')).search(this.value).draw();
        });

        TabelData.on('order.dt search.dt', function () {
            TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });
       /// Button Action
	$('#tambah_data').click(function() {  
        refreshLab()
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    }); 
    
    $("#filter_periode").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months",
        autoclose : 'true'
    });
       /// Button Action
	$('.btn_cari').click(function() {    
		tabel_view()
    }); 
   
        
});         

    function tabel_view(){
		var tanggal = $("#filter_periode").val()
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
        "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Data_BAPPUC', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Data_BAPPUC', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Data_BAPPUC', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Data_BAPPUC', exportOptions: {columns: [0,1,2]}},

            ],
                "columnDefs": [
                //{"visible": false, "targets": 3},
                //{"visible": false, "targets": 4},
                {"orderable": false, "targets": 5},
                {"orderable": false, "targets": 0},
            ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' }, 
                  { data : 'no_bappuc' },
                  { data : 'tanggal' }, 
                  { data : 'nama_contoh' },
                  { data : 'print_bppuc' },
                  { data : 'print_spuc' },
                  { data : 'print_kupp' },
                  { data : 'print_spb' },
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/display_bppuc/get_data', 
		                data: {
		                    "tanggal" : tanggal
		                },
		                       
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){ 

                              var print_bppuc = '<center><button onclick="print_bppuc(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-print"></i></span>BAPPUC</button></center>';

                              var print_spuc = '<center><button onclick="print_spuc(\''+json[i].ID_TRANSAKSI+'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-print"></i></span>SPUC</button></center>';

                              var print_kupp = '<center><button onclick="print_kupp(\''+json[i].ID_TRANSAKSI+'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-print"></i></span>KUPP</button></center>';
                              var print_spb = '<center><button onclick="print_spb(\''+json[i].ID_TRANSAKSI+'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-print"></i></span>SPB</button></center>';
                             console.log(print_spuc);
                              var aksi = '<center><button onclick="view_bppuc(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-ok"></i></span>Detail</button></center>';
                             return_data.push({
                              'no'	    :  no ,  
                              'no_bappuc'      :    '<a onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\')" class="btn-labelx success" title="Detail KKPP">'+json[i].NO_BAPPUC+' </a> ', 
                              'tanggal'	    :  json[i].TANGGAL ,
                              'nama_contoh'          :  json[i].NAMA_CONTOH , 
                              'print_bppuc' : print_bppuc ,
                              'print_spuc' : print_spuc,
                              'print_kupp' : print_kupp,
                              'print_spb' : print_spb,
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
    }
	function refreshLab() {
		$.post("<?php echo base_url(); ?>master/beban/refreshLab", function(data) {
			$("#lab").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

	function simpan() {
		var lab = $("#lab").val(); 
		var beban_kerja = $("#beban_kerja").val();  
		if(lab == "" && beban_kerja == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/beban/simpan", {"lab": lab,"beban_kerja": beban_kerja 
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
     
	function edit(baris) { 
		var kolom = TabelData.row(baris).data(); 
        
		$("#id_beban").val(kolom['id_beban']);
		$("#lab").val(kolom['id_lab']);
		$("#lab").prop( "disabled", true );
        $("#lab").css("color", "black");
		$("#beban_kerja").val(kolom['beban']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>"); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_beban").val(); 
		var lab = $("#lab").val(); 
		var beban_kerja = $("#beban_kerja").val();  
		if(lab == "" && beban_kerja == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/beban/update", {"id": id, "lab": lab,"beban_kerja": beban_kerja }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Beban</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_beban'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/beban/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

    function print_bppuc(code)
    {
        window.open("<?=base_url()?>master/display_bppuc/bappcu_pdf/"+code);
    }

    function print_spuc(code)
    {
        window.open("<?=base_url()?>master/display_bppuc/spuc_pdf/"+code);
    }

    function print_kupp(code)
    {
        window.open("<?=base_url()?>kupp/kupp_pdf/"+code);
    }
    function print_spb(code)
    {
      window.open("<?=base_url()?>master/display_bppuc/spb_excel/"+code);
        
    }

</script>