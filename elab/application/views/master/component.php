

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Master Component</h2>
		<div style="text-align:left">
			<button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><i class="fa fa-plus">&nbsp;Tambah</i></button>
		</div>
        <div class="ibox-content">
            <div class="row">
                <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                    <thead>
                        <tr>
                            <th  style='width:10px;'>No</th>
                            <th  style='width:50px;'>Kode Component</th>
                            <th>Nama Component</th> 
                            <th>Simbol</th> 
                            <th>Satuan</th> 
                            <th></th> 
                            <th  style='width:120px;'>Aksi</th>
                        </tr>
                    </thead>
                    <tbody> 			
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'> 
				<div class='form-group'>
					<label>Kode Component:</label>
					<input type='hidden' class='form-control input-xs' id='id_component'>
					<input type='text' class='form-control input-xs' id='kd_component'>
				</div>
				<div class='form-group'>
					<label>Nama Component:</label>
					<input type='text' class='form-control input-xs' id='nm_component' >
				</div> 
				<div class='form-group'>
					<label>Simbol:</label>
					<input type='text' class='form-control input-xs' id='simbol' >
				</div> 
				<div class='form-group'>
					<label>Satuan:</label>
					<input type='text' class='form-control input-xs' id='satuan' >
				</div> 
			</div>
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){  
    tabel_view()
        
       /// Button Action
	$('#tambah_data').click(function() { 
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_component, #kd_component, #nm_component, #simbol, #satuan").val('');
		$("#dlg").modal("show");
    });
    
        
});        


    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [ 
                    { "visible": false, "targets": 5 },  
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'kode' },
                  { data : 'nama' },
                  { data : 'simbol' },
                  { data : 'satuan' },
                  { data : 'id' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/component/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) {
                        console.log(json)
                          var return_data = new Array();
                          var no =1;
                          for(var i=0;i< json.length; i++){
                              if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span>Edit</button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span>Hapus</button></center>'
                              }
                            return_data.push({
                              'no'	 :  no ,
                              'kode' :  json[i].KD_COMPONENT , 
                              'nama' :  json[i].NM_COMPONENT , 
                              'simbol' :  json[i].SIMBOL , 
                              'satuan' :  json[i].SATUAN , 
                              'id' :  json[i].ID_COMPONENT , 
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
        
    }

	function simpan() {
		var kd_component = $("#kd_component").val(); 
		var nm_component = $("#nm_component").val(); 
		var simbol = $("#simbol").val(); 
		var satuan = $("#satuan").val(); 
		if(nm_component == "") {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/component/simpan", {"kd_component": kd_component,"nm_component": nm_component,"simbol": simbol,"satuan": satuan, 
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	} 
	function edit(baris) {
		var kolom = TabelData.row(baris).data();  
		$("#id_component").val(kolom['id']);
		$("#kd_component").val(kolom['kode']);
		$("#nm_component").val(kolom['nama']);
		$("#simbol").val(kolom['simbol']);
		$("#satuan").val(kolom['satuan']);
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#id_kelompok").prop('readonly', 'readonly'); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>"); 
		$("#dlg").modal("show");
	}
    
	function update() {
		var id = $("#id_component").val();
		var kd_component = $("#kd_component").val(); 
		var nm_component = $("#nm_component").val(); 
		var simbol = $("#simbol").val(); 
		var satuan = $("#satuan").val(); 
		if(nm_component == "") {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama harus terisi.");
			return;
		}  
		$.post("<?php echo base_url(); ?>master/component/update", {"id": id,"kd_component": kd_component,"nm_component": nm_component,"simbol": simbol,"satuan": satuan, }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Component</b>",
			"message": "Anda yakin ingin menghapus kelompok \"" + kolom['nama']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/component/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>