
<style type="text/css">
	div.dataTables_length{
		margin-right: 0.5em;
		margin-top: 0.2em;
	}
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="">
                	<div class="col-lg-6">
                		<h2><i class="fa fa-list"></i> Master Menu</h2>
                	</div>
                	<div class="col-lg-6">
               			 <div style="text-align:right;">
		                    <button type="button" class="btn-sm btn-success  btn_tambah" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>
		                </div> 		
                	</div>
                </div>
               
                <div class="ibox-content">
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel" width="100%">
                            <thead>
                                <tr>
                                    <th><center> Posisi </center></th>
                                    <th><center> Urutan </center></th>
                                    <th><center> Nama </center></th>
                                    <th><center> Link </center></th>
                                    <th><center> Controller </center></th>
                                    <th><center> Icon </center></th> 
                                    <th> </th>
                                    <th> </th>
                                    <th> </th>
                                    <th> </th>
                                    <th> </th>
                                    <th><center> Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span> </center></th>
                                </tr>
                                <tr id="filtering">
                                    <td><center> Posisi </center></td>
                                    <td><center> Urutan </center></td>
                                    <td><center> Nama </center></td>
                                    <td><center> Link </center></td>
                                    <td><center> Controller </center></td>
                                    <td><center> Icon </center></td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <th><center></center></th>
                                </tr>
                            </thead>
                            <tbody></tbody> 
                        </table>
                    </div>
                </div>
            </div>  
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style="font-size: 16px;font-weight: bold;" id='judul_input'></div>
			</div>
			<div class='modal-body'>
				<input type='hidden' id='input_id_menu' style='display:none;'>
				<div class='form-group form-group-xs'>
					<label>Posisi Menu:</label>
					<select class='form-control selectpicker' data-live-search='false' id='input_posisi_menu' onchange="showParent();">
						<option value='' data-hidden='true' selected='selected'>-- Pilih Posisi Menu --</option>
						<option value='1'>Parent</option>
						<option value='2'>Submenu</option>
					</select>
				</div>
				<div class='form-group'>
					<label>Nama Menu:</label>
					<input type='text' class='form-control input-xs' id='input_nama_menu' maxlength='64'>
				</div>
				<div class='form-group'>
					<label>Link Menu:</label>
					<input type='text' class='form-control input-xs' id='input_link_menu' maxlength='128' placeholder='Tidak perlu diisi jika ini akan menjadi dropdown'>
				</div>
				<div class='form-group'>
					<label>Controller Menu:</label>
					<input type='text' class='form-control input-xs' id='input_controller_menu' maxlength='128' placeholder='Tidak perlu diisi jika ini akan menjadi dropdown'>
				</div>
				<div class='form-group'>
					<label>Urutan Menu:</label>
					<input type='number' class='form-control input-xs' id='input_urutan_menu' min='1' placeholder='Boleh sama, hanya untuk keperluan sorting'>
				</div>
				<div class="row" id='par-only'>
					<div class="col-md-6">
						<div class='form-group'>
							<label>Icon Menu:</label>
							<input type='text' class='form-control input-xs' id='input_icon_menu' maxlength='32'>
						</div>
					</div>
					<div class="col-md-6">
						<div class='form-group form-group-xs'>
							<label>Icon Colour:</label>
							<select class='form-control selectpicker' data-live-search='false' id='input_icon_color'>
								<option value='' data-hidden='true' selected='selected'>-- Pilih Warna Ikon --</option>
								<option value='danger'>Red</option>
								<option value='primary'>Green</option>
								<option value='success'>Blue</option>
								<option value='warning'>Orange</option>
								<option value='info'>Light Blue</option>
							</select>
						</div>
					</div>
				</div>
				<div class='form-group form-group-xs' id='sub-only' style="display:none;">
					<label>Parent Menu:</label>
					<select class='form-control selectpicker' data-live-search='false' id='input_parent_menu'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Parent Menu --</option>
					</select>
				</div>
			</div>
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-simpan btn-xs' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-edit btn-xs' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-tutup btn-xs' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>
<script type='text/javascript'>
	var TabelData;
	$(document).ready(function() { 

		$('#tabel thead td').each( function (i) {
	        var title = $('#tabel thead td').eq( $(this).index() ).text();
	        $(this).html( '<input type="text" style="width : 100%;" placeholder="Search '+title+'" data-index="'+i+'" />' );
	    });

        tabel_view() ;        
        refreshParent();
        $('#tambah_data').click(function() { 
             
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#input_id_menu, #input_posisi_menu, #input_nama_menu, #input_link_menu, #input_controller_menu, #input_icon_menu, #input_icon_color, #input_urutan_menu, #input_parent_menu").val(''); 
            $("#dlg").modal("show");
        });

        $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
	        TabelData.column( $(this).data('index') ).search( this.value ).draw();
	     });

         $(document).on('click','#btn_filtering', function(){
       		var data = $(this).data('filtering');
       		if(data==1){
       			$('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
       			$('#filtering').hide(500);

       		}else{
       			$('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
       			$('#filtering').show(500);
       		}
       });
	});
    
    
    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" }, 
                "columnDefs": [
                    { "visible": false, "targets": 10 },
                    { "visible": false, "targets": 9 },
                    { "visible": false, "targets": 8 },
                    { "visible": false, "targets": 7 }, 
                    { "visible": false, "targets": 6 }, 
                    { "visible": false, "targets": 1 }, 
                    { "orderable": false, "targets": 11 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "orderCellsTop":true,
                "paging": true,
                "lengthMenu":[10,25,50,100],
                "sDom": 'Rfrtlip',
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                // "autoWidth": false,
                // "scrollY": '50vh',
                "scrollX": true,
                columns : [
                  { data : 'posisi' },
                  { data : 'urutan' },
                  { data : 'nama' },
                  { data : 'link' }, 
                  { data : 'controller' },
                  { data : 'icon' },
                  { data : 'warna' },
                  { data : 'id_menu' },
                  { data : 'id_posisi' },
                  { data : 'id_parent' },
                  { data : 'id_icon' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>master/menu/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){ 
                            var posisi = (json[i].POSISI_MENU=='1' ? 'Parent' : 'Submenu')
                              if(write_menu==''){
                                  var aksi = '- no access -';
                              }else{
                                  var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                              }
                            return_data.push({
                              'posisi'	    :  posisi ,
                              'urutan'	    :  json[i].URUTAN_MENU ,
                              'nama'        :   json[i].NAMA_MENU, 
                              'link'	    :  json[i].LINK_MENU ,
                              'controller'  :  json[i].CONTROLLER_MENU ,  
                              'icon'        :  '<center><i class="fa '+json[i].ICON_MENU+'"></i></center>'  ,  
                              'warna'       :  json[i].ICON_COLOR ,  
                              'id_menu'     :  json[i].ID_MENU ,  
                              'id_posisi'     :  json[i].POSISI_MENU ,  
                              'id_parent'   :  json[i].PARENT_MENU ,  
                              'id_icon'   :  json[i].ICON_MENU ,  
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});
    }
    
	function edit(baris) {
		var kolom = TabelData.row(baris).data();
		$("#input_id_menu").val(kolom['id_menu']);
		$("#input_posisi_menu").val(kolom['id_posisi']);
		$("#input_nama_menu").val(kolom['nama']);
		$("#input_link_menu").val(kolom['link']);
		$("#input_controller_menu").val(kolom['controller']);
		$("#input_icon_menu").val(kolom['id_icon']);
		$("#input_icon_color").val(kolom['warna']);
		$("#input_urutan_menu").val(kolom['urutan']);
		$("#input_parent_menu").val(kolom['id_parent']);
		$(".selectpicker").selectpicker("refresh");
		showParent();
		$("#input_simpan").hide();
		$("#input_update").show();
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Edit Menu</b>");
		$("#dlg_header").removeClass("modal-primary").addClass("modal-success");
		$("#dlg").modal("show");
	}
	function update() {
		var id_menu = $("#input_id_menu").val();
		var posisi_menu = $("#input_posisi_menu").val();
		var nama_menu = $("#input_nama_menu").val();
		var link_menu = $("#input_link_menu").val();
		var controller_menu = $("#input_controller_menu").val();
		var icon_menu = $("#input_icon_menu").val();
		var icon_color = $("#input_icon_color").val();
		var urutan_menu = $("#input_urutan_menu").val();
		var parent_menu = $("#input_parent_menu").val();
		$('#input_update').attr('disabled',true);
		$('#input_update').html('<i class="fa fa-spinner fa-spin"></i> Processing'); 

		if(posisi_menu == "" || nama_menu == "" || (posisi_menu == 1 && (icon_menu == "" || icon_color == "")) || urutan_menu == "" || (posisi_menu == 2 && parent_menu == "")) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi kecuali Link.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/menu/update", {"id_menu": id_menu, "posisi_menu": posisi_menu, "nama_menu": nama_menu, "link_menu": link_menu, "controller_menu": controller_menu, "icon_menu": icon_menu, "icon_color": icon_color, "urutan_menu": urutan_menu, "parent_menu": parent_menu}, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                tabel_view()
				refreshParent();
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
			$('#input_update').attr('disabled',false);
			$('#input_update').html('<i class="fa fa-save"></i> Update'); 
		});
	}
	function konfirmasi(baris) {
		var kolom = TabelData.row(baris).data();
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Menu</b>",
			"message": "Anda yakin ingin menghapus menu \"" + kolom['nama'] + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-hapus btn-xs",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_menu'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-tutup btn-xs",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
	function hapus(id_menu, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/menu/hapus", {"id_menu": id_menu}, function(data) {
			if(data === "berhasil") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				tabel_view()
				refreshParent();
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}
	function tambah() {
		$("#input_nama_menu").val("");
		$("#input_link_menu").val("");
		$("#input_controller_menu").val("");
		$("#input_icon_menu").val("");
		$("#input_icon_color").val("");
		$("#input_urutan_menu").val("");
		$("#input_parent_menu").val("");
        $("#input_posisi_menu").val("")
		$(".selectpicker").selectpicker("refresh");
		showParent();
		$("#input_simpan").show();
		$("#input_update").hide();
		$("#judul_input").html("<b><i class='fa fa-plus'></i>&nbsp;Tambah Menu</b>");
		$("#dlg_header").removeClass("modal-success").addClass("modal-primary");
		$("#dlg").modal("show");
	}
	function simpan() {
		var posisi_menu = $("#input_posisi_menu").val();
		var nama_menu = $("#input_nama_menu").val();
		var link_menu = $("#input_link_menu").val();
		var controller_menu = $("#input_controller_menu").val();
		var icon_menu = $("#input_icon_menu").val();
		var icon_color = $("#input_icon_color").val();
		var urutan_menu = $("#input_urutan_menu").val();
		var parent_menu = $("#input_parent_menu").val();
		$('#input_simpan').attr('disabled',true);
		if(posisi_menu == "" || nama_menu == "" || (posisi_menu == 1 && (icon_menu == "" || icon_color == "")) || urutan_menu == "" || (posisi_menu == 2 && parent_menu == "")) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua kolom harus terisi kecuali Link.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/menu/simpan", {"posisi_menu": posisi_menu, "nama_menu": nama_menu, "link_menu": link_menu, "controller_menu": controller_menu, "icon_color": icon_color, "icon_menu": icon_menu, "urutan_menu": urutan_menu, "parent_menu": parent_menu}, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menyimpan data.");
				 tabel_view()
				refreshParent();
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
			$('#input_simpan').attr('disabled',false);
		});
	}
	function refreshParent() {
		$.post("<?php echo base_url(); ?>master/menu/refreshParent", function(data) {
			$("#input_parent_menu").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
	function showParent() {
		var posisi_menu = $("#input_posisi_menu").val();
		if(posisi_menu == 2) {
			$("#par-only").hide();
			$("#input_icon_menu").val("");
			$("#sub-only").show();
		} else {
			$("#sub-only").hide();
			$("#input_parent_menu").val("");
			$("#par-only").show();
		}
	}
</script>