  <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
     div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }

   /* .dataTables_wrapper .dataTables_filter {
      float: right;
      text-align: right;
      visibility: hidden;
    }*/
 </style>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                    <div class="">
                      <div class="col-lg-6"><h2><i class="fa fa-list"></i> Master Sertifikat</h2></div>
                      <div class="col-lg-6">
                        <div style="text-align:right">
                           <button type="button" class="btn-sm btn-success" onclick="add()"><span class="fa fa-plus"></span>&nbsp;Tambah</button>&nbsp;&nbsp;&nbsp;
                           <!-- <button type="button" onclick="excel()" class="btn-sm btn-warning" ><i class="fa fa-file-excel-o"></i>&nbsp;Export Excel</button> -->
                        </div>
                      </div>
                    </div>

                    <div class="ibox-content">
                      <div class="row">
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" width="100%">
                        <thead>
                          <tr>
                            <th class='filter_table' style='cursor: pointer;'>No</th>
                            <th><center>Kelompok<center></center></th>
                            <th><center>Lab</center></th>
                            <th><center>Nomor Sertifikat</center></th>
                            <th><center>Masa Berlaku</center></th>
                            <th><center>Status</center></th> 
                            <th><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>
                           <tr id='filtering'>
                            <th></th>
                            <td>Kelompok</td>
                            <td>Lab</td>
                            <td>Nomor Sertifikat</td>
                            <td>Masa Berlaku</td>
                            <td>Status</td> 
                            <th></th>
                          </tr>

                        </thead>



                      </table>

                </div>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          $(document).ready(function(){
             $('[data-toggle="tooltip"]').tooltip();
             tableData = $('#Table').DataTable({
                  "destroy": true,
                  'dom': 'Bfrtip',
                  'buttons': [
                      {extend: 'copy',title: 'Master_Sertifikat',exportOptions: {columns: [0, 1, 2, 3, 4, 5]}},
                      {extend: 'excel',title: 'Master_Sertifikat',exportOptions: {columns: [0, 1, 2, 3, 4, 5]}},
                      {extend: 'pdf',title: 'Master_Sertifikat',exportOptions: {columns: [0, 1, 2, 3, 4, 5]}},
                      {extend: 'print',title: 'Master_Sertifikat',exportOptions: {columns: [0, 1, 2, 3, 4, 5]}},
                      ],
                  "processing": true,
                  // "sDom": 'Rfrtlip',
                  "serverSide": false,
                  "searching":true,
                  // 'fDom':false,
                  "info":true,
                  "processing": "DataTables is currently busy",
                  "lengthMenu": [10, 25, 50, 100],
                  "pageLength": 10,
                  "ordering": true,
                  "orderCellsTop" : true,
                  "columnDefs" :[
                    {'orderable' :false, 'targets':6},
                    // {'visible' :false, 'targets':6},
                  ],
                  "ajax": {
                      "url": "<?php echo site_url('master/Sertifikat/ajax_list') ?>",
                      "type": "POST"
                  },
                  "columns" :[
                  {data: 'NO', name: 'NO'},
                  {data: 'NAMA_KELOMPOK', name: 'NAMA_KELOMPOK'},
                  {data: 'NM_LAB', name: 'NM_LAB'},
                  {data: 'N_SERT', name: 'N_SERT'},
                  {data: 'M_BERLAKU', name: 'M_BERLAKU'},
                  {data: 'STATUS', name: 'STATUS'},
                  // {data: 'ID_SERTIFIKAT', name: 'ID_SERTIFIKAT'},
                  // {data: 'ID_SERTIFIKATs', render:function(d,t,f){
                    // return "<center><button class='btn btn-primary btn-xs waves-effect' onclick='edit("+d+")' title='Edit'><span class='fa fa-pencil'></span></button>&nbsp;<button class='btn btn-danger btn-xs waves-effect' onclick='hapus("+d+")' title='Delete'><span class='fa fa-trash-o'></span></button></center>";
                  // }},
                  {data: 'ID_SERTIFIKAT', render:function(d,t,f){
                    return '<center><button class="btn btn-primary btn-xs waves-effect" onclick="edit('+d+')" title="Edit"><span class="fa fa-pencil"></span></button>&nbsp;<button  onclick="konfirmasi(\'' + d + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>';
                  }}, 
                  ],

            });
              $("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
             // $("div.toolbar").html('<b>Custom tool bar! Text/images etc.</b>');
            $('#Table thead td').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="'+title+'" data-index="'+(i+1)+'"/>' );
            } );

            $( tableData.table().container() ).on( 'keyup', 'thead input', function () {
                console.log($(this).data('index')+"-"+this.value);
                tableData.column( $(this).data('index') ).search( this.value ).draw();
             });

            tableData.on( 'order.dt search.dt', function () {
                tableData.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            	$('.filter_table').click(function() {
                       var status=  $('#filtering').val();
                       if(status=='1'){
                            $('#filtering').val('0')
                            $('.filtering').hide(500)
                       }else{
                            $('#filtering').val('1')
                            $('.filtering').show(500)
                       }
                });

          // $('#Table_filter').remove();


          $(document).on('click','#btn_filtering', function(){
            var data = $(this).data('filtering');
            if(data==1){
              $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
              $('#filtering').hide(500);

            }else{
              $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
              $('#filtering').show(500);
            }
         });

          });

          var save_method;
          var table;


          function add()
          {
            save_method ='add';
            $('#submit').text('SIMPAN');
            $('#form')[0].reset();
            $('.modal-title').text('Tambah Data');
            $('#myModal').modal('show');
          }
          function save()
          {
            var url;

            if(save_method == 'add'){
              url = '<?php echo site_url('master/Sertifikat/tambah'); ?>';
            }
            else{
              url = '<?php echo site_url('master/Sertifikat/update'); ?>';
            }
            $.ajax({
              url:url,
              type:'POST',
              data:$('form').serialize(),
              dataType:'JSON',
              success:function(data){
                //show_toaster(1, "Success", "Success Save Data");
                 informasi(BootstrapDialog.TYPE_SUCCESS, "Sukses menyimpan data.");
                // location.reload();
                tableData.ajax.reload();
                $('#myModal').modal('hide');
              },
              error:function(jqXHR, textStatus, errorThrown)
              {
               informasi(BootstrapDialog.TYPE_DANGER, "Harap Periksa data.");
               //show_toaster(2, "Failed", "Failed Save Data");
              }
            });
          }
          // function hapus(id){
            // if (confirm("Apakah yakin ingin menghapus?")) {
            // $.ajax({
                // url:'<?php echo base_url() ?>master/Sertifikat/hapusData',
                // type: 'post',
                // data: {id:id},
                // success: function () {
                    // show_toaster(1, "Success", "Delete Data Successfully");
                    // tableData.ajax.reload();
                // },
                // error: function () {
                    // show_toaster(2, "Failed", "Delete Data Failed");
                // }
                 // });
                // }

            // }
            
            
            function konfirmasi(baris) {
                var kolom = tableData.row(baris).data();
                console.log(baris)
                BootstrapDialog.show({
                    "type": BootstrapDialog.TYPE_DANGER,
                    "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Sertifikat</b>",
                    "message": "Anda yakin ingin menghapus Sertifikat  ?",
                    "closeByBackdrop": false,
                    "closeByKeyboard": false,
                    "buttons": [{
                        "cssClass": "btn btn-danger btn-xs btn-hapus",
                        "icon": "fa fa-trash",
                        "label": "Hapus",
                        "action": function (dialog) {
                            hapus(baris, dialog);
                        }
                    }, {
                        "cssClass": "btn btn-default btn-xs btn-tutup",
                        "icon": "fa fa-times",
                        "label": "Tutup",
                        "action": function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            }


            function hapus(id, dialog) {
                dialog.setClosable(false);
                $.post("<?php echo base_url(); ?>master/Sertifikat/hapusData", {"id": id}, function (datas) {
                    var data = JSON.parse(datas);
                    if (data.status == true) {
                        dialog.close();
                        informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                        tableData.ajax.reload();
                    } else {
                        informasi(BootstrapDialog.TYPE_DANGER, data.message);
                    }
                }).fail(function () {
                    informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
                }).always(function () {
                    dialog.setClosable(true);
                });
            }
          function edit(id){
            save_method= 'update';
            $('#form')[0].reset();

            $.ajax({
              url : '<?php echo site_url('master/Sertifikat/edit'); ?>/'+id,
              type : 'GET',
              dataType:'JSON',
              success:function(data){
                $('[name="id_sertifikat"]').val(data.ID_SERTIFIKAT);
                $('[name="kelompok"]').val(data.KELOMPOK);
                $('[name="lab"]').val(data.LAB);
                $('[name="no_Sert"]').val(data.N_SERT);
                $('[name="masa"]').val(data.M_BERLAKU);
                $('[name="status"]').val(data.STATUS);


                $('#myModal').modal('show');
                $('.modal-title').text('UPDATE');
                $('#submit').text('UPDATE');
              }
            });
          }
          function excel(){
            $.ajax({
              url : '<?php echo site_url('master/Sertifikat/excel') ?>',
              type:'POST',
              success:function(data){
                window.open('<?php echo site_url('master/Sertifikat/excel') ?>','_blank' );
              }
            });
          }

        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Tambah Data</h4>
             </div>
             <div class="modal-body form">
                <form action="" method="POST" id="form">
                  <div class="form-group">
                    <input type="hidden" name="id_sertifikat">
                  </div>
                  <div class="form-group">
                    <label>Kelompok</label>
                    <select class="form-control"   name="kelompok" id="kelompok">
                                                              <option value="">--Pilih--</option>
                                                        <?php
foreach ($kelompok->result() as $key) {
    ?>
                                                          <option value="<?php echo $key->ID_KELOMPOK ?>"><?php echo $key->NAMA_KELOMPOK ?></option>
                                                          <?php
}
?>
                                                      </select>
                  </div>
                  <div class="form-group">
                    <label>Laboratorium</label>
                    <select class="form-control"   name="lab" id="lab">
                                                              <option value="">--Pilih--</option>
                                                        <?php
foreach ($lab->result() as $key) {
    ?>
                                                          <option value="<?php echo $key->ID_LAB ?>"><?php echo $key->NM_LAB ?></option>
                                                          <?php
}
?>
                                                      </select>
                  </div>
                  <div class="form-group">
                    <label>Nomor Sertifikat</label>
                    <input type="text" name="no_Sert" class="form-control" id="no_Sert">
                  </div>

                  <div class="form-group">
                    <label>Masa Berlaku</label>
                    <input type="date" name="masa" class="form-control" id="masa">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select name="status" id="status" class="form-control">
                        <option value="" >Select</option>
                        <option value="ON" >ON</option>
                        <option value="OFF" >OFF</option>
                    </select>
                    <!-- <input type="text" name="status" class="form-control" id="status"> -->
                  </div>
                  </form>

             </div>
             <div class="modal-footer">
              <button class="btn btn-primary" onclick="save()" id="submit">Simpan</button>

             </div>
            </div>

          </div>
        </div>
