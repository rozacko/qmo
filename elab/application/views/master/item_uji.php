 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    div.dataTables_length{
      margin-right: 0.5em;
      margin-top: 0.2em;
    }

 </style>


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			     <div class="">
              <div  class="col-md-6">
                <h2><i class="fa fa-list"></i> Master Nilai Batas</h2>
              </div>
              <div class="col-md-6">
                <div style="text-align:right;">
                    <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span class="fa fa-plus"></span>&nbsp;Tambah</button>&nbsp;&nbsp;&nbsp;
                    <!-- <button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel</button> -->
                </div>
              </div>
           </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel" width="100%">
                        <thead>
                            <tr >
                                <th  style='width:10px;'>No</th>
                                <th>Parameter Uji </th>
                                <th>Spesifikasi Standar Produk </th>
                                <th >Min </th>
                                <th >Max </th>
                                <th >Nilai Cetak	 </th>
                                <th>Standar Metode Uji </th>
                                <th> </th>
                                <th> </th>
                                <th> </th>
                                <th  style='width:100px;'><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                            </tr>
                            <tr id="filtering">
                                <th  style='width:10px;'></th>
                                <td>Parameter Uji </td>
                                <td>Spesifikasi Standar Produk </td>
                                <td >Min </td>
                                <td >Max </td>
                                <td >Nilai Cetak	 </td>
                                <td>Standar Metode Uji </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <th  style='widtd:100px;'></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>
              <div id="form_insert">
                <div class='form-group'>
                  <label>Tipe Insert:</label>
                  <select class='form-control selectpicker' data-live-search='false' id='tipe'>
                    <option value='0' selected='selected'>New</option>
                    <option value='1'  '>Existing</option>
                  </select>
                </div>

                <div class='form-group' id='form_existing'>
                  <label>Existing:</label>
                  <select class='form-control selectpicker' data-live-search='true' id='existing'>
                    <option value='' data-hidden='true' selected='selected'>-- Pilih Data --</option>
                  </select>
                </div>
              </div>
            <div id="form_uji">
				<div class='form-group'>
					<label>Parameter Uji:</label>
					<input type='hidden' class='form-control input-xs' id='id_item' >
					<select class='form-control selectpicker' data-live-search='true' id='uji' data-size="10" >
						<option value='' data-hidden='true' selected='selected'>-- Pilih Uji --</option>
					</select>
				</div>
				<div class='form-group'>
					<label>Standart:</label>
					<select class='form-control selectpicker' data-live-search='true' id='standart' data-size="10" >
						<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>
					</select>
				</div>
				<div class='form-group'>
					<label>Nilai Minimal:</label>
					<input type='text' class='form-control input-xs' id='min' >
				</div>
				<div class='form-group'>
					<label>Nilai Maximal:</label>
					<input type='text' class='form-control input-xs' id='max' >
				</div>
				<div class='form-group'>
					<label>Nilai Cetak:</label>
					<select class='form-control selectpicker' data-live-search='false' id='cetak'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Cetak --</option>
						<option value='-' > - </option>
						<option value='Min' > Min </option>
						<option value='Max' > Max </option>
						<option value='MinMax' > MinMax </option>
					</select>
				</div>
				<div class='form-group'>
					<label>Spesifikasi:</label>
					<input type='text' class='form-control input-xs' id='metode' >
				</div>
                </div>


			</div>
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-save'></i>&nbsp;Update</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){
   $('#tabel thead td').each( function (i) {
              var title = $('#example thead th').eq( $(this).index() ).text();
              $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+(i+1)+'" />' );
          });
        tabel_view()
        refreshItem()
        refreshStandart()
         refreshExisting()



       /// Button Action
	$('#tambah_data').click(function() {
         refreshExisting()
        refreshItem()
        refreshStandart()
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $("#form_insert").show();
        $("#form_uji").show();
          $('#form_existing').hide()
          $("#tipe").val('0')
        $("#uji, #standart, #min, #max, #cetak, #metode,#biaya, #id_item").val('');
		$("#dlg").modal("show");
    });
    $('#tipe').change(function() {
      var tipe =  $('#tipe').val();
      if(tipe=='0'){
          $('#form_uji').show()
          $('#form_existing').hide()
          $("#component ,#existing ,#uji, #standart, #min, #max, #cetak, #metode,#biaya, #id_item").val('');
      }else{
          $('#form_uji').hide()
          $('#form_existing').show()
      }
      });

    $('#existing').change(function() {


      $("#uji").val($("#existing").val());
      $("#standart").val($("#existing option:selected").attr('standart'));
      $("#min").val($("#existing option:selected").attr('min'));
      $("#max").val($("#existing option:selected").attr('max'));
      $("#cetak").val($("#existing option:selected").attr('cetak'));
      $("#metode").val($("#existing option:selected").attr('metode'));
      $("#biaya").val($("#existing option:selected").attr('biaya'));

      $(".selectpicker").selectpicker("refresh");

          $('#form_uji').show()
    });

        $("#btn_xls").on("click",function (){
          var link = '<?php echo base_url(); ?>master/item_uji/export_xls/';
          var buka = window.open(link,'_blank');
          buka.focus();
        })

        $(document).on('click','#btn_filtering', function(){
          var data = $(this).data('filtering');
          if(data==1){
            $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
            $('#filtering').hide(500);

          }else{
            $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
            $('#filtering').show(500);
          }
       });

});

      document.addEventListener('DOMContentLoaded', () => {
         const  cleave = new Cleave('#biaya', {
            numeral: true,
            numeralThousandsGroupStyle: 'thousand'
        });
    });
  function refreshExisting() {
    $.post("<?php echo base_url(); ?>master/item_uji/refreshExisting", function(data) {
      $("#existing").html(data);
    }).fail(function() {
      // Nope
    }).always(function() {
      $(".selectpicker").selectpicker("refresh");
    });
  }
	function refreshItem() {
		$.post("<?php echo base_url(); ?>master/item_uji/refreshItem", function(data) {
			$("#uji").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
	function refreshStandart() {
		$.post("<?php echo base_url(); ?>master/item_uji/refreshStandart", function(data) {
			$("#standart").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}
    function tabel_view(){
       	TabelData = $('#tabel').DataTable({
				        "oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 7 },
                    { "visible": false, "targets": 8 },
                    { "visible": false, "targets": 9 },
                    { "orderable": false, "targets": 10 },
                  ],
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Nilai_batas', exportOptions: {columns: [0,1,2,3,4,5,6]}},
                {extend: 'excel',title: 'Master_Nilai_batas', exportOptions: {columns: [0,1,2,3,4,5,6]}},
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    exportOptions: {columns: [0,1, 2, 3, 4, 5, 6,7]}
                },
                {extend: 'print', exportOptions: {columns: [0,1,2,3,4,5,6]}},
            ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": true,
                "orderCellsTop":true,
                "searching": true,
                "ordering": true,
                // "sDom":'Rfrtlip',
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'uji' },
                  { data : 'standart' },
                  { data : 'min' },
                  { data : 'max' },
                  { data : 'cetak' },
                  { data : 'metode' },
                  { data : 'id_item' },
                  { data : 'id_uji' },
                  { data : 'id_standart' },
                  { data : 'aksi' },
                ],
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/item_uji/get_data',
                     dataType: 'JSON',
                     dataSrc : function (json) {
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){

                          if(write_menu==''){
                              var aksi = '- no access -';
                          }else{
                              var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                          }
                            return_data.push({
                              'no'	    : no ,
                              'uji'	    : json[i].NAMA_UJI ,
                              'standart'      :  json[i].SPESIFIKASI,
                              'min'	    :  json[i].MIN ,
                              'max'	    :  json[i].MAX ,
                              'cetak'	    :  json[i].NILAI_CETAK ,
                              'metode'	    :    json[i].NAMA_STANDART ,
                              // 'biaya'      :   (json[i].BIAYA != null ? formatRupiah(json[i].BIAYA, 'Rp. ') : ''),
                              'id_item'	    :  json[i].ID_ITEM ,
                              'id_uji'	    :  json[i].ID_UJI ,
                              'id_standart'          :  json[i].ID_STANDART ,
                              // 'id_biaya'          :  json[i].BIAYA ,
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		});// Apply the search
            TabelData.columns().every( function () {
                var that = this;

                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );



           $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
            console.log($(this).data('index')+"-"+this.value);
            TabelData.column( $(this).data('index') ).search( this.value ).draw();
         });

    }

	function simpan() {
		var uji = $("#uji").val();
		var standart = $("#standart").val();
		var min = $("#min").val();
		var max = $("#max").val();
		var cetak = $("#cetak").val();
		var metode = $("#metode").val();
		// var biaya = $("#biaya").val();
		if(uji == "" && standart == "" && min == "" && max == "" && cetak == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Semua harus terisi.");
			return;
		}
		$.post("<?php echo base_url(); ?>master/item_uji/simpan", {"uji": uji,"standart": standart,"min": min,"max": max,"cetak": cetak,"metode": metode
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() {
		});
	}

	function edit(baris) {
		var kolom = TabelData.row(baris).data();

		$("#id_item").val(kolom['id_item']);
		$("#uji").val(kolom['id_uji']);
		$("#standart").val(kolom['id_standart']);
		$("#min").val(kolom['min']);
		$("#max").val(kolom['max']);
		$("#cetak").val(kolom['cetak']);
		$("#metode").val(kolom['standart']);
		$("#biaya").val(kolom['id_biaya']);
        $("#form_insert").hide();
        $("#form_uji").show();
        $(".btn-simpan").hide();
        $(".btn-edit").show();
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");
	}

	function update() {
		var id = $("#id_item").val();
		var uji = $("#uji").val();
		var standart = $("#standart").val();
		var min = $("#min").val();
		var max = $("#max").val();
		var cetak = $("#cetak").val();
		var metode = $("#metode").val();
		// var biaya = $("#biaya").val();
		if(uji == "" && standart == "" && min == "" && max == "" && cetak == ""  ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama Contoh harus terisi.");
			return;
		}
		$.post("<?php echo base_url(); ?>master/item_uji/update", {"id": id,"uji": uji,"standart": standart,"min": min,"max": max,"cetak": cetak,"metode": metode }, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() {
		});
	}
    function konfirmasi(baris) {
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
			"message": "Anda yakin ingin menghapus Item Uji \"" + kolom['uji']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id_item'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}


	function hapus(id, dialog) {
		dialog.setClosable(false);
		$.post("<?php echo base_url(); ?>master/item_uji/hapus", {"id": id}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() {
			dialog.setClosable(true);
		});
	}
    function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}

</script>
