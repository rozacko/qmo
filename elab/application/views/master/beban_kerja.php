
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    .ui-datepicker-calendar {
        display: none;
    }


.redClass  {
  color: red !important;
  font-weight : bold;
}
 </style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Display Beban Kerja</h2>
            <div style="text-align:left; display:none" >
                <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><i class="fa fa-plus">&nbsp;Tambah</i></button>&nbsp;&nbsp;&nbsp;<button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><i class="fa fa-file-excel-o">&nbsp;Export Excel</i></button>
            </div>
            <div class='row'>
				<div class='form-group col-sm-3'>
					<label>Laboratorium:</label>
					<select class='form-control selectpicker' data-live-search='false' id='laboratorium'>
						<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>
					</select>
				</div>
				<div class='form-group col-sm-2'>
					<label>Bulan Tahun:</label>
                    <input name="startDate" id="startDate" class="form-control date-picker" style='background-color:white' value='<?php echo date('m-Y') ?>' readonly=''/>
				</div>
				<div class='form-group col-sm-2'>
					<label>&nbsp;&nbsp;&nbsp;</label> <br />
					<button type="button" class="btn-sm btn-success filter" id="filter"><i class="fa fa-search">&nbsp;Search</i></button>
				</div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th  style='width:10px;'></th>
                                <th  width="35%">Kategori </th>
                                <th >Max. Beban Kerja </th>
                                <th >Rekap SPUC</th>
                                <th >Rekap Sampel</th>
                                <th >Sampel selesai</th>
                                <th >Sampel belum selesai</th>
                                <th >Overload</th>
                            </tr>
                            <tr id="filtering">
                                 <th  style='widtd:10px;'></th>
                                <td width="35%">Kategori </td>
                                <td >Max. Beban Kerja </td>
                                <td >Rekap SPUC</td>
                                <td >Rekap Sampel</td>
                                <td >Sampel selesai</td>
                                <td >Sampel belum selesai</td>
                                <td ></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){
    $('#tabel thead td').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
        tabel_view()
       refreshLab()

      $("#startDate").datepicker( {
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months", autoclose: true
        });
       /// Button Action
	$('#filter').click(function() {
         tabel_view()
    });

        $("#btn_xls").on("click",function (){
          var link = '<?php echo base_url(); ?>master/contoh/export_xls/';
          var buka = window.open(link,'_blank');
          buka.focus();
        })

        $(document).on('click','#btn_filtering', function(){
          var data = $(this).data('filtering');
          if(data==1){
            $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
            $('#filtering').hide(500);

          }else{
            $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
            $('#filtering').show(500);
          }
       });

});

	function refreshLab() {
		$.post("<?php echo base_url(); ?>master/beban_kerja/refreshLab", function(data) {
			$("#laboratorium").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	}

    function tabel_view(){
        var id_lab = $("#laboratorium").val()
        var date = $("#startDate").val()
       	TabelData = $('#tabel').DataTable({
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    // { "visible": false, "targets": 3 },
                    {'orderable' : false, 'targets': 7},
                    {'orderable' : false, 'targets': 0}
                  ],
                "destroy": true,
                'dom': 'Bfrtip',
                'buttons': [
                    {extend: 'copy',title: 'Master_Beban_kerja', exportOptions: {columns: [0,1,2, 3, 4, 5, 6, 7]}},
                    {extend: 'excel',title: 'Master_Beban_kerja', exportOptions: {columns: [0,1,2, 3, 4, 5, 6, 7]}},
                    {extend: 'pdf',title: 'Master_Beban_kerja', exportOptions: {columns: [0,1,2, 3, 4, 5, 6, 7]}},
                    {extend: 'print',title: 'Master_Beban_kerja', exportOptions: {columns: [0,1,2, 3, 4, 5, 6, 7]}},
                ],
                "serverSide": false,
                "processing": false,
                "orderCellsTop": true,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'kategori' },
                  { data : 'max' },
                  { data : 'spuc' },
                  { data : 'sampel' },
                  { data : 'selesai' },
                  { data : 'belum' },
                  { data : 'overload' },
                ],
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/master/beban_kerja/get_data',
                      data : {
                        'id_lab'  : id_lab,
                        'date'  : date,
                      },
                     dataType: 'JSON',
                     dataSrc : function (json) {
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){

                            return_data.push({
                              'no'	    :  no ,
                              'kategori'	    :  json[i].NM_KATEGORI ,
                              'max'      :   json[i].BEBAN_KERJA,
                              'spuc'          :  json[i].SPUC ,
                              'sampel'	    :  json[i].SAMPEL ,
                              'selesai'      :   json[i].SELESAI,
                              'belum'          :  json[i].BELUM ,
                              'overload' : json[i].OVERLOAD
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  },
                  "createdRow": function( row, data, dataIndex){
                        if( data['overload'] >  0){
                            $(row).addClass('redClass');
                        }
                    }
		});
    // Apply the search
            // TabelData.columns().every( function () {
            //     var that = this;
            //
            //     $( 'input', this.header() ).on( 'keyup change clear', function () {
            //         if ( that.search() !== this.value ) {
            //             that
            //                 .search( this.value )
            //                 .draw();
            //         }
            //     } );
            // } );
            $(TabelData.table().container()).on('keyup', 'thead input', function () {
                console.log($(this).data('index') + "-" + this.value);
                TabelData.column($(this).data('index')).search(this.value).draw();
            });

            TabelData.on('order.dt search.dt', function () {
                TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();

    }

	function simpan() {
		var nm_contoh = $("#nm_contoh").val();
		var kelompok = $("#kelompok").val();
		if(nm_contoh == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama Contoh harus terisi.");
			return;
		}
		$.post("<?php echo base_url(); ?>master/contoh/simpan", {"nm_contoh": nm_contoh,"kelompok": kelompok
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() {
		});
	}

	function edit(baris) {
		var kolom = TabelData.row(baris).data();

		$("#id_contoh").val(kolom['id']);
		$("#nm_contoh").val(kolom['contoh']);
		$("#kelompok").val(kolom['id_kelompok']);
        $(".btn-simpan").hide();
        $(".btn-edit").show();
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");
	}

	function update() {
		var id = $("#id_contoh").val();
		var nm_contoh = $("#nm_contoh").val();
		var kelompok = $("#kelompok").val();
		if( nm_contoh == ""
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama Contoh harus terisi.");
			return;
		}
		$.post("<?php echo base_url(); ?>master/contoh/update", {"id": id, "nm_contoh": nm_contoh, "kelompok": kelompok }, function(datas) {
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() {
		});
	}
    function konfirmasi(baris) {
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}


	function hapus(id, dialog) {
		dialog.setClosable(false);
		$.post("<?php echo base_url(); ?>master/contoh/hapus", {"id": id}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() {
			dialog.setClosable(true);
		});
	}

</script>
