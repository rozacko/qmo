<style>
    thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Laboratorium</h2>
                </div>
                <div class="col-md-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><span
                                    class="fa fa-plus"></span>&nbsp;Tambah
                        </button>&nbsp;&nbsp;&nbsp;
                        <!-- <button type="button" id='btn_xls' class="btn-sm btn-warning"
                                                           id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export
                            Excel
                        </button> -->
                    </div>
                </div>
            </div>


            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example"
                               style="font-size:95%" id="tabel">
                            <thead>
                            <tr>
                                <th style='width:50px;'>No</th>
                                <th>Kode Laboratorium</th>
                                <th>Nama Laboratorium</th>
                                <th>Company</th>
                                <th>Plant</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style='width:120px;'>
                                    <center>Aksi <span class="fa fa-filter pull-right" data-filtering="1"
                                                       id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                    </center>
                                </th>
                            </tr>
                            <tr id="filtering">
                                <th style='width:50px;'></th>
                                <td>Kode Laboratorium</td>
                                <td>Nama Laboratorium</td>
                                <td>Company</td>
                                <td>Plant</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <th style='widtd:120px;'></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header' id='dlg_header'>
                <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i>
                </button>
                <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
            </div>
            <div class='modal-body'>
                <div class='form-group'>
                    <label>Kode Laboratorium:</label>
                    <input type='text' class='form-control input-xs' id='kd_lab'>
                </div>
                <div class='form-group'>
                    <label>Nama Laboratorium:</label>
                    <input type='hidden' class='form-control input-xs' id='id_lab'>
                    <input type='text' class='form-control input-xs' id='nm_lab'>
                </div>
                <div class='form-group'>
                    <label>Company:</label>
                    <select class='form-control selectpicker' data-live-search='false' id='company'>
                        <option value='' data-hidden='true' selected='selected'>-- Pilih Lokasi --</option>
                    </select>
                </div>
                <div class='form-group'>
                    <label>Plant:</label>
                    <select class='form-control selectpicker' data-live-search='false' id='lokasi'>
                        <option value='' data-hidden='true' selected='selected'>-- Pilih Lokasi --</option>
                    </select>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i
                            class='fa fa-save'></i>&nbsp;Simpan
                </button>
                <button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i
                            class='fa fa-save'></i>&nbsp;Update
                </button>
                <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i
                            class='fa fa-times'></i>&nbsp;Tutup
                </button>
            </div>
        </div>
    </div>
</div>


<script>
    var TabelData;
    $(document).ready(function () {
        $('#tabel thead td').each(function (i) {
            var title = $(this).text();
            $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });
        tabel_view()
        refreshCompany()


        // refreshLokasi()
        /// Button Action
        $('#tambah_data').click(function () {
            // refreshCompany()
            $("#judul_input").html('Form Tambah');
            $(".btn-simpan").show();
            $(".btn-edit").hide();
            $(".id_primary").hide();
            $("#kd_lab, #id_lab, #company,  #nm_lab, #lokasi").val('');
            $("#dlg").modal("show");
            $(".selectpicker").selectpicker("refresh");
        });
        $('#company').change(function () {
            refreshLokasi($("#company option:selected").attr('id_company'))
        });

        $("#btn_xls").on("click", function () {
            var link = '<?php echo base_url(); ?>master/laboratorium/export_xls/';
            var buka = window.open(link, '_blank');
            buka.focus();
        });

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });
    });


    function refreshLokasi(id_company, plant) {
        $.post("<?php echo base_url(); ?>master/laboratorium/refreshLokasi", {
            "id_company": id_company
        }, function (data) {
            $("#lokasi").html(data);
            $('#lokasi').val(plant);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function refreshCompany() {
        $.post("<?php echo base_url(); ?>master/laboratorium/refreshCompany", function (data) {
            $("#company").html(data);
        }).fail(function () {
            // Nope
        }).always(function () {
            $(".selectpicker").selectpicker("refresh");
        });
    }

    function tabel_view() {
        TabelData = $('#tabel').DataTable({
            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "columnDefs": [
                {"visible": false, "targets": 5},
                {"visible": false, "targets": 6},
                {"visible": false, "targets": 7},
                {"orderable": false, "targets": 8},
            ],
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Laboratorium', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'excel',title: 'Master_Laboratorium', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'pdf',title: 'Master_Laboratorium', exportOptions: {columns: [0,1,2,3,4]}},
                {extend: 'print',title: 'Master_Laboratorium', exportOptions: {columns: [0,1,2,3,4]}},
            ],
            "destroy": true,
            "serverSide": false,
            "processing": false,
            "paging": true,
            "orderCellsTop": true,
            "lengthChange": true,
            // "sDom": 'Rfrtlip',
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            // "scrollY": '50vh',
            "scrollCollapse": true,
            columns: [
                {data: 'no'},
                {data: 'kd_lab'},
                {data: 'nama'},
                {data: 'company'},
                {data: 'plant'},
                {data: 'id_lab'},
                {data: 'kd_company'},
                {data: 'id_plant'},
                {data: 'aksi'},
            ],
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>master/laboratorium/get_data',
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {

                        if (write_menu == '') {
                            var aksi = '- no access -';
                        } else {
                            var aksi = '<center><button onclick="edit(\'' + i + '\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi(\'' + i + '\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>'
                        }
                        return_data.push({
                            'no': no,
                            'kd_lab': json[i].KD_LAB,
                            'nama': json[i].NM_LAB,
                            'company': json[i].NM_COMPANY,
                            'plant': json[i].NM_PLANT,
                            'id_lab': json[i].ID_LAB,
                            'kd_company': json[i].ID_COMPANY,
                            'id_plant': json[i].ID_PLANT,
                            'aksi': aksi
                        })
                        no += 1;
                    }
                    return return_data;
                }
            }
        });  // Apply the search
        $(TabelData.table().container()).on('keyup', 'thead input', function () {
            TabelData.column($(this).data('index')).search(this.value).draw();
        });
    }

    function edit(baris) {
        var kolom = TabelData.row(baris).data();

        $("#kd_lab").val(kolom['kd_lab']);
        $("#id_lab").val(kolom['id_lab']);
        $("#nm_lab").val(kolom['nama']);
        $("#company").val(kolom['kd_company']);
        refreshLokasi($("#company option:selected").attr('id_company'), kolom['id_plant'])
        $(".btn-simpan").hide();
        $(".btn-edit").show();
        $("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Form Edit</b>");
        $("#dlg").modal("show");
        $(".selectpicker").selectpicker("refresh");
    }

    function simpan() {
        var kd_lab = $("#kd_lab").val();
        var nm_lab = $("#nm_lab").val();
        var company = $("#company").val();
        var lokasi = $("#lokasi").val();
        if (nm_lab == "" || company == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama dan Company harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/laboratorium/simpan", {
            "nm_lab": nm_lab, "lokasi": lokasi, "kd_lab": kd_lab, "company": company
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
        }).always(function () {
        });
    }


    function update() {
        var id = $("#id_lab").val();
        var kd_lab = $("#kd_lab").val();
        var nm_lab = $("#nm_lab").val();
        var company = $("#company").val();
        var lokasi = $("#lokasi").val();
        if (nm_lab == "" || company == "") {
            informasi(BootstrapDialog.TYPE_WARNING, "Nama dan Company harus terisi.");
            return;
        }
        $.post("<?php echo base_url(); ?>master/laboratorium/update", {
            "id": id,
            "nm_lab": nm_lab,
            "lokasi": lokasi,
            "kd_lab": kd_lab,
            "company": company
        }, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                $("#dlg").modal("hide");
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
        }).always(function () {
        });
    }

    function konfirmasi(baris) {
        var kolom = TabelData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Laboratorium</b>",
            "message": "Anda yakin ingin menghapus Laboratorium \"" + kolom['nm_lab'] + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Hapus",
                "action": function (dialog) {
                    hapus(kolom['id_lab'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }


    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>master/laboratorium/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
                tabel_view()
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }

</script>
