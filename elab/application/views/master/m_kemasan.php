<style type="text/css">
    div.dataTables_length {
        margin-right: 0.5em;
        margin-top: 0.2em;
    }

</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="">
                <div class="col-md-6">
                    <h2><i class="fa fa-list"></i> Master Kemasan</h2>
                </div>
                <div class="col-md-6">
                    <div style="text-align:right;">
                        <button type="button" class="btn-sm btn-success  btn_tambah" onclick="add()">
                          <span class="fa fa-plus"></span>&nbsp;Tambah
                        </button>&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-striped table-bordered table-hover dataTables-example"
                               style="font-size:95%" width="100%">
                            <thead>
                            <tr>
                                <th width="30px">No</th>
                                <th>Kode Kemasan</th>
                                <th>Nama Kemasan</th>
                                <th>
                                    <center>Aksi <span class="fa fa-filter pull-right" data-filtering="1"
                                                       id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span>
                                    </center>
                                </th>
                            </tr>
                            <tr id="filtering">
                                <th></th>
                                <td>Kode Kemasan</td>
                                <td>Nama Kemasan</td>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var tableData;

    $(document).ready(function () {

        tableData = $('#dataTable').DataTable({
            "processing": true,
            "serverSide": false,
            "lengthMenu": [10, 25, 50, 100],
            "pageLength": 10,
            // "sDom": 'Rfrtlip',
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy',title: 'Master_Kemasan', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel',title: 'Master_Kemasan', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf',title: 'Master_Kemasan', exportOptions: {columns: [0,1,2]}},
                {extend: 'print',title: 'Master_Kemasan', exportOptions: {columns: [0,1,2]}},
            ],
            "info": true,
            "searching": true,
            "ordering": true,
            "columnDefs": [
                {'orderable': false, 'targets': 3},
                {'orderable': false, 'targets': 0},
            ],
            "orderCellsTop": true,
            "ajax": {
                "url": "<?php echo site_url('master/c_m_kemasan/ajax_list')?>",
                "type": "POST"
            },
            columns: [
                {data: 'NO', name: 'NO'},
                {data: 'KODE_KEMASAN', name: 'KODE_KEMASAN'},
                {data: 'NAMA_KEMASAN', name: 'NAMA_KEMASAN'},
                {
                    data: 'ID_KEMASAN', render: function (d, t, f, meta) {
                        return "<center><button class='btn btn-primary btn-xs waves-effect' onclick='edit(" + d + ")'><span class='fa fa-pencil'></span></button>&nbsp;<button class='btn btn-danger btn-xs waves-effect' onclick='konfirmasi(" + meta.row + ")'><span class='fa fa-trash-o'></span></button></center>";
                    }
                },
            ]

        });
        $('#dataTable thead td').each(function (i) {
            var title = $(this).text();
            $(this).html('<input type="text" style="width : 100%;" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
        });
        $(tableData.table().container()).on('keyup', 'thead input', function () {
            tableData.column($(this).data('index')).search(this.value).draw();
        });


        tableData.on('order.dt search.dt', function () {
            tableData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $(document).on('click', '#btn_filtering', function () {
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });
    });

    function add() {
        $('#myModal').modal('show');
    }

    function edit(id) {


        $.ajax({
            url: '<?php echo site_url('master/c_m_kemasan/edit/') ?>/' + id,
            type: 'GET',
            dataType: 'JSON',
            success: function (data) {
                $('[name="ID_KEMASAN"]').val(data.ID_KEMASAN);
                $('[name="KODE_KEMASAN"]').val(data.KODE_KEMASAN);

                $('[name="NAMA_KEMASAN"]').val(data.NAMA_KEMASAN);


                $('#myEdit').modal('show');
            }
        });


    }

    function konfirmasi(baris, nama) {
        var kolom = tableData.row(baris).data();
        console.log(kolom)
        BootstrapDialog.show({
            "type": BootstrapDialog.TYPE_DANGER,
            "title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
            "message": "Anda yakin ingin menghapus Item Uji \"" + kolom.NAMA_KEMASAN + "\"?",
            "closeByBackdrop": false,
            "closeByKeyboard": false,
            "buttons": [{
                "cssClass": "btn btn-danger btn-xs btn-hapus",
                "icon": "fa fa-trash",
                "label": "Hapus",
                "action": function (dialog) {
                    hapus(kolom['ID_KEMASAN'], dialog);
                }
            }, {
                "cssClass": "btn btn-default btn-xs btn-tutup",
                "icon": "fa fa-times",
                "label": "Tutup",
                "action": function (dialog) {
                    dialog.close();
                }
            }]
        });
    }

    function hapdus(id) {
        if (confirm("Apakah yakin ingin menghapus?")) {
            $.ajax({
                url: '<?php echo base_url() ?>master/c_m_kemasan/hapus',
                type: 'post',
                data: {id: id},
                success: function () {
                    alert('Berhasil Menghapus');
                    location.reload();
                },
                error: function () {
                    alert('Gagal');
                }
            });
        }
    }

    function hapus(id, dialog) {
        dialog.setClosable(false);
        $.post("<?php echo base_url(); ?>master/c_m_kemasan/hapus", {"id": id}, function (datas) {
            var data = JSON.parse(datas);
            if (data.notif == "1") {
                dialog.close();
                show_toaster(1, 'Success', 'Berhasil Delete Data')
                tableData.ajax.reload();
            } else {
                informasi(BootstrapDialog.TYPE_DANGER, data.message);
            }
        }).fail(function () {
            informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
        }).always(function () {
            dialog.setClosable(true);
        });
    }
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah Data</h4>
            </div>
            <div class="modal-body">
                <!-- <?php echo base_url() ?>master/c_m_kemasan/tambah -->
                <form action="" method="POST" name="add_kemasan" id="add_kemasan">

                    <div class="form-group">
                        <label>Kode Kemasan</label>
                        <input type="number" name="KODE_KEMASAN" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label>Nama Kemasan</label>
                        <input type="text" name="NAMA_KEMASAN" class="form-control" required="">
                    </div>
                    <input type="submit" name="tambah" id="tambah" class="btn btn-primary pull-right"
                           value="Simpan"><br>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>

<!-- Modal Edit-->
<div id="myEdit" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">FORM EDIT</h4>
            </div>
            <div class="modal-body">
                <!-- <?php echo base_url() ?>master/c_m_kemasan/update -->
                <form action="" method="POST" name="edit_kemasan" id="edit_kemasan">
                    <div class="form-group">

                        <input type="hidden" name="ID_KEMASAN" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Kode Kemasan</label>
                        <input type="number" name="KODE_KEMASAN" class="form-control" required="">
                    </div>

                    <div class="form-group">
                        <label>Nama Kemasan</label>
                        <input type="text" name="NAMA_KEMASAN" class="form-control" required="">
                    </div>
                    <input type="submit" name="update" id="update_btn" class="btn btn-primary pull-right"
                           value="Update"><br>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#add_kemasan').submit(function (e) {
            e.preventDefault();
            // alert('a');

            var formData = $('#add_kemasan').serializeArray();
            $.ajax({
                'url': '<?php echo base_url() ?>master/c_m_kemasan/tambah',
                "type": 'POST',
                "data": formData,
                "beforeSend": function () {
                    $('#tambah').attr('disabled', true);
                }
            })
                .done(function (data) {
                    var data = JSON.parse(data);
                    show_toaster(data.notif, "Success", data.message);
                    $('#myModal').modal('hide');
                    $('#add_kemasan')[0].reset();
                })
                .fail(function (data) {
                    // console.log(data.message);
                    var dt = JSON.parse(data);
                    show_toaster(dt.notif, "Failed", dt.message);
                })
                .always(function () {
                    tableData.ajax.reload();
                    $('#tambah').attr('disabled', false);
                });

        });

        $('#edit_kemasan').submit(function (e) {
            e.preventDefault();
            // alert('a');

            var formData = $('#edit_kemasan').serializeArray();
            $.ajax({
                'url': '<?php echo base_url() ?>master/c_m_kemasan/update',
                "type": 'POST',
                "data": formData,
            })
                .done(function (data) {
                    var data = JSON.parse(data);
                    show_toaster(data.notif, "Success", data.message);
                    $('#myEdit').modal('hide');
                    $('#edit_kemasan')[0].reset();
                })
                .fail(function (data) {
                    // console.log(data.message);
                    var dt = JSON.parse(data);
                    show_toaster(dt.notif, "Failed", dt.message);
                })
                .always(function () {
                    tableData.ajax.reload();
                });

        })
    })
</script>
