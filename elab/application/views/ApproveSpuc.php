
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
    .form-group {
        margin-bottom : 0px;
    }
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> KUPP Disetujui</h2>   
            
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:95%" id="tabel">
                        <thead>
                            <tr>
                                <th  style='width:10px;'>No</th>
                                <th   style='width:20px;'>Detail SPUC  </th>
                                <th  style='width:20px;' >Detail KUPP    </th> 
                                <th  >NO SPUC  </th> 
                                <th  >Tanggal Terima  </th> 
                                <th  >Nama Contoh   </th>   
                                <th  >   </th>  
                                <th  >   </th>  
                                <th  >   </th>  
                                <th  >   </th>  
                                <th  >   </th>  
                            </tr> 
                            
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
                <div class="row">
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>SPUC:</label> 
                                <p id='no_spuc'></p>
                            </div> 
                    </div>
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>Tanggal Penerimaan Contoh :</label> 
                                <p id='tgl_terima'></p>
                            </div> 
                    </div>
                </div> <hr>    
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">1. Peralatan & bahan pengujian dapat digunakan</p> 
                    <div class="col-lg-6">  
                                <p style='font-weight:bold'  id='peralatan'></p>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">2. Personil dapat Melakukan Pengujian</p> 
                    <div class="col-lg-6"> 
                                <p style='font-weight:bold'  id='personil'></p>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">3. Metode Pengujian</p> 
                    <div class="col-lg-6"> 
                                <p style='font-weight:bold'  id='metode'></p>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">4. Waktu Penyelesaian Pengujian Sesuai Jadwal</p> 
                    <div class="col-lg-6"> 
                                <p style='font-weight:bold' id='waktu'></p>
                    </div>
                </div>   
                 <hr>
                  <center><p>Berdasarkan kaji ulang permintaan pengujian diatas, contoh uji ini Dapat dikerjakan  </p> </center>
                  <center><p>Dengan Syarat  </p> </center>
                  <center> <p style='font-weight:bold'  id="keterangan"></p>
                  </center>  
			</div> 
			<div class='modal-footer'> 
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){  
    $('#tabel thead td').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
        tabel_view() 
        
});        
 
    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
        'dom': 'Bfrtip',
                  'buttons': [
                      {extend: 'copy',title: 'KUPP_disetujui', exportOptions: {columns: [3,4,5]}},
                      {extend: 'excel',title: 'KUPP_disetujui', exportOptions: {columns: [3,4,5]}},
                      {extend: 'pdf',title: 'KUPP_disetujui', exportOptions: {columns: [3,4,5]}},
                      {extend: 'print',title: 'KUPP_disetujui', exportOptions: {columns: [3,4,5]}},

                  ],
                    "columnDefs": [
                    { "visible": false, "targets": 6 }, 
                    { "visible": false, "targets": 7 }, 
                    { "visible": false, "targets": 8 }, 
                    { "visible": false, "targets": 9 }, 
                    { "visible": false, "targets": 10 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'spuc' },
                  { data : 'kupp' },
                  { data : 'no_spuc' }, 
                  { data : 'tgl_terima' }, 
                  { data : 'contoh' },  
                  { data : 'peralatan' },  
                  { data : 'personil' },  
                  { data : 'metode' },  
                  { data : 'waktu' },  
                  { data : 'keterangan' },   
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approvespuc/get_data',  
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){   
                            return_data.push({
                              'no'	    : no ,
                              'spuc'	    :   '<center><button onclick="detail_spuc(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-warning btn-xs waves-effect btn_edit" title="Detail KKPP"><span class="btn-labelx"><i class="fa fa-check"></i></span> </button> </center>', 
                              'kupp'	    :   '<center><button onclick="edit(\''+ i +'\')" class="btn btn-success btn-xs waves-effect btn_edit" title="Detail KKPP"><span class="btn-labelx"><i class="fa fa-check"></i></span> </button> </center>',
                              'no_spuc'      :  json[i].NO_BAPPUC,   
                              'tgl_terima'      :   json[i].TANGGAL,  
                              'contoh'          :  json[i].NAMA_CONTOH ,  
                              'peralatan'          :  json[i].PERALATAN ,  
                              'personil'          :  json[i].PERSONIL ,  
                              'metode'          :  json[i].METODE ,  
                              'waktu'          :  json[i].WAKTU ,  
                              'keterangan'          :  json[i].KETERANGAN ,  
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search
            TabelData.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        
    } 
     
     
	function edit(baris) { 
		var kolom = TabelData.row(baris).data();  
		$("#no_spuc").html(kolom['no_spuc']);
        
		$("#tgl_terima").html(kolom['tgl_terima']);
		$("#peralatan").html(kolom['peralatan']=="Y" ? "YA" : "TIDAK");
		$("#personil").html(kolom['personil']=="Y" ? "YA" : "TIDAK");
		$("#metode").html(kolom['metode']=="Y" ? "YA" : "TIDAK");
		$("#waktu").html(kolom['waktu']=="Y" ? "YA" : "TIDAK"); 
		$("#keterangan").html('" '+(kolom['keterangan']==null ? " ":  kolom['keterangan'] )+' "'); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;KAJI ULANG PERMINTAAN PENGUJIAN</b>"); 
		$("#dlg").modal("show"); 
	}
    

</script>