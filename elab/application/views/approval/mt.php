
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
    .form-group {
        margin-bottom : 0px;
    }
    div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }
  
    button.link { background:none;border:none; }
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
          <div class=""> 
            <div class="col-lg-6">
              <h2><i class="fa fa-list"></i> Approval MT</h2>  
            </div>
            <div class="col-lg-6">
              <div style="text-align:right;display:none">
                    <button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel</button> 
              </div>
            </div>
            
          </div>
			      
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:85%" id="tabel" width="100%">
                        <thead>
                            <tr>
                                <th  style='width:20px;'>No</th>
                                <th>NO SPUC  </th> 
                                <th>Tanggal Terima  </th> 
                                <th>Nama Contoh   </th> 
                                <th> </th> 
                                <th> </th> 
                                <th><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                            </tr>
                            <tr id="filtering">
                                <th  style='width:20px;'></th>
                                <td>NO SPUC  </td> 
                                <td>Tanggal Terima  </td> 
                                <td>Nama Contoh   </td> 
                                <td> </td> 
                                <td> </td> 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
                <div class="row">
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>SPUC:</label>
                                <input type='hidden' class='form-control input-sm' id='id_transaksi' >
                                <input type='text' class='form-control input-sm' id='spuc' readonly style="">
                            </div> 
                    </div>
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>Tanggal Penerimaan Contoh:</label> 
                                <input type='text' class='form-control input-sm' id='tgl_terima' >
                            </div> 
                    </div>
                </div> <hr>    
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">1. Peralatan & bahan pengujian dapat digunakan</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="peralatan"  id="peralatan" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="peralatan" id="peralatan" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">2. Personil dapat Melakukan Pengujian</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="personil"  id="personil" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="personil"  id="personil" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">3. Metode Pengujian</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="metode" id="metode" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="metode" id="metode" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">4. Waktu Penyelesaian Pengujian Sesuai Jadwal</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="waktu" id="waktu" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="waktu"  id="waktu" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                <div class="col-lg-2"> </div>
                    <p class="col-lg-4 col-form-label">5. Transaksi Prioritas (CITO)</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="cito"  id="cito" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="cito"   id="cito" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                 <hr>
                  <center><p>Berdasarkan kaji ulang permintaan pengujian diatas, contoh uji ini</p> </center>
                  <center> <label class="check-label"><i class=" "></i><input type="radio" name="approve"  id="approve" value="Y" disabled>&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="approve" id="tolak" value="N" disabled>&nbsp;Tidak</label>
                  </center> 
                  <p>Dengan Syarat :</p>
                   <div class="row">  <div class="col-lg-12"> 
                        <textarea name="syarat" id='syarat' style="width:100%"></textarea>
                    </div>
                    </div>
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-check'></i>&nbsp;Setuju</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){  
   
        tabel_view()
        refreshKelompok() 
        $('#tabel thead td').each( function (i) {
          var title = $('#tabel thead th').eq( $(this).index() ).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+(i+1)+'" />' );
      });

      $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
        TabelData.column( $(this).data('index') ).search( this.value ).draw();
      });

       /// Button Action
	$('#tambah_data').click(function() { 
        
        refreshKelompok() 
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    });
    
        $("#btn_xls").on("click",function (){
          var link = '<?php echo base_url(); ?>master/contoh/export_xls/';
          var buka = window.open(link,'_blank');
          buka.focus();
        })
        
        $('#peralatan, #personil, #metode, #waktu').change(
            function(){ 
                if ($("#peralatan:checked").val() == 'Y' && $("#personil:checked").val() == 'Y' && $("#metode:checked").val() == 'Y' && $("#waktu:checked").val() == 'Y' ) { 
                    $("#approve").prop('checked',true); 
                     $("#tolak").prop('checked',false); 
                }
                else{
                    $("#tolak").prop('checked',true); 
                    //$('#input_update').attr("disabled", "disabled");
                     $("#approve").prop('checked',false); 
                }

            });

        $(document).on('click','#btn_filtering', function(){
            var data = $(this).data('filtering');
            if(data==1){
              $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
              $('#filtering').hide(500);

            }else{
              $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
              $('#filtering').show(500);
            }
         });
            
});        

	function refreshKelompok() {
		$.post("<?php echo base_url(); ?>master/contoh/refreshKelompok", function(data) {
			$("#kelompok").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	} 

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 4 }, 
                    { "visible": false, "targets": 5 }, 
                    { "orderable": false, "targets": 6 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "orderCellsTop":true,
                "sDom": 'Rfrtlip',
                "paging": true,
                "lengthMenu": [10, 25, 50, 100],
                "lengthChange": true,
                "searching": true,
                "info":true,
                "ordering": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'no_spuc' }, 
                  { data : 'tgl_terima' }, 
                  { data : 'contoh' }, 
                  { data : 'id' }, 
                  { data : 'bappuc' }, 
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/mt/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){ 
                            return_data.push({
                              'no'	      : no ,
                              'no_spuc'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\')">'+json[i].NO_BAPPUC+'</a>', 
                              'tgl_terima':  json[i].TANGGAL,  
                              'contoh'    :  json[i].NAMA_CONTOH , 
                              'id'        :  json[i].ID_TRANSAKSI,   
                              'bappuc'        :  json[i].NO_BAPPUC,   
                              'aksi' : '<center><button onclick="edit(\''+ i +'\')" class="btn btn-success btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-check"></i></span> Kaji</button> </center>'
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search
            TabelData.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        
    } 
     
	function edit(baris) { 
        $('#input_update').html('<i class="fa fa-check"></i>&nbsp;Setuju'); 
				$('#input_update').removeAttr("disabled");
		var kolom = TabelData.row(baris).data(); 
        
		$("#id_transaksi").val(kolom['id']);
		$("#spuc").val(kolom['bappuc']);
		$("#tgl_terima").val(kolom['tgl_terima']); 
		
        $(':radio').prop('checked',false)
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Kaji Kesesuaian  Permintaan Pengujian</b>"); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
        
            
		var id = $("#id_transaksi").val(); 
		var spuc = $("#spuc").val(); 
        // var peralatan = '';var personil = '';var metode = '';var waktu = ''; 
		var peralatan = $("input[name='peralatan']:checked").val(); 
		var personil = $("input[name='personil']:checked").val(); 
		var metode = $("input[name='metode']:checked").val(); 
		var waktu = $("input[name='waktu']:checked").val(); 
		var cito = $("input[name='cito']:checked").val();  
		var approve = $("input[name='approve']:checked").val(); 
		var syarat = $("#syarat").val();  
		// if( $("#peralatan").prop("checked") == false || $("#personil").prop("checked") == false || $("#metode").prop("checked") == false || $("#waktu").prop("checked") == false
		// ) {
			// informasi(BootstrapDialog.TYPE_WARNING, "Mohon Lengkapi Kaji Ulang.");
			// return;
		// } 
		if( peralatan  == null  || personil  == null  || metode  == null  || waktu  == null  
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Mohon Lengkapi Kaji Ulang.");
			return;
		}if($("#tolak:checked").val() == 'N' && $("#syarat").val() == '') { 
                   informasi(BootstrapDialog.TYPE_WARNING, "Mohon Keterangan Diisi.");
             return;
                    
                }else{
                
           $('#input_update').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Proses'); 
			$('#input_update').attr("disabled", "disabled");        
		$.post("<?php echo base_url(); ?>approval/mt/update", {"spuc": spuc, "id": id, "peralatan": peralatan, "personil": personil, "metode": metode, "waktu": waktu, "cito": cito, "approve": approve, "syarat": syarat }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
                 
				$('#input_update').removeAttr("disabled");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
      }
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/contoh/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>