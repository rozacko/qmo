<?php
	$ukuran = array("K"=>1, "M"=>2, "G"=>3);
	$maksimal["upload"]["size"] = ini_get("upload_max_filesize");
	$maksimal["upload"]["number"] = floatval(preg_replace("/[^0-9]/", "", $maksimal["upload"]["size"]));
	$maksimal["upload"]["alphabet"] = strtoupper(preg_replace("/[^a-zA-Z]/", "", $maksimal["upload"]["size"]));
	for($i = 0; $i < $ukuran[$maksimal["upload"]["alphabet"]]; $i++) {
		$maksimal["upload"]["number"] *= 1024;
	}
	$maksimal["upload"]["number"] -= 512;
	$maksimal["post"]["size"] = ini_get("post_max_size");
	$maksimal["post"]["number"] = floatval(preg_replace("/[^0-9]/", "", $maksimal["post"]["size"]));
	$maksimal["post"]["alphabet"] = strtoupper(preg_replace("/[^a-zA-Z]/", "", $maksimal["post"]["size"]));
	for($i = 0; $i < $ukuran[$maksimal["post"]["alphabet"]]; $i++) {
		$maksimal["post"]["number"] *= 1024;
	}
	$maksimal["post"]["number"] -= 512;
	$dipakai = $maksimal["upload"]["number"] > $maksimal["post"]["number"] ? "post" : "upload";
?>
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
    .form-group {
        margin-bottom : 0px;
    }
    div.div-akses {
    width: 100%;
    padding: 15px 15px 0px 15px;
    color: #555;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}
div.dataTables_length {
  margin-right: 0.5em;
  margin-top: 0.2em;
}

label.check-label, label.check-label input {
    font-weight: normal;
    cursor: pointer;
}
label.check-label:hover {
    font-weight: bold;
}
 </style>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Approval Analis</h2>  
            <!-- <div style="text-align:left" >
                <button type="button" class="btn-sm btn-success btn_tambah" id="tambah_data"><i class="fa fa-plus">&nbsp;Tambah</i></button>&nbsp;&nbsp;&nbsp;<button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><i class="fa fa-file-excel-o">&nbsp;Export Excel</i></button> 
            </div>
            -->
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:85%" id="tabel" width="100%">
                        <thead>
                            <tr>
                                <th  style='width:3px;'>No</th> 
                                <th>NO SPUC  </th> 
                                <th>CITO </th> 
                                <th>Tanggal Terima  </th> 
                                <th>Nama Contoh   </th> 
                                <th>Tanggal Uji Awal    </th> 
                                <th>Tanggal Uji Akhir    </th> 
                                <th>Data Pengujian    </th>  
                                <th>Komentar Penyelia    </th>   
                                <th> </th> 
                                <th> </th> 
                                <th> </th> 
                                <th> </th> 
                                <th  style='width:120px;'><center>Aksi <span class="fa fa-filter pull-right" data-filtering="1" id="btn_filtering" > <i class="fa fa-angle-double-up"></i> </span></center></th>
                            </tr>
                            <tr id="filtering">
                                <th  style='width:3px;'></th> 
                                <td>NO SPUC  </td> 
                                <td>CITO  </td> 
                                <td>Terima  </td> 
                                <td>Contoh   </td> 
                                <td>Awal   </td> 
                                <td>Akhir   </td> 
                                <th> </th> 
                                <td>Komentar Penyelia   </td> 
                                <td> </td> 
                                <td> </td> 
                                <td> </td> 
                                <td> </td> 
                                <th  style='widtd:120px;'></th>
                            </tr>
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog modal-lg'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>  
                <div class="row">
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>SPUC:</label>
                                <input type='hidden' class='form-control input-sm' id='id_transaksi' >
                                <input type='text' class='form-control input-sm' id='spuc' readonly style="">
                            </div> 
                    </div>
                    <div class="col-lg-6"> 
                            <div class='form-group'>
                                <label>Tanggal Penerimaan Contoh:</label> 
                                <input type='text' class='form-control input-sm' id='tgl_terima' >
                            </div> 
                    </div>
                </div> <hr>    
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">1. Peralatan & bahan pengujian dapat digunakan</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="peralatan"  id="peralatan" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="peralatan" id="peralatan" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">2. Personil dapat Melakukan Pengujian</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="personil"  id="personil" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="personil"  id="personil" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">3. Metode Pengujian</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="metode" id="metode" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="metode" id="metode" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                    <p class="col-lg-6 col-form-label">4. Waktu Penyelesaian Pengujian Sesuai Jadwal</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="waktu" id="waktu" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="waktu"  id="waktu" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                <div class="form-group row">
                <div class="col-lg-2"> </div>
                    <p class="col-lg-4 col-form-label">5. Transaksi Prioritas (CITO)</p> 
                    <div class="col-lg-6"> 
                        <label class="check-label"><i class=" "></i><input type="radio" name="cito"  id="cito" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="cito"   id="cito" value="N">&nbsp;Tidak</label>
                    </div>
                </div>  
                 <hr>
                  <center><p>Berdasarkan kaji ulang permintaan pengujian diatas, contoh uji ini</p> </center>
                  <center> <label class="check-label"><i class=" "></i><input type="radio" name="approve"  id="approve" value="Y">&nbsp;Ya</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="approve" id="approve" value="N">&nbsp;Tidak</label>
                  </center> 
                  <p>Dengan Syarat :</p>
                   <div class="row">  <div class="col-lg-12"> 
                        <textarea name="syarat" id='syarat' style="width:100%">
                     
                        </textarea>
                    </div>
                    </div>
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-check'></i>&nbsp;Setuju</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<div class='modal fade' id='dlg_upload' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header modal-success'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title'><b><i class="fa fa-file-excel"></i> Upload Excel</b></div>
			</div>
			<form method="post" action="<?php echo base_url(); ?>approval/analis/UploadTemplate" id="form_upload">
				<div class='modal-body'>
					<div class='form-group'>
						<div style='font-size:16px;margin-bottom:8px;'>File harus Excel 2007 (xlsx)</div>
					</div>
                    <input type='hidden' name='id_transaksi_upload' id='id_transaksi_upload'>
                    <input type='hidden' name='kategori_upload' id='kategori_upload'>
                    
					<div class='form-group'>
						<div class='input-group' style='width:100%;'>
							<span class='input-group-btn'>
								<span class='btn btn-success btn-file' id='tbl_file'>
									<i class='fa fa-file-excel-o'></i><input type='file' name='file_input' id='file_input' onchange='cek(this);'>
								</span>
							</span>
							<input type='text' class='form-control input-xs' style='padding:3px 8px 3px 8px;font-family:Menlo,Monaco,Consolas,"Courier New",monospace;font-weight:bold;' id='nama_file_input' value='Pilih file...' readonly='readonly'>
						</div>
						<div class='progress' style='width:100%;height:16px;margin:6px 0px;position:relative;'>
							<div class='progress-bar progress-bar-striped progress-bar-info active' id='proses_upload' style='position:absolute;top:0px;left:0px;width:0%;'></div>
							<div id='persen_upload' style='position:absolute;top:0px;left:0px;width:100%;text-align:center;font-weight:bold;'>0%</div>
						</div>
						<div style='width:100%;text-align:right;margin:8px 0px;font-weight:bold;'>[&nbsp;<span id='upload_size'>0&nbsp;Bytes</span>&nbsp;/&nbsp;<span id='total_size'>0&nbsp;Bytes</span>&nbsp;]</div>
					</div>

                    <div class="form-group">
                        <label>Upload KE </label>
                        <select class="form-control" name="countupload" id="countupload">
                            <option>1</option>
                            <option>2</option> 
                        </select>
                    </div>
				</div>
				<div class='modal-footer'>
					<button type="submit" class="btn btn-success btn-xs btn-simpan"><i class="fa fa-upload"></i> Upload</button>
					<button type='button' class='btn btn-default btn-tutup btn-xs' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class='modal fade' id='dlg_detail_upload' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header modal-success'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title'><b><i class="fa fa-file-excel"></i> Data Pengujian</b></div>
			</div> 
				<div class='modal-body'>
                        <div class="row">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:85%" id="tabel_detail" width="100%">
                                <thead>
                                    <tr>
                                        <th  style='width:3px;'>No</th> 
                                        <th>Parameter  </th> 
                                        <th>Sampel </th> 
                                        <th>Nilai </th>  
                                    </tr> 
                                    <tr>
                                        <th  style='width:3px;'>No</th> 
                                        <td>Parameter  </td> 
                                        <td>Sampel </td> 
                                        <td>Nilai </td>  
                                    </tr> 
                                </thead>
                                <tbody> 			
                                </tbody>
                            </table>
                        </div>
                        </div>
				</div>
				<div class='modal-footer'> 
					<button type='button' class='btn btn-default btn-tutup btn-xs' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
    var TabelData;
    var TabelDatas;
$(document).ready(function(){  


        $("#form_upload").ajaxForm({
			"beforeSend": function(jqXHR) {
				if($("#file_input").val() == "") {
					informasi(BootstrapDialog.TYPE_WARNING, "Pilih file terlebih dahulu.");
					jqXHR.abort();
				} else {
					$("#proses_upload").width("0%");
					$("#persen_upload").html("0%");
					$("#upload_size").html("0&nbsp;Bytes");
					$("#btn_upload").attr("disabled", "disabled");
					$("#btn_upload").html("<i class='fa fa-spinner fa-spin'></i>");
					$("#file_input").attr("disabled", "disabled");
					$("#tbl_file").attr("disabled", "disabled");
					$(".btn-simpan").attr("disabled", "disabled");
					$(".btn-tutup").attr("disabled", "disabled");
				}
			},
			"uploadProgress": function(event, uploaded, total, percent) {
				total -= temp_size;
				uploaded -= uploaded >= total ? total : 0;
				var terupload = uploaded >= 1024 ? (uploaded / 1024) : uploaded + "&nbsp;Bytes";
				terupload = isNaN(terupload) ? terupload : (terupload >= 1024 ? (terupload / 1024).toFixed(2) + "&nbsp;MB" : terupload.toFixed(2) + "&nbsp;KB");
				$("#upload_size").html(terupload);
				$("#proses_upload").width(percent + "%");
				$("#persen_upload").html(percent + "%");
			},
			"success": function(result) {
                var data = JSON.parse(result);
				if(data.notif == "1") {
                    tabel_view()
					$("#file_input").val("");
					$("#id_transaksi_upload").val("");
					$("#nama_file_input").val("Pilih file...");
					$("#proses_upload").width("0%");
					$("#persen_upload").html("0%");
					$("#upload_size").html("0&nbsp;Bytes");
					$("#total_size").html("0&nbsp;Bytes");
					// TabelData.draw();
					$("#dlg_upload").modal("hide");
                    informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				} else {
					informasi(BootstrapDialog.TYPE_DANGER, data.message);
				}
			},
			"error": function() {
				informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
			},
			"complete": function() {
				$("#file_input").removeAttr("disabled"); 
				$("#tbl_file").removeAttr("disabled");
				$(".btn-simpan").removeAttr("disabled");
				$(".btn-tutup").removeAttr("disabled");
			}
		});
        
     $('#tabel thead td').each( function (i) {
                var title = $(this).text();
                $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+(i+1)+'" />' );
            } );
    $('#tabel_detail thead td').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );
        tabel_view()
        refreshKelompok() 
       /// Button Action
	$('#tambah_data').click(function() { 
        
        refreshKelompok() 
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    });
    
        $("#btn_xls").on("click",function (){
          var link = '<?php echo base_url(); ?>master/contoh/export_xls/';
          var buka = window.open(link,'_blank');
          buka.focus();
        });

     $( TabelData.table().container() ).on( 'keyup', 'thead input', function () {
        TabelData.column( $(this).data('index') ).search( this.value ).draw();
     });

     // $('#tabel_filter').remove();

      $(document).on('click','#btn_filtering', function(){
          var data = $(this).data('filtering');
          if(data==1){
            $('#btn_filtering').data('filtering',0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
            $('#filtering').hide(500);

          }else{
            $('#btn_filtering').data('filtering',1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
            $('#filtering').show(500);
          }
       });
        
});        

	function refreshKelompok() {
		$.post("<?php echo base_url(); ?>master/contoh/refreshKelompok", function(data) {
			$("#kelompok").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	} 

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "columnDefs": [
                    { "visible": false, "targets": 9 }, 
                    { "visible": false, "targets": 10 }, 
                    { "visible": false, "targets": 11 },
                    { "visible": false, "targets": 12 }, 
                    { "orderable": false, "targets": 13 }, 
                  ],
                "destroy": true,
                "orderCellsTop":true,
                "serverSide": false,
                "processing": false,
                "sDom":'Rfrtlip',
                "paging": true,
                "lengthMenu":[10,25,50,100],
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'no_spuc' },
                  { data : 'cito' }, 
                  { data : 'tgl_terima' }, 
                  { data : 'contoh' }, 
                  { data : 'tgl_awal' }, 
                  { data : 'tgl_akhir' }, 
                  { data : 'data_uji' }, 
                  { data : 'komentar' }, 
                  { data : 'id' },
                  { data : 'id_approve' }, 
                  { data : 'id_kategori' }, 
                  { data : 'bappuc' },
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/analis/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){ 
                             
                              // if(write_menu==''){
                                  // var aksi = '- no access -';
                              // }else{
                                  // var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span>Edit</button> <button  onclick="konfirmasi(\''+ i +'\')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span>Hapus</button></center>'
                              // }
                              
                              var style_akhir = '';
                              var style_awal = '';
                              if(json[i].TGL_AKHIR==null && json[i].TGL_AWAL==null){
                                 var  style_akhir = "style='display:none'";
                                 var  style_awal = "";
                              }else if(json[i].TGL_AWAL!=null && json[i].TGL_AKHIR==null){
                                 var  style_akhir = " ";
                                 var  style_awal = "style='display:none'";
                              }else if(json[i].TGL_AWAL!=null && json[i].TGL_AKHIR!=null){
                                  
                                 var  style_akhir = "style='display:none'";
                                 var  style_awal = "style='display:none'";
                              }
                              
                              
                              // if(json[i].TOT_APPROVE==0){
                                  // var aksi = ''
                              // }else{
                                  var aksi = '<center><button '+style_awal+' onclick="mulai(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span>Mulai</button> <button '+style_akhir+'  onclick="selesai(\''+ i +'\')" class="btn btn-success btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-edit"></i></span>Selesai</button> <button onclick="load_export(\''+ json[i].NAMA_CONTOH +'\', \''+ json[i].JUMLAH +'\', \''+ json[i].NO_BAPPUC +'\', \''+ i +'\', \''+ json[i].TANGGAL +'\', \''+ json[i].TGL_AWAL +'\', \''+ json[i].TGL_AKHIR +'\', \''+ json[i].ID_KATEGORI +'\', \''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-warning btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-file-excel-o"></i></span>Download</button>  <button  onclick="upload(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-upload"></i></span>Upload</button> </center>'
                              // }
                            return_data.push({
                              'no'	    : no ,
                              'no_spuc'      :    '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\', \'1\')">'+json[i].NO_BAPPUC+'</a>',  
                              'cito'          : json[i].CITO,
                              'tgl_terima'      :   json[i].TANGGAL,  
                              'contoh'          :  json[i].NAMA_CONTOH , 
                              'tgl_awal'          :  json[i].TGL_AWAL , 
                              'tgl_akhir'          :  json[i].TGL_AKHIR , 
                              'data_uji'          :   '<center><button class="btn-success" title="Click for Detail" onclick="tabel_detail(\''+ json[i].ID_TRANSAKSI +'\', \''+ json[i].ID_KATEGORI +'\')"><span><i class="fa fa-info"></i></span> Detail</button> </center>'  , 
                              'komentar'          :  json[i].KOMENTAR  , 
                              'id'      :   json[i].ID_TRANSAKSI,   
                              'id_approve'      :   json[i].ID_APPROVE,  
                              'id_kategori'      :   json[i].ID_KATEGORI,
                              'bappuc' :json[i].NO_BAPPUC,   
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    }
    
    
    function tabel_detail(id_transaksi, id_kategori){
        
		$("#dlg_detail_upload").modal("show");
       	
    var TabelDatas= $('#tabel_detail').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" }, 
                "destroy": true,
                "orderCellsTop":true,
                "serverSide": false,
                "processing": false,
                "sDom":'Rfrtlip',
                "paging": true,
                "lengthMenu":[10,25,50,100],
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'uji' },
                  { data : 'sampel' }, 
                  { data : 'nilai' },  
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/analis/detail_data', 
                      data : {
                        'id_transaksi' :    id_transaksi,
                        'id_kategori' :    id_kategori,
                      },
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){   
                            return_data.push({
                              'no'	    : no ,   
                              'uji'          : json[i].NAMA_UJI,
                              'sampel'      :   json[i].SAMPEL,  
                              'nilai'          :  json[i].NILAI ,  
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search 
        
            TabelDatas.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    }
     
     function load_export(contoh, jumlah, spuc, metode, tgl_terima, tgl_kerja, tgl_selesai, kategori, id_transaksi) {         
                var base_url    =  "<?php echo base_url(); ?>"; 
                      var page    =   base_url + 'approval/analis/export_excel/?contoh=' +contoh+ '&&jumlah=' +jumlah+ '&&spuc=' +spuc+ '&&metode=' +metode+ '&&tgl_terima=' +tgl_terima+ '&&tgl_kerja=' +tgl_kerja+ '&&tgl_selesai=' +tgl_selesai+ '&&kategori=' +kategori+'&&id_transaksi=' +id_transaksi;
                        // e.preventDefault();
                        $.ajax({
                            url: page,
                            type: 'GET',	
                            data: {
                               'contoh': contoh,
                               'jumlah': jumlah,
                               'spuc': spuc,
                               'metode': metode,
                               'tgl_kerja': tgl_kerja,
                               'tgl_terima': tgl_terima,
                               'tgl_selesai': tgl_selesai,
                               'kategori': kategori,
                               'id_transaksi': id_transaksi,
                           },
                            success: function() {
                                 window.open(page,'_blank' );
                                // window.location = page;
                            }
                        });
                
     }
     
     function upload(baris) {
         var kolom = TabelData.row(baris).data(); 
         
		$("#id_transaksi_upload").val(kolom['id']);
		$("#kategori_upload").val(kolom['id_kategori']);
		$("#file_input").val("");
		$("#nama_file_input").val("Pilih file...");
		$("#total_size").html("0&nbsp;Bytes");
		temp_size = 0;
		$("#dlg_upload").modal("show");
	}

	function simpan() {
		var nm_contoh = $("#nm_contoh").val(); 
		var kelompok = $("#kelompok").val();  
		if(nm_contoh == "" ) {
			informasi(BootstrapDialog.TYPE_WARNING, "Nama Contoh harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>master/contoh/simpan", {"nm_contoh": nm_contoh,"kelompok": kelompok 
		}, function(datas) {
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, data.message);
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menyimpan data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
     
	function edit(baris) { 
		var kolom = TabelData.row(baris).data(); 
        
		$("#id_transaksi").val(kolom['id']);
		$("#spuc").val(kolom['bappuc']);
		$("#tgl_terima").val(kolom['tgl_terima']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("<b><i class='fa fa-pencil-alt'></i>&nbsp;Kaji Kesesuaian  Permintaan Pengujian</b>"); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_transaksi").val(); 
		var approve = $("#approve").val(); 
		var syarat = $("#syarat").val();  
		if( approve == "" 
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
			return;
		} 
		$.post("<?php echo base_url(); ?>approval/mt/update", {"id": id, "approve": approve, "syarat": syarat }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function mulai(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_SUCCESS,
			"title": "<b><i class='fa fa-check'></i>&nbsp;Mulai Analisa</b>",
			"message": "Mulai Analisa NO. SPUC \"" + kolom['bappuc']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-primary btn-xs input_update",
				"icon": "fa fa-check",
				"label": "Mulai",
				"action": function(dialog) {
					start(kolom['id'], kolom['id_approve'], kolom['id_kategori'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function start(id,id_approve,id_kategori, dialog) {
		dialog.setClosable(false); 
           $('.input_update').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Proses'); 
			$('.input_update').attr("disabled", "disabled");        
		$.post("<?php echo base_url(); ?>approval/analis/mulai", {"id": id,"id_approve": id_approve,"id_kategori": id_kategori}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil merubah data.");
				 tabel_view()
				$('.input_update').removeAttr("disabled");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal merubah data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}
    
    
    function selesai(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_PRIMARY,
			"title": "<b><i class='fa fa-check'></i>&nbsp;Selesai Analisa</b>",
			"message": "Selesai Analisa NO. SPUC \"" + kolom['no_spuc']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-success btn-xs btn-selesai",
				"icon": "fa fa-check",
				"label": "Mulai",
				"action": function(dialog) {
					finish(kolom['id'], kolom['id_kategori'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function finish(id,id_kategori, dialog) {
		dialog.setClosable(false); 
         $('.btn-selesai').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Proses'); 
			$('.btn-selesai').attr("disabled", "disabled");
		$.post("<?php echo base_url(); ?>approval/analis/selesai", {"id": id,"id_kategori": id_kategori}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil merubah data.");
				 tabel_view()
				$('.btn-selesai').removeAttr("disabled");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal merubah data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_PRIMARY,
			"title": "<b><i class='fa fa-check'></i>&nbsp;Hapus Contoh</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/contoh/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}
	function cek(input) {
		if((input.value).length > 0) {
			var temp_nama = input.files[0].name;
			temp_size = parseInt(input.files[0].size);
			var total = temp_size >= 1024 ? (temp_size / 1024) : temp_size + "&nbsp;Bytes";
			total = isNaN(total) ? total : (total >= 1024 ? (total / 1024).toFixed(2) + "&nbsp;MB" : total.toFixed(2) + "&nbsp;KB");
			if(temp_size > <?php echo $maksimal[$dipakai]["number"]; ?>) {
				input.value = "";
				$("#nama_file_input").val("Pilih file...");
				$("#total_size").html("0&nbsp;Bytes");
				temp_size = 0;
				informasi(BootstrapDialog.TYPE_INFO, "Ukuran file terlalu besar.");
				return;
			}
			temp_nama_arr = temp_nama.split(".");
			if(temp_nama_arr[temp_nama_arr.length-1].toLowerCase() === "xlsx") {
				$("#nama_file_input").val(temp_nama);
				$("#total_size").html(total);
			} else {
				input.value = "";
				$("#nama_file_input").val("Pilih file...");
				$("#total_size").html("0&nbsp;Bytes");
				temp_size = 0;
				informasi(BootstrapDialog.TYPE_INFO, "File harus Excel 2007 (xlsx)");
			}
		} else {
			$("#nama_file_input").val("Pilih file...");
			$("#total_size").html("0&nbsp;Bytes");
			temp_size = 0;
		}
	}

</script>