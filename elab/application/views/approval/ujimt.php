
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
    .form-group {
        margin-bottom : 0px;
    }
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			<h2><i class="fa fa-list"></i> Persetujuan Hasil Uji oleh MT</h2>   
            <div style="text-align:left;display:none"  >
                &nbsp;&nbsp;&nbsp;<button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><i class="fa fa-file-excel-o">&nbsp;Export Excel</i></button> 
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:75%" id="tabel">
                        <thead>
                            <tr>
                                <th rowspan='2' style='width:10px;'>No</th> 
                                <th rowspan='2' >NO SPUC  </th> 
                                <th rowspan='2' >CITO   </th> 
                                <th rowspan='2' >Tanggal Terima  </th> 
                                <th rowspan='2' >Nama Contoh   </th> 
                                <th colspan='2' >SURAT PERINTAH UJI CONTOH   </th> 
                                <th rowspan='2' >Analis    </th> 
                                <th colspan='2' >HASIL PENGUJIAN   </th>  
                                <th rowspan='2' ></th> 
                                <th rowspan='2' ></th> 
                                <th rowspan='2' >Aksi</th> 
                            </tr>
                            <tr> 
                                <th   >MT    </th> 
                                <th  >Penyelia    </th> 
                                <th  >Penyelia    </th> 
                                <th   >MT    </th> 
                                
                            </tr> 
                            
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>   
                 <hr>
                  <center><p> </p> </center> 
                  <input type="hidden" name="id_transaksi" id="id_transaksi" >
                  <input type="hidden" name="id_kategori" id="id_kategori" > 
                  <p  >APAKAH ANDA MENYETUJUI HASIL UJI?</p>  
                  <p id='build_kategori'></p>  
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-check'></i>&nbsp;Setuju</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>


<script>
    var TabelData;
$(document).ready(function(){  
    $('#tabel thead td').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
        tabel_view() 
        
});        

	function refreshKelompok() {
		$.post("<?php echo base_url(); ?>master/contoh/refreshKelompok", function(data) {
			$("#kelompok").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	} 

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
               
                "columnDefs": [
                    { "visible": false, "targets": 11 }, 
                    { "visible": false, "targets": 10 }, 
                  ],
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' }, 
                  { data : 'no_spuc' },
                  { data : 'cito' }, 
                  { data : 'tgl_terima' }, 
                  { data : 'contoh' }, 
                  { data : 'spuc_mt' }, 
                  { data : 'spuc_penyelia' }, 
                  { data : 'analis' }, 
                  { data : 'uji_penyelia' }, 
                  { data : 'uji_mt' },  
                  { data : 'id' },  
                  { data : 'id_kategori' },  
                  { data : 'aksi' },  
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/ujimt/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){   
                             var aksi = '<center><button onclick="edit(\''+ i +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><i class="btn-labelx fa fa-check"> </i>Hasil Uji</button> </center>';
                            return_data.push({
                              'no'	    : no , 
                              'no_spuc'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\')">'+json[i].NO_BAPPUC+'</a>', 
                              'cito'          :  json[i].CITO,  
                              'tgl_terima'      :   json[i].TANGGAL,  
                              'contoh'          :  json[i].NAMA_CONTOH , 
                              'spuc_mt'      :   json[i].SPUC_MT,   
                              'spuc_penyelia'          :  json[i].SPUC_PENYELIA , 
                              'analis'      :   json[i].ANALIS,  
                              'uji_penyelia'          :  json[i].HASIL_PENYELIA , 
                              'uji_mt'          :  json[i].HASIL_MT,
                              'id'          :  json[i].ID_TRANSAKSI ,  
                              'id_kategori'          :  json[i].ID_KATEGORI ,  
                              'aksi'          : aksi, 
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search
            TabelData.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        
    } 
    
    
	function edit(baris) { 
		var kolom = TabelData.row(baris).data();  
        $.post("<?php echo base_url(); ?>approval/ujimt/getKategori",{ "id": kolom['id']  }, function(data) {
			$("#build_kategori").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			// $(".selectpicker").selectpicker("refresh");
		});
		$("#id_transaksi").val(kolom['id']);
		$("#id_kategori").val(kolom['id_kategori']);
		$("#id_status").val(status);
		$("#spuc").val(kolom['no_spuc']);
		$("#tgl_terima").val(kolom['tgl_terima']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html("Persetujuan Hasil Uji NO. SPUC "+kolom['no_spuc']); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
	function update() {
		var id = $("#id_transaksi").val(); 
		var id_kategori = $("#id_kategori").val();     
		// if( approve == "" 
		// ) {
			// informasi(BootstrapDialog.TYPE_WARNING, "Form harus terisi.");
			// return;
		// } 
		$.post("<?php echo base_url(); ?>approval/ujimt/update", {"id_kategori": id_kategori, "id": id }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
     
     function lhu(id_transaksi, id_kategori) {
        var base_url    =  "<?php echo base_url(); ?>";
        // if(id_kategori=='1'||id_kategori=='2'){
            var link = 'lhu_old';
        // }else{
              // var link = 'lhu';
        // }
        if(id_kategori=='3' || id_kategori=='5')
        {
          link ='lhu';
        }
        var page    =   base_url + 'hasil_uji/'+link+'/?id_transaksi=' +id_transaksi+ '&&id_kategori=' +id_kategori;
                // e.preventDefault();
                $.ajax({
                    url: page,
                    type: 'GET',
                    // data: {
                       // 'id_kategori': id_kategori,
                       // 'id_transaksi': id_transaksi
                   // },
                    success: function() {
                         // window.open(page,'_blank' );
                        // window.location = page;
                    }
                });
     }
     
      function shu(id_transaksi, id_kategori) {
        var base_url    =  "<?php echo base_url(); ?>";
        var link = 'shu';
        if(id_kategori=='3' || id_kategori=='5')
        {
          link ='newshu';
        }
        var page    =   base_url + 'hasil_uji/'+link+'/?id_transaksi=' +id_transaksi;
                // e.preventDefault();
                $.ajax({
                    url: page,
                    type: 'GET',
                    data: {
                       'id_transaksi': id_transaksi
                   },
                    success: function() {
                         // window.open(page,'_blank' );
                        // window.location = page;
                    }
                });
     }
     

</script>