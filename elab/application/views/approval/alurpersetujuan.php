
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

    .form-group {
        margin-bottom : 0px;
    }
 </style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
          <h2><i class="fa fa-list"></i> Approval Alur Penyaluran</h2>

            <div class="ibox-content">
                <div class="row">
                  <br>
                </div>
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:75%" id="tabel">
                        <thead>
                            <tr>
                                <th rowspan='2' style='width:10px;'>No</th>
                                <th rowspan='2' >Detail SPUC  </th>
                                <th rowspan='2' >NO SPUC  </th>
                                <th rowspan='2' >CITO   </th>
                                <th rowspan='2' >Tanggal Terima  </th>
                                <th rowspan='2' >Nama Contoh   </th>
                                <th colspan='2' >SURAT PERINTAH UJI CONTOH   </th>
                                <th rowspan='2' >Analis    </th>
                                <th colspan='2' >HASIL PENGUJIAN   </th>
                                <th rowspan='2' >Aksi</th>
                            </tr>
                            <tr>
                                <th   >MT    </th>
                                <th  >Penyelia    </th>
                                <th  >Penyelia    </th>
                                <th   >MT    </th>

                            </tr>

                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
</div>
</div>



<script>
    var TabelData;
$(document).ready(function(){
    var tables = $('#tabel').DataTable({
          processing: true,
          serverSide: true,
          searching: true,
          "autoWidth": false,
          "aLengthMenu": [[10,25, 50, 75, -1], [10,25, 50, 75, "All"]],
          ajax: {
              'type': 'POST',
              'url': '<?php echo site_url(); ?>index.php/approval/alurpersetujuan/get_data',
          },
          columns: [
          { data : 'NO', name: 'NO' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'KODE_UJI', name: 'KODE_UJI' },
          { data : 'ID_TRANSAKSI', name: 'ID_TRANSAKSI' },
          ],
          "columnDefs": [
            { "orderable": false, "targets": 0 }
          ]
      });

});

</script>