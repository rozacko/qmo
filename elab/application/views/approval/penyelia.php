
 <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
    .form-group {
        margin-bottom : 0px;
    }
    div.dataTables_length {
      margin-right: 0.5em;
      margin-top: 0.2em;
    }
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
			      <div class="col-lg-6"><h2><i class="fa fa-list"></i> Approval Penyelia</h2></div>
            <div class="col-lg-6" style='display:none'>
              <div style="text-align:right;">
                &nbsp;&nbsp;&nbsp;<button type="button"  id='btn_xls'  class="btn-sm btn-warning" id="tambah_data"><span class="fa fa-file-excel-o"></span>&nbsp;Export Excel</button> 
              </div>  
            </div>
            
            <div class="ibox-content">
                <div class="row">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:75%" id="tabel">
                        <thead>
                            <tr>
                                <th rowspan='2' style='width:10px;'>No</th> 
                                <th rowspan='2' >NO SPUC  </th> 
                                <th rowspan='2' >CITO   </th> 
                                <th rowspan='2' >Tanggal Terima  </th> 
                                <th rowspan='2' >Nama Contoh   </th> 
                                <th rowspan='2' >Detail   </th> 
                                <th colspan='2' >SURAT PERINTAH UJI CONTOH   </th> 
                                <th rowspan='2' >Analis    </th> 
                                <th colspan='2' >HASIL PENGUJIAN   </th> 
                                <th rowspan='2' ></th> 
                                <th rowspan='2' ></th> 
                                <th rowspan='2' >Aksi</th> 
                            </tr>
                            <tr> 
                                <th   >MT    </th> 
                                <th  >Penyelia    </th> 
                                <th  >Penyelia    </th> 
                                <th   >MT    </th> 
                                
                            </tr> 
                            
                        </thead>
                        <tbody> 			
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>  
</div>
</div>


<div class='modal fade' id='dlg' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header' id='dlg_header'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
			</div>
			<div class='modal-body'>   
                 <hr>
                  <center><p> </p> </center>
                  <center> <label class="check-label"><i class=" "></i><input type="radio" name="approve"  id="approve" value="Y">&nbsp;Setuju</label>
                        <label class="check-label" style="margin-left:12px"><i class=" "></i>&nbsp;&nbsp;<input type="radio" name="approve" id="approve" value="N">&nbsp;Pending</label>
                  </center> 
                  <input type="hidden" name="id_transaksi" id="id_transaksi" >
                  <input type="hidden" name="id_kategori" id="id_kategori" >
                  <input type="hidden" name="id_status" id="id_status" >
                  <p>Keterangan :</p>
                   <div class="row">  <div class="col-lg-12"> 
                        <textarea name="syarat" id='syarat' style="width:100%"></textarea>
                    </div>
                    </div>
			</div> 
			<div class='modal-footer'>
				<button type='button' class='btn btn-primary btn-xs btn-simpan' onclick='simpan();' id='input_simpan'><i class='fa fa-save'></i>&nbsp;Simpan</button>
				<button type='button' class='btn btn-success btn-xs btn-edit' onclick='update();' id='input_update'><i class='fa fa-check'></i>&nbsp;OK</button>
				<button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
			</div>
		</div>
	</div>
</div>

<div class='modal fade' id='dlg_detail_upload' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header modal-success'>
				<button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
				<div class='modal-title'><b><i class="fa fa-file-excel"></i> Data Pengujian</b></div>
			</div> 
				<div class='modal-body'>
                        <div class="row">
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" style="font-size:85%" id="tabel_detail" width="100%">
                                <thead>
                                    <tr>
                                        <th  style='width:3px;'>No</th> 
                                        <th>Parameter  </th> 
                                        <th>Sampel </th> 
                                        <th>Nilai </th>  
                                    </tr> 
                                    <tr>
                                        <th  style='width:3px;'>No</th> 
                                        <td>Parameter  </td> 
                                        <td>Sampel </td> 
                                        <td>Nilai </td>  
                                    </tr> 
                                </thead>
                                <tbody> 			
                                </tbody>
                            </table>
                        </div>
                        </div>
				</div>
				<div class='modal-footer'> 
					<button type='button' class='btn btn-default btn-tutup btn-xs' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
    var TabelData;
$(document).ready(function(){  
    $('#tabel thead td').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" data-index="'+(1+i)+'"/>' );
    } );
        tabel_view()
        refreshKelompok() 
       /// Button Action
	$('#tambah_data').click(function() { 
        
        refreshKelompok() 
        $("#judul_input").html('Form Tambah');
        $(".btn-simpan").show();
        $(".btn-edit").hide();
        $(".id_primary").hide();
        $("#id_contoh, #nm_contoh, #kelompok, #lokasi").val('');
		$("#dlg").modal("show");
    });
    
        $("#btn_xls").on("click",function (){
          var link = '<?php echo base_url(); ?>master/contoh/export_xls/';
          var buka = window.open(link,'_blank');
          buka.focus();
        })
        
});        

	function refreshKelompok() {
		$.post("<?php echo base_url(); ?>master/contoh/refreshKelompok", function(data) {
			$("#kelompok").html(data);
		}).fail(function() {
			// Nope
		}).always(function() {
			$(".selectpicker").selectpicker("refresh");
		});
	} 

    function tabel_view(){
       	TabelData = $('#tabel').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" },
                "destroy": true,
                "serverSide": false,
                "processing": false,
                "paging": true,
                "lengthChange": true,
                "lengthMenu": [10, 25, 50, 100],
                "searching": true,
                "sDom":'Rfrtlip',
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "columnDefs":[
                  { "visible": false, "targets":11 },
                  { "visible": false, "targets":12 },
                ],
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' }, 
                  { data : 'no_spuc' },
                  { data : 'cito' }, 
                  { data : 'tgl_terima' }, 
                  { data : 'contoh' }, 
                  { data : 'data_uji' }, 
                  { data : 'spuc_mt' }, 
                  { data : 'spuc_penyelia' }, 
                  { data : 'analis' }, 
                  { data : 'uji_penyelia' }, 
                  { data : 'uji_mt' }, 
                  { data : 'id' }, 
                  { data : 'id_kategori' }, 
                  { data : 'aksi' }, 
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/penyelia/get_data', 
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){
                            var hide = (json[i].KETERANGAN_TOLAK == null ? ' style="display:none" '  : '')
                                    console.log(json[i].KETERANGAN_TOLAK)
                          if(json[i].TOMBOL=='SPUC'){
                              var aksi = '<center '+hide+' ><button onclick="edit(\''+ i +'\', \''+ json[i].TOMBOL +'\')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-check"></i></span> SPUC</button> </center>';
                          }else{ 
                              var aksi = '<center><button onclick="edit(\''+ i +'\', \''+ json[i].TOMBOL +'\')" class="btn btn-success btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-check"></i></span> Hasil Uji</button> </center>';
                          }
                            return_data.push({
                              'no'	    : no ,
                              // 'spuc'	    :   '<center><button onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\')" class="btn btn-warning btn-xs waves-effect btn_edit" title="Detail KKPP"><span class="btn-labelx"><i class="fa fa-check"></i></span> </button> </center>', 
                              'no_spuc'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc_folder(\''+ json[i].ID_TRANSAKSI +'\', \'1\')">'+json[i].NO_BAPPUC+'</a>', 
                              'cito'          :  json[i].CITO,  
                              'tgl_terima'      :   json[i].TANGGAL,  
                              'contoh'          :  json[i].NAMA_CONTOH , 
                               'data_uji'          :   '<center><button class="btn-success" title="Click for Detail" onclick="tabel_detail(\''+ json[i].ID_TRANSAKSI +'\', \''+ json[i].ID_KATEGORI +'\')"><span><i class="fa fa-info"></i></span> Detail</button> </center>'  , 
                              'spuc_mt'      :   json[i].SPUC_MT,   
                              'spuc_penyelia'          :  json[i].SPUC_PENYELIA , 
                              'analis'      :   json[i].ANALIS,  
                              'uji_penyelia'          :  json[i].HASIL_PENYELIA , 
                              'uji_mt'          :  json[i].HASIL_MT , 
                              'id'          :  json[i].ID_TRANSAKSI ,  
                              'id_kategori'          :  json[i].ID_KATEGORI ,  
                              'aksi' : aksi
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search
            TabelData.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        
    } 
     
	function edit(baris, tipe) { 
        $('#input_update').html('<i class="fa fa-check"></i>&nbsp;OK'); 
				$('#input_update').removeAttr("disabled");
		var kolom = TabelData.row(baris).data(); 
        if(tipe=='SPUC'){
            var isi = "<b><i class='fa fa-pencil-alt'></i>&nbsp;Persetujuan SPUC "+kolom['no_spuc']+"</b>";
            var status = 'a1'
        }else{ 
            var isi = "<b><i class='fa fa-pencil-alt'></i>&nbsp;Persetujuan Hasil Uji "+kolom['no_spuc']+"</b>";
            var status = 'a3'
        }
        
		$("#id_transaksi").val(kolom['id']);
		$("#id_kategori").val(kolom['id_kategori']);
		$("#id_status").val(status);
		$("#spuc").val(kolom['no_spuc']);
		$("#tgl_terima").val(kolom['tgl_terima']); 
        $(".btn-simpan").hide();
        $(".btn-edit").show(); 
		$("#judul_input").html(isi); 
		$("#dlg").modal("show");
		$(".selectpicker").selectpicker("refresh");  
	}
    
	function update() {
		var id = $("#id_transaksi").val(); 
		var id_kategori = $("#id_kategori").val(); 
		var approve = $("input[name='approve']:checked").val(); 
		var syarat = $("#syarat").val();  
		var id_status = $("#id_status").val();  
		if( approve == "" 
		) {
			informasi(BootstrapDialog.TYPE_WARNING, "Keterangan harus terisi.");
			return;
		} else if( approve == "N"  && syarat == ''
		) {
           informasi(BootstrapDialog.TYPE_WARNING, "Keterangan harus terisi.");
			return;
        }

            $('#input_update').html('<i class="fa fa-spinner fa-spin"></i>&nbsp;Proses'); 
			$('#input_update').attr("disabled", "disabled");  
		$.post("<?php echo base_url(); ?>approval/penyelia/update", {"id_kategori": id_kategori, "id": id, "approve": approve, "syarat": syarat, "id_status": id_status }, function(datas) {  
			 var data = JSON.parse(datas);
			if(data.notif == "1") {
				$("#dlg").modal("hide");
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil mengubah data.");
				 tabel_view()
                 
				$('#input_update').removeAttr("disabled");
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal mengubah data. Server sedang bermasalah.");
		}).always(function() { 
		});
	}
    function konfirmasi(baris) { 
		var kolom = TabelData.row(baris).data();
        console.log(kolom)
		BootstrapDialog.show({
			"type": BootstrapDialog.TYPE_DANGER,
			"title": "<b><i class='fa fa-trash'></i>&nbsp;Hapus Contoh</b>",
			"message": "Anda yakin ingin menghapus Contoh \"" + kolom['contoh']  + "\"?",
			"closeByBackdrop": false,
			"closeByKeyboard": false,
			"buttons": [{
				"cssClass": "btn btn-danger btn-xs btn-hapus",
				"icon": "fa fa-trash",
				"label": "Hapus",
				"action": function(dialog) {
					hapus(kolom['id'], dialog);
				}
			},{
				"cssClass": "btn btn-default btn-xs btn-tutup",
				"icon": "fa fa-times",
				"label": "Tutup",
				"action": function(dialog) {
					dialog.close();
				}
			}]
		});
	}
    
    function tabel_detail(id_transaksi, id_kategori){
        
		$("#dlg_detail_upload").modal("show");
       	
    var TabelDatas= $('#tabel_detail').DataTable({ 
				"oLanguage": { "sEmptyTable": "Tidak Terdapat Data" }, 
                "destroy": true,
                "orderCellsTop":true,
                "serverSide": false,
                "processing": false,
                "sDom":'Rfrtlip',
                "paging": true,
                "lengthMenu":[10,25,50,100],
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                // "scrollY": '50vh',
                "scrollCollapse": true,
                columns : [
                  { data : 'no' },
                  { data : 'uji' },
                  { data : 'sampel' }, 
                  { data : 'nilai' },  
                ],    
                  ajax: {
                     type: 'POST',
                      url: '<?php echo site_url(); ?>index.php/approval/analis/detail_data', 
                      data : {
                        'id_transaksi' :    id_transaksi,
                        'id_kategori' :    id_kategori,
                      },
                     dataType: 'JSON',
                     dataSrc : function (json) { 
                          var return_data = new Array();
                          var no = 1;
                          for(var i=0;i< json.length; i++){   
                            return_data.push({
                              'no'	    : no ,   
                              'uji'          : json[i].NAMA_UJI,
                              'sampel'      :   json[i].SAMPEL,  
                              'nilai'          :  json[i].NILAI ,  
                            })
                            no+=1;
                          }
                          return return_data;
                    }
                  }
		}); 
    // Apply the search 
        
            TabelDatas.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    }
    
	function hapus(id, dialog) {
		dialog.setClosable(false); 
		$.post("<?php echo base_url(); ?>master/contoh/hapus", {"id": id}, function(datas) { 
            var data = JSON.parse(datas);
			if(data.notif == "1") {
				dialog.close();
				informasi(BootstrapDialog.TYPE_SUCCESS, "Berhasil menghapus data.");
				 tabel_view()
			} else {
				informasi(BootstrapDialog.TYPE_DANGER, data.message);
			}
		}).fail(function() {
			informasi(BootstrapDialog.TYPE_DANGER, "Gagal menghapus data. Server sedang bermasalah.");
		}).always(function() { 
			dialog.setClosable(true);
		});
	}

</script>