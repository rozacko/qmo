  <style>
 thead input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;

    }
    
 </style>

<!-- Detail SPUC -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/detail_spuc.js"></script> 
 <div id='detail_spuc'></div>
<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h2><i class="fa fa-list"></i> Laporan Keuangan</h2>
              
               
                    <!--
                    <div class="container" style="padding-top: 10px;  margin: 0px;padding: 0px;">
                      
                        <div class="row">
                          <form id="form">
                            <div class="col-md-2">
                                <input type="text"  placeholder="Tanggal mulai" class="form-control" readonly="" id="start_date" class="form-control"> 
                             
                            </div>
                            <div class="col-md-2">
                                <input type="text"  placeholder="Tanggal Selesai" class="form-control" readonly="" id="end_date" class="form-control">  
                              
                            </div>
                            <div class="col-md-2">
                              <select class="form-control selectpicker" data-live-search='true' id="company">
                                <option value="" selected data-hidden='true'> --  Pilih Company -- </option>
                                <?php 
                                foreach ($company->result() as $key) {
                                  ?>
                                  <option value="<?php echo $key->ID_COMPANY ?>"><?php echo $key->NM_COMPANY ?></option>
                                  <?php
                                }
                                ?>
                              </select>  
                              
                            </div>
                            <div class="col-md-2">
                              <select class="form-control selectpicker" name="3" id="lab" readonly style='background-color: white;'>
                                                             
                                                             <option value='' data-hidden = 'true'> --  Pilih Lab -- </option>
                                                        
                                                      </select>
                              
                            </div>
                            <div class="col-md-2">
                              <input type="button" name="search" id="search" value="Search" class="btn-info" />
                              <input type="button" name="search" id="reload" value="Reset" class="btn-default" />
                              </form>
                            </div>
                              

                        </div>
                      
                    </div>
                    -->
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="Table" class="table table-striped table-bordered table-hover dataTables-example" style="font-size:90%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>NO SPUC</th>
                            <th>Tanggal Terima</th>
                            <th>Nama Company</th>
                            <th>Laboratorium</th>
                            <th><center>Total <span class="fa fa-filter pull-right" data-filtering="1"
                                                    id="btn_filtering"> <i class="fa fa-angle-double-up"></i> </span></center></th>
                          </tr>  
                          <tr id="filtering">
                            <th></th>
                            <td>NO SPUC</td>
                            <td>Tanggal Terima</td>
                            <td>Nama Company</td>
                            <td>Laboratorium</td>
                            <th> </th>
                          </tr>  
                        </thead>

                       
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <script type="text/javascript">
          function add(){
            $('#myModal').modal('show');
           }
         
          $(document).ready(function(){

             $('#start_date').datepicker({
                
                 todayHighlight: true,
                format: "dd-mm-yyyy",
                autoclose: true
               });
             $('#end_date').datepicker({
                
                 todayHighlight: true,
                format: "dd-mm-yyyy",
                autoclose: true
               });
             
                // var dataTable = $('#Table').DataTable({
                 // "processing" : true,
                 // "serverSide" : true,
                 // "lengthMenu": [10, 25, 50, 100],
                 // "pageLength": 10, 
                 // 'columnDefs': [ 
                     // {
                          // "targets": 5,
                          // "className": "text-right",
                     // }],
                // "ordering": false,
                 // "ajax" : {
                  // "url":"<?php echo site_url('Keuangan/ajax_list')?>",
                  // "type":"POST",
                  // "data": function ( data ) {
                      // data.start_date = $('#start_date').val();
                      // data.end_date = $('#end_date').val();
                      // data.company = $('#company').val();
                      // data.lab = $('#lab').val();
                  // }
                 // }
                // });
               

               // $('#search').click(function(){
                // var start_date = $('#start_date').val();
                // var end_date = $('#end_date').val();
                // var company = $('#company').val();
                // var lab = $('#lab').val();
                // if(start_date == '' || end_date == '' || company == '' || lab ==''){
                  // alert('Semua Data Harus Terisi');
                // }else{
               // dataTable.ajax.reload();
               // }
               // });
               // $('#reload').click(function(){ //button reset event click
                  // $('#form')[0].reset();
                  // $('.selectpicker').selectpicker('refresh');
                  // dataTable.ajax.reload();  //just reload table
              // }); 
              
              
                TabelData = $('#Table').DataTable({

            "oLanguage": {"sEmptyTable": "Tidak Terdapat Data"},
            "orderCellsTop": true,
            'dom': 'Bfrtip',
            'buttons': [
                {extend: 'copy', exportOptions: {columns: [0,1,2]}},
                {extend: 'excel', exportOptions: {columns: [0,1,2]}},
                {extend: 'pdf', exportOptions: {columns: [0,1,2]}},
                {extend: 'print', exportOptions: {columns: [0,1,2]}},

            ],
            "columnDefs": [
                { className: "dt-right", "targets": [5] },
                // {"visible": false, "targets": 3},
                // {"visible": false, "targets": 4},
                {"orderable": false, "targets": 5},
                // {"orderable": false, "targets": 0},
            ],
            "serverSide": false,
            "processing": false,
            "paging": true,
            'lengthmenu': [10, 25, 50, 100],
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "scrollCollapse": true,
            ajax: {
                type: 'POST',
                url: '<?php echo site_url(); ?>index.php/Keuangan/ajax_list',
                dataType: 'JSON',
                dataSrc: function (json) {
                    var return_data = new Array();
                    var no = 1;
                    for (var i = 0; i < json.length; i++) {
 
                        return_data.push({
                            'id': no,
                            'no_spuc'   :  '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc(\''+ json[i].ID_TRANSAKSI +'\')">'+json[i].NO_BAPPUC+'</a>', 
                            'terima': json[i].TANGGAL,
                            'company': json[i].NM_COMPANY,
                            'lab': json[i].KD_LAB,
                            'total': json[i].BIAYA, 
                        })
                        no += 1;
                    }
                    return return_data;
                }
            },
            columns: [
                {data: 'id'},
                {data: 'no_spuc'},
                {data: 'terima'},
                {data: 'company'},
                {data: 'lab'},
                {data: 'total'},
            ],
        });
              
           
            $('#Table thead td').each(function (i) {
                var title = $('#Table thead th').eq($(this).index()).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + (i + 1) + '" />');
            });

            $(TabelData.table().container()).on('keyup', 'thead input', function () {
                console.log($(this).data('index') + "-" + this.value);
                TabelData.column($(this).data('index')).search(this.value).draw();
            });

            TabelData.on('order.dt search.dt', function () {
                TabelData.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i + 1;
                });
            }).draw();


        $(document).on('click', '#btn_filtering', function () { 
            var data = $(this).data('filtering');
            if (data == 1) {
                $('#btn_filtering').data('filtering', 0).html("&nbsp; <i class='fa fa-angle-double-down'></i>"); //setter
                $('#filtering').hide(500);

            } else {
                $('#btn_filtering').data('filtering', 1).html("&nbsp; <i class='fa fa-angle-double-up'></i>"); //setter; //setter
                $('#filtering').show(500);
            }
        });

          });
          
            $('#company').change(function() {   
             refreshLab($("#company option:selected").val()) 
          });

            function refreshLab(id) {
              $.post("<?php echo base_url(); ?>Keuangan/lab", {"id": id   
              }, function(data) {
                $("#lab").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
           
        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Detail Data</h4>
             </div>
             <div class="modal-body">
                
                  
                  <div class="form-group">
                    <label>Periode Transaksi</label>
                    <input type="text" id="nama_instansi" name="nama_instansi" class="form-control" required="">
                  </div>
                  <div class="form-group">
                    <label>Total Transaksi</label>
                    <input type="text" name="NPWP" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Total Biaya</label>
                    <textarea class="form-control" name="alamat" rows="5"></textarea>
                  </div>

                  
                  
               
             </div>
             <div class="modal-footer">
              <button type="button" class="close" data-dismiss="modal">Back</button>
              
             </div>
            </div>

          </div>
        </div>
       
        
         