<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
	function __construct() {
		$this->ci = &get_instance();
	}

	function render($url, $data=NULL) {
		$this->ci->load->model('master/M_menu');
		$data["menu"] = $this->ci->M_menu->get_data();
		$this->ci->load->view('plain/default_header', $data);
		$this->ci->load->view('plain/sidebar', $data);
		$this->ci->load->view($url, $data);
		$this->ci->load->view('plain/default_footer', $data);
	}

}
?>