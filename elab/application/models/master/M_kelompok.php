<?php

class M_kelompok extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_KELOMPOK (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_KELOMPOK SET ".implode(",", $data)." WHERE ID_KELOMPOK={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_KELOMPOK WHERE ID_KELOMPOK={$id}");
	}

}
?>