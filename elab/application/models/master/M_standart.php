<?php

class M_standart extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI A
                LEFT JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI
                order by A.id_standart asc
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI A
                LEFT JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI
                
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_STANDART_UJI (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_STANDART_UJI SET ".implode(",", $data)." WHERE ID_STANDART={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_STANDART_UJI WHERE ID_STANDART={$id}");
	}
    
    
	public function getKategori() {
		return $this->db->query("SELECT * FROM ELAB_M_KATEGORI")->result_array();
	}

}
?>