<?php

class M_bebankerja extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * from ELAB_M_KATEGORI order by ID_KATEGORI
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_CONTOH SET ".implode(",", $data)." WHERE ID_CONTOH={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_CONTOH WHERE ID_CONTOH={$id}");
	}
    
    
	public function getKelompok() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}
	public function get_nmlab($id) {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM where ID_LAB='{$id}'")->row_array();
	}
    
    
	public function get_spuc($id, $id_lab, $date) { 
		return $this->db->query("select  A.ID_TRANSAKSI, B.ID_KATEGORI, JUMLAH from ELAB_T_BOX A
        join ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
				join ELAB_T_BPPUC C ON A.ID_TRANSAKSI = C.ID_TRANSAKSI
        WHERE B.ID_KATEGORI = '{$id}' and KODE_LAB = '{$id_lab}' AND NO_SPUC_4 = '{$date}'
        GROUP BY A.ID_TRANSAKSI, B.ID_KATEGORI, JUMLAH
        ORDER BY A.ID_TRANSAKSI 
        ")->result_array();
	}
    
	public function get_selesai($id, $id_kategori) {
		return $this->db->query("select count(*) as SELESAI from ELAB_T_UPLOAD a
        where ID_TRANSAKSI in ({$id}) and ID_KATEGORI = '{$id_kategori}'")->row_array();
	}
    

}
?>