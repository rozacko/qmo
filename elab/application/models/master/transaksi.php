<div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                <h2><i class="fa fa-list"></i> Transaksi BAPPUC</h2>
                    <div style="text-align:left">
                        <button class="btn btn-sm btn-success" onclick="add()" style="margin-top: 10px; margin-bottom: 20px;"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
                    </div>
                    <div class="ibox-content">
                      <div class="row">
                    
                    <table id="dataTable" class="table table-striped table-bordered table-hover" style="font-size:95%">
                        <thead>
                          <tr>
                            <th></th>
                            <th>NO BAPPUC</th>
                            <th>Tanggal Terima</th>
                            <th>Nama Contoh</th>
                            <th>Aksi</th>
                          </tr>
                        </thead>
                      </table>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
            
      
        <script type="text/javascript">
          var dataTable;
          $(document).ready(function(){
            
    tableData = $('#dataTable').DataTable({ 
        "processing": true,
        "serverSide": true,
    "lengthMenu": [10, 25, 50, 100],
    "pageLength": 10,
        "ordering": false,
        "ajax": {
            "url": "<?php echo site_url('master/transaksi/ajax_list')?>",
            "type": "POST"
        },
    
  });


            
            
            

           $("#wizard").steps();

            $("#form").steps({

                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {

                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");

                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);
                    
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";


                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);
                    //$("#warna").select2({
                          //placeholder:"--Pilih--"
                        //});
                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
             

            
              //edit
              $("#wizard").steps();
            $("#form1").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });


            

          
          });
          $(function(){
            $('#contoh').hide();
            $('#contoh1').hide();
            $('#uji_kim').hide();
            $('#uji_fis').hide();
            $('#uji_beton').hide();
            $('#peminta').hide();
           

           
            
            $('#nama').change(function() {   
             refreshNama($("#nama option:selected").val()) 
          });

            function refreshNama(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view", {"id": id   
              }, function(data) {
                $("#contoh1").html(data);
                $('#contoh').slideDown();
                $('#contoh1').slideDown();
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
           
           $('#checkbox').hide();
            $('#nama').change(function(data){
              refreshCheckbox($("#nama option:selected").val()) 
             

            });
            function refreshCheckbox(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/checkbox",{"id": id   
              }, function(data) {
                $("#checkbox").html(data);
                $('#checkbox').slideDown();
              $('#peminta').slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            //menampilkan metode uji
            $('#nama').change(function() {   
             refreshUji($("#nama option:selected").val()) 
          });

            function refreshUji(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/uji", {"id": id   
              }, function(data) {
                $("#semua_uji").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            $('#plant').change(function() {   
             refreshLab($("#plant option:selected").val()) 
          });

            function refreshLab(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/lab", {"id": id   
              }, function(data) {
                $("#lab").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            //

            
          
           //edit====================================================
           
           
            
           

            $('#nama_edit').change(function() {   
             refreshLokasi_edit($("#nama_edit option:selected").val()) 
          });

            $('#nama_edit').change(function() {   
             refreshUji_edit($("#nama_edit option:selected").val()) 
          });
           function refreshUji_edit(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view_uji", {"id": id   
              }, function(data) {
                 $("#semua_uji_edit").html(data);
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            
           
           
            $('#nama_edit').change(function(){
              refreshCheckbox_edit($("#nama_edit option:selected").val()) 
             

            });
           
           
          

            
            });
            

          function add(){
            $('#myModal').modal('show');
            $('#form')[0].reset();
            $('#nama').val('');
          }
          function edit(id){
          $('#form1')[0].reset();
         
         $.ajax({
           url : '<?php echo site_url('master/Transaksi/edit') ?>/'+id,
           type : 'GET',
           dataType : 'JSON',
            success:function(data){
              $('[name="ID_TRANSAKSI"]').val(data.ID_TRANSAKSI);
              $('[name="1"]').val(data.KODE_UJI);
              $('[name="2"]').val(data.ID_PLANT);
              
              
              $('[name="4"]').val(data.NO_SPUC_4);
              $('[name="tanggal"]').val(data.TANGGAL);
              $('[name="nama_kelompok1"]').val(data.NAMA_KELOMPOK);
              
              $('[name="jumlah"]').val(data.JUMLAH);
              $('[name="uji"]').val(data.ID_STANDART);
              
              $('[name="kemasan"]').val(data.KEMASAN);
              $('[name="berat"]').val(data.BERAT);
              $('[name="warna"]').val(data.WARNA);
              $('[name="ket_warna"]').val(data.KETERANGAN_WARNA);
              $('[name="asal_contoh"]').val(data.ASAL_CONTOH);
              $('[name="bentuk_contoh"]').val(data.BENTUK_CONTOH);
              $('[name="bentuk"]').val(data.BENTUK);
              $('[name="kondisi"]').val(data.KONDISI);
              
              
              $('[name="tanggal_mulai"]').val(data.TANGGAL_MULAI);
              $('[name="tanggal_selesai"]').val(data.TANGGAL_SELESAI);
              $('[name="no_spk"]').val(data.NO_SPK);
              $('[name="peminta"]').val(data.PEMINTA);
              $('[name="alamat"]').val(data.ALAMAT);
              $('[name="keterangan"]').val(data.KETERANGAN);
              
              
              refreshLokasi_edit($("#nama_edit option:selected").val(),data.ID_CONTOH)
              
              
             
               
              

              $('#myEdit').modal('show');
          }
        });
    }

   
           
            function refreshLokasi_edit(id,contoh) {
              $.post("<?php echo base_url(); ?>master/Transaksi/view_edit", {"id": id   
              }, function(data) {
               
                $("#contoh1_edit").html(data);
                $('#contoh1_edit').val(contoh);
                refreshCheckbox_edit($("#nama_edit option:selected").val(),data.ID_UJI) 
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
              function refreshCheckbox_edit(id,checkbox) {
              $.post("<?php echo base_url(); ?>master/Transaksi/checkbox_edit", {"id": id   
              }, function(data) {
                $("#checkbox_edit").html(data);
                $("#checkbox_edit").val(checkbox);
                
              
              }).fail(function() {
                // Nope
              });
            }
     
          function hapus(id){
            if (confirm("Apakah yakin ingin menghapus?")) {
        $.ajax({
            url:'<?php echo base_url() ?>master/Transaksi/hapus',
            type: 'post',
            data: {id:id},
            success: function () {
                alert('Berhasil Menghapus');
                location.reload();
            },
            error: function () {
                alert('Gagal');
            }
             });
            } 
            
            }
         function baca(id){
    
         
         $.ajax({
           url : '<?php echo base_url() ?>master/Transaksi/detail',
           type : 'GET',
           data: {id:id},
           
            success:function(data){
              
            

              $('#myDetail').modal('show');
          }
        });
    }
        </script>
        <!-- Modal -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-lg">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Tambah</h4>
             </div>
             <div class="modal-body">
                 
                    <div class="ibox">
                        
                        <div class="ibox-content">
                            <form id="form" action="<?php echo base_url() ?>master/Transaksi/tambah" class="wizard-big" method="POST">
                                <h1>Halaman 1</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>NO SPUC *</label>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select class="form-control" required=""  name="1" id="kd_uji" onchange="pilih_eks(this);">
                                                              <option value="">--KODE UJI--</option>
                                                        <?php 
                                                        foreach ($kode->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->KODE_UJI ?>"><?php echo $key->NAMA_UJI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control"  name="2" id="plant">
                                                              
                                                        
                                                              <?php 
                                                              for($a=0;$a<count($plant);$a++)
                                                              {
                                                                ?>
                                                                <option value="<?php echo $plant[$a]['ID_PLANT']; ?>" ><?php echo $plant[$a]['NM_PLANT']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control" name="3" id="lab" disabled="">
                                                             
                                                             <option value=""></option>
                                                             <?php 
                                                              for($a=0;$a<count($datauser);$a++)
                                                              {
                                                                ?>
                                                                <option value="<?php echo $plant[$a]['ID_LAB']; ?>" ><?php echo $plant[$a]['NM_LAB']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                        
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                         <input type="text" name="4"  value="<?php echo date('m.Y') ?>" readonly class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label style="padding-right: 38px;">Tanggal Terima</label>
                                               
                                                  <input type="text" name="tanggal" id="tanggal" class="form-control" style="width: 400px;" required="" placeholder="dd-mm-yyyy" readonly >
                                                
                                            </div>
                                            
                                            <div class="form-group">
                                                <label style="padding-right: 30px;">Nama Kelompok</label>
                                                <select class="form-control" style="width: 400px;"  id="nama" name="nama_kelompok" required="">
                                                      
                                                      <?php 
                                                        foreach ($lihat->result() as $a) {
                                                          ?>
                                                          <option value="<?php echo $a->ID_KELOMPOK ?>"><?php echo $a->NAMA_KELOMPOK ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                    </select>
                                              </div>
                                              <div class="form-group" >
                                                <label style="padding-right: 48px;" id="contoh">Nama Contoh</label>
                                                <select class="form-control" style="width: 400px;"  id="contoh1" name="nama_contoh" required="">
                                                      <option selected="selected"></option>
                                                     
                                                    </select>
                                              </div>
                                              
                                              <div id="checkbox">
                                                 
                                               
                                              </div>
                                        </div>
                                        
                                    </div>
                                  </fieldset>
                  
                                   <h1>Halaman2</h1>
                                  <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            
                                            <div class="form-group">
                                                <label>Jumlah Contoh</label>
                                                <input type="text" name="jumlah" class="form-control required">
                                            </div>
                                            
                                            </div>
                                        </div>
                                       
                                          
                                          <div id="semua_uji">

                                          </div>
                                    
                                </fieldset>

                                <h1>Halaman 3</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                        
                                          <div class="row">
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Kemasan Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="kemasan">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($tampilkan->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->NAMA_KEMASAN ?>"><?php echo $a->NAMA_KEMASAN ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                </select>
                                              </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Berat Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="berat">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($berat->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->ID_BERAT ?>"><?php echo $a->NAMA_BERAT ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                  
                                                </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Warna Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                 
                                                  
                                                  
                                                  <input type="text" name="warna" list="warna" class="form-control"  style="width: 330px;" placeholder="Ketik bila ada warna lain">
                                                  <datalist id="warna">
                                                  <?php 
                                                    foreach ($warna->result() as $a) {
                                                      ?>
                                                      
                                                      <option value="<?php echo $a->NAMA_WARNA ?>"><?php echo $a->NAMA_WARNA ?></option>
                                                      
                                                      <?php
                                                    }
                                                    ?>
                                                    </datalist>
                                                      
                                                
                                                  
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Keterangan Warna</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  
                                                  <select class="form-control"  style="width: 330px;" name="ket_warna">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KET_WARNA ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    
                                                  </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Asal Contoh</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <input type="text" name="asal_contoh" class="form-control" style="width: 330px;">
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Bentuk Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk_contoh">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Wujud Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH_UJI_2 ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Kondisi Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control"   style="width: 330px;" name="kondisi">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KONDISI_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>Tanggal Mulai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_mulai" class="form-control" style="width: 330px;">
                                                  </div>
                                                  
                                                
                                                
                                                   <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label >Tanggal Selesai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_selesai" class="form-control" style="width: 330px;">
                                                  </div>
                                                
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>NO SPK/SPP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                      <input type="text" name="no_spk" class="form-control" style="width: 330px;" >
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                    <label>Nama Peminta</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="peminta">
                                                      <div id="divhtmlpeminta"></div>
                                                    </div>
													                         
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="ALAMAT">
                                                    <label>Alamat</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlalamat"></div>
                                                     
                                                      
                                                    </div>
                                                  
                                                  
													                           <div class="col-md-6" style="margin-bottom: 10px;" id="NPWP">
                                                    <label>NPWP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlidentitas"></div>
                                                      
                                                      
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;" id="penanggung_jawab">
                                                    <label>Penanggung Jawab</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlperson"></div>
                                                        
                                                      
                                                    </div>
                                                    
													                         <div id="keterangan_peminta">
                                                    <div class="col-md-6" >
                                                    <label>Keterangan</label>
                                                  </div>
                                                    <div class="col-md-6">
                                                      <textarea class="form-control" rows="5" name="keterangan" style="width: 330px;"></textarea>
                                                    </div>
                                                  </div>
                                                
                                        </div>
                                      
                                        
                                </fieldset>

                  </form>

                
             
             
             
            
          </div>
          </div>
        </div>
      </div>
    </div>

       
        <!-- Modal EDIT-->
        <div id="myEdit" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">

             <!-- Modal content-->
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Form Update</h4>
             </div>
             <div class="modal-body">
                 
                     <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        
                        <div class="ibox-content">
                            

                            <form id="form1" action="<?php echo base_url() ?>master/Transaksi/update" class="wizard-big" method="POST">
                                <h1>Halaman 1</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <input type="hidden" name="ID_TRANSAKSI">
                                          </div>
                                            <div class="form-group">
                                                <label>NO SPUC *</label>
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select class="form-control" required=""  name="1">
                                                              <option value="">--KODE UJI--</option>
                                                        <?php 
                                                        foreach ($kode->result() as $key ) {
                                                          ?>
                                                          <option value="<?php echo $key->KODE_UJI ?>"><?php echo $key->NAMA_UJI ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                      </select>
                                                    </div>
                                                       <div class="col-md-3">
                                                        <select class="form-control"  name="2" id="plant_edit">
                                                              
                                                       <?php 
                                                              for($a=0;$a<count($plant);$a++)
                                                              {
                                                                ?>
                                                                <option value="<?php echo $plant[$a]['ID_PLANT']; ?>" ><?php echo $plant[$a]['NM_PLANT']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="form-control"   name="KD_LAB" id="lab_edit" >
                                                              
                                                              <?php 
                                                              foreach ($datauser as $key) {
                                                                ?>
                                                                <option value="<?php echo $datauser[0]['ID_LAB']; ?>" ><?php echo $datauser[0]['NM_LAB']; ?></option>
                                                                <?php
                                                              }
                                                              ?>
                                                        
                                                      </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                         <input type="text" name="4"  value="<?php echo date('M.Y') ?>" readonly class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="padding-right: 38px;">Tanggal Terima</label>
                                                <input type="text" name="tanggal" id="tanggal1" class="form-control" style="width: 400px;" required="" readonly>
                                            </div>
                                           
                                            <div class="form-group">
                                                <label style="padding-right: 30px;">Nama Kelompok</label>
                                                <select class="form-control" style="width: 400px;"  id="nama_edit" name="nama_kelompok1">
                                                      
                                                      <?php 
                                                        foreach ($lihat->result() as $a) {
                                                          ?>
                                                          <option value="<?php echo $a->ID_KELOMPOK ?>"><?php echo $a->NAMA_KELOMPOK ?></option>
                                                          <?php
                                                        }
                                                        ?>
                                                    </select>
                                              </div>
                                              <div class="form-group" >
                                                <label style="padding-right: 48px;" id="contoh_edit">Nama Contoh</label>
                                                <select class="form-control" style="width: 400px;"  id="contoh1_edit" name="nama_contoh1">
                                                       <option selected="selected"></option>
                                                     
                                                    </select>
                                              </div>
                                              <div id="checkbox_edit">
                                              </div>
                                        </div>
                                        
                                    </div>
                                  </fieldset>
                  
                                  <h1>Halaman2</h1>
                                  <fieldset>
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <input type="hidden" name="ID_T_UJI">

                                          </div>
                                            <div class="form-group">
                                                <label>Jumlah Contoh</label>
                                                <input type="text" name="jumlah" class="form-control" required="">
                                            </div>
                                            
                                            </div>
                                        </div>
                                        
                                         <div id="semua_uji_edit">
                                              
                                            
                                       
                                        
                                          
                                        
                                      </div>
                                </fieldset>

                                <h1>Halaman 3</h1>
                                <fieldset style="overflow-y: scroll; max-height:100%;">
                                    
                                        
                                          <div class="row">
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Kemasan Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="kemasan">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($tampilkan->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->NAMA_KEMASAN ?>"><?php echo $a->NAMA_KEMASAN ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                </select>
                                              </div>
                                            <div class="col-md-6" style="margin-bottom: 10px;">
                                                <label>Berat Contoh Uji</label>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                <select class="form-control"  style="width: 330px;" name="berat">
                                                  <option></option>
                                                  <?php 
                                                    foreach ($berat->result() as $a) {
                                                      ?>
                                                      <option value="<?php echo $a->NAMA_BERAT ?>"><?php echo $a->NAMA_BERAT ?></option>
                                                      <?php
                                                    }
                                                    ?>
                                                  
                                                </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Warna Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                 
                                                  
                                                  
                                                  <input type="text" name="warna" list="warna" class="form-control"  style="width: 330px;" placeholder="Ketik bila ada warna lain">
                                                  <datalist id="warna">
                                                  <?php 
                                                    foreach ($warna->result() as $a) {
                                                      ?>
                                                      
                                                      <option value="<?php echo $a->NAMA_WARNA ?>"><?php echo $a->NAMA_WARNA ?></option>
                                                      
                                                      <?php
                                                    }
                                                    ?>
                                                    </datalist>
                                                      
                                                
                                                  
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Keterangan Warna</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  
                                                  <select class="form-control"  style="width: 330px;" name="ket_warna">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KET_WARNA ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                    
                                                  </select>
                                              </div>
                                              <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Asal Contoh</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <input type="text" name="asal_contoh" class="form-control" style="width: 330px;">
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Bentuk Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk_contoh">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Wujud Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control" style="width: 330px;" name="bentuk">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->BENTUK_CONTOH_UJI_2 ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                  <label>Kondisi Contoh Uji</label>
                                                </div>
                                                <div class="col-md-6" style="margin-bottom: 10px;">
                                                   <select class="form-control"   style="width: 330px;" name="kondisi">
                                                    <option></option>
                                                    <?php 
                                                      foreach ($all->result() as $a) {
                                                        ?>
                                                        <option value="<?php echo $a->ID_CONTOH_UJI ?>"><?php echo $a->KONDISI_CONTOH ?></option>
                                                        <?php
                                                      }
                                                      ?>
                                                  </select>
                                                </div>
                                                
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>Tanggal Mulai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_mulai" class="form-control" style="width: 330px;">
                                                  </div>
                                                  
                                                
                                                
                                                   <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label >Tanggal Selesai</label>
                                                  </div>
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <input type="date" name="tanggal_selesai" class="form-control" style="width: 330px;">
                                                  </div>
                                                
                                                <div id="peminta_edit">
                                                  <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                    <label>NO SPK/SPP</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;margin-top: 10px;">
                                                      <input type="text" name="no_spk" class="form-control" style="width: 330px;" >
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                    <label>Nama Peminta</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <div id="divhtmlpeminta"></div>
                                                    </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                    <label>Alamat</label>
                                                  </div>
                                                    <div class="col-md-6" style="margin-bottom: 10px;">
                                                      <input type="text" name="alamat" class="form-control"  style="width: 330px;">
                                                    </div>
                                                    <div class="col-md-6">
                                                    <label>Keterangan</label>
                                                  </div>
                                                    <div class="col-md-6">
                                                      <textarea class="form-control" rows="5" name="keterangan" style="width: 330px;"></textarea>
                                                    </div>
                                                </div>
                                        </div>
                                            
                                        
                                </fieldset>
                
             </div>
             
             </form>
            
          </div>
          </div>
        </div>

        
       
       
<script type="text/javascript">
$(function(){
  $('#tanggal').datepicker({
    format:'dd-mm-yyyy',
    autoclose: true,
      todayHighlight: true
  });
 $('#tanggal1').datepicker({
    format:'dd-mm-yyyy',
    autoclose: true
  });

    $("#ALAMAT").hide();
    $("#NPWP").hide();
    $("#penanggung_jawab").hide();
    $("#keterangan_peminta").hide();

  $('#divhtmlpeminta').change(function() {   
               refreshPeminta($("#divhtmlpeminta option:selected").val()) 
          });
            
            function refreshPeminta(id) {

              $.post("<?php echo base_url(); ?>master/Transaksi/peminta3", {"id": id   
              }, function(data) {
                
                $("#divhtmlalamat").html(data);
                $("#ALAMAT").slideDown();
                
                
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }

       //===================================     
  $('#divhtmlpeminta').change(function() {   
             refreshPeminta2($("#divhtmlpeminta option:selected").val()) 
          });

            function refreshPeminta2(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/peminta4", {"id": id   
              }, function(data) {
               
                $("#divhtmlidentitas").html(data);
                $("#NPWP").slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
  $('#divhtmlpeminta').change(function() {   
             refreshPenanggung($("#divhtmlpeminta option:selected").val()) 
          });

            function refreshPenanggung(id) {
              $.post("<?php echo base_url(); ?>master/Transaksi/penanggung", {"id": id   
              }, function(data) {
                $("#divhtmlperson").html(data);

                $("#penanggung_jawab").slideDown();
                $("#keterangan_peminta").slideDown();
              }).fail(function() {
                // Nope
              }).always(function() {
                $(".selectpicker").selectpicker("refresh");
              });
            }
            //==============================
            
  
});  

function pilih_eks(val)
{
	kodeuji = val.value;
	if(kodeuji=='ISO'){
		$('#namapeminta').val(kodeuji);
		$.ajax({
           url : '<?php echo base_url() ?>master/Transaksi/peminta',
           type : 'GET',
		   dataType : 'JSON',
           data: {'status':kodeuji},
            success:function(data){
			   htmlpeminta = "<select id='namapeminta' name='namapeminta' class='form-control selectpicker' data-live-search='true'>";
         htmlpeminta += "<option value=''>--Pilih--</option>";
			   for(a=0;a<data.length;a++)
			   {
          
					htmlpeminta += "<option value='"+data[a].ID_PELANGGAN+"'>"+data[a].NAMA_INSTANSI+"</option>";
			   }
			   htmlpeminta += "</select>";
			   $('#divhtmlpeminta').html(htmlpeminta); 
         $('#namapeminta').selectpicker('refresh');
         
          }

		});
	}
	else {
		$('#namapeminta').val(kodeuji);
		$.ajax({
           url : '<?php echo base_url() ?>master/Transaksi/peminta2',
           type : 'GET',
		   dataType : 'JSON',
           data: {'status':kodeuji},
            success:function(data){
			   htmlpeminta = "<select id='namapeminta' name='namapeminta' class='form-control selectpicker' data-live-search='true'>";
         htmlpeminta += "<option value=''>--Pilih--</option>";
			   for(a=0;a<data.length;a++)
			   {
          
					htmlpeminta += "<option value='"+data[a].mk_nopeg+"'>"+data[a].mk_nama+"</option>";
			   }
			   htmlpeminta += "</select>";
			   $('#divhtmlpeminta').html(htmlpeminta); 
         $('#namapeminta').selectpicker('refresh');
          }
		});
	}
}

</script>

      