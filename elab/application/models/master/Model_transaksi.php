<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transaksi extends CI_Model {

    var $table = 'ELAB_T_BPPUC A, ELAB_M_CONTOH B, ELAB_M_LABORATORIUM C, M_PLANT D';
    var $column_order = array(); 
    var $column_search = array('B.NAMA_CONTOH','A.TANGGAL','A.NO_BAPPUC');
    var $order = array('B.NAMA_CONTOH' => 'asc',
					   'A.TANGGAL'=>'asc',
					   'A.NO_BAPPUC'=>'asc'); 
 
    public function __construct()
    {
        parent::__construct(); 
		$this->db = $this->load->database('default', TRUE);
		$this->hris = $this->load->database('hris', TRUE);
    }
 
    private function _get_datatables_query()
    {
		$this->db->select ('*'); 	
		$this->db->from('ELAB_T_BPPUC A');
		
		$this->db->join('ELAB_M_CONTOH B','B.ID_CONTOH=A.ID_CONTOH');
		$this->db->join('ELAB_M_LABORATORIUM C','C.ID_LAB=A.KODE_LAB');
		$this->db->join('M_PLANT D','D.ID_PLANT=A.ID_PLANT');
		$this->db->where('A.STATUS','a0');
		$this->db->where('A.IS_DELETE','0');
		$this->db->order_by('A.ID_TRANSAKSI', 'DESC');
		//$query = $this->db->get();

        $i = 0;
     
        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->select ('*'); 	
		$this->db->from('ELAB_T_BPPUC A');
		
		$this->db->join('ELAB_M_CONTOH B','B.ID_CONTOH=A.ID_CONTOH');
		$this->db->join('ELAB_M_LABORATORIUM C','C.ID_LAB=A.KODE_LAB');
		$this->db->join('M_PLANT D','D.ID_PLANT=A.ID_PLANT');
		$this->db->where('A.STATUS','a0');
		$this->db->order_by('A.ID_TRANSAKSI', 'DESC');
        return $this->db->count_all_results();
    }
	

	public function tampil(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KEMASAN order by KODE_KEMASAN ASC');
		return $sql;
	}
	public function t_uji(){
		$sql = $this->db->query('SELECT * FROM ELAB_T_UJI');
		return $sql;
	}
	public function lihat(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KELOMPOK');
		return $sql;
	}
	public function view(){
		$sql = $this->db->query("SELECT * FROM ELAB_T_BPPUC A 
						        JOIN ELAB_M_CONTOH B ON B.ID_CONTOH=A.ID_CONTOH
						        JOIN ELAB_M_LABORATORIUM C ON C.ID_LAB = A.KODE_LAB
						        JOIN M_PLANT D ON D.ID_PLANT=A.ID_PLANT where status='a0' order by ID_TRANSAKSI DESC");
		return $sql;
	}
	public function view_2(){
		$sql = $this->db->query('SELECT * FROM ELAB_T_BPPUC A 
								
                				
                				
                				JOIN ELAB_M_CONTOH E ON E.ID_CONTOH=A.ID_CONTOH
                				JOIN ELAB_WARNA F ON A.WARNA=F.NAMA_WARNA 
                				JOIN ELAB_SEMUA_CONTOH_UJI G ON A.KETERANGAN_WARNA = G.ID_CONTOH_UJI OR A.BENTUK_CONTOH = G.ID_CONTOH_UJI OR A.BENTUK = G.ID_CONTOH_UJI OR A.KONDISI = G.ID_CONTOH_UJI
                				JOIN ELAB_CONTOH_BERAT H ON A.BERAT = H.ID_BERAT');
		return $sql;
	}
	 
	public function berat(){
		$sql = $this->db->query('SELECT * FROM ELAB_CONTOH_BERAT');
		return $sql;
	}
	public function box_view(){
		$sql = $this->db->query('SELECT * FROM ELAB_T_BPPUC 
								
                				');
		return $sql;
	}
	
	
	public function all(){
		$sql = $this->db->query('SELECT * FROM ELAB_SEMUA_CONTOH_UJI');
		return $sql;
	}
	public function trans(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_TRANSAKSI');
		return $sql;
	}
	public function kode(){
		$sql = $this->db->query('SELECT * FROM ELAB_KODE_UJI');
		return $sql;
	}
	public function kategori(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KATEGORI');
		return $sql;
	}
	public function plant(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_LABORATORIUM A
								JOIN M_PLANT B ON B.ID_PLANT = A.ID_PLANT');
		return $sql;
	}
	public function standart(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_STANDART_UJI');
		return $sql;
	}
	public function warna(){
		$sql = $this->db->query('SELECT * FROM ELAB_WARNA');
		return $sql;
	}
	public function contoh(){
		$sql = $this->db->query('SELECT A.ID_KELOMPOK,A.NAMA_KELOMPOK,B.ID_CONTOH,B.ID_KELOMPOK FROM ELAB_M_KELOMPOK A,ELAB_M_CONTOH B WHERE A.ID_KELOMPOK=B.ID_KELOMPOK');
		return $sql;
	}
	public function lab(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_LABORATORIUM A
								JOIN M_PLANT B ON A.ID_PLANT = B.ID_PLANT');
		return $sql;
	}

	public function link($id){

        $this->db->where('ID_KELOMPOK',$id);
        $result = $this->db->get('ELAB_M_CONTOH');
        if ($result->num_rows() > 0) {
        	return $result->result_array();
        }
	}
	
	public function uji($id){
		
        $sql = $this->db->query("SELECT * FROM ELAB_M_KELOMPOK A 
								 JOIN ELAB_M_KATEGORI B ON A.ID_KELOMPOK = B.ID_KELOMPOK
								WHERE A.ID_KELOMPOK = '{$id}'");
        return $sql->result_array();
	}
	public function standart_uji($id){
		
        $sql = $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI A
                JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI
                WHERE A.ID_KATEGORI = '{$id}' 
                order by ID_STANDART");
        return $sql->result_array();
	}
	public function tombol($id){
		
        $sql = $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI A
                JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI
                WHERE A.ID_STANDART = '{$id}'
                  ");
        return $sql->result_array();
	}
	
	public function uji_edit($id){
		
        $sql = $this->db->query("SELECT * FROM ELAB_M_KELOMPOK A 
								 JOIN ELAB_M_JENIS B ON A.ID_KELOMPOK = B.ID_KELOMPOK
								WHERE A.ID_KELOMPOK = '{$id}'");
        return $sql->result_array();
	}
	public function standart_uji_edit($id){
		
        $sql = $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI A
								JOIN ELAB_M_JENIS B ON A.ID_JENIS = B.ID_JENIS
								WHERE A.ID_JENIS = '{$id}'");
        return $sql->result_array();
	}
	public function link_edit($id){

        $this->db->where('ID_KELOMPOK',$id);
        $result = $this->db->get('ELAB_M_CONTOH');
        if ($result->num_rows() > 0) {
        	return $result->result_array();
        }
	}

	
	public function box($id,$Kp){

        $sql = $this->db->query("SELECT * FROM ELAB_M_KELOMPOK A 
								 JOIN ELAB_M_KATEGORI B ON A.ID_KELOMPOK = B.ID_KELOMPOK
								 JOIN ELAB_M_UJI C ON B.ID_KATEGORI = C.ID_KATEGORI
                				WHERE A.ID_KELOMPOK='{$id}' AND C.ID_KATEGORI ='{$Kp}'");
        return $sql->result_array();
    }
    
	public function Kbox($id){

        $sql = $this->db->query("SELECT distinct C.ID_KATEGORI,C.NM_KATEGORI FROM ELAB_T_CONTOH A 
        						JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI 
                				JOIN ELAB_M_KATEGORI C ON B.ID_KATEGORI = C.ID_KATEGORI WHERE A.ID_KELOMPOK ='{$id}'
								");
        return $sql->result_array();
    }

    
   

    public function box_e($id){

        $sql = $this->db->query("SELECT * FROM ELAB_M_UJI
                                where id_kategori = '{$id}'");
        // $sql = $this->db->query("SELECT * FROM ELAB_T_CONTOH A 
        						// JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI 
                				// JOIN ELAB_M_KATEGORI D ON B.ID_KATEGORI = D.ID_KATEGORI
                				// WHEREs A.ID_KELOMPOK='{$id}' AND D.ID_KATEGORI ='{$Kp}'");
        return $sql->result_array();
    }

	public function Kbox_e($id){

        $sql = $this->db->query("SELECT distinct C.ID_KATEGORI,C.NM_KATEGORI FROM ELAB_T_CONTOH A 
        						JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI 
                				JOIN ELAB_M_KATEGORI C ON B.ID_KATEGORI = C.ID_KATEGORI WHERE A.ID_KELOMPOK ='{$id}'");
        return $sql->result_array();
    }


    

    public function cek_tuji($id,$Kp){

        $sql = $this->db->query("SELECT *
									FROM ELAB_T_UJI
								WHERE ID_TRANSAKSI ='{$id}' AND ID_STANDART='{$Kp}'");
        return $sql->row_array();
    }
    

    public function Jbox_edit($id,$Kp){

        $sql = $this->db->query("SELECT *
									FROM ELAB_T_BOX
								WHERE ID_TRANSAKSI ='{$id}' AND ID_UJI='{$Kp}'");
        return $sql->row_array();
    }
     
    
    public function plant_lab($id){
    	$sql = $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM where ID_LAB ='{$id}'");
    	return $sql->result_array();
    }
    public function plant_lab_edit($id){
    	$sql = $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM where ID_LAB ='{$id}'");
    	return $sql->result_array();
    }
    public function cek_standart($id){
    	$sql = $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI where ID_LAB ='{$id}'");
    	return $sql->result_array();
    }

    
	public function add_box($data)
	{
		return $this->db->insert("ELAB_T_BOX",$data);
	}
	public function add_warna($data)
	{
		return $this->db->insert("ELAB_WARNA",$data);
	}
	public function add_uji($data)
	{
		return $this->db->insert("ELAB_T_UJI",$data);
	}
	public function add($data)
	{
		return $this->db->insert("ELAB_T_BPPUC",$data);
	}
	
	public function hapusData($id){
        return $this->db->query("DELETE FROM  ELAB_T_BPPUC WHERE ID_TRANSAKSI={$id} "); 
	}
	public function editData($id)
	{
		 
		$this->db->select ( '*' ); 
		$this->db->from('ELAB_T_BPPUC A');
		$this->db->join('ELAB_M_CONTOH B','B.ID_CONTOH=A.ID_CONTOH');
		$this->db->join('ELAB_M_LABORATORIUM C','C.ID_LAB=A.KODE_LAB');
		
		$this->db->where('A.ID_TRANSAKSI',$id);
		$query = $this->db->get();

		return $query->row();

    
	}

	public function tampilkanSesuai($tabel,$kolom,$id) {
		$this->db->where($kolom, $id);
        $q = $this->db->get($tabel);
        return $q;
    }

    public function ambilId($tabel){
    	$this->db->limit(2);
    	$this->db->order_by("ID_TRANSAKSI","DESC");
    	$q = $this->db->get($tabel);
    	return $q;
    }
    
    public function ambilKat($tabel, $id){
    	$q = $this->db->query("select * from {$tabel} where ID_STANDART = '{$id}'");
    	return $q;
    }
    
    public function ambilKatuji($trans, $id){
    	$q = $this->db->query("select * from ELAB_M_UJI A
                join ELAB_T_UJI B ON A.ID_KATEGORI = B.ID_KATEGORI 
                where ID_UJI = {$id} AND ID_TRANSAKSI = {$trans}");
    	return $q;
    }
    public function ambilHarga($id_uji, $id_standart, $lab){
    	$q = $this->db->query("select * from elab_m_biaya where id_uji = {$id_uji} and standart = {$id_standart} and lab = {$lab}");
    	return $q;
    }
    
   

	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	public function updateData_box($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	public function delete_box($id){ 
		$this->db->query("delete from ELAB_T_BOX WHERE ID_TRANSAKSI = {$id}");
	}
	public function delete_uji($id){ 
		$this->db->query("delete from ELAB_T_UJI WHERE ID_TRANSAKSI = {$id}");
	}
	//public function updateData_uji($where, $data, $table){
		//$this->db->where($where);
		//$this->db->update($table, $data);
	//}
	
	public function viewData($id)
	{
		
		$sql = $this->db->query("SELECT * FROM ELAB_T_BPPUC A 
								JOIN ELAB_T_BOX B ON B.ID_CONTOH = A.ID_CONTOH
                				
                				
                				JOIN ELAB_M_CONTOH E ON E.ID_CONTOH=A.ID_CONTOH
                				JOIN ELAB_WARNA F ON A.WARNA=F.NAMA_WARNA 
                				JOIN ELAB_SEMUA_CONTOH_UJI G ON A.KETERANGAN_WARNA = G.ID_CONTOH_UJI OR A.BENTUK_CONTOH = G.ID_CONTOH_UJI OR A.BENTUK = G.ID_CONTOH_UJI OR A.KONDISI = G.ID_CONTOH_UJI
                				JOIN ELAB_CONTOH_BERAT H ON A.BERAT = H.ID_BERAT
                				WHERE A.ID_TRANSAKSI='{$id}'");
		return $sql->result_array();
		
	}
	
	public function view_peminta(){
		$sql = $this->db->query("SELECT distinct NAMA_INSTANSI,ID_PELANGGAN,ALAMAT,NPWP FROM ELAB_M_PELANGGAN");
		return $sql->result_array();
	}
	public function view_peminta2($filter){ 
		$sql = $this->hris->query("SELECT
	k.mk_nopeg,
	k.mk_nama,
	k.muk_kode,
	uk.muk_nama,
	k.company
FROM
	m_karyawan k
	LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode
     where LOWER(k.mk_nopeg) LIKE '%{$filter}%' OR LOWER(k.mk_nama) LIKE '%{$filter}%'
     limit 10
    ");
		return $sql->result_array();
	}
	public function view_peminta3($id){
		$sql = $this->db->query("SELECT distinct NAMA_INSTANSI,ID_PELANGGAN,ALAMAT,NPWP FROM ELAB_M_PELANGGAN WHERE ID_PELANGGAN='{$id}'");
		return $sql->result_array();
	}
	public function view_peminta4($id){
		$sql = $this->db->query("SELECT distinct NAMA_INSTANSI,ID_PELANGGAN,ALAMAT,NPWP FROM ELAB_M_PELANGGAN WHERE ID_PELANGGAN='{$id}'");
		return $sql->result_array();
	}
	public function penanggung($id){
		$sql = $this->db->query("SELECT distinct NAMA_INSTANSI,ID_PELANGGAN,ALAMAT,NPWP,PENANGGUNG_JAWAB FROM ELAB_M_PELANGGAN WHERE ID_PELANGGAN='{$id}'");
		return $sql->result_array();
	}
	public function view_peminta5($id){
		$db2 = $this->load->database('db_kedua', TRUE);
		$sql = $db2->query("SELECT
	k.mk_nopeg,
	k.mk_nama,
	k.muk_kode,
	uk.muk_nama,
	k.company
FROM
	m_karyawan k
	LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode WHERE mk_nopeg='{$id}'");
		return $sql->result_array();
	}
	public function view_peminta6($id){
		$db2 = $this->load->database('db_kedua', TRUE);
		$sql = $db2->query("SELECT
	k.mk_nopeg,
	k.mk_nama,
	k.muk_kode,
	uk.muk_nama,
	k.company
FROM
	m_karyawan k
	LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode WHERE mk_nopeg='{$id}'");
		return $sql->result_array();
	}
	public function ambilLab($id){
		$sql = $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM A
								JOIN M_PLANT B ON A.ID_PLANT = B.ID_PLANT WHERE B.ID_PLANT='{$id}'");
		return $sql->result_array();
	}
	public function ambilLab_e($id){
		$sql = $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM A
								JOIN M_PLANT B ON A.ID_PLANT = B.ID_PLANT WHERE B.ID_PLANT='{$id}'");
		return $sql->result_array();
	}
    
	public function get_lab($id){
		$sql = $this->db->query("select B.ID_PLANT, B.NM_PLANT from ELAB_M_LABORATORIUM A
        JOIN M_PLANT B ON A.ID_PLANT = B.ID_PLANT 
        where A.ID_LAB in ({$id})	GROUP BY B.ID_PLANT,
	B.NM_PLANT ");
		return $sql->result_array();
	}
	public function pemintaInternal($id){
        if($id==''){
            $nopeg = " ";
        }else{
            $nopeg = " where LOWER(k.mk_nopeg) = {$id}";
        }
		$sql = $this->hris->query("SELECT
            k.mk_nopeg,
            k.mk_nama,
            k.muk_kode,
            uk.muk_nama,
            k.company
        FROM 
            m_karyawan k
            LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode
            {$nopeg}
            limit 10
            ");
		return $sql->row_array();
	}
	
	public function getlabplant($id){
		$sql = $this->db->query("select * from elab_m_laboratorium a
        join m_plant b on a.id_plant = b.id_plant
        where a.id_plant = {$id}");
		return $sql->row_array();
	}
	public function cekTo($role, $lab){
		$sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_LAB = '{$lab}' 
            GROUP BY A.ID_USER, EMAIL	
            ORDER BY
                A.ID_USER
");
		return $sql->result_array();
	}
    
    // SELECT A.ID_USER, A.ID_LAB, A.ID_ROLE, FULLNAME, EMAIL  FROM ELAB_M_USERLAB A
// JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER
// WHERE A.ID_ROLE = '2' AND A.ID_LAB = '13'
// -- AND A.ID_KATEGORI = '1'
// GROUP BY A.ID_USER, A.ID_LAB, A.ID_ROLE, FULLNAME, EMAIL
// ORDER BY A.ID_USER
}