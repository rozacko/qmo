<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_m_kemasan extends CI_Model {

	public function add($data)
	{
		return $this->db->insert("ELAB_M_KEMASAN",$data);
	}
	public function hapusData($id){
		return $this->db->where('ID_KEMASAN',$id)->delete('ELAB_M_KEMASAN');

		// return $this->db->result();

	}
	public function editData($id)
	{
		$this->db->from('ELAB_M_KEMASAN');
		$this->db->where('ID_KEMASAN',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$data = $this->db->update($table, $data);
		return $data;
	}

	public function tampil(){
		$sql = $this->db->query('SELECT rownum AS NO,KODE_KEMASAN,NAMA_KEMASAN, ID_KEMASAN FROM ELAB_M_KEMASAN order by KODE_KEMASAN ASC');
		return $sql->result_array();
	}
}
