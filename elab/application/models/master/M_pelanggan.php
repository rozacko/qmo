<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pelanggan extends CI_Model
{
    var $table = 'ELAB_M_PELANGGAN';
    var $column_order = array();
    var $column_search = array('NAMA_INSTANSI', 'NPWP', 'ALAMAT', 'TELEPON', 'CP', 'PENANGGUNG_JAWAB', 'EMAIL');
    var $order = array('NAMA_INSTANSI' => 'asc',
        'NPWP' => 'asc',
        'ALAMAT' => 'asc',
        'TELEPON' => 'asc',
        'CP' => 'asc',
        'PENANGGUNG_JAWAB' => 'asc',
        'EMAIL' => 'asc');

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function new_get_datatables()
    {
        $this->db->select('rownum AS NO,ID_PELANGGAN, NAMA_INSTANSI, NPWP, ALAMAT, TELEPON, CP, PENANGGUNG_JAWAB, EMAIL');
        $this->db->from($this->table);
        $query = $this->db->get();
        // $query = $this->db->from($this->table)->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function add($data)
    {
        return $this->db->insert("ELAB_M_PELANGGAN", $data);
    }

    public function hapus($id)
    {
        $this->db->where('ID_PELANGGAN', $id);
        $this->db->delete('ELAB_M_PELANGGAN');
    }

    public function get_id($id)
    {
        $this->db->from('ELAB_M_PELANGGAN');
        $this->db->where('ID_PELANGGAN', $id);
        $query = $this->db->get();

        return $query->row();
    }

    public function updateData($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    public function tampil()
    {
        $sql = $this->db->query('SELECT * FROM ELAB_M_PELANGGAN');
        return $sql;
    }

    public function get_excel()
    {
        $sql = $this->db->query('SELECT * FROM ELAB_M_PELANGGAN');
        return $sql->result_array();
    }
}
