<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_m_uji_limbah extends CI_Model {

	public function add($data)
	{
		return $this->db->insert("ELAB_M_UJI_LIMBAH",$data);
	}
	public function hapusData($id){
		$this->db->where('ID_UJI',$id);
		$this->db->delete('ELAB_M_UJI_LIMBAH');
	}
	public function editData($id)
	{
		$this->db->from('ELAB_M_UJI_LIMBAH');
		$this->db->where('ID_UJI',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function tampil(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_UJI_LIMBAH');
		return $sql;
	}
}