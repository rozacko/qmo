<?php

class M_role extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
	
 	public function get_data()
 	{  
		return $this->db->query("SELECT * FROM ELAB_M_ROLE")->result_array();;
	}
	
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_ROLE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id_role, $data) {
		return $this->db->query("UPDATE ELAB_M_ROLE SET ".implode(",", $data)." WHERE id_role={$id_role}");
	}
	
	public function hapus($id_role) {
		$q = $this->db->query("DELETE FROM ELAB_M_SUBROLE WHERE id_role={$id_role}");
		return $q && $this->db->query("DELETE FROM ELAB_M_ROLE WHERE id_role={$id_role}");
	}
	
	public function getHalaman() {
		return $this->db->query("SELECT * FROM ELAB_M_MENU ORDER BY posisi_menu ASC, urutan_menu ASC")->result_array();
	}
	
	public function getSubrole($id_role) {
		return $this->db->query("SELECT * FROM ELAB_M_SUBROLE WHERE id_role={$id_role}")->result_array();
	}
	
	public function deleteSubrole($id_role) {
		return $this->db->query("DELETE FROM ELAB_M_SUBROLE WHERE id_role={$id_role}");
	}
	
	public function addSubrole($tabel, $data) {
		return  $this->db->insert_batch($tabel, $data);
		// return $this->db->query("INSERT INTO ELAB_M_SUBROLE VALUES ".implode(", ", $akses));
	}
	
}
?>