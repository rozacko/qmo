<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_biaya extends CI_Model {

	var $table = 'ELAB_M_BIAYA A ,ELAB_M_UJI D,ELAB_M_LABORATORIUM C,ELAB_M_STANDART_UJI B,ELAB_M_KATEGORI E';
    var $column_order = array(); 
    var $column_search = array('D.NAMA_UJI','C.NM_LAB','B.NAMA_STANDART','E.NM_KATEGORI','A.BIAYA');
    var $order = array('D.NAMA_UJI' => 'asc',
						'C.NM_LAB'=>'asc',
						'B.NAMA_STANDART'=>'asc',
						'E.NM_KATEGORI'=>'asc',
						'A.BIAYA'=>'asc'); 
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	 private function _get_datatables_query()
    {
    	
		$this->db->select ('*'); 	
		$this->db->from('ELAB_M_BIAYA A');
		
		$this->db->join('ELAB_M_STANDART_UJI B','A.STANDART = B.ID_STANDART');
		$this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');
		$this->db->join('ELAB_M_UJI D','A.ID_UJI = D.ID_UJI');
		$this->db->join('ELAB_M_KATEGORI E','E.ID_KATEGORI = A.ID_KATEGORI');
		
		
		

        $i = 0;
     
        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->db->select ('*');    
        $this->db->from('ELAB_M_BIAYA A');
        
        $this->db->join('ELAB_M_STANDART_UJI B','A.STANDART = B.ID_STANDART');
        $this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');
        $this->db->join('ELAB_M_UJI D','A.ID_UJI = D.ID_UJI');
        $this->db->join('ELAB_M_KATEGORI E','E.ID_KATEGORI = A.ID_KATEGORI');

        return $this->db->get()->result_array();
    }
 
    function get_datatabless()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->select ('*');    
        $this->db->from('ELAB_M_BIAYA A');
        
        $this->db->join('ELAB_M_STANDART_UJI B','A.STANDART = B.ID_STANDART');
        $this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');
        $this->db->join('ELAB_M_UJI D','A.ID_UJI = D.ID_UJI');
        $this->db->join('ELAB_M_KATEGORI E','E.ID_KATEGORI = A.ID_KATEGORI');
        return $this->db->count_all_results();
    }
	
	public function add($data)
	{
		return $this->db->insert("ELAB_M_BIAYA",$data);
	}
	public function hapus($id){
		$this->db->where('ID_BIAYA',$id);
		$this->db->delete('ELAB_M_BIAYA');
	}
	public function get_id($id)
	{
		$this->db->from('ELAB_M_BIAYA');
		
		$this->db->where('ID_BIAYA',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function tampil(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_UJI');
		return $sql;
	}
	public function standart(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_STANDART_UJI C');
		return $sql;
	}
	public function lab(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_LABORATORIUM');
		return $sql;
	}
	public function kategori(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KATEGORI');
		return $sql;
	}
	public function all(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_BIAYA A
              JOIN ELAB_M_STANDART_UJI B ON A.STANDART = B.ID_STANDART
              JOIN ELAB_M_LABORATORIUM C ON A.LAB = C.ID_LAB
              JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
              JOIN ELAB_M_KATEGORI E ON E.ID_KATEGORI = A.ID_KATEGORI');
		return $sql;
	}
	public function get_excel(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_BIAYA A
              JOIN ELAB_M_STANDART_UJI B ON A.STANDART = B.ID_STANDART
              JOIN ELAB_M_LABORATORIUM C ON A.LAB = C.ID_LAB
              JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
              JOIN ELAB_M_KATEGORI E ON E.ID_KATEGORI = A.ID_KATEGORI');
		return $sql->result_array();
	}
}