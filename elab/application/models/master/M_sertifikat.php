<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sertifikat extends CI_Model {


	var $table = 'ELAB_M_SERTIFIKAT A ,ELAB_M_KELOMPOK B,ELAB_M_LABORATORIUM C';
    var $column_order = array();
    var $column_search = array('B.NAMA_KELOMPOK','C.NM_LAB','A.N_SERT','A.M_BERLAKU','A.STATUS');
    var $order = array('B.NAMA_KELOMPOK' => 'asc',
						'C.NM_LAB'=>'asc',
						'A.N_SERT'=>'asc',
						'A.M_BERLAKU'=>'asc',
						'A.STATUS'=>'asc');
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	 private function _get_datatables_query()
    {

		$this->db->select ('*');
		$this->db->from('ELAB_M_SERTIFIKAT A');

		$this->db->join('ELAB_M_KELOMPOK B','A.KELOMPOK = B.ID_KELOMPOK');
		$this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');

        $i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function get_list()
    {
        $this->db->select ('rownum AS NO, ID_SERTIFIKAT, NAMA_KELOMPOK, NM_LAB, N_SERT, M_BERLAKU, STATUS');    
        $this->db->from('ELAB_M_SERTIFIKAT A');

        $this->db->join('ELAB_M_KELOMPOK B','A.KELOMPOK = B.ID_KELOMPOK');
        $this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');

        return $this->db->get()->result_array();
    }

    public function count_all()
    {
        $this->db->select ('*');
		$this->db->from('ELAB_M_SERTIFIKAT A');

		$this->db->join('ELAB_M_KELOMPOK B','A.KELOMPOK = B.ID_KELOMPOK');
		$this->db->join('ELAB_M_LABORATORIUM C','A.LAB = C.ID_LAB');
        return $this->db->count_all_results();
    }


	public function add($data)
	{
		return $this->db->insert("ELAB_M_SERTIFIKAT",$data);
	}
	public function hapus($id){
		$this->db->where('ID_SERTIFIKAT',$id);
		$this->db->delete('ELAB_M_SERTIFIKAT');
	}
	public function get_id($id)
	{
		$this->db->from('ELAB_M_SERTIFIKAT');
		$this->db->where('ID_SERTIFIKAT',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function tampil(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_SERTIFIKAT A
              JOIN ELAB_M_KELOMPOK B ON A.KELOMPOK = B.ID_KELOMPOK
              JOIN ELAB_M_LABORATORIUM C ON A.LAB = C.ID_LAB');
		return $sql;
	}
	public function get_excel(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_SERTIFIKAT A
              JOIN ELAB_M_KELOMPOK B ON A.KELOMPOK = B.ID_KELOMPOK
              JOIN ELAB_M_LABORATORIUM C ON A.LAB = C.ID_LAB');
		return $sql->result_array();
	}
	public function kelompok(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KELOMPOK');
		return $sql;
	}
	public function lab(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_LABORATORIUM');
		return $sql;
	}
}
