<?php

class M_item_uji extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT
								* 
							FROM
								ELAB_M_ITEMUJI A
								LEFT JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
								-- LEFT JOIN ELAB_M_COMPONENT D ON D.ID_COMPONENT = B.ID_COMPONENT
								LEFT JOIN ELAB_M_STANDART_UJI C ON A.ID_STANDART = C.ID_STANDART
                                order by id_item
                ")->result_array();
	}
    
    public function get_data_excel() {
		return $this->db->query("SELECT
								* 
							FROM
								ELAB_M_ITEMUJI A
								LEFT JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
								-- LEFT JOIN ELAB_M_COMPONENT D ON D.ID_COMPONENT = B.ID_COMPONENT
								LEFT JOIN ELAB_M_STANDART_UJI C ON A.ID_STANDART = C.ID_STANDART
                                order by id_item
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_ITEMUJI (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_ITEMUJI SET ".implode(",", $data)." WHERE ID_ITEM={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_ITEMUJI WHERE ID_ITEM={$id}");
	}
    
    
	public function getUji() {
		return $this->db->query("
								SELECT * FROM ELAB_M_UJI A
								LEFT JOIN ELAB_M_COMPONENT B ON A.ID_COMPONENT = B.ID_COMPONENT
								")->result_array();
	}
	public function getStandart() {
		return $this->db->query("SELECT * FROM ELAB_M_STANDART_UJI")->result_array();
	}

}
?>