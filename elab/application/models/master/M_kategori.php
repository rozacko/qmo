<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_KATEGORI extends CI_Model {

	var $table = 'ELAB_M_KATEGORI';
    var $column_order = array();
    var $column_search = array('NM_KATEGORI');
    var $order = array('NM_KATEGORI' => 'asc');
	public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
	 private function _get_datatables_query()
    {


		$this->db->from($this->table);
		$i = 0;

        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getdata()
    {		$this->db->select('rownum AS NO, ID_KATEGORI, NM_KATEGORI');
				$this->db->from($this->table);
        // $this->db->from($this->table);
        return $this->db->get()->result_array();
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function add($data)
	{
		return $this->db->insert("ELAB_M_KATEGORI",$data);
	}
	public function hapus($id){
		$this->db->where('ID_KATEGORI',$id);
		$this->db->delete('ELAB_M_KATEGORI');
	}
	public function get_id($id)
	{
		$this->db->from('ELAB_M_KATEGORI');

		$this->db->where('ID_KATEGORI',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}




	public function get_excel(){
		$sql = $this->db->query('SELECT * FROM ELAB_M_KATEGORI');
		return $sql->result_array();
	}
}
