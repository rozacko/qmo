<?php

class D_bppuc extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->hris = $this->load->database('hris', TRUE);
	}
    
    public function get_data($tanggal) {
		if($tanggal != ''){
			$date = " where a.tanggal = '{$tanggal}' ";
		}
		else{
			$date = "";
		}
		return $this->db->query("SELECT * FROM elab_t_bppuc A
            LEFT JOIN elab_m_contoh B ON A.id_contoh = B.id_contoh {$date}
                ")->result_array();
	}
    public function get_transaksi($id = NULL, $tanggal) {
        if($id==''){
            $param = '';
            $date = " where  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }else{
             $param = ' where TANGGAL_SELESAI  IS NULL';
            $date = " and  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }
		// return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param} {$date }
                // ")->result_array();
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param}  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_BEBAN (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_BEBAN SET ".implode(",", $data)." WHERE ID_BEBAN={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_BEBAN WHERE ID_BEBAN={$id}");
	}
    
    
	public function getKelompok() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}
	public function getLab() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM")->result_array();
	}

	public function getBppuc($id)
	{
		$sql = "select b.nama_contoh, a.no_bappuc, 
		to_char(to_date(a.tanggal,'DD-MM-YYYY'),'DD Month YYYY','NLS_DATE_LANGUAGE = indonesian') tanggal,
		a.kemasan,
		case when coalesce(a.keterangan_warna,'1')='1' then 'NORMAL' else 'TIDAK NORMAL' end warna_contoh,
		case when coalesce(a.bentuk_contoh,'1')='1' then 'NORMAL' else 'TIDAK NORMAL' end bentuk_contoh
		from elab_t_bppuc a 
		left join elab_m_contoh b on b.id_contoh=a.id_contoh 
		where id_transaksi='{$id}' ";

		return $this->db->query($sql)->row(0);
	}

	public function getSupcMain($id)
	{
		$sql = "select a.no_spk, to_char(to_date(a.tanggal,'DD-MM-YYYY'),'DD Month YYYY','NLS_DATE_LANGUAGE=indonesian') tanggal,
		a.jumlah,
		a1.nama_contoh,
		b1.nama_kelompok, d.nm_kategori,
		b1.id_kelompok, d.id_kategori
		from elab_t_bppuc a
		left join elab_m_contoh a1 on a1.id_contoh=a.id_contoh
		left join elab_m_kelompok b1 on b1.id_kelompok=a1.id_kelompok
		left join elab_t_contoh b2 on b2.id_kelompok=a1.id_kelompok
		left join elab_m_uji c on c.id_uji=b2.id_uji
		left join elab_m_kategori d on d.id_kategori=c.id_kategori
		--left join elab_t_box b on b.id_uji=c.id_uji and b.id_transaksi=a.id_transaksi
		where a.id_transaksi='{$id}'
		group by a.no_spk, to_char(to_date(a.tanggal,'DD-MM-YYYY'),'DD Month YYYY','NLS_DATE_LANGUAGE=indonesian'),
		a.jumlah,
		a1.nama_contoh,
		b1.nama_kelompok, d.nm_kategori, b1.id_kelompok, d.id_kategori
		order by d.id_kategori";

		return $this->db->query($sql);
	}

	public function getSupcCheckBox($datapost)
	{
		
	}

	public function get_spuc_pdf($id)
	{
		$sql="select ID_TRANSAKSI, A.ID_UJI, NAMA_UJI, NM_KATEGORI from ELAB_T_BOX A
				join ELAB_M_UJI B on A.ID_UJI = B.ID_UJI
				join ELAB_M_KATEGORI C  on C.ID_KATEGORI = B.ID_KATEGORI
				where A.ID_TRANSAKSI = '{$id}'
				order by ID_TRANSAKSI";
		return $this->db->query($sql)->result_array();
	}

	public function elab_t_bppuc($id)
	{
		$sql="select * FROM 
				ELAB_T_BPPUC  A left join ELAB_M_CONTOH  B ON A.ID_CONTOH = B.ID_CONTOH
				where A.ID_TRANSAKSI = '{$id}'
				order by ID_TRANSAKSI ASC";
		return $this->db->query($sql)->result_array();
	}
	public function metode_uji($id)
	{
		$sql="select D.NAMA_STANDART FROM 
				ELAB_T_BPPUC  A LEFT JOIN ELAB_T_UJI C ON C.ID_TRANSAKSI = A.ID_TRANSAKSI
        		LEFT JOIN ELAB_M_STANDART_UJI D ON D.ID_STANDART = C.ID_STANDART
				where A.ID_TRANSAKSI = '{$id}'
				order by A.ID_TRANSAKSI ASC";
		return $this->db->query($sql)->result_array();
	}
	public function get_excel($id){
		
		$sql = $this->db->query("SELECT 
	A.ID_TRANSAKSI,
	  SUM( A.HARGA )  AS BIAYA,
	G.NAMA_CONTOH,
	F.NAMA_INSTANSI,
	F.ALAMAT,
	F.NPWP,
	D.NO_SPK,
	D.ASAL_CONTOH,
	D.JUMLAH,
	D.PEMINTA,
	D.KODE_UJI
FROM
	ELAB_T_BOX A
	LEFT JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
	RIGHT JOIN ELAB_T_BPPUC D ON D.ID_TRANSAKSI = A.ID_TRANSAKSI 
	LEFT JOIN ELAB_M_PELANGGAN F ON D.PEMINTA = F.ID_PELANGGAN
	LEFT JOIN ELAB_M_CONTOH G ON D.ID_CONTOH = G.ID_CONTOH 
WHERE
	A.ID_TRANSAKSI = '{$id}' 
	GROUP BY 
		A.ID_TRANSAKSI, 
		G.NAMA_CONTOH,
		F.NAMA_INSTANSI,
		F.ALAMAT,
		F.NPWP,
		D.NO_SPK,
		D.ASAL_CONTOH,
		D.JUMLAH,
		D.PEMINTA,
		D.KODE_UJI
              ");
		return $sql->result_array();
	}
	public function get_excel2($id){
		$sql = $this->db->query("SELECT distinct B.ID_KATEGORI,E.NM_KATEGORI FROM ELAB_T_BOX A 
									LEFT JOIN ELAB_M_UJI B ON A.ID_UJI=B.ID_UJI
									RIGHT JOIN ELAB_T_BPPUC D ON D.ID_TRANSAKSI = A.ID_TRANSAKSI
									LEFT JOIN ELAB_M_KATEGORI E ON E.ID_KATEGORI = B.ID_KATEGORI
              WHERE D.ID_TRANSAKSI='{$id}'");
		return $sql->result_array();
	}
	
	public function get_excel3($id,$kat){
		$sql = $this->db->query("SELECT * FROM ELAB_T_BOX A 
									LEFT JOIN ELAB_M_UJI B ON A.ID_UJI=B.ID_UJI
									RIGHT JOIN ELAB_T_BPPUC D ON D.ID_TRANSAKSI = A.ID_TRANSAKSI
									LEFT JOIN ELAB_M_KATEGORI E ON E.ID_KATEGORI = B.ID_KATEGORI
              WHERE D.ID_TRANSAKSI='{$id}' AND B.ID_KATEGORI ='{$kat}'");
		return $sql->result_array();
	}
	public function peminta_internal($id){ 

		$sql = $this->hris->query("SELECT
                k.mk_nopeg,
                k.mk_nama,
                k.muk_kode,
                uk.muk_nama,
                k.company
            FROM
                m_karyawan k
                LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode
                 where k.mk_nopeg = '{$id}'
                ");
                    return $sql->row_array();
	}

}
?>