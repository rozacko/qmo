<?php

class Display_bppuc_Model extends MY_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($tanggal=NULL) {
        if(isset($tanggal) && $tanggal != ''){
            $date = " where A.TANGGAL='{$tanggal}' ";
        }
        else{
            $date ='';
        }
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC A
            LEFT JOIN ELAB_M_CONTOH B ON A.ID_CONTOH = B.ID_CONTOH {$date}
                ")->result_array();
	}
    public function get_transaksi($id = NULL, $tanggal) {
        if($id==''){
            $param = '';
            $date = " where  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }else{
             $param = ' where TANGGAL_SELESAI  IS NULL';
            $date = " and  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }
		// return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param} {$date }
                // ")->result_array();
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param}  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_BEBAN (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_BEBAN SET ".implode(",", $data)." WHERE ID_BEBAN={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_BEBAN WHERE ID_BEBAN={$id}");
	}
    
    
	public function getKelompok() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}
	public function getLab() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM")->result_array();
	}

}
?>