<?php

class M_laboratorium extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM A 
                LEFT JOIN M_COMPANY B ON A.ID_COMPANY = B.ID_COMPANY  
                LEFT JOIN M_PLANT C ON A.ID_PLANT = C.ID_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_LABORATORIUM (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_LABORATORIUM SET ".implode(",", $data)." WHERE ID_LAB={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_LABORATORIUM WHERE ID_LAB={$id}");
	}
    
     
	public function getLokasi($id_company=null) {
        if($id_company==null){
             $company = "";
        }else{
            $company = " WHERE ID_COMPANY = '{$id_company}'";
        }
		return $this->db->query("SELECT * FROM M_PLANT {$company}")->result_array();
	}
	public function getCompany() {
		return $this->db->query("SELECT * FROM M_COMPANY")->result_array();
	}

}
?>