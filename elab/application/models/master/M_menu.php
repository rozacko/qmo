<?php

class M_menu extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
	
 	public function get_data()
 	{
		return $this->db->query("SELECT * FROM ELAB_M_MENU ORDER BY posisi_menu ASC, urutan_menu ASC")->result_array();
	}
	
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_MENU (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id_menu, $data) {
		return $this->db->query("UPDATE ELAB_M_MENU SET ".implode(",", $data)." WHERE id_menu={$id_menu}");
	}
	
	public function hapus($id_menu) {
		return $this->db->query("DELETE FROM ELAB_M_MENU WHERE id_menu={$id_menu}");
	}
	
	public function getParentMenu() {
		return $this->db->query("SELECT * FROM ELAB_M_MENU WHERE posisi_menu=1 ORDER BY posisi_menu ASC, urutan_menu ASC")->result_array();
	}
	
}
?>