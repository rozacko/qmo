<?php

class M_component extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM ELAB_M_COMPONENT where IS_DELETE = '0'")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_COMPONENT (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_COMPONENT SET ".implode(",", $data)." WHERE ID_COMPONENT={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_COMPONENT WHERE ID_COMPONENT={$id}");
	}

}
?>