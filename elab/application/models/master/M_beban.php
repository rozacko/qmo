<?php

class M_beban extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM A
            LEFT JOIN ELAB_M_BEBAN B ON A.ID_LAB = B.ID_LAB 
                ")->result_array();
	}
    public function get_transaksi($id = NULL, $tanggal) {
        if($id==''){
            $param = '';
            $date = " where  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }else{
             $param = ' where TANGGAL_SELESAI  IS NULL';
            $date = " and  to_char(TANGGAL, 'MM-YYYY')='{$tanggal}'";
        }
		// return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param} {$date }
                // ")->result_array();
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC  {$param}  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_BEBAN (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_BEBAN SET ".implode(",", $data)." WHERE ID_BEBAN={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  ELAB_M_BEBAN WHERE ID_BEBAN={$id}");
	}
    
    
	public function getKelompok() {
		return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}
	public function getLab() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM")->result_array();
	}

	public function max_day($day)
	{
		$query="SELECT	TO_CHAR(LAST_DAY(TO_DATE( '{$day}', 'DD-MM-YYYY' ) ) ,'DD') AS NUMDAYS FROM	DUAL";

		return $this->db->query($query)->row_array();
	}

	public function bppuc($date)
	{
		$query="SELECT	A.ID_TRANSAKSI,	TO_CHAR(TO_DATE( TANGGAL, 'DD-MM-YYYY' ), 'DD' ) AS TGL ,TO_CHAR(TO_DATE( TANGGAL, 'DD-MM-YYYY' ), 'DAY') AS NM,	A.TANGGAL FROM	ELAB_T_BPPUC A	LEFT JOIN ELAB_T_BOX B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI WHERE	TO_CHAR(TO_DATE( TANGGAL, 'DD-MM-YYYY' ), 'MM-YYYY' ) ='{$date}'";
		return $this->db->query($query)->result_array();
	}

}
?>