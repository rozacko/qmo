<?php

class M_analis extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_lab, $status, $kategori) {
		return $this->db->query(" SELECT A.ID_APPROVE, A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL, B.JUMLAH
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                      where A.STATUS = '{$status}' and KODE_LAB = '{$id_lab}' and ID_KATEGORI = '{$kategori}' 
                     ORDER BY A.ID_TRANSAKSI desc
                ")->result_array();
	}
    public function get_detail_data($id, $kategori ) {
		return $this->db->query(" 
                    	select T.*, U.NAMA_UJI from elab_t_upload T
                        join ELAB_M_UJI U ON T.ID_UJI = U.ID_UJI
                        where ID_TRANSAKSI = {$id} 
                        and T.ID_UJI in (SELECT A.ID_UJI from ELAB_T_BOX A 
                        JOIN  ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                        WHERE A.ID_TRANSAKSI = {$id} AND B.ID_KATEGORI = {$kategori})
                        ORDER BY T.ID_UJI, SAMPEL
                        
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_T_BPPUC SET ".implode(",", $data)." WHERE ID_TRANSAKSI={$id}");
	}
	public function saveFromExcel($tabel, $data) { 
        
        $this->db->trans_begin();
 
		$this->db->insert_batch($tabel, $data); 

		if ($this->db->trans_status() === FALSE)
		{
		        $this->db->trans_rollback();
		        return FALSE;
		}
		else
		{
		        $this->db->trans_commit();
		        return TRUE;
		}
	}
     
	public function upload($id, $status, $id_kategori) {
		return $this->db->query("UPDATE ELAB_T_APPROVE SET TGL_UPLOAD = SYSDATE  WHERE status='{$status}' and id_kategori={$id_kategori} and ID_TRANSAKSI='{$id}'");
	}
	public function selesai($id, $status, $id_kategori) {
		return $this->db->query("UPDATE ELAB_T_APPROVE SET TGL_AKHIR = SYSDATE  WHERE status='{$status}' and id_kategori={$id_kategori} and ID_TRANSAKSI='{$id}'");
	}
    
    public function deleteUpload($id_transaksi, $kategori) {
		return $this->db->query("DELETE FROM ELAB_T_UPLOAD WHERE ID_TRANSAKSI={$id_transaksi} and ID_KATEGORI={$kategori}");
	}
    public function deleteApprove($id_transaksi, $status) {
		return $this->db->query("DELETE FROM ELAB_T_APPROVE WHERE ID_TRANSAKSI={$id_transaksi} and  STATUS='{$status}'");
	}
    
    public function cek_approve($id, $status, $kategori) {
		return $this->db->query("select  ID_APPROVE,
	TO_CHAR( A.TGL_AWAL, 'dd-MM-yyyy' ) as AWAL, 
	TO_CHAR( A.TGL_AKHIR, 'dd-MM-yyyy' ) as AKHIR from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' and ID_KATEGORI = '{$kategori}'
                    ORDER BY STATUS DESC
                ")->row_array();
	}
    public function cek_cito($id, $status) {
		return $this->db->query(" SELECT
                    CITO, B.FULLNAME
                FROM
                    ELAB_T_APPROVE A 
                    join ELAB_M_USERS B on a.ID_USER = B.ID_USER
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' 
                    ORDER BY STATUS DESC
                ")->row_array();
	}
    public function cek_komentar($id ) {
		return $this->db->query(" select * from ELAB_T_APPROVE
                    where ID_TRANSAKSI = '{$id}' and TANGGAL IS NOT NULL AND ROWNUM = 1
                     and STATUS = 't2'
                    order by TANGGAL ASC
                ")->row_array();
	}
    
    public function cek_assign($id, $status, $id_kategori) {
		return $this->db->query(" SELECT
                    CITO, B.FULLNAME
                FROM
                    ELAB_T_APPROVE A 
                    join ELAB_M_USERS B on a.ID_USER = B.ID_USER
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' and A.ID_KATEGORI = '{$id_kategori}'
                    ORDER BY STATUS DESC
                ")->row_array();
	}


	public function saveUpload($data,$transaksi,$kategori,$counupload=0)
	{
		$this->db->trans_begin();

		if($counupload==1){
			$this->db->delete('ELAB_T_UPLOAD', array('ID_TRANSAKSI' => $transaksi,'ID_KATEGORI'=>$kategori));
		}
		$this->db->insert_batch('ELAB_T_UPLOAD',$data);

		if ($this->db->trans_status() === FALSE)
		{
		        $this->db->trans_rollback();
		        return FALSE;
		}
		else
		{
		        $this->db->trans_commit();
		        return TRUE;
		}
	}
    
    
    public function get_nilai($id, $id_uji, $sampel) {
		return $this->db->query("
                    SELECT NILAI, SAMPEL FROM ELAB_T_UPLOAD A
                    WHERE ID_TRANSAKSI = {$id} AND ID_UJI = {$id_uji} AND SAMPEL = '{$sampel}'
                    ORDER BY SAMPEL
                ")->row_array();
	}  
	 public function metode($id) {
		return $this->db->query("
                    SELECT * FROM ELAB_M_STANDART_UJI WHERE ID_STANDART = '{$id}'
                ")->row_array();
	}  
	public function box($id,$kategori) {
		return $this->db->query("
                    SELECT * FROM ELAB_T_UJI A LEFT JOIN ELAB_M_STANDART_UJI B ON A.ID_STANDART = B.ID_STANDART WHERE A.ID_TRANSAKSI = '{$id}' AND A.ID_KATEGORI='{$kategori}'
                ")->row_array();
	}
	public function transaksi($id) {
		return $this->db->query("
                    SELECT * FROM ELAB_T_BPPUC WHERE ID_TRANSAKSI = '{$id}'
                ")->row_array();
	}  
    public function get_cek_parameter($id, $id_uji, $start, $end) {
		return $this->db->query(" 
                    select A.ID_UJI, B.NAMA_UJI, SAMPEL, NILAI  FROM ELAB_T_UPLOAD A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    WHERE A.ID_TRANSAKSI = {$id} AND A.ID_UJI = {$id_uji}   and (SAMPEL>={$start} and SAMPEL<={$end})
                    ORDER BY A.ID_UJI, SAMPEL
                ")->result_array();
	} 
    
	public function get_spuc($id ) {
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC WHERE ID_TRANSAKSI = {$id}
                ")->row_array();
	}
	public function cekTo($role, $lab, $kategori){
        $id_kategori = ($kategori=='' ? '' : " AND A.ID_KATEGORI IN ({$kategori}) ");
		$sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_LAB = '{$lab}' {$id_kategori}
            GROUP BY A.ID_USER, EMAIL	
            ORDER BY
                A.ID_USER
");
		return $sql->result_array();
	}
}
?>