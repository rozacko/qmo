<?php

class M_mt extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_lab) {
		return $this->db->query("select * from ELAB_T_BPPUC  A
                    join ELAB_M_CONTOH B on A.ID_CONTOH = B.ID_CONTOH
                    where status = 'a0' AND A.IS_DELETE = '0'  and KODE_LAB in ({$id_lab})
                    ORDER BY ID_TRANSAKSI DESC
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_T_BPPUC SET ".implode(",", $data)." WHERE ID_TRANSAKSI={$id}");
	}
	public function get_spuc($id ) {
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC WHERE ID_TRANSAKSI = {$id}
                ")->row_array();
	}
	  
	public function cekTo($role, $lab, $kategori){
        $id_kategori = ($kategori=='' ? '' : " AND A.ID_KATEGORI IN ({$kategori}) ");
		$sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_LAB = '{$lab}' {$id_kategori}
            GROUP BY A.ID_USER, EMAIL	
            ORDER BY
                A.ID_USER
");
		return $sql->result_array();
	}
	public function cekKategori($id){
		$sql = $this->db->query("
            SELECT ID_KATEGORI FROM ELAB_T_BOX A
            JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
            WHERE ID_TRANSAKSI = '{$id}'
            GROUP BY ID_KATEGORI
");
		return $sql->result_array();
	}
    
    	
	

}
?>