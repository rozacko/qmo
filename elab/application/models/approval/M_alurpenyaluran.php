<?php

class M_alurpenyaluran extends CI_Model
{

	public function __construct()
	{

		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
    
	}

  private function query()
  {
    $columns=['ID_TRANSAKSI','KODE_UJI'];
    $this->db->from('ELAB_T_BPPUC');
    $i = 0;
    foreach ($columns as $item)
    {

        if($_POST['search']['value'])
        {
            if($i===0)
            {
                $this->db->group_start();
                $this->db->like($item, $_POST['search']['value']);
            }
            else
            {
                $this->db->or_like($item,$_POST['search']['value']);
            }

            if(count($columns) - 1 == $i)
                $this->db->group_end();
        }
        $i++;
    }
     
    if(isset($_POST['order']) && $_POST['order']['0']['column'] < 2 )
    {
        $this->db->order_by($columns[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
        $order = $this->order;
        $this->db->order_by(key($order), $order[key($order)]);
    }
  }
    
  public function data() {
    $this->query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result();
	}

  public function recordsFiltered() {
    $this->query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function recordsTotal()
  {
      $this->db->from("ELAB_T_BPPUC");
      return $this->db->count_all_results();
  }
    
}
?>