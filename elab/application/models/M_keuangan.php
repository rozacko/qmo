<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Keuangan extends CI_Model {

    var $table = 'ELAB_T_BPPUC A,ELAB_M_LABORATORIUM B, M_COMPANY C';
    var $column_order = array(); 
    var $column_search = array('A.TANGGAL','B.NM_LAB','B.ID_LAB','C.NM_COMPANY','C.ID_COMPANY');
    var $order = array('A.TANGGAL' => 'asc',
                        'B.NM_LAB'=>'asc',
                        'C.NM_COMPANY'=>'asc'); 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function get_data()
    {
       return $this->db->query("SELECT * FROM ELAB_T_BPPUC A
       join ELAB_M_LABORATORIUM B on A.KODE_LAB = B.ID_LAB
       join M_COMPANY C on C.ID_COMPANY = B.ID_COMPANY
       order  by A.ID_TRANSAKSI
                ")->result_array();
    }
    
    
     private function _get_datatables_query()
    {
         
        
        if($this->input->post('start_date') && $this->input->post('end_date'))
        {
            
             $this->db->where('A.TANGGAL >=', date($this->input->post('start_date')));
            $this->db->where('A.TANGGAL <=', date($this->input->post('end_date')));
        }
        if ($this->input->post('company')) {
            $this->db->where('C.ID_COMPANY', $this->input->post('company'));
        }
        if ($this->input->post('lab')) {
            $this->db->where('B.ID_LAB', $this->input->post('lab'));
        }
       
        $this->db->select ('*');    
        $this->db->from('ELAB_T_BPPUC A');        
        $this->db->join('ELAB_M_LABORATORIUM B','A.KODE_LAB = B.ID_LAB');
        $this->db->join('M_COMPANY C','C.ID_COMPANY = B.ID_COMPANY');
        $this->db->order_by('A.ID_TRANSAKSI');
        $i = 0;
     
        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->select ('*');    
        $this->db->from('ELAB_T_BPPUC A');        
        $this->db->join('ELAB_M_LABORATORIUM B','A.KODE_LAB = B.ID_LAB');
        $this->db->join('M_COMPANY C','C.ID_COMPANY = B.ID_COMPANY');
        return $this->db->count_all_results();
    }
    public function lab(){
        $sql = $this->db->query('SELECT * FROM ELAB_M_LABORATORIUM');
        return $sql;
    }
    public function company(){
        $sql = $this->db->query('SELECT * FROM  M_COMPANY ');
        return $sql;
    }
    
    
    public function get_biaya($id, $id_lab = NULL){
        // $sql = $this->db->query("SELECT f.ID_TRANSAKSI, g.ID_KATEGORI, f.ID_STANDART from elab_t_uji f
            // join ELAB_M_STANDART_UJI g on f.ID_STANDART = g.ID_STANDART 
            // where f.ID_TRANSAKSI = '{$id}'")->result_array();
           // $standart = '';
            // foreach($sql as $v){
                // $standart .= $v['ID_STANDART'].",";
            // } 
            // $standart = ($standart=='' ? '2222222222222222222222222222222' : rtrim($standart, ","));
                
        // $sql = $this->db->query("select a.id_uji, a.ID_TRANSAKSI, b.ID_KATEGORI, LAB,  BIAYA , c.STANDART from elab_t_box a 
                // join ELAB_M_UJI b on a.ID_UJI = b.id_uji
                // left join ELAB_M_BIAYA c on a.ID_UJI = c.id_uji
                // where a.ID_TRANSAKSI =  '{$id}' and c.STANDART in ({$standart}) and LAB = '{$id_lab}'
                // order by b.ID_KATEGORI, b.id_uji
            // ")->result_array();
            $sql = $this->db->query("SELECT A.*, B.JUMLAH  FROM ELAB_T_BOX A
                    join ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI

                    WHERE A.ID_TRANSAKSI={$id}")->result_array();
            $biaya = 0;
            if(count($sql)>0){
                      foreach($sql as $v){
                        // $biaya += $v['BIAYA'].",";
                        $harga = ($v['HARGA']!='' ? $v['HARGA'] : 0);
                        $biaya += $harga*$v['JUMLAH']; 
                    }  
            }
        return $biaya;
    }
    
    
	public function ambilLab($id){
		$sql = $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM A
								JOIN M_PLANT B ON A.ID_PLANT = B.ID_PLANT
                                JOIN M_COMPANY C ON B.ID_COMPANY = C.ID_COMPANY
                                WHERE C.ID_COMPANY='{$id}'");
		return $sql->result_array();
	}
    
}