<?php
 
class M_user Extends CI_Model { 
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
 
	public function data($where){
        	return $this->db->query("SELECT * from ELAB_M_USERS a
                    left join ELAB_M_ROLE b on a.id_role=b.id_role
                    where lower(a.USERNAME) = '{$where['USERNAME']}' AND a.ISACTIVE = 'Y'")->row();
	}
	public function akses_menu($where){
        	return $this->db->query("SELECT
                B.ID_ROLE,B.PERMISSION,
                C.ID_SUBROLE, C.READ, C.WRITE,D.*
            FROM
                ELAB_M_USERS A 
                JOIN ELAB_M_USERLAB Y ON A.ID_USER = Y.ID_USER
                JOIN ELAB_M_ROLE B ON Y.ID_ROLE = B.ID_ROLE
                -- JOIN ELAB_M_ROLE B ON A.ID_ROLE = B.ID_ROLE
                JOIN ELAB_M_SUBROLE C ON B.ID_ROLE = C.ID_ROLE
                JOIN ELAB_M_MENU D ON D.ID_MENU = C.ID_MENU where lower(A.USERNAME) = '{$where['USERNAME']}'")->result_array();
	}
	public function akses_url($user, $link){
        	return $this->db->query("SELECT
                B.ID_ROLE,
                C.ID_SUBROLE,D.*
            FROM
                ELAB_M_USERS A
                JOIN ELAB_M_ROLE B ON A.ID_ROLE = B.ID_ROLE
                JOIN ELAB_M_SUBROLE C ON B.ID_ROLE = C.ID_ROLE
                JOIN ELAB_M_MENU D ON D.ID_MENU = C.ID_MENU where B.ID_ROLE = '{$user}' AND lower(D.LINK_MENU) LIKE ('{$link}%')")->result_array();
	}
  
    public function get_data($type) {
		return $this->db->query("SELECT * FROM ELAB_M_USERS A
                LEFT JOIN ELAB_M_ROLE B ON A.ID_ROLE = B.ID_ROLE 
                LEFT JOIN ELAB_M_LABORATORIUM C ON A.ID_LAB = C.ID_LAB 
                WHERE ISACTIVE='Y' and TYPE='{$type}'
                ")->result_array();
	}
    
	public function simpan($column, $data) { 
        
        $this->db->query("INSERT INTO ELAB_M_USERS (".implode(",", $column).") VALUES (".implode(",", $data).")");
        
        $query = $this->db->query('SELECT MAX(ID_USER) as ID FROM ELAB_M_USERS');
        $row = $query->row();
        $id = $row->ID;
         if($id == NULL) $id = 1;
        return $id;;
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_USERS SET ".implode(",", $data)." WHERE ID_USER={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE ELAB_M_USERS SET ISACTIVE='N'  WHERE ID_USER={$id}");
	}
    
    
	public function getCompany() {
		return $this->db->query("SELECT * FROM M_COMPANY")->result_array();
	}
    
	public function getPlant($id) {
		return $this->db->query("SELECT * FROM COQ_PLANT WHERE ID_COMPANY = {$id}")->result_array();
	}
    
	public function getRole() {
		return $this->db->query("SELECT * FROM ELAB_M_ROLE")->result_array();
	}

	public function getLab() {
		return $this->db->query("SELECT * FROM ELAB_M_LABORATORIUM")->result_array();
	}

	public function getKategori() {
		return $this->db->query("SELECT * FROM ELAB_M_KATEGORI")->result_array();
	}
	public function cek_userlab($id) {
		return $this->db->query("
            SELECT A.ID_LAB, A.ID_ROLE, B.NM_LAB, C.NAMA_ROLE, D.ID_KATEGORI, D.NM_KATEGORI, A.ID_COMPANY, E.NM_COMPANY, A.ID_PLANT, F.NM_PLANT
            FROM ELAB_M_USERLAB A 
            left join ELAB_M_LABORATORIUM B ON A.ID_LAB = B.ID_LAB 
            join ELAB_M_ROLE C ON A.ID_ROLE = C.ID_ROLE 
            LEFT join ELAB_M_KATEGORI D ON A.ID_KATEGORI = D.ID_KATEGORI 
            LEFT join M_COMPANY E ON A.ID_COMPANY = E.ID_COMPANY 
            LEFT join COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT 
            where A.ID_USER = '{$id}'")->result_array();
	}
	public function cek_userlab_login($where) {
		return $this->db->query("
            SELECT A.ID_USER,A.ID_LAB, A.ID_ROLE, B.NM_LAB, C.NAMA_ROLE, D.USERNAME, A.ID_PLANT, D.ID_KATEGORI, D.NM_KATEGORI
            FROM ELAB_M_USERLAB A 
            LEFT join ELAB_M_LABORATORIUM B ON A.ID_LAB = B.ID_LAB 
            join ELAB_M_ROLE C ON A.ID_ROLE = C.ID_ROLE 
            LEFT join ELAB_M_KATEGORI D ON A.ID_KATEGORI = D.ID_KATEGORI 
						 join ELAB_M_USERS D on A.ID_USER = D.ID_USER
            where lower(D.USERNAME) ='{$where['USERNAME']}'")->result_array();
	}



	public function addSubrole($tabel, $data) {
		return  $this->db->insert_batch($tabel, $data); 
	}
	public function deleteSubrole($id) {
		return $this->db->query("DELETE FROM ELAB_M_USERLAB WHERE ID_USER={$id}");
	}
    
	public function send_email($id, $lab) {
		return $this->db->query("
                SELECT EMAIL FROM ELAB_M_USERS A
                    LEFT JOIN ELAB_M_USERLAB B ON A.ID_USER = B.ID_USER
                    WHERE B.ID_ROLE = '{$id}' AND B.ID_LAB = '{$lab}'
                GROUP BY EMAIL ");
	}
    
    
    
}
