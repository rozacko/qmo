<?php

class M_kupp extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->hris = $this->load->database('hris', TRUE);
	}
    
    public function get_data($id, $status ) { 
		return $this->db->query("
                  Select NO_BAPPUC, B.TANGGAL, PERALATAN, PERSONIL, METODE, WAKTU, A.KETERANGAN, FULLNAME from ELAB_T_APPROVE A
                    join ELAB_T_BPPUC B on A.ID_TRANSAKSI = B.ID_TRANSAKSI 
                    join ELAB_M_USERS C on A.ID_USER = C.ID_USER 
                    where A.ID_TRANSAKSI = '{$id}' AND A.STATUS = '{$status}'
                     
                ")->row_array();	
                
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_T_BPPUC SET ".implode(",", $data)." WHERE ID_TRANSAKSI={$id}");
	}
	
    public function cek_approve($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' {$kategori}
                    ORDER BY STATUS DESC
                ")->row_array();
	} 
    public function cek_off($id, $off, $id_kategori) {
		return $this->db->query("   SELECT A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                      where A.STATUS = '{$off}' AND A.ID_TRANSAKSI = '{$id}' AND A.ID_KATEGORI = {$id_kategori}
                     ORDER BY A.ID_TRANSAKSI
                ")->result_array();	 
	}
    
    public function cek_kategori($id) {
		return $this->db->query("select C.ID_KATEGORI, NM_KATEGORI from ELAB_T_BOX  A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    JOIN ELAB_M_KATEGORI C ON B.ID_KATEGORI = C.ID_KATEGORI
                    where ID_TRANSAKSI = '{$id}'  
                    group by C.ID_KATEGORI, NM_KATEGORI
                ")->result_array();
	} 
    public function cek_parameter($id) {
		return $this->db->query("select ID_KATEGORI, B.NAMA_UJI from ELAB_T_BOX  A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    where ID_TRANSAKSI = '{$id}'  
                    order by B.ID_KATEGORI, B.ID_UJI
                ")->result_array();
	} 
 
    
    public function cek_tombol($id, $status, $kategori) {
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' and ID_KATEGORI = '{$kategori}'
                    ORDER BY STATUS DESC
                ")->row_array();
	}
    
    public function cek_transaksi($id) {
		return $this->db->query("SELECT A.*, B.NAMA_CONTOH, C.KET_WARNA, D.BENTUK_CONTOH AS BENTUK_, E.KONDISI_CONTOH, F.NAMA_INSTANSI, F.ALAMAT AS ALAMAT_INS,G.NAMA_BERAT  FROM ELAB_T_BPPUC A
                JOIN ELAB_M_CONTOH B ON A.ID_CONTOH = B.ID_CONTOH
                LEFT JOIN ELAB_SEMUA_CONTOH_UJI C ON A.KETERANGAN_WARNA = C.ID_CONTOH_UJI
                LEFT JOIN ELAB_SEMUA_CONTOH_UJI D ON A.BENTUK = D.ID_CONTOH_UJI
                LEFT JOIN ELAB_SEMUA_CONTOH_UJI E ON A.KONDISI = E.ID_CONTOH_UJI
                LEFT JOIN ELAB_M_PELANGGAN F ON A.PEMINTA = F.ID_PELANGGAN
                LEFT JOIN ELAB_CONTOH_BERAT G ON A.BERAT = G.ID_BERAT
                WHERE A.ID_TRANSAKSI = {$id}
                ")->row_array();
	}
    
    public function peminta_internal($filter){ 
		$sql = $this->hris->query("SELECT
            k.mk_nopeg,
            k.mk_nama,
            k.muk_kode,
            uk.muk_nama,
            k.company
        FROM
            m_karyawan k
            LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode
             where LOWER(k.mk_nopeg)= '{$filter}'  
            ");
		return $sql->row_array();
	}
    
    public function metode($id){ 
		$sql = $this->db->query("SELECT * FROM ELAB_T_UJI A
            LEFT JOIN ELAB_M_STANDART_UJI B ON A.ID_STANDART = B.ID_STANDART
            LEFT JOIN ELAB_M_JENIS C ON B.ID_JENIS = C.ID_JENIS
            WHERE A.ID_TRANSAKSI = '{$id}'
            ");
		return $sql->result_array();
	}
     

}
?>