<?php

class M_hasiluji extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
		$this->hris = $this->load->database('hris', TRUE);
	}
    
    public function get_data($id_lab, $on ) { 
		return $this->db->query("
                  SELECT A.ID_TRANSAKSI, B.NO_BAPPUC, B.TANGGAL, B.ID_CONTOH, C.NAMA_CONTOH FROM ELAB_T_APPROVE A
                    JOIN ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                    JOIN ELAB_M_CONTOH C ON B.ID_CONTOH = C.ID_CONTOH
                    WHERE A.STATUS IN ({$on}) AND KODE_LAB = '{$id_lab}'  
                    GROUP BY A.ID_TRANSAKSI,  B.NO_BAPPUC, B.TANGGAL, B.ID_CONTOH, C.NAMA_CONTOH 
                ")->result_array();	
                
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	} 
	 
     
    public function cek_kategori($id, $on ) { 
		return $this->db->query("
                  SELECT A.ID_KATEGORI, B.NM_KATEGORI FROM ELAB_T_APPROVE A
                  JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI
                    WHERE STATUS IN ({$on}) AND ID_TRANSAKSI = {$id}
                ")->result_array();	
                
	}
     
    public function cek_shu($id, $on ) { 
		return $this->db->query("
                  SELECT * FROM ELAB_T_APPROVE A 
                    WHERE STATUS IN ({$on}) AND ID_TRANSAKSI = {$id}
                ")->result_array();	
                
	}

    public function get_parameter($id, $id_kategori) {
		return $this->db->query("SELECT A.ID_TRANSAKSI, A.ID_UJI, NAMA_UJI, SIMBOL, SATUAN from ELAB_T_BOX A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    WHERE A.ID_TRANSAKSI = {$id} and B.ID_KATEGORI = {$id_kategori}
                    ORDER BY B.ID_UJI
                ")->result_array();
	} 
    public function get_header($id, $status, $id_kategori) {
		return $this->db->query("SELECT NAMA_CONTOH, JUMLAH, A.TANGGAL, to_char(B.TGL_AWAL, 'DD-MM-YYYY') as TGL_AWAL, A.NO_BAPPUC, NM_KATEGORI, A.SAMPEL FROM ELAB_T_BPPUC A
            JOIN ELAB_T_APPROVE B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
            JOIN ELAB_M_CONTOH C ON A.ID_CONTOH = C.ID_CONTOH
            JOIN ELAB_M_KATEGORI D on B.ID_KATEGORI = D.ID_KATEGORI
            WHERE A.ID_TRANSAKSI = {$id} and B.STATUS = '{$status}' and b.ID_KATEGORI = '{$id_kategori}'
                ")->row_array();
	} 
    public function get_nilai($id, $id_uji) {
		return $this->db->query("
                    SELECT NILAI, SAMPEL FROM ELAB_T_UPLOAD A
                    WHERE ID_TRANSAKSI = {$id} AND ID_UJI = {$id_uji} 
                ")->result_array();
	} 
    public function get_title($id) {
		return $this->db->query("
                   select * from elab_m_kategori where id_kategori = {$id}
                ")->row_array();
	} 
    public function get_lab($id) {
		return $this->db->query("
                   select nm_lab, JUMLAH from elab_t_bppuc a
                   join elab_m_laboratorium b on a.kode_lab =  b.id_lab
                   where id_transaksi = {$id}
                ")->row_array();
	} 
    public function get_shu($id) {
		return $this->db->query("select NAMA_UJI, SIMBOL, SATUAN, B.ID_UJI, B.ID_KATEGORI from ELAB_T_BOX A
                JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                WHERE A.ID_TRANSAKSI = {$id}
                ORDER BY B.ID_KATEGORI ASC, B.ID_UJI ASC
                ")->result_array();
	}
    public function get_shu_kategori($id) {
		return $this->db->query("select  B.ID_KATEGORI,  C.NM_KATEGORI from ELAB_T_BOX A
                JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                JOIN ELAB_M_KATEGORI C ON B.ID_KATEGORI = C.ID_KATEGORI
                WHERE A.ID_TRANSAKSI = {$id}
                GROUP BY B.ID_KATEGORI,  C.NM_KATEGORI 
                ORDER BY B.ID_KATEGORI ASC 
                ")->result_array();
	}
    public function get_header_shu($id) {
		return $this->db->query("
            SELECT A.ID_CONTOH, NAMA_CONTOH , A.KODE_LAB, A.KODE_UJI, C.NM_LAB, KD_LAB, A.JUMLAH, A.KETERANGAN, NO_BAPPUC, NO_SPK, KEMASAN, PEMINTA, D.ALAMAT, D.NAMA_INSTANSI, A.TANGGAL FROM ELAB_T_BPPUC A
            JOIN ELAB_M_CONTOH B ON A.ID_CONTOH = B.ID_CONTOH
            JOIN ELAB_M_LABORATORIUM C ON A.KODE_LAB = C.ID_LAB
            LEFT JOIN ELAB_M_PELANGGAN D ON A.PEMINTA = D.ID_PELANGGAN
            WHERE A.ID_TRANSAKSI= {$id}
                ")->row_array();
	} 
    public function get_metode($id) {
		return $this->db->query("
            select A.ID_T_UJI,  A.ID_STANDART, NAMA_STANDART from ELAB_T_UJI A
            LEFT JOIN ELAB_M_STANDART_UJI B ON A.ID_STANDART = B.ID_STANDART 
            WHERE A.ID_TRANSAKSI= {$id}
                ")->row_array();
	} 

    public function lhu($where=array(),$where_in=[])
    {
        if(count($where_in)!=0)
        {   $data = $this->db->from('ELAB_T_UPLOAD')->where($where)
            ->where_in('ID_UJI',$where_in);
            return $data->get()->result_array();
        }else{
            $data = $this->db->get_where('ELAB_T_UPLOAD', $where);
            return $data->result_array();
        }
    }

    public function approve_users($where=array())
    {
        $data = $this->db->join('ELAB_M_USERS','ELAB_T_APPROVE.ID_USER=ELAB_M_USERS.ID_USER','left')
        ->get_where('ELAB_T_APPROVE', $where);
        return $data->row_array();
    }
    public function numbering($id)
    {
       return $this->db->query("
           SELECT TO_CHAR(TANGGAL, 'mm.yyyy') as TANGGAL, TO_CHAR(TANGGAL, 'dd-mm-yyyy') as TERBIT, NUMBERING from ELAB_T_APPROVE where STATUS ='a5' and ID_TRANSAKSI = '{$id}'
                ")->row_array();
    }
    public function get_standart($id)
    {
       return $this->db->query(" 
                SELECT * FROM ELAB_T_UJI A
                    LEFT JOIN ELAB_M_STANDART_UJI B ON A.ID_STANDART = B.ID_STANDART
                    WHERE A.ID_TRANSAKSI = {$id}
                    ORDER BY A.ID_KATEGORI
                ")->result_array();
    }
    public function get_standart_lhu($id, $kategori)
    {
       return $this->db->query(" 
                SELECT * FROM ELAB_T_UJI A
                    LEFT JOIN ELAB_M_STANDART_UJI B ON A.ID_STANDART = B.ID_STANDART
                    WHERE A.ID_TRANSAKSI = {$id} and B.ID_KATEGORI = {$kategori}
                    ORDER BY A.ID_KATEGORI
                ")->row_array();
    }
    public function get_pengujian($id)
    {
       return $this->db->query(" 
                SELECT TO_CHAR(TGL_AWAL, 'dd-mm-yyyy') as AWAL, TO_CHAR(TGL_AKHIR, 'dd-mm-yyyy') as AKHIR, ID_KATEGORI from ELAB_T_APPROVE where STATUS ='a3' and ID_TRANSAKSI = '{$id}'
order by ID_KATEGORI
                ")->result_array();
    }
    public function view_peminta($id){
                $sql = $this->hris->query("SELECT
                k.mk_nopeg,
                k.mk_nama,
                k.muk_kode,
                uk.muk_nama,
                k.company
            FROM
                m_karyawan k
                LEFT JOIN m_unit_kerja uk ON uk.muk_kode=k.muk_kode
                 where k.mk_nopeg = '{$id}'
                ");
                    return $sql->row_array();
	}
    
    
}
?>