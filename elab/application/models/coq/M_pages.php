<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pages extends CI_Model {

	var $table = 'COQ_PORTAL';
    var $column_order = array(); 
    var $column_search = array('MENU','LINK');
    var $order = array('ID_MENU'=>'ASC'); 
	public function __construct()
    {
        parent::__construct();
        
    }
	 private function _get_datatables_query()
    {
    	
		$this->db->select ( '*' ); 
        $this->db->from($this->table);
        

        $i = 0;
     
        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
       
        $this->db->select ( '*' ); 
        $this->db->from($this->table);
        
        return $this->db->count_all_results();
    }
	/*public function tampil(){
        $sql = $this->db->query("SELECT * FROM menu where is_main_menu ='0'");
        return $sql->result_array();
    }*/
	public function add($data)
	{
		return $this->db->insert("COQ_PORTAL",$data);
	}
    
	public function hapus($id){
		$this->db->where('ID_MENU',$id);
		$this->db->delete('COQ_PORTAL');
	}
	public function editData($id)
    {
         
        $this->db->select ( '*' ); 
        $this->db->from('COQ_PORTAL');
        $this->db->where('ID_MENU',$id);
        $query = $this->db->get();

        return $query->row();

    
    }
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
   


	
}