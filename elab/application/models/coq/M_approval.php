<?php

class M_approval extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data_request() {
		return $this->db->query("SELECT ID_REQUEST AS ID, NM_REQUEST AS NAMA, NM_STANDART, NM_PRODUCT_TYPE, 'Request' AS TIPE
                    FROM COQ_REQUEST A
                    JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                    JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                    ORDER BY ID_REQUEST DESC
                ")->result_array();
	} 
    public function get_data_spec($plant = NULL) {
         /*$id_plant = ($plant == '9999' || $plant == '' ? ' ' : "AND A.ID_PLANT IN ({$plant})");*/
         if ($plant == '9999' || $plant == '') {
            $id_plant = '';
        }elseif ($plant == '9998') {
            $id_plant = "AND A.ID_PLANT IN (8,9,10,11)";
        }elseif ($plant == '9997') {
            $id_plant = "AND A.ID_PLANT IN (1,42,41)";
        }elseif ($plant == '9996') {
            $id_plant = "AND A.ID_PLANT IN (4,5)";
        }else{
            $id_plant = " AND A.ID_PLANT IN ({$plant})";
        }
		return $this->db->query("SELECT ID_SPEC_HEADER AS ID, KD_SPEC AS KODE, NM_SPEC AS NAMA, NM_STANDART, NM_PRODUCT_TYPE,ID_REQUEST,NM_PLANT,NM_COMPANY,A.ID_PLANT, 'Specification' AS TIPE
                FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE 
                LEFT JOIN COQ_PLANT D ON A.ID_PLANT = D.ID_PLANT 
                LEFT JOIN M_COMPANY E ON D.ID_COMPANY = E.ID_COMPANY
                WHERE A.IS_DELETE = 0 {$id_plant}
                ORDER BY ID_SPEC_HEADER DESC
                ")->result_array();
	} 
    public function get_data_quality($plant = NULL) {
        /*$id_plant = ($plant == '9999' || $plant == '' ? ' ' : "AND A.ID_PLANT IN ({$plant})");*/
        if ($plant == '9999' || $plant == '') {
            $id_plant = '';
        }elseif ($plant == '9998') {
            $id_plant = "AND A.ID_PLANT IN (8,9,10,11)";
        }elseif ($plant == '9997') {
            $id_plant = "AND A.ID_PLANT IN (1,42,41)";
        }elseif ($plant == '9996') {
            $id_plant = "AND A.ID_PLANT IN (4,5)";
        }else{
            $id_plant = " AND A.ID_PLANT IN ({$plant})";
        }
		return $this->db->query("SELECT ID_QUALITY_HEADER AS ID, KD_QUALITY AS KODE, NM_QUALITY AS NAMA, NM_STANDART, NM_PRODUCT_TYPE, ID_REQUEST,NM_PLANT,NM_COMPANY,A.ID_PLANT, 'Quality' AS TIPE
                FROM COQ_QUALITY_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE 
                LEFT JOIN COQ_PLANT D ON A.ID_PLANT = D.ID_PLANT 
                LEFT JOIN M_COMPANY E ON D.ID_COMPANY = E.ID_COMPANY 
                WHERE A.IS_DELETE = 0 {$id_plant}
                ORDER BY ID_QUALITY_HEADER DESC
                ")->result_array();
	} 
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO COQ_METODE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO COQ_APPROVAL (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_METODE SET ".implode(",", $data)." WHERE ID_METODE={$id}");
	}
	public function updateStatusProgress($id, $id_user, $status, $tipe) {
		return $this->db->query("INSERT INTO COQ_APPROVAL (ID_JOIN, ID_USER, STATUS, TANGGAL, TIPE) VALUES ({$id}, {$id_user}, {$status}, 'SYSDATE', {$tipe})  ");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  COQ_METODE WHERE ID_METODE={$id}");
	}
    public function get_review($id){
        $this->db->select('A.*,B.FULLNAME,D.NAMA_ROLE,E.NM_PLANT');
        $this->db->from('COQ_APPROVAL A');
        $this->db->where('ID_JOIN',$id);
        $this->db->join('ELAB_M_USERS B','A.ID_USER=B.ID_USER','left');
        $this->db->join('ELAB_M_USERLAB C','A.ID_USER = C.ID_USER','left');
        $this->db->join('ELAB_M_ROLE D','C.ID_ROLE = D.ID_ROLE');
        $this->db->join('COQ_PLANT E','A.ID_PLANT = E.ID_PLANT','left');
        $this->db->order_by("ID_APPROVAL","DESC");
        $query = $this->db->get();

        return $query->row();
    }
    public function cekTo($role, $plant){
        $sql = $this->db->query("
           
            SELECT * FROM (             SELECT
                A.ID_USER, EMAIL,B.FULLNAME
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_PLANT = '{$plant}' AND B.ISACTIVE='Y'
            GROUP BY A.ID_USER, EMAIL,B.FULLNAME   
            ORDER BY A.ID_USER) WHERE ROWNUM = '1'
");
        return $sql->row_array();
    }
    public function cekTo2($role,$user){
        $sql = $this->db->query("
             SELECT * FROM (             SELECT
                A.ID_USER, EMAIL,B.FULLNAME
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_PLANT = '{$plant}' AND B.ISACTIVE='Y'
            GROUP BY A.ID_USER, EMAIL,B.FULLNAME   
            ORDER BY A.ID_USER) WHERE ROWNUM = '1'
");
        return $sql->row_array();
    }
	// public function getApprove($id) {
		// return $this->db->query("SELECT
                // * 
            // FROM
                // COQ_APPROVAL WHERE   ID_JOIN = '{$id}'  and rownum = 1
                // order BY ID_APPROVAL DESC")->row_array();
	// }
    
    
    public function getApprove($id, $tipe){
    	$this->db->limit(1);
         $this->db->where('ID_JOIN', $id);
         $this->db->where('TIPE', $tipe);
    	$this->db->order_by("ID_APPROVAL","DESC");
    	$q = $this->db->get('COQ_APPROVAL');
    	return $q;
    }
    
	public function getDetailSpec($id) {
		return $this->db->query("SELECT A.*, A.KD_SPEC AS KODE,  NM_STANDART, NM_PRODUCT_TYPE, ID_REQUEST,NM_PLANT,NM_COMPANY 
                FROM COQ_SPEC_HEADER A
                JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE 
                JOIN COQ_PLANT D ON A.ID_PLANT = D.ID_PLANT 
                JOIN M_COMPANY E ON D.ID_COMPANY = E.ID_COMPANY 
                   where ID_SPEC_HEADER = '{$id}'
                ORDER BY ID_SPEC_HEADER
               ")->row_array();
	}
	public function getDetailQuality($id) {
		return $this->db->query("SELECT A.*, A.KD_QUALITY AS KODE,  NM_STANDART, NM_PRODUCT_TYPE, ID_REQUEST,NM_PLANT,NM_COMPANY 
                FROM COQ_QUALITY_HEADER A
                JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE 
                JOIN COQ_PLANT D ON A.ID_PLANT = D.ID_PLANT 
                JOIN M_COMPANY E ON D.ID_COMPANY = E.ID_COMPANY 
                   where ID_QUALITY_HEADER = '{$id}' 
               ")->row_array();
	}
    
    
    
	public function cek_spec($id, $id_kategori) {
		return $this->db->query("
            SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT FROM COQ_SPEC_DETAIL A
            JOIN COQ_SPEC_HEADER B ON A.ID_SPEC_HEADER = B.ID_SPEC_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            LEFT JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_SPEC_HEADER = '{$id}' and D.ID_KATEGORI = '{$id_kategori}'	ORDER BY A.ID_SPEC_DETAIL ASC")->result_array();
	}
    
    
	public function cek_quality($id, $id_kategori) {
		return $this->db->query("
            SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT FROM COQ_QUALITY_DETAIL A
            JOIN COQ_QUALITY_HEADER B ON A.ID_QUALITY_HEADER = B.ID_QUALITY_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            LEFT JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_QUALITY_HEADER = '{$id}' and D.ID_KATEGORI = '{$id_kategori}'	ORDER BY A.ID_QUALITY_DETAIL ASC")->result_array();
	}
    
	public function getStandart() {
		return $this->db->query("SELECT * FROM COQ_STANDART")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM COQ_PLANT")->result_array();
	}
    
	public function cekStatusProgress($id_join, $id_tipe, $status) {
		return $this->db->query("SELECT * FROM COQ_APPROVAL
                    where ID_JOIN = '{$id_join}' and TIPE = '{$id_tipe}' and STATUS IN ()
                    ")->result_array();
	}
    
    function get_spec($id)
    {
        return $this->db->query("SELECT * FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON C.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
				LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_SPEC_HEADER = '{$id}' 
                ORDER BY ID_SPEC_HEADER DESC  
                ")->result_array();
    
    }
    function get_spec2($id,$idStand)
    {
        return $this->db->query("SELECT * FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON A.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
                LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_SPEC_HEADER = '{$id}' AND E.ID_STANDART = '{$idStand}'
                ORDER BY ID_SPEC_HEADER DESC  
                ")->result_array();

    }
    public function cek_spec_template($id) {
        return $this->db->query("
          SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT FROM COQ_SPEC_DETAIL A
            JOIN COQ_SPEC_HEADER B ON A.ID_SPEC_HEADER = B.ID_SPEC_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            LEFT JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_SPEC_HEADER = '{$id}' ORDER BY A.ID_SPEC_DETAIL ASC
            ")->result_array();
    }
    function get_quality($id)
    {
        return $this->db->query("SELECT * FROM COQ_QUALITY_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON C.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE 
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT 
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
				LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_QUALITY_HEADER  = '{$id}'
                ORDER BY ID_QUALITY_HEADER DESC  
                ")->result_array();
    
    }
    function get_quality2($id,$idStand)
    {

        return $this->db->query("SELECT * FROM COQ_QUALITY_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON A.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE 
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT 
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
                LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_QUALITY_HEADER  = '{$id}' AND E.ID_STANDART = '{$idStand}'
                ORDER BY ID_QUALITY_HEADER DESC  
                ")->result_array();
    
    }
    public function cek_quality_template($id) {
        return $this->db->query("
          SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT FROM COQ_QUALITY_DETAIL A
            JOIN COQ_QUALITY_HEADER B ON A.ID_QUALITY_HEADER = B.ID_QUALITY_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            LEFT JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_QUALITY_HEADER = '{$id}' ORDER BY A.ID_QUALITY_DETAIL ASC
            ")->result_array();
    }
     public function ambilStand($tabel, $id){
        $q = $this->db->query("select * from {$tabel} where ID_QUALITY_HEADER = '{$id}'");
        return $q;
    }
}
?>