<?php

class M_parameter extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT
                        A.*,
                        B.*,
                        C.*,
                        ( SELECT S.NAMA_UJI FROM ELAB_M_UJI S WHERE S.ID_UJI = A.PARENT ) AS PARENT_PARAMETER 
                    FROM
                        ELAB_M_UJI A
                        LEFT JOIN COQ_PRODUCT B ON A.ID_PRODUCT = B.ID_PRODUCT 
                        LEFT JOIN ELAB_M_KATEGORI C ON A.ID_KATEGORI = C.ID_KATEGORI
                    WHERE
                        IS_DELETE = 0 
                    ORDER BY
                        A.ID_PRODUCT, A.ID_UJI
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM 
                ELAB_M_UJI A
                -- join ELAB_M_COMPONENT R ON A.ID_COMPONENT = R.ID_COMPONENT 
                LEFT JOIN ELAB_M_KATEGORI B ON A.ID_KATEGORI = B.ID_KATEGORI  
                WHERE A.IS_DELETE = '0'
                ORDER BY ID_UJI ASC
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_UJI (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_UJI SET ".implode(",", $data)." WHERE ID_UJI={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE ELAB_M_UJI SET IS_DELETE = 1  WHERE ID_UJI={$id}");
	}
    
    
	public function getProduk() {
		return $this->db->query("SELECT * FROM COQ_PRODUCT")->result_array();
	}
	public function getExisting($id) {
        $produk = ($id!='' ? "AND ID_PRODUCT = '{$id}'" : '');
		return $this->db->query("SELECT * FROM  ELAB_M_UJI WHERE PARENT IS NULL
         AND
                        IS_DELETE = 0 {$produk}
                    ORDER BY
                        ID_PRODUCT, ID_UJI")->result_array();
	}
	public function getKategori() {
		return $this->db->query("SELECT * FROM ELAB_M_KATEGORI")->result_array();
	}
	public function getCompany() {
		return $this->db->query("SELECT * FROM M_COMPANY")->result_array();
	}
	public function getComponent() {
		return $this->db->query("SELECT * FROM ELAB_M_COMPONENT")->result_array();
	}

}
?>