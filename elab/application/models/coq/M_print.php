<?php

class M_print extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_user = NULL) {
        $user = ($id_user=='' ? '' : " AND ID_USER = '{$id_user}'");
		return $this->db->query("SELECT * FROM COQ_REQUEST A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT
                LEFT JOIN COQ_PACK E ON A.ID_PACK = E.ID_PACK
                LEFT JOIN COQ_COUNTRY F ON A.ID_COUNTRY = F.ID_COUNTRY
                WHERE A.IS_DELETE = 0 {$user}
                ORDER BY ID_REQUEST DESC  
                ")->result_array();
	} 
	public function getPlantID($id) {
		return $this->db->query("SELECT * FROM COQ_T_PLANT A
                                LEFT JOIN COQ_PLANT B ON A.ID_PLANT = B.ID_PLANT
                                WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function getFileID($id) {
		return $this->db->query("SELECT * FROM COQ_FILE WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function cek_new($tabel, $id) {
		return $this->db->query("SELECT * from {$tabel} WHERE ID_REQUEST='{$id}'")->row_array();
	}

}
?>