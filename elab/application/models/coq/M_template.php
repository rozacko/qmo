<?php

class M_template extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT A.*, B.*, C.*, D.*, E.NM_REQUEST, E.ID_REQUEST FROM COQ_TEMPLATE_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_REQUEST E ON A.ID_REQUEST = E.ID_REQUEST 
                WHERE A.IS_DELETE = 0
                ORDER BY ID_TEMPLATE_HEADER DESC 
                                        
                ")->result_array();
	} 
    
    public function get_listParameter(/*$id_standart, $id_produk*/) {
        /*$standart = ($id_standart != '' ? " AND C.ID_STANDART = {$id_standart}" : '');
        $produk = ($id_produk != '' ? " AND A.ID_PRODUCT = {$id_produk}" : '');*/
		return $this->db->query("
                SELECT A.ID_UJI, NAMA_UJI,
                (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = A.PARENT) AS NAMA_PARENT, 
                SIMBOL, SATUAN, A.MARK, A.ID_PRODUCT, B.ID_METODE,B.ID_STANDART_UJI, C.NM_METODE, C.ID_STANDART, D.NM_STANDART  FROM ELAB_M_UJI A
                LEFT JOIN COQ_STANDART_UJI B ON A.ID_UJI = B.ID_PARAMETER
                LEFT JOIN COQ_METODE C ON B.ID_METODE = C.ID_METODE
                LEFT JOIN COQ_STANDART D ON C.ID_STANDART = D.ID_STANDART
                WHERE A.ID_PRODUCT IS NOT NULL AND B.ID_STANDART_UJI IS NOT NULL
                AND ID_UJI NOT IN ( SELECT PARENT FROM ELAB_M_UJI 
                WHERE PARENT IS NOT NULL 
                GROUP BY PARENT
                )
                ORDER BY A.ID_UJI ASC           
                ")->result_array();
	} 
    
	public function cek_template($id) {
		return $this->db->query("
           SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT 
            FROM
                COQ_TEMPLATE_DETAIL A
                JOIN COQ_TEMPLATE_HEADER B ON A.ID_TEMPLATE_HEADER = B.ID_TEMPLATE_HEADER
                JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
                JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
                JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where A.ID_TEMPLATE_HEADER = '{$id}' ORDER BY ID_TEMPLATE_DETAIL ASC")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO COQ_TEMPLATE_HEADER (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function simpanMessage($column, $data) {
		return $this->db->query("INSERT INTO COQ_CHAT (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_TEMPLATE_HEADER SET ".implode(",", $data)." WHERE ID_TEMPLATE_HEADER={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE COQ_TEMPLATE_HEADER SET IS_DELETE = 1 WHERE ID_TEMPLATE_HEADER={$id}");
	}
    
    
	public function addPlant($tabel, $data) {
		return  $this->db->insert_batch($tabel, $data); 
	}
     
	public function EditFile($token, $id) {
		return $this->db->query("UPDATE COQ_FILE SET ID_REQUEST = {$id} WHERE TOKEN={$token}");
	}
	
    
	public function getPacking() {
		return $this->db->query("SELECT * FROM COQ_PACK")->result_array();
	}
	public function getStandart() {
		return $this->db->query("SELECT * FROM COQ_STANDART")->result_array();
	}
	public function getTipe() {
		return $this->db->query("SELECT * FROM COQ_PRODUCT_TYPE")->result_array();
	}
	public function getProduk() {
		return $this->db->query("SELECT * FROM COQ_PRODUCT")->result_array();
	}
	public function getPlant() {
		return $this->db->query("SELECT * FROM COQ_PLANT")->result_array();
	}
	public function getCountry() {
		return $this->db->query("SELECT * FROM COQ_COUNTRY")->result_array();
	}
	public function getRequest() {
		return $this->db->query("SELECT * FROM COQ_REQUEST")->result_array();
	}
    
    
	public function getPlantID($id) {
		return $this->db->query("SELECT * FROM COQ_T_PLANT A
                                LEFT JOIN COQ_PLANT B ON A.ID_PLANT = B.ID_PLANT
                                WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function getFileID($id) {
		return $this->db->query("SELECT * FROM COQ_FILE WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function getChatID($id) {
		return $this->db->query("SELECT A.*, B.FULLNAME FROM COQ_CHAT A
                        JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
                        WHERE ID_REQUEST= '{$id}' order by A.ID_CHAT ASC")->result_array();
	}
    
    
	public function addTemplate($tabel, $data) {
		return  $this->db->insert_batch($tabel, $data); 
	}
	public function deleteTemplate($id) {
		return $this->db->query("DELETE FROM COQ_TEMPLATE_DETAIL WHERE ID_TEMPLATE_HEADER={$id}");
	}
     
    public function ambilId($tabel){
    	$this->db->limit(2);
    	$this->db->order_by("ID_TEMPLATE_HEADER","DESC");
    	$q = $this->db->get($tabel);
    	return $q;
    }
    

}
?>