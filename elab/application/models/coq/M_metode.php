<?php

class M_metode extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM COQ_METODE A
                JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                ")->result_array();
	} 
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO COQ_METODE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_METODE SET ".implode(",", $data)." WHERE ID_METODE={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  COQ_METODE WHERE ID_METODE={$id}");
	}
    
    
	public function getStandart() {
		return $this->db->query("SELECT * FROM COQ_STANDART")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}

}
?>