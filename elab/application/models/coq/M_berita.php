<?php

class M_berita extends CI_Model
{
    var $table = 'COQ_NEWS';
    var $column_order = array();
    var $column_search = array('ID_NEWS','TITLE','IMAGES','ISI_BERITA','CREATE_DATE');
    var $order = array('ID_NEWS' =>'desc');


    public function __construct()
    {
        parent::__construct();
        /*$this->db = $this->load->database('default', TRUE);*/
        $this->db = $this->load->database('default', TRUE);
       /* $this->hris = $this->load->database('hris', TRUE);*/
    }

    public function listallnews() {
        # code...
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    private function _get_datatables_query()
    {
       
       
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if($i===0) // first loop
                {
                    // $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if(count($this->column_search) - 1 == $i); //last loop
                    // $this->db->group_end(); //close bracket


            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

   
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
     public function count_all()
    {
       
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function add($data)
    {
        return $this->db->insert("COQ_NEWS",$data);
    }
    public function editData($id)
    {
         
        $this->db->select ( '*' ); 
        $this->db->from('COQ_NEWS');
        $this->db->where('ID_NEWS',$id);
        $query = $this->db->get();

        return $query->row();

    
    }
     public function setuju($where, $data, $table){
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    public function hapus($id){
        $this->db->where('ID_NEWS',$id);
        $this->db->delete('COQ_NEWS');
    }
}