<?php

class M_company extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function get_data()
    {
        return $this->db->query("SELECT * FROM M_COMPANY
                ")->result_array();
    }

    public function get_data_excel()
    {
        return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
    }

    public function simpan($column, $data)
    {
        return $this->db->query("INSERT INTO M_COMPANY (" . implode(",", $column) . ") VALUES (" . implode(",", $data) . ")");
    }

    public function update($id, $data)
    {
        return $this->db->query("UPDATE M_COMPANY SET " . implode(",", $data) . " WHERE ID_COMPANY={$id}");
    }

    public function hapus($id)
    {
        return $this->db->query("DELETE FROM  M_COMPANY WHERE ID_COMPANY={$id}");
    }


    public function getKelompok()
    {
        return $this->db->query("SELECT * FROM ELAB_M_KELOMPOK")->result_array();
    }

    public function getLokasi()
    {
        return $this->db->query("SELECT * FROM M_PLANT")->result_array();
    }

}

?>