<?php

class M_standart_uji extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM COQ_STANDART_UJI A
                JOIN ELAB_M_UJI B ON A.ID_PARAMETER = B.ID_UJI
                LEFT JOIN COQ_METODE C ON A.ID_METODE = C.ID_METODE
                JOIN COQ_PRODUCT D ON B.ID_PRODUCT = D.ID_PRODUCT
                ")->result_array();
	} 
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO COQ_STANDART_UJI (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_STANDART_UJI SET ".implode(",", $data)." WHERE ID_STANDART_UJI={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  COQ_STANDART_UJI WHERE ID_STANDART_UJI={$id}");
	}
    
    
	public function getParameter() {
		return $this->db->query("SELECT * FROM ELAB_M_UJI A
        JOIN COQ_PRODUCT B ON A.ID_PRODUCT = B.ID_PRODUCT
        ")->result_array();
	}
	public function getMetode() {
		return $this->db->query("SELECT * FROM COQ_METODE")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM M_PLANT")->result_array();
	}

}
?>