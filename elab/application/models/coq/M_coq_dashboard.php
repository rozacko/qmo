<?php

class M_coq_dashboard extends CI_Model
{


    public function show_active_bulanini()
    {
        $now = date('Ym');
        return $dataFinal = $this->db->query("SELECT Count(*) AS JML_REQUEST, us.FULLNAME  FROM COQ_REQUEST cr LEFT JOIN ELAB_M_USERS us ON us.ID_USER = cr.ID_USER WHERE cr.IS_DELETE =0
      AND TO_CHAR(cr.CREATE_DATE, 'YYYYMM') = '$now' GROUP BY cr.ID_USER, us.FULLNAME ORDER BY JML_REQUEST DESC")->result();

    }


    public function clinker_inproses_se()
    {
        
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CLINKER_SE FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SE' AND ID_PRODUCT = '2' 
                                        
                ");
    }
     public function clinker_inproses_sd()
    {
        
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CLINKER_SD FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SD' AND ID_PRODUCT = '2' 
                                        
                ");
    }
    public function clinker_inproses_ce()
    {
        
         return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CLINKER_CE FROM COQ_QUALITY_HEADER WHERE SUBSTR(KD_QUALITY,1,2)='CE' AND ID_PRODUCT = '2' 
                                        
                ");
    }
     public function clinker_inproses_cd()
    {
        
         return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CLINKER_CD FROM COQ_QUALITY_HEADER WHERE SUBSTR(KD_QUALITY,1,2)='CD' AND ID_PRODUCT = '2' 
                                        
                ");
    }
    ///////////////////////////////////////////////////////////





    public function clinker_bulanini_se()
    {
        $now = date('mY');
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CLINKER_BULAN_SE FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SE' AND ID_PRODUCT = '2' AND TO_CHAR(TANGGAL, 'MMYYYY')='$now' 
                                        
                ");

    }
    public function clinker_bulanini_sd()
    {
        $now = date('mY');
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CLINKER_BULAN_SD FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SD' AND ID_PRODUCT = '2' AND TO_CHAR(TANGGAL, 'MMYYYY')='$now' 
                                        
                ");
    }
    public function clinker_bulanini_ce()
    {
        $now = date('mY');
        return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CLINKER_BULAN_CE FROM 
                            COQ_QUALITY_HEADER 
                            WHERE SUBSTR(KD_QUALITY,1,2)='CE' 
                            AND ID_PRODUCT = '2' 
                            AND TO_CHAR(TO_DATE(TANGGAL), 'MMYYYY')='$now'
                                        
                ");
    }
    public function clinker_bulanini_cd()
    {
        $now = date('mY');
        return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CLINKER_BULAN_CD FROM 
                            COQ_QUALITY_HEADER 
                            WHERE SUBSTR(KD_QUALITY,1,2)='CD' 
                            AND ID_PRODUCT = '2' 
                            AND TO_CHAR(TO_DATE(TANGGAL), 'MMYYYY')='$now'
                                        
                ");
    }

    ////////////////////////////////////////////////


    public function clinker_tahunini_se()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_SPEC) AS JUMLAH_CLINKER_TAHUN_SE FROM COQ_SPEC_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_SPEC_HEADER = B.ID_JOIN
                                    WHERE SUBSTR(A.KD_SPEC,1,2)='SE' AND A.ID_PRODUCT = '2' AND TO_CHAR(A.TANGGAL, 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
    public function clinker_tahunini_sd()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_SPEC) AS JUMLAH_CLINKER_TAHUN_SD FROM COQ_SPEC_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_SPEC_HEADER = B.ID_JOIN
                                    WHERE SUBSTR(A.KD_SPEC,1,2)='SD' AND A.ID_PRODUCT = '2' AND TO_CHAR(A.TANGGAL, 'YYYY')='$now' AND STATUS ='4' 
                                        
                ");
    }
    public function clinker_tahunini_ce()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_QUALITY) AS JUMLAH_CLINKER_TAHUN_CE FROM COQ_QUALITY_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_QUALITY_HEADER = B.ID_JOIN WHERE SUBSTR(A.KD_QUALITY,1,2)='CE' AND ID_PRODUCT = '2' AND TO_CHAR(TO_DATE(A.TANGGAL), 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
    public function clinker_tahunini_cd()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_QUALITY) AS JUMLAH_CLINKER_TAHUN_CD FROM COQ_QUALITY_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_QUALITY_HEADER = B.ID_JOIN WHERE SUBSTR(A.KD_QUALITY,1,2)='CD' AND ID_PRODUCT = '2' AND TO_CHAR(TO_DATE(A.TANGGAL), 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
    ///////////////////CEMENT//////////////////////////////

     public function cement_inproses_se()
    {
        
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CEMENT_SE FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SE' AND ID_PRODUCT = '1' 
                                        
                ");
    }
     public function cement_inproses_sd()
    {
        
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CEMENT_SD FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SD' AND ID_PRODUCT = '1' 
                                        
                ");
    }
    public function cement_inproses_ce()
    {
        
         return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CEMENT_CE FROM COQ_QUALITY_HEADER WHERE SUBSTR(KD_QUALITY,1,2)='CE' AND ID_PRODUCT = '1' 
                                        
                ");
    }
     public function cement_inproses_cd()
    {
        
         return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CEMENT_CD FROM COQ_QUALITY_HEADER WHERE SUBSTR(KD_QUALITY,1,2)='CD' AND ID_PRODUCT = '1' 
                                        
                ");
    }


    /////////////////////////////////////
     public function cement_bulanini_se()
    {
        $now = date('mY');
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CEMENT_BULAN_SE FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SE' AND ID_PRODUCT = '1' AND TO_CHAR(TANGGAL, 'MMYYYY')='$now' 
                                        
                ");

    }
    public function cement_bulanini_sd()
    {
        $now = date('mY');
         return $this->db->query("SELECT COUNT(KD_SPEC) AS JUMLAH_CEMENT_BULAN_SD FROM COQ_SPEC_HEADER WHERE SUBSTR(KD_SPEC,1,2)='SD' AND ID_PRODUCT = '1' AND TO_CHAR(TANGGAL, 'MMYYYY')='$now' 
                                        
                ");
    }
    public function cement_bulanini_ce()
    {
        $now = date('mY');
        return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CEMENT_BULAN_CE FROM 
                            COQ_QUALITY_HEADER 
                            WHERE SUBSTR(KD_QUALITY,1,2)='CE' 
                            AND ID_PRODUCT = '1' 
                            AND TO_CHAR(TO_DATE(TANGGAL), 'MMYYYY')='$now'
                                        
                ");
    }
    public function cement_bulanini_cd()
    {
        $now = date('mY');
        return $this->db->query("SELECT COUNT(KD_QUALITY) AS JUMLAH_CEMENT_BULAN_CD FROM 
                            COQ_QUALITY_HEADER 
                            WHERE SUBSTR(KD_QUALITY,1,2)='CD' 
                            AND ID_PRODUCT = '1' 
                            AND TO_CHAR(TO_DATE(TANGGAL), 'MMYYYY')='$now'
                                        
                ");
    }

    //////////////////////////////////
     public function cement_tahunini_se()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_SPEC) AS JUMLAH_CEMENT_TAHUN_SE FROM COQ_SPEC_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_SPEC_HEADER = B.ID_JOIN
                                    WHERE SUBSTR(A.KD_SPEC,1,2)='SE' AND A.ID_PRODUCT = '1' AND TO_CHAR(A.TANGGAL, 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
    public function cement_tahunini_sd()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_SPEC) AS JUMLAH_CEMENT_TAHUN_SD FROM COQ_SPEC_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_SPEC_HEADER = B.ID_JOIN
                                    WHERE SUBSTR(A.KD_SPEC,1,2)='SD' AND A.ID_PRODUCT = '1' AND TO_CHAR(A.TANGGAL, 'YYYY')='$now' AND STATUS ='4' 
                                        
                ");
    }
    public function cement_tahunini_ce()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_QUALITY) AS JUMLAH_CEMENT_TAHUN_CE FROM COQ_QUALITY_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_QUALITY_HEADER = B.ID_JOIN WHERE SUBSTR(A.KD_QUALITY,1,2)='CE' AND ID_PRODUCT = '1' AND TO_CHAR(TO_DATE(A.TANGGAL), 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
    public function cement_tahunini_cd()
    {
        $now = date('Y');
         return $this->db->query("SELECT COUNT(A.KD_QUALITY) AS JUMLAH_CEMENT_TAHUN_CD FROM COQ_QUALITY_HEADER A
                                    LEFT JOIN COQ_APPROVAL B ON A.ID_QUALITY_HEADER = B.ID_JOIN WHERE SUBSTR(A.KD_QUALITY,1,2)='CD' AND ID_PRODUCT = '1' AND TO_CHAR(TO_DATE(A.TANGGAL), 'YYYY')='$now' AND STATUS ='4'
                                        
                ");
    }
}