<?php

class M_plant extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT * FROM COQ_PLANT A
                        LEFT JOIN M_COMPANY B  ON A.ID_COMPANY = B.ID_COMPANY
                ")->result_array();
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN COQ_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO COQ_PLANT (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_PLANT SET ".implode(",", $data)." WHERE ID_PLANT={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("DELETE FROM  COQ_PLANT WHERE ID_PLANT={$id}");
	}
    
    
	public function getCompany() {
		return $this->db->query("SELECT * FROM M_COMPANY")->result_array();
	}
	public function getLokasi() {
		return $this->db->query("SELECT * FROM COQ_PLANT")->result_array();
	}

}
?>