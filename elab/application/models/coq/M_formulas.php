<?php

class M_formulas extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data() {
		return $this->db->query("SELECT
                        A.*,
                        B.*
                    FROM
                        ELAB_M_FORMULAS A
                        LEFT JOIN ELAB_M_UJI B ON A.ID_PARAM_UJI = B.ID_UJI 
                    WHERE
                        A.IS_DELETE = 0 AND B.IS_DELETE = 0
                    ORDER BY
                        A.ID_FORMULAS
                ")->result_array();
	}
    
	public function getParameter() {
		return $this->db->query("SELECT * FROM ELAB_M_UJI A
        JOIN COQ_PRODUCT B ON A.ID_PRODUCT = B.ID_PRODUCT WHERE A.ALIASESPRM IS NOT NULL AND A.IS_DELETE = 0
        ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_FORMULAS (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_M_FORMULAS SET ".implode(",", $data)." WHERE ID_FORMULAS={$id}");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE ELAB_M_FORMULAS SET IS_DELETE = 1  WHERE ID_FORMULAS={$id}");
	}

}
?>