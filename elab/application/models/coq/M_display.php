<?php

class M_display extends CI_Model
{
	 var $table = 'COQ_REQUEST';
    var $column_order = array(); 
    var $column_search = array('ID_REQUEST');
    var $order = array('ID_REQUEST','desc');
	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
     
 
   
 private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // looping awal
        {
            if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // looping awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        return $this->db->query("SELECT * FROM COQ_REQUEST A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT
                LEFT JOIN COQ_PACK E ON A.ID_PACK = E.ID_PACK
                LEFT JOIN COQ_COUNTRY F ON A.ID_COUNTRY = F.ID_COUNTRY
                WHERE A.IS_DELETE = 0 
                ORDER BY ID_REQUEST DESC  
                ")->result_array();
    
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    public function get_id($id)
    {
        $this->db->select('*');
         $this->db->from('COQ_REQUEST A');
        $this->db->join('COQ_STANDART B','A.ID_STANDART = B.ID_STANDART','left');
        $this->db->join('COQ_PRODUCT_TYPE C','A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE','left');
        $this->db->join('COQ_PRODUCT D','A.ID_PRODUCT = D.ID_PRODUCT','left');
        $this->db->join('COQ_PACK E','A.ID_PACK = E.ID_PACK','left');
        $this->db->join('COQ_COUNTRY F','A.ID_COUNTRY = F.ID_COUNTRY','left');
        $this->db->where('A.ID_REQUEST',$id);
        $query = $this->db->get();

        return $query->row();
    }
	function get_data($id)
    {
        return $this->db->query("SELECT * FROM COQ_REQUEST A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT
                LEFT JOIN COQ_PACK E ON A.ID_PACK = E.ID_PACK
                LEFT JOIN COQ_COUNTRY F ON A.ID_COUNTRY = F.ID_COUNTRY
                LEFT JOIN COQ_T_PLANT G ON A.ID_REQUEST = G.ID_REQUEST
                LEFT JOIN M_PLANT H ON G.ID_PLANT = H.ID_PLANT
                LEFT JOIN M_COMPANY I ON H.ID_COMPANY = I.ID_COMPANY
                WHERE A.ID_REQUEST='{$id}'
                ")->result_array();
    
    }
    public function cek_spec($id) {
        return $this->db->query("
          SELECT A.*, D.*, E.NM_METODE  FROM COQ_SPEC_DETAIL A
            JOIN COQ_SPEC_HEADER B ON A.ID_SPEC_HEADER = B.ID_SPEC_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
                JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  B.ID_REQUEST = '{$id}'	ORDER BY A.ID_SPEC_DETAIL ASC
            ")->result_array();
    }

}
