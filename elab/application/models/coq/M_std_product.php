<?php

class M_std_product extends CI_Model
{

    var $table = 'COQ_STANDART_PRODUCT A';
    var $column_order = array(); 
    var $column_search = array('A.ID_PRODUCT_TYPE','A.ID_STANDART');
    var $order = array('A.ID_STD' => 'asc'); 
 
    public function __construct()
    {
        parent::__construct(); 
		$this->db = $this->load->database('default', TRUE);
		//$this->hris = $this->load->database('hris', TRUE);
    }
 
    private function _get_datatables_query()
    {
		
		$this->db->from($this->table);
		
		//$query = $this->db->get();

        $i = 0;
     
        foreach ($this->column_search as $item)
        {
            if($_POST['search']['value'])
            {
                if($i===0)
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order']))
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
       return $this->db->query('SELECT * FROM COQ_STANDART_PRODUCT A
								LEFT JOIN COQ_PRODUCT_TYPE B ON A.ID_PRODUCT_TYPE = B.ID_PRODUCT_TYPE
								LEFT JOIN COQ_STANDART C ON A.ID_STANDART = C.ID_STANDART')->result_array();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function add($data)
	{
		return $this->db->insert("COQ_STANDART_PRODUCT",$data);
	}
	public function hapus($id){
		$this->db->where('ID_STD',$id);
		$this->db->delete('COQ_STANDART_PRODUCT');
	}
	public function get_id($id)
	{
		$this->db->from('COQ_STANDART_PRODUCT');
		
		$this->db->where('ID_STD',$id);
		$query = $this->db->get();

		return $query->row();
	}
	public function updateData($where, $data, $table){
		$this->db->where($where);
		$this->db->update($table, $data);
	}
	/*public function ambil_data(){
		$sql = $this->db->query('SELECT * FROM COQ_STANDART_PRODUCT A.*,B.*,C.ID_STANDART,C.NM_STANDART AS NAMA_STANDART
								LEFT JOIN COQ_PRODUCT_TYPE B ON A.NM_PRODUCT_TYPE = B.ID_PRODUCT_TYPE
								LEFT JOIN COQ_STANDART C ON A.NM_STANDART = C.ID_STANDART');
		return $sql->result_array();
	}*/
	
	public function product(){
		$sql = $this->db->query('SELECT * FROM COQ_PRODUCT_TYPE');
		return $sql;
	}
	public function standart(){
		$sql = $this->db->query('SELECT * FROM COQ_STANDART');
		return $sql;
	}

}
?>