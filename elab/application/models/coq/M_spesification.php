<?php

class M_spesification extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', TRUE);
    }

    public function get_data($plant = NULL)
    {
        /*$id_plant = ($plant == '9999' || $plant == '' ? ' ' : " AND A.ID_PLANT IN ({$plant})");*/
        if ($plant == '9999' || $plant == '') {
            $id_plant = '';
        }elseif ($plant == '9998') {
            $id_plant = "AND A.ID_PLANT IN (8,9,10,11)";
        }elseif ($plant == '9997') {
            $id_plant = "AND A.ID_PLANT IN (1,42,41)";
        }elseif ($plant == '9996') {
            $id_plant = "AND A.ID_PLANT IN (4,5)";
        }else{
            $id_plant = " AND A.ID_PLANT IN ({$plant})";
        }
        return $this->db->query("SELECT A.*, B.*, C.*, D.*, E.NM_REQUEST, E.ID_REQUEST, F.NM_PLANT,G.* FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_REQUEST E ON A.ID_REQUEST = E.ID_REQUEST 
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT
                LEFT JOIN COQ_BRAND G ON A.ID_BRAND = G.ID_BRAND
                WHERE A.IS_DELETE = 0 {$id_plant}
                ORDER BY ID_SPEC_HEADER DESC  
                                        
                ")->result_array();
    }

    public function get_listParameter($id_standart, $id_produk)
    {
        $standart = ($id_standart != '' ? " AND (C.ID_STANDART = {$id_standart} OR C.ID_STANDART IS NULL)" : '');
        $produk = ($id_produk != '' ? " AND A.ID_PRODUCT = {$id_produk}" : '');
        return $this->db->query("
                SELECT A.ID_UJI,
                (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = A.PARENT) AS NAMA_PARENT, 
                NAMA_UJI, SIMBOL, SATUAN, A.MARK, A.ID_PRODUCT, B.ID_METODE, C.NM_METODE, C.ID_STANDART, D.NM_STANDART, B.ID_STANDART_UJI  FROM ELAB_M_UJI A
                JOIN COQ_STANDART_UJI B ON A.ID_UJI = B.ID_PARAMETER
                LEFT JOIN COQ_METODE C ON B.ID_METODE = C.ID_METODE
                LEFT JOIN COQ_STANDART D ON C.ID_STANDART = D.ID_STANDART
                WHERE A.ID_PRODUCT IS NOT NULL  {$standart}  {$produk}
                AND ID_UJI NOT IN ( SELECT PARENT FROM ELAB_M_UJI 
                WHERE PARENT IS NOT NULL 
                GROUP BY PARENT
                )
                ORDER BY A.ID_UJI ASC           
                ")->result_array();
    }

    public function get_listTemplate()
    {
        return $this->db->query("SELECT A.*, B.*, C.*, D.*, E.NM_REQUEST, E.ID_REQUEST FROM COQ_TEMPLATE_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_REQUEST E ON A.ID_REQUEST = E.ID_REQUEST 
                WHERE A.IS_DELETE = '0'
                ORDER BY ID_TEMPLATE_HEADER DESC 
                                        
                ")->result_array();
    }


    public function cek_spec($id)
    {
        return $this->db->query("
             SELECT A.*, D.*, E.NM_METODE  FROM COQ_SPEC_DETAIL A
            JOIN COQ_SPEC_HEADER B ON A.ID_SPEC_HEADER = B.ID_SPEC_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_SPEC_HEADER = '{$id}'	ORDER BY A.ID_SPEC_DETAIL ASC")->result_array();
    }

    public function cek_template($id)
    {
        return $this->db->query("
            SELECT A.*, D.*, E.NM_METODE  FROM COQ_TEMPLATE_DETAIL A
            JOIN COQ_TEMPLATE_HEADER B ON A.ID_TEMPLATE_HEADER = B.ID_TEMPLATE_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
                JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where A.ID_TEMPLATE_HEADER = '{$id}' ORDER BY ID_TEMPLATE_DETAIL")->result_array();
    }
     public function tampil()
    {
        return $this->db->query("
            SELECT A.*, D.*, E.NM_METODE  FROM COQ_TEMPLATE_DETAIL A
            JOIN COQ_TEMPLATE_HEADER B ON A.ID_TEMPLATE_HEADER = B.ID_TEMPLATE_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
                JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
             ORDER BY ID_TEMPLATE_DETAIL")->result_array();
    }

    public function simpan($column, $data)
    {
        $this->db->query("INSERT INTO COQ_SPEC_HEADER (" . implode(",", $column) . ") VALUES (" . implode(",", $data) . ")");
        $this->db->limit(2);
        $this->db->order_by("ID_SPEC_HEADER", "DESC");
        $q = $this->db->get('COQ_SPEC_HEADER');
        return $q->result();
    }

    public function simpanMessage($column, $data)
    {
        return $this->db->query("INSERT INTO COQ_CHAT (" . implode(",", $column) . ") VALUES (" . implode(",", $data) . ")");
    }

    public function simpan_approve($column, $data)
    {
        return $this->db->query("INSERT INTO COQ_APPROVAL (" . implode(",", $column) . ") VALUES (" . implode(",", $data) . ")");
    }

    public function update($id, $data)
    {
        return $this->db->query("UPDATE COQ_SPEC_HEADER SET " . implode(",", $data) . " WHERE ID_SPEC_HEADER={$id}");
    }

    public function update_status_request($id)
    {
        return $this->db->query("UPDATE COQ_REQUEST SET STATUS = 1 WHERE ID_REQUEST={$id}");
    }

    public function updateKode($id, $kode)
    {
        return $this->db->query("UPDATE COQ_SPEC_HEADER SET KD_SPEC = '{$kode}' WHERE ID_SPEC_HEADER={$id}");
    }

    public function hapus($id)
    {
        return $this->db->query("UPDATE COQ_SPEC_HEADER SET IS_DELETE = 1  WHERE ID_SPEC_HEADER={$id}");
    }


    public function addPlant($tabel, $data)
    {
        return $this->db->insert_batch($tabel, $data);
    }

    public function ambilId($tabel)
    {
        $this->db->limit(1);
        $this->db->order_by("ID_SPEC_HEADER", "DESC");
        $q = $this->db->get($tabel);
        return $q;
    }

    public function EditFile($token, $id)
    {
        return $this->db->query("UPDATE COQ_FILE SET ID_REQUEST = {$id} WHERE TOKEN={$token}");
    }


    public function getKapal($id)
    {
        return $this->db->query("SELECT * FROM COQ_REQUEST where ID_REQUEST = '{$id}'")->row_array();
    }

    public function getPacking()
    {
        return $this->db->query("SELECT * FROM COQ_PACK")->result_array();
    }

    public function getStandart()
    {
        return $this->db->query("SELECT * FROM COQ_STANDART")->result_array();
    }

    public function getTipe()
    {
        return $this->db->query("SELECT * FROM COQ_PRODUCT_TYPE")->result_array();
    }
    public function getBrand($id)
    {
        return $this->db->query("SELECT * FROM COQ_PRODUCT_TYPE A LEFT JOIN COQ_BRAND B ON A.ID_PRODUCT_TYPE=B.ID_PRODUCT_TYPE WHERE A.ID_PRODUCT_TYPE='{$id}'")->result_array();
    }

    public function getProduk()
    {
        return $this->db->query("SELECT * FROM COQ_PRODUCT")->result_array();
    }

    public function getPlant()
    {
        return $this->db->query("SELECT * FROM COQ_PLANT")->result_array();
    }

    public function getCountry()
    {
        return $this->db->query("SELECT * FROM COQ_COUNTRY")->result_array();
    }

    public function getRequest()
    {
        return $this->db->query("SELECT * FROM COQ_REQUEST where is_delete = 0")->result_array();
    }


    public function getPlantID($id)
    {
        return $this->db->query("SELECT * FROM COQ_T_PLANT A
                                LEFT JOIN COQ_PLANT B ON A.ID_PLANT = B.ID_PLANT
                                WHERE ID_REQUEST= '{$id}'")->result_array();
    }

    public function getFileID($id)
    {
        return $this->db->query("SELECT * FROM COQ_FILE WHERE ID_REQUEST= '{$id}'")->result_array();
    }

    public function getChatID($id)
    {
        return $this->db->query("SELECT A.*, B.FULLNAME FROM COQ_CHAT A
                        JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
                        WHERE ID_REQUEST= '{$id}' order by A.ID_CHAT ASC")->result_array();
    }


    public function addTemplate($tabel, $data)
    {
        return $this->db->insert_batch($tabel, $data);
    }

    public function deleteTemplate($id)
    {
        return $this->db->query("DELETE FROM COQ_SPEC_DETAIL WHERE ID_SPEC_HEADER={$id}");
    }
    public function cekTo($role, $plant){
        $sql = $this->db->query("
            SELECT * FROM (             SELECT
                A.ID_USER, EMAIL,B.FULLNAME
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_PLANT = '{$plant}' AND B.ISACTIVE='Y'
            GROUP BY A.ID_USER, EMAIL,B.FULLNAME   
            ORDER BY A.ID_USER) WHERE ROWNUM = '1'
");
        return $sql->row_array();
    }
    function get_data_template($id)
    {
        return $this->db->query("SELECT * FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON A.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
                LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_SPEC_HEADER = '{$id}' 
                ORDER BY ID_SPEC_HEADER DESC  
                ")->result_array();

    }
    function get_data_template2($id,$idStand)
    {
        return $this->db->query("SELECT * FROM COQ_SPEC_HEADER A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT 
                LEFT JOIN COQ_STANDART_PRODUCT E ON A.ID_PRODUCT_TYPE = E.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PLANT F ON A.ID_PLANT = F.ID_PLANT
                LEFT JOIN M_COMPANY G ON F.ID_COMPANY = G.ID_COMPANY
                LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                WHERE ID_SPEC_HEADER = '{$id}' AND E.ID_STANDART = '{$idStand}'
                ORDER BY ID_SPEC_HEADER DESC  
                ")->result_array();

    }

    public function cek_spec_template($id)
    {
        return $this->db->query("
         SELECT A.*, D.*,E.NM_METODE, (SELECT NAMA_UJI FROM ELAB_M_UJI WHERE ID_UJI = D.PARENT) AS NAMA_PARENT FROM COQ_SPEC_DETAIL A
            JOIN COQ_SPEC_HEADER B ON A.ID_SPEC_HEADER = B.ID_SPEC_HEADER
            JOIN COQ_STANDART_UJI C ON A.ID_STANDART_UJI = C.ID_STANDART_UJI
            JOIN ELAB_M_UJI D ON A.ID_UJI = D.ID_UJI
            LEFT JOIN COQ_METODE E ON E.ID_METODE = C.ID_METODE
            where  A.ID_SPEC_HEADER = '{$id}' ORDER BY A.ID_SPEC_DETAIL ASC
            ")->result_array();
    }

    public function get_user($id)
    {
        return $this->db->query("
          SELECT A.*,B.NM_SPEC,C.FULLNAME FROM COQ_APPROVAL A
			JOIN COQ_SPEC_HEADER B ON B.ID_SPEC_HEADER = A.ID_JOIN
			JOIN ELAB_M_USERS C ON C.ID_USER = A.ID_USER WHERE A.ID_JOIN ='{$id}' order by a.id_approval asc
            ")->result_array();
    }

    public function cek_status($id, $plant)
    {
        $this->db->limit(1);
        $this->db->join('COQ_SPEC_HEADER', 'COQ_SPEC_HEADER.ID_SPEC_HEADER = COQ_APPROVAL.ID_JOIN');
        $this->db->where('ID_JOIN', $id);
        $this->db->where('COQ_SPEC_HEADER.ID_PLANT', $plant);
        $this->db->where('TIPE','0');
        $this->db->order_by("ID_APPROVAL", "DESC");
        $q = $this->db->get('COQ_APPROVAL');
        return $q;
    }
    public function ambilStand($tabel, $id){
        $q = $this->db->query("select * from {$tabel} where ID_QUALITY_HEADER = '{$id}'");
        return $q;
    }
}

?>