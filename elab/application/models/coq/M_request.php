<?php

class M_request extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_user = NULL, $id_plant = NULL) {
   /* $user = ($id_user=='' ? " AND G.ID_PLANT IN ({$id_plant})" : " AND ID_USER = '{$id_user}'");*/
    
    if ($id_plant == '9999') {
        $user = "";
    }elseif ($id_plant == '9998') {
            $user = "AND G.ID_PLANT IN (8,9,10,11)";
        }elseif ($id_plant == '9997') {
            $user = "AND G.ID_PLANT IN (1,42,41)";
        }elseif ($id_plant == '9996') {
            $user = "AND G.ID_PLANT IN (4,5)";
        }else{
            $user = " AND G.ID_PLANT IN ({$id_plant})";
        }
        
    
		return $this->db->query("SELECT * FROM COQ_REQUEST A
                LEFT JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                LEFT JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE
                LEFT JOIN COQ_PRODUCT D ON A.ID_PRODUCT = D.ID_PRODUCT
                LEFT JOIN COQ_PACK E ON A.ID_PACK = E.ID_PACK
                LEFT JOIN COQ_COUNTRY F ON A.ID_COUNTRY = F.ID_COUNTRY
                LEFT JOIN COQ_T_PLANT G ON A.ID_REQUEST = G.ID_REQUEST 
                LEFT JOIN COQ_BRAND H ON A.ID_BRAND = H.ID_BRAND
                LEFT JOIN COQ_PLANT I ON G.ID_PLANT = I.ID_PLANT
                WHERE A.IS_DELETE = 0  {$user}
               
                ORDER BY A.ID_REQUEST DESC  
                ")->result_array();
    
	} 
	public function getDetailReq($id) {
		return $this->db->query("SELECT A.*, A.KD_REQUEST AS KODE,  NM_STANDART, NM_PRODUCT_TYPE,E.NM_PLANT
                FROM COQ_REQUEST A
                JOIN COQ_STANDART B ON A.ID_STANDART = B.ID_STANDART
                JOIN COQ_PRODUCT_TYPE C ON A.ID_PRODUCT_TYPE = C.ID_PRODUCT_TYPE 
                JOIN COQ_T_PLANT D ON A.ID_REQUEST = D.ID_REQUEST
                JOIN COQ_PLANT E ON D.ID_PLANT = E.ID_PLANT
                where A.ID_REQUEST = '{$id}'
                ORDER BY A.ID_REQUEST DESC
               ")->row_array();
	}
	public function cekTo($role, $plant){
        $sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL,B.FULLNAME
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_PLANT = '{$plant}' 
            GROUP BY A.ID_USER, EMAIL,B.FULLNAME   
            ORDER BY
                A.ID_USER
");
        return $sql->row_array();
    }
    public function cekToSa($role, $plant,$id_user){
        $sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL,B.FULLNAME
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_PLANT = '{$plant}' AND A.ID_USER ='{$id_user}'
            GROUP BY A.ID_USER, EMAIL,B.FULLNAME   
            ORDER BY
                A.ID_USER
");
        return $sql->row_array();
    }
	public function simpan($column, $data) {
		 $this->db->query("INSERT INTO COQ_REQUEST (".implode(",", $column).") VALUES (".implode(",", $data).")");
           $this->db->limit(2);
            $this->db->order_by("ID_REQUEST","DESC");
            $q = $this->db->get('COQ_REQUEST');
            return $q->result();
	}
	public function getBrand($id)
    {
        return $this->db->query("SELECT * FROM COQ_PRODUCT_TYPE A LEFT JOIN COQ_BRAND B ON A.ID_PRODUCT_TYPE=B.ID_PRODUCT_TYPE WHERE A.ID_PRODUCT_TYPE='{$id}'")->result_array();
    }
	public function simpanMessage($column, $data) {
		return $this->db->query("INSERT INTO COQ_CHAT (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_tolakRequest($column, $data) {
		return $this->db->query("INSERT INTO COQ_APPROVAL (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE COQ_REQUEST SET ".implode(",", $data)." WHERE ID_REQUEST={$id}");
	}
	
	public function updateKode($id, $kode) {
		return $this->db->query("UPDATE COQ_REQUEST SET KD_REQUEST = '{$kode}' WHERE ID_REQUEST={$id}");
	}
	
	
	public function hapus($id) {
		return $this->db->query("UPDATE COQ_REQUEST SET IS_DELETE = 1  WHERE ID_REQUEST={$id}");
	}
    
    
	public function addPlant($tabel, $data) {
		return  $this->db->insert_batch($tabel, $data); 
	}
    
    
    public function ambilId($tabel){
    	$this->db->limit(2);
    	$this->db->order_by("ID_REQUEST","DESC");
    	$q = $this->db->get($tabel);
    	return $q;
    }
    
	public function EditFile($token, $id) {
		return $this->db->query("UPDATE COQ_FILE SET ID_REQUEST = {$id} WHERE TOKEN={$token}");
	}
	
    
	public function getPacking() {
		return $this->db->query("SELECT * FROM COQ_PACK")->result_array();
	}
	public function getStandart() {
		return $this->db->query("SELECT * FROM COQ_STANDART")->result_array();
	}
	public function getTipe() {
		return $this->db->query("SELECT * FROM COQ_PRODUCT_TYPE")->result_array();
	}
	public function getProduk() {
		return $this->db->query("SELECT * FROM COQ_PRODUCT")->result_array();
	}
	public function getPlant() {
		return $this->db->query("SELECT * FROM COQ_PLANT")->result_array();
	}
	public function getCountry() {
		return $this->db->query("SELECT * FROM COQ_COUNTRY")->result_array();
	}
    
    
	public function getPlantID($id) {
		return $this->db->query("SELECT * FROM COQ_T_PLANT A
                                LEFT JOIN COQ_PLANT B ON A.ID_PLANT = B.ID_PLANT
                                WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function getFileID($id) {
		return $this->db->query("SELECT * FROM COQ_FILE WHERE ID_REQUEST= '{$id}'")->result_array();
	}
	public function getChatID($id) {
		return $this->db->query("SELECT A.*, B.FULLNAME FROM COQ_CHAT A
                        JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
                        WHERE ID_REQUEST= '{$id}' order by A.ID_CHAT ASC")->result_array();
	}
	public function cek_new($tabel, $id, $id_plant = NULL) {
        $plant = ($id_plant=='' ? '' : " AND ID_PLANT = '{$id_plant}'");
		return $this->db->query("SELECT * from {$tabel} WHERE ID_REQUEST={$id} {$plant}")->row_array();
	}
     
    public function cek_status($id, $plant){
    	$this->db->limit(1);
         $this->db->where('ID_REQUEST', $id);
         $this->db->where('ID_PLANT', $plant);
    	$this->db->order_by("ID_APPROVAL","DESC");
    	$q = $this->db->get('COQ_APPROVAL');
    	return $q;
    }
	public function getDetailRequest($id, $status, $plant) {
		return $this->db->query("SELECT
                        A.*,
                        B.KD_REQUEST AS KODE,
                        NM_STANDART,
                        NM_PRODUCT_TYPE,NM_PLANT,NM_COMPANY 
                    FROM
                        COQ_APPROVAL A
                        JOIN COQ_REQUEST B ON A.ID_REQUEST = B.ID_REQUEST
                        JOIN COQ_STANDART C ON B.ID_STANDART = C.ID_STANDART
                        JOIN COQ_PRODUCT_TYPE D ON B.ID_PRODUCT_TYPE = D.ID_PRODUCT_TYPE 
                        JOIN COQ_PLANT E ON A.ID_PLANT = E.ID_PLANT 
                        JOIN M_COMPANY F ON E.ID_COMPANY = F.ID_COMPANY 
                    WHERE
                        A.ID_REQUEST  = '{$id}' AND A.STATUS = '{$status}' AND A.ID_PLANT = '{$plant}'
                    ORDER BY
                         A.ID_REQUEST
               ")->row_array();
	}

}
?>