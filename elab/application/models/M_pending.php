<?php

class M_pending extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_lab, $on, $id_kategori ) { 
		return $this->db->query("
                  SELECT A.STATUS, A.ID_TRANSAKSI,A.ID_KATEGORI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL, A.KETERANGAN,A.ID_KATEGORI
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                       where A.STATUS in ({$on})  and KODE_LAB = '{$id_lab}'   and A.ID_KATEGORI = '{$id_kategori}' 
                     ORDER BY A.ID_TRANSAKSI
                     
                ")->result_array();	
                
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
    public function cek_a2($id, $kategori) {
		return $this->db->query("SELECT * FROM ELAB_T_APPROVE 
        WHERE STATUS in ('a2', 't0' ) AND ID_TRANSAKSI = {$id}  AND ID_KATEGORI = {$kategori} 
        
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $status, $data) {
		return $this->db->query("UPDATE ELAB_T_APPROVE SET ".implode(",", $data)." WHERE STATUS='{$status}' AND ID_TRANSAKSI = {$id}");
	}
	
    public function cek_approve($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO, ID_KATEGORI from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and  STATUS in ({$status}) {$kategori}
                    ORDER BY STATUS DESC
                ")->result_array();
	} 
    public function cek_dislike($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO, ID_KATEGORI , KETERANGAN_TOLAK from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and  STATUS in ({$status})  {$kategori}
                    ORDER BY ID_KATEGORI ASC
                ")->row_array();
	} 
    public function cek_off($id, $off, $id_kategori) {
		return $this->db->query("   SELECT A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                      where A.STATUS = '{$off}' AND A.ID_TRANSAKSI = '{$id}' AND A.ID_KATEGORI = {$id_kategori}
                     ORDER BY A.ID_TRANSAKSI
                ")->result_array();	 
	}
    
   public function cek_kategori($id, $ID_KATEGORI) {
		return $this->db->query("select 
                    B.ID_KATEGORI, C.WARNA, C.NM_KATEGORI  
                    from ELAB_T_BOX  A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    JOIN ELAB_M_KATEGORI C ON C.ID_KATEGORI = B.ID_KATEGORI 
                    where ID_TRANSAKSI = '{$id}' and B.ID_KATEGORI = '{$ID_KATEGORI}' 
                     group by B.ID_KATEGORI, C.WARNA, C.NM_KATEGORI
                ")->row_array();
	} 
    
    public function cek_tombol($id, $status, $kategori) {
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' and ID_KATEGORI = '{$kategori}'
                    ORDER BY STATUS DESC
                ")->row_array();
	}
     
	public function get_spuc($id ) {
		return $this->db->query("SELECT * FROM ELAB_T_BPPUC WHERE ID_TRANSAKSI = {$id}
                ")->row_array();
	}
    
	public function cekTo($role, $lab, $kategori){
        $id_kategori = ($kategori=='' ? '' : " AND A.ID_KATEGORI IN ({$kategori}) ");
		$sql = $this->db->query("
            SELECT
                A.ID_USER, EMAIL
            FROM
                ELAB_M_USERLAB A
                JOIN ELAB_M_USERS B ON A.ID_USER = B.ID_USER 
            WHERE
                A.ID_ROLE = '{$role}' 
                AND A.ID_LAB = '{$lab}' {$id_kategori}
            GROUP BY A.ID_USER, EMAIL	
            ORDER BY
                A.ID_USER
");
		return $sql->result_array();
	}

}
?>