<?php

class M_dashboard extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_lab  ) { 
		return $this->db->query("
                 SELECT A.ID_TRANSAKSI, NO_BAPPUC, A.TANGGAL, B.NAMA_CONTOH, A.KETERANGAN from ELAB_T_BPPUC  A 
                        join 	ELAB_M_CONTOH B ON B.ID_CONTOH = A.ID_CONTOH 
                       where KODE_LAB = '{$id_lab}' 
                ORDER BY ID_TRANSAKSI 
                     
                ")->result_array();	
                
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_T_BPPUC SET ".implode(",", $data)." WHERE ID_TRANSAKSI={$id}");
	}
	
    public function cek_approve($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO, ID_KATEGORI from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and  STATUS in ({$status}) {$kategori}
                    ORDER BY STATUS DESC
                ")->result_array();
	} 
    public function cek_dislike($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO, ID_KATEGORI from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and  STATUS in ({$status})  {$kategori}
                    ORDER BY ID_KATEGORI ASC
                ")->row_array();
	} 
    public function cek_off($id, $off, $id_kategori) {
		return $this->db->query("   SELECT A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                      where A.STATUS = '{$off}' AND A.ID_TRANSAKSI = '{$id}' AND A.ID_KATEGORI = {$id_kategori}
                     ORDER BY A.ID_TRANSAKSI
                ")->result_array();	 
	}
    
   public function cek_kategori($id, $ID_KATEGORI) {
		return $this->db->query("select 
                    B.ID_KATEGORI, C.WARNA, C.NM_KATEGORI  
                    from ELAB_T_BOX  A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    JOIN ELAB_M_KATEGORI C ON C.ID_KATEGORI = B.ID_KATEGORI 
                    where ID_TRANSAKSI = '{$id}' and B.ID_KATEGORI = '{$ID_KATEGORI}' 
                     group by B.ID_KATEGORI, C.WARNA, C.NM_KATEGORI
                ")->row_array();
	} 
    
    public function cek_tombol($id, $status, $id_kategori) {
          if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' {$kategori}
                    ORDER BY STATUS DESC
                ")->row_array();
	} 
     
   public function get_spuc_penyelia($id) {
		return $this->db->query("SELECT B.ID_KATEGORI FROM ELAB_T_BOX A
                    JOIN ELAB_M_UJI B ON A.ID_UJI = B.ID_UJI
                    WHERE ID_TRANSAKSI = {$id}
                    GROUP BY  B.ID_KATEGORI
                ")->result_array();
	} 

	
}
?>