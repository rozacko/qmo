<?php

class M_approvespuc extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		$this->db = $this->load->database('default', TRUE);
	}
    
    public function get_data($id_lab, $on ) { 
		return $this->db->query("
                  SELECT A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL 
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                        JOIN ELAB_M_USERS D ON A.ID_USER = D.ID_USER
                       where A.STATUS in ({$on})  and KODE_LAB = '{$id_lab}' 
                       group by A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL 
                     ORDER BY A.ID_TRANSAKSI
                     
                ")->result_array();	
                
	}
    public function get_data_excel() {
		return $this->db->query("SELECT * FROM ELAB_M_CONTOH A
                LEFT JOIN ELAB_M_KELOMPOK B ON A.ID_KELOMPOK = B.ID_KELOMPOK
                LEFT JOIN M_PLANT B ON A.KD_PLANT = B.KD_PLANT  
                ")->result_array();
	}
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO ELAB_M_CONTOH (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	public function simpan_approve($column, $data) {
		return $this->db->query("INSERT INTO ELAB_T_APPROVE (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function update($id, $data) {
		return $this->db->query("UPDATE ELAB_T_BPPUC SET ".implode(",", $data)." WHERE ID_TRANSAKSI={$id}");
	}
	
    public function cek_approve($id, $status, $id_kategori) {
        if($id_kategori==''){
             $kategori = '';
        }else{
             $kategori = " and ID_KATEGORI = {$id_kategori}";
        }
        // $kategori = ($id_kategori=='' ? '' : " and ID_KATEGORI = {$id_kategori}")
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' {$kategori}
                    ORDER BY STATUS DESC
                ")->row_array();
	} 
    public function cek_off($id, $off, $id_kategori) {
		return $this->db->query("   SELECT A.ID_TRANSAKSI, A.STATUS, C.NAMA_CONTOH, B.NO_BAPPUC, B.TANGGAL
                    FROM
                        ELAB_T_APPROVE A
                        join 	ELAB_T_BPPUC B ON A.ID_TRANSAKSI = B.ID_TRANSAKSI
                        join 	ELAB_M_CONTOH C ON C.ID_CONTOH = B.ID_CONTOH 
                      where A.STATUS = '{$off}' AND A.ID_TRANSAKSI = '{$id}' AND A.ID_KATEGORI = {$id_kategori}
                     ORDER BY A.ID_TRANSAKSI
                ")->result_array();	 
	}
    
    public function cek_tolak($id, $STATUS) {
		return $this->db->query("select A.*, B.*, to_char(A.TANGGAL, 'YYYY-MM-DD HH:MI:SS') as  TGL_TOLAK  from
                        ELAB_T_APPROVE A
                        JOIN ELAB_M_USERS B ON A.ID_USER = A.ID_USER 
                    where ID_TRANSAKSI = '{$id}' and STATUS in ({$STATUS}) 
                ")->row_array();
	} 
 
    
    public function cek_tombol($id, $status, $kategori) {
		return $this->db->query("select STATUS, CITO from ELAB_T_APPROVE  A 
                    where ID_TRANSAKSI = '{$id}' and STATUS = '{$status}' and ID_KATEGORI = '{$kategori}'
                    ORDER BY STATUS DESC
                ")->row_array();
	}
     

}
?>