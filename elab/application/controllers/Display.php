
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_print');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Display SPEC/QUALITY";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/print', $data);
	}
    
     
	 public function get_data()
    { 
        
        $sess_user = $this->session->userdata("USERLAB");
         $session_user = '';
         $session_role = '';
         $user = '';
        foreach($sess_user as $v){
            $session_user .= $v['ID_USER'].",";
            $session_role .= $v['ID_ROLE'].",";
        }  
        $role = rtrim($session_role, ",");
        $detail['role'] = $role;
        if($role=='31'){ 
            $user = rtrim($session_user, ",");
        }
        // $detail_data = $this->M_mt->get_data(rtrim($session_labor, ","));
        $detail  = $this->M_print->get_data($user);
        $getdata  = array();  
            foreach ($detail as $val) {  
                $tipe_request = ($val['TIPE_REQUEST']==0 ? 'COQ_SPEC_HEADER' :'COQ_QUALITY_HEADER');
                $cek_new  = $this->M_print->cek_new($tipe_request, $val['ID_REQUEST']);
                
                if($val['TIPE_REQUEST']==0){ 
                    $id_specqua            = (count($cek_new['ID_SPEC_HEADER']) > 0 ? $cek_new['ID_SPEC_HEADER'] : '');    
                    $nm_specqua            = (count($cek_new['NM_SPEC']) > 0 ? $cek_new['NM_SPEC'] : '');    
                }else{
                    $id_specqua            = (count($cek_new['ID_QUALITY_HEADER']) > 0 ? $cek_new['ID_QUALITY_HEADER'] : '');    
                    $nm_specqua            = (count($cek_new['NM_QUALITY']) > 0 ? $cek_new['NM_QUALITY'] : '');  
                }
                
                $data['ID_REQUEST']         = $val['ID_REQUEST']; 
                $data['KD_REQUEST']         = $val['KD_REQUEST']; 
                $data['NM_REQUEST']         = $val['NM_REQUEST']; 
                $data['NM_STANDART']        = $val['NM_STANDART']; 
                $data['KD_PRODUCT_TYPE']    = $val['KD_PRODUCT_TYPE']; 
                $data['NM_PACK']            = $val['NM_PACK'];  
                $data['NM_PRODUCT']         = $val['NM_PRODUCT']; 
                $data['TIPE_REQUEST']       = $val['TIPE_REQUEST']; 
                $data['STATUS']             = $val['STATUS']; 
                $data['ID_STANDART']        = $val['ID_STANDART']; 
                $data['ID_PRODUCT_TYPE']    = $val['ID_PRODUCT_TYPE']; 
                $data['ID_PACK']            = $val['ID_PACK']; 
                $data['ID_PRODUCT']         = $val['ID_PRODUCT'];  
                $data['NOTE']               = $val['NOTE']; 
                $data['NM_COUNTRY']         = $val['NM_COUNTRY']; 
                $data['BUYER']              = $val['BUYER']; 
                $data['TOTAL']              = $val['TOTAL']; 
                $data['STARTDATE']          = $val['STARTDATE']; 
                $data['ENDDATE']            = $val['ENDDATE'];    
                $data['ID_SPEC']            = $id_specqua;    
                $data['NM_SPEC']            = $nm_specqua;       
                $data['NEW']                = count($cek_new);   
                $getdata[] = $data; 
            } 
        echo json_encode(array( 'data' => $getdata, 'role' => $role));   

    }
      public function detail_request()
    {  
            $id_request = $_POST['id_request'];
            $plant = $this->M_print->getPlantID($id_request);
            $file = $this->M_print->getFileID($id_request);
            
            $v_plant = '';
            foreach($plant as $p){
                $v_plant .= $p['NM_PLANT'].', ';
            }
            $v_plant = rtrim($v_plant, ", ");
            
            $v_file = '<div>'; 
            foreach($file as $v){
                $ext = strtolower(pathinfo($v['NM_FILE'], PATHINFO_EXTENSION));
                if($ext == 'xlsx' || $ext == 'xls' || $ext == 'xls' || $ext == 'csv'){
                    $icon = ' fa-file-excel-o';
                }else if($ext == 'doc' || $ext == 'docx' || $ext == 'xls'){ 
                    $icon = ' fa-file-word-o';
                }else if($ext == 'pdf'){
                    $icon = ' fa-file-pdf-o'; 
                }else if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    $icon = ' fa-file-image-o'; 
                }else{
                     $icon = ' fa-info';  
                }
                $base =  base_url()."assets/upload/{$v['NM_FILE']}";
                $v_file .= "<a href='{$base}'><span class='btn-labelx'><i class='fa {$icon} fa-lg'></i>  {$v['NM_FILE']} </span></a> <br /> <br />";
            }
            $v_file .= '</div>';
             
                echo json_encode(array('notif' => '1', 'plant' => $v_plant, 'file' => $v_file)); 
    

    }
	
    
}
