
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portal_pages extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib'); 
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Portal Pages";
		$this->load->view('portal_pages', $data);
	}
}