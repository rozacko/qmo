
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kupp extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib'); 
		$this->load->library('FPDF'); 
		$this->load->model('M_kupp');
			$this->load->model('approval/M_penyelia');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "KUPP";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/beban', $data);
	}


	 public function kupp_pdf($id_transaksi)
    { 
    
       $val = $this->M_kupp->get_data($id_transaksi, 'a1');
       
		$pdf = new FPDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->Image('assets/image/logo_sgja.jpg',10,10,-150); 
		//$pdf->addJpegFromFile('images/logo_kan.jpg',45,80,125,60) ;
		// $pdf->SetLineWidth(1);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10);
		$pdf->Cell(150,'5',"PT SEMEN INDONESIA (PERSERO) Tbk. ", 0, 0);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,'5',"F/PPU - 13/03", 0, 1);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10);
		$pdf->Cell(75,'5',"DEPARTEMEN LITBANG PRODUK & APLIKASI ", 0, 1);


		// $pdf->Rect(40,10,520,770);
		$pdf->Ln(20);
		$pdf->SetFont('Arial','',16);
		$pdf->Cell(190,5,"KAJI ULANG PERMINTAAN PENGUJIAN ".date('Y'), 0, 1,'C'); 

		$pdf->Ln(20);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',13);
		$pdf->Cell(110,'8',"No. SPUC", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$val['NO_BAPPUC'], 0, 1);
		$pdf->Cell(10);$pdf->Cell(110,'8',"Tanggal Penerimaan Contoh", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$val['TANGGAL'], 0, 1);
		$pdf->Ln(10);
		$pdf->Cell(10);$pdf->Cell(110,'8',"KAJI ULANG MELIPUTI :", 0, 1); 
		$pdf->Cell(10);$pdf->Cell(110,'8',"1. Peralatan & Bahan Pengujian dapat digunakan", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',($val['PERALATAN']=='Y' ? 'V' : ' '), 0, 1);
		$pdf->Cell(10);$pdf->Cell(110,'8',"2. Personil dapat Melakukan Pengujian", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',($val['PERSONIL']=='Y' ? 'V' : ' '), 0, 1);
		$pdf->Cell(10);$pdf->Cell(110,'8',"3. Metode Pengujian", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',($val['METODE']=='Y' ? 'V' : ' '), 0, 1);
		$pdf->Cell(10);$pdf->Cell(110,'8',"4. Waktu Penyelesaian Pengujian Sesuai Jadwal", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',($val['WAKTU']=='Y' ? 'V' : ' '), 0, 1);
        
		$pdf->Ln(10);
        $pdf->Cell(10);$pdf->Cell(170,'8',"Berdasarkan kaji ulang permintaan pengujian diatas, contoh uji ini :", 0, 1, 'C');
        $pdf->Cell(10);$pdf->Cell(170,'8',"Dapat dikerjakan", 0, 1, 'C');
        
		$pdf->Cell(10);$pdf->Cell(100,'8',"Dengan Syarat :", 0, 1); 
		$pdf->Cell(10);$pdf->Cell(170,'15',($val['KETERANGAN']!='' ? $val['KETERANGAN'] : "................................................................................."), 0, 1, 'C');
		

		$pdf->Ln(50);

		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10);$pdf->Cell(120,'8',date("d F Y"), 0, 0); 
		$pdf->Ln(30);


		$pdf->Cell(10);$pdf->Cell(120,'5',"(".$val['FULLNAME'].")", 0, 1); 

		// $pdf->Rect(350,548,40,15);
		$pdf->Output();
    }
    
    public function detail_spuc(){
        $id_transaksi = $_POST['id_transaksi'];
        $hidden = (ISSET($_POST['hidden']) ? $_POST['hidden'] : '');
        $val = $this->M_kupp->cek_transaksi($id_transaksi);
        $kategori = $this->M_kupp->cek_kategori($id_transaksi);
        $param = $this->M_kupp->cek_parameter($id_transaksi);
        $sam = $val['SAMPEL'];
        $str = explode(",",$sam);
        $sampel ='';
        $no=1;
        foreach ($str as $key) {
            $sampel .= "<tr> <td>Kode Sampel ".$no."</td> <td>:</td>  <td>  {$key}</td>  </tr>";
            $no++;
        }
        if(count($kategori)>1){
            $width = '6';
        }else{
             $width = '12';
        }
            
            $parameter = '';
            foreach($kategori as $v){
                $parameter .= "<div class='col-lg-{$width}'>  
                                 <table>
                                    <tr> <td>Pengujian {$v['NM_KATEGORI']}</td> </tr> 
                                </table><hr><table>";
                foreach($param as $p){
                    if($v['ID_KATEGORI']==$p['ID_KATEGORI']){ 
                        $parameter .= "<tr> <td>{$p['NAMA_UJI']} <i class='fa fa-check-circle'></i></td> </tr>";
                    }
                    
                }
                 $parameter .= "</table>
                            </div>";
            } 
            
              $metode = $this->M_kupp->metode($id_transaksi);
              $metod = '';
              if(count($metode)>0){
                  foreach($metode as $m){
                      $metod .= "<tr> <td>Metode Uji {$m['NM_JENIS']}</td> <td>:</td>  <td>  {$m['NAMA_STANDART']}</td>  </tr>"; 
                  } 
              }else{
                   $metod .= "<tr> <td>Metode Uji </td> <td>:</td>  <td>  --  </td>  </tr>"; 
              }
                $peminta = '';
            if($hidden != '1'){ 
                if($val['KODE_UJI']=='ISO'){
                    $peminta = "  <tr> <td>Nama Peminta</td> <td>:</td>  <td>  {$val['NAMA_INSTANSI']}</td>  </tr> 
                                <tr> <td>Alamat</td> <td>:</td>  <td>  {$val['ALAMAT_INS']}</td>  </tr>
                                <tr> <td>Keterangan</td> <td>:</td>  <td>  {$val['KETERANGAN']}</td>  </tr>";
                }else{ 
                    $v = $this->M_kupp->peminta_internal($val['PEMINTA']);
                     $peminta = "  <tr> <td>Nama Peminta</td> <td>:</td>  <td>  {$v['mk_nama']}</td>  </tr> 
                                    <tr> <td>No. Pegawai</td> <td>:</td>  <td>  {$v['mk_nopeg']}</td>  </tr>
                                    <tr> <td>Unit Kerja</td> <td>:</td>  <td>  {$v['muk_nama']}</td>  </tr>";
                }
            }
            
            
     
        $detail =  "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
                                </div>
                                <div class='modal-body'>  
                                    <div class='row'>
                                        <div class='col-lg-6'>  
                                            <table style='font-size: 15px;'>
                                                <tr> <td width='40%'>Nama Contoh</td> <td width='5%'>:</td>  <td width='55%' nowrap> {$val['NAMA_CONTOH']}</td>  </tr>
                                                <tr> <td>No. BAPPUC</td> <td>:</td>  <td>  {$val['NO_BAPPUC']}</td>  </tr>
                                                <tr> <td>Tanggal Terima</td> <td>:</td>  <td>   {$val['TANGGAL']}</td>  </tr>
                                                <tr> <td>Jumlah Contoh</td> <td>:</td>  <td>   {$val['JUMLAH']}</td>  </tr>
                                                <tr> <td>Kemasan Contoh Uji</td> <td>:</td>  <td>   {$val['KEMASAN']}</td>  </tr>
                                                <tr> <td>Warna Contoh Uji</td> <td>:</td>  <td>   {$val['WARNA']}</td>  </tr>
                                                <tr> <td>Keterangan Warna</td> <td>:</td>  <td>   {$val['KET_WARNA']}</td>  </tr>
                                                <tr> <td>Bentuk Contoh Uji</td> <td>:</td>  <td>  {$val['BENTUK_']}</td>  </tr>
                                                {$sampel}
                                            </table>
                                        </div>
                                        <div class='col-lg-6'> 
                                            <table style='font-size: 15px;'>
                                                <tr> <td width='40%'>Berat Contoh Uji</td> <td width='5%'>:</td>  <td width='55%' nowrap> {$val['NAMA_BERAT']}</td>  </tr>
                                                <tr> <td>Kondisi Contoh Uji</td> <td>:</td>  <td>  {$val['KONDISI_CONTOH']}</td>  </tr>
                                                    {$metod}
                                                <tr> <td>Tanggal Mulai</td> <td>:</td>  <td>  {$val['TANGGAL_MULAI']}</td>  </tr>
                                                <tr> <td>Tanggal Selesai</td> <td>:</td>  <td>  {$val['TANGGAL_SELESAI']}</td>  </tr>
                                                <tr> <td>No. SPK/SPP</td> <td>:</td>  <td>  {$val['NO_SPK']}</td>  </tr>
                                                    {$peminta}
                                            </table>
                                        </div>
                                    </div> <hr>     
                                    <div class='row'>
                                        {$parameter}
                                    </div>   
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";
        
        echo $detail;
    }
    
}
