
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pending extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_pending');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "SPUC Pending Penyelia";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('pending', $data);
	}
     
	 public function get_data()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
         $session_kate = ''; 
             $getdata = array();  
        foreach($sess_lab as $v){   
                 $on =   "'t1'";
                 $off =  'a2';  
             $detail_data = $this->M_pending->get_data($v['ID_LAB'], $on, $v['ID_KATEGORI']);
           
             $spuc_mt = ''; $spuc_penyelia = ''; $analis = ''; $hasil_penyelia = ''; $hasil_mt = ''; 
             // echo json_encode($detail_data);
            foreach($detail_data as $val){ 
                    $cek_a2 = $this->M_pending->cek_a2($val['ID_TRANSAKSI'],  $v['ID_KATEGORI']);
                   
                   if(count($cek_a2) < 1 ){ 
                        $ket_tolak = $this->M_pending->cek_dislike($val['ID_TRANSAKSI'], "'t1'", $v['ID_KATEGORI'])['KETERANGAN_TOLAK'];
                                // print_r($ket_tolak);
                       $spuc_mt = ($this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['STATUS']=='a1' ? '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-up"></i></center>' : ''); 
                        $spuc_penyelia = '<center>';$analis = '<center>';$hasil_penyelia = '<center>';$hasil_mt = ' ';
                          $cek_spuc_penyelia = $this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a2', 't1'", '');
                        foreach ($cek_spuc_penyelia as $t) {
                            $jempol = ($t['STATUS'] == 'a2' ? " fa-thumbs-up" : " fa-thumbs-down");
                            if ($t['STATUS'] == 't1') {
                                $cek = $this->M_pending->cek_dislike($val['ID_TRANSAKSI'], "'a2'", $t['ID_KATEGORI']);
                                $jempol = (count($cek > 0) && $t['ID_KATEGORI'] == $cek['ID_KATEGORI'] ? '' : $jempol);
                            }
                            $cek_kategori = $this->M_pending->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $spuc_penyelia .= "<i style='color:{$cek_kategori['WARNA']}' title='Penyelia {{$cek_kategori['NM_KATEGORI']}}' class='fa fa-lg {$jempol}'></i>&nbsp;";
                        }
                        $cek_analis = $this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a3'", '');  
                        foreach($cek_analis as $t){
                            $cek_kategori = $this->M_pending->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $analis .="<i style='color:{$cek_kategori['WARNA']}' title='Analis {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg fa-thumbs-up'></i>&nbsp;";
                        } $analis .= "</center>";
                       $cek_hasil_penyelia = $this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a4', 't2'", '');
                        foreach ($cek_hasil_penyelia as $t) {
                            $jempol = ($t['STATUS'] == 'a4' ? " fa-thumbs-up" : " fa-thumbs-down");
                            if ($t['STATUS'] == 't2') {
                                $cek = $this->M_pending->cek_dislike($val['ID_TRANSAKSI'], "'a4'", $t['ID_KATEGORI']);
                                $jempol = (count($cek > 0) && $t['ID_KATEGORI'] == $cek['ID_KATEGORI'] ? '' : $jempol);
                            }
                            $cek_kategori = $this->M_pending->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $hasil_penyelia .= "<i style='color:{$cek_kategori['WARNA']}'  title='Penyelia {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg {$jempol}'></i>&nbsp;";
                        }
                        $hasil_penyelia .= "</center>";
                        $hasil_mt = (empty($this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a5'", '')[0]['STATUS']) ? ' ': '<center><i style="color:blue" class="fa fa-lg fa-thumbs-up"></i></center>');  
                        $cito = ($this->M_pending->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['CITO']=='Y' ? 'YA' : '');
                        $data['SPUC'] = '--';
                        $data['NO_BAPPUC'] = $val['NO_BAPPUC'];
                        $data['CITO'] = $cito;
                        $data['TANGGAL'] = $val['TANGGAL'];
                        $data['NAMA_CONTOH'] = $val['NAMA_CONTOH'];
                        $data['SPUC_MT'] =$spuc_mt;
                        $data['SPUC_PENYELIA'] = $spuc_penyelia;
                        $data['ANALIS'] = $analis;
                        $data['HASIL_PENYELIA'] =$hasil_penyelia;
                        $data['HASIL_MT'] = $hasil_mt;
                        $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                        $data['ID_KATEGORI'] = $v['ID_KATEGORI']; 
                        $data['STATUS'] = $val['STATUS'];  
                        $data['KETERANGAN'] = $val['KETERANGAN'];  
                        $data['KETERANGAN_TOLAK'] = $ket_tolak;  
                        $getdata[] = $data;  
                   }
            }
       }    
		echo json_encode($getdata);

    }
    
    
    
    public function update()
    {
        if (isset($_POST["approve"])
        ) {
            $id = intval($_POST["id"]);
            $id_user = $this->session->userdata("USER")->ID_USER;
            $id_kategori = intval($_POST["id_kategori"]);
            $id_status = $_POST["id_status"]; 
            $syarat = $_POST["syarat"]; 
            
            $status = ($_POST["approve"] == 'Y' ? 't0' : 't1');
            
           if($_POST["approve"] == 'Y'){
                    $date = date('d-m-Y h:i:s');
                    $column = array(
                        'STATUS', 'ID_USER', 'ID_TRANSAKSI', 'KETERANGAN', 'TANGGAL', 'ID_KATEGORI'
                    );

                    $data = array(
                        "'" . $status . "'", "'" . $id_user . "'", "'" . $id . "'", "'" . trim($_POST["syarat"]) . "'", "SYSDATE", "'" . $id_kategori . "'"
                    );

                    $q = $this->M_pending->simpan_approve($column, $data);
           }else{
                        $column = array(
                            'KETERANGAN_TOLAK'
                        );

                        $data = array(
                            "'" . $syarat . "'"
                        );

                        $jml_kolom = count($column);
                        $data_baru = array();

                        for ($i = 0; $i < $jml_kolom; $i++) {
                            $data_baru[] = $column[$i] . "=" . $data[$i];
                        }
                        
                        $q = $this->M_pending->update($id, 't1', $data_baru);
               
           }
            
         
            
          

			$get_spuc = $this->M_pending->get_spuc($id);  

            if ($status == 't1') {
                $htmlContent = " Mohon dilakukan Approval NO SPUC :  {$get_spuc['NO_BAPPUC']} "; 
                $cekTo = $this->M_pending->cekTo('3', $get_spuc['KODE_LAB'], $id_kategori);
            } else {
                $htmlContent = " NO. SPUC {$get_spuc['NO_BAPPUC']} Dinyatakan tidak layak. ";  
                $cekTo = $this->M_pending->cekTo('1', $get_spuc['KODE_LAB'], '');
            }
 
            $to = array();
            foreach($cekTo as $v){
                array_push($to, $v['EMAIL']);
            } 

            $config['protocol'] = "sendmail";
            $config['smtp_host'] = "mx3.semenindonesia.com";
            $config['smtp_port'] = "25";
            $config['smtp_user'] = "";
            $config['smtp_pass'] = "";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
            $config['crlf'] = "\r\n";
            $this->load->library('email', $config);
            $this->email->from('noreply@semenindonesia.com', 'Team IT ELAB'); 
            $this->email->to($to);
            $this->email->subject('Pending SPUC'); 
            $this->email->message($htmlContent);
            $this->email->send();

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

      
     
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_pending->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
