
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approvespuc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_approvespuc');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "KUPP Disetujui";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('ApproveSpuc', $data);
	}
	public function ditolak()
	{
		$data['title'] = "SPUC Tidak Layak";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('TolakSpuc', $data);
	}
     
	 public function get_data()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
         $session_kate = ''; 
        foreach($sess_lab as $v){   
                 $on =   "'a1'"; 
             $detail_data = $this->M_approvespuc->get_data($v['ID_LAB'], $on);  
             $getdata = array();  
            foreach($detail_data as $val){     
            
                        $tolak = $this->M_approvespuc->cek_tolak($val['ID_TRANSAKSI'], $on);  
                            $data['SPUC'] = '--';
                            $data['NO_BAPPUC'] = $val['NO_BAPPUC']; 
                            $data['TANGGAL'] = $val['TANGGAL'];
                            $data['NAMA_CONTOH'] = $val['NAMA_CONTOH']; 
                            $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                            $data['ID_KATEGORI'] = $v['ID_KATEGORI'];  
                            $data['PERALATAN'] = $tolak['PERALATAN'];  
                            $data['PERSONIL'] = $tolak['PERSONIL'];  
                            $data['METODE'] = $tolak['METODE'];  
                            $data['WAKTU'] = $tolak['WAKTU'];  
                            $data['KETERANGAN'] = $tolak['KETERANGAN'];  
                            $getdata[] = $data;  
            }
       }    
		echo json_encode($getdata);

    }
	 public function get_data_tolak()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
         $session_kate = ''; 
             $getdata = array();  
        foreach($sess_lab as $v){   
                 $on =   "'t0'"; 
             $detail_data = $this->M_approvespuc->get_data($v['ID_LAB'], $on); 
             // echo json_encode($detail_data);
            foreach($detail_data as $val){     
                        $tolak = $this->M_approvespuc->cek_tolak($val['ID_TRANSAKSI'], $on);  
                            $data['SPUC'] = '--';
                            $data['NO_BAPPUC'] = $val['NO_BAPPUC']; 
                            $data['TANGGAL'] = $val['TANGGAL'];
                            $data['NAMA_CONTOH'] = $val['NAMA_CONTOH']; 
                            $data['TGL_TOLAK'] = $tolak['TGL_TOLAK']; 
                            $data['FULLNAME'] = $tolak['FULLNAME']; 
                            $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                            $data['ID_KATEGORI'] = $v['ID_KATEGORI'];  
                            $data['PERALATAN'] = $tolak['PERALATAN'];  
                            $data['PERSONIL'] = $tolak['PERSONIL'];  
                            $data['METODE'] = $tolak['METODE'];  
                            $data['WAKTU'] = $tolak['WAKTU'];  
                            $data['KETERANGAN'] = $tolak['KETERANGAN'];  
                            $getdata[] = $data;  
            }
       }    
		echo json_encode($getdata);

    }
      
     
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_approvespuc->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
