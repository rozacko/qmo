
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_guide extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->helper(array('url','download'));
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "User Guide";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
       

		$this->layout->render('user_guide', $data);
	}
	public function download(){
		force_download('assets/download/UG Rebuild Aplikasi E-Lab.pdf',NULL);
	}
}
