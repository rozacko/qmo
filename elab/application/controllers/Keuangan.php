<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_keuangan');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['lab'] = $this->M_keuangan->lab();
		$data['company'] = $this->M_keuangan->company();
		$data['title'] = "Halaman Utama";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('keuangan', $data);
	}

	public function ajax_list()
    {

        // $list = $this->M_keuangan->get_datatables();
        // $data = array();
        // $no = $_POST['start'];
        // foreach ($list as $arr) {
            // $biaya = $this->M_keuangan->get_biaya($arr->ID_TRANSAKSI, $arr->ID_LAB);
            // $no++;
            // $row = array();
            // $row[] = $no;
            // $row[] = $arr->ID_TRANSAKSI.'/'.$arr->KODE_UJI.'/'.$arr->ID_PLANT.'/'.$arr->KODE_LAB.'/'.$arr->NO_SPUC_4;
            // $row[] = $arr->TANGGAL;
            // $row[] = $arr->NM_COMPANY;
            // $row[] = $arr->NM_LAB;
            // $row[] = $this->rupiah($biaya);
            // $data[] = $row;
        // }
        // $output = array(
            // "draw" => $_POST['draw'],
            // "recordsTotal" => $this->M_keuangan->count_all(),
            // "recordsFiltered" => $this->M_keuangan->count_filtered(),
            // "data" => $data,
        // );
           $detail_data = $this->M_keuangan->get_data();
        
         foreach($detail_data as $val){     
                       $biaya = $this->M_keuangan->get_biaya($val['ID_TRANSAKSI'], $val['KODE_LAB']); 
                            $data['NO_BAPPUC'] = $val['NO_BAPPUC']; 
                            $data['TANGGAL'] = $val['TANGGAL'];
                            $data['NM_COMPANY'] = $val['NM_COMPANY']; 
                            $data['KD_LAB'] = $val['KD_LAB']; 
                            $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI']; 
                            $data['BIAYA'] = $this->rupiah($biaya);  
                            $getdata[] = $data;  
            }
        echo json_encode($getdata);
    }
    
	public function lab(){
		$id= $this->input->post('id');
		$sql = $this->M_keuangan->ambilLab($id);
		
		echo "<option value='' data-hidden = 'true'> --  Pilih Lab -- </option>";
        foreach($sql as $p){
		echo "<option value='{$p["ID_LAB"]}'>".htmlspecialchars($p["NM_LAB"])."</option>";
		}
			
	}
	
    private function rupiah($angka){
        
        $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
        return $hasil_rupiah;

    }

}
