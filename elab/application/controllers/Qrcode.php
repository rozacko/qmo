<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QRCodes extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_pending');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		

		$this->load->library('ciqrcode');

$params['data'] = 'This is a text to encode become QR Code';
$params['level'] = 'H';
$params['size'] = 10;
$params['savename'] = FCPATH.'tes.png';
$this->ciqrcode->generate($params);

echo '<img src="'.base_url().'tes.png" />';
	}
}