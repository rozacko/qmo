
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_dashboard');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Halaman Utama";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->set_graph_flot();
        $this->htmllib->set_chart_js();
        $this->htmllib->set_peity_js();

		$this->layout->render('dashboard/welcome', $data);
	}
    
    
	 public function get_data()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
         $session_kate = ''; 
             $getdata = array();  
        foreach($sess_lab as $v){    
             $detail_data = $this->M_dashboard->get_data($v['ID_LAB'] );
           
             $spuc_mt = ''; $spuc_penyelia = ''; $analis = ''; $hasil_penyelia = ''; $hasil_mt = ''; 
             // echo json_encode($detail_data);
            foreach($detail_data as $val){  
                            $spuc_mt = '';
                           $cek_mt = $this->M_dashboard->cek_approve($val['ID_TRANSAKSI'], "'a1', 't0'", '');
                           if(count($cek_mt)>0){
                               foreach($cek_mt as $cm){
                                   $spuc_mt = ($cm['STATUS'] == 'a1' ? '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-up"></i></center>' : '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-down"></i></center>');
                               }   
                           } 
                            $spuc_penyelia = '<center>';$analis = '<center>';$hasil_penyelia = '<center>';$hasil_mt = ' ';
                            
                              $get_spuc_penyelia = $this->M_dashboard->get_spuc_penyelia($val['ID_TRANSAKSI'], "'a2', 't1'", '');  
                                foreach($get_spuc_penyelia as $sp){ 
                                    $cek_spuc_penyelia = $this->M_dashboard->cek_dislike($val['ID_TRANSAKSI'], "'t1'", $sp['ID_KATEGORI']);   
                                    $jempol = ' ';
                                    if(count($cek_spuc_penyelia)>0){ 
                                          $jempol =  " fa-thumbs-down"  ;
                                         if($cek_spuc_penyelia['STATUS']=='t1'){
                                              $cek_spuc_penyelia = $this->M_dashboard->cek_dislike($val['ID_TRANSAKSI'], "'a2'",  $sp['ID_KATEGORI']);
                                               $jempol = (count($cek_spuc_penyelia>0)  ? ' fa-thumbs-up' : $jempol);     
                                         }
                                    }else{ 
                                        $jempol =  " fa-thumbs-up"  ;
                                        $cek_spuc_penyelia = $this->M_dashboard->cek_dislike($val['ID_TRANSAKSI'], "'a2'",  $sp['ID_KATEGORI']);   
                                        $jempol = ($cek_spuc_penyelia!=''  ? $jempol : ''); 
                                    }
                                    $cek_kategori = $this->M_dashboard->cek_kategori($val['ID_TRANSAKSI'],   $sp['ID_KATEGORI']); 
                                    $spuc_penyelia .="<i style='color:{$cek_kategori['WARNA']}' title='Penyelia {{$cek_kategori['NM_KATEGORI']}}' class='fa fa-lg {$jempol}'></i>&nbsp;";
                                } 
                                $spuc_penyelia .= "</center>";
                                
                            $cek_analis = $this->M_dashboard->cek_approve($val['ID_TRANSAKSI'], "'a3'", '');  
                            foreach($cek_analis as $t){
                                $cek_kategori = $this->M_dashboard->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                                $analis .="<i style='color:{$cek_kategori['WARNA']}' title='Analis {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg fa-thumbs-up'></i>&nbsp;";
                            } $analis .= "</center>";
                            $cek_hasil_penyelia = $this->M_dashboard->cek_approve($val['ID_TRANSAKSI'], "'a4', 't2'", '');
                            foreach ($cek_hasil_penyelia as $t) {
                                $jempol = ($t['STATUS'] == 'a4' ? " fa-thumbs-up" : " fa-thumbs-down");
                                if ($t['STATUS'] == 't2') {
                                    $cek = $this->M_dashboard->cek_dislike($val['ID_TRANSAKSI'], "'a4'", $t['ID_KATEGORI']);
                                    $jempol = (count($cek > 0) && $t['ID_KATEGORI'] == $cek['ID_KATEGORI'] ? '' : $jempol);
                                }
                                $cek_kategori = $this->M_dashboard->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                                $hasil_penyelia .= "<i style='color:{$cek_kategori['WARNA']}'  title='Penyelia {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg {$jempol}'></i>&nbsp;";
                            }
                            
                           $cek_hasil_mt = $this->M_dashboard->cek_approve($val['ID_TRANSAKSI'], "'a5'", '');
                           $hasil_mt = (count($cek_hasil_mt)>0? '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-up"></i></center>' : ''); 
                           
                           $cek_cito = $this->M_dashboard->cek_tombol($val['ID_TRANSAKSI'], "a1", '');
                           $cito = (isset($cek_cito) && $cek_cito['CITO']=='Y' ? 'YA' : ''); 
                           
                            // $cito = ($this->M_dashboard->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['CITO']=='Y' ? 'YA' : '');
                            $data['SPUC'] = '--';
                            $data['NO_BAPPUC'] = $val['NO_BAPPUC'];
                            $data['CITO'] = $cito;
                            $data['TANGGAL'] = $val['TANGGAL'];
                            $data['NAMA_CONTOH'] = $val['NAMA_CONTOH'];
                            $data['SPUC_MT'] =$spuc_mt;
                            $data['SPUC_PENYELIA'] = $spuc_penyelia;
                            $data['ANALIS'] = $analis;
                            $data['HASIL_PENYELIA'] =$hasil_penyelia;
                            $data['HASIL_MT'] = $hasil_mt;
                            $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI']; 
                            $data['KETERANGAN'] = $val['KETERANGAN'];  
                            $getdata[] = $data;  
            }
       }    
		echo json_encode($getdata);

    }
}
