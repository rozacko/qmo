
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parameter extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_parameter');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Parameter Uji";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js(); 

		$this->layout->render('coq/parameter', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_parameter->get_data();
		echo json_encode($detail_data);

    }
	public function refreshProduk() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>";
		$data = $this->M_parameter->getProduk();
		foreach($data as $p) {
			echo "<option value='{$p["ID_PRODUCT"]}'>".htmlspecialchars($p["NM_PRODUCT"])."</option>";
		}
	}
    
	public function refreshComponent() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Component --</option>";
		$data = $this->M_parameter->getComponent();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPONENT"]}'>".htmlspecialchars($p["NM_COMPONENT"])."</option>";
		}
	}
    
	public function refreshExisting() {
        $id_produk = $_POST["id"];
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Parent --</option>";
		$data = $this->M_parameter->getExisting($id_produk);
		foreach($data as $p) {
			echo "<option value='{$p["ID_UJI"]}' >".htmlspecialchars($p["NAMA_UJI"])."</option>";
		}
	}
	public function refreshKategori() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kategori --</option>";
		$data = $this->M_parameter->getKategori();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KATEGORI"]}'>".htmlspecialchars($p["NM_KATEGORI"])."</option>";
		}
	}
	public function refreshCompany() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>";
		$data = $this->M_parameter->getCompany();
		foreach($data as $p) {
			echo "<option value='{$p["KD_COMPANY"]}'>".htmlspecialchars($p["NM_COMPANY"])."</option>";
		}
	} 
	public function simpan() {
        
		if(isset($_POST["parameter"]) ){
			// $column = array( 
				 // 'NM_PARAMETER','SYMBOL','UNIT_SI' ,'UNIT_EN','UNIT_SNI','TEST_ASTM','TEST_EN' ,'TEST_SNI','MARK_ASTM','MARK_EN','MARK_SNI' ,'PARENT','ID_PRODUCT' 
			// );
			$column = array( 
				 'NAMA_UJI','SIMBOL','PARENT' ,'ID_PRODUCT','SATUAN','MARK','ID_KATEGORI'
			);
			if($_POST["tipe"]=='0'){
                $parent_menu = '';
            }else{
                 $parent_menu = $_POST["existing"];
            }
			$data = array(
				 "'".$_POST["parameter"]."'", "'".$_POST["simbol"]."'", "'".$parent_menu."'", "'".$_POST["produk"]."'", "'".$_POST["satuan"]."'", "'".$_POST["mark"]."'", "'".$_POST["kategori"]."'" 
			);
            // $data = array(
				 // "'".$_POST["parameter"]."'", "'".$_POST["simbol"]."'", "'".$_POST["unit_si"]."'", "'".$_POST["unit_en"]."'", "'".$_POST["unit_sni"]."'", "'".$_POST["test_astm"]."'", "'".$_POST["test_en"]."'", "'".$_POST["test_sni"]."'", "'".$_POST["mark_astm"]."'", "'".$_POST["mark_en"]."'", "'".$_POST["mark_sni"]."'", "'".$parent_menu."'", "'".$_POST["produk"]."'",  
			// );
			$q = $this->M_parameter->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["parameter"]) ){
			$id = intval($_POST["id"]);
         	$column = array( 
				 'NAMA_UJI','SIMBOL','PARENT' ,'ID_PRODUCT','SATUAN','MARK','ID_KATEGORI'
			);
			if($_POST["tipe"]=='0'){
                $parent_menu = '';
            }else{
                 $parent_menu = $_POST["existing"];
            }
			$data = array(
				 "'".$_POST["parameter"]."'", "'".$_POST["simbol"]."'", "'".$parent_menu."'", "'".$_POST["produk"]."'", "'".$_POST["satuan"]."'", "'".$_POST["mark"]."'" , "'".$_POST["kategori"]."'"
			);
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_parameter->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_parameter->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_parameter->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Kategori');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Satuan'); 
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Symbol'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Parameter Uji'); 
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) { 
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NAMA_UJI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NM_KATEGORI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["SATUAN"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["SIMBOL"]);  
                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Parameter_Uji.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
