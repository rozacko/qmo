
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Metode extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_metode');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Metode";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/metode', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_metode->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshStandart() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
		$data = $this->M_metode->getStandart();
		foreach($data as $p) {
			echo "<option value='{$p["ID_STANDART"]}'>".htmlspecialchars($p["NM_STANDART"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["nm_metode"]) ){
			$column = array(
				'NM_METODE' , 'ID_STANDART'
			);
			
			$data = array(
				"'".$_POST["nm_metode"]."'", "'".$_POST["id_standart"]."'"  
			);
			 
			$q = $this->M_metode->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_metode"]) 
		) {
			$id = intval($_POST["id"]);
			$column = array(
				'NM_METODE' , 'ID_STANDART'
			);
			
			$data = array(
				"'".$_POST["nm_metode"]."'", "'".$_POST["id_standart"]."'"  
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_metode->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_metode->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
