
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standart_uji extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_standart_uji');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Standart Uji";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/standart_uji', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_standart_uji->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshParameter() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Parameter --</option>";
		$data = $this->M_standart_uji->getParameter();
		foreach($data as $p) {
			echo "<option value='{$p["ID_UJI"]}'>".htmlspecialchars($p["NAMA_UJI"])." (".$p["NM_PRODUCT"].")</option>";
		}
	} 
    
	public function refreshMetode() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Metode --</option>";
		$data = $this->M_standart_uji->getMetode();
		foreach($data as $p) {
			echo "<option value='{$p["ID_METODE"]}'>".htmlspecialchars($p["NM_METODE"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["id_parameter"]) ){
			$column = array(
				'ID_PARAMETER' , 'ID_METODE'
			);
			
			$data = array(
				"'".$_POST["id_parameter"]."'", "'".$_POST["id_metode"]."'"  
			);
			 
			 
			$q = $this->M_standart_uji->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["id_parameter"]) 
		) {
			$id = intval($_POST["id"]);
			$column = array(
				'ID_PARAMETER' , 'ID_METODE'
			);
			
			$data = array(
				"'".$_POST["id_parameter"]."'", "'".$_POST["id_metode"]."'"  
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_standart_uji->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_standart_uji->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
