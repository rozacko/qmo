
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_email extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		//$this->load->model('coq/M_approval');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Approval Email";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/approval_email', $data);
	}
     
	
    
    
}
