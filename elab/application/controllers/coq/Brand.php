
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_brand');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['product'] = $this->M_brand->product();
		/*$data['standart'] = $this->M_std_product->standart();*/
		$data['title'] = "Master Brand";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/brand', $data);
	}
     
	 
    public function get_data()
    {
        $list = $this->M_brand->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $arr) {
            $no++;
            $row = array();
            $row[] = $no; 
            
            $row[] = $arr['KD_PRODUCT_TYPE'] .' - '. $arr['NM_PRODUCT_TYPE'];
            $row[] = $arr['NM_BRAND'];
            $row[] = $arr['KODE_BRAND'];

            $row[] = '<center>
            			<button class="btn btn-primary btn-xs waves-effect" onclick="edit(\''.$arr['ID_BRAND'].'\')" title="Edit"><span class="fa fa-pencil"></span> </button>
                        <button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\''.$arr['ID_BRAND'].'\')" title="Delete"><span class="fa fa-trash-o"></span> </button>
                      </center>';
            $data[] = $row;
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_brand->count_all(),
			"recordsFiltered" => $this->M_brand->count_filtered(),
			"data" => $data,
        );
        echo json_encode($output);
    }

    public function tambah()
	{
		$type_product = $this->input->post('type_product');
		
		$brand = $this->input->post('brand');
		$kode = $this->input->post('kode');

	
		

		

		
			$data = array('ID_PRODUCT_TYPE' =>$type_product,						  
						  'NM_BRAND'=>$brand,'KODE_BRAND'=>$kode);
			$this->M_brand->add($data);
			echo json_encode(array('status'=>TRUE));
		
	}
	public function hapus(){
		$id = $this->input->post('id');
		$this->M_brand->hapus($id);

		echo json_encode(array('status'=>true));
	}
	public function edit($id){
		$sql = $this->M_brand->get_id($id);
		echo json_encode($sql);
	}
	public function update()
	{
		$id = $this->input->post('id_brand');
		$type_product = $this->input->post('type_product');
		
		$brand = $this->input->post('brand');
		$kode = $this->input->post('kode');

		

		

		
			$data = array('ID_PRODUCT_TYPE' =>$type_product,						  
						  'NM_BRAND'=>$brand,'KODE_BRAND'=>$kode);
			$where = array('ID_BRAND'=>$id);
			$sql = $this->M_brand->updateData($where, $data, 'COQ_BRAND');

			$status = false;
			if($sql > 0){
				$status = TRUE;
			}
			echo json_encode(array('status'=>$status));
	}
    
}
