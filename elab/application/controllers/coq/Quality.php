<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->helper(array('url', 'file'));
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('coq/M_quality');
        $this->load->model('coq/M_approval');
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Create Certificated";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->dropzone_plugin();

        $this->layout->render('coq/quality', $data);
    }

    public function ambil_nilai()
    {


        $id = $this->input->post('id');
        $data = $this->M_quality->cek_template($id);
        echo "<option value=''>--Pilih Parameter --</option>";
        foreach ($data as $p) {
            echo "<option value='{$p["NILAI_RESULT"]}'>" . htmlspecialchars($p["NAMA_UJI"]) . " = " . $p["NILAI_RESULT"] . "</option>";

        }

    }

    public function ambil_quality()
    {


        $id = $this->input->post('id');
        $data = $this->M_quality->cek_spec($id);
        echo "<option value=''>--Pilih Parameter --</option>";
        foreach ($data as $p) {
            echo "<option value='{$p["NILAI_SPEC"]}'>" . htmlspecialchars($p["NAMA_UJI"]) . " = " . $p["NILAI_SPEC"] . "</option>";

        }

    }

    public function get_data()
    {
        $sess_user = $this->session->userdata("USERLAB");
        $session_plant = '';
        $session_role = '';
        foreach ($sess_user as $v) {
            $session_plant .= $v['ID_PLANT'] . ",";
            $session_role .= $v['ID_ROLE'] . ",";
        }
        $plant = rtrim($session_plant, ",");
        $role = rtrim($session_role, ",");
        $detail_data = $this->M_quality->get_data($plant);

        $getdata = array();
        foreach ($detail_data as $val) {
            $cek_status = $this->M_quality->cek_status($val['ID_QUALITY_HEADER'], $val['ID_PLANT'])->result();
            $status = $cek_status[0]->STATUS;
            $data['KD_QUALITY'] = $val['KD_QUALITY'];
            $data['NM_QUALITY'] = $val['NM_QUALITY'];
            $data['NM_STANDART'] = $val['NM_STANDART'];
            $data['KD_PRODUCT_TYPE'] = $val['KD_PRODUCT_TYPE'];
            $data['NM_BRAND'] = $val['NM_BRAND'];
            $data['NM_PLANT'] = $val['NM_PLANT'];

            $data['NM_PRODUCT'] = $val['NM_PRODUCT'];
            $data['NM_REQUEST'] = $val['NM_REQUEST'];
            $data['ID_QUALITY_HEADER'] = $val['ID_QUALITY_HEADER'];
            $data['ID_STANDART'] = $val['ID_STANDART'];
            $data['ID_PRODUCT_TYPE'] = $val['ID_PRODUCT_TYPE'];
            $data['ID_PRODUCT'] = $val['ID_PRODUCT'];
            $data['ID_REQUEST'] = $val['ID_REQUEST'];
            $data['ID_PLANT'] = $val['ID_PLANT'];
            $data['START_PRODUCE'] = $val['START_PRODUCE'];
            $data['END_PRODUCE'] = $val['END_PRODUCE'];
            $data['FORMULAS'] = $val['FORMULAS'];
            $data['STATUS'] = $status;
            $data['ROLE'] = $role;

            $getdata[] = $data;
        }
        echo json_encode($getdata);

    }

    public function get_listParameter()
    {
        $id_standart = $_POST['id_standart'];
        $id_produk = $_POST['id_produk'];
        $detail_data = $this->M_quality->get_listParameter($id_standart, $id_produk);
        echo json_encode($detail_data);

    }

    public function get_listTemplate()
    {
        $detail_data = $this->M_quality->get_listTemplate();
        echo json_encode($detail_data);

    }

    public function get_spec()
    {
        $id_quality = $this->input->post("id_quality");
        $detail_data = $this->M_quality->cek_spec($id_quality);
        echo json_encode($detail_data);
    }

    public function get_template()
    {
        $id_template = $this->input->post("id_template");
        $detail_data = $this->M_quality->cek_template($id_template);
        echo json_encode($detail_data);
    }


    public function detail_request()
    {
        $id_request = $_POST['id_request'];
        $plant = $this->M_quality->getPlantID($id_request);
        $file = $this->M_quality->getFileID($id_request);

        $v_plant = '';
        foreach ($plant as $p) {
            $v_plant .= $p['NM_PLANT'] . ', ';
        }
        $v_plant = rtrim($v_plant, ", ");

        $v_file = '<div>';
        foreach ($file as $v) {
            $ext = strtolower(pathinfo($v['NM_FILE'], PATHINFO_EXTENSION));
            if ($ext == 'xlsx' || $ext == 'xls' || $ext == 'xls' || $ext == 'csv') {
                $icon = ' fa-file-excel-o';
            } else if ($ext == 'doc' || $ext == 'docx' || $ext == 'xls') {
                $icon = ' fa-file-word-o';
            } else if ($ext == 'pdf') {
                $icon = ' fa-file-pdf-o';
            } else if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                $icon = ' fa-file-image-o';
            } else {
                $icon = ' fa-info';
            }
            $base = base_url() . "assets/upload/{$v['NM_FILE']}";
            $v_file .= "<a href='{$base}'><span class='btn-labelx'><i class='fa {$icon} fa-lg'></i>  {$v['NM_FILE']} </span></a> <br /> <br />";
        }
        $v_file .= '</div>';

        echo json_encode(array('notif' => '1', 'plant' => $v_plant, 'file' => $v_file));


    }


    public function refreshChat()
    {

        $data = $this->M_quality->getChatID($_POST['id_request']);
        $id_user = $this->session->userdata("USER")->ID_USER;
        $chat = '';
        $gambar = base_url() . 'assets/img/landing/avatar1.jpg';
        foreach ($data as $v) {
            $posisi = ($id_user == $v['ID_USER'] ? 'right' : 'left');
            $tanggal = date('D, d M Y', strtotime($v['TANGGAL']));
            $chat .= "<div class='chat-message {$posisi}'>
                        <img class='message-avatar' src='{$gambar}' alt='' >
                        <div class='message'>
                            <a class='message-author' href='#'> {$v['FULLNAME']} </a>
                            <span class='message-date'> {$tanggal} </span>
                            <span class='message-content'>
                                                        {$v['CHAT']}
                            </span>
                        </div>
                    </div>";
        }

        echo json_encode(array('notif' => '1', 'chat' => $chat));
    }


    public function refreshRequest()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>";
        $data = $this->M_quality->getRequest();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_REQUEST"]}'>" . htmlspecialchars($p["NM_REQUEST"]) . "</option>";
        }
    }

    public function refreshStandart()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
        $data = $this->M_quality->getStandart();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_STANDART"]}'>" . htmlspecialchars($p["NM_STANDART"]) . "</option>";
        }
    }

    public function refreshTipe()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>";
        $data = $this->M_quality->getTipe();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PRODUCT_TYPE"]}'>" . htmlspecialchars($p["NM_PRODUCT_TYPE"]) . " (<b>" . htmlspecialchars($p["KD_PRODUCT_TYPE"]) . "</b>) </option>";
        }
    }

    public function refreshProduk()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>";
        $data = $this->M_quality->getProduk();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PRODUCT"]}'>" . htmlspecialchars($p["NM_PRODUCT"]) . "</option>";
        }
    }

    public function refreshPlant()
    {
        echo "<option value='' data-hidden='true' >-- Pilih Plant --</option>";
        $data = $this->M_quality->getPlant();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PLANT"]}'>" . htmlspecialchars($p["NM_PLANT"]) . "</option>";
        }
    }

    public function refreshCountry()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Negara --</option>";
        $data = $this->M_quality->getCountry();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_COUNTRY"]}'>" . htmlspecialchars($p["NM_COUNTRY"]) . "</option>";
        }
    }

    public function fileUpload()
    {

        $config['upload_path'] = FCPATH . '/assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png|ico|pdf|xlsx|xls|doc|docx';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $token = $this->input->post('token_foto');
            $nama = $this->upload->data('file_name');
            $this->db->insert('COQ_FILE', array('NM_FILE' => $nama, 'TOKEN' => $token));
        }
    }

    //Untuk menghapus foto
    function remove_foto()
    {

        //Ambil token foto
        $token = $this->input->post('token');
        $foto = $this->db->get_where('COQ_FILE', array('TOKEN' => $token));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->NM_FILE;
            if (file_exists($file = FCPATH . '/assets/upload/' . $nama_foto)) {
                unlink($file);
            }
            $this->db->delete('COQ_FILE', array('TOKEN' => $token));

        }
    }


    public function simpanMessage()
    {
        if (isset($_POST["message"])) {
            $column = array(
                'ID_USER', 'CHAT', 'TANGGAL', 'ID_REQUEST'
            );

            $id_user = $this->session->userdata("USER")->ID_USER;
            $date = date('d-m-Y H:i:s');

            $data = array(
                "'" . $id_user . "'", "'" . $_POST["message"] . "'", "sysdate", "'" . $_POST["id_request"] . "'"
            );
            $q = $this->M_quality->simpanMessage($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function simpan()
    {

        $this->load->library('Template2');
        $header = $this->input->post("header");
        $isi = $this->input->post("isi_data");
        if (isset($header["nm_quality"])) {

            // $ambilid = $this->M_quality->ambilId('COQ_QUALITY_HEADER')->row();
            // $idKode = ($ambilid->ID_QUALITY_HEADER)+1;
            $getKapal = $this->M_quality->getKapal($header["request"]);
            if ($getKapal['IS_KAPAL'] == '') {
                $kodee = 'CD';
            } else if ($getKapal['IS_KAPAL'] == '0') {
                $kodee = 'CD';
            } else if ($getKapal['IS_KAPAL'] == '1') {
                $kodee = 'CE';
            }

            $id_user = $this->session->userdata("USER")->ID_USER;
            $column = array(
                'NM_QUALITY', 'ID_STANDART', 'ID_PRODUCT_TYPE', 'ID_PRODUCT', 'ID_REQUEST', 'ID_USER', 'TANGGAL', 'IS_DELETE', 'ID_PLANT', 'START_PRODUCE', 'END_PRODUCE', 'FORMULAS','ID_BRAND','NM_KAPAL','BILL_NUMBER','BILL_DATE','PORT_LOADING','PORT_DISCHARGE','DESC_SAMPLE','NOTIFY_PARTY','QUANTITY','IS_PRINT'
            );
            
            $data = array(
                "'" .  $header["nm_quality"] . "'", "'" . $header["standart"] . "'", "'" . $header["tipe"] . "'", "'" . $header["produk"] . "'", "'" . $header["request"] . "'", "'" . $id_user . "'", "SYSDATE", "0", "'" . $header["plant"] . "'", "'" . $header["start"] . "'", "'" . $header["end"] . "'", "'" . $header["formulas"] . "'", "'" . $header["brand"] . "'", "'" . str_replace("'", " ", $header["vessel_name"]) . "'", "'" . str_replace("'", " ", $header["bill_number_2"]) . "'", "'" . str_replace("'", " ", $header["bill_date_2"]) . "'", "'" . str_replace("'", " ", $header["port_loading_2"]) . "'", "'" . str_replace("'", " ", $header["port_discharge_2"]) . "'", "'" . str_replace("'", " ", $header["desc_sample_2"]) . "'", "'" . str_replace("'", " ", $header["notify_party_2"]) . "'", "'" . str_replace("'", " ", $header["quantity_1"]) . "'", "'" . $header["prints"] . "'"
            );

            $q = $this->M_quality->simpan($column, $data);
            $id_ = ($q[0]->ID_QUALITY_HEADER);

            $kode = $kodee . date('Y') . str_pad($id_, 4, '0', STR_PAD_LEFT);
            $this->M_quality->updateKode($id_, $kode);
            //uPDATE Request
            if ($header["request"] != '') {
                $this->M_quality->update_status_request($header["request"]);
            }

            $column = array(
                'ID_JOIN', 'ID_USER', 'STATUS', 'TANGGAL', 'TIPE', 'ID_REQUEST', 'ID_PLANT'
            );
            $data = array(
                "'" . $id_ . "'", "'" . $id_user . "'", "1", "SYSDATE", "1", "'" . $header["request"] . "'", "'" . $header["plant"] . "'"
            );

            $q = $this->M_quality->simpan_approve($column, $data);
            //// TEMPLATE 
            $i = 0;
            if (ISSET($isi)) {
                foreach ($isi as $key => $val) {
                    $datas[$i]['ID_QUALITY_HEADER'] = $id_;
                    $datas[$i]['ID_UJI'] = $val['parameter'];
                    $datas[$i]['ID_STANDART_UJI'] = $val['standart'];
                    $datas[$i]['NILAI_STD'] = $val['nilai'];
                    $datas[$i]['NILAI_SPEC'] = $val['result'];
                    $i++;
                }
                $q = $this->M_quality->addTemplate('COQ_QUALITY_DETAIL', $datas);
            }

            ////Send Email to Manajer
            $tipe = 'Quality';
            $detail = $this->M_approval->getDetailQuality($id_);


            $cekTo = $this->M_quality->cekTo('32',$header["plant"]);
            $cekSend = $this->M_quality->cekTo('30',$header["plant"]);
            $cek = $cekTo['EMAIL'];
            $send = $cekSend['FULLNAME'];
            $kepada = $cekTo['FULLNAME'];
            $status1 = '2';
            $status2 = '21';
            $array = array();

//            $email = 'a.khafitsbi.ext@semenindonesia.com';
            /*$email = 'department.qa@semenindonesia.com';
            $cc = 'robiyanto@sisi.id';
            $bcc = '';*/
            $email = $cek;
            $cc = '';
            $bcc = '';

            $param['short'] = 'ERF';
            $param['link'] = 'erf';
            $param['title'] = $tipe;
            $param['id_tipe'] = '1';
            $param['sender'] = $send;
            $param['code'] = '123123';
            $param['ID'] = $id_;
            $param['STATUS1'] = $status1;
            $param['STATUS2'] = $status2;
            $param['KEPADA'] = $kepada;
            $param['KIMIA'] = $this->M_approval->cek_quality($id_, '1');
            $param['FISIKA'] = $this->M_approval->cek_quality($id_, '2');
            $param['APP']['BADGE'] = '';
            $param['APP']['NAMA'] = '';
            $param['APP']['UK_TEXT'] = '';
            $param['TRN']['NO'] = "<b>ID. {$tipe} : </b> " . $detail['KODE'];
            $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
            $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
            $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
            $param['TRN']['NOTE'] = "<b>Keterangan :</b> ";
            $this->kirim_email_with_attchment($email, $cc, $bcc, $this->template2->set_app($param), '', $id_);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function kirim_email2($to, $cc, $bcc, $docisi, $data)
    {
        $email_TO = $to;
        $email_CC = $cc;
        $eol = "\r\n";
        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($email_TO, $subject, $docisi, $headers);
    }

    public function kirim_email_with_attchment($to, $cc, $bcc, $docisi, $data, $id)
    {
        $ambilS = $this->M_quality->ambilStand('COQ_QUALITY_HEADER', $id)->row_array();  
        $idStand = $ambilS['ID_STANDART'];
        $datas = [
            'detail' => $this->M_quality->get_data_template($id),
        ];
        $user = [
            'Approval' => $this->M_quality->get_user($id),
        ];
        $par = $this->M_quality->cek_spec_template($id);
        $detail_data = $this->M_quality->get_data_template2($id,$idStand);

        //$detail = $this->M_display->get_data($id);
        //echo json_encode($par);exit;
        // echo json_encode(count($datas['elab_t_bppuc']));exit;


        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6

        // $this->load->view('login');
        if ($datas[0]['ID_COMPANY'] == '8') {
            $mpdf->SetWatermarkText('PT SEMEN PADANG');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '9') {
            $mpdf->SetWatermarkText('PT SEMEN TONASA');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '10') {
            $mpdf->SetWatermarkText('PT SEMEN INDONESIA');
            $mpdf->showWatermarkText = true;
        }


        $html = $this->load->view('printpdf/create_template_email', ['data' => $datas, 'list_par' => $par, 'detail' => $detail_data], true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $fileatt = $mpdf->Output('Certificated.pdf', \Mpdf\Output\Destination::STRING_RETURN);
        $attachment = chunk_split(base64_encode($fileatt));

        $email_TO = $to;
        $email_CC = $cc;
        $fileName = 'File_Cerfificated.pdf';
        $eol = PHP_EOL;
        $separator = md5(uniqid(time()));

        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
        $headers .= "This is a multi-part message in MIME format.\r\n";
        $message = "--" . $separator . $eol;
        $message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
        $message .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $message .= $docisi . $eol;

        $message .= "--" . $separator . $eol;
        $message .= "Content-Type: application/octet-stream; name=\"" . $fileName . "\"" . $eol;
        $message .= "Content-Transfer-Encoding: base64" . $eol;
        $message .= "Content-Disposition: attachment" . $eol . $eol;
        $message .= $attachment . $eol;
        $message .= "--" . $separator . "--";

        mail($email_TO, $subject, $message, $headers);
    }

    public function update()
    {
        $this->load->library('Template2');
        $header = $this->input->post("header");
        $isi = $this->input->post("isi_data");
        if (isset($header["nm_quality"])
        ) {
            $id = intval($header["id"]);
            $column = array(
                'NM_QUALITY', 'ID_STANDART', 'ID_PRODUCT_TYPE', 'ID_PRODUCT', 'ID_REQUEST', 'ID_PLANT', 'START_PRODUCE', 'END_PRODUCE', 'FORMULAS','ID_BRAND','NM_KAPAL','BILL_NUMBER','BILL_DATE','PORT_LOADING','PORT_DISCHARGE','DESC_SAMPLE','NOTIFY_PARTY','QUANTITY'
            );
            $data = array(
                "'" . $header["nm_quality"] . "'", "'" . $header["standart"] . "'", "'" . $header["tipe"] . "'", "'" . $header["produk"] . "'", "'" . $header["request"] . "'", "'" . $header["plant"] . "'", "'" . $header["start"] . "'", "'" . $header["end"] . "'", "'" . $header["formulas"] . "'", "'" . $header["brand"] . "'", "'" . str_replace("'", " ", $header["vessel_name"]) . "'", "'" . str_replace("'", " ", $header["bill_number_2"]) . "'", "'" . str_replace("'", " ", $header["bill_date_2"]) . "'", "'" . str_replace("'", " ", $header["port_loading_2"]) . "'", "'" . str_replace("'", " ", $header["port_discharge_2"]) . "'", "'" . str_replace("'", " ", $header["desc_sample_2"]) . "'", "'" . str_replace("'", " ", $header["notify_party_2"]) . "'", "'" . str_replace("'", " ", $header["quantity_1"]) . "'"
            );


            $jml_kolom = count($column);
            $data_baru = array();

            for ($i = 0; $i < $jml_kolom; $i++) {
                $data_baru[] = $column[$i] . "=" . $data[$i];
            }

            $q = $this->M_quality->update($id, $data_baru);

            //// TEMPLATE
            $q = $this->M_quality->deleteTemplate($id);
            $i = 0;
            if (ISSET($isi)) {
                foreach ($isi as $key => $val) {
                    $datas[$i]['ID_QUALITY_HEADER'] = $id;
                    $datas[$i]['ID_UJI'] = $val['parameter'];
                    $datas[$i]['ID_STANDART_UJI'] = $val['standart'];
                    $datas[$i]['NILAI_STD'] = $val['nilai'];
                    $datas[$i]['NILAI_SPEC'] = $val['result'];
                    $i++;
                }
                $q = $this->M_quality->addTemplate('COQ_QUALITY_DETAIL', $datas);
            }
            ///  CEK UPDATE AWAL ATAU UPDATE REJECT DARI MANAJER DAN SM
            $id_user = $this->session->userdata("USER")->ID_USER;
            $cek_status = $this->M_quality->cek_status($id, $header["plant"])->result();
            $status = $cek_status[0]->STATUS;
            if ($status == '21' || $status == '31' || $status == '1' || $status == '41') {


                ////Send Email to Manajer
                $tipe = 'Quality';
                $detail = $this->M_approval->getDetailQuality($id);


                $cekTo = $this->M_quality->cekTo('32',$header["plant"]);
            $cekSend = $this->M_quality->cekTo('30',$header["plant"]);
            $cek = $cekTo['EMAIL'];
            $send = $cekSend['FULLNAME'];
            $kepada = $cekTo['FULLNAME'];
            $status1 = '2';
            $status2 = '21';
            $array = array();

//            $email = 'a.khafitsbi.ext@semenindonesia.com';
            /*$email = 'department.qa@semenindonesia.com';
            $cc = 'robiyanto@sisi.id';
            $bcc = '';*/
            $email = $cek;
            $cc = '';
            $bcc = '';

            $param['short'] = 'ERF';
            $param['link'] = 'erf';
            $param['title'] = $tipe;
            $param['id_tipe'] = '1';
            $param['sender'] = $send;
            $param['code'] = '123123';
            $param['ID'] = $id;
            $param['STATUS1'] = $status1;
            $param['STATUS2'] = $status2;
            $param['KEPADA'] = $kepada;
            $param['KIMIA'] = $this->M_approval->cek_quality($id, '1');
            $param['FISIKA'] = $this->M_approval->cek_quality($id, '2');
            $param['APP']['BADGE'] = '';
            $param['APP']['NAMA'] = '';
            $param['APP']['UK_TEXT'] = '';
            $param['TRN']['NO'] = "<b>ID. {$tipe} : </b> " . $detail['KODE'];
            $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
            $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
            $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
            $param['TRN']['NOTE'] = "<b>Keterangan :</b> ";
            $this->kirim_email_with_attchment($email, $cc, $bcc, $this->template2->set_app($param), '', $id);


                $column = array(
                    'ID_JOIN', 'ID_USER', 'STATUS', 'TANGGAL', 'TIPE', 'ID_REQUEST', 'ID_PLANT'
                );
                $data = array(
                    "'" . $id . "'", "'" . $id_user . "'", "1", "SYSDATE", "1", "'" . $header["request"] . "'", "'" . $header["plant"] . "'"
                );

                $q = $this->M_quality->simpan_approve($column, $data);

            }
            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }


    public function hapus()
    {
        if (isset($_POST["id"])) {
            $q = $this->M_quality->hapus($_POST["id"]);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }
    public function cek_approve($id){
        $user =  $this->M_quality->get_user($id);
        $datas = [
            'detail' => $this->M_quality->get_data_template($id),
        ];
        
        $par = $this->M_quality->cek_spec_template($id);
        $detail_data = $this->M_quality->get_data_template($id);
        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6

        $mpdf = new mPDF('utf-8',    // mode - default ''
            'A4',    // format - A4, for example, default ''\
            0,     // font size - default 0
            '',    // default font family
            15,    // margin_left
            15,    // margin right
            10,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            'L');  // L - landscape, P - portrait
        // $this->load->view('login');

        


        $html = $this->load->view('printpdf/detailApproveCOQ', ['data' => $datas, 'list_par' => $par, 'approve' => $user], true);

        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
//        echo '<pre>';
//        print_r($datas);
//                echo $html;exit();
        $mpdf->WriteHTML($html);
        $mpdf->Output();
//        $mpdf->Output('COQ.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }
    
    public function template($id)
    {
        $ambilS = $this->M_quality->ambilStand('COQ_QUALITY_HEADER', $id)->row_array();  
        $idStand = $ambilS['ID_STANDART'];
        $datas = [
            'detail' => $this->M_quality->get_data_template($id),
        ];
        $user = [
            'Approval' => $this->M_quality->get_user($id),
        ];
        $par = $this->M_quality->cek_spec_template($id);
        $detail_data = $this->M_quality->get_data_template2($id,$idStand);

        //$detail = $this->M_display->get_data($id);
        //echo json_encode($par);exit;
        // echo json_encode(count($datas['elab_t_bppuc']));exit;


        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6

        $mpdf = new mPDF('utf-8',    // mode - default ''
            'A4',    // format - A4, for example, default ''\
            0,     // font size - default 0
            '',    // default font family
            15,    // margin_left
            15,    // margin right
            10,     // margin top
            0,    // margin bottom
            0,     // margin header
            0,     // margin footer
            'L');  // L - landscape, P - portrait
        // $this->load->view('login');



        $html = $this->load->view('printpdf/templateCOQ', ['data' => $datas, 'list_par' => $par, 'approve' => $user, 'detail' => $detail_data], true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
//        echo '<pre>';
//        print_r($datas);
//                echo $html;exit();
        $mpdf->WriteHTML($html);
        /*$mpdf->Output();*/
        $mpdf->Output('COQ.pdf', \Mpdf\Output\Destination::DOWNLOAD);
    }

    public function detail()
    {
        $id = $_POST['id'];
        $detail_data = $this->M_quality->get_data_template($id);
        $par = $this->M_quality->cek_spec_template($id);
        $parameter = '';
        $date=date('m');
        $fisika = '';
        $no = 0;
        $logo = '';
        $kan = '';
        $kode = '';

        if ($detail_data[0]['ID_PRODUCT_TYPE'] == '22' || $detail_data[0]['ID_PRODUCT_TYPE'] == '26' || $detail_data[0]['KODE_BRAND'] == '') {
            $kode .= 'CL';
        }else{
            $kode .= $detail_data[0]['KODE_BRAND'];
        }


    
       
        if ($detail_data[0]['ID_COMPANY'] == '8') {
            
            $kan .= "<td align='right' width='5%''><img src='../assets/image/kan_logo.jpeg' width='68' height='50' /> ";
            
        }elseif ($detail_data[0]['ID_COMPANY'] == '9') {
         $kan .= "<td align='right' width='5%'><img src='../assets/image/kan_logo_tonasa.jpeg' width='68' height='50' /> ";
            
        }elseif ($detail_data[0]['ID_COMPANY'] == '12') {
            if ($detail_data[0]['ID_PLANT'] == '10') {
               $kan .= "<td align='right' width='5%'><img src='../assets/image/kan_logo_sbi.jpeg' width='68' height='50' /> ";
            
            }elseif ($detail_data[0]['ID_PLANT']  == '9') {
               $kan .= "<td align='right' width='5%'><img src='../assets/image/kan_logo_sbi.jpeg' width='68' height='50' /> ";
            
            }
        }
        
                
        if ($detail_data[0]['ID_COMPANY'] == '8') {
            $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td> <td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }elseif ($detail_data[0]['ID_COMPANY'] == '10') {
           $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td> <td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }
        elseif ($detail_data[0]['ID_COMPANY'] == '9') {
           $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td> <td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }
        elseif ($detail_data[0]['ID_COMPANY'] == '7') {
           $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td><td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }
        elseif ($detail_data[0]['ID_COMPANY'] == '11') {
           $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td><td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }
        elseif ($detail_data[0]['ID_COMPANY'] == '12' || $detail_data[0]['ID_COMPANY'] == '13') {
           $logo .= "<tr><td align='left'><img src='../assets/image/Logo SIG.png' width='60' height='48' /></td> <td><small style='font-size:8px;font-family: Arial Bold; '><br><br><b>PT. Semen Indonesia (Persero) Tbk.</b><br>South Quarter Tower A Lt. 19-20<br>Jl. RA Kartini Kav. 8, Jakarta Selatan 12430, Indonesia</small></td>
                <td style='padding-right: 80px;'><br><small style='font-size:8px;font-family: Arial Bold; '>p. +62 21 5261174-5<br>f. +62 21 5261176</small></td></tr>";
        }
        foreach ($par as $p) {
            if(empty($p['NM_BRAND'])){
                $p['NM_BRAND'] = '-';
            }
            if (empty($p['NILAI_STD'])) {
                $p['NILAI_STD'] = '-';
                $p['MARK'] = '';
            }
            if (empty($p['NILAI_SPEC'])) {
                $p['NILAI_SPEC'] = '-';
                $p['MARK'] = '';
            }
            
            if ($p['ID_KATEGORI'] == '1') {
                if (count($p['NAMA_UJI']) == '0') {
                    $parameter .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                } else {
                    $no++;
                    $parameter .= "<tr style='border-bottom: 1px dotted black;'><td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_UJI']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td style='border-right: 1px solid black;color:blue;'><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                }
            } elseif ($p['ID_KATEGORI'] == '2') {
                if (count($p['NAMA_UJI']) == '0') {
                    $fisika .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                } else {

                    $no++;
                    if (count($p['NAMA_PARENT']) == '0') {

                        $fisika .= "<tr style='border-bottom: 1px dotted black;'><td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td ><small style='font-size: 10px;'><center> {$p['NAMA_UJI']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td style='border-right: 1px solid black;color:blue;'><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                    } else {
                        $fisika .= "<tr style='border-bottom: 1px dotted black;'><td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_PARENT']}<br> {$p['NAMA_UJI']}</center></small></td>
                    <td style='border-right: 1px solid black;' ><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td style='border-right: 1px solid black;'><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td style='border-right: 1px solid black;color:blue;'><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                    }
                }
            }
        }
        //echo json_encode($detail_data);exit();

        $detail = "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'></div>
                                </div>
                                <div class='modal-body' style='overflow-y: scroll; max-height:100%;'>  
                                             <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                {$logo}
                                                
                                            </table>
                                            <hr style='height: 2px;border: 0; box-shadow: inset 0 12px 12px -12px; padding: 0px;margin: 0px;'>
                                            <br>
                                            <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                 <tr><td align='center'style='padding-left: 30px' ><center><u><b>CERTIFICATE OF QUALITY (COQ)</b></u></center></td> {$kan}</tr>
                                                <tr><td align='center' style='padding-left: 30px'><center><b>PT {$detail_data[0]['NM_COMPANY']} ({$detail_data[0]['NM_PLANT']})</b></center></td> </tr>
                                                
                                            </table>
                                         
                                           <table width='80%' align='center' style='border-collapse: collapse; border: none;'>
                                                <tr>

                                                    <td style='text-align: center;'>
                                                        <small style='font-weight: bold'>Certificate No :</small>
                                                        <small style='font-weight: bold;font-size: 12px;color: blue;'>{$detail_data[0]['KD_QUALITY']}/{$date}/{$kode}</small>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td style='text-align: center; '>
                                                    <small style='font-weight: bold'>Brand : </small>
                                                        <small style='font-weight: bold;font-size: 12px;color: blue;'>
                                                            
                                                                 {$detail_data[0]['NM_BRAND']}

                                                        </small>
                                                    </td>


                                                </tr>
                                            </table>

                                            <table width='200px' style='border-collapse: collapse; border: none; max-width: 200px'>
                                                <tr>
                                                    <td width='80px' style='text-align: right'>
                                                        <small style='font-size: 10px;'>Date Produce</small>
                                                    </td>
                                                    <td width='5px'>
                                                        <small style='font-size: 10px;'>:</small>
                                                    </td>
                                                    <td>
                                                        <small style='font-weight: normal;font-size: 10px;color: blue;'>{$detail_data[0]['START_PRODUCE']}</small>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style='text-align: right;'>
                                                        <small style='font-size: 10px; align-content: right'>To</small>
                                                    </td>
                                                    <td>
                                                        <small style='font-size: 10px'>:</small>
                                                    </td>
                                                    <td>
                                                        <small style='font-weight: normal;font-size: 10px;color: blue;'>{$detail_data[0]['END_PRODUCE']}</small>
                                                    </td>

                                                </tr>
                                            </table>
                                         <table  width='100%' style='border-collapse: collapse;border: 1px solid black;'>
                                             <thead>
                                                    <tr>
                                                       
                                                        <th colspan='4' width='52%' style='border: 1px solid black;'>

                                                            <center style='font-size: 10px;margin-top:5px;'><p>QUALITY PARAMETERS</p></center>
                                                        </th>
                                                    
                                                        <th align='center' colspan='3' width='48%' style='border: 1px solid black;'>
                                                        <center><small style='font-size: 10px;'>{$detail_data[0]['NM_PRODUCT_TYPE']} {$detail_data[0]['KD_PRODUCT_TYPE']}</small></center></th>
                                                    </tr>
                                                </thead>   
                                                <tbody>
                                                        <tr>

                                                            
                                                            <th colspan='3' width='44%' style='text-align: left;border: 1px solid black;'>
                                                                <small style='font-size: 10px;'>&nbsp;&nbsp;<b>I.CHEMICAL COMPOSITION : </b></small>
                                                            </th>

                                                            <th width='8%' style='border: 1px solid black;'>
                                                                <center>
                                                                    <small style='font-size: 10px;'><b>UNIT</b></small>
                                                                </center>
                                                            </th>
                                                            <th align='center' width='17%' style='border: 1px solid black;'>
                                                                <center>
                                                                    <small style='font-size: 10px;'><b>TESTING METHOD</b></small>
                                                                </center>
                                                            </th>
                                                            <th align='center' width='17%' style='border: 1px solid black;'>
                                                                <center>
                                                                    <small style='font-size: 10px;'><b>TEST RESULT</b></small>
                                                                </center>
                                                            </th>
                                                            <th align='center' width='14%' style='border: 1px solid black;'>
                                                                <center>
                                                                    <small style='font-size: 10px;'><b>SPECIFICATION</b></small>
                                                                </center>
                                                            </th>
                                                        </tr>
                                            </tbody>
                                                
                                                {$parameter}
                                                <tbody>
                                                        

                                            
                                                        <tr>

                                                            
                                                            <th colspan='3' width='44%' style='text-align: left;border: 1px solid black;'>
                                                                <small style='font-size: 10px;'>&nbsp;&nbsp;<b>II.PHYSICAL PROPERTIES : </b></small>
                                                            </th>

                                                            <th width='8%' style='border: 1px solid black;'>
                                                                
                                                            </th>
                                                            <th align='center' width='17%' style='border: 1px solid black;'>
                                                                
                                                            </th>
                                                            <th align='center' width='17%' style='border: 1px solid black;'>
                                                                
                                                            </th>
                                                            <th align='center' width='14%' style='border: 1px solid black;'>
                                                                
                                                            </th>
                                                        </tr>
                                            </tbody>
                                                
                                                {$fisika}
                                               
                                            </tbody>
                                        </table>

                                    
                                        
                                   
                                      
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";

        echo $detail;


    }
}
