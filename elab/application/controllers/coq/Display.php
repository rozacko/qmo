<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('coq/M_display');
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Display SPEC/QUALITY";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('coq/print', $data);
    }


    public function ajax_list()
    {
        $list = $this->M_display->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $arr) {
            $no++;
            $row = array();
            $row[] = $no;

            $row[] = "<a class='btn-labelx success' title='Click for Detail' onclick='detail_request(" . $arr['ID_REQUEST'] . ")'>" . $arr['KD_REQUEST'] . "</a>";
            $row[] = $arr['NM_PRODUCT_TYPE'];
            $row[] = $arr['CREATE_DATE'];
            $row[] = '
            			<button class="btn btn-primary btn-xs waves-effect" onclick="print(' . $arr['ID_REQUEST'] . ')" title="Print"><span class="fa fa-print"></span> </button>
                       
                      
            			<button class="btn btn-success btn-xs waves-effect" onclick="print(' . $arr['ID_REQUEST'] . ')" title="Print"><span class="fa fa-eye"></span> </button>
                       
                      ';
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_display->count_all(),
            "recordsFiltered" => $this->M_display->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function edit($id)
    {
        $sql = $this->M_display->get_id($id);
        echo json_encode($sql);
    }

    public function template($id)
    {
        $datas = [
            'detail' => $this->M_display->get_data($id),
        ];
        $par = $this->M_display->cek_spec($id);
        $detail_data = $this->M_display->get_data($id);
        //$detail = $this->M_display->get_data($id);
        //echo json_encode($par);exit;
        // echo json_encode(count($datas['elab_t_bppuc']));exit;

        
        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6
        
        // $this->load->view('login');
        if ($detail_data['ID_COMPANY'] == '8') {
            $mpdf->SetWatermarkText('PT SEMEN PADANG');
            $mpdf->showWatermarkText = true;
        }elseif ($detail_data['ID_COMPANY'] == '9') {
            $mpdf->SetWatermarkText('PT SEMEN TONASA');
            $mpdf->showWatermarkText = true;
        }elseif ($detail_data['ID_COMPANY'] == '10') {
             $mpdf->SetWatermarkText('PT SEMEN INDONESIA');
            $mpdf->showWatermarkText = true;
        }
       
        

        $html = $this->load->view('printpdf/template', ['data' => $datas, 'list_par' => $par], true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
}
