<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->helper(array('url', 'file'));
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('coq/M_pages');
        
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Pages";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->dropzone_plugin();

        $this->layout->render('coq/pages', $data);
    }
     public function ajax_list()
    {
        $list = $this->M_pages->get_datatables();
        //echo json_encode(['data'=>$list]);
         $data = array();
         $no = $_POST['start'];
         foreach ($list as $arr) {
             $no++;
             $row = array();
             $row[] = $no;
             $row[] = $arr->MENU;
             $row[] = $arr->LINK;
             $row[] = '<center><button onclick="edit('.$arr->ID_MENU.')" class="btn btn-primary btn-xs waves-effect btn_edit"><span class="btn-labelx"><i class="fa fa-pencil"></i></span></button> <button  onclick="konfirmasi('.$arr->ID_MENU.')" class="btn btn-danger btn-xs waves-effect btn_hapus"><span class="btn-labelx"><i class="fa fa-trash-o"></i></span></button></center>';
             $data[] = $row;
         }
         $output = array(
       "draw" => $_POST['draw'],
       "recordsTotal" => $this->M_pages->count_all(),
       "recordsFiltered" => $this->M_pages->count_filtered(),
       "data" => $data,
         );
         echo json_encode($output);
    }
    public function hapus(){
    $id = $this->input->post('id');
    $this->M_pages->hapus($id);

    echo json_encode(array('status'=>true));
  }
  public function tambah(){
    
    $menu = $this->input->post('menu');
    $link = $this->input->post('link');
    $flag = '0';
    
    $data = array('MENU' =>$menu,
                  'LINK'=>$link,
                  'FLAG_ACTIVE'=>$flag);
      $this->M_pages->add($data);
      echo json_encode(array('status'=>TRUE));
  }
  public function edit($id){
    $sql = $this->M_pages->editData($id);
    echo json_encode($sql);
  }
 public function update(){
    $id = $this->input->post('id_menu');
    $menu = $this->input->post('menu');
     $link = $this->input->post('link');
    $flag = '0';
    $data = array('MENU' =>$menu,
                  'LINK'=>$link,
              'FLAG_ACTIVE'=>$flag);
      $where = array('ID_MENU'=>$id);
      $sql = $this->M_pages->updateData($where, $data, 'COQ_PORTAL');

      $status = false;
      if($sql > 0){
        $status = TRUE;
      }
      echo json_encode(array('status'=>$status));
  }
    
}
