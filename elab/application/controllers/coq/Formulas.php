
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulas extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_parameter');
		$this->load->model('coq/M_formulas');
		$this->load->model('coq/M_standart_uji');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Parameter Uji Formulas";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js(); 

		$this->layout->render('coq/formulas', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_formulas->get_data();
		echo json_encode($detail_data);

    }

    public function get_parameter_uji() {
    	# code...
		$data = $this->M_formulas->getParameter();
		
		foreach($data as $p) {
			echo '<button type="button" style="margin: 3px;" class="btn bg-orange margin" onclick="insertAtCaret(\'vformulas\', \''.$p["ALIASESPRM"].'\');return false;" >'.$p["NAMA_UJI"].'</button>';
		}
		// echo json_encode($this->M_standart_uji->getParameter());
    }

	public function simpan() {
        
		if(isset($_POST["parameter"]) ){
			// $column = array( 
				 // 'NM_PARAMETER','SYMBOL','UNIT_SI' ,'UNIT_EN','UNIT_SNI','TEST_ASTM','TEST_EN' ,'TEST_SNI','MARK_ASTM','MARK_EN','MARK_SNI' ,'PARENT','ID_PRODUCT' 
			// );

			$column = array( 
				 'VFORMULAS','ALIASES','ID_PARAM_UJI'
			);

			$data = array(
				 "'".$_POST["vformulas"]."'", "'".$_POST["aliases"]."'", "'".$_POST["parameter"]."'" 
			);
            // $data = array(
				 // "'".$_POST["parameter"]."'", "'".$_POST["simbol"]."'", "'".$_POST["unit_si"]."'", "'".$_POST["unit_en"]."'", "'".$_POST["unit_sni"]."'", "'".$_POST["test_astm"]."'", "'".$_POST["test_en"]."'", "'".$_POST["test_sni"]."'", "'".$_POST["mark_astm"]."'", "'".$_POST["mark_en"]."'", "'".$_POST["mark_sni"]."'", "'".$parent_menu."'", "'".$_POST["produk"]."'",  
			// );
			$q = $this->M_formulas->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["parameter"]) ){
			$id = intval($_POST["id"]);
         	$column = array( 
				 'VFORMULAS','ALIASES','ID_PARAM_UJI'
			);
			$data = array(
				 "'".$_POST["vformulas"]."'", "'".$_POST["aliases"]."'", "'".$_POST["parameter"]."'" 
			);
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_formulas->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_formulas->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}

	public function testing($value='')
	{
		# code...
		echo $value;
	}

	public function exceuteformulas() {
		# code...

		$formulaoption = '';

		if (isset($_POST["formulas"])) {
			# code...
			$formulaoption = strtolower($_POST["formulas"]);
		}

		if (isset($_POST["parameter"])) {
			# code...
			$paramselected = $_POST["parameter"];
			$paramclean = array();
			foreach ($paramselected as $key => $value) {
				# code...

				$paramclean[$this->cleans($key)] = $value;

			}

			extract($paramclean, EXTR_SKIP);
		}


	    // $equation =  evalmath('totoal+totoal+totoal+totoal');
	    $equation =  $this->evalmath($_POST["formulas"]);

	    if ( $equation != "" ){

	        $result = @eval("return " . $equation . ";" );
	    }
	    if ($result == null) {

	        throw new Exception("Unable to calculate equation");
	    }

	    $hasil = array();
	    $hasil['Hasil Kalkulasi'] = $result;
	    echo json_encode($hasil);

	}

	public function cleans($string) {
	   // $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

	   return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
	}

	public function evalmath($equation)
    {
        $result = 0;
        // sanitize imput
        $equation = preg_replace("/[^a-z0-9+\-.*\/()%]/","",$equation);
        // convert alphabet to $variabel 
        $equation = preg_replace("/([a-z])+/i", "\$$0", $equation); 
        // convert percentages to decimal
        $equation = preg_replace("/([+-])([0-9]{1})(%)/","*(1\$1.0\$2)",$equation);
        $equation = preg_replace("/([+-])([0-9]+)(%)/","*(1\$1.\$2)",$equation);
        $equation = preg_replace("/([0-9]{1})(%)/",".0\$1",$equation);
        $equation = preg_replace("/([0-9]+)(%)/",".\$1",$equation);
        /*if ( $equation != "" ){
            $result = @eval("return " . $equation . ";" );
        }
        if ($result == null) {
            throw new Exception("Unable to calculate equation");
        }
        return $result;*/
        return $equation;
    }
    
    
}
