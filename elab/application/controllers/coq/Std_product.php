
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Std_product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_std_product');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['product'] = $this->M_std_product->product();
		$data['standart'] = $this->M_std_product->standart();
		$data['title'] = "Master Standart Product";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/standart_produk', $data);
	}
     
	 
    public function get_data()
    {
        $list = $this->M_std_product->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $arr) {
            $no++;
            $row = array();
            $row[] = $no; 
            
            $row[] = $arr['KD_PRODUCT_TYPE'] .' - '. $arr['NM_PRODUCT_TYPE'];
            $row[] = $arr['NM_STANDART'];
            $row[] = $arr['NM_STANDART_PRODUCT'];
            $row[] = '<center>
            			<button class="btn btn-primary btn-xs waves-effect" onclick="edit(\''.$arr['ID_STD'].'\')" title="Edit"><span class="fa fa-pencil"></span> </button>
                        <button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\''.$arr['ID_STD'].'\')" title="Delete"><span class="fa fa-trash-o"></span> </button>
                      </center>';
            $data[] = $row;
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_std_product->count_all(),
			"recordsFiltered" => $this->M_std_product->count_filtered(),
			"data" => $data,
        );
        echo json_encode($output);
    }

    public function tambah()
	{
		$type_product = $this->input->post('type_product');
		
		$standart = $this->input->post('standart');
		
		$std_product = $this->input->post('std_product');
		

		

		
			$data = array('ID_PRODUCT_TYPE' =>$type_product,						  
						  'ID_STANDART'=>$standart,
						  'NM_STANDART_PRODUCT'=>$std_product);
			$this->M_std_product->add($data);
			echo json_encode(array('status'=>TRUE));
		
	}
	public function hapus(){
		$id = $this->input->post('id');
		$this->M_std_product->hapus($id);

		echo json_encode(array('status'=>true));
	}
	public function edit($id){
		$sql = $this->M_std_product->get_id($id);
		echo json_encode($sql);
	}
	public function update()
	{
		$id = $this->input->post('id_std');
		$type_product = $this->input->post('type_product');
		
		$standart = $this->input->post('standart');
		
		$std_product = $this->input->post('std_product');
		

		

		
			$data = array('ID_PRODUCT_TYPE' =>$type_product,						  
						  'ID_STANDART'=>$standart,
						  'NM_STANDART_PRODUCT'=>$std_product);
			$where = array('ID_STD'=>$id);
			$sql = $this->M_std_product->updateData($where, $data, 'COQ_STANDART_PRODUCT');

			$status = false;
			if($sql > 0){
				$status = TRUE;
			}
			echo json_encode(array('status'=>$status));
	}
    
}
