
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_type extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_product_type');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Product Type";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/product_type', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_product_type->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshKelompok() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kelompok --</option>";
		$data = $this->M_product_type->getKelompok();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KELOMPOK"]}'>".htmlspecialchars($p["NAMA_KELOMPOK"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["kd_product_type"]) ){
			$column = array(
				'NM_PRODUCT_TYPE', 'KD_PRODUCT_TYPE',  
			);
			
			$data = array(
				"'".$_POST["nm_product_type"]."'", "'".$_POST["kd_product_type"]."'" 
			);
			 
			$q = $this->M_product_type->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["kd_product_type"]) 
		) {
			$id = intval($_POST["id"]);
            $column = array(
				'NM_PRODUCT_TYPE', 'KD_PRODUCT_TYPE',  
			);
			
			$data = array(
				"'".$_POST["nm_product_type"]."'", "'".$_POST["kd_product_type"]."'" 
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_product_type->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_product_type->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
