<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_berita');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Berita";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/v_berita', $data);
	}
     public function ajax_list()
  {
    $list = $this->M_berita->get_datatables();
        //echo json_encode(['data'=>$list]);
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $arr) {
     $no++;
     $row = array();
     $row[] = $no;
     $row[] = $arr->TITLE;
     $row[] = $arr->ISI_BERITA;
     $row[] = $arr->IMAGES;
     $row[] = $arr->CREATE_DATE;
     $row[] = "<button class='btn btn-primary btn-xs waves-effect' onclick='edit(".$arr->ID_NEWS.")' title='edit'><i class='fa fa-edit'></i></button>
     <button class='btn btn-danger btn-xs waves-effect' onclick='hapus(".$arr->ID_NEWS.")'  title='hapus'><i class='fa fa-trash'></i></button>";
     $data[] = $row;
   }
   $output = array(
     "draw" => $_POST['draw'],
     "recordsTotal" => $this->M_berita->count_all(),
     "recordsFiltered" => $this->M_berita->count_filtered(),
     "data" => $data,
   );
   echo json_encode($output);
 }
	function tambah(){
  if (isset($_POST['save'])) {
   $title = $this->input->post('title');
   $content = $this->input->post('content');
   $destination_folder = 'assets/upload/';
   $image = /*$id.''.*/$_FILES['foto']['name']; 

   $file = "$destination_folder/$image";
   

    
    

   $data = array('TITLE' =>$title,
     'ISI_BERITA'=>$content,
     'IMAGES'=>$image);
   $this->M_berita->add($data);
   move_uploaded_file($_FILES['foto']['tmp_name'], $file);
   
   
   redirect('coq/Master_berita');

 }
 }
function upload_image(){
    if(isset($_FILES["image"]["name"])){
        $config['upload_path'] = 'assets/upload/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image')){
            $this->upload->display_errors();
            return FALSE;
        }else{
            $data = $this->upload->data();
            //Compress Image
            $config['image_library']='gd2';
            $config['source_image']='assets/upload/'.$data['file_name'];
            $config['create_thumb']= FALSE;
            $config['maintain_ratio']= TRUE;
            $config['quality']= '60%';
            $config['width']= 800;
            $config['height']= 800;
            $config['new_image']= 'assets/upload/'.$data['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo base_url().'assets/upload/'.$data['file_name'];
        }
    }
}
function upload_image_e(){
    if(isset($_FILES["image"]["name"])){
        $config['upload_path'] = 'assets/upload/';
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image')){
            $this->upload->display_errors();
            return FALSE;
        }else{
            $data = $this->upload->data();
            //Compress Image
            $config['image_library']='gd2';
            $config['source_image']='assets/upload/'.$data['file_name'];
            $config['create_thumb']= FALSE;
            $config['maintain_ratio']= TRUE;
            $config['quality']= '60%';
            $config['width']= 800;
            $config['height']= 800;
            $config['new_image']= 'assets/upload/'.$data['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            echo base_url().'assets/upload/'.$data['file_name'];
        }
    }
}
function delete_image(){
    $src = $this->input->post('src');
    $file_name = str_replace(base_url().'assets/upload/', '', $src);
   
}
function delete_image_e(){
    $src = $this->input->post('src');
    $file_name = str_replace(base_url().'assets/upload/', '', $src);
    
}

public function edit($id){
  $sql = $this->M_berita->editData($id);
  echo json_encode($sql);
}
function update(){
  if (isset($_POST['update'])) {
   $id = $this->input->post('id_e');
   $title = $this->input->post('title_e');
   $content = $this->input->post('content_e');
   $destination_folder = 'assets/upload/';
   $image = /*$id.''.*/$_FILES['foto_e']['name']; 
   $file = "$destination_folder/$image";
   

   $data = array('TITLE' =>$title,
     'ISI_BERITA'=>$content,
     'IMAGES'=>$image);
   $where = array('ID'=>$id);
   $sql = $this->M_berita->setuju($where, $data, 'COQ_NEWS');
   move_uploaded_file($_FILES['foto_e']['tmp_name'], $file);
   
   redirect('coq/Master_berita');
 }

}
public function hapus(){
  $id = $this->input->post('id');
  $this->M_berita->hapus($id);

  echo json_encode(array('status'=>true));
}

    
    
}
