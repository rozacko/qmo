
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plant extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_plant');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Plant";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/plant', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_plant->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshCompany() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>";
		$data = $this->M_plant->getCompany();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPANY"]}'>".htmlspecialchars($p["NM_COMPANY"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["nm_plant"]) ){
			$column = array(
				'NM_PLANT','KD_PLANT','ID_COMPANY', 
			);
			
			$data = array(
				"'".$_POST["nm_plant"]."'","'".$_POST["kd_plant"]."'","'".$_POST["company"]."'", 
			);
			 
			$q = $this->M_plant->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_plant"]) 
		) {
			$id = intval($_POST["id"]);
			$column = array(
				'NM_PLANT','KD_PLANT','ID_COMPANY', 
			);
			
			$data = array(
				"'".$_POST["nm_plant"]."'","'".$_POST["kd_plant"]."'","'".$_POST["company"]."'", 
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_plant->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_plant->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
