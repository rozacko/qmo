<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->helper(array('url', 'file'));
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('coq/M_request');
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Request Spec dan Quality";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->dropzone_plugin();

        $this->layout->render('coq/request', $data);
    }

    public function get_data()
    {

        $sess_user = $this->session->userdata("USERLAB");
        $session_user = '';
        $session_role = '';
        $session_plant = '';
        $user = '';
        foreach ($sess_user as $v) {
            $session_user .= $v['ID_USER'] . ",";
            $session_role .= $v['ID_ROLE'] . ",";
            $session_plant .= $v['ID_PLANT'] . ",";
        }
        $role = rtrim($session_role, ",");
        $plant = rtrim($session_plant, ",");
        $detail['role'] = $role;
        if ($role == '31') {
            $user = rtrim($session_user, ",");
        }elseif ($role == '30') {
            $user = rtrim($session_user, ",");
        }
        
        $detail = $this->M_request->get_data($user, $plant);
        $getdata = array();
        foreach ($detail as $val) {
            $tipe_request = ($val['TIPE_REQUEST'] == 0 ? 'COQ_SPEC_HEADER' : 'COQ_QUALITY_HEADER');
            $cek_new = $this->M_request->cek_new($tipe_request, $val['ID_REQUEST'], $val['ID_PLANT']);
            $cek_status = $this->M_request->cek_status($val['ID_REQUEST'], $val['ID_PLANT'])->result();
            $status = $cek_status[0]->STATUS;
            $id_join = $cek_status[0]->ID_JOIN;

            if ($val['TIPE_REQUEST'] == 0) {
                $id_specqua = (count($cek_new['ID_SPEC_HEADER']) > 0 ? $cek_new['ID_SPEC_HEADER'] : '');
                $nm_specqua = (count($cek_new['NM_SPEC']) > 0 ? $cek_new['NM_SPEC'] : '');
            } else {
                $id_specqua = (count($cek_new['ID_QUALITY_HEADER']) > 0 ? $cek_new['ID_QUALITY_HEADER'] : '');
                $nm_specqua = (count($cek_new['NM_QUALITY']) > 0 ? $cek_new['NM_QUALITY'] : '');
            }

            $data['ID_REQUEST'] = $val['ID_REQUEST'];
            $data['KD_REQUEST'] = $val['KD_REQUEST'];
            $data['NM_REQUEST'] = $val['NM_REQUEST'];
            $data['NM_STANDART'] = $val['NM_STANDART'];
            $data['KD_PRODUCT_TYPE'] = $val['KD_PRODUCT_TYPE'];
            $data['NM_PACK'] = $val['NM_PACK'];
            $data['NM_PRODUCT'] = $val['NM_PRODUCT'];
            $data['TIPE_REQUEST'] = $val['TIPE_REQUEST'];
            $data['STATUS'] = $status;
            $data['ID_STANDART'] = $val['ID_STANDART'];
            $data['ID_PRODUCT_TYPE'] = $val['ID_PRODUCT_TYPE'];
            $data['ID_PACK'] = $val['ID_PACK'];
            $data['ID_PRODUCT'] = $val['ID_PRODUCT'];
            $data['NOTE'] = $val['NOTE'];
            $data['NM_COUNTRY'] = $val['NM_COUNTRY'];
            $data['BUYER'] = $val['BUYER'];
            $data['TOTAL'] = $val['TOTAL'];
            $data['STARTDATE'] = $val['STARTDATE'];
            $data['ENDDATE'] = $val['ENDDATE'];
            $data['ID_SPEC'] = $id_specqua;
            $data['NM_SPEC'] = $nm_specqua;
            $data['NEW'] = count($cek_new);
            $data['IS_KAPAL'] = $val['IS_KAPAL'];
            $data['ID_PLANT'] = $val['ID_PLANT'];
            $data['NM_PLANT'] = $val['NM_PLANT'];
            $data['NM_BRAND'] = $val['NM_BRAND'];
            $data['NM_KAPAL'] = $val['NM_KAPAL'];
            $data['BILL_NUMBER'] = $val['BILL_NUMBER'];
            $data['BILL_DATE'] = $val['BILL_DATE'];
            $data['PORT_LOADING'] = $val['PORT_LOADING'];
            $data['PORT_DISCHARGE'] = $val['PORT_DISCHARGE'];
            $data['DESC_SAMPLE'] = $val['DESC_SAMPLE'];
            $data['NOTIFY_PARTY'] = $val['NOTIFY_PARTY'];
            $data['ID_BRAND'] = $val['ID_BRAND'];
            $data['ID_JOIN'] = $id_join;
            $getdata[] = $data;
        }
        echo json_encode(array('data' => $getdata, 'role' => $role));

    }
    public function refreshBrand()
    {
        $id = $this->input->post('id');
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Brands --</option>";
        $data = $this->M_request->getBrand($id);
        foreach ($data as $p) {
            echo "<option value='{$p["ID_BRAND"]}'>" . htmlspecialchars($p["NM_BRAND"]) . "</option>";
        }
    }

    public function detail_request()
    {
        $id_request = $_POST['id_request'];
        $plant = $this->M_request->getPlantID($id_request);
        $file = $this->M_request->getFileID($id_request);

        $v_plant = '';
        foreach ($plant as $p) {
            $v_plant .= $p['NM_PLANT'] . ', ';
        }
        $v_plant = rtrim($v_plant, ", ");

        $v_file = '<div>';
        foreach ($file as $v) {
            $ext = strtolower(pathinfo($v['NM_FILE'], PATHINFO_EXTENSION));
            if ($ext == 'xlsx' || $ext == 'xls' || $ext == 'xls' || $ext == 'csv') {
                $icon = ' fa-file-excel-o';
            } else if ($ext == 'doc' || $ext == 'docx' || $ext == 'xls') {
                $icon = ' fa-file-word-o';
            } else if ($ext == 'pdf') {
                $icon = ' fa-file-pdf-o';
            } else if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                $icon = ' fa-file-image-o';
            } else {
                $icon = ' fa-info';
            }
            $base = base_url() . "assets/upload/{$v['NM_FILE']}";
            $v_file .= "<a href='{$base}'><span class='btn-labelx'><i class='fa {$icon} fa-lg'></i>  {$v['NM_FILE']} </span></a> <br /> <br />";
        }
        $v_file .= '</div>';

        echo json_encode(array('notif' => '1', 'plant' => $v_plant, 'file' => $v_file));


    }


    public function refreshChat()
    {

        $data = $this->M_request->getChatID($_POST['id_request']);
        $id_user = $this->session->userdata("USER")->ID_USER;
        $chat = '';
        $gambar = base_url() . 'assets/img/landing/avatar1.jpg';
        foreach ($data as $v) {
            $posisi = ($id_user == $v['ID_USER'] ? 'right' : 'left');
            $tanggal = date('D, d M Y', strtotime($v['TANGGAL']));
            $chat .= "<div class='chat-message {$posisi}'>
                        <img class='message-avatar' src='{$gambar}' alt='' >
                        <div class='message'>
                            <a class='message-author' href='#'> {$v['FULLNAME']} </a>
                            <span class='message-date'> {$tanggal} </span>
                            <span class='message-content'>
                                                        {$v['CHAT']}
                            </span>
                        </div>
                    </div>";
        }

        echo json_encode(array('notif' => '1', 'chat' => $chat));
    }


    public function refreshPacking()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Packing --</option>";
        $data = $this->M_request->getPacking();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PACK"]}'>" . htmlspecialchars($p["NM_PACK"]) . "</option>";
        }
    }

    public function refreshStandart()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
        $data = $this->M_request->getStandart();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_STANDART"]}'>" . htmlspecialchars($p["NM_STANDART"]) . "</option>";
        }
    }

    public function refreshTipe()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>";
        $data = $this->M_request->getTipe();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PRODUCT_TYPE"]}'>" . htmlspecialchars($p["NM_PRODUCT_TYPE"]) . " (<b>" . htmlspecialchars($p["KD_PRODUCT_TYPE"]) . "</b>) </option>";
        }
    }

    public function refreshProduk()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>";
        $data = $this->M_request->getProduk();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PRODUCT"]}'>" . htmlspecialchars($p["NM_PRODUCT"]) . "</option>";
        }
    }

    public function refreshPlant()
    {
        echo "<option value='' data-hidden='true' >-- Pilih Plant --</option>";
        $data = $this->M_request->getPlant();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_PLANT"]}'>" . htmlspecialchars($p["NM_PLANT"]) . "</option>";
        }
    }

    public function refreshCountry()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Negara --</option>";
        $data = $this->M_request->getCountry();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_COUNTRY"]}'>" . htmlspecialchars($p["NM_COUNTRY"]) . "</option>";
        }
    }

    public function fileUpload()
    {

        $config['upload_path'] = FCPATH . '/assets/upload/';
        $config['allowed_types'] = 'gif|jpg|png|ico|pdf|xlsx|xls|doc|docx';
        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $token = $this->input->post('token_foto');
            $nama = $this->upload->data('file_name');
            $this->db->insert('COQ_FILE', array('NM_FILE' => $nama, 'TOKEN' => $token));
        }
    }

    //Untuk menghapus foto
    function remove_foto()
    {

        //Ambil token foto
        $token = $this->input->post('token');
        $foto = $this->db->get_where('COQ_FILE', array('TOKEN' => $token));
        if ($foto->num_rows() > 0) {
            $hasil = $foto->row();
            $nama_foto = $hasil->NM_FILE;
            if (file_exists($file = FCPATH . '/assets/upload/' . $nama_foto)) {
                unlink($file);
            }
            $this->db->delete('COQ_FILE', array('TOKEN' => $token));

        }
    }
    

    public function simpanMessage()
    {
        if (isset($_POST["message"])) {
            $column = array(
                'ID_USER', 'CHAT', 'TANGGAL', 'ID_REQUEST'
            );

            $id_user = $this->session->userdata("USER")->ID_USER;
            $date = date('d-m-Y H:i:s');

            $data = array(
                "'" . $id_user . "'", "'" . $_POST["message"] . "'", "sysdate", "'" . $_POST["id_request"] . "'"
            );
            $q = $this->M_request->simpanMessage($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function simpan()
    {
        $this->load->library('Template4');
        if (isset($_POST["nm_request"])) {
            $id_user = $this->session->userdata("USER")->ID_USER;
            $start = ($_POST["start"] != '' ? "TO_DATE('01-{$_POST["start"]}', 'DD-MM-YYYY')" : '');
            $end = ($_POST["start"] != '' ? "TO_DATE('01-{$_POST["end"]}', 'DD-MM-YYYY')" : '');

            // $column = array(
            // 'NM_REQUEST' , 'ID_STANDART', 'ID_PRODUCT', 'ID_PRODUCT_TYPE' , 'NOTE' , 'TIPE_REQUEST', 'BUYER',  'ID_COUNTRY',  'TOTAL',  'STARTDATE',   'ENDDATE', 'ID_PACK',  'ID_USER' ,  'CREATE_DATE',  'IS_DELETE',  'IS_KAPAL'
            // );
            // $data = array(
            // "'".$_POST["nm_request"]."'",  "'".$_POST["standart"]."'", "'".$_POST["produk"]."'", "'".$_POST["tipe"]."'", "'".$_POST["note"]."'", "'".$_POST["tipe_request"]."'", "'".$_POST["buyer"]."'", "'".$_POST["country"]."'", "'".$_POST["total"]."'",  "'".$start."'",  "'".$end."'", "'".$_POST["packing"]."'", "'".$id_user."'", "SYSDATE", "0", "'".$_POST["kapal"]."'"
            // );
            $column = array(
                'NM_REQUEST', 'ID_STANDART', 'ID_PRODUCT', 'ID_PRODUCT_TYPE', 'NOTE', 'TIPE_REQUEST', 'BUYER', 'ID_COUNTRY', 'TOTAL', 'STARTDATE', 'ENDDATE', 'ID_PACK', 'ID_USER', 'CREATE_DATE', 'IS_DELETE', 'IS_KAPAL','ID_BRAND','NM_KAPAL','BILL_NUMBER','BILL_DATE','PORT_LOADING','PORT_DISCHARGE','DESC_SAMPLE','NOTIFY_PARTY'
            );
            $data = array(
                "'" . str_replace("'", " ", $_POST["nm_request"]) . "'", "'" . $_POST["standart"] . "'", "'" . $_POST["produk"] . "'", "'" . $_POST["tipe"] . "'", "'" . str_replace("'", " ", $_POST["note"]) . "'", "'" . $_POST["tipe_request"] . "'", "'" . str_replace("'", " ", $_POST["buyer"]) . "'", "'" . $_POST["country"] . "'", "'" . str_replace("'", " ", $_POST["total"]) . "'", "" . $start . "", "" . $end . "", "'" . $_POST["packing"] . "'", "'" . $id_user . "'", "SYSDATE", "0", "'" . $_POST["kapal"] . "'", "'" . $_POST["brand"] . "'", "'" . str_replace("'", " ", $_POST["nm_kapal"]) . "'", "'" . str_replace("'", " ", $_POST["bill_number"]) . "'", "'" . str_replace("'", " ", $_POST["bill_date"]) . "'", "'" . str_replace("'", " ", $_POST["port_loading"]) . "'", "'" . str_replace("'", " ", $_POST["port_discharge"]) . "'", "'" . str_replace("'", " ", $_POST["desc_sample"]) . "'", "'" . str_replace("'", " ", $_POST["notify_party"]) . "'"
            );

            $q = $this->M_request->simpan($column, $data);
            $id_ = ($q[0]->ID_REQUEST);
            $kode = 'RQ' . date('Y') . str_pad($id_, 4, '0', STR_PAD_LEFT);
            $this->M_request->updateKode($id_, $kode);

            $plant = $_POST["plant"];
            $tokenFile = (ISSET($_POST["tokenFile"]) ? $_POST["tokenFile"] : '');
            $i = 0;
            if ($plant != '') {
                foreach ($plant as $val) {
                    $datas[$i]['ID_REQUEST'] = $id_;
                    $datas[$i]['ID_PLANT'] = $val;
                    $i++;
                    $column = array(
                        'ID_REQUEST', 'ID_USER', 'STATUS', 'TANGGAL', 'ID_PLANT'
                    );

                    $data = array(
                        "'" . $id_ . "'", "'" . $id_user . "'", "0", "SYSDATE", "'" . $val . "'"
                    );

                    // Simpan ke tabel approval
                    $this->M_request->simpan_tolakRequest($column, $data);

                }
                $q = $this->M_request->addPlant('COQ_T_PLANT', $datas);
            }
            ////Send Email to Manajer
            /// * Isi body mendeskripsikan file yang di update
            ///
            $tipe = ($_POST["tipe_request"] == '0' ? 'Specification' : 'Certificated');
            $detail = $this->M_request->getDetailReq($id_);

//            print_r($detail);
//            exit();
            
            $cekTo = $this->M_request->cekTo('30',$val);
            $cekSend = $this->M_request->cekToSa('31',$val,$id_user);
            $cek = $cekTo['EMAIL'];
            $send = $cekSend['FULLNAME'];
            $kepada = $cekTo['FULLNAME'];
            $status1 = '0';
            /*$status2 = '21';*/
            $array = array();

            $email = $cek;
            $cc = '';
            $bcc = '';

            $param['short'] = 'ERF';
            $param['link'] = 'erf';
            $param['title'] = $tipe;
            $param['id_tipe'] = $_POST["tipe_request"];
            $param['sender'] = '{ SALES }';
            $param['code'] = '123123';
            $param['ID'] = $id_;
            $param['STATUS1'] = $status1;
            /*$param['STATUS2'] = $status2;*/
            $param['KEPADA'] = $kepada;
            /*$param['KIMIA'] = $this->M_approval->cek_spec($id_, '1');
            $param['FISIKA'] = $this->M_approval->cek_spec($id_, '2');*/
            $param['APP']['BADGE'] = '';
            $param['APP']['NAMA'] = '';
            $param['APP']['UK_TEXT'] = '';
            $param['TRN']['NO'] = "<b>ID. {$tipe} : </b> " . $detail['KODE'];
            $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
            $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
            $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
            $param['TRN']['NOTE'] = "<b>Keterangan :</b> ";
            

            $this->kirimjajal($email, $cc, $bcc, $this->template4->set_app($param), '', $id_);
            if ($tokenFile != '') {
                foreach ($tokenFile as $val) {
                    $this->M_request->EditFile($val, $id_);
                }
            }
            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function update()
    {
        if (isset($_POST["nm_request"])
        ) {
            $id = intval($_POST["id"]);
            /*$column = array(
                'NM_REQUEST', 'ID_STANDART', 'ID_PRODUCT_TYPE', 'ID_PRODUCT', 'NOTE', 'TIPE_REQUEST','ID_BRAND'
            );*/
            $column = array(
                'NM_REQUEST', 'ID_STANDART', 'ID_PRODUCT', 'ID_PRODUCT_TYPE', 'NOTE', 'TIPE_REQUEST', 'BUYER', 'ID_COUNTRY', 'TOTAL', 'STARTDATE', 'ENDDATE', 'ID_PACK', 'ID_USER', 'CREATE_DATE', 'IS_DELETE', 'IS_KAPAL','ID_BRAND'
            );

            $data = array(
                "'" . $_POST["nm_request"] . "'", "'" . $_POST["standart"] . "'", "'" . $_POST["tipe"] . "'", "'" . $_POST["produk"] . "'", "'" . $_POST["note"] . "'", "'" . $_POST["tipe_request"] . "'", "'" . $_POST["buyer"] . "'", "'" . $_POST["country"] . "'", "'" . $_POST["total"] . "'", "" . $start . "", "" . $end . "", "'" . $_POST["packing"] . "'", "'" . $id_user . "'", "SYSDATE", "0", "'" . $_POST["kapal"] . "'", "'" . $_POST["brand"] . "'"
            );


            $jml_kolom = count($column);
            $data_baru = array();

            for ($i = 0; $i < $jml_kolom; $i++) {
                $data_baru[] = $column[$i] . "=" . $data[$i];
            }

            $q = $this->M_request->update($id, $data_baru);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function tolakRequest()
    {

        $this->load->library('Template4');
        $id_user = $this->session->userdata("USER")->ID_USER;
        if (isset($_POST["syarat"])
        ) {

            $column = array(
                'ID_REQUEST', 'KETERANGAN', 'ID_USER', 'STATUS', 'TANGGAL', 'ID_PLANT'
            );

            $data = array(
                "'" . $_POST["id"] . "'", "'" . $_POST["syarat"] . "'", "'" . $id_user . "'", "11", "SYSDATE", "'" . $_POST["plant"] . "'"
            );

            $q = $this->M_request->simpan_tolakRequest($column, $data);

            $detail = $this->M_request->getDetailRequest($_POST["id"], "11", $_POST["plant"]);
            $type = 'Request';
            $kepada = 'Sales';
            $cekTo = $this->M_request->cekTo('30',$_POST["plant"]);
            $cekSend = $this->M_request->cekTo('31',$_POST["plant"]);
            $cek = $cekTo['EMAIL'];
            $send = $cekSend['FULLNAME'];
            $kepada = $cekTo['FULLNAME'];
            /*$status1 = '0';*/
            /*$status2 = '21';*/
            $array = array();

            $email = $cek;
            $cc = '';
            $bcc = '';
            

            $param['short'] = 'ERF';
            $param['link'] = 'erf';
            $param['title'] = $type;
            $param['sender'] = "{ ADMIN COQ } ";
            $param['code'] = '123123';
            $param['ID'] = $_POST["id"];
            $param['KEPADA'] = $kepada;
            $param['APP']['BADGE'] = '';
            $param['APP']['NAMA'] = '';
            $param['APP']['UK_TEXT'] = '';
            $param['TRN']['NO'] = "<b>ID. {$type} : </b> " . $detail['KODE'];
            $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
            $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
            $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
            $param['TRN']['NOTE'] = "<b>Keterangan :</b> " . $_POST["syarat"];

            $param['hasil'] = 'REJECT BY ADMIN';
            $this->kirim_email2($email, $cc, $bcc, $this->template4->set_app($param), '');

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }


    public function kirimjajal($to, $cc, $bcc,$docisi, $param, $id)
    {
        $email_TO = $to;
        $email_CC = $cc;
        $eol = PHP_EOL;
        $separator = md5(uniqid(time()));

//        $eol = "\r\n";
        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
        $headers .= "This is a multi-part message in MIME format.\r\n";
        $message = "--" . $separator . $eol;
        $message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
        $message .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $message .= $docisi . $eol;

        $message .= "--" . $separator . $eol;
        $message .= "Content-Type: application/octet-stream;" . $eol;
        $message .= "Content-Transfer-Encoding: base64" . $eol;
        $message .= "Content-Disposition: attachment" . $eol . $eol;
        $message .= $eol;
        $message .= "--" . $separator . "--";

        mail($email_TO, $subject, $message, $headers);
    }

    public function hapus()
    {
        if (isset($_POST["id"])) {
            $q = $this->M_request->hapus($_POST["id"]);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }


}
