
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->helper(array('url','file'));
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_template');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Template Spec dan Quality";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->dropzone_plugin();

		$this->layout->render('coq/template', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_template->get_data();
		echo json_encode($detail_data);

    }
	 public function get_listParameter()
    { 
        /*$id_standart = $_POST['id_standart'];
        $id_produk = $_POST['id_produk'];*/
        $detail_data = $this->M_template->get_listParameter(/*$id_standart, $id_produk*/);
		echo json_encode($detail_data);

    }
    
	 public function get_template()
    { 
        $id_template = $this->input->post("id_template");
        $detail_data = $this->M_template->cek_template($id_template);
        echo json_encode($detail_data);
    }
    
    
     
	 public function detail_request()
    {  
            $id_request = $_POST['id_request'];
            $plant = $this->M_template->getPlantID($id_request);
            $file = $this->M_template->getFileID($id_request);
            
            $v_plant = '';
            foreach($plant as $p){
                $v_plant .= $p['NM_PLANT'].', ';
            }
            $v_plant = rtrim($v_plant, ", ");
            
            $v_file = '<div>'; 
            foreach($file as $v){
                $ext = strtolower(pathinfo($v['NM_FILE'], PATHINFO_EXTENSION));
                if($ext == 'xlsx' || $ext == 'xls' || $ext == 'xls' || $ext == 'csv'){
                    $icon = ' fa-file-excel-o';
                }else if($ext == 'doc' || $ext == 'docx' || $ext == 'xls'){ 
                    $icon = ' fa-file-word-o';
                }else if($ext == 'pdf'){
                    $icon = ' fa-file-pdf-o'; 
                }else if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
                    $icon = ' fa-file-image-o'; 
                }else{
                     $icon = ' fa-info';  
                }
                $base =  base_url()."assets/upload/{$v['NM_FILE']}";
                $v_file .= "<a href='{$base}'><span class='btn-labelx'><i class='fa {$icon} fa-lg'></i>  {$v['NM_FILE']} </span></a> <br /> <br />";
            }
            $v_file .= '</div>';
             
                echo json_encode(array('notif' => '1', 'plant' => $v_plant, 'file' => $v_file)); 
    

    }
    
    
	 public function refreshChat()
    { 
     
        $data = $this->M_template->getChatID($_POST['id_request']);
        $id_user = $this->session->userdata("USER")->ID_USER; 
        $chat = '';
        $gambar = base_url().'assets/img/landing/avatar1.jpg';
        foreach($data as $v){
            $posisi = ($id_user==$v['ID_USER'] ? 'right' : 'left');
            $tanggal = date('D, d M Y', strtotime($v['TANGGAL']));
            $chat .="<div class='chat-message {$posisi}'>
                        <img class='message-avatar' src='{$gambar}' alt='' >
                        <div class='message'>
                            <a class='message-author' href='#'> {$v['FULLNAME']} </a>
                            <span class='message-date'> {$tanggal} </span>
                            <span class='message-content'>
                                                        {$v['CHAT']}
                            </span>
                        </div>
                    </div>"; 
        } 

        echo json_encode(array('notif' => '1', 'chat' => $chat)); 
    }
     
    
	public function refreshRequest() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Request --</option>";
		$data = $this->M_template->getRequest();
		foreach($data as $p) {
			echo "<option value='{$p["ID_REQUEST"]}'>".htmlspecialchars($p["NM_REQUEST"])."</option>";
		}
	} 
	public function refreshStandart() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
		$data = $this->M_template->getStandart();
		foreach($data as $p) {
			echo "<option value='{$p["ID_STANDART"]}'>".htmlspecialchars($p["NM_STANDART"])."</option>";
		}
	} 
    
	public function refreshTipe() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Tipe Produk --</option>";
		$data = $this->M_template->getTipe();
		foreach($data as $p) {
			echo "<option value='{$p["ID_PRODUCT_TYPE"]}'>".htmlspecialchars($p["NM_PRODUCT_TYPE"])." (<b>".htmlspecialchars($p["KD_PRODUCT_TYPE"])."</b>) </option>";
		}
	} 
    
	public function refreshProduk() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Produk --</option>";
		$data = $this->M_template->getProduk();
		foreach($data as $p) {
			echo "<option value='{$p["ID_PRODUCT"]}'>".htmlspecialchars($p["NM_PRODUCT"])."</option>";
		}
	} 
    
	public function refreshPlant() {
		echo "<option value='' data-hidden='true' >-- Pilih Plant --</option>";
		$data = $this->M_template->getPlant();
		foreach($data as $p) {
			echo "<option value='{$p["ID_PLANT"]}'>".htmlspecialchars($p["NM_PLANT"])."</option>";
		}
	} 
    
	public function refreshCountry() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Negara --</option>";
		$data = $this->M_template->getCountry();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COUNTRY"]}'>".htmlspecialchars($p["NM_COUNTRY"])."</option>";
		}
	} 
    
     public function fileUpload(){

           $config['upload_path']   = FCPATH.'/assets/upload/';
            $config['allowed_types'] = 'gif|jpg|png|ico|pdf|xlsx|xls|doc|docx';
            $this->load->library('upload',$config);

            if($this->upload->do_upload('userfile')){
                $token=$this->input->post('token_foto');
                $nama=$this->upload->data('file_name');
                $this->db->insert('COQ_FILE',array('NM_FILE'=>$nama,'TOKEN'=>$token));
            }
     }
    
	//Untuk menghapus foto
	function remove_foto(){

		//Ambil token foto
		$token=$this->input->post('token'); 
		$foto=$this->db->get_where('COQ_FILE',array('TOKEN'=>$token)); 
		if($foto->num_rows()>0){
			$hasil=$foto->row();
			$nama_foto=$hasil->NM_FILE;
			if(file_exists($file= FCPATH.'/assets/upload/'.$nama_foto)){
				unlink($file);
			}
			$this->db->delete('COQ_FILE',array('TOKEN'=>$token));

		} 
	}
    
    
    public function simpanMessage()
    {
        if (isset($_POST["message"])) {
            $column = array(
                'ID_USER', 'CHAT', 'TANGGAL', 'ID_REQUEST'
            );
            
            $id_user = $this->session->userdata("USER")->ID_USER;
            $date = date('d-m-Y H:i:s');

            $data = array(
                "'" . $id_user . "'", "'" . $_POST["message"] . "'", "sysdate", "'" . $_POST["id_request"] . "'"
            );
            $q = $this->M_template->simpanMessage($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

	public function simpan() {  
    
            $header = $this->input->post("header");
            $isi = $this->input->post("isi_data");
		if(isset($header["nm_template"]) ){
             
            $id_user = $this->session->userdata("USER")->ID_USER;
			$column = array(
				'NM_TEMPLATE' , 'ID_STANDART', 'ID_PRODUCT_TYPE' , 'ID_PRODUCT' , 'ID_REQUEST', 'ID_USER',  'TANGGAL', 'TIPE_REQUEST' 
			);  
			$data = array(
				"'".$header["nm_template"]."'", "'".$header["standart"]."'", "'".$header["tipe"]."'", "'".$header["produk"]."'", "'".$header["request"]."'", "'".$id_user."'", "SYSDATE" , "'".$header["tipe_request"]."'"
			);
			 
			$q = $this->M_template->simpan($column, $data); 
            $ambilid = $this->M_template->ambilId('COQ_TEMPLATE_HEADER')->result();
             $ide = ($ambilid[0]->ID_TEMPLATE_HEADER);
             $i = 0;
            foreach($isi as $key=>$val)
            {  
                  $datas[$i]['ID_TEMPLATE_HEADER'] = $ide ; 
                  $datas[$i]['ID_UJI'] = $val['parameter'];
                  $datas[$i]['ID_STANDART_UJI'] = $val['standart'];  
                  $datas[$i]['NILAI_TEMPLATE'] = $val['nilai']; 
                  $datas[$i]['NILAI_RESULT'] = $val['result']; 
                  $i++;
            }  
			 $q = $this->M_template->addTemplate('COQ_TEMPLATE_DETAIL', $datas);
            
             
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
        
            $header = $this->input->post("header");
            $isi = $this->input->post("isi_data");
		if(isset($header["nm_template"]) 
		) {
			$id = intval($header["id"]);
			$column = array(
				'NM_TEMPLATE' , 'ID_STANDART', 'ID_PRODUCT_TYPE' , 'ID_PRODUCT' , 'ID_REQUEST',  'TIPE_REQUEST'
			);  
			$data = array(
				"'".$header["nm_template"]."'", "'".$header["standart"]."'", "'".$header["tipe"]."'", "'".$header["produk"]."'", "'".$header["request"]."'" , "'".$header["tipe_request"]."'" 
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_template->update($id, $data_baru);
            
            //// TEMPLATE
            $q = $this->M_template->deleteTemplate($id);
            $i = 0;
            foreach($isi as $key=>$val)
            {  
                  $datas[$i]['ID_TEMPLATE_HEADER'] = $id ; 
                  $datas[$i]['ID_UJI'] = $val['parameter'];
                  $datas[$i]['ID_STANDART_UJI'] = $val['standart'];  
                  $datas[$i]['NILAI_TEMPLATE'] = $val['nilai']; 
                  $datas[$i]['NILAI_RESULT'] = $val['result']; 
                  $i++;
            }  
			 $q = $this->M_template->addTemplate('COQ_TEMPLATE_DETAIL', $datas);
            
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_template->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
