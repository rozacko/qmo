
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_landing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_landing');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		/*$data['product'] = $this->M_landing->product();
		$data['standart'] = $this->M_landing->standart();*/
		$data['title'] = "Master Standart Product";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/standart_produk', $data);
	}
     
	 
    /*public function get_data()
    {
        $list = $this->M_landing->ambil_data();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $arr) {
            $no++;
            $row = array();
            $row[] = $no; 
            
            $row[] = $arr['KD_PRODUCT_TYPE'];
            $row[] = $arr['NAMA_STANDART'];
            $row[] = '<center>
            			<button class="btn btn-primary btn-xs waves-effect" onclick="edit(\''.$arr['ID_STD'].'\')" title="Edit"><span class="fa fa-pencil"></span> </button>
                        <button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\''.$arr['ID_STD'].'\')" title="Delete"><span class="fa fa-trash-o"></span> </button>
                      </center>';
            $data[] = $row;
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->M_std_product->count_all(),
			"recordsFiltered" => $this->M_std_product->count_filtered(),
			"data" => $data,
        );
        echo json_encode($output);
    }
*/
	/*public function refreshKelompok() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kelompok --</option>";
		$data = $this->M_standart->getKelompok();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KELOMPOK"]}'>".htmlspecialchars($p["NAMA_KELOMPOK"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["nm_standart"]) ){
			$column = array(
				'NM_STANDART' 
			);
			
			$data = array(
				"'".$_POST["nm_standart"]."'" 
			);
			 
			$q = $this->M_standart->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_standart"]) 
		) {
			$id = intval($_POST["id"]);
			$column = array(
				'NM_STANDART' 
			);
			
			$data = array(
				"'".$_POST["nm_standart"]."'" 
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_standart->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_standart->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    */
    
}
