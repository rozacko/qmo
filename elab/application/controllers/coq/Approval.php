<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('coq/M_approval');
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Approval";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('coq/approval', $data);
    }

    public function get_data()
    {

        $sess_user = $this->session->userdata("USERLAB");
        $session_role = '';
        $session_plant = '';
        foreach ($sess_user as $v) {
            $session_plant .= $v['ID_PLANT'] . ",";
            $session_role .= $v['ID_ROLE'] . ",";
        }
        $plant = rtrim($session_plant, ",");
        $role = rtrim($session_role, ","); // 32 Manager 34 senior manajer
        $status = "'1', '11'";
        if ($role == '32') {
            $status = " AND STATUS IN('2', '21', '3', '31')";
        } else if ($role == '34') {
            $status = " AND STATUS IN('3','31','4','41')";
        }
        $data_spec = $this->M_approval->get_data_spec($plant);
        $getdata_spec = array();
        foreach ($data_spec as $val) {
            $total = $this->M_approval->getApprove($val['ID'], '0')->result();
            $status = (count($total) > 0 ? $total[0]->STATUS : '');
            if ($status != '4' && $status != '41' && $status == '3' && $role == '34') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_spec[] = $data;
            }
            if ($status != '3' && $status != '31' && $status == '2' && $role == '35') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_spec[] = $data;
            }
            if ($status != '2' && $status != '21' && $status == '1' && $role == '32') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_spec[] = $data;
            }

        }
        $data_quality = $this->M_approval->get_data_quality($plant);
        $getdata_quality = array();
        foreach ($data_quality as $val) {
            $total = $this->M_approval->getApprove($val['ID'], '1')->result();
            $status = (count($total) > 0 ? $total[0]->STATUS : '');
            if ($status != '4' && $status == '3' && $role == '34') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_quality[] = $data;
            }
            if ($status != '3' && $status != '31' && $status == '2' && $role == '35') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_quality[] = $data;
            }
            if ($status != '2' && $status != '21' && $status == '1' && $role == '32') {
                $data['ID'] = $val['ID'];
                $data['NAMA'] = $val['NAMA'];
                $data['NM_STANDART'] = $val['NM_STANDART'];
                $data['NM_PRODUCT_TYPE'] = $val['NM_PRODUCT_TYPE'];
                $data['TIPE'] = $val['TIPE'];
                $data['KODE'] = $val['KODE'];
                $data['ID_REQUEST'] = $val['ID_REQUEST'];
                $data['ID_PLANT'] = $val['ID_PLANT'];
                $data['NM_PLANT'] = $val['NM_PLANT'];
                $data['NM_COMPANY'] = $val['NM_COMPANY'];
                $data['ROLE'] = $role;
                $data['STATUS'] = $status;
                $getdata_quality[] = $data;
            }
        }
        $getdata = array_merge($getdata_spec, $getdata_quality);
        echo json_encode($getdata);


    }
    public function informasi($id){
        $sql = $this->M_approval->get_review($id);
        echo json_encode($sql);
    }

    public function refreshStandart()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
        $data = $this->M_approval->getStandart();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_STANDART"]}'>" . htmlspecialchars($p["NM_STANDART"]) . "</option>";
        }
    }


    public function simpan()
    {
        if (isset($_POST["nM_approval"])) {
            $column = array(
                'NM_approval', 'ID_STANDART'
            );

            $data = array(
                "'" . $_POST["nM_approval"] . "'", "'" . $_POST["id_standart"] . "'"
            );

            $q = $this->M_approval->simpan($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function update()
    {

        $this->load->library('Template2');
        $this->load->library('Template3');
        if (isset($_POST["id"])
        ) {
            $id = intval($_POST["id"]);
            $id_user = $this->session->userdata("USER")->ID_USER;

            $sess_user = $this->session->userdata("USERLAB");
            $session_role = '';
            foreach ($sess_user as $v) {
                $session_role .= $v['ID_ROLE'] . ",";
            }
            $role = rtrim($session_role, ","); // 32 Manager 34 senior manajer  38 Staff
            if ($_POST["tipe"] == '1') {
                if ($role == '32') {
                    $status = ($role == '32' ? "2" : "3");
                } elseif ($role == '35') {
                    $status = ($role == '35' ? "3" : "4");
                } elseif ($role == '34') {
                    $status = ($role == '34' ? "4" : "5");
                }


            } else if ($_POST["tipe"] == '2') {
                if ($role == '32') {
                    $status = ($role == '32' ? "21" : "31");
                } elseif ($role == '35') {
                    $status = ($role == '35' ? "31" : "41");
                } elseif ($role == '34') {
                    $status = ($role == '34' ? "41" : "51");
                }
            }


            $column = array(
                'ID_JOIN', 'KETERANGAN', 'TIPE', 'ID_USER', 'STATUS', 'TANGGAL', 'ID_REQUEST', 'ID_PLANT'
            );

            $data = array(
                "'" . $_POST["id"] . "'", "'" . $_POST["syarat"] . "'", "'" . $_POST["id_tipe"] . "'", "'" . $id_user . "'", "'" . $status . "'", "SYSDATE", "'" . $_POST["id_request"] . "'", "'" . $_POST["id_plant"] . "'"
            );


            $q = $this->M_approval->simpan_approve($column, $data);

            $tipe = ($_POST["id_tipe"] == '0' ? 'Specification' : 'Certificated');
            $tabel = ($_POST["id_tipe"] == '0' ? 'COQ_SPEC_HEADER' : 'COQ_SPEC_QUALITY');
            $where = ($_POST["id_tipe"] == '0' ? 'ID_SPEC' : 'ID_QUALITY');
            $kode = ($_POST["id_tipe"] == '0' ? 'KD_SPEC' : 'KD_QUALITY');
            $detail = ($_POST["id_tipe"] == '0' ? $this->M_approval->getDetailSpec($_POST["id"]) : $this->M_approval->getDetailQuality($_POST["id"]));

            /*$email = 'department.qa@semenindonesia.com';
            $cc = '';
            $bcc = '';*/

            $param['short'] = 'ERF';
            $param['link'] = 'erf';
            $param['title'] = $tipe;
            $param['id_tipe'] = $_POST["id_tipe"];
            /*$param['sender'] = "{ ADMIN COQ } ";*/
            $param['code'] = '123123';
            $param['ID'] = $_POST["id"];
            $param['KIMIA'] = ($_POST["id_tipe"] == '0' ? $this->M_approval->cek_spec($_POST["id"], '1') : $this->M_approval->cek_quality($_POST["id"], '1'));
            $param['FISIKA'] = ($_POST["id_tipe"] == '0' ? $this->M_approval->cek_spec($_POST["id"], '2') : $this->M_approval->cek_quality($_POST["id"], '2'));
            $param['APP']['BADGE'] = '';
            $param['APP']['NAMA'] = '';
            $param['APP']['UK_TEXT'] = '';
            $param['TRN']['NO'] = "<b>ID. {$tipe} : </b> " . $detail['KODE'];
            $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
            $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
            $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
            $param['TRN']['NOTE'] = "<b>Keterangan :</b> " . $_POST["syarat"];

            if ($status == '2') {
                $cekTo = $this->M_approval->cekTo('35',$_POST["id_plant"]);
                $cekSend = $this->M_approval->cekTo('32',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                /*$kepada = 'Staff Holding';*/
                $status1 = ($role == '32' ? '3' : '2');
                $status2 = ($role == '32' ? '31' : '21');
                $param['STATUS1'] = $status1;
                $param['STATUS2'] = $status2;
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $this->kirimjajal_1($cek, $cc, $bcc, $this->template2->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);

            } else if ($status == '3') {
                $cekTo = $this->M_approval->cekTo('34',$_POST["id_plant"]);
                $cekSend = $this->M_approval->cekTo('35',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                /*$param['KEPADA'] = "Admin";
                $param['hasil'] = 'APPROVE BY SENIOR MANAGER';*/
                /*$kepada = 'Senior Manager Unit Quality Assurance';*/
                //$param['hasil'] = 'APPROVE BY SENIOR MANAGER';
                $status3 = ($role == '35' ? '4' : '3');
                $status4 = ($role == '35' ? '41' : '31');
                $param['STATUS1'] = $status3;
                $param['STATUS2'] = $status4;
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $this->kirimjajal_1($cek, $cc, $bcc, $this->template2->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);
            } elseif ($status == '4') {
                $cekTo = $this->M_approval->cekTo('30',$_POST["id_plant"]);
                $cekToCc = $this->M_approval->cekTo2('31','90'); //role dan id_user
                $cekSend = $this->M_approval->cekTo('34',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $cc = $cekToCc['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $param['hasil'] = 'Telah Di Approve';
                $this->kirimjajal_1($cek, $cc, $bcc, $this->template3->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);
            } else if ($status == '21') {
                $cekTo = $this->M_approval->cekTo('30',$_POST["id_plant"]);
                $cekSend = $this->M_approval->cekTo('32',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $param['hasil'] = 'Telah Di Reject';
                $this->kirimjajal_1($cek, $cc, $bcc, $this->template3->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);
            } else if ($status == '31') {
                $cekTo = $this->M_approval->cekTo('30',$_POST["id_plant"]);
                $cekSend = $this->M_approval->cekTo('35',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $param['hasil'] = 'Telah Di Reject';
               $this->kirimjajal_1($cek, $cc, $bcc, $this->template3->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);
            } else if ($status == '41') {
                $cekTo = $this->M_approval->cekTo('30',$_POST["id_plant"]);
                $cekSend = $this->M_approval->cekTo('34',$_POST["id_plant"]);
                $cek = $cekTo['EMAIL'];
                $send = $cekSend['FULLNAME'];
                $kepada = $cekTo['FULLNAME'];
                $param['KEPADA'] = $kepada;
                $param['sender'] = $send;
                $param['hasil'] = 'Telah Di Reject';
               $this->kirimjajal_1($cek, $cc, $bcc, $this->template3->set_app($param), '',$_POST["id"],$_POST["id_tipe"]);
            }
            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }


    public function kirim_email2($to, $cc, $bcc, $docisi, $data)
    {
        $email_TO = $to;
        $email_CC = $cc;
        $eol = "\r\n";
        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        mail($email_TO, $subject, $docisi, $headers);
    }
     public function kirimjajal_1($to, $cc, $bcc, $docisi, $data, $id,$tipe)
    {

        /*=====================================================*/
        /*  UNTUK MENCARI DATA SPEC FOR TEMPLATE  */
        /*  GET DATA ALL SPECIFICATION by single ID  */
        /*======================================================*/
        if ($tipe == '0') {
            # code...
        
        $ambilS = $this->M_approval->ambilStand('COQ_QUALITY_HEADER', $id)->row_array();  
        $idStand = $ambilS['ID_STANDART'];
        $datas = [
            'detail' => $this->M_approval->get_spec($id),
        ];
        
        $par = $this->M_approval->cek_spec_template($id);
        $detail_data = $this->M_approval->get_spec2($id,$idStand);

        //$detail = $this->M_display->get_data($id);
        //echo json_encode($par);exit;
        // echo json_encode(count($datas['elab_t_bppuc']));exit;


        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6

        // $this->load->view('login');
        if ($datas[0]['ID_COMPANY'] == '8') {
            $mpdf->SetWatermarkText('PT SEMEN PADANG');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '9') {
            $mpdf->SetWatermarkText('PT SEMEN TONASA');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '10') {
            $mpdf->SetWatermarkText('PT SEMEN INDONESIA');
            $mpdf->showWatermarkText = true;
        }


        $html = $this->load->view('printpdf/create_template_email', ['data' => $datas, 'list_par' => $par, 'detail' => $detail_data], true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $fileatt = $mpdf->Output('SPEC.pdf', \Mpdf\Output\Destination::STRING_RETURN);
        $attachment = chunk_split(base64_encode($fileatt));

        $email_TO = $to;
        $email_CC = $cc;
        $fileName = 'FILE_SPEC.pdf';
        $eol = PHP_EOL;
        $separator = md5(uniqid(time()));

//        $eol = "\r\n";
        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
        $headers .= "This is a multi-part message in MIME format.\r\n";
        $message = "--" . $separator . $eol;
        $message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
        $message .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $message .= $docisi . $eol;

        $message .= "--" . $separator . $eol;
        $message .= "Content-Type: application/octet-stream; name=\"" . $fileName . "\"" . $eol;
        $message .= "Content-Transfer-Encoding: base64" . $eol;
        $message .= "Content-Disposition: attachment" . $eol . $eol;
        $message .= $attachment . $eol;
        $message .= "--" . $separator . "--";

        mail($email_TO, $subject, $message, $headers);

        }elseif ($tipe == '1') {
             $ambilS = $this->M_approval->ambilStand('COQ_QUALITY_HEADER', $id)->row_array();  
        $idStand = $ambilS['ID_STANDART'];
        $datas = [
            'detail' => $this->M_approval->get_quality($id),
        ];
        
        $par = $this->M_approval->cek_quality_template($id);
        $detail_data = $this->M_approval->get_quality2($id,$idStand);

        //$detail = $this->M_display->get_data($id);
        //echo json_encode($par);exit;
        // echo json_encode(count($datas['elab_t_bppuc']));exit;


        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6

        // $this->load->view('login');
        if ($datas[0]['ID_COMPANY'] == '8') {
            $mpdf->SetWatermarkText('PT SEMEN PADANG');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '9') {
            $mpdf->SetWatermarkText('PT SEMEN TONASA');
            $mpdf->showWatermarkText = true;
        } elseif ($datas[0]['ID_COMPANY'] == '10') {
            $mpdf->SetWatermarkText('PT SEMEN INDONESIA');
            $mpdf->showWatermarkText = true;
        }


        $html = $this->load->view('printpdf/create_template_email', ['data' => $datas, 'list_par' => $par, 'detail' => $detail_data], true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $fileatt = $mpdf->Output('COQ.pdf', \Mpdf\Output\Destination::STRING_RETURN);
        $attachment = chunk_split(base64_encode($fileatt));

        $email_TO = $to;
        $email_CC = $cc;
        $fileName = 'FILE_COQ.pdf';
        $eol = PHP_EOL;
        $separator = md5(uniqid(time()));

//        $eol = "\r\n";
        $repEmail = 'no-reply.elab@semenindonesia.com';
        $subject = "E-Laboratorary - Approval COQ";
        $headers = 'From: E-COQ <' . $repEmail . '>' . $eol;
        $headers .= "CC: " . $email_CC . "\r\n";
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";
        $headers .= "This is a multi-part message in MIME format.\r\n";
        $message = "--" . $separator . $eol;
        $message .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
        $message .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
        $message .= $docisi . $eol;

        $message .= "--" . $separator . $eol;
        $message .= "Content-Type: application/octet-stream; name=\"" . $fileName . "\"" . $eol;
        $message .= "Content-Transfer-Encoding: base64" . $eol;
        $message .= "Content-Disposition: attachment" . $eol . $eol;
        $message .= $attachment . $eol;
        $message .= "--" . $separator . "--";

        mail($email_TO, $subject, $message, $headers);
        }
    }

    public function prosesApproval($id, $tipe, $statusLINK)
    {
        $this->load->library('Template2');
        $this->load->library('Template3');
        $id_user = $this->session->userdata("USER")->ID_USER;
        $sess_user = $this->session->userdata("USERLAB");
        $session_role = '';
        foreach ($sess_user as $v) {
            $session_role .= $v['ID_ROLE'] . ",";
        }
        $role = rtrim($session_role, ","); // 32 Manager 34 senior manajer 

        $total = $this->M_approval->getApprove($id, $tipe)->result();
        $status = (count($total) > 0 ? $total[0]->STATUS : '');
        ///untuk cek approve

        $statusUser1 = ($role == '32' ? '2' : '3');
        $statusUser2 = ($role == '32' ? '21' : '31');


        //untuk status email selanjutnya
        if ($role == '32') {
            $statusEmail1 = ($role == '32' ? '3' : '2');
            $statusEmail2 = ($role == '32' ? '31' : '21');
        } elseif ($role == '35') {

            $statusEmail3 = ($role == '35' ? '4' : '3');
            $statusEmail4 = ($role == '35' ? '41' : '31');
        }


        if ($role != '') {
            if ($statusUser1 == $status || $statusUser2 == $status) {
                echo '<script language="javascript">' .
                    'alert("Approval sudah diproses, Terimakasih");';
                $URL = base_url() . "coq";
                echo "setTimeout(function(){ window.location.href = '$URL'; }, 300);";
                echo '</script>';
            } else {

                $detail = ($tipe == '0' ? $this->M_approval->getDetailSpec($id) : $this->M_approval->getDetailQuality($id));


                $column = array(
                    'ID_JOIN', 'TIPE', 'ID_USER', 'STATUS', 'TANGGAL', 'ID_REQUEST', 'ID_PLANT'
                );
                $data = array(
                    "'" . $id . "'", "'" . $tipe . "'", "'" . $id_user . "'", "'" . $statusLINK . "'", "SYSDATE", "'" . $detail['ID_REQUEST'] . "'", "'" . $detail['ID_PLANT'] . "'"
                );
                $data = $this->M_approval->simpan_approve($column, $data);
                if ($data == true) {
                    // SEND EMAIL LANJUTAN

                    $type = ($tipe == '0' ? 'Specification' : 'Quality');
                    if ($role == '32') {
                        $kepada = ($role == '32' ? 'Manager Holding' : 'Manager Unit Quality Assurance');
                    } elseif ($role == '35') {
                        $kepada = ($role == '35' ? 'Senior Manager Unit Quality Assurance' : 'Manager Holding');
                    }


//                    $email = 'department.qa@semenindonesia.com';
                    $email = 'mr.robiyanto@gmail.com';
                    $cc = 'robiyanto@sisi.id';
                    $bcc = '';

                    $param['short'] = 'ERF';
                    $param['link'] = 'erf';
                    $param['title'] = $type;
                    $param['id_tipe'] = $tipe;
                    $param['sender'] = "{ ADMIN COQ } ";
                    $param['code'] = '123123';
                    $param['ID'] = $id;
                    if ($role == '32') {
                        $param['STATUS1'] = $statusEmail1;
                        $param['STATUS2'] = $statusEmail2;
                    } elseif ($role == '35') {
                        $param['STATUS1'] = $statusEmail3;
                        $param['STATUS2'] = $statusEmail4;
                    }

                    /*$param['STATUS3'] = $statusEmail3;
                    $param['STATUS4'] = $statusEmail4;*/
                    $param['KEPADA'] = $kepada;
                    $param['KIMIA'] = ($tipe == '0' ? $this->M_approval->cek_spec($id, '1') : $this->M_approval->cek_quality($id, '1'));
                    $param['FISIKA'] = ($tipe == '0' ? $this->M_approval->cek_spec($id, '2') : $this->M_approval->cek_quality($id, '2'));
                    $param['APP']['BADGE'] = '';
                    $param['APP']['NAMA'] = '';
                    $param['APP']['UK_TEXT'] = '';
                    $param['TRN']['NO'] = "<b>ID. {$type} : </b> " . $detail['KODE'];
                    $param['TRN']['NOTIFIKASI'] = "<b>Standart :</b> " . $detail['NM_STANDART'];
                    $param['TRN']['DESKRIPSI'] = "<b>Tipe Produk :</b> " . $detail['NM_PRODUCT_TYPE'];
                    $param['TRN']['PERIODE_AWAL'] = "<b>Plant :</b> " . $detail['NM_PLANT'];
                    $param['TRN']['NOTE'] = "<b>Keterangan :</b> ";

                    /* ===========================================*/
                    /* ***    DIMATIKAN*/
                    /* ***    Fungsi untuk mengirim email pemberitahuan tanpa attachment*/
                    /*if ($statusLINK == '2') {
                        $this->kirim_email2($email, $cc, $bcc, $this->template2->set_app($param), '');
                    } else if ($statusLINK == '21') {
                        $param['hasil'] = 'REJECT BY MANAGER';
                        $this->kirim_email2($email, $cc, $bcc, $this->template3->set_app($param), '');
                    } else if ($statusLINK == '3') {
                        $param['hasil'] = 'APPROVE BY Manager Holding';
                        $this->kirim_email2($email, $cc, $bcc, $this->template2->set_app($param), '');
                    } else if ($statusLINK == '31') {
                        $param['hasil'] = 'REJECT BY Manager Holding';
                        $this->kirim_email2($email, $cc, $bcc, $this->template3->set_app($param), '');
                    } else if ($statusLINK == '4') {
                        $param['hasil'] = 'APPROVE BY SENIOR MANAGER';
                        $this->kirim_email2($email, $cc, $bcc, $this->template2->set_app($param), '');
                    } else if ($statusLINK == '41') {
                        $param['hasil'] = 'REJECT BY SENIOR MANAGER';
                        $this->kirim_email2($email, $cc, $bcc, $this->template3->set_app($param), '');
                    }*/

                    /* ===========================================*/
                    /* ***    Fungsi untuk mengirim email pemberitahuan dengnan attachment*/
                    /* ===========================================*/

                    if ($statusLINK == '2') {
                        $this->kirimjajal($email, $cc, $bcc, $this->template2->set_app($param), '', $id);
                    } else if ($statusLINK == '21') {
                        $param['hasil'] = 'REJECT BY MANAGER';
                        $this->kirimjajal($email, $cc, $bcc, $this->template3->set_app($param), '', $id);
                    } else if ($statusLINK == '3') {
                        $param['hasil'] = 'APPROVE BY Manager Holding';
                        $this->kirimjajal($email, $cc, $bcc, $this->template2->set_app($param), '', $id);
                    } else if ($statusLINK == '31') {
                        $param['hasil'] = 'REJECT BY Manager Holding';
                        $this->kirimjajal($email, $cc, $bcc, $this->template3->set_app($param), '', $id);
                    } else if ($statusLINK == '4') {
                        $param['hasil'] = 'APPROVE BY SENIOR MANAGER';
                        $this->kirimjajal($email, $cc, $bcc, $this->template2->set_app($param), '', $id);
                    } else if ($statusLINK == '41') {
                        $param['hasil'] = 'REJECT BY SENIOR MANAGER';
                        $this->kirimjajal($email, $cc, $bcc, $this->template3->set_app($param), '', $id);
                    }

                    echo '<script language="javascript">' .
                        'alert("Terimakasih telah melakukan approval");';
                    $URL = base_url() . "coq";
                    echo "setTimeout(function(){ window.location.href = '$URL'; }, 300);";
                    echo '</script>';
                } else {
                    echo '<script language="javascript">' .
                        'alert("Gagal memproses approval");';
                    $URL = base_url() . "coq";
                    echo "setTimeout(function(){ window.location.href = '$URL'; }, 300);";
                    echo '</script>';
                }
            }
        } else {
            echo '<script language="javascript">' .
                'alert("Anda belum login!!!");';
            $URL = base_url() . "coq";
            echo "setTimeout(function(){ window.location.href = '$URL'; }, 300);";
            echo '</script>';
        }
    }

    public function hapus()
    {
        if (isset($_POST["id"])) {
            $q = $this->M_approval->hapus($_POST["id"]);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function get_detail()
    {
        $id = $_POST['id'];
        $detail_spec = $this->M_approval->get_spec($id);
        $par_spec = $this->M_approval->cek_spec_template($id);
        $detail_quality = $this->M_approval->get_quality($id);
        $par_quality = $this->M_approval->cek_quality_template($id);
        $parameter = '';
        $parameter_quality = '';
        $fisika = '';
        $fisika_quality = '';

        $no = 0;
        //echo json_encode($par_spec);exit();
        if ($id == $par_spec[0]['ID_SPEC_HEADER']) {
            error_reporting(0);
            foreach ($par_spec as $p) {
                if (empty($p['NILAI_STD'])) {
                    $p['NILAI_STD'] = '-';
                    $p['MARK'] = '';
                }
                if (empty($p['NILAI_SPEC'])) {
                    $p['NILAI_SPEC'] = '-';
                    $p['MARK'] = '';
                }
                if ($p['ID_KATEGORI'] == '1') {
                    if (count($p['NAMA_UJI']) == '0') {
                        $parameter .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no++;
                        $parameter .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                    }
                } elseif ($p['ID_KATEGORI'] == '2') {
                    error_reporting(0);
                    if (count($p['NAMA_UJI']) == '0') {
                        $fisika .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no++;
                        if (count($p['NAMA_PARENT']) == '0') {
                            $fisika .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center> {$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                        } else {
                            $fisika .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_PARENT']}<br> {$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                        }
                    }
                }
            }
        } elseif ($id == $par_quality[0]['ID_QUALITY_HEADER']) {
            foreach ($par_quality as $p) {
                if (empty($p['NILAI_STD'])) {
                    $p['NILAI_STD'] = '-';
                    $p['MARK'] = '';
                }
                if (empty($p['NILAI_SPEC'])) {
                    $p['NILAI_SPEC'] = '-';
                    $p['MARK'] = '';
                }
                error_reporting(0);
                if ($p['ID_KATEGORI'] == '1') {
                    if (count($p['NAMA_UJI']) == '0') {
                        $parameter_quality .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        $no++;
                        $parameter_quality .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                    }
                } elseif ($p['ID_KATEGORI'] == '2') {
                    error_reporting(0);
                    if (count($p['NAMA_UJI']) == '0') {
                        $fisika_quality .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no++;
                        if (count($p['NAMA_PARENT']) == '0') {
                            $fisika_quality .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center> {$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                        } else {
                            error_reporting(0);
                            $fisika_quality .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'><center>{$p['NAMA_PARENT']}<br> {$p['NAMA_UJI']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                        }
                    }
                }
            }
        }

        if ($id == $detail_spec[0]['ID_SPEC_HEADER']) {
            $detail = "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'>Detail Specification</div>
                                </div>
                                <div class='modal-body' style='overflow-y: scroll; max-height:100%;'>  
                                    
                                         
                                            <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                <tr><td align='center'><center>Specification of {$detail_spec[0]['NM_PRODUCT_TYPE']}</center></td> </tr>
                                                <tr><td align='center'><center>PT {$detail_spec[0]['NM_COMPANY']} ({$detail_spec[0]['NM_PLANT']})</center></td> </tr>
                                                
                                            </table>

                                            <table style='font-size: 12px;'>
                                                <tr>
                                                    <td>Date Produce : {$detail_spec[0]['START_PRODUCE']}</td>
                                                    
                                                  </tr>
                                                  <tr>
                                                    <td>To : {$detail_spec[0]['END_PRODUCE']}</td>
                                                  </tr>
                                            </table>
                                         <table width='100%' border='1' style='border-collapse: collapse;'>
        <thead>
            <tr>
                <th width='4%'  rowspan='3'> </th>
                <th align='center' rowspan='3' colspan='3' width='40%'><center style='font-size: 12px;'><p>QUALITY PARAMETERS</p></center></th>
                
                <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_spec[0]['NM_PRODUCT_TYPE']} {$detail_spec[0]['KD_PRODUCT_TYPE']}</small></center></th>
            </tr>
            <tr>
                
                  
                  <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_spec[0]['NM_STANDART_PRODUCT']}</small></center></th>
                 
            </tr>
            <tr >
                
                <th align='center' width='13%'><center><small style='font-size: 8px;''>TESTING METHOD</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>SPECIFICATION</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>TEST RESULT</small></center></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>I.CHEMICAL COMPOSITION : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$parameter}

            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>II.PHYSICAL PROPERTIES : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$fisika}
           
        </tbody>
    </table>

    <table width='100%'>
        <tr> <td><p style='font-size:10px;'>THE {$detail_spec[0]['NM_PRODUCT']} IS SUITABLE FOR THE PRODUCTION OF CEMENT CONFORMING TO {$detail_spec[0]['NM_STANDART_PRODUCT']}</p></td>  
        <tr> <td ><b style='font-size:10px;'><i>Formulas for mineral specification of the {$detail_spec[0]['NM_PRODUCT']}</i></b></td> 
        <tr> <td><p style='font-size:10px;white-space: pre-wrap;'><i>{$detail_spec[0]['FORMULAS']}</i></p></td>  
                        
                     </tr> 
    </table>
                                        
                                   <hr>     
                                      
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";

            echo $detail;
        } elseif ($id == $detail_quality[0]['ID_QUALITY_HEADER']) {
            $detail = "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'>Detail Quality</div>
                                </div>
                                <div class='modal-body' style='overflow-y: scroll; max-height:100%;'>  
                                    
                                         
                                            <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                <tr><td align='center'><center>Certificate of Quality {$detail_quality[0]['NM_PRODUCT_TYPE']}</center></td> </tr>
                                                <tr><td align='center'><center>PT {$detail_quality[0]['NM_COMPANY']} ({$detail_quality[0]['NM_PLANT']})</center></td> </tr>
                                                
                                            </table>

                                            <table style='font-size: 12px;'>
                                                <tr>
                                                    <td>Date Produce : {$detail_quality[0]['START_PRODUCE']}</td>
                                                    
                                                  </tr>
                                                  <tr>
                                                    <td>To : {$detail_quality[0]['END_PRODUCE']}</td>
                                                  </tr>
                                            </table>
                                         <table width='100%' border='1' style='border-collapse: collapse;'>
        <thead>
            <tr>
                <th width='4%'  rowspan='3'> </th>
                <th align='center' rowspan='3' colspan='3' width='40%'><center style='font-size: 12px;'><p>QUALITY PARAMETERS</p></center></th>
                
                <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_quality[0]['NM_PRODUCT_TYPE']} {$detail_quality[0]['KD_PRODUCT_TYPE']}</small></center></th>
            </tr>
            <tr>
                
                  
                  <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_quality[0]['NM_STANDART_PRODUCT']}</small></center></th>
                 
            </tr>
            <tr >
                
                <th align='center' width='13%'><center><small style='font-size: 8px;''>TESTING METHOD</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>SPECIFICATION</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>TEST RESULT</small></center></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>I.CHEMICAL COMPOSITION : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$parameter_quality}

            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>II.PHYSICAL PROPERTIES : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$fisika_quality}
           
        </tbody>
    </table>

    <table width='100%'>
        <tr> <td><p style='font-size:10px;'>THE {$detail_quality[0]['NM_PRODUCT']} IS SUITABLE FOR THE PRODUCTION OF CEMENT CONFORMING TO {$detail_quality[0]['NM_STANDART_PRODUCT']}</p></td>  
        <tr> <td ><b style='font-size:10px;'><i>Formulas for mineral specification of the {$detail_quality[0]['NM_PRODUCT']}</i></b></td> 
        <tr> <td><p style='font-size:10px;white-space: pre-wrap;'><i>{$detail_quality[0]['FORMULAS']}</i></p></td>  
                        
                     </tr> 
    </table>
                                        
                                   <hr>     
                                      
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";

            echo $detail;
        }
    }


    public function get_detail1()
    {
        $id = $_POST['id'];
        $detail_spec = $this->M_approval->get_spec($id);
        $par_spec = $this->M_approval->cek_spec_template($id);
        $detail_quality = $this->M_approval->get_quality($id);
        $par_quality = $this->M_approval->cek_quality_template($id);
        $parameter = '';
        $parameter_quality = '';
        $fisika = '';
        $fisika_quality = '';

        $no = 0;
        $no2 = 0;
        if ($id == $par_spec[0]['ID_SPEC_HEADER']) {
            error_reporting(0);
            foreach ($par_spec as $p) {
                if (empty($p['NILAI_STD'])) {
                    $p['NILAI_STD'] = '-';
                    $p['MARK'] = '';
                }
                if (empty($p['NILAI_SPEC'])) {
                    $p['NILAI_SPEC'] = '-';
                    $p['MARK'] = '';
                }
                if ($p['ID_KATEGORI'] == '1') {
                    if (count($p['NAMA_UJI']) == '0') {
                        $parameter .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no++;
                        $parameter .= "<tr><td><small style='font-size: 10px;'><center>$no</center></small</td>
                    <td><small style='font-size: 10px;'>&nbsp;{$p['NAMA_UJI']}</small></td>
                    <td><small style='font-size: 10px;'>&nbsp;{$p['SIMBOL']}</small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                    }
                } elseif ($p['ID_KATEGORI'] == '2') {
                    error_reporting(0);
                    if (count($p['NAMA_UJI']) == '0') {
                        $fisika .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no2++;
                        if (count($p['NAMA_PARENT']) == '0') {
                            $fisika .= "<tr><td><small style='font-size: 10px;'><center>$no2</center></small</td>
                    <td colspan='2'><small style='font-size: 10px;'>&nbsp;{$p['NAMA_UJI']}</small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                        } else {
                            $fisika .= "<tr><td><small style='font-size: 10px;'><center>$no2</center></small</td>
                    <td colspan='2'><small style='font-size: 10px;'>&nbsp;{$p['NAMA_PARENT']}<br> {$p['NAMA_UJI']}</small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td></tr>";
                        }
                    }
                }
            }
            $parameter .= "<tr> <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
        } elseif ($id == $par_quality[0]['ID_QUALITY_HEADER']) {
            foreach ($par_quality as $p) {
                if (empty($p['NILAI_STD'])) {
                    $p['NILAI_STD'] = '-';
                    $p['MARK'] = '';
                }
                if (empty($p['NILAI_SPEC'])) {
                    $p['NILAI_SPEC'] = '-';
                    $p['MARK'] = '';
                }
                error_reporting(0);
                if ($p['ID_KATEGORI'] == '1') {
                    if (count($p['NAMA_UJI']) == '0') {
                        $parameter_quality .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        $no2++;
                        $parameter_quality .= "<tr><td><small style='font-size: 10px;'><center>$no2</center></small</td>
                    <td><small style='font-size: 10px;'>&nbsp;{$p['NAMA_UJI']}</small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                    }
                } elseif ($p['ID_KATEGORI'] == '2') {
                    error_reporting(0);
                    if (count($p['NAMA_UJI']) == '0') {
                        $fisika_quality .= "  <tr> <td ></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td> </tr>";
                    } else {
                        error_reporting(0);
                        $no2++;
                        if (count($p['NAMA_PARENT']) == '0') {
                            $fisika_quality .= "<tr><td><small style='font-size: 10px;'><center>$no2</center></small</td>
                    <td><small style='font-size: 10px;'> &nbsp;{$p['NAMA_UJI']}</small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                        } else {
                            error_reporting(0);
                            $fisika_quality .= "<tr><td><small style='font-size: 10px;'><center>$no2</center></small</td>
                    <td><small style='font-size: 10px;'>&nbsp;['NAMA_PARENT']}<br> {$p['NAMA_UJI']} </small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SIMBOL']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['SATUAN']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NM_METODE']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['MARK']} {$p['NILAI_STD']}</center></small></td>
                    <td><small style='font-size: 10px;'><center>{$p['NILAI_SPEC']}</center></small></td></tr>";
                        }
                    }
                }
            }
        }
        if ($id == $detail_spec[0]['ID_SPEC_HEADER']) {
            $detail = "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'>Detail Specification</div>
                                </div>
                                <div class='modal-body' style='overflow-y: scroll; max-height:100%;'>  
                                    
                                         
                                            <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                <tr><td align='center'><center>Specification of {$detail_spec[0]['NM_PRODUCT_TYPE']}</center></td> </tr>
                                                <tr><td align='center'><center>PT {$detail_spec[0]['NM_COMPANY']} ({$detail_spec[0]['NM_PLANT']})</center></td> </tr>
                                                
                                            </table>

                                            <table style='font-size: 12px;'>
                                                <tr>
                                                    <td>Date Produce : {$detail_spec[0]['START_PRODUCE']}</td>
                                                    
                                                  </tr>
                                                  <tr>
                                                    <td>To : {$detail_spec[0]['END_PRODUCE']}</td>
                                                  </tr>
                                            </table>
                                         <table width='100%' border='1' style='border-collapse: collapse;'>
        <thead>
            <tr>
                <th width='4%'  rowspan='3'> </th>
                <th align='center' rowspan='3' colspan='3' width='40%'><center style='font-size: 12px;'><p>QUALITY PARAMETERS</p></center></th>
                
                <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_spec[0]['NM_PRODUCT_TYPE']} {$detail_spec[0]['KD_PRODUCT_TYPE']}</small></center></th>
            </tr>
            <tr>
                
                  
                  <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_spec[0]['NM_STANDART_PRODUCT']}</small></center></th>
                 
            </tr>
            <tr >
                
                <th align='center' width='13%'><center><small style='font-size: 8px;''>TESTING METHOD</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>SPECIFICATION</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>TEST RESULT</small></center></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>&nbsp;I.CHEMICAL COMPOSITION : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$parameter}

            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>&nbsp;II.PHYSICAL PROPERTIES : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$fisika}
           
        </tbody>
    </table>

    <table width='100%'>
        <tr> <td><p style='font-size:10px;'>THE {$detail_spec[0]['NM_PRODUCT']} IS SUITABLE FOR THE PRODUCTION OF CEMENT CONFORMING TO {$detail_spec[0]['NM_STANDART_PRODUCT']}</p></td>  
        <tr> <td ><b style='font-size:10px;'><i>Formulas for mineral specification of the {$detail_spec[0]['NM_PRODUCT']}</i></b></td> 
        <tr> <td><p style='font-size:10px;white-space: pre-wrap;'><i>{$detail_spec[0]['FORMULAS']}</i></p></td>  
                        
                     </tr> 
    </table>
                                        
                                   <hr>     
                                      
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";

            echo $detail;
        } elseif ($id == $detail_quality[0]['ID_QUALITY_HEADER']) {
            $detail = "<div class='modal fade' id='dlg_detail' role='dialog' aria-hidden='true' data-backdrop='static' data-keyboard='false'>
                        <div class='modal-dialog modal-lg'>
                            <div class='modal-content'>
                                <div class='modal-header' id='dlg_header'>
                                    <button type='button' class='close' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times'></i></button>
                                    <div class='modal-title' style='    font-size: 16px;   font-weight: bold;' id='judul_input'>Detail Quality</div>
                                </div>
                                <div class='modal-body' style='overflow-y: scroll; max-height:100%;'>  
                                    
                                         
                                            <table width='100%' style='border-collapse: collapse; border: none;' font-size=15px;'>
                                                <tr><td align='center'><center>Certificate of Quality {$detail_quality[0]['NM_PRODUCT_TYPE']}</center></td> </tr>
                                                <tr><td align='center'><center>PT {$detail_quality[0]['NM_COMPANY']} ({$detail_quality[0]['NM_PLANT']})</center></td> </tr>
                                                
                                            </table>

                                            <table style='font-size: 12px;'>
                                                <tr>
                                                    <td>Date Produce : {$detail_quality[0]['START_PRODUCE']}</td>
                                                    
                                                  </tr>
                                                  <tr>
                                                    <td>To : {$detail_quality[0]['END_PRODUCE']}</td>
                                                  </tr>
                                            </table>
                                         <table width='100%' border='1' style='border-collapse: collapse;'>
        <thead>
            <tr>
                <th width='4%'  rowspan='3'> </th>
                <th align='center' rowspan='3' colspan='3' width='40%'><center style='font-size: 12px;'><p>QUALITY PARAMETERS</p></center></th>
                
                <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_quality[0]['NM_PRODUCT_TYPE']} {$detail_quality[0]['KD_PRODUCT_TYPE']}</small></center></th>
            </tr>
            <tr>
                
                  
                  <th align='center' rowspan='1' colspan='3' width='40%'><center><small style='font-size: 10px;'>{$detail_quality[0]['NM_STANDART_PRODUCT']}</small></center></th>
                 
            </tr>
            <tr >
                
                <th align='center' width='13%'><center><small style='font-size: 8px;''>TESTING METHOD</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>SPECIFICATION</small></center></th>
                <th align='center'  width='13%'><center><small style='font-size: 8px;'>TEST RESULT</small></center></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b> &nbsp;I.CHEMICAL COMPOSITION : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$parameter_quality}

            <tr>
                
                    <td></td>
                <td colspan='2'><small style='font-size: 10px;'><b>&nbsp;II.PHYSICAL PROPERTIES : </b></small></td>
                
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            {$fisika_quality}
           
        </tbody>
    </table>

    <table width='100%'>
        <tr> <td>
       
        
        
        <p style='font-size:10px;'>THE {$detail_quality[0]['NM_PRODUCT']} IS SUITABLE FOR THE PRODUCTION OF CEMENT CONFORMING TO {$detail_quality[0]['NM_STANDART_PRODUCT']}</p></td>  
        <tr> <td ><b style='font-size:10px;'><i>Formulas for mineral specification of the {$detail_quality[0]['NM_PRODUCT']}</i></b></td> 
        <tr> <td><p style='font-size:10px;white-space: pre-wrap;'><i>{$detail_quality[0]['FORMULAS']}</i></p></td>  
                        
                     </tr> 
    </table>
                                        
                                   <hr>     
                                      
                                </div> 
                                <div class='modal-footer'> 
                                    <button type='button' class='btn btn-default btn-xs btn-tutup' data-dismiss='modal'><i class='fa fa-times'></i>&nbsp;Tutup</button>
                                </div>
                            </div>
                        </div>
                    </div>";

            echo $detail;
        }
    }

   
}
