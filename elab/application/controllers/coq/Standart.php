
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standart extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('coq/M_standart');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Standart";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('coq/standart', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_standart->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshKelompok() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kelompok --</option>";
		$data = $this->M_standart->getKelompok();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KELOMPOK"]}'>".htmlspecialchars($p["NAMA_KELOMPOK"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["nm_standart"]) ){
			$column = array(
				'NM_STANDART' 
			);
			
			$data = array(
				"'".$_POST["nm_standart"]."'" 
			);
			 
			$q = $this->M_standart->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_standart"]) 
		) {
			$id = intval($_POST["id"]);
			$column = array(
				'NM_STANDART' 
			);
			
			$data = array(
				"'".$_POST["nm_standart"]."'" 
			);
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_standart->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_standart->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	} 
    
    
}
