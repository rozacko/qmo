
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		 $this->load->model('coq/M_coq_dashboard','M_COQ_DASBOARD');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Dashboard COQ";
		$data['se_clinker'] =$this->M_COQ_DASBOARD->clinker_inproses_se();
		$data['sd_clinker'] =$this->M_COQ_DASBOARD->clinker_inproses_sd();
		$data['ce_clinker'] =$this->M_COQ_DASBOARD->clinker_inproses_ce();
		$data['cd_clinker'] =$this->M_COQ_DASBOARD->clinker_inproses_cd();

		$data['se_clinker_bulan'] =$this->M_COQ_DASBOARD->clinker_bulanini_se();
		$data['sd_clinker_bulan'] =$this->M_COQ_DASBOARD->clinker_bulanini_sd();
		$data['ce_clinker_bulan'] =$this->M_COQ_DASBOARD->clinker_bulanini_ce();
		$data['cd_clinker_bulan'] =$this->M_COQ_DASBOARD->clinker_bulanini_cd();

		$data['se_clinker_tahun'] =$this->M_COQ_DASBOARD->clinker_tahunini_se();
		$data['sd_clinker_tahun'] =$this->M_COQ_DASBOARD->clinker_tahunini_sd();
		$data['ce_clinker_tahun'] =$this->M_COQ_DASBOARD->clinker_tahunini_ce();
		$data['cd_clinker_tahun'] =$this->M_COQ_DASBOARD->clinker_tahunini_cd();
		//===============CEMENT===========================//
		$data['se_cement'] =$this->M_COQ_DASBOARD->cement_inproses_se();
		$data['sd_cement'] =$this->M_COQ_DASBOARD->cement_inproses_sd();
		$data['ce_cement'] =$this->M_COQ_DASBOARD->cement_inproses_ce();
		$data['cd_cement'] =$this->M_COQ_DASBOARD->cement_inproses_cd();

		$data['se_cement_bulan'] =$this->M_COQ_DASBOARD->cement_bulanini_se();
		$data['sd_cement_bulan'] =$this->M_COQ_DASBOARD->cement_bulanini_sd();
		$data['ce_cement_bulan'] =$this->M_COQ_DASBOARD->cement_bulanini_ce();
		$data['cd_cement_bulan'] =$this->M_COQ_DASBOARD->cement_bulanini_cd();

		$data['se_cement_tahun'] =$this->M_COQ_DASBOARD->cement_tahunini_se();
		$data['sd_cement_tahun'] =$this->M_COQ_DASBOARD->cement_tahunini_sd();
		$data['ce_cement_tahun'] =$this->M_COQ_DASBOARD->cement_tahunini_ce();
		$data['cd_cement_tahun'] =$this->M_COQ_DASBOARD->cement_tahunini_cd();
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->set_graph_flot();
        $this->htmllib->set_chart_js();
        $this->htmllib->set_peity_js();

		$this->layout->render('dashboard/coq', $data);
	}


	public function getAktifUser(){

	    $showActiveUser = $this->M_COQ_DASBOARD->show_active_bulanini();
        echo json_encode($showActiveUser);
	}
}
