
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ujimt extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('approval/M_ujimt');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Persetujuan Hasil Uji oleh MT";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('approval/ujimt', $data);
	}
     
	 public function get_data()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
         $session_kate = ''; 
             $getdata = array();  
        foreach($sess_lab as $v){   
                 $on =   "'a4', 't2'";
                 $off =  'a2';  
             $detail_data = $this->M_ujimt->get_data($v['ID_LAB'], $on, $v['ID_KATEGORI']);
           
             $spuc_mt = ''; $spuc_penyelia = ''; $analis = ''; $hasil_penyelia = ''; $hasil_mt = ''; 
             // echo json_encode($detail_data);
            foreach($detail_data as $val){    
                $cek = $this->M_ujimt->cek_status($val['ID_TRANSAKSI']);
                if(count($cek)<1){ 
                     $spuc_mt = ($this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['STATUS']=='a1' ? '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-up"></i></center>' : ''); 
                    $spuc_penyelia = '<center>';$analis = '<center>';$hasil_penyelia = '<center>';$hasil_mt = ' ';
                    $cek_spuc_penyelia = $this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a2', 't1'", '');  
                    foreach($cek_spuc_penyelia as $t){
                          $jempol = ($t['STATUS']=='a2' ? " fa-thumbs-up" : " fa-thumbs-down");
                         if($t['STATUS']=='t1'){
                              $cek = $this->M_ujimt->cek_dislike($val['ID_TRANSAKSI'], "'a2'", $t['ID_KATEGORI']);
                               $jempol = (count($cek>0) && $t['ID_KATEGORI']== $cek['ID_KATEGORI'] ? '' : $jempol);     
                         }
                        $cek_kategori = $this->M_ujimt->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                        $spuc_penyelia .="<i style='color:{$cek_kategori['WARNA']}' title='Penyelia {{$cek_kategori['NM_KATEGORI']}}' class='fa fa-lg {$jempol}'></i>&nbsp;";
                    } $spuc_penyelia .= "</center>";
                    $cek_analis = $this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a3'", '');  
                    foreach($cek_analis as $t){
                        $cek_kategori = $this->M_ujimt->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                        $analis .="<i style='color:{$cek_kategori['WARNA']}' title='Analis {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg fa-thumbs-up'></i>&nbsp;";
                    } $analis .= "</center>";
                    $cek_hasil_penyelia = $this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a4', 't2'", '');  
                    foreach($cek_hasil_penyelia as $t){
                        $jempol = ($t['STATUS']=='a4' ? " fa-thumbs-up" : " fa-thumbs-down");
                         if($t['STATUS']=='t2'){
                              $cek = $this->M_ujimt->cek_dislike($val['ID_TRANSAKSI'], "'a4'", $t['ID_KATEGORI']);
                               $jempol = (count($cek>0) && $t['ID_KATEGORI']== $cek['ID_KATEGORI'] ? '' : $jempol);     
                         }
                        $cek_kategori = $this->M_ujimt->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                        $hasil_penyelia .="<i style='color:{$cek_kategori['WARNA']};cursor:pointer'  onclick='lhu({$val['ID_TRANSAKSI']}, {$t['ID_KATEGORI']})'   title='Penyelia {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg {$jempol}'></i>&nbsp;";
                    } $hasil_penyelia .= "</center>";
                    $hasil_mt = (empty($this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a5'", '')[0]['STATUS']) ? ' ': '<center><i style="color:blue" class="fa fa-lg fa-thumbs-up"></i></center>');  
                    $cito = ($this->M_ujimt->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['CITO']=='Y' ? 'YA' : '');
                    $data['SPUC'] = '--';
                    $data['NO_BAPPUC'] = $val['NO_BAPPUC'];
                    $data['CITO'] = $cito;
                    $data['TANGGAL'] = $val['TANGGAL'];
                    $data['NAMA_CONTOH'] = $val['NAMA_CONTOH'];
                    $data['SPUC_MT'] =$spuc_mt;
                    $data['SPUC_PENYELIA'] = $spuc_penyelia;
                    $data['ANALIS'] = $analis;
                    $data['HASIL_PENYELIA'] =$hasil_penyelia;
                    // $data['HASIL_MT'] = $hasil_mt;
                    $data['HASIL_MT'] = "<center><button onclick='shu({$val['ID_TRANSAKSI']},{$v['ID_KATEGORI']})'  title='SHU {$val['NAMA_CONTOH']}'  class='btn btn-primary btn-xs waves-effect btn_edit' ><span class='btn-labelx'><i class='fa fa-download'></i></span> </button></center>";
                    $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                    $data['ID_KATEGORI'] = $v['ID_KATEGORI'];  
                    $getdata[] = $data; 
                    
                } 
            }
       }    
		echo json_encode($getdata);

    }
    
    
	public function getKategori() { 
		$data = $this->M_ujimt->getKategori($_POST["id"]);
        $no = 1;
		foreach($data as $p) {
            echo "<input type='checkbox' name='kategori{$no}'  id='kategori{$no}'  value='{$p["ID_KATEGORI"]}'>Revisi ".htmlspecialchars($p["NM_KATEGORI"])."<br />";
            $no += 1;            
		}
	} 
    
    
	public function update() {
		if(isset($_POST["id"]) 
		) {
			$id = intval($_POST["id"]);
            $id_user = $this->session->userdata("USER")->ID_USER; 
			$status = 'a5';  
            $number = $this->M_ujimt->numbering();
			$numbering = ($number=='' ? 0 : $number['TOTAL']+1);
            
            $date = date('d-m-Y h:i:s');
            $column = array(
				'STATUS','ID_USER' ,'ID_TRANSAKSI', 'TANGGAL', 'NUMBERING'  
			);
			
			$data = array(
				"'".$status."'", "'".$id_user."'" , "'".$id."'" ,"SYSDATE" , "{$numbering}"
			);
			 
			$q = $this->M_ujimt->simpan_approve($column, $data);
            
                
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
      
     
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_ujimt->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
