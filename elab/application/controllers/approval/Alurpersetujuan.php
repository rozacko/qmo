
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alurpersetujuan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('approval/M_alurpenyaluran');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Approval Analis";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('approval/alurpersetujuan', $data);
	}
     
	 public function get_data()
    { 
    	$datas=$this->M_alurpenyaluran->data();
    	$no=1;
    	foreach ($datas as $key => $value) {
    		# code...
    		$value->NO=$no;
    		$datas[$key]=$value;
    		$no++;
    	}

		$print['draw']=$_POST['draw'];
		$print['recordsFiltered']= $this->M_alurpenyaluran->recordsFiltered();
		$print['recordsTotal']=$this->M_alurpenyaluran->recordsTotal();
		$print['data']=$datas;
     	echo json_encode($print);
    }
     
}
