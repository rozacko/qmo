<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyelia extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('approval/M_penyelia');
        $this->load->library('FPDF');
    }

    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Approval Penyelia";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('approval/penyelia', $data);
    }

    public function get_data()
    {
        $sess_lab = $this->session->userdata("USERLAB");
        $session_labor = '';
        $session_kate = '';
            $getdata = array();
        foreach ($sess_lab as $v) {
            $on = "'a1', 'a3', 't1', 't2'";

            $detail_data = $this->M_penyelia->get_data($v['ID_LAB'], $on,$v['ID_KATEGORI']);

            $spuc_mt = '';
            $spuc_penyelia = '';
            $analis = '';
            $hasil_penyelia = '';
            $hasil_mt = '';
            foreach ($detail_data as $val) {
                
                $cek_kategori = $this->M_penyelia->cek_kategori($val['ID_TRANSAKSI'], $v['ID_KATEGORI']);
                $off = ($val['STATUS'] == 'a3' ? "'a4' " : "'a2'");
                $cek_off = $this->M_penyelia->cek_off($val['ID_TRANSAKSI'], $off, $v['ID_KATEGORI']);
                // if($val['STATUS']=='a3'){
                    $cek_t0 = $this->M_penyelia->cek_t0($val['ID_TRANSAKSI']);
                if (count($cek_kategori) > 0 && count($cek_t0) < 1) {
                    if ($val['STATUS'] == 'a1' && count($cek_off) == 0 || $val['STATUS'] == 'a3' && $val['ID_KATEGORI'] == $v['ID_KATEGORI'] && count($cek_off) == 0) {
                        $spuc_mt = ($this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['STATUS'] == 'a1' ? '<center><i style="color:blue"  title="MT"  class="fa fa-lg fa-thumbs-up"></i></center>' : '');
                        $spuc_penyelia = '<center>';
                        $analis = '<center>';
                        $hasil_penyelia = '<center>';
                        $hasil_mt = ' ';
                        $cek_spuc_penyelia = $this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a2', 't1'", '');
                        $ket_tolak = '';
                        foreach ($cek_spuc_penyelia as $t) {
                            $jempol = ($t['STATUS'] == 'a2' ? " fa-thumbs-up" : " fa-thumbs-down"); 
                            if ($t['STATUS'] == 't1') {
                                $cek = $this->M_penyelia->cek_dislike($val['ID_TRANSAKSI'], "'a2'", $t['ID_KATEGORI']);
                                $jempol = (count($cek > 0) && $t['ID_KATEGORI'] == $cek['ID_KATEGORI'] ? '' : $jempol);
                                $ket_tolak =  $t['KETERANGAN_TOLAK'];
                            }
                            $cek_kategori = $this->M_penyelia->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $spuc_penyelia .= "<i style='color:{$cek_kategori['WARNA']}' title='Penyelia {{$cek_kategori['NM_KATEGORI']}}' class='fa fa-lg {$jempol}'></i>&nbsp;";
                        }
                        $spuc_penyelia .= "</center>";
                        $cek_analis = $this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a3'", '');
                        foreach ($cek_analis as $t) {
                            $cek_kategori = $this->M_penyelia->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $analis .= "<i style='color:{$cek_kategori['WARNA']}' title='Analis {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg fa-thumbs-up'></i>&nbsp;";
                        }
                        $analis .= "</center>";
                        $cek_hasil_penyelia = $this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a4', 't2'", '');
                        foreach ($cek_hasil_penyelia as $t) {
                            $jempol = ($t['STATUS'] == 'a4' ? " fa-thumbs-up" : " fa-thumbs-down");
                            if ($t['STATUS'] == 't2') {
                                $cek = $this->M_penyelia->cek_dislike($val['ID_TRANSAKSI'], "'a4'", $t['ID_KATEGORI']);
                                $jempol = (count($cek > 0) && $t['ID_KATEGORI'] == $cek['ID_KATEGORI'] ? '' : $jempol);
                            }
                            $cek_kategori = $this->M_penyelia->cek_kategori($val['ID_TRANSAKSI'], $t['ID_KATEGORI']);
                            $hasil_penyelia .= "<i style='color:{$cek_kategori['WARNA']}'  title='Penyelia {{$cek_kategori['NM_KATEGORI']}}'  class='fa fa-lg {$jempol}'></i>&nbsp;";
                        }
                        $hasil_penyelia .= "</center>";
                        $hasil_mt = (empty($this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a5'", '')[0]['STATUS']) ? ' ' : '<center><i style="color:blue" class="fa fa-lg fa-thumbs-up"></i></center>');
                        $cito = ($this->M_penyelia->cek_approve($val['ID_TRANSAKSI'], "'a1'", '')[0]['CITO'] == 'Y' ? 'YA' : '');

                        $cek = 0;
                        $tombol = 'SPUC';
                        $cek = count($this->M_penyelia->cek_tombol($val['ID_TRANSAKSI'], $val['STATUS'], $v['ID_KATEGORI']));
                        $tombol = ($cek == 0 ? 'SPUC' : 'UJI');


                        $data['SPUC'] = '--';
                        $data['NO_BAPPUC'] = $val['NO_BAPPUC'];
                        $data['CITO'] = $cito;
                        $data['TANGGAL'] = $val['TANGGAL'];
                        $data['NAMA_CONTOH'] = $val['NAMA_CONTOH'];
                        $data['SPUC_MT'] = $spuc_mt;
                        $data['SPUC_PENYELIA'] = $spuc_penyelia;
                        $data['ANALIS'] = $analis;
                        $data['HASIL_PENYELIA'] = $hasil_penyelia;
                        $data['HASIL_MT'] = $hasil_mt;
                        $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                        $data['ID_KATEGORI'] = $v['ID_KATEGORI'];
                        $data['STATUS'] = $val['STATUS'];
                         $get_tolak = $this->M_penyelia->cek_dislike($val['ID_TRANSAKSI'], "'t1'", $v['ID_KATEGORI']);
                         $ket_tolak = (count($get_tolak)<1 ? 'kosong' : $get_tolak['KETERANGAN_TOLAK']);
                        $data['KETERANGAN_TOLAK'] = $ket_tolak;
                        $data['TOMBOL'] = $tombol;
                        $getdata[] = $data;
                    }
                }
            }
        }
        echo json_encode($getdata);

    }


    public function update()
    {
        if (isset($_POST["approve"])
        ) {
            $id = intval($_POST["id"]);
            $id_user = $this->session->userdata("USER")->ID_USER;
            $id_kategori = intval($_POST["id_kategori"]);
            $id_status = $_POST["id_status"];
            if ($id_status == 'a1') {
                // if($id_kategori=='1'){ // KIMIA
                // $approve = 'a21';//Approve KIMIA
                // }else if($id_kategori=='2'){// FISIKA
                // $approve = 'a22'; //Approve FISIKA
                // }else{
                $approve = 'a2';
                // }
                $tolak = 't1';
            } else if ($id_status == 'a3') {
                // if($id_kategori=='1'){ // KIMIA
                // $approve = 'a41';//Approve KIMIA
                // }else if($id_kategori=='2'){// FISIKA
                // $approve = 'a42'; //Approve FISIKA
                // }else{
                $approve = 'a4';
                // }
                $tolak = 't2';
            }
            $status = ($_POST["approve"] == 'Y' ? $approve : $tolak);
            $column = array(
                'STATUS'
            );

            $data = array(
                "'" . $status . "'"
            );

            $jml_kolom = count($column);
            $data_baru = array();

            for ($i = 0; $i < $jml_kolom; $i++) {
                $data_baru[] = $column[$i] . "=" . $data[$i];
            }
            $q = $this->M_penyelia->update($id, $data_baru);
            $date = date('d-m-Y h:i:s');
            $column = array(
                'STATUS', 'ID_USER', 'ID_TRANSAKSI', 'KETERANGAN', 'TANGGAL', 'ID_KATEGORI'
            );

            $data = array(
                "'" . $status . "'", "'" . $id_user . "'", "'" . $id . "'", "'" . trim($_POST["syarat"]) . "'", "SYSDATE", "'" . $id_kategori . "'"
            );

            $q = $this->M_penyelia->simpan_approve($column, $data);

			$get_spuc = $this->M_penyelia->get_spuc($id);  

            if ($status == 'a2' ) {
                $htmlContent = "Mohon dilakukan Pengujian NO SPUC :  {$get_spuc['NO_BAPPUC']}";  
                $cekTo = $this->M_penyelia->cekTo('4', $get_spuc['KODE_LAB'], $id_kategori);
            } else if ($status == 'a4') {
                $htmlContent = "Mohon dilakukan Approval Hasil Uji NO SPUC :  {$get_spuc['NO_BAPPUC']}";  
                $cekTo = $this->M_penyelia->cekTo('2', $get_spuc['KODE_LAB'], '');
            } else if($status == 't1'){
                $htmlContent = "NO. SPUC {$get_spuc['NO_BAPPUC']} telah  dipending oleh Penyelia";  
                $cekTo = $this->M_penyelia->cekTo('2', $get_spuc['KODE_LAB'], '');
            }else {
                $htmlContent = "Hasil Uji NO. SPUC {$get_spuc['NO_BAPPUC']} telah  dipending oleh Penyelia";  
                $cekTo = $this->M_penyelia->cekTo('2', $get_spuc['KODE_LAB'], '');
            }
 
            $to = array();
            foreach($cekTo as $v){
                array_push($to, $v['EMAIL']);
            } 
            $config['protocol'] = "sendmail";
            $config['smtp_host'] = "mx3.semenindonesia.com";
            $config['smtp_port'] = "25";
            $config['smtp_user'] = "";
            $config['smtp_pass'] = "";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
            $config['crlf'] = "\r\n";
            $this->load->library('email', $config);
            $this->email->from('noreply@semenindonesia.com', 'Team IT ELAB'); 
            $this->email->to($to);
            $this->email->subject('Pengujian Analis'); 
            $this->email->message($htmlContent);
            $this->email->send();

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }


    public function export_xls()
    {
        require_once APPPATH . "/third_party/PHPExcel.php";


        $data = $this->M_penyelia->get_data_excel();
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );

        $style_center_title = array('font' => array(
            'bold' => true,
            'size' => 40
        ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $style_bg = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e3e62e')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );
        $style_blue = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '565daf')
            ),
            'font' => array(
                'color' => array('rgb' => '000000')
            )
        );

        $style_bold = array('font' => array(
            'bold' => true,
            'size' => 12
        )
        );

        $objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel();

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok');

        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg);

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');


        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]);

            $no++;
            $urutan++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');

    }


    public function lhu()
    {
        // GET DATA
        // parameter
        $param = $this->M_penyelia->get_parameter('137');
        $header = $this->M_penyelia->get_header('137', 'a3', '1');
        $parameter = '';
        $satuan = '';
        $nilai = '';
        foreach ($param as $v) {
            $parameter .= $v['NAMA_UJI'] . "\n\n";
            $satuan .= $v['SATUAN'] . "\n\n";
            $upload = $this->M_penyelia->get_nilai($v['ID_TRANSAKSI'], $v['ID_UJI']);
            $no = 1;
            foreach ($upload as $val) {
                $nilai .= $val['NILAI'] . "     ";
                if ($no == 6 || $no == 12) {
                    $nilai .= "\n";
                }
                $no += 1;
            }
        }

        $pdf = new FPDF('P', 'mm', 'A4');
        $pdf->AddPage();
        $pdf->Image('assets/image/logo_sgja.jpg', 10, 10, -150);
        //$pdf->addJpegFromFile('images/logo_kan.jpg',45,80,125,60) ;
        // $pdf->SetLineWidth(1);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(10);
        $pdf->Cell(150, '5', "PT SEMEN INDONESIA (PERSERO) Tbk. ", 0, 0);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(30, '5', "F/PPU - 13/03", 0, 1);
        $pdf->SetFont('Arial', '', 11);
        $pdf->Cell(10);
        $pdf->Cell(75, '5', "DEPARTEMEN LITBANG PRODUK & APLIKASI ", 0, 1);


        // $pdf->Rect(40,10,520,770);
        $pdf->Ln(5);
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(190, 5, "HASIL PENGUJIAN " . $header['NM_KATEGORI'], 0, 1, 'C');

        $pdf->Ln(5);
        $pdf->Cell(10);
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(50, '8', "Nama Contoh", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', $header['NAMA_CONTOH'], 0, 1);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "Jumlah ", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', $header['JUMLAH'], 0, 1);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "Diterima Tanggal ", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', $header['TANGGAL'], 0, 1);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "Dikerjakan Tanggal ", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', $header['TGL_AWAL'], 0, 1);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "SPUC ", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', $header['NO_BAPPUC'], 0, 1);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "Metode Uji", 0, 0);
        $pdf->Cell(5, '8', ":  ", 0, 0);
        $pdf->Cell(5, '8', " - ", 0, 1);
        $pdf->Ln(5);

        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(5);
        $pdf->Cell(75, '8', "ITEM PENGUJIAN", 1, 0, 'C');
        $pdf->Cell(20, '8', "SATUAN", 1, 0, 'C');
        $pdf->Cell(90, '8', "KODE CONTOH", 1, 1, 'C');

        $x = $pdf->GetX();
        $y = $pdf->GetY();

        // $col1="SIo2   AL203   FE2O3   Cao   MgO   LOL   \nFL\nInsol\nCl\nNa\nk2\n\n\n\n";
        $col1 = "{$parameter}";
        $pdf->Cell(5);
        $pdf->MultiCell(75, 5, $col1, 1, 1);

        $pdf->SetXY($x + 80, $y);

        // $col2="Kehalusan (Blaine)\nWaktu Pengikatan\nKekekalan Bentuk\nKuat Tekan\nKadar Udara\nPanas Hidrasi\nKetahanan Sulfat\nFalse Set\n\n                                             Manajer Teknik II\n\n\n\n                                          (Roganda Saragih ST)";
        $col2 = "{$satuan}";
        $pdf->MultiCell(20, 5, $col2, 1, 1);
        $pdf->SetXY($x + 100, $y);

        $col3 = "{$nilai}";
        $pdf->MultiCell(90, 5, $col3, 1);

        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(10);
        $pdf->Cell(50, '8', "Gresik, 13 Oct 2017", 0, 0);
        $pdf->Ln(30);


        $pdf->Cell(10);
        $pdf->Cell(50, '5', "(Tri Eddy)", 0, 1);

        // $pdf->Rect(350,548,40,15);
        $pdf->Output();
    }


}
