
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mt extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('approval/M_mt');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Approval MT";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('approval/mt', $data);
	}
     
	 public function get_data()
    { 
        $sess_lab = $this->session->userdata("USERLAB");
         $session_labor = '';
        foreach($sess_lab as $v){
            $session_labor .= $v['ID_LAB'].",";
        } 
        $detail_data = $this->M_mt->get_data(rtrim($session_labor, ","));
		echo json_encode($detail_data);

    }
     
	
	public function update() {
       
		if(isset($_POST["approve"]) 
		) {
			$id = intval($_POST["id"]);
			$spuc = $_POST["spuc"];
            
            $id_user = $this->session->userdata("USER")->ID_USER;
			$status = ($_POST["approve"]=='Y' ? 'a1' : 't0'); 
			$column = array(
				'STATUS' 
			);
			
			$data = array(
				"'".$status."'" 
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			} 
			$q = $this->M_mt->update($id, $data_baru);
            
            $column = array(
				'STATUS','ID_USER' ,'ID_TRANSAKSI','KETERANGAN','PERALATAN','PERSONIL','METODE','WAKTU','CITO','TANGGAL' 
			);
			$cito = isset($_POST["cito"]) ? $_POST["cito"] : '';
			$data = array(
				"'".$status."'", "'".$id_user."'" , "'".$id."'" , "'".trim($_POST["syarat"])."'" , "'".$_POST["peralatan"]."'" , "'".$_POST["personil"]."'" , "'".$_POST["metode"]."'" , "'".$_POST["waktu"]."'" , "'".$cito."'", "SYSDATE" 
			);
            
			 
			$q = $this->M_mt->simpan_approve($column, $data); 
			$get_spuc = $this->M_mt->get_spuc($id);  
            // $this->load->library('email');
            
            if($status=='a1'){
                $htmlContent = " Mohon dilakukan persetujuan No SPUC : {$get_spuc['NO_BAPPUC']} ";  
                $cekKategori = $this->M_mt->cekKategori($id);
                 $Kategori = '';
                foreach($cekKategori as $v){
                    $Kategori .= $v['ID_KATEGORI'].",";
                }  
                $cekTo = $this->M_mt->cekTo('3', $get_spuc['KODE_LAB'], rtrim($Kategori, ","));
            }else{
                $cekTo = $this->M_mt->cekTo('1', $get_spuc['KODE_LAB'], '');
                $htmlContent = " No. SPUC ({{$get_spuc['NO_BAPPUC']}})  Ditolak ";
                $htmlContent .= 'Silahkan membuat ulang kembali. ';  
            } 
            $to = array();
            foreach($cekTo as $v){
                array_push($to, $v['EMAIL']);
            } 
		       $config['protocol']  = "sendmail";
                $config['smtp_host'] = "mx3.semenindonesia.com";
                $config['smtp_port'] = "25";
                $config['smtp_user'] = "";
                $config['smtp_pass'] = "";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";
                $config['charset']   = 'utf-8';
                $config['wordwrap']  = true;
                $config['crlf']      = "\r\n";
                    $this->load->library('email',$config); 
                    $this->email->from('noreply@semenindonesia.com', 'Team IT ELAB'); 
                    $this->email->to($to);
                    $this->email->subject('Approval Penyelia '); 
                    $this->email->message($htmlContent);
                     $this->email->send(); 
                
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
     
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_mt->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
