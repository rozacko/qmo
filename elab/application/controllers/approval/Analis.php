
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Analis extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('approval/M_analis');
        $this->load->model('M_hasiluji');
        $this->load->helper('url');
    }
    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Approval Analis";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('approval/analis', $data);
    }

    public function get_data()
    {
        $sess_lab = $this->session->userdata("USERLAB");
        $getdata  = array();
        foreach ($sess_lab as $v) {
            $on          = 'a2';
            $off         = 'a3';
            $status      = 'a3';
            $detail_data = $this->M_analis->get_data($v['ID_LAB'], $on, $v['ID_KATEGORI']);
            $no          = 0;
            foreach ($detail_data as $val) {
                // $cek_kategori = $this->M_analis->cek_kategori($val['ID_TRANSAKSI'], $v['ID_KATEGORI']);
                // $cek_off = $this->M_analis->cek_off($val['ID_TRANSAKSI'], $off, $v['ID_KATEGORI']);
                // if( count($cek_kategori)>0 && count($cek_off)==0 ){
                $tgl                  = $this->M_analis->cek_approve($val['ID_TRANSAKSI'], $status, $v['ID_KATEGORI']);
                $cito                 = $this->M_analis->cek_cito($val['ID_TRANSAKSI'], 'a1');
                $komentar                 = $this->M_analis->cek_komentar($val['ID_TRANSAKSI']);
                $tot_appprove         = count($tgl);
                $data['NO_BAPPUC']    = $val['NO_BAPPUC'];
                $data['TANGGAL']      = $val['TANGGAL'];
                $data['NAMA_CONTOH']  = $val['NAMA_CONTOH'];
                $data['JUMLAH']       = $val['JUMLAH'];
                $data['TGL_AWAL']     = $tgl['AWAL'];
                $data['TGL_AKHIR']    = $tgl['AKHIR'];
                $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                $data['ID_APPROVE']   = $tgl['ID_APPROVE'];
                $data['ID_KATEGORI']  = $v['ID_KATEGORI'];
                $data['CITO']         = $cito['CITO'];
                $data['KOMENTAR']         =$komentar['KETERANGAN'];
                $data['TOT_APPROVE']  = $tot_appprove;
                $no += 1;
                $getdata[] = $data;
                // }
            }
        }

        echo json_encode($getdata);

    }
    
    
    public function detail_upload()
    {
        
       // $param        = $this->M_hasiluji->get_parameter($id_transaksi, $id_kategori);
        $param        = $this->M_hasiluji->get_parameter('262', '2');
        
        $lembar['1']          = '';
        $lembar['2']          = '';
        $urut         = 1; 
        foreach ($param as $v) {
            $upload = $this->M_hasiluji->get_nilai($v['ID_TRANSAKSI'], $v['ID_UJI']); 
            if(count($upload) <= 12){
                $kali = 1;  
            }else if(count($upload) >= 12){
                $kali = 2;   
            } 
            for($i=1;$i<=$kali;$i++){
                if($i==1){ 
                    $start = 1;
                    $end = 12;
                }else{ 
                    $start = 13;
                    $end = 24;
                }
                 $lembar["{$i}"] .= "<table width='100%'> <tr><td width='5%' rowspan='2' >{$urut}</td>";
                 $cek_param = $this->M_analis->get_cek_parameter($v['ID_TRANSAKSI'], $v['ID_UJI'], '2', $start, $end); 
                if (count($cek_param) > 6) {
                    $lembar["{$i}"] .= "<td rowspan='2' width='31%'>{$v["NAMA_UJI"]}</td>";
                    $lembar["{$i}"] .= "<td rowspan='2' width='10%'>{$v["SATUAN"]}</td>";
                } else {
                    $lembar["{$i}"] .= "<td  width='35%'>{$v["NAMA_UJI"]}</td>";
                    $lembar["{$i}"] .= "<td width='10%'>{$v["SATUAN"]}</td>";
                }
                $no   = 1;
                $isi1 = '';
                $halaman = 1;
                for($s=$start;$s<=$end;$s++){ 
                    $get_nilai = $this->M_analis->get_nilai($v['ID_TRANSAKSI'], $v['ID_UJI'], $s);
                    $nilai     = ($get_nilai['NILAI'] == '' ? '' : $get_nilai['NILAI']);
                     if ($i < 7) {
                        $lembar["{$i}"] .= "<td  width='9%' >{$nilai}</td>";
                    } else {
                        $isi1 .= "<td  width='9%' >{$nilai}</td>"; 
                        $halaman = 2;
                    }
                }
                    
                if ($halaman == 2) {
                    $lembar["2"] .= "<tr>";
                    $lembar["2"] .= $isi1;
                    $lembar["2"] .= "</tr>";
                }

                $lembar["{$i}"] .= "</tr></table>"; 
            
                $urut += 1;
            }
               
        }
        print_r($lembar['1']);exit();
        
}

  public function detail_data()
    { 
            $detail_data = $this->M_analis->get_detail_data($_POST['id_transaksi'], $_POST['id_kategori']);
            $no          = 0;
            $getdata = array();
            foreach ($detail_data as $val) { 
                $data['NAMA_UJI']    = $val['NAMA_UJI'];
                $data['SAMPEL']      = $val['SAMPEL'];
                $data['NILAI']      = $val['NILAI']; 
                $no += 1;
                $getdata[] = $data;
                // }
            } 

        echo json_encode($getdata);
    }

    public function update()
    {
        if (isset($_POST["approve"])
        ) {
            $id      = intval($_POST["id"]);
            $id_user = $this->session->userdata("USER")->ID_USER;
            $status  = ($_POST["approve"] == 'Y' ? 'a1' : 'a0');
            $column  = array(
                'STATUS',
            );

            $data = array(
                "'" . $status . "'",
            );

            $jml_kolom = count($column);
            $data_baru = array();

            for ($i = 0; $i < $jml_kolom; $i++) {
                $data_baru[] = $column[$i] . "=" . $data[$i];
            }
            $q = $this->M_analis->update($id, $data_baru);

            $column = array(
                'STATUS', 'ID_USER', 'ID_TRANSAKSI', 'KETERANGAN',
            );

            $data = array(
                "'" . $status . "'", "'" . $id_user . "'", "'" . $id . "'", "'" . trim($_POST["syarat"]) . "'",
            );

            $q = $this->M_analis->simpan_approve($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function export_xls()
    {
        require_once APPPATH . "/third_party/PHPExcel.php";

        $data         = $this->M_analis->get_data_excel();
        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'),
                ),
            ),
        );

        $style_center_title = array('font' => array(
            'bold' => true,
            'size' => 40,
        ),
            'alignment'                        => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );

        $style_bg = array(
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'e3e62e'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
            ),
        );
        $style_blue = array(
            'fill' => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '565daf'),
            ),
            'font' => array(
                'color' => array('rgb' => '000000'),
            ),
        );

        $style_bold = array('font' => array(
            'bold' => true,
            'size' => 12,
        ),
        );

        $objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel();

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok');

        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg);

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');

        $no     = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
            $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
            $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
            $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]);

            $no++;
            $urutan++;
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');

    }

    public function mulai()
    {
        if (isset($_POST["id"]) && isset($_POST["id_approve"])) {
            $id_user = $this->session->userdata("USER")->ID_USER;
            // $q = $this->M_analis->mulai($_POST["id"], $_POST["id_approve"]);
            $column = array(
                'STATUS', 'ID_USER', 'ID_TRANSAKSI', 'TGL_AWAL', 'ID_KATEGORI',
            );

            $data = array(
                "'a3'", "'" . $id_user . "'", "'" . $_POST["id"] . "'", "SYSDATE", "'" . $_POST["id_kategori"] . "'",
            );

            $q = $this->M_analis->simpan_approve($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function selesai()
    {
        if (isset($_POST["id"]) && isset($_POST["id_kategori"])) {

            $q = $this->M_analis->selesai($_POST["id"], 'a3', $_POST["id_kategori"]); 
            
			$get_spuc = $this->M_analis->get_spuc($_POST["id"]);  
            $cekTo = $this->M_analis->cekTo('3', $get_spuc['KODE_LAB'], $_POST["id_kategori"]); 
            $htmlContent = " Mohon dilakukan Approval Hasil Uji NO SPUC :  {$get_spuc['NO_BAPPUC']} "; 
            $to = array();
            foreach($cekTo as $v){
                array_push($to, $v['EMAIL']);
            } 
            $config['protocol'] = "sendmail";
            $config['smtp_host'] = "mx3.semenindonesia.com";
            $config['smtp_port'] = "25";
            $config['smtp_user'] = "";
            $config['smtp_pass'] = "";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
            $config['crlf'] = "\r\n";
            $this->load->library('email', $config);
            $this->email->from('noreply@semenindonesia.com', 'Team IT ELAB'); 
            $this->email->to($to);
            $this->email->subject('Approval Hasil Uji'); 
            $this->email->message($htmlContent);
            $this->email->send();
            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function email()
    {
        // $this->load->library('email');
        $config['protocol']  = "sendmail";
        $config['smtp_host'] = "mx3.semenindonesia.com";
        $config['smtp_port'] = "25";
        $config['smtp_user'] = "";
        $config['smtp_pass'] = "";
        $config['mailtype']  = "html";
        $config['newline']   = "\r\n";
        $config['charset']   = 'utf-8';
        $config['wordwrap']  = true;
        $config['crlf']      = "\r\n";
        // $config = Array(
        // 'protocol' => 'smtp',
        // 'smtp_host' => 'mx3.semenindonesia.com',
        // 'smtp_port' => "25",
        // 'smtp_user' => '',
        // 'smtp_pass' => '',
        // 'mailtype'  => 'html',
        // 'charset'   => 'iso-8859-1'
        // );
        // $this->load->library('email', $config);
        // $this->email->set_newline("\r\n");
        // $this->email->from("bisniskokoh@semenindonesia.com","ADMIN BISNIS KOKOH");
        
        $this->load->library('email',$config);
                // $this->email->initialize($config);
        $this->email->from('noreply@semenindonesia.com', 'HRIS Batch');

        // $this->email->to('95irhasmadani95@gmail.com');
        $this->email->to('a.khafitsbi.ext@semenindonesia.com');
        $this->email->subject('TESSS Elab pit');
        $this->email->message('TUsSSS Elab pit');
        if ($this->email->send()) {
            echo 'bisa bang';
        } else {
            show_error($this->email->print_debugger());
        }

    }
    
    public function export_excel()
    {
        
        $contoh       = $_GET["contoh"];
        $jumlah       = $_GET["jumlah"];
        $spuc         = $_GET["spuc"];
        $metode       = $_GET["metode"];
        $tgl_terima   = $_GET["tgl_terima"];
        $tgl_kerja    = $_GET["tgl_kerja"];
        $tgl_selesai  = $_GET["tgl_selesai"];
        $kategori     = $_GET["kategori"];
        $id_transaksi = $_GET["id_transaksi"];
        $standart     = $this->M_analis->metode($id_transaksi,$kategori);
        
        // $nm_bagian = $this->M_risk_priority->cek_bagian($muk_kode);
        // if(isset($tahun) && $tahun != "" && isset($muk_kode) && $muk_kode != "") {
        require_once APPPATH . "/third_party/PHPExcel.php";

        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $styles    = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
        );
        $sam = $this->M_analis->transaksi($id_transaksi);
        $box    = $this->M_analis->box($id_transaksi);
        $cek = $this->M_analis->cek_assign($id_transaksi, 'a2', $kategori);
       
        if ($kategori == '1') {

            if ($box['ID_UJI'] == '221') {
            $objPHPExcel  = $objReader->load("assets/template/Form_Uji_XRF.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();
            // $objWorksheet->getStyle("A2:W3")->applyFromArray($styles);
            
            $objWorksheet->setCellValue("E6", 'Semen');
            $objWorksheet->setCellValue("E12", $contoh);
            $objWorksheet->setCellValue("E7", $jumlah);
            $objWorksheet->setCellValue("E10", $spuc);
            $objWorksheet->setCellValue("E11", $standart['NAMA_STANDART']);
            $objWorksheet->setCellValue("E8", $tgl_kerja);
            $objWorksheet->setCellValue("E9", $tgl_selesai);
            $objWorksheet->setCellValue("F18", '1');
            $objWorksheet->setCellValue("G18", '2');
            $objWorksheet->setCellValue("H18", '3');
            $objWorksheet->setCellValue("I18", '4');
            $objWorksheet->setCellValue("J18", '5');
            $objWorksheet->setCellValue("K18", '6');
            $objWorksheet->setCellValue("I59", $this->session->userdata("USER")->FULLNAME);
            //$objWorksheet->setCellValue("I59", $cek['FULLNAME']);

            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Kimia XRF.xlsx";
            }else{
            $objPHPExcel  = $objReader->load("assets/template/analis_kimia.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();
            // $objWorksheet->getStyle("A2:W3")->applyFromArray($styles);

            $objWorksheet->setCellValue("F7", $contoh);
            $objWorksheet->setCellValue("F8", $jumlah);
            $objWorksheet->setCellValue("F9", $spuc);
            $objWorksheet->setCellValue("F10", $standart['NAMA_STANDART']);
            $objWorksheet->setCellValue("N7", $tgl_kerja);
            $objWorksheet->setCellValue("N8", $tgl_terima);
            $objWorksheet->setCellValue("V10", $this->session->userdata("USER")->FULLNAME);
            $objWorksheet->setCellValue("Y10", $cek['FULLNAME']);

            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Kimia.xlsx";
            }
        } else if ($kategori == '2') {
            if ($box['ID_UJI'] == '223' || $metode == '0') {
            $objPHPExcel  = $objReader->load("assets/template/Form_PSD.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();
            // $objWorksheet->getStyle("A2:W3")->applyFromArray($styles);

            $objWorksheet->setCellValue("D7", $contoh);
            $objWorksheet->setCellValue("D8", $spuc);
            $objWorksheet->setCellValue("D10", $tgl_kerja);
            $objWorksheet->setCellValue("D9", $tgl_terima);
            $objWorksheet->setCellValue("E14", '1');
            $objWorksheet->setCellValue("F14", '2');
            $objWorksheet->setCellValue("G14", '3');
            $objWorksheet->setCellValue("H14", '4');
            $objWorksheet->setCellValue("I14", '5');
            $objWorksheet->setCellValue("J14", '6');

            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Fisika PSD.xlsx";
            }else{
            $objPHPExcel  = $objReader->load("assets/template/analis_fisika.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();

             $objWorksheet->getStyle("A2:W3")->applyFromArray($styles);

            
            $objWorksheet->setCellValue("O7", $spuc);
            $objWorksheet->setCellValue("O8", $tgl_terima);
            $objWorksheet->setCellValue("O9", $jumlah);

            $objWorksheet->setCellValue("O10", $tgl_kerja);
            $objWorksheet->setCellValue("T10", $tgl_selesai);
            $objWorksheet->setCellValue("O11", $standart['NAMA_STANDART']);
            $objWorksheet->setCellValue("AF10", $this->session->userdata("USER")->FULLNAME);
            $objWorksheet->setCellValue("AJ10", $cek['FULLNAME']);
            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Fisika.xlsx";
            }
        }else if($kategori=='3'){
            $objPHPExcel  = $objReader->load("assets/template/beton internal.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();
            
            $objWorksheet->setCellValue("G9", $spuc);
            $objWorksheet->setCellValue("G10", $tgl_terima);
            

            $objWorksheet->setCellValue("G13", $tgl_kerja);
            $objWorksheet->setCellValue("K13", $tgl_selesai);
            $objWorksheet->setCellValue("G14", $standart['NAMA_STANDART']);
            $objWorksheet->setCellValue("W12", $this->session->userdata("USER")->FULLNAME);
            $objWorksheet->setCellValue("AA12", $cek['FULLNAME']);

            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Beton.xlsx";
        }else if($kategori=='5'){
            $objPHPExcel  = $objReader->load("assets/template/analis_limbah.xlsx");
            $objWorksheet = $objPHPExcel->getActiveSheet();
              $objWorksheet->setCellValue("D8", $spuc);
            $objWorksheet->setCellValue("D9", $contoh);
            $objWorksheet->setCellValue("D10", $tgl_terima); 
            $objWorksheet->setCellValue("D11", $tgl_kerja); 
            $tempfile = "[" . date("Y-m-d") . "]".$id_transaksi."_Template Upload Analis Limbah.xlsx";
        }

        

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $tempfile . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as.XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save("php://output");

        // } else { ,
        // echo "Error";
        // }
    }

    public function UploadTemplate()
    {
        $username=$this->session->userdata("USER")->USERNAME;

        if($username=="analis.limbah"){
            $this->uploadAnalisLimbah();
        }elseif($username=="analis.beton"){
            $this->uploadAnalisbeton();
        }else{
             $this->UploadTemplate2();
        }
       
    }

    private function uploadAnalisLimbah()
    {
        require_once APPPATH . "/third_party/PHPExcel.php";
        $objReader     = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel   = $objReader->load($_FILES["file_input"]["tmp_name"]);
        $objWorksheet  = $objPHPExcel->getActiveSheet();
        $highestRow    = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        // WHERE
        $id_transaksi = $_POST['id_transaksi_upload'];
        $id_kategori  = $_POST['kategori_upload'];
        $countupload  = $_POST['countupload'];
        $id_user      = $this->session->userdata("USER")->ID_USER;

        if($objWorksheet->getCell("A6")->getCalculatedValue()!="LEMBAR HASIL UJI LIMBAH B3" or $highestRow < 244  ){
            echo json_encode(['notif'=>0,'message'=>'Template Salah, Upload Template Analis Limbah']);exit;
        }
        if(
        $objWorksheet->getCell("B16")->getCalculatedValue() != "Moisture Content" or
        $objWorksheet->getCell("B17")->getCalculatedValue() != "Total Sulphur (adb)" or
        $objWorksheet->getCell("B19")->getCalculatedValue() != "Calori   (adb)" or
        $objWorksheet->getCell("B21")->getCalculatedValue() != "Loss On Ignition" or
        $objWorksheet->getCell("B22")->getCalculatedValue() != "Ash Content [adb]" or
        $objWorksheet->getCell("B26")->getCalculatedValue() != "Silicon Dioxide" or
        $objWorksheet->getCell("B27")->getCalculatedValue() != "Aluminium Oxide" or
        $objWorksheet->getCell("B28")->getCalculatedValue() != "Ferric Oxide" or
        $objWorksheet->getCell("B29")->getCalculatedValue() != "Calcium Oxide" or
        $objWorksheet->getCell("B31")->getCalculatedValue() != "Magnesium Oxide" or
        $objWorksheet->getCell("B32")->getCalculatedValue() != "Sulfur trioksida" or
        $objWorksheet->getCell("B35")->getCalculatedValue() != "Arsenic" or
        $objWorksheet->getCell("B36")->getCalculatedValue() != "Cadmium" or
        $objWorksheet->getCell("B37")->getCalculatedValue() != "Chromium" or
        $objWorksheet->getCell("B38")->getCalculatedValue() != "Timbal" or
        $objWorksheet->getCell("B39")->getCalculatedValue() != "Merkuri" or
        $objWorksheet->getCell("B40")->getCalculatedValue() != "Cobalt" or
        $objWorksheet->getCell("B41")->getCalculatedValue() != "Nikel" or
        $objWorksheet->getCell("B42")->getCalculatedValue() != "Copper" or
        $objWorksheet->getCell("B43")->getCalculatedValue() != "Zinc" or
        $objWorksheet->getCell("B44")->getCalculatedValue() != "Selenium")
        {
             echo json_encode(['notif'=>0,'message'=>'Template Salah, Upload Template Analis Limbah']);exit;
        }
        

        $Arr = [];

        /*H16 Moisture Content*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 61, 'NILAI' => round($objWorksheet->getCell("H16")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B16")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Total Sulphur abd*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 62, 'NILAI' => round($objWorksheet->getCell("H17")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B17")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Calori   (adb)*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 63, 'NILAI' => round($objWorksheet->getCell("H19")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B19")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Loss On Ignition*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 181, 'NILAI' => round($objWorksheet->getCell("H21")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B21")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Ash Content*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 182, 'NILAI' => round($objWorksheet->getCell("H22")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B22")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Silicon Dioxide*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 183, 'NILAI' => round($objWorksheet->getCell("H26")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B26")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Alumninium Oxide*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 184, 'NILAI' => round($objWorksheet->getCell("H27")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B27")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Feric Oxide*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 185, 'NILAI' => round($objWorksheet->getCell("H28")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B28")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Calcium Oxice*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 186, 'NILAI' => round($objWorksheet->getCell("H29")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B29")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Magnesium Oxide*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 187, 'NILAI' => round($objWorksheet->getCell("H31")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B31")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Sulfur Trioksida*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 188, 'NILAI' => round($objWorksheet->getCell("H32")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B32")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Arsenic*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 64, 'NILAI' => round($objWorksheet->getCell("H35")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B35")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Cadmium*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 65, 'NILAI' => round($objWorksheet->getCell("H36")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B36")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Chromiun*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 66, 'NILAI' => round($objWorksheet->getCell("H37")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B37")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Timbal*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 189, 'NILAI' => round($objWorksheet->getCell("H38")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B38")->getCalculatedValue()];
        array_push($Arr,$data);
        /*merkuri*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 68, 'NILAI' => round($objWorksheet->getCell("H39")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B39")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Cobalt*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 71, 'NILAI' => round($objWorksheet->getCell("H40")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B40")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Nikel*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 72, 'NILAI' => round($objWorksheet->getCell("H41")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B41")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Cooper*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 73, 'NILAI' => round($objWorksheet->getCell("H42")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B42")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Zinc*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 75, 'NILAI' => round($objWorksheet->getCell("H43")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B43")->getCalculatedValue()];
        array_push($Arr,$data);
        /*Selenium*/
        $data  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 76, 'NILAI' => round($objWorksheet->getCell("H44")->getCalculatedValue(),2), 'KETERANGAN' => $objWorksheet->getCell("B44")->getCalculatedValue()];
        array_push($Arr,$data);


        if(empty($Arr))
        {
            echo json_encode(array('notif' => '0', 'message' => 'Tidak Ada Data Yang Disimpan'));exit;
        }

        $data=$this->M_analis->saveUpload($Arr,$id_transaksi,$id_kategori);
        
        if($data){

            // $destination_folder = 'assets/uploadexcel';
            // $new_name_file = $id_transaksi."-".$id_kategori.".xlsx";
            // $file = "$destination_folder/$new_name_file";
            // unlink($file);
            // // insert Data base
            // move_uploaded_file($_FILES['file_input']['tmp_name'], $file);

            echo json_encode(array('notif' => '1', 'message' => 'Save Data Upload Berhasil'));exit;
        }else{
            echo json_encode(array('notif' => '0', 'message' => 'Save Data Upload Gagal'));exit;
        }
       
    }

    private function uploadAnalisbeton()
    {

        require_once APPPATH . "/third_party/PHPExcel.php";
        $objReader     = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel   = $objReader->load($_FILES["file_input"]["tmp_name"]);
        $objWorksheet  = $objPHPExcel->getActiveSheet();
        $highestRow    = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        // WHERE
        $id_transaksi = $_POST['id_transaksi_upload'];
        $id_kategori  = $_POST['kategori_upload'];
        $countupload  = $_POST['countupload'];
        $id_user      = $this->session->userdata("USER")->ID_USER;

        $Arr = [];

        /*Calculation Kuat BETON*/
        $loopsampel  = 6;
        $columnstart = 23;
        $maxColumn   = $this->abjadToNum("AC");
        $rowheight   = 42;
        $C17=$objWorksheet->getCell("C17")->getCalculatedValue();
        $C160=$objWorksheet->getCell("C160")->getCalculatedValue();
        $C172=$objWorksheet->getCell("C172")->getCalculatedValue();
        $C204=$objWorksheet->getCell("C204")->getCalculatedValue();
        $C225=$objWorksheet->getCell("C225")->getCalculatedValue();
        $C245=$objWorksheet->getCell("C245")->getCalculatedValue();
        $C260=$objWorksheet->getCell("C260")->getCalculatedValue();


        /*Cek Template*/
        if($highestRow<320){
            echo json_encode(array('notif' => '0', 'message' => 'Template Salah, Upload Tempale Analis Beton'));exit;
        }

        if(
         $C17 != "PENGUJIAN KUAT TEKAN BETON"  or
         $C160 != "PENGUJIAN SLUMP BETON" or
         $C172 != "PENGUJIAN  KUAT LENTUR DENGAN 3 TITIK PEMBEBANAN"  or
         $C204 != "PENGUJIAN KUAT LENTUR DENGAN 1 TITIK PEMBEBANAN TERPUSAT" or
         $C225 != "PENGUJIAN KUAT TARIK BELAH SILINDER" or
         $C245 != "PENGUJIAN PERMEABILITY" or
         $C260 != "PENGUJIAN SETTING TIME BETON"
        ){
            echo json_encode(array('notif' => '0', 'message' => 'Template Salah'));exit;
        }

        /*End Cek Template*/
        $kali=(($countupload-1)*6);

        /*Looop Sebanyak Max Jumlah SAMPEL Kuat Tekan Beton*/
        for ($i = 1; $i <= 6; $i++) 
        {
            # code...

            /*Loop Find Check Column*/
            $checkColumn = [];
            for ($j = 6; $j < $maxColumn; $j++) {
                # code...
                $abjad = $this->numToAbjad($j);
                $value = $objWorksheet->getCell("{$abjad}{$columnstart}")->getCalculatedValue();
                if ($value == 'v' or $value == 'V') {
                    array_push($checkColumn, $j);
                }
            }

            if(count($checkColumn)==0){
                continue;
            }

            $arrDays    = [1, 3, 7, 14, 21, 28];
            $arrDaysKey = [168, 169, 170, 171, 172, 173];
            $maxLoop    = 0;
            $loopColumn = 6;

            foreach ($arrDays as $key => $val) {
                # code...
                $colstart=$loopColumn+(($key+1)*4);
                               
                $value1  = 0;$rows1   = ($columnstart + 1);
                $value2  = 0;$rows2   = ($columnstart + 2);
                $value3  = 0;$rows3   = ($columnstart + 3);
                $value4  = 0;$rows4   = ($columnstart + 4);
                $value5  = 0;$rows5   = ($columnstart + 5);
                $value6  = 0;$rows6   = ($columnstart + 6);
                $value7  = 0;$rows7   = ($columnstart + 7);
                $value8  = 0;$rows8   = ($columnstart + 8);
                $value9  = 0;$rows9   = ($columnstart + 9);
                $value10 = 0;$rows10  = ($columnstart + 10);
                $value11 = 0;$rows11  = ($columnstart + 11);
                $value12 = 0;$rows12  = ($columnstart + 13);
                $value13 = 0;$rows13  = ($columnstart + 14);

                $counter = 0;

                for ($k = $maxLoop; $k < count($checkColumn); $k++) {
                    # code...

                    if ($checkColumn[$k] < $colstart && $checkColumn[$k] >= ($colstart-4) ) {
                        $counter += 1;
                        $huruf = $this->numToAbjad($checkColumn[$k]);
                        $value1 += $objWorksheet->getCell("{$huruf}{$rows1}")->getCalculatedValue();
                        $value2 += $objWorksheet->getCell("{$huruf}{$rows2}")->getCalculatedValue();
                        $value3 += $objWorksheet->getCell("{$huruf}{$rows3}")->getCalculatedValue();
                        $value4 += $objWorksheet->getCell("{$huruf}{$rows4}")->getCalculatedValue();
                        $value5 += $objWorksheet->getCell("{$huruf}{$rows5}")->getCalculatedValue();
                        $value6 += $objWorksheet->getCell("{$huruf}{$rows6}")->getCalculatedValue();
                        $value7 += $objWorksheet->getCell("{$huruf}{$rows7}")->getCalculatedValue();
                        $value8 += $objWorksheet->getCell("{$huruf}{$rows8}")->getCalculatedValue();
                        $value9 += $objWorksheet->getCell("{$huruf}{$rows9}")->getCalculatedValue();
                        $value10 += $objWorksheet->getCell("{$huruf}{$rows10}")->getCalculatedValue();
                        $value11 += $objWorksheet->getCell("{$huruf}{$rows11}")->getCalculatedValue();
                        $value12 += $objWorksheet->getCell("{$huruf}{$rows12}")->getCalculatedValue();
                        $value13 += $objWorksheet->getCell("{$huruf}{$rows13}")->getCalculatedValue();
                        // $value += $objWorksheet->getCell("{$huruf}24")->getCalculatedValue();
                        $maxLoop = $k;
                    }
                }  
                                
                if($counter>0){
                    $parse1  = "C" . $rows1;
                    $parse2  = "D" . $rows2;
                    $parse3  = "D" . $rows3;
                    $parse4  = "D" . $rows4;
                    $parse5  = "C" . $rows5;
                    $parse6  = "C" . $rows6;
                    $parse7  = "C" . $rows7;
                    $parse8  = "C" . $rows8;
                    $parse9  = "C" . $rows9;
                    $parse10 = "C" . $rows10;
                    $parse11 = "C" . $rows11;
                    $parse12 = "C" . $rows12;
                    $parse13 = "C" . $rows13;

                    $out1  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value1 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse1)->getCalculatedValue())];
                    $out2  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value2 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse2)->getCalculatedValue())];
                    $out3  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value3 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse3)->getCalculatedValue())];
                    $out4  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value4 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse4)->getCalculatedValue())];
                    $out5  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value5 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse5)->getCalculatedValue())];
                    $out6  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value6 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse6)->getCalculatedValue())];
                    $out7  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value7 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse7)->getCalculatedValue())];
                    $out8  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value8 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse8)->getCalculatedValue())];
                    $out9  = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value9 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse9)->getCalculatedValue())];
                    $out10 = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value10 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse10)->getCalculatedValue())];
                    $out11 = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value11 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse11)->getCalculatedValue())];
                    $out12 = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value12 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse12)->getCalculatedValue())];
                    $out13 = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => $arrDaysKey[$key], 'NILAI' => round(($value13 / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell($parse13)->getCalculatedValue())];

                    array_push($Arr, $out1);
                    array_push($Arr, $out2);
                    array_push($Arr, $out3);
                    array_push($Arr, $out4);
                    array_push($Arr, $out5);
                    array_push($Arr, $out6);
                    array_push($Arr, $out7);
                    array_push($Arr, $out8);
                    array_push($Arr, $out9);
                    array_push($Arr, $out10);
                    array_push($Arr, $out11);
                    array_push($Arr, $out12);
                    array_push($Arr, $out13);
                }
                

            }

            $columnstart += 23;

        }


        // /*Pengujian Slum BETON*/
        $ArrSlum = [];
        $slumBetonRows=166; /* Rown Start Check Slup beton*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrSlum, $j);
            }
        }

        $maxLoop=0;
        $loopColumn=6;

        for ($i=1; $i <=6 ; $i++) { 
            $valueslum=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrSlum); $k++) {
                if ($ArrSlum[$k] < $colstart && $ArrSlum[$k] >= ($colstart-4) ) {
                    $counter += 1;
                    $huruf = $this->numToAbjad($ArrSlum[$k]);
                    $valueslum += $objWorksheet->getCell("{$huruf}167")->getCalculatedValue();
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 161, 'NILAI' => round(($valueslum / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "NILAI SLUMP SAMPEL ".$i];
                array_push($Arr,$dataslum);
            }
        }


        // /*PENGUJIAN  KUAT LENTUR DENGAN 3 TITIK PEMBEBANAN*/
        $ArrKL = [];
        $slumBetonRows=178; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        // echo json_encode($ArrKL);exit;
        $maxLoop=0;
        $loopColumn=6;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL1= $objWorksheet->getCell("{$huruf}197")->getCalculatedValue();
                    $valueKL2= $objWorksheet->getCell("{$huruf}198")->getCalculatedValue();
                    $valueKL3= $objWorksheet->getCell("{$huruf}199")->getCalculatedValue();
                    if($valueKL1!="-" and $valueKL1!="")
                    {
                        $valueKL+=$valueKL1;
                        $counter+=1;
                    }
                    if($valueKL2!="-" and $valueKL2!="")
                    {
                        $valueKL+=$valueKL2;
                        $counter+=1;
                    }
                    if($valueKL3!="-" and $valueKL3!="")
                    {
                        $valueKL+=$valueKL3;
                        $counter+=1;
                    }
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 162, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "NILAI KUAT LENTUR SAMPEL KE-".$i];
                array_push($Arr,$dataslum);
            }
        }



        /*PENGUJIAN KUAT LENTUR DENGAN 1 TITIK PEMBEBANAN TERPUSAT*/
        $ArrKL = [];
        $slumBetonRows=210; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        // echo json_encode($ArrKL);exit;
        $maxLoop=0;
        $loopColumn=6;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}220")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 163, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "KUAT LENTUR TERPUSAT 1 TITIK TERPUSAT SAMPEL KE-".$i];
                array_push($Arr,$dataslum);
            }
        }


        /*Pengujian TARIK BELAH SELINDER*/
        $ArrKL = [];
        $slumBetonRows=231; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        
        $maxLoop=0;
        $loopColumn=6;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}240")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 164, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "TARIK BELAH SELINDER SAMPEL KE-".$i];
                array_push($Arr,$dataslum);
            }
        }


        /*Pengujian PERMEABILITY*/
        $ArrKL = [];
        $slumBetonRows=251; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        
        $maxLoop=0;
        $loopColumn=6;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}255")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 165, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "KEDALAMANREMBESAN"];
                array_push($Arr,$dataslum);
            }
        }


        /*PENGUJIAN SETTING TIME BETON*/
        // Loop n SAMPEL
        $awalRow=280;
        for ($i=1; $i <=6 ; $i++) {  
            # code...
            // 
             $hurufAwal="F".$awalRow;
            $hurufAkhir="F".($awalRow+1);

            if(($i%2)==0){
                $hurufAwal="R".$awalRow;
                $hurufAkhir="R".($awalRow+1);
                 $awalRow+=21;
            }
           

            $awal=PHPExcel_Style_NumberFormat::toFormattedString($objWorksheet->getCell($hurufAwal)->getCalculatedValue(),'h:i');
            $akhir=PHPExcel_Style_NumberFormat::toFormattedString($objWorksheet->getCell($hurufAkhir)->getCalculatedValue(),'h:i');
            $dataawal = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 166, 'NILAI' => $awal, 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "WAktu Pengikatan Awal"];
            $dataakhir = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 167, 'NILAI' => $akhir, 'SAMPEL' => ($kali+$i), 'KETERANGAN' => "WAktu Pengikatan Akhir"];

            array_push($Arr,$dataawal);
            array_push($Arr,$dataakhir);
        }

        /*PENGUJIAN  BERAT ISI DAN RONGGA UDARA DALAM AGREGAT*/
        $ArrKL = [];
        $slumBetonRows=331; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        
        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=346;
        for ($p=0; $p < 3 ; $p++) { 
            # code...
            $maxLoop=0;
            for ($i=1; $i <=6 ; $i++) { 
                $valueKL=0;
                $counter=0;
                $colstart=$loopColumn+($i*4);
                # code...
                 for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                    if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                        $huruf = $this->numToAbjad($ArrKL[$k]);
                        $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                        $counter+=1;                   
                        $maxLoop = $k;
                    }
                 }
                 if($counter>0){
                     $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 192, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue())];
                    array_push($Arr,$dataslum);
                }
            }
            $barisketerangan+=2;
        }


        
        
        /*PENGUJIAN KEAUSAN AGREGAT*/
        $ArrKL = [];
        $slumBetonRows=361; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=373;

        for ($p=0; $p < 2 ; $p++) { 
            # code...
            $maxLoop=0;
            for ($i=1; $i <=6 ; $i++) { 
                $valueKL=0;
                $counter=0;
                $colstart=$loopColumn+($i*4);
                # code...
                 for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                    if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                        $huruf = $this->numToAbjad($ArrKL[$k]);
                        $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                        $counter+=1;                   
                        $maxLoop = $k;
                    }
                 }
                 if($counter>0){
                     $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 193, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue())];
                    array_push($Arr,$dataslum);
                }
            }
            $barisketerangan+=17;
        }

        /* PENGUJIAN BERAT JENIS & PENYERAPAN AGREGAT HALUS*/
        $ArrKL = [];
        $slumBetonRows=401; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        // echo json_encode($ArrKL);exit;

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=408;
        for ($p=0; $p < 8 ; $p++) { 
            # code...
            $ket="density";
            if($p<3)
            {
                $ket="relatif";
            }elseif($p==3)
            {
                $ket="";
            }
            $maxLoop=0;
            for ($i=1; $i <=6 ; $i++) { 
                $valueKL=0;
                $counter=0;
                $colstart=$loopColumn+($i*4);
                # code...
                 for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                    if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                        $huruf = $this->numToAbjad($ArrKL[$k]);
                        $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                        $counter+=1;                   
                        $maxLoop = $k;
                    }
                 }
                     if($counter>0){
                         $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 194, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()." ".$ket)];
                        array_push($Arr,$dataslum);
                    }
                 
            }
            if($p==2){
                $barisketerangan+=2;
            }else{
            $barisketerangan+=1;                
            }
        }

        /*PENGUJIAN BERAT JENIS AGREGAT KASAR*/
        $ArrKL = [];
        $slumBetonRows=426; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }
        

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=432;
        for ($p=0; $p < 8 ; $p++) { 
            # code...
            $ket="density";
            if($p<3)
            {
                $ket="relatif";
            }elseif($p==3)
            {
                $ket="";
            }
            $maxLoop=0;
            for ($i=1; $i <=6 ; $i++) { 
                $valueKL=0;
                $counter=0;
                $colstart=$loopColumn+($i*4);
                # code...
                 for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                    if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                        $huruf = $this->numToAbjad($ArrKL[$k]);
                        $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                        $counter+=1;                   
                        $maxLoop = $k;
                    }
                 }
                     if($counter>0){
                         $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 195, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()." ".$ket)];
                        array_push($Arr,$dataslum);
                    }
                 
            }
            if($p==2){
                $barisketerangan+=2;
            }else{
            $barisketerangan+=1;                
            }
        }

        /*ANALISA AYAKAN AGREGAT HALUS DAN KASAR*/
        $barisketerangan=452;
        for ($p=0; $p < 8 ; $p++) { 
            # code...
            $maxLoop=9;
            for ($i=1; $i <=6 ; $i++) { 
                $huruf = $this->numToAbjad($maxLoop);
                $valueKL = $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                if($valueKL!=0 and $valueKL!=null and $valueKL!="")
                {
                    $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 196, 'NILAI' => round($valueKL ,2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()." halus")];
                    array_push($Arr,$dataslum);
                }
                $maxLoop+=4;
            }
            $barisketerangan+=1;
        }
        

        $barisketerangan=468;
        for ($p=0; $p < 7 ; $p++) { 
            # code...
            $maxLoop=9;
            for ($i=1; $i <=6 ; $i++) { 
                $huruf = $this->numToAbjad($maxLoop);
                $valueKL = $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                if($valueKL!=0 and $valueKL!=null and $valueKL!="")
                {
                    $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 196, 'NILAI' => round($valueKL ,2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()." kasar")];
                    array_push($Arr,$dataslum);
                }
                $maxLoop+=4;
            }
            $barisketerangan+=1;
        }


        /*UJI KADAR BUTIR HALUS LEBIH KECIL DARI 75 DALAM AGREGAT DENGAN PENCUCIAN*/
        $ArrKL = [];
        $slumBetonRows=486; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=489;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 197, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => strtolower($objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue())];
                array_push($Arr,$dataslum);
            }
        }


        /*UJI KADAR ORGANIK PADA AGREGAT HALUS*/
        $ArrKL = [];
        $slumBetonRows=500; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=501;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 198, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => $objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()];
                array_push($Arr,$dataslum);
            }
        }

         /*PENGUJIAN TOTAL KADAR AIR AGREGAT YANG DAPAT MENGUAP DENGAN PENGERINGAN*/
        $ArrKL = [];
        $slumBetonRows=512; /* Rown Start Check KUAT LENTUR*/
        for ($j = 6; $j < $maxColumn; $j++) {
            # code...
            $abjad = $this->numToAbjad($j);
            $value = $objWorksheet->getCell("{$abjad}{$slumBetonRows}")->getCalculatedValue();
            if ($value == 'v' or $value == 'V') {
                array_push($ArrKL, $j);
            }
        }

        $maxLoop=0;
        $loopColumn=6;
        $barisketerangan=516;

        for ($i=1; $i <=6 ; $i++) { 
            $valueKL=0;
            $counter=0;
            $colstart=$loopColumn+($i*4);
            # code...
             for ($k = $maxLoop; $k < count($ArrKL); $k++) {
                if ($ArrKL[$k] < $colstart && $ArrKL[$k] >= ($colstart-4) ) {
                    $huruf = $this->numToAbjad($ArrKL[$k]);
                    $valueKL+= $objWorksheet->getCell("{$huruf}{$barisketerangan}")->getCalculatedValue();
                    $counter+=1;                   
                    $maxLoop = $k;
                }
             }
             if($counter>0){
                 $dataslum = ['ID_TRANSAKSI'=>$id_transaksi,'ID_KATEGORI'=>$id_kategori,'ID_USER'=>$id_user,'ID_UJI' => 199, 'NILAI' => round(($valueKL / $counter),2), 'SAMPEL' => ($kali+$i), 'KETERANGAN' => $objWorksheet->getCell("D{$barisketerangan}")->getCalculatedValue()];
                array_push($Arr,$dataslum);
            }
        }

        
      
        
        // echo json_encode($Arr);exit;

        if(empty($Arr))
        {
            echo json_encode(array('notif' => '0', 'message' => 'Tidak Ada Data Yang Disimpan'));exit;
        }

        $data=$this->M_analis->saveUpload($Arr,$id_transaksi,$id_kategori,$countupload);
        
        if($data){

            // $destination_folder = 'assets/uploadexcel';
            // $new_name_file = $id_transaksi."-".$id_kategori.".xlsx";
            // $file = "$destination_folder/$new_name_file";
            // unlink($file);
            // // insert Data base
            // move_uploaded_file($_FILES['file_input']['tmp_name'], $file);
            
            echo json_encode(array('notif' => '1', 'message' => 'Save Data Upload Berhasil'));
        }else{
            echo json_encode(array('notif' => '0', 'message' => 'Save Data Upload Gagal'));
        }
    }

    // public function UploadTemplate2()
     private function UploadTemplate2()
    {
        // if(isset($_FILES["file_input"]) && $_FILES["file_input"]["name"] != "") {
        require_once APPPATH . "/third_party/PHPExcel.php";

        $objReader     = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel   = $objReader->load($_FILES["file_input"]["tmp_name"]);
        $objWorksheet  = $objPHPExcel->getActiveSheet();
        $highestRow    = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        // WHERE
        $id_transaksi = $_POST['id_transaksi_upload'];
        $id_kategori  = $_POST['kategori_upload'];
        $countupload  = $_POST['countupload'];
        $id_user      = $this->session->userdata("USER")->ID_USER; 
        $tambah = 13;    
        
        //HAPUS upload yg SUDAH ADA
        if($countupload=='1'){
            $this->M_analis->deleteUpload($id_transaksi, $id_kategori);
            $tambah = 1;    
        }

        if ($id_kategori == '1') {
            // SIO2
            $data = '';   $kolom = array('G', 'J', 'M', 'P', 'S', 'V');
            $baris = array('22', '30');
            $i     = 0; 
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '10';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++; 
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ; 
            // R2O3
            $baris = array('49', '55');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '12';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // Fe2O3
            $baris = array('70', '76');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '11';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // P2O5
            $baris = array('90', '96');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '54';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // TiO2
            $baris = array('105', '111');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '55';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // Al2O3
            $baris = array('120', '126');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '20';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // CaO
            $baris = array('137', '143');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                  
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '13';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // MgO
            $baris = array('162', '168');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                  
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '14';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // SO3
            $baris = array('183', '190');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                  
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '15';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // LOI
            $baris = array('208', '217');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '24';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // INSOLUBLE
            $baris = array('229', '236');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '22';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // FREE LIME
            $baris = array('253', '259');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                  
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '23';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // K2O
            $baris = array('273', '279');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '21';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // Na2O
            $baris = array('288', '294');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '53';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // Cl
            $baris = array('305', '311');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) { 
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '77';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // S2
            $baris = array('327', '333');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '78';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // Kadar Air
            $baris = array('345', '354');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '79';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;

                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;

            // Save T_APPROV
            $q = $this->M_analis->upload($id_transaksi, 'a3', $id_kategori);

            ////////////// SELESAI KIMIA   \\\\\\\\\\\\\

        } else if ($id_kategori == '2') {
            // KUAT TEKAN 1 Hari
            // $data = '';   $kolom = array('H','N','T','Z','AF','AL');
            // $baris = array('21','29');
            // $i = 0;
            // foreach($baris as $b){
            // foreach($kolom as $k){
            // $data[$i]['NILAI'] = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
            // $data[$i]['ID_TRANSAKSI'] =  $id_transaksi;
            // $data[$i]['ID_UJI'] = '10';
            // $data[$i]['SAMPEL'] = $i+1;
            // $data[$i]['ID_USER'] = $id_user;
            // $i++;
            // }
            // }
            // KUAT TEKAN 3 Hari
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('22', '30');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '5';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data));
            // KUAT TEKAN 7 Hari
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('23', '31');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '6';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // KUAT TEKAN 28 Hari
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('24', '32');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '7';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;

            // PENENTUAN WAKTU PENGIKATAN SEMEN DENGAN JARUM VICAT AWAL
            $data = '';   $kolom = array('E', 'K', 'Q', 'W', 'AC', 'AI');
            $baris = array('54', '74');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '3';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // PENENTUAN WAKTU PENGIKATAN SEMEN DENGAN JARUM VICAT AKHIR
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('54', '74');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '80';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            // PENENTUAN PEMUAIAN AUTOCLAVE DAN KEKEKALAN BENTUK DENGAN LE-CHATELIAR

            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('92', '106');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '4';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN CEPAT KAKU SEMEN (FALSE SET)
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('127', '139');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '8';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            } 
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;

            //PENENTUAN KEHALUSAN SEMEN DENGAN ALAT BLAINE

            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('153', '171');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '1';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN KEHALUSAN SEMEN DENGAN ALAT MESH 325
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('180', '186');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '51';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN KELECAKAN/ALIRAN MORTAR SEMEN
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('197', '206');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '84';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN KAPASITAS PANAS ALAT KALORIMETER
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('226', '262');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '85';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN PANAS HIDRASI SEMEN 7 Hari
            $data = '';   $kolom = array('F', 'L', 'R', 'X', 'AD', 'AJ');
            $baris = array('307', '342');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '82';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN PANAS HIDRASI SEMEN 28 Hari
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('307', '342');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '83';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN KANDUNGAN UDARA MORTAR SEMEN
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('373', '384');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '86';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENENTUAN DAYA SIMPAN AIR
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('393');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '101';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENGUJIAN KETAHANAN SULFAT 28 hARI
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('403', '411');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '102';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENGUJIAN KETAHANAN SULFAT 90 hARI
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('404', '412');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                    
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '103';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENGUJIAN KETAHANAN SULFAT 180 hARI
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('405', '413');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '121';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENGUJIAN KETAHANAN SULFAT 365 hARI
            $data = '';   $kolom = array('H', 'N', 'T', 'Z', 'AF', 'AL');
            $baris = array('406', '414');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '122';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }
            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;
            //PENGUJIAN SUSUT MUAI TERHADAP SULFAT
            $data = '';   $kolom = array('D', 'J', 'P', 'V', 'AB', 'AH');
            $baris = array('423', '431');
            $i     = 0;
            foreach ($baris as $b) {
                foreach ($kolom as $k) {
                   
                    $nilai = $objWorksheet->getCell("{$k}{$b}")->getCalculatedValue();
                    if(is_numeric($nilai)){
                        $data[$i]['NILAI']        = $nilai;
                        $data[$i]['ID_TRANSAKSI'] = $id_transaksi;
                        $data[$i]['ID_UJI']       = '141';
                        $data[$i]['SAMPEL']       = $i + $tambah;
                        $data[$i]['ID_KATEGORI']  = $id_kategori;
                        $data[$i]['ID_USER']      = $id_user;
                        $i++;
                    }
                }
            }

            (empty($data) ? '' : $this->M_analis->saveFromExcel('ELAB_T_UPLOAD', $data)); ;

            // Save T_APPROV
            $q = $this->M_analis->upload($id_transaksi, 'a3', $id_kategori);

        }
        // $q = $this->M_analis->saveFromExcel($data);

        // if($q) {
        echo json_encode(array('notif' => '1', 'message' => 'Berhasil Upload'));
        // } else {
        // echo "Gagal menyimpan data.";
        // }
    }

    private function numToAbjad($number)
    {
        $number = intval($number);
        if ($number <= 0) {
            return '';
        }
        $alphabet = '';
        while ($number != 0) {
            $p        = ($number - 1) % 26;
            $number   = intval(($number - $p) / 26);
            $alphabet = chr(65 + $p) . $alphabet;
        }
        return $alphabet;
    }

    private function abjadToNum($string)
    {
        $string = strtoupper($string);
        $length = strlen($string);
        $number = 0;
        $level  = 1;
        while ($length >= $level) {
            $char = $string[$length - $level];
            $c    = ord($char) - 64;
            $number += $c * (26 ** ($level - 1));
            $level++;
        }
        return $number;
    }
}
