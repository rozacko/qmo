
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spuc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib'); 
		$this->load->library('FPDF'); 
		$this->load->model('master/D_bppuc','model');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "BAPPUC / SPUC";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/beban', $data);
	}


	 public function bappcu_pdf($id)
    { 
		$dtl = $this->model->getBppuc($id);
		$pdf = new FPDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->Image('assets/image/logo_sgja.jpg',10,10,-150); 
		//$pdf->addJpegFromFile('images/logo_kan.jpg',45,80,125,60) ;
		// $pdf->SetLineWidth(1);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10);
		$pdf->Cell(150,'5',"PT SEMEN INDONESIA (PERSERO) Tbk. ", 0, 0);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,'5',"F/PPU - 13/04", 0, 1);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10);
		$pdf->Cell(75,'5',"DEPARTEMEN LITBANG PRODUK & APLIKASI ", 0, 1);


		// $pdf->Rect(40,10,520,770);
		$pdf->Ln(20);
		$pdf->SetFont('Arial','',16);
		$pdf->Cell(190,5,"BERITA ACARA PEMERIKSAAN & PENERIMAAN", 0, 1,'C');
		$pdf->Cell(190,5,"CONTOH UJI", 0, 1,'C');

		$pdf->Ln(20);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',14);
		$pdf->Cell(75,'8',"NAMA CONTOH", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->NAMA_CONTOH, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"NO. SPUC", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->NO_BAPPUC, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"TANGGAL PENERIMAAN", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->TANGGAL, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"KEMASAN CONTOH UJI", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->KEMASAN, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"WARNA CONTOH UJI", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->WARNA_CONTOH, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"BENTUK CONTOH UJI", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 0); 
		$pdf->Cell(5,'8',$dtl->BENTUK_CONTOH, 0, 1);
		$pdf->Cell(10);$pdf->Cell(75,'8',"KETERANGAN", 0, 0);
		$pdf->Cell(5,'8',":  ", 0, 1);  
		$pdf->Cell(10);$pdf->Cell(190,'15',"......................................................................................", 0, 1, 'C');
		

		$pdf->Ln(80);

		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10);$pdf->Cell(120,'8',"Diserahkan Oleh :", 0, 0);
		$pdf->Cell(30,'8',"Diterima oleh :", 0, 1);
		$pdf->Ln(30);


		$pdf->Cell(10);$pdf->Cell(120,'5',"(....................................)", 0, 0);
		$pdf->Cell(30,'5',"(....................................)", 0, 1);
		$pdf->Cell(10);$pdf->Cell(120,'5',"Nama dan tanda tangan,", 0, 0);
		$pdf->Cell(30,'5',"Nama dan tanda tangan,", 0, 1);  

		// $pdf->Rect(350,548,40,15);
		$pdf->Output();
    }


	 public function spuc_pdf($id)
    { 
		$master_spuc = $this->model->getSupcMain($id);
		$data_supc = $master_spuc->row(0);
		$jumlah_kategori = $master_spuc->num_rows();
		$pdf = new FPDF('P','mm','A4');
		$pdf->AddPage();
		$pdf->Image('assets/image/logo_sgja.jpg',10,10,-150); 
		//$pdf->addJpegFromFile('images/logo_kan.jpg',45,80,125,60) ;
		// $pdf->SetLineWidth(1);
		$pdf->SetFont('Arial','',12);
		$pdf->Cell(10);
		$pdf->Cell(150,'5',"PT SEMEN INDONESIA (PERSERO) Tbk. ", 0, 0);
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(30,'5',"F/PPU - 13/01", 0, 1);
		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10);
		$pdf->Cell(75,'5',"DEPARTEMEN LITBANG PRODUK & APLIKASI ", 0, 1);


		// $pdf->Rect(40,10,520,770);
		$pdf->Ln(20);
		$pdf->SetFont('Arial','U',16);
		$pdf->Cell(190,8,"SURAT PERINTAH UJI CONTOH 2018", 0, 1,'C');
		$pdf->SetFont('Arial','',16);
		$pdf->Cell(190,5,"(SPUC)", 0, 1,'C');

		$pdf->Ln(20);
		$pdf->Cell(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(50,'5',"No.", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"682/ISO/G/2018", 0, 1);
		$pdf->Cell(10);$pdf->Cell(50,'5',"Tanggal", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"08-May-2018", 0, 1);
		$pdf->Cell(10);$pdf->Cell(50,'5',"Jumlah Contoh", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"1", 0, 1);
		$pdf->Cell(10);$pdf->Cell(50,'5',"Jenis Contoh", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"Semen Portland Type I", 0, 1);
		$pdf->Cell(10);$pdf->Cell(50,'5',"Metode Uji", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"SNI 2049:2015", 0, 1);
		$pdf->Cell(10);$pdf->Cell(50,'5',"Batas Waktu Uji", 0, 0);
		$pdf->Cell(5,'5',":  ", 0, 0); 
		$pdf->Cell(5,'5',"00-00-0000 s/d 00-00-0000", 0, 1); 

		$pdf->Ln(10);

		$pdf->SetFont('Arial','',8);
		$pdf->Cell(10);$pdf->Cell(85,'8',"KIMIA", 1, 0, 'C');
		$pdf->Cell(85,'8',"FISIKA", 1, 1, 'C'); 
 

		$x = $pdf->GetX();
		$y = $pdf->GetY();

		$col1="SIo2\nAL203\nFE2O3\nCao\nMgO\nLOL\nFL\nInsol\nCl\nNa\nk2\n\n\n\n";
		$pdf->Cell(10);$pdf->MultiCell(170, 5, $col1, 1, 1);

		// $pdf->SetXY($x + 95, $y);

		// $col2="Kehalusan (Blaine)\nWaktu Pengikatan\nKekekalan Bentuk\nKuat Tekan\nKadar Udara\nPanas Hidrasi\nKetahanan Sulfat\nFalse Set\n\n                                             Manajer Teknik II\n\n\n\n                                          (Roganda Saragih ST)";
		// $pdf->MultiCell(85, 5, $col2, 1);  

 
		$pdf->Cell(10);$pdf->Cell(85,'5',"Keterangan", 1, 0);
		$pdf->Cell(50,'5',"Diteruskan kepada", 1,0);
		$pdf->Cell(35,'5',"Paraf", 1, 1);
		$pdf->Cell(10);
		$pdf->Cell(85,'30'," ", 1, 0);
		$pdf->Cell(50,'30',"Penyelia Fisika \nPenyelia Kimia", 1,0);
		$pdf->Cell(35,'30',"", 1, 1); 

		$pdf->Cell(10);$pdf->Cell(85,'15',"Kembali ke Administrasi....( dilampiri konsep hasil uji ) ", 1, 0);
		$pdf->Cell(25,'15',"Administrasi", 1,0);
		$pdf->Cell(25,'15'," ", 1,0);
		$pdf->Cell(35,'15',"", 1, 1); 

		// $pdf->Rect(350,548,40,15);
		$pdf->Output();
    }
  

}
