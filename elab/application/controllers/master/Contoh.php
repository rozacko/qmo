
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contoh extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_Contoh');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Contoh";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/contoh', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_Contoh->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshKelompok() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kelompok --</option>";
		$data = $this->M_Contoh->getKelompok();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KELOMPOK"]}'>".htmlspecialchars($p["NAMA_KELOMPOK"])."</option>";
		}
	} 
    
    
	public function simpan() {
		if(isset($_POST["nm_contoh"]) ){
			$column = array(
				'NAMA_CONTOH','ID_KELOMPOK' 
			);
			
			$data = array(
				"'".$_POST["nm_contoh"]."'", "'".$_POST["kelompok"]."'" 
			);
			 
			$q = $this->M_Contoh->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_contoh"]) 
		) {
			$id = intval($_POST["id"]);
			
			$column = array(
				'NAMA_CONTOH','ID_KELOMPOK' 
			);
			
			$data = array(
				"'".$_POST["nm_contoh"]."'", "'".$_POST["kelompok"]."'" 
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_Contoh->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_Contoh->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_Contoh->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
