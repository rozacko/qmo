
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standart extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_Standart');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Standart Uji";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/standart_uji', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_Standart->get_data();
		echo json_encode($detail_data);

    }
    
	public function refreshJenis() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kategori --</option>";
		$data = $this->M_Standart->getKategori();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KATEGORI"]}'>".htmlspecialchars($p["NM_KATEGORI"])."</option>";
		}
	}
    
    
	public function simpan() {
		if(isset($_POST["standart_uji"]) && isset($_POST["status"])){
			$column = array(
				'NAMA_STANDART','STATUS','ID_KATEGORI', 'KETERANGAN'
			);
			
			$data = array(
				"'".$_POST["standart_uji"]."'", "'".$_POST["status"]."'", "'".$_POST["jenis"]."'", "'".$_POST["keterangan"]."'",  
			);
			 
			$q = $this->M_Standart->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["standart_uji"]) && isset($_POST["status"])  
		) {
			$id = intval($_POST["id"]);
			
			$column = array(
				'NAMA_STANDART','STATUS','ID_KATEGORI', 'KETERANGAN'
			);
			
			$data = array(
				"'".$_POST["standart_uji"]."'", "'".$_POST["status"]."'", "'".$_POST["jenis"]."'", "'".$_POST["keterangan"]."'",  
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_Standart->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_Standart->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_Standart->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Standart Metode Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Status');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kategori'); 
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Keterangan'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Standar Uji');
  
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
            $status = ($v["STATUS"] == 'Y' ? 'ON' : 'OFF');
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NAMA_STANDART"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no,  $status);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NM_KATEGORI"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["KETERANGAN"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Standar_uji.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
