
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_m_kemasan extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/Model_m_kemasan');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		
		$data['title'] = "Halaman Utama";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/m_kemasan', $data);
	}

	public function ajax_list()
	{
		$data['data'] = $this->Model_m_kemasan->tampil();

		echo json_encode($data);
	}
	public function tambah(){
		// echo json_encode($_POST);exit;
		$data = $this->Model_m_kemasan->add($_POST);
		 if($data) {
			echo json_encode(array('notif' => '1', 'message' => 'Berhasil Menyimpan Data'));
		} else {
			echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
		}
		exit;
		// if (isset($_POST['tambah'])) {
		

		
		// $kode = $this->input->post('KODE_KEMASAN');
		// $nama = $this->input->post('NAMA_KEMASAN');

		// $data = array(
		// 			  'KODE_KEMASAN' =>$kode,
		// 			  'NAMA_KEMASAN' =>$nama);
		// $this->Model_m_kemasan->add($data);
		// redirect('master/c_m_kemasan');
		// }
		// $this->load->view('master/m_kemasan');
	}
	public function edit($id){
		$sql = $this->Model_m_kemasan->editData($id);
		echo json_encode($sql);
	}
	public function hapus(){
		$id = $this->input->post('id');
        $data = $this->Model_m_kemasan->hapusData($id);

        if($data) {
			echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
		} else {
			echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
		}
	}
	
	public function update(){
		$data = array('KODE_KEMASAN' =>$this->input->post('KODE_KEMASAN'),
					      'NAMA_KEMASAN' =>$this->input->post('NAMA_KEMASAN'));
						$where = array('ID_KEMASAN'=>$this->input->post('ID_KEMASAN'));

		$sql = $this->Model_m_kemasan->updateData($where, $data, 'ELAB_M_KEMASAN');		
		 if($sql) {
			echo json_encode(array('notif' => '1', 'message' => 'Berhasil Update Data'));
		} else {
			echo json_encode(array('notif' => '0', 'message' => 'Gagal Update data.'));
		}
		exit;

		// if(isset($_POST['update'])){
		// 	$id = $this->input->post('ID_KEMASAN');
		// 	$kode = $this->input->post('KODE_KEMASAN');
		// 	$nama = $this->input->post('NAMA_KEMASAN');
		// 	$data = array('KODE_KEMASAN' =>$kode,
		// 			      'NAMA_KEMASAN' =>$nama);
		// 				$where = array('ID_KEMASAN'=>$id);
		// 				$sql = $this->Model_m_kemasan->updateData($where, $data, 'ELAB_M_KEMASAN');
		// 				redirect('master/c_m_kemasan');
		// 				}
					
		// $this->load->view('master/m_kemasan');
		
	}
}
