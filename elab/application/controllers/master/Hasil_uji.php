
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hasil_uji extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('M_hasiluji');
        $this->load->model('approval/M_analis');
    }
    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        date_default_timezone_set('Asia/Jakarta');
        $data['title'] = "Hasil Pengujian";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('hasil_uji', $data);
    }

    public function get_data()
    {
        $sess_lab      = $this->session->userdata("USERLAB");
        $session_labor = '';
        $session_kate  = '';

            $getdata = array();
        foreach ($sess_lab as $v) {
            $on          = "'a4', 'a5'";
            $detail_data = $this->M_hasiluji->get_data($v['ID_LAB'], $on, $v['ID_KATEGORI']);
            // echo json_encode($detail_data);
            foreach ($detail_data as $val) {
                $cek_kategori = $this->M_hasiluji->cek_kategori($val['ID_TRANSAKSI'], $on);
                $lhu          = "<center>";
                foreach ($cek_kategori as $k) {
                    $lhu .= "<button onclick='lhu({$val['ID_TRANSAKSI']}, {$k['ID_KATEGORI']})' title='LHU {$k['NM_KATEGORI']}' class='btn btn-success btn-xs waves-effect btn_edit'  ><span class='btn-labelx'><i class='fa fa-download'></i></span></button>&nbsp;&nbsp;";
                }$lhu .= "</center>";
                $data['SPUC']         = '--';
                $data['NO_BAPPUC']    = $val['NO_BAPPUC'];
                $data['TANGGAL']      = $val['TANGGAL'];
                $data['NAMA_CONTOH']  = $val['NAMA_CONTOH'];
                $data['ID_TRANSAKSI'] = $val['ID_TRANSAKSI'];
                $data['ID_KATEGORI']  = $v['ID_KATEGORI'];
                $data['LHU']          = $lhu;
                $data['SHU']          = "<center><button onclick='shu({$val['ID_TRANSAKSI']},{$v['ID_KATEGORI']})'  title='SHU {$val['NAMA_CONTOH']}'  class='btn btn-primary btn-xs waves-effect btn_edit' ><span class='btn-labelx'><i class='fa fa-download'></i></span> </button></center>";
                $getdata[]            = $data;
            }
        }
        echo json_encode($getdata);

    }

    public function lhu()
    {
        require_once APPPATH."/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6
        
        // echo json_encode($datas);exit;
        $id_transaksi = $_GET['id_transaksi'];
        $id_kategori  = $_GET['id_kategori'];
        $param        = $this->M_hasiluji->get_parameter($id_transaksi, $id_kategori);
        $paramArr=[];
        // echo json_encode($paramArr);exit;
        $datas['header']      = $this->M_hasiluji->get_header_shu($id_transaksi);
        $datas['title']        = strtoupper($this->M_hasiluji->get_title($id_kategori)['NM_KATEGORI']);

        $approve['ELAB_T_APPROVE.ID_TRANSAKSI']=$id_transaksi;
        $approve['ELAB_T_APPROVE.STATUS']='a4';
        $approve['ELAB_T_APPROVE.ID_KATEGORI']=$id_kategori;
        $datas['approveArr']      = $this->M_hasiluji->approve_users($approve);
        $datas['barcode']="https://api.qrserver.com/v1/create-qr-code/?data=username:".$datas['approveArr']['USERNAME']."%20tanggal:".$datas['approveArr']['TANGGAL']."&amp;size=50x50";

        foreach ($param as $value) {
            array_push($paramArr, $value['ID_UJI']);
        }
        
        $datas['param']=$paramArr;
        $where=['ID_TRANSAKSI'=>$_GET['id_transaksi'],'ID_KATEGORI'=>$_GET['id_kategori']];
       

       
        if($id_kategori==3){
            
            $datas['data']=$this->M_hasiluji->lhu($where,$paramArr);
            $datas['print']=$this->buildArr($datas['data']);
            // echo json_encode($datas['print']);exit;

            $html=$this->load->view('printpdf/lhubeton',['data'=>$datas],true);
            
        }elseif ($id_kategori==5) {
            # code...
            
            $datas['data']=$this->M_hasiluji->lhu($where);
            $html=$this->load->view('printpdf/lhulimbah',['data'=>$datas],true);
        }
        $mpdf->WriteHTML($html);
        $mpdf->Output();


    }

    public function lhu_old()
    { 
 
        ini_set('max_execution_time', 0);
        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6 
        $mpdf = new mPDF(); // mpdf6
        // $mpdf->Image('assets/image/logo_sgja.jpg', 0, 0, 210, 297, 'jpg', '', true, false);
        //Header

        $id_transaksi = $_GET['id_transaksi'];
        $id_kategori  = $_GET['id_kategori'];
        $title        = strtoupper($this->M_hasiluji->get_title($id_kategori)['NM_KATEGORI']);
        $lab          = strtoupper($this->M_hasiluji->get_lab($id_transaksi)['NM_LAB']);
        $param        = $this->M_hasiluji->get_parameter($id_transaksi, $id_kategori);
        $header       = $this->M_hasiluji->get_header($id_transaksi, 'a3', $id_kategori);
        $uji          = $this->M_hasiluji->get_metode($id_transaksi);

        $approve['ELAB_T_APPROVE.ID_TRANSAKSI']=$id_transaksi;
        $approve['ELAB_T_APPROVE.STATUS']='a4';
        $approve['ELAB_T_APPROVE.ID_KATEGORI']=$id_kategori;
        $approveArr       = $this->M_hasiluji->approve_users($approve);

        // echo json_encode($approveArr['USERNAME']);exit;
        $lembar['1']          = '';
        $lembar['2']          = '';
        $urut         = 1;
        foreach ($param as $v) {
            $upload = $this->M_hasiluji->get_nilai($v['ID_TRANSAKSI'], $v['ID_UJI']); 
            if(count($upload) <= 12){
                $kali = 1; 
            }else if(count($upload) >= 12){
                $kali = 2; 
            } 
            for($i=1;$i<=$kali;$i++){ 
                if($i==1){ 
                    $start = 1;
                    $mid = 7;
                    $end = 12;
                }else{ 
                    $start = 13;
                    $mid = 19;
                    $end = 24;
                }
                 $lembar["{$i}"] .= "<table width='100%' border = '1'> <tr><th width='5%' rowspan='2' >{$urut}</th>";
                if (count($upload) > 6) {
                    $lembar["{$i}"] .= "<th rowspan='2' width='31%' align='left'>{$v["NAMA_UJI"]}</th>";
                    $lembar["{$i}"] .= "<th rowspan='2' width='10%'>{$v["SATUAN"]}</th>";
                } else {
                    $lembar["{$i}"] .= "<th  width='35%'>{$v["NAMA_UJI"]}</th>";
                    $lembar["{$i}"] .= "<th width='10%'>{$v["SATUAN"]}</th>";
                }
                $no   = 1;
                $isi1 = '';
                
                $cek_param = $this->M_analis->get_cek_parameter($v['ID_TRANSAKSI'], $v['ID_UJI'], $start, $end);
                foreach ($cek_param as $val) {
                    if ($no < 7) {
                        $lembar["{$i}"] .= "<th  width='9%' align='right'>{$val["NILAI"]}</th>";
                    } else {
                        $isi1 .= "<th  width='9%'  align='right'>{$val["NILAI"]}</th>";
                    }
                    $no += 1;
                }
                if (count($cek_param) > 6) {
                    $lembar["{$i}"] .= "<tr>";
                    $lembar["{$i}"] .= $isi1;
                    $lembar["{$i}"] .= "</tr>";
                }

                $lembar["{$i}"] .= "</tr></table>"; 
            
            }
               
                $urut += 1;
        }
        
        // echo print_r($lembar["2"]);exit();
         
        $html = '';
        $tabel = '';
         for($i=1;$i<=$kali;$i++){
            $tabel =  $lembar["{$i}"]; 
                $add_halaman = ($i==1 ? "<pagebreak />" : '');
        $html .= "<style>
                th {
                  border: 1px solid #006;
                  font-weight  : none;
                }
                table {
                      border-collapse: collapse;
                      font-size : 10px;
                      
                    }

                </style>
               <table width='100%'  border='0'>
                    <tr>
                        <td rowspan='2' width='10%'> <img src='assets/image/logo_sgja.jpg'/></td> <td  width='55%' style='font-size:11px'>PT SEMEN INDONESIA (PERSERO) Tbk.</td>   <td  width='35%' style='font-size:9px;text-align:right'>F/5062/006   </td>
                    </tr>
                    <tr>
                        <td style='font-size:11px'>DEPTARTEMEN LITBANG PRODUK & APLIKASI</td>   <td style='font-size:9px;text-align:right'>Edisi : 1, Revisi : 1, Tanggal : 13-06-2002</td>
                    </tr>
                </table>
                 <table width='100%'>
                    <tr>     <td  width='100%' style='text-align:center;font-size:20px;font-weight:bold'>PENGUJIAN {$title}</td></tr>
                 </table>
                <table width='100%' border = '0'>
                     <tr>     <td width='30%'>Nama Contoh </td> <td width='3%'>: </td> <td width='60%'>{$header['NAMA_CONTOH']} </td>    </tr>
                     <tr>     <td width='30%'>Jumlah Contoh </td> <td width='3%'>: </td> <td width='60%'>{$header['JUMLAH']} </td>    </tr>
                     <tr>     <td width='30%'>Diterima tanggal</td> <td width='3%'>: </td> <td width='60%'>{$header['TANGGAL']} </td>    </tr>
                     <tr>     <td width='30%'>Dikerjakan tanggal </td> <td width='3%'>: </td> <td width='60%'>{$header['TGL_AWAL']} </td>    </tr>
                     <tr>     <td width='30%'>SPUC </td> <td width='3%'>: </td> <td width='60%'>{$header['NO_BAPPUC']} </td>    </tr>
                     <tr>     <td width='30%'>Metode Uji </td> <td width='3%'>: </td> <td width='60%'>{$uji['NAMA_STANDART']}</td>    </tr>
                </table>
                <br />
                 <table width='100%' border = '1'>
                     <tr>     <th  rowspan='3' align='center'  width='5%'>No </th> <th rowspan='3' width='31%' align='center' >ITEM PENGUJIAN </th> <th width='10%' rowspan='3' align='center' >SATUAN </th> <th colspan='6' width='50%' align='center' >KODE CONTOH </th></tr>
                     <tr>     <th width='9%' >1</th><th width='9%' >2</th><th width='9%' >3</th><th width='9%' >4</th><th width='9%' >5</th><th width='9%' >6</th></tr>
                     <tr>     <th width='9%' >7</th><th width='9%' >8</th><th width='9%' >9</th><th width='9%' >10</th><th width='9%' >11</th><th width='9%' >12</th></tr>
                 </table>
                 {$tabel}

                 <br />
                 <table width='100%'>
                     <tr>     <td width='75%' ></td><td width='25%' >Gresik, </td> </tr>
                     <tr>     <td width='85%' ></td><td width='25%' >{$lab} </td> </tr>
                     <tr>     <td width='85%' ></td><td width='25%' ><img src='https://api.qrserver.com/v1/create-qr-code/?data=username:{$approveArr['USERNAME']}%20tanggal:{$approveArr['TANGGAL']}&amp;size=100x100' alt='' title='' /><br>
                     {$approveArr['FULLNAME']}</td> </tr>
                 </table><br><br>
                 <table width='100%'>
                    <tr>     <td  width='100%'></td></tr>
                 </table>
                 {$add_halaman}

         ";
         }

        $mpdf->WriteHTML($html);
        $mpdf->Output();
        // exit;
    }

    public function preview_excel()
    {
        ini_set('max_execution_time', 0);
        require_once APPPATH . "/third_party/PHPExcel.php";
        require_once APPPATH . "/third_party/PHPExcel/IOFactory.php";
        $inputFileType = 'Excel2007';
        $inputFileName = 'assets/template/analis_kimia.xlsx';

        $objReader   = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($inputFileName);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
        $objWriter->save('php://output');
        exit;
    }

    public function newshu()
    {
        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF(); // mpdf6

        $id_transaksi = $_GET['id_transaksi'];
        $header       = $this->M_hasiluji->get_header_shu($id_transaksi);
        $kategori     = $this->M_hasiluji->get_shu_kategori($id_transaksi);
        $param        = $this->M_hasiluji->get_shu($id_transaksi);

        $approve['ELAB_T_APPROVE.ID_TRANSAKSI']=$id_transaksi;
        $approve['ELAB_T_APPROVE.STATUS']='a5';
        $approve['ELAB_T_APPROVE.ID_KATEGORI']=$id_kategori;
        $approveArr       = $this->M_hasiluji->approve_users($approve);

        echo json_encode($approveArr);
    }

    public function shu()
    {

        require_once APPPATH . "/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF(); // mpdf6

        $id_transaksi = $_GET['id_transaksi'];
        $header       = $this->M_hasiluji->get_header_shu($id_transaksi);
        if($header['KODE_UJI']!='ISO'){
             $get_ldap     = $this->M_hasiluji->view_peminta($header['PEMINTA']);
             $header['PEMINTA'] = $get_ldap['mk_nama'];
        }
        $kategori     = $this->M_hasiluji->get_shu_kategori($id_transaksi);
        $param        = $this->M_hasiluji->get_shu($id_transaksi);
        $get_standart        = $this->M_hasiluji->get_standart($id_transaksi);
        $acuan = ''; 
        foreach($get_standart as $s){
             $acuan .= $s['NAMA_STANDART'].' dan ';
        }
        $acuan = rtrim($acuan,"dan ");
        $get_pengujian        = $this->M_hasiluji->get_pengujian($id_transaksi);
        $pengujian = ''; 
        foreach($get_pengujian as $p){
             $pengujian .= $p['AWAL'].' dan ';
        }
        $pengujian = rtrim($pengujian,"dan ");
        $numbering = $this->M_hasiluji->numbering($id_transaksi);
        $urut       = str_pad($numbering['NUMBERING'], 3, '0', STR_PAD_LEFT);

        $approve['ELAB_T_APPROVE.ID_TRANSAKSI']=$id_transaksi;
        $approve['ELAB_T_APPROVE.STATUS']='a5';
        $approve['ELAB_T_APPROVE.ID_KATEGORI']=$id_kategori;
        $approveArr       = $this->M_hasiluji->approve_users($approve);

        $isi          = '';
        $urut         = 1;
        $isi          = " ";
        // foreach ($kategori as $v) {
            // $nm_kategori = strtoupper($v['NM_KATEGORI']);
            // $isi .= "<tr><td style='font-weight:bold'>PENGUJIAN {$nm_kategori}</td><td></td><td></td><td></td><td></td></tr>";
            // foreach ($param as $val) {
                // if ($v['ID_KATEGORI'] == $val['ID_KATEGORI']) {
                    // $isi .= "<tr><td>{$val['NAMA_UJI']}   ( {$val['SIMBOL']} )</td><td>{$val['SATUAN']}</td><td>-</td><td></td><td> butir 7.1.3.2</td></tr>";
                // }
            // }
        // }
        
        $lembar['1']          = '';
        $lembar['2']          = '';
        $number = 1;
       foreach ($kategori as $v) {
                $nm_kategori = strtoupper($v['NM_KATEGORI']);
                $lembar["1"] .= "<tr><td style='font-weight:bold'>PENGUJIAN {$nm_kategori}</td><td></td><td></td><td colspan='6'></td><td></td></tr>";
                $lembar["2"] .= "<tr><td style='font-weight:bold'>PENGUJIAN {$nm_kategori}</td><td></td><td></td><td colspan='6'></td><td></td></tr>";
                $urut         = 1;
                foreach ($param as $val) { 
                    if ($v['ID_KATEGORI'] == $val['ID_KATEGORI']) {
                        // $isi .= "<tr><td>{$val['NAMA_UJI']}   ( {$val['SIMBOL']} )</td><td>{$val['SATUAN']}</td><td>-</td><td></td><td> butir 7.1.3.2</td></tr>";
                  
                            $upload = $this->M_hasiluji->get_nilai($id_transaksi, $val['ID_UJI']); 
                            if(count($upload) <= 12){
                                $kali = 1; 
                            }else if(count($upload) >= 12){
                                $kali = 2;  
                            } 
                            for($i=1;$i<=$kali;$i++){ 
                            if($i==1){ 
                                $start = 1;
                                $mid = 7;
                                $end = 12;
                            }else{ 
                                $start = 13;
                                $mid = 19;
                                $end = 24;
                            }
                                if (count($upload) > 6) {
                                    $lembar["{$i}"] .= "<tr><td rowspan='2' width='31%'>{$val["NAMA_UJI"]} ({$val["SIMBOL"]})</td>";
                                    $lembar["{$i}"] .= "<td rowspan='2' width='5%'>{$val["SATUAN"]}</td>";
                                    $lembar["{$i}"] .= "<td rowspan='2' width='10%'>standar</td>"; 
                                } else {
                                    $lembar["{$i}"] .= "<tr><td  width='35%'>{$val["NAMA_UJI"]} ({$val["SIMBOL"]})</td>";
                                    $lembar["{$i}"] .= "<td width='5%'>{$val["SATUAN"]}</td>";
                                    $lembar["{$i}"] .= "<td width='10%'>standar</td>"; 
                                }
                                $no   = 1;
                                $isi1 = '';
                                
                                
                                $cek_param = $this->M_analis->get_cek_parameter($id_transaksi, $val['ID_UJI'], $start, $end); 
                                
                                foreach ($cek_param as $u) {
                                    if ($no < 7) {
                                        $lembar["{$i}"] .= "<td  width='9%' >{$u["NILAI"]}</td>";
                                    } else {
                                        $isi1 .= "<td  width='9%' >{$u["NILAI"]}</td>";
                                    }
                                    $no += 1;
                                }
                                if (count($cek_param) > 6) {
                                    
                                    $lembar["{$i}"] .= "<td rowspan='2' width='10%' >metode</td>";
                                    $lembar["{$i}"] .= "<tr>";
                                    $lembar["{$i}"] .= $isi1;
                                    $lembar["{$i}"] .= "</tr>";
                                }else{
                                    $lembar["{$i}"] .= "<td width='10%' >metode</td>";
                                }
 
                                $lembar["{$i}"] .= "</tr> "; 
                            
                                $urut += 1;
                            }
                         }
                } 
                $number +=1;
        }

        $html = "<style>
                    
                </style>
               <table width='100%'>
                    <tr>
                        <td rowspan='4' width='20%'> <img width='20%' height='15%' src='assets/image/logo_sgja.jpg'/></td> <td align='center'  width='80%' style='font-size:15px;font-weight:bold'>{$header['NM_LAB']}</td>
                    </tr>
                    <tr><td align='center'   style='font-size:12px'>PT SEMEN INDONESIA (PERSERO) Tbk.</td> </tr>
                    <tr><td  align='center'  style='font-size:12px;'>Jl. Veteran, Gresik 61122, JAWA TIMUR-INDONESIA</td></tr>
                    <tr>
                        <td align='center' style='font-size:12px;'  > Telp.(031) 3981731-3  Fax. (031) 3983209, 3972264</td>
                    </tr>
                </table>
                  <table width='100%'>
                    <tr> <td align='right' style='font-size:8px;'  >F/PPU - 15/01 </td> </tr>
                    <tr> <td align='center'  style='text-decoration: underline;font-weight:bold'>LAPORAN PENGUJIAN  </td> </tr>
                    <tr> <td align='center' style='font-weight:bold'>SEMEN </td> </tr>
                </table>
                 <table width='100%' >
                    <tr>   
                        <td width='35%'>NO. LAPORAN </td>  
                        <td width='5%'> : </td>  <td width='60%'>{$urut}/PP.02.01/23300000/{$numbering['TANGGAL']}  </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>JUMLAH/BAHAN </td>  
                        <td width='5%'> : </td>  <td width='60%'>{$header['JUMLAH']}  </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>KETERANGAN  </td>  <td width='5%'> : </td>  
                        <td width='60%'>{$header['KETERANGAN']} </td> 
                    </tr>
                    <tr> 
                        <td width='35%'> </td>  
                        <td width='5%'>   </td>  
                        <td width='60%'>No. SPUC  :   {$header['NO_BAPPUC']}</td> 
                    </tr>
                    <tr> 
                        <td width='35%'>  </td>  
                        <td width='5%'>   </td>  
                        <td width='60%'>No. SPK :  {$header['NO_SPK']}</td>
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>KEMASAN  </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$header['KEMASAN']}  </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>DIBUAT UNTUK  </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$header['NAMA_INSTANSI']}  </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>ALAMAT  </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$header['ALAMAT']}  </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>DITERIMA TANGGAL </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$header['TANGGAL']}  </td> 
                    </tr> <br />
                    <tr> 
                        <td width='35%'>TANGGAL PENGUJIAN  </td>  
                        <td width='5%'> : </td> 
                        <td width='60%'>{$pengujian} </td> 
                    </tr>  <br />
                    <tr> 
                        <td width='35%'>STANDAR ACUAN  </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$acuan}  </td> 
                    </tr>   <br /> 
                    <tr> 
                        <td width='35%'>DITERBITKAN TANGGAL</td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>{$numbering['TERBIT']}  </td>
                    </tr><br />  
                    <tr> 
                        <td width='35%'>HASIL PENGUJIAN  </td>  
                        <td width='5%'> : </td>  
                        <td width='60%'>Terlampir  </td> 
                    </tr>   
                    
                    </table><br><br>
                    <table width='100%'>
                    <tr> <td width='35%'> </td>  <td width='5%'>  </td>  <td width='60%' align='center'>Laboratory Management Officer  </td> </tr> 
                    <tr> <td width='35%'>  </td>  <td width='5%'>  </td>  <td width='60%' align='center'>";
                       
                     if(count($approveArr)!=0)
                     {
                         $html.="<img src='https://api.qrserver.com/v1/create-qr-code/?data=username:{$approveArr['USERNAME']}%20tanggal:{$approveArr['TANGGAL']}&amp;size=100x100' alt='' title='' /><br>
                     {$approveArr['FULLNAME']} ";  
                     }
                     $html.="</td> </tr>
                </table>


                 <table width='100%'>
                     <tr>     <td style='font-style: italic;'>*) tidak termasuk ruang lingkup akreditasi. </td> </tr>
                     <tr>     <td >Kesimpulan    :   Hasil Uji memenuhi standar  </td> </tr>
                 </table>
                 <table width='100%' border='1'>
                    <tr>
                        <td rowspan='4' width='20%'> <img  width='20%' height='15%' src='assets/image/kan.png'/></td> <td  width='80%' style='font-size:12px;font-weight:bold'> KETERANGAN </td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> -  Laboratorium ini diakreditasi oleh Komite Akreditasi Nasional (KAN) No. LP-151-IDN</td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> -  Pengujian yang dilaporkan telah dilaksanakan sesuai dengan persyaratan registrasi </td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'>-  Hasil Pengujian ini tidak untuk diumumkan dan hanya berlaku untuk contoh yang bersangkutan </td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> Laboratorium Penguji </td>
                        <td style='font-size:9px;'> -  Laboratorium ini diakreditasi oleh Komite Akreditasi Nasional (KAN) No. LP-151-IDN</td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> {$header['KD_LAB']}  </td>
                        <td> </td>
                 </table>
                  <table width='100%'>
                    <tr><td>Catatan dari Prosedur No : P/PPU/15 </td>
                    </tr>
                   </table>
                   <pagebreak />
         ";
 
 
        $tabel = '';
         for($i=1;$i<=$kali;$i++){
            $tabel =  $lembar["{$i}"];
                $add_halaman = ($i==2 ? "<pagebreak />" : '');
        $html .= "{$add_halaman}<style>
                    table {
                      border-collapse: collapse;
                    }

                   
                </style>
               <table width='100%'>
                    <tr>
                        <td rowspan='4' width='20%'> <img width='20%' height='15%' src='assets/image/logo_sgja.jpg'/></td> <td align='center'  width='80%' style='font-size:15px;font-weight:bold'>{$header['NM_LAB']}</td>
                    </tr>
                    <tr><td align='center'   style='font-size:12px'>PT SEMEN INDONESIA (PERSERO) Tbk.</td> </tr>
                    <tr><td  align='center'  style='font-size:12px;'>Jl. Veteran, Gresik 61122, JAWA TIMUR-INDONESIA</td></tr>
                    <tr>
                        <td align='center' style='font-size:12px;'  > Telp.(031) 3981731-3  Fax. (031) 3983209, 3972264</td>
                    </tr>
                </table>

                 <table width='100%'>
                    <tr><td align='right' style='font-size:10px;' >F/PPU - 15/01</td></tr>
                    <tr><td align='center' style='font-size:11px;font-weight:bold;'>HASIL PENGUJIAN</td></tr>
                    <tr><td align='center' style='font-size:10px;'>No. : {$urut}/PP.02.01/23300000/{$numbering['TANGGAL']}  </td></tr>
                    <tr><td>Nama Bahan      :   </td></tr>
                 </table>

                 <table width='100%'  border='1'>
                    <thead>
                        <tr>
                            <th align='center' rowspan='4' width='40%' style='font-size:11px;font-weight:bold;'>JENIS PENGUJIAN</th>
                            <th align='center' rowspan='4' width='12%' style='font-size:11px;font-weight:bold;'>SATUAN</th>
                            <th align='center'  width='16%' style='font-size:11px;font-weight:bold;'>STANDAR</th>
                            <th align='center' colspan='6' width='17%' style='font-size:11px;font-weight:bold;'>HASIL UJI</th>
                            <th align='center' rowspan='4' width='15%' style='font-size:11px;font-weight:bold;'>METODE PENGUJIAN</th>
                        </tr>
                        <tr>
                            <th align='center' rowspan='3' style='font-size:11px;'>SNI 0302:2014</th>
                            <th align='center' colspan='6' style='font-size:11px;font-weight:bold;'>KODE CONTOH</th>

                        </tr>
                        <tr>     <th >1</th><th  >2</th><th  >3</th><th  >4</th><th  >5</th><th  >6</th>
                        </tr>
                         <tr>     <th >7</th><th  >8</th><th  >9</th><th  >10</th><th  >11</th><th  >12</th>
                        </tr>   
                     

                        
                    </thead>
                    <tbody>

                        {$tabel}
                    </tbody>
                 </table>

                 <table width='100%'>
                     <tr>     <td style='font-style: italic;'>*) tidak termasuk ruang lingkup akreditasi. </td> </tr>
                     <tr>     <td >Kesimpulan    :   Hasil Uji memenuhi standar  </td> </tr>
                 </table>
                 <table width='100%' border='1'>
                    <tr>
                        <td rowspan='4' width='20%'> <img  width='20%' height='15%' src='assets/image/kan.png'/></td> <td  width='80%' style='font-size:12px;font-weight:bold'> KETERANGAN : {$header['SAMPEL']}</td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> -  Laboratorium ini diakreditasi oleh Komite Akreditasi Nasional (KAN) No. LP-151-IDN</td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> -  Pengujian yang dilaporkan telah dilaksanakan sesuai dengan persyaratan registrasi </td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'>-  Hasil Pengujian ini tidak untuk diumumkan dan hanya berlaku untuk contoh yang bersangkutan </td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> Laboratorium Penguji </td>
                        <td style='font-size:9px;'> -  Laboratorium ini diakreditasi oleh Komite Akreditasi Nasional (KAN) No. LP-151-IDN</td>
                    </tr>
                    <tr>
                        <td style='font-size:9px;'> {$header['KD_LAB']}  </td>
                        <td> </td>
                 </table>
                  <table width='100%'>
                    <tr><td>Catatan dari Prosedur No : P/PPU/15 </td>
                    </tr>
                   </table>
                   
         ";
         };

        $mpdf->WriteHTML($html);
        $mpdf->Output();
        // exit;
    }

    private function buildArr($datas)
    {
        $sampleuniq=array_unique(array_column($datas, 'SAMPEL'));

        $halaman= count($sampleuniq)/6;

        $newArr=[];
        $newArr['halaman']=$halaman;
        $harikeys1=array_keys(array_column($datas, 'KETERANGAN'), 'kuat tekan');
        $slumpkeys=array_keys(array_column($datas, 'ID_UJI'),'161');
        $beban3keys=array_keys(array_column($datas, 'ID_UJI'),'162');
        $beban1keys=array_keys(array_column($datas, 'ID_UJI'),'163');
        $selinderkeys=array_keys(array_column($datas, 'ID_UJI'),'163');
        $permeabilitykeys=array_keys(array_column($datas, 'ID_UJI'),'165');
        $timeAwalkeys=array_keys(array_column($datas, 'ID_UJI'),'166');
        $timeAkhirkeys=array_keys(array_column($datas, 'ID_UJI'),'167');
        $arrDaysKey = [168,169, 170, 171, 172, 173];
        $arrSlump=[161];
        $arr3Beban=[162];
        $arr1Beban=[163];
        $arrselinder=[164];
        $arrpermeability=[165];
        $arrawal=[166];
        $arrakhir=[167];
        

        $beratisiKeys=array_keys(array_column($datas, 'KETERANGAN'), 'berat isi');
        $beratisissdKeys=array_keys(array_column($datas, 'KETERANGAN'), 'berat isi ssd');
        $ronggaUdaraVoidContentKeys=array_keys(array_column($datas, 'KETERANGAN'), 'rongga udara / void content');
        $arrBeratIsi=[192];
        $keausankecilKeys=array_keys(array_column($datas, 'KETERANGAN'), 'keausan kecil');
        $keausanbesarKeys=array_keys(array_column($datas, 'KETERANGAN'), 'keausan besar');
        $arrKeausan=[193];
        
        // $keys194=array_keys(array_column($datas, 'ID_UJI'),'194');
        // $Arr=$this->makeArr($datas,$keys194);

        $bjdorelatifKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj kering oven relatif');
        // return $Arr;
        $bjdodensityKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj kering oven density');
        $bjssdrelatifKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj ssd relatif');
        $bjssddensityKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj ssd density');
        $bjnyatadensityKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj nyata density');
        $bjnyatarelatifKeys=array_keys(array_column($datas, 'KETERANGAN'), 'bj nyata relatif');
        $penyerapanKeys=array_keys(array_column($datas, 'KETERANGAN'), 'penyerapan');
        $arrbjdo=[194];
        $arrbjdos=[195];


        $halus95Keys=array_keys(array_column($datas, 'KETERANGAN'), '9.5 halus');
        $halus475Keys=array_keys(array_column($datas, 'KETERANGAN'), '4.75 halus');
        $halus236Keys=array_keys(array_column($datas, 'KETERANGAN'), '2.36 halus');
        $halus118Keys=array_keys(array_column($datas, 'KETERANGAN'), '1.18 halus');
        $halus06Keys=array_keys(array_column($datas, 'KETERANGAN'), '0.6 halus');
        $halus03Keys=array_keys(array_column($datas, 'KETERANGAN'), '0.3 halus');
        $halus015Keys=array_keys(array_column($datas, 'KETERANGAN'), '0.15 halus');

        $kasar50=array_keys(array_column($datas, 'KETERANGAN'), '50 kasar');
        $kasar37=array_keys(array_column($datas, 'KETERANGAN'), '37.5 kasar');
        $kasar25=array_keys(array_column($datas, 'KETERANGAN'), '25 kasar');
        $kasar19=array_keys(array_column($datas, 'KETERANGAN'), '19 kasar');
        $kasar12=array_keys(array_column($datas, 'KETERANGAN'), '12.5 kasar');
        $kasar9=array_keys(array_column($datas, 'KETERANGAN'), '9.5 kasar');
        $arrAyarakan=[196];

        $keys197=array_keys(array_column($datas, 'ID_UJI'),'197');
        $arr197=[197];

        $keys198=array_keys(array_column($datas, 'ID_UJI'),'198');
        $arr198=[198];

        $keys199=array_keys(array_column($datas, 'ID_UJI'),'199');
        $arr199=[199];

        







        
        

        


        for ($i=1; $i <=$halaman ; $i++) { 
            # code...
            $newArr['page'][$i]['kuat']=$this->makeArrKuatTekan($datas,$harikeys1,$arrDaysKey,$i);
            $newArr['page'][$i]['slump']=$this->makeArrKuatTekan($datas,$slumpkeys,$arrSlump,$i);
            $newArr['page'][$i]['beban3']=$this->makeArrKuatTekan($datas,$beban3keys,$arr3Beban,$i);
            $newArr['page'][$i]['beban1']=$this->makeArrKuatTekan($datas,$beban1keys,$arr1Beban,$i);
            $newArr['page'][$i]['selinder']=$this->makeArrKuatTekan($datas,$selinderkeys,$arrselinder,$i);
            $newArr['page'][$i]['permeability']=$this->makeArrKuatTekan($datas,$permeabilitykeys,$arrpermeability,$i);
            $newArr['page'][$i]['awal']=$this->makeArrKuatTekan($datas,$timeAwalkeys,$arrawal,$i);
            $newArr['page'][$i]['akhir']=$this->makeArrKuatTekan($datas,$timeAkhirkeys,$arrakhir,$i);
            $newArr['page'][$i]['akhir']=$this->makeArrKuatTekan($datas,$timeAkhirkeys,$arrakhir,$i);
            $newArr['page'][$i]['beratisi']=$this->makeArrKuatTekan($datas,$beratisiKeys,$arrBeratIsi,$i);
            $newArr['page'][$i]['beratisissd']=$this->makeArrKuatTekan($datas,$beratisissdKeys,$arrBeratIsi,$i);
            $newArr['page'][$i]['ronggaudaravoid']=$this->makeArrKuatTekan($datas,$ronggaUdaraVoidContentKeys,$arrBeratIsi,$i);
            $newArr['page'][$i]['keausankecil']=$this->makeArrKuatTekan($datas,$keausankecilKeys,$arrKeausan,$i);
            $newArr['page'][$i]['keausanbesar']=$this->makeArrKuatTekan($datas,$keausanbesarKeys,$arrKeausan,$i);
            $newArr['page'][$i]['bjdorelatif']=$this->makeArrKuatTekan($datas,$bjdorelatifKeys,$arrbjdo,$i);
            $newArr['page'][$i]['bjdodensity']=$this->makeArrKuatTekan($datas,$bjdodensityKeys,$arrbjdo,$i);
            $newArr['page'][$i]['bjssdrelatif']=$this->makeArrKuatTekan($datas,$bjssdrelatifKeys,$arrbjdo,$i);
            $newArr['page'][$i]['bjssddensity']=$this->makeArrKuatTekan($datas,$bjssddensityKeys,$arrbjdo,$i);
            $newArr['page'][$i]['bjnyatadensity']=$this->makeArrKuatTekan($datas,$bjnyatadensityKeys,$arrbjdo,$i);
            $newArr['page'][$i]['bjnyatarelatif']=$this->makeArrKuatTekan($datas,$bjnyatarelatifKeys,$arrbjdo,$i);
            $newArr['page'][$i]['penyerapan']=$this->makeArrKuatTekan($datas,$penyerapanKeys,$arrbjdo,$i);

            $newArr['page'][$i]['bjdorelatif']+=$this->makeArrKuatTekan($datas,$bjdorelatifKeys,$arrbjdos,$i);
            $newArr['page'][$i]['bjdodensity']+=$this->makeArrKuatTekan($datas,$bjdodensityKeys,$arrbjdos,$i);
            $newArr['page'][$i]['bjssdrelatif']+=$this->makeArrKuatTekan($datas,$bjssdrelatifKeys,$arrbjdos,$i);
            $newArr['page'][$i]['bjssddensity']+=$this->makeArrKuatTekan($datas,$bjssddensityKeys,$arrbjdos,$i);
            $newArr['page'][$i]['bjnyatadensity']+=$this->makeArrKuatTekan($datas,$bjnyatadensityKeys,$arrbjdos,$i);
            $newArr['page'][$i]['bjnyatarelatif']+=$this->makeArrKuatTekan($datas,$bjnyatarelatifKeys,$arrbjdos,$i);
            $newArr['page'][$i]['penyerapan']+=$this->makeArrKuatTekan($datas,$penyerapanKeys,$arrbjdos,$i);

            $newArr['page'][$i]['halus95']=$this->makeArrKuatTekan($datas,$bjnyatarelatifKeys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus475']=$this->makeArrKuatTekan($datas,$halus475Keys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus236']=$this->makeArrKuatTekan($datas,$halus236Keys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus118']=$this->makeArrKuatTekan($datas,$halus118Keys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus06']=$this->makeArrKuatTekan($datas,$halus06Keys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus03']=$this->makeArrKuatTekan($datas,$halus03Keys,$arrAyarakan,$i);
            $newArr['page'][$i]['halus015']=$this->makeArrKuatTekan($datas,$halus015Keys,$arrAyarakan,$i);

            $newArr['page'][$i]['kasar50'] = $this->makeArrKuatTekan($datas,$kasar50,$arrAyarakan,$i);
            $newArr['page'][$i]['kasar37'] = $this->makeArrKuatTekan($datas,$kasar37,$arrAyarakan,$i);
            $newArr['page'][$i]['kasar25'] = $this->makeArrKuatTekan($datas,$kasar25,$arrAyarakan,$i);
            $newArr['page'][$i]['kasar19'] = $this->makeArrKuatTekan($datas,$kasar19,$arrAyarakan,$i);
            $newArr['page'][$i]['kasar12'] = $this->makeArrKuatTekan($datas,$kasar12,$arrAyarakan,$i);
            $newArr['page'][$i]['kasar9'] = $this->makeArrKuatTekan($datas,$kasar9,$arrAyarakan,$i);

            $newArr['page'][$i]['butirhalus75'] = $this->makeArrKuatTekan($datas,$keys197,$arr197,$i);
            $newArr['page'][$i]['198'] = $this->makeArrKuatTekan($datas,$keys198,$arr198,$i);
            $newArr['page'][$i]['199'] = $this->makeArrKuatTekan($datas,$keys199,$arr199,$i);
        }
        
        // $newArr['data']=$datas;
        
        return $newArr;
    }

    private function makeArrKuatTekan($datas,$keys,$arrDaysKey,$page)
    {
        $newArr=[];
        foreach ($keys as $key) {
            # code...
            array_push($newArr, $datas[$key]);
        }


        $kuatArr=[];
        foreach ($arrDaysKey as $day) {
            # code...
            $h=array_keys(array_column($newArr, 'ID_UJI'), $day);
            $Arr=$this->makeArr($newArr,$h);
            $kuatArr[$day]=[];

            for ($i=((($page-1)*6)+1); $i <= $page*6 ; $i++) { 
                # code...
                $tf = array_search($i,array_column($Arr, 'SAMPEL'));
                if($tf===false)
                {
                    array_push($kuatArr[$day],'-');
                }else{
                    array_push($kuatArr[$day],$Arr[$tf]['NILAI']);
                }
                
            }
        }
        
        return $kuatArr;
    }


    private function makeArr($datas,$keys)
    {
        $newArr=[];
        foreach ($keys as $key) {
            # code...
            array_push($newArr, ['ID_UJI'=>$datas[$key]['ID_UJI'],'SAMPEL'=>$datas[$key]['SAMPEL'],'NILAI'=>$datas[$key]['NILAI'],'KETERANGAN'=>$datas[$key]['KETERANGAN']]);
        }
        return $newArr;
    }

}
