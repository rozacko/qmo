
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beban extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->ci = &get_instance();
        $this->load->library('Layout');
        $this->load->library('Htmllib');
        $this->load->model('master/M_Beban');
    }
    //fungsi yang digunakan untuk pemanggilan halaman home
    public function index()
    {
        $data['title'] = "Master Beban Kerja";
        $this->htmllib->set_table_js();
        $this->htmllib->set_table_cs();
        $this->htmllib->set_graph_js();
        $this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

        $this->layout->render('master/beban_v2', $data);
    }

    public function get_data($input = null, $solve = null, $waktukerja = null)
    {
        // $tanggal = $_POST["tanggal"];
        if (is_null($input) or $input == "" or $input == "null") {
            $tanggal = "01-" . date('m-Y');
            $date    = date('m-Y');
        } else {
            $tanggal = "20-" . $input;
            $date    = $input;

        }

        if (is_null($solve) or $solve == "" or $solve == "null") {
            $ks = 5;
        } else {
            $ks = $solve;
        }

        if (is_null($waktukerja) or $waktukerja == "" or $waktukerja == "null") {
            $waktukerja = 2;
        } else {
            $waktukerja = $waktukerja;
        }

        $data['NUMDAYS'] = $this->M_Beban->max_day($tanggal)['NUMDAYS'];
        $data['BPPUC']   = $this->M_Beban->bppuc($date);

        $Arr = [];

        $sisa = 0;
        $bm   = 0;
        // $ks   = 5;

        for ($i = 1; $i <= $data['NUMDAYS']; $i++) {
            # code...
            if ($i < 10) {
                $tgl = "0" . $i;
            } else {
                $tgl = $i;
            }
            $nm    = date('D', strtotime($tgl . "-" . $date));
            $beban = count(array_keys(array_column($data['BPPUC'], 'TGL'), $i)) * $waktukerja;

            $selesai = 0;

            if ($sisa == 0 && $i == 1) {
                $selesai = 0;
            } elseif ($sisa <= $ks && $i != 1 && $sisa != 0) {
                $selesai = $sisa;
            } elseif ($sisa != 0) {
                $selesai = $ks;
            }

            $dataArr = [
                'no'      => $i,
                'tanggal' => $tgl . "-" . $date,
                'hari'    => $this->nameday($nm),
                'nhari'   => $this->nday($nm),
                'libur'   => $this->libur($nm),
                'sisa'    => $sisa,
                'beban'   => $beban,
                'selesai' => $selesai,
            ];

            array_push($Arr, $dataArr);

            $sisa = ($sisa + $beban) - $selesai;
        }

        echo json_encode(['data' => $Arr]);
        // exit;
        // $total         = count($this->M_Beban->get_transaksi('', $tanggal));
        // $belum_selesai = count($this->M_Beban->get_transaksi('belum', $tanggal));
        // $json          = array();
        // foreach ($detail_data as $v) {
        //     $data['ID_BEBAN']    = $v['ID_BEBAN'];
        //     $data['ID_LAB']      = $v['ID_LAB'];
        //     $data['NM_LAB']      = $v['NM_LAB'];
        //     $data['BEBAN_KERJA'] = $v['BEBAN_KERJA'];
        //     $data['TOTAL']       = $total;
        //     $data['OVERLOAD']    = $total - intval($v['BEBAN_KERJA']);
        //     $data['BELUM']       = $belum_selesai;
        //     $json[]              = $data;
        // }
        // echo json_encode($json);

    }

    public function refreshLab()
    {
        echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>";
        $data = $this->M_Beban->getLab();
        foreach ($data as $p) {
            echo "<option value='{$p["ID_LAB"]}'>" . htmlspecialchars($p["NM_LAB"]) . "</option>";
        }
    }

    public function simpan()
    {
        if (isset($_POST["lab"])) {
            $column = array(
                'ID_LAB', 'BEBAN_KERJA',
            );

            $date = date('d-m-Y H:i:s');

            $data = array(
                "'" . $_POST["lab"] . "'", "'" . $_POST["beban_kerja"] . "'",
            );
            $q = $this->M_Beban->simpan($column, $data);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    public function update()
    {

        if (isset($_POST["lab"])) {
            $id     = intval($_POST["id"]);
            $column = array(
                'ID_LAB', 'BEBAN_KERJA',
            );
            $date = date('Y-m-d h:i:s');

            $data = array(
                "'" . $_POST["lab"] . "'", "'" . $_POST["beban_kerja"] . "'",
            );

            $jml_kolom = count($column);
            $data_baru = array();

            for ($i = 0; $i < $jml_kolom; $i++) {
                $data_baru[] = $column[$i] . "=" . $data[$i];
            }

            $q = $this->M_Beban->update($id, $data_baru);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data2.'));
        }
    }

    public function hapus()
    {
        if (isset($_POST["id"])) {
            $q = $this->M_Beban->hapus($_POST["id"]);

            if ($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
        } else {
            echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
        }
    }

    private function nameday($hariEng)
    {
        switch ($hariEng) {
            case 'Sun':
                $nmdays = "Minggu";
                break;

            case 'Mon':
                $nmdays = "Senin";
                break;

            case 'Tue':
                $nmdays = "Selasa";
                break;

            case 'Wed':
                $nmdays = "Rabu";
                break;

            case 'Thu':
                $nmdays = "Kamis";
                break;

            case 'Fri':
                $nmdays = "Jumat";
                break;

            case 'Sat':
                $nmdays = "Sabtu";
                break;

            default:
                $nmdays = "Tidak di ketahui";
                break;
        }
        return $nmdays;
    }

    private function nday($hariEng)
    {
        switch ($hariEng) {
            case 'Sun':
                $nmdays = 7;
                break;

            case 'Mon':
                $nmdays = 1;
                break;

            case 'Tue':
                $nmdays = 2;
                break;

            case 'Wed':
                $nmdays = 3;
                break;

            case 'Thu':
                $nmdays = 4;
                break;

            case 'Fri':
                $nmdays = 5;
                break;

            case 'Sat':
                $nmdays = 6;
                break;

            default:
                $nmdays = "Tidak di ketahui";
                break;
        }
        return $nmdays;
    }

    private function libur($hariEng)
    {
        $libur = "Masuk";
        if ($hariEng == "Sun" or $hariEng == "Sat") {
            # code...
            $libur = "Libur";
        }
        return $libur;
    }

}
