
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/Model_transaksi');
		
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Halaman Utama";
		$data['tampilkan'] = $this->Model_transaksi->tampil();
		$data['lihat'] = $this->Model_transaksi->lihat();
		$data['view'] = $this->Model_transaksi->view();
		$data['view_2'] = $this->Model_transaksi->view_2();
		$data['berat'] = $this->Model_transaksi->berat();
		$data['all'] = $this->Model_transaksi->all();
		$data['trans'] = $this->Model_transaksi->trans();
		$data['kode'] = $this->Model_transaksi->kode();
		$data['plant'] = $this->Model_transaksi->plant();
		$data['standart'] = $this->Model_transaksi->standart();
		$data['contoh'] = $this->Model_transaksi->contoh();
		$data['lab'] = $this->Model_transaksi->lab();
		$data['view_box'] = $this->Model_transaksi->box_view();
		$data['warna'] = $this->Model_transaksi->warna();
		$data['kategori'] = $this->Model_transaksi->kategori();
		$data['t_uji'] = $this->Model_transaksi->t_uji();
		$data['datauser'] = $this->session->userdata("USERLAB");

		$sesionUser = $this->session->userdata("USERLAB");
        // echo json_encode($sesionUser);
        $session_labor = '';
        foreach($sesionUser as $v){
            $session_labor .= $v['ID_LAB'].",";
        } 
        $plant = $this->Model_transaksi->get_lab(rtrim($session_labor, ",")); 
        $opt = Array();
        $a=0;
		foreach($plant as $p)
        {
            
			// $plan = $this->Model_transaksi->tampilkanSesuai('M_PLANT','ID_PLANT',$sesionUser[$a]['ID_PLANT'])->result();
			$opt[$a]['ID_PLANT'] =$p['ID_PLANT'];
			$opt[$a]['NM_PLANT'] = $p['NM_PLANT'];$a+=1;
        }		
        $data['plant'] = $opt;

		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();


		$this->layout->render('master/transaksi', $data);
	}


    public function ajax_list()
    {
        $list = $this->Model_transaksi->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $arr) {
            $no++;
            $row = array();
            $row[] = $no; 
            // $row[] =  "<center><button onclick='detail_spuc_folder({$arr->ID_TRANSAKSI})' class='btn btn-warning btn-xs waves-effect btn_edit' title='Detail KKPP'><span class='btn-labelx'><i class='fa fa-check'></i></span> </button> </center>";
            $row[] = '<a class="btn-labelx success" title="Click for Detail" onclick="detail_spuc_folder(\''.$arr->ID_TRANSAKSI.'\')">'.$arr->NO_BAPPUC.'</a>';
            $row[] = $arr->TANGGAL;
            $row[] = $arr->NAMA_CONTOH;
            $row[] = '<center>
            			<button class="btn btn-primary btn-xs waves-effect" onclick="edit(\''.$arr->ID_TRANSAKSI.'\')" title="Edit"><span class="fa fa-pencil"></span> </button>
                        <button class="btn btn-danger btn-xs waves-effect" onclick="konfirmasi(\''.$arr->ID_TRANSAKSI.'\', \''.$arr->NO_BAPPUC.'\')" title="Delete"><span class="fa fa-trash-o"></span> </button>
                      </center>';
            $data[] = $row;
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Model_transaksi->count_all(),
			"recordsFiltered" => $this->Model_transaksi->count_filtered(),
			"data" => $data,
        );
        echo json_encode($output);
    }


	public function tambah(){
		
			
			$nomer1 = $this->input->post('1');
			$nomer2 = $this->input->post('2');
			
			$nomer4 = $this->input->post('4');
			$kd_lab = $this->input->post('3');
			$tanggal = $this->input->post('tanggal');
			//$tanggal = date('d-M-Y', strtotime($tanggal));
			$nama = $this->input->post('nama_kelompok');
			$contoh = $this->input->post('nama_contoh');
			$jumlah = $this->input->post('jumlah');
			
			$kemasan = $this->input->post('kemasan');
			$berat = $this->input->post('berat');
			$warna = $this->input->post('warna');
			$ket_warna = $this->input->post('ket_warna');
			$asal = $this->input->post('asal_contoh');
			$bentuk = $this->input->post('bentuk_contoh');
			$bentuk_contoh = $this->input->post('bentuk');
			$kondisi = $this->input->post('kondisi');
			$status ='a0';
			$mulai = $this->input->post('tanggal_mulai');
			
			$selesai = $this->input->post('tanggal_selesai');
			
			$spk = $this->input->post('no_spk');
			$peminta = $this->input->post('namapeminta');
			$alamat = $this->input->post('alamat');
			$npwp = $this->input->post('identitas');
			$penanggung_jawab = $this->input->post('penanggung_jawab');
			$keterangan = $this->input->post('keterangan');
			$ambilid = $this->Model_transaksi->ambilId('ELAB_T_BPPUC')->result();
			$idTransaksi = ($ambilid[0]->ID_TRANSAKSI)+1;
            
			$getlabplant = $this->Model_transaksi->getlabplant($kd_lab);
			$all = $idTransaksi.'/'.$nomer1.'/'.$getlabplant['KD_PLANT'].'/'.$getlabplant['KD_LAB'].'/'.$nomer4;
			$kode = implode(",",$this->input->post('sampel'));
			
			
			$data = array(
						  'KODE_UJI' =>$nomer1,
						  'ID_PLANT' =>$nomer2,
						  'KODE_LAB'=>$kd_lab,
						  
						  'NO_SPUC_4' =>$nomer4,
						  'TANGGAL' =>$tanggal,
						  'NAMA_KELOMPOK'=>$nama,
						  'ID_CONTOH'=>$contoh,
						  'JUMLAH'=>$jumlah,
						  
						  'KEMASAN'=>$kemasan,
						  'BERAT'=>$berat,
						  'WARNA'=>$warna,
						  'KETERANGAN_WARNA'=>$ket_warna,
						  'ASAL_CONTOH'=>$asal,
						  'BENTUK_CONTOH'=>$bentuk,
						  'BENTUK'=>$bentuk_contoh,
						  'KONDISI'=>$kondisi,
						  
						  'STATUS'=>$status,
						  'TANGGAL_MULAI'=>$mulai,
						  'TANGGAL_SELESAI'=>$selesai,
						  'NO_SPK'=>$spk,
						  'PEMINTA'=>$peminta,
						  'ALAMAT'=>$alamat,
						  'NPWP'=>$npwp,
						  'PENANGGUNG_JAWAB'=>$penanggung_jawab,
						  'NO_BAPPUC'=>$all,
						  'KETERANGAN'=>$keterangan,
						  'SAMPEL'=>$kode);
			
			 
			
                	
                    
			$this->Model_transaksi->add($data);
		
			
		
                $box= $this->input->post('in_checkbox');
                $data = array();
                $ambilid = $this->Model_transaksi->ambilId('ELAB_T_BPPUC')->result();
                $idTrans = $ambilid[0]->ID_TRANSAKSI;
                $idKel = $ambilid[0]->NAMA_KELOMPOK;			
                
			//==========================
			$ujian = $this->input->post('uji');
			$data = array(); 
			foreach( $ujian as $k){
                	$ambilkat = $this->Model_transaksi->ambilKat('ELAB_M_STANDART_UJI', $k)->row_array();  
                    $idKat = $ambilkat['ID_KATEGORI'];
                    $data = array('ID_STANDART' =>$k,
						 'ID_TRANSAKSI'=>$idTrans,
						 'ID_KATEGORI' =>$idKat);
                    $this->Model_transaksi->add_uji($data);
			}
            
            	//==============================
                
                foreach( $box as $k){
                
                	$standart = $this->Model_transaksi->ambilKatuji($idTransaksi, $k)->row_array();  
                    $idStd = $standart['ID_STANDART'];
                    $idKat = $standart['ID_KATEGORI'];
                    
                	$getHarga = $this->Model_transaksi->ambilHarga($k, $idStd, $kd_lab)->row_array();  
                      $data = array('ID_UJI' =>$k,
                                    'ID_KELOMPOK'=>$idKel,
                                    'ID_TRANSAKSI'=>$idTrans,
                                    'HARGA'=>$getHarga['BIAYA']);
                      $this->Model_transaksi->add_box($data);
                }
			//=====================	
			$warna = $this->input->post('warna');
			$data = array('NAMA_WARNA'=>$warna);	
			$this->Model_transaksi->add_warna($data);
            
            $cekTo = $this->Model_transaksi->cekTo('2', $kd_lab);
            
            $to = array();
            foreach($cekTo as $v){
                array_push($to, $v['EMAIL']);
            } 
		       $config['protocol']  = "smtp";
                $config['smtp_host'] = "mx3.semenindonesia.com";
                $config['smtp_port'] = "25";
                $config['smtp_user'] = "";
                $config['smtp_pass'] = "";
                $config['mailtype']  = "html";
                $config['newline']   = "\r\n";
                $config['charset']   = 'utf-8';
                $config['wordwrap']  = true;
                $config['crlf']      = "\r\n";
                    $this->load->library('email',$config); 
                    $this->email->from('noreply@semenindonesia.com', 'Team IT ELAB'); 
                    $this->email->to($to);
                    // $this->email->to('a.khafitsbi.ext@semenindonesia.com');
                    
                    $this->email->subject('Admin  Request BAPPUC');
                    $htmlContent = "<h1>Mohon dilakukan persetujuan No SPUC :  {$all}</h1>";
                    // $htmlContent .= '<a href="">Approve</a>  <a href="">Tolak</a>';
                    $this->email->message($htmlContent);
                    $this->email->send(); 
		  
            
            
			redirect('master/transaksi');
		
		$this->load->view('master/transaksi');
	}
	public function edit($id){
		$sql = $this->Model_transaksi->editData($id);
		echo json_encode($sql);
	}
	public function pemintaInternal(){
        $id = $this->input->post('id');
		$sql = $this->Model_transaksi->pemintaInternal($id);
		echo $sql['mk_nama'];
	}
	public function hapus(){
		 $id = $this->input->post('id'); 
        if(isset($_POST["id"])) {
			 $q = $this->Model_transaksi->hapusData($_POST["id"]);
             // $q = '';
            if($q) {
                
			// echo '1';
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
			// echo '2';
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
			// echo '3';
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	public function update(){
		
			$id = $this->input->post('id_transaksi');
			$nomer1 = $this->input->post('1');
			$nomer2 = $this->input->post('2');
			
			$kode = implode(",",$this->input->post('sampel_e'));
			$nomer4 = $this->input->post('4');
			$kd_lab = $this->input->post('KD_LAB');
			$tanggal = $this->input->post('tanggal');
			//$tanggal = date('d-M-Y', strtotime($tanggal));
			$nama = $this->input->post('nama_kelompok1');
			$contoh = $this->input->post('nama_contoh1');
			$jumlah = $this->input->post('jumlah');
			
			$kemasan = $this->input->post('kemasan');
			$berat = $this->input->post('berat');
			$warna = $this->input->post('warna');
			$ket_warna = $this->input->post('ket_warna');
			$asal = $this->input->post('asal_contoh');
			$bentuk = $this->input->post('bentuk_contoh');
			$bentuk_contoh = $this->input->post('bentuk');
			$kondisi = $this->input->post('kondisi');
			
			$mulai = $this->input->post('tanggal_mulai');
			
			$selesai = $this->input->post('tanggal_selesai');
			
			$spk = $this->input->post('no_spk');
			$peminta = $this->input->post('namapemintaedit'); 
			$alamat = $this->input->post('alamat');
			$keterangan = $this->input->post('keterangan');

			
			$data = array(
						  'KODE_UJI' =>$nomer1,
						  'ID_PLANT' =>$nomer2,
						  'KODE_LAB'=>$kd_lab,
						  
						  'NO_SPUC_4' =>$nomer4,
						  'TANGGAL' =>$tanggal,
						  'NAMA_KELOMPOK'=>$nama,
						  'ID_CONTOH'=>$contoh,
						  'JUMLAH'=>$jumlah,
						  'SAMPEL'=>$kode,
						  'KEMASAN'=>$kemasan,
						  'BERAT'=>$berat,
						  'WARNA'=>$warna,
						  'KETERANGAN_WARNA'=>$ket_warna,
						  'ASAL_CONTOH'=>$asal,
						  'BENTUK_CONTOH'=>$bentuk,
						  'BENTUK'=>$bentuk_contoh,
						  'KONDISI'=>$kondisi,
						  
						  
						  'TANGGAL_MULAI'=>$mulai,
						  'TANGGAL_SELESAI'=>$selesai,
						  'NO_SPK'=>$spk,
						  'PEMINTA'=>$peminta,
						  'ALAMAT'=>$alamat,
						  
						  'KETERANGAN'=>$keterangan);
                          
			$where = array('ID_TRANSAKSI'=>$id);
			$sql = $this->Model_transaksi->updateData($where, $data, 'ELAB_T_BPPUC');
			//+==============================+++
			$id_b = $this->input->post('ID_BOX');
			$id_k = $this->input->post('ID_KELOMPOK');
			$id_t = $this->input->post('id_transaksi');
			$box_e= $this->input->post('in_checkbox_edit');
			$data = array();			
            
            $sql = $this->Model_transaksi->delete_box($id_t);
			// foreach( $box_e as $k){
		      // $data = array('ID_UJI' =>$k,
		      				// 'ID_KELOMPOK'=>$id_k,
		      				// 'ID_TRANSAKSI'=>$id_t);
		  		
		      // $where = array('ID_BOX'=>$id_b);
		      // $sql_e = $this->Model_transaksi->updateData_box($where, $data, 'ELAB_T_BOX');
		  // }
          
          	// foreach( $box_e as $k){
		        // $data = array('ID_UJI' =>$k,
		      				// 'ID_KELOMPOK'=>$id_k,
		      				// 'ID_TRANSAKSI'=>$id_t);
		      // $this->Model_transaksi->add_box($data);
		    // }
            
            
            	//==============================
                
                foreach( $box_e as $k){
                
                	$standart = $this->Model_transaksi->ambilKatuji($id_t, $k)->row_array();  
                    $idStd = $standart['ID_STANDART'];
                    $idKat = $standart['ID_KATEGORI'];
                    
                	$getHarga = $this->Model_transaksi->ambilHarga($k, $idStd, $kd_lab)->row_array();  
                      $data = array('ID_UJI' =>$k,
                                    'ID_KELOMPOK'=>$id_k,
                                    'ID_TRANSAKSI'=>$id_t,
                                    'HARGA'=>$getHarga['BIAYA']);
                      $this->Model_transaksi->add_box($data);
                }
			//=====================	
            
		  //+==============================+++
            $sql = $this->Model_transaksi->delete_uji($id_t);
			$id_t = $this->input->post('ID_T_UJI');
			$id_kat = $this->input->post('ID_KATEGORI');
			$id_tr = $this->input->post('id_transaksi');
			$uji_edit= $this->input->post('uji_e');
			$data = array();			
			foreach( $uji_edit as $u){
                
              $ambilkat = $this->Model_transaksi->ambilKat('ELAB_M_STANDART_UJI', $u)->row_array();
                    $idKat = $ambilkat['ID_KATEGORI'];
		      $data = array('ID_STANDART' =>$u,
		      				'ID_KATEGORI'=>$idKat,
		      				'ID_TRANSAKSI'=>$id_tr);
		  		
                // $where = array('ID_T_UJI'=>$id_t);
              
                $this->Model_transaksi->add_uji($data);
		      // $sql_e = $this->Model_transaksi->updateData($where, $data, 'ELAB_T_UJI');
		  }  
          
			redirect('master/Transaksi');
		
		$this->load->view('master/transaksi');
	}
	public function view(){
		
		
		$id = $this->input->post('id');
		$data = $this->Model_transaksi->link($id);
		echo '<option value="">--Pilih--</option>'; 
		foreach ($data as $p) {
			echo "<option value='{$p["ID_CONTOH"]}'>".htmlspecialchars($p["NAMA_CONTOH"])."</option>";
		}
		
	}
	

	public function view_edit(){
		
		
		$id = $this->input->post('id');
		$data = $this->Model_transaksi->link_edit($id);
		echo '<option value="">--Pilih--</option>'; 
		foreach ($data as $p) {
			echo "<option value='{$p["ID_CONTOH"]}'>".htmlspecialchars($p["NAMA_CONTOH"])."</option>";
		}
		
	}
	public function uji(){
		
		
		$id = $this->input->post('id');

		$data = $this->Model_transaksi->uji($id);
		
		foreach ($data as $p) {
			$standart = $this->Model_transaksi->standart_uji($p['ID_KATEGORI']);
			//$ket = $this->Model_transaksi->keterangan($p['ID_KATEGORI']);
			?>
										<div class="row">
											<div class="col-md-11">
											<div class="form-group">
                                                
                                                  <label >Metode Uji <?php echo $p["NM_KATEGORI"]; ?></label>

                                                    <select class="form-control"  name='uji[]' id="uji">
                                                    <option value=""></option>
                                                  	<?php 

                                                  	foreach ($standart as $key) {
                                                  		?>
                                                  		<option value="<?php echo $key["ID_STANDART"]; ?>" ><?php echo  $key["NAMA_STANDART"];?></option>
                                                  		<?php
                                                  	}
                                                  	?>
                                                  </select>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                	<label >&nbsp;</label>
                                                	<div class="col-md-1" id="tombol"></div>
                                            	</div>
                                            </div>
                                           
			<?php
			
			
		}
		
	}
	public function change(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->tombol($id);
		
        foreach($sql as $p){
        	if (count($p["KETERANGAN"])) {
        		echo "<button class='btn btn-primary btn-md waves-effect' title='Keterangan Metode Uji :{$p["KETERANGAN"]}' disabled><span class='fa fa-pencil'></span> </button>";
        	}else{
        		
		
		echo "<button class='btn btn-primary btn-md waves-effect' title='Keterangan Metode Uji : Tidak ada Keterangan ' disabled><span class='fa fa-pencil'></span> </button>";
		
		}
        	}
		
			
	}
	public function view_uji(){
		
		
		$id = $this->input->post('id');
		$id_transaksi = $this->input->post('id_transaksi');

		$data = $this->Model_transaksi->uji($id);
		
		foreach ($data as $p) {
			$standart = $this->Model_transaksi->standart_uji($p['ID_KATEGORI']);
			?>
										<div class="row">
											<div class="col-md-11">
												<div class="form-group">
                                                
                                                  <label >Metode Uji <?php echo $p["NM_KATEGORI"]; ?></label>
                                                    <select class="form-control"  name='uji_e[]'>
                                                    <option value=""></option>
                                                  	<?php 

                                                  	foreach ($standart as $key) {
                                                            $cek = $this->Model_transaksi->cek_tuji($id_transaksi, $key["ID_STANDART"]);
                                                            if(count($cek)<1){
                                                                $ceked = ' ';
                                                            }else{ 
                                                                $ceked = ' selected';
                                                            }
                                                  		?>
                                                  		<option value="<?php echo $key["ID_STANDART"]; ?>" <?php echo $ceked; ?>><?php echo  $key["NAMA_STANDART"];?></option>
                                                  		<?php
                                                  		
                                                  	}
                                                  	?>
                                                  </select>
                                                </div>
                                                </div>
                                                <div class="col-md-1" id="tombol_edit"></div>
                                            </div>
                                           
			<?php
			
			
		}
		
	}

	public function change_edit(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->tombol($id);
		
        foreach($sql as $p){
        	if (count($p["KETERANGAN"])) {
        		echo "<button class='btn btn-primary btn-xs waves-effect' title='Keterangan Metode Uji :{$p["KETERANGAN"]}' disabled><span class='fa fa-pencil'></span> </button>";
        	}else{
        		
		
		echo "<button class='btn btn-primary btn-xs waves-effect' title='Keterangan Metode Uji : Tidak ada Keterangan ' disabled><span class='fa fa-pencil'></span> </button>";
		
		}
        	}
		
			
	}
	
	public function checkbox(){
		
		
		$id = $this->input->post('id');
		$Kdata = $this->Model_transaksi->Kbox($id);
		echo "<div class='row'>";
		
		foreach ($Kdata as $Kp) {
			echo "<div class='col-md-6'>";
			$data = $this->Model_transaksi->box($id,$Kp['ID_KATEGORI']);
			echo "<table style='display' width='100%' font-size='5px'>
			<thead><tr><td style='padding-top:5px;'><b>".$Kp['NM_KATEGORI']."</b></td></tr></thead><tbody>";
			foreach ($data as $p) {
				echo "<tr><td style='padding-top:5px;'>".$p['NAMA_UJI']."</td><td>";
				echo "<div class='col-md-2'>";
				echo "<input type='checkbox' name='in_checkbox[]' value='".$p['ID_UJI']."'>";
				
				echo "</td></tr>";
			}
			echo "</tbody></table></div>";
		}		
		echo "</div>";
		echo "</div>";

	}

	public function checkbox_edit(){
		 
		$id = $this->input->post('id');
		$id_transaksi = $this->input->post('id_transaksi');
		$KdataE = $this->Model_transaksi->Kbox_e($id);
		echo "<div class='row'>";
		
		foreach ($KdataE as $Kp) {
			echo "<div class='col-md-6'>";
			// $dataE = $this->Model_transaksi->box_e($Kp['ID_KATEGORI']);
			$dataE = $this->Model_transaksi->box($id ,$Kp['ID_KATEGORI']);

			echo "<table style='display' width='100%'>
			<thead ><tr><th >".$Kp['NM_KATEGORI']."</th></tr></thead><tbody>";
			foreach ($dataE as $p) {
				$dataJ = $this->Model_transaksi->Jbox_edit($id_transaksi, $p['ID_UJI']);
				
				echo "<tr><td style='padding-top:5px;'>".$p['NAMA_UJI']."</td><td>";
				echo "<div class='col-md-2'>";
				
				if(count($dataJ)<1){
                    $checked = ' >';
                }else{ 
                      $checked = " checked>";
                }
				echo "<input type='checkbox' name='in_checkbox_edit[]' value='".$p['ID_UJI']."' {$checked}";
                echo "<input type='hidden' name='ID_BOX' value='".$dataJ['ID_BOX']."'>";
                echo "<input type='hidden' name='ID_TRANSAKSI' value='".$dataJ['ID_TRANSAKSI']."'>";
                echo "<input type='hidden' name='ID_KELOMPOK' value='".$dataJ['ID_KELOMPOK']."'>"; 
				// foreach ($dataJ as $j ) {

						// if ($j['ID_UJI'] && $j['ID_TRANSAKSI']) {
							// echo " checked>";
						// }
						// echo "<input type='hidden' name='ID_BOX' value='".$j['ID_BOX']."'>";
						// echo "<input type='hidden' name='ID_TRANSAKSI' value='".$j['ID_TRANSAKSI']."'>";
						// echo "<input type='hidden' name='ID_KELOMPOK' value='".$j['ID_KELOMPOK']."'>"; 
					
					// }
				
		echo "</td></tr>";
			}
			echo "</tbody></table></div>";
		}		
		echo "</div>";
		echo "</div>";
		
	}
	
	

	public function peminta(){
		$sql = $this->Model_transaksi->view_peminta();
        echo json_encode($sql);
				
	}

	public function peminta2(){
        
        	$filter = isset($_POST["q"]) && $_POST["q"] != "" ? $_POST["q"] : "";
		$sub = $this->Model_transaksi->view_peminta2($filter);
		$result = array(array("value"=>"", "text"=>"-- Pilih Peminta Internal --", "data"=>array("value2"=>"", "text2"=>"")));
		foreach($sub as $s) {
			$result[] = array("value"=>$s["mk_nopeg"], "text"=>"({$s["mk_nopeg"]}) - ({$s["mk_nama"]})");
		}
		echo json_encode($result);
		// $sql = $this->Model_transaksi->view_peminta2();
        // echo json_encode($sql);
				
	}
	public function peminta3(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->view_peminta3($id);
		echo "<select class='form-control'  name='alamat'   id='alamat' style='width: 330px;' readonly>";	
		
        foreach($sql as $p){
        	
        
		echo "<option value='{$p["ID_PELANGGAN"]}'>".htmlspecialchars($p["ALAMAT"])."</option>";
		}
		echo "</select>";
				
	}
	public function peminta4(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->view_peminta4($id);
		echo "<select class='form-control'  name='identitas' id='identitas' style='width: 330px;' readonly>";	
		
        foreach($sql as $p){
		echo "<option value='{$p["ID_PELANGGAN"]}'>".htmlspecialchars($p["NPWP"])."</option>";
		}
		echo "</select>";
				
	}
	public function penanggung(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->penanggung($id);
		echo "<select class='form-control'  name='penanggung_jawab'  id='penanggung_jawab' style='width: 330px;' readonly>";	
		
        foreach($sql as $p){
		echo "<option value='{$p["ID_PELANGGAN"]}'>".htmlspecialchars($p["PENANGGUNG_JAWAB"])."</option>";
		}
		echo "</select>";
				
	}
	public function peminta5(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->view_peminta5($id);
		echo "<select class='form-control'  name='nama_instansi' style='width: 330px;' readonly>";	
        foreach($sql as $p){
		echo "<option value='{$p["mk_nopeg"]}'>".htmlspecialchars($p["muk_nama"])."</option>";
		}
		echo "</select>";
				
	}
	public function peminta6(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->view_peminta6($id);
		echo "<select class='form-control'  name='nik' style='width: 330px;' readonly>";
        foreach($sql as $p){
		echo "<option value='{$p["mk_nopeg"]}'>".htmlspecialchars($p["muk_kode"])."</option>";
		}
		echo "</select>";	
	}
	public function lab(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->ambilLab($id);
		
        foreach($sql as $p){
		echo "<option value='{$p["ID_LAB"]}'>".htmlspecialchars($p["NM_LAB"])."</option>";
		}
			
	}
	public function lab_e(){
		$id= $this->input->post('id');
		$sql = $this->Model_transaksi->ambilLab_e($id);
		 $sesionUser = $this->session->userdata("USERLAB");
         
        foreach($sql as $p){
            foreach($sesionUser as $v){
                if($v['ID_LAB']==$p['ID_LAB']){ 
                    echo "<option value='{$p["ID_LAB"]}'>".htmlspecialchars($p["NM_LAB"])."</option>";
                } 
            }
		} 
	}
}
?>
