
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_pelanggan');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['tampilkan'] = $this->M_pelanggan->tampil();
		$data['title'] = "Master Pelanggan";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/pelanggan', $data);
	}

	public function ajax_list()
    {
        $list = $this->M_pelanggan->new_get_datatables();
        echo json_encode(['data'=>$list]);
      //   $data = array();
      //   $no = $_POST['start'];
      //   foreach ($list as $arr) {
      //       $no++;
      //       $row = array();
      //       $row[] = $no;
      //       $row[] = $arr->NAMA_INSTANSI;
      //       $row[] = $arr->NPWP;
      //       $row[] = $arr->ALAMAT;
      //       $row[] = $arr->TELEPON;
      //       $row[] = $arr->CP;
      //       $row[] = $arr->PENANGGUNG_JAWAB;
      //       $row[] = $arr->EMAIL;
      //       $row[] = "<button class='btn btn-primary btn-xs waves-effect' onclick='edit(".$arr->ID_PELANGGAN.")'><span class='fa fa-pencil'></span></button>
			// 		  <button class='btn btn-danger btn-xs waves-effect' onclick='hapus(".$arr->ID_PELANGGAN.")'><span class='fa fa-trash-o'></span></button>";
      //       $data[] = $row;
      //   }
      //   $output = array(
			// "draw" => $_POST['draw'],
			// "recordsTotal" => $this->M_pelanggan->count_all(),
			// "recordsFiltered" => $this->M_pelanggan->count_filtered(),
			// "data" => $data,
      //   );
      //   echo json_encode($output);
    }

	public function tambah()
	{
		$nama = $this->input->post('nama_instansi');

		$npwp = $this->input->post('NPWP');

		$alamat = $this->input->post('alamat');
		$telepon = $this->input->post('telepon');
		$cp = $this->input->post('cp');
		$penanggung = $this->input->post('penanggung');
		$email = $this->input->post('email');


		if($nama ==''){

		}else{



			$data = array('NAMA_INSTANSI' =>$nama,

						  'NPWP'=>$npwp,
						  'ALAMAT'=>$alamat,
						  'TELEPON'=>$telepon,
						  'CP'=>$cp,
						  'PENANGGUNG_JAWAB'=>$penanggung,
						  'EMAIL'=>$email);
			$this->M_pelanggan->add($data);
			echo json_encode(array('status'=>TRUE));
		}

	}
	public function hapusData(){
		$id = $this->input->post('id');
		$this->M_pelanggan->hapus($id);

		echo json_encode(array('status'=>true));
	}
	public function edit($id){
		$sql = $this->M_pelanggan->get_id($id);
		echo json_encode($sql);
	}
	public function update()
	{
		$id = $this->input->post('id_pelanggan');
		$nama = $this->input->post('nama_instansi');

		$npwp = $this->input->post('NPWP');

		$alamat = $this->input->post('alamat');
		$telepon = $this->input->post('telepon');
		$cp = $this->input->post('cp');
		$penanggung = $this->input->post('penanggung');
		$email = $this->input->post('email');

		if($nama ==''){

		}else{

			$data = array('NAMA_INSTANSI' =>$nama,

						  'NPWP'=>$npwp,
						  'ALAMAT'=>$alamat,
						  'TELEPON'=>$telepon,
							'CP'=>$cp,
						'PENANGGUNG_JAWAB'=>$penanggung,
						  'EMAIL'=>$email);
			$where = array('ID_PELANGGAN'=>$id);
			$sql = $this->M_pelanggan->updateData($where, $data, 'ELAB_M_PELANGGAN');

			$status = false;
			if($sql > 0){
				$status = TRUE;
			}
			echo json_encode(array('status'=>$status));
		}
	}
	public function excel(){


		require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_pelanggan->get_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel();

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Nama Instansi');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' NPWP');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Alamat');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Telepon');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', ' Contact Person');
        $objPHPExcel->getActiveSheet()->setCellValue('G1', ' Penanggung Jawab');
        $objPHPExcel->getActiveSheet()->setCellValue('H1', ' Email');

		$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($style_bg);

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Pelanggan');


        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NAMA_INSTANSI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NPWP"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["ALAMAT"]);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["TELEPON"]);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $no, $v["CP"]);
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $no, $v["PENANGGUNG_JAWAB"]);
                $objPHPExcel->getActiveSheet()->setCellValue('H' . $no, $v["EMAIL"]);

                $no++;
                $urutan++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Pelanggan.xlsx"');
        $objWriter->save('php://output');

	}
}
