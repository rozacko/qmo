
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display_bppuc extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->db = $this->load->database('default', TRUE);
        $this->hris = $this->load->database('hris', TRUE);
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->library('FPDF'); 
		$this->load->model('master/D_bppuc','model');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Display BAPPUC/SPUC";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/display_bppuc', $data);
	}
     
	 public function get_data()
    { 
        $tanggal = $_POST["tanggal"];
        $detail_data = $this->model->get_data($tanggal);
        $json = array();
        //print_r($detail_data);die();
        foreach($detail_data as $v){
            $data['ID_TRANSAKSI'] = $v['ID_TRANSAKSI'];
            $data['NO_BAPPUC'] = $v['NO_BAPPUC'];
            $data['TANGGAL'] = $v['TANGGAL'];
            $data['NAMA_CONTOH'] = $v['NAMA_CONTOH'];
            $json[] = $data;
        }
		echo json_encode($json);

    } 
    
    
    
	public function refreshLab() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>";
		$data = $this->M_Beban->getLab();
		foreach($data as $p) {
			echo "<option value='{$p["ID_LAB"]}'>".htmlspecialchars($p["NM_LAB"])."</option>";
		}
	}
    
	public function simpan() {
		if(isset($_POST["lab"]) ){
			$column = array(
				'ID_LAB','BEBAN_KERJA' 
			);
			
			$date = date('d-m-Y H:i:s');
			
			$data = array(
				"'".$_POST["lab"]."'", "'".$_POST["beban_kerja"]."'" 
			); 
			$q = $this->M_Beban->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {

		if(isset($_POST["lab"]) ){
			$id = intval($_POST["id"]);
			$column = array(
				'ID_LAB','BEBAN_KERJA' 
			);
			$date = date('Y-m-d h:i:s');
			
			$data = array(
				"'".$_POST["lab"]."'", "'".$_POST["beban_kerja"]."'" 
			); 
			 
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_Beban->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data2.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_Beban->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}

	public function bappcu_pdf($id)
    { 
		$datas=[
			'elab_t_bppuc' => $this->model->elab_t_bppuc($id),
		];
		// echo json_encode($datas);exit;

		require_once APPPATH."/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6
    	// $this->load->view('printpdf/bappcu');
    	// exit;
        $html = $this->load->view('printpdf/bappcu',['data'=>$datas],true);
        // echo json_encode($html);exit;
        // // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }


	 public function spuc_pdf($id)
    { 
		$master_spuc = $this->model->get_spuc_pdf($id);
		
		$categories=array_column($master_spuc, 'NM_KATEGORI');
		$nm_uji=array_column($master_spuc, 'NAMA_UJI');
		$uniq_categories=array_unique($categories);
		$datas=[
			'elab_t_bppuc' => $this->model->elab_t_bppuc($id),
			'master'=>$master_spuc,
			'nm_uji'=>$nm_uji,
			'kategri'=>$categories,
			'uniq'=>$uniq_categories,
            'metode'=>$this->model->metode_uji($id),
			'implode'=>implode($nm_uji, '\n'),
		];

		foreach ($datas['uniq'] as $key ) {
			# code...
			$datas[$key]=array_keys(array_column($master_spuc, 'NM_KATEGORI'), $key);
		}

		foreach ($datas['uniq'] as $key) {
			# code...	
			$datas[$key."text"]=$this->text($datas[$key],$datas['master']);
		}
		
		// echo json_encode(count($datas['elab_t_bppuc']));exit;

		require_once APPPATH."/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6
    	// $this->load->view('login');
        $html = $this->load->view('printpdf/spuc',['data'=>$datas],true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $mpdf->Output();
    }
    public function spb_excel($id){

		
		require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->model->get_excel($id);

        $style_border = array(
                    'borders' => array(
                        'outline' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );
        $style_all = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
         $style_center = array('font' => array(
                    'size' => 12
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
         $style_left = array('font' => array(
                    'size' => 12
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000'),
                    'size' => 12,
                    'bold'=>true
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );
        $style_line = array('font' => array(
                                'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
                              )
                            );
        $styleArray = array(
					 'font' => array(
					 'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
					 'name' => 'Times New Roman',
					 'size' => 10,
					 'bold' => true
					 )
					);
         $style_aj = array(
					 'font' => array(
					 'name' => 'Times New Roman',
					 'size' => 12
					 )
					);
         $style_kec = array(
					 'font' => array(
					 'name' => 'Times New Roman',
					 'size' => 10
					 )
					);


      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 
        $objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Semen_Indonesia_img');
		$objDrawing->setDescription('Semen_Indonesia_img');
		$objDrawing->setPath('assets/image/SEMEN.png');
		$objDrawing->setCoordinates('A2');                      
		//setOffsetX works properly
		//$objDrawing->setOffsetX(1);
		//$objDrawing->setOffsetY(1);                
		//set width, height
		$objDrawing->setWidth(70); 
		$objDrawing->setHeight(50);

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        //$objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        //$objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
        $objPHPExcel->getActiveSheet()->SetCellValue('B2','          PT SEMEN INDONESIA (PERSERO) Tbk');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3','          UNIT OF LABORATORY MANAGEMENT');
		$objPHPExcel->getActiveSheet()->SetCellValue('B6',' Kepada Yth.');
        $objPHPExcel->getActiveSheet()->setCellValue('B7', ' KA BIRO KEUANGAN');
        $objPHPExcel->getActiveSheet()->setCellValue('B9', ' Harap dibuatkan Invoice dan Faktur pajak atas pembayaran biaya pengujian/analisa dari :');
        $objPHPExcel->getActiveSheet()->setCellValue('B11', ' Nama Pengirim');
        $objPHPExcel->getActiveSheet()->setCellValue('B12', ' Alamat');
        $objPHPExcel->getActiveSheet()->setCellValue('B14', ' No NPWP/NIK');
        $objPHPExcel->getActiveSheet()->setCellValue('B15', ' No Surat');
        $objPHPExcel->getActiveSheet()->setCellValue('B16', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('B17', ' Asal Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('B18', ' Jumlah Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('B20', ' Pengujian / Analisa');
        $objPHPExcel->getActiveSheet()->setCellValue('B21', ' Biaya');
        $objPHPExcel->getActiveSheet()->setCellValue('B22', ' Pot. PPH 2% dari biaya uji');
        $objPHPExcel->getActiveSheet()->setCellValue('B23', ' PPN 10 %');
        $objPHPExcel->getActiveSheet()->setCellValue('B24', ' Biaya  +  PPN 10 %');
        $objPHPExcel->getActiveSheet()->setCellValue('B26', ' NB :    Pembayaran ke Rekening Bendahara Semen Indonesia');
        $objPHPExcel->getActiveSheet()->setCellValue('B27', ' No. Rek. :   140 000 18 000 86  ( Bank Mandiri )');
        $objPHPExcel->getActiveSheet()->setCellValue('B28', ' Berita     :   Biaya Analisa');
        $objPHPExcel->getActiveSheet()->setCellValue('B30', ' Copy kwitansi / bukti transfer harap diserahkan kepada kami, sebagai bukti pembayaran.');
        $objPHPExcel->getActiveSheet()->setCellValue('B39', ' Catatan');
        $objPHPExcel->getActiveSheet()->setCellValue('B40', ' Harap membawa :');
        $objPHPExcel->getActiveSheet()->setCellValue('B41', ' - Copy NPWP');
        $objPHPExcel->getActiveSheet()->setCellValue('B42', ' - Copy PKP');
        $objPHPExcel->getActiveSheet()->setCellValue('B43', ' - Copy Bukti Pemotongan PPH Pasal 23 ( UNTUK AMBIL HASIL UJI ASLI )');
        $objPHPExcel->getActiveSheet()->setCellValue('E32', ' Laboratory of Management Officer');
        $objPHPExcel->getActiveSheet()->setCellValue('E33', ' Gresik,');
        $objPHPExcel->getActiveSheet()->setCellValue('E37', ' Yudi Darmawan');
        $objPHPExcel->getActiveSheet()->setCellValue('F2', ' F/4032/025');
        $objPHPExcel->getActiveSheet()->setCellValue('F3', ' Edisi : 1 Revisi : 4 Tgl : 1-4-2008');
        $objPHPExcel->getActiveSheet()->setCellValue('C11', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C12', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C14', ':');
        $objPHPExcel->getActiveSheet()->setCellValue('C15', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C16', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C17', ':');
        $objPHPExcel->getActiveSheet()->setCellValue('C18', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C20', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C21', ':');
        $objPHPExcel->getActiveSheet()->setCellValue('C22', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C23', ':'); 
        $objPHPExcel->getActiveSheet()->setCellValue('C24', ':'); 
        //$objPHPExcel->getActiveSheet()->setCellValue('C1', ' Standart Metode Uji');
        //$objPHPExcel->getActiveSheet()->setCellValue('D1', ' Metode Uji');
        //$objPHPExcel->getActiveSheet()->setCellValue('E1', ' Laboratorium');
        //$objPHPExcel->getActiveSheet()->setCellValue('F1', ' Biaya'); 
        
		$objPHPExcel->getActiveSheet()->getStyle('A2:J44')->applyFromArray($style_border);
		$objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray($styleArray);
		$objPHPExcel->getActiveSheet()->getStyle('B27')->applyFromArray($style_bold);
		$objPHPExcel->getActiveSheet()->getStyle('B28')->applyFromArray($style_bold);
		$objPHPExcel->getActiveSheet()->getStyle('E37')->applyFromArray($style_line);
		$objPHPExcel->getActiveSheet()->getStyle('F2')->applyFromArray($style_kec);
		$objPHPExcel->getActiveSheet()->getStyle('F3')->applyFromArray($style_kec);
        $objPHPExcel->getActiveSheet()->getStyle('E18')->applyFromArray($style_left);
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(2);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(0);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(3);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('SPB');
 
		
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                //$objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                
        	
        	if ($v['KODE_UJI']!='ISO') {
            
                $q = $this->model->peminta_internal($v['PEMINTA']);
                
                    $objPHPExcel->getActiveSheet()->setCellValue('E11' , $q["mk_nama"]);
                    $objPHPExcel->getActiveSheet()->setCellValue('E12' , $q["muk_nama"]);
                    $objPHPExcel->getActiveSheet()->setCellValue('E14' , $q["muk_kode"]);
                
            }else{
            $objPHPExcel->getActiveSheet()->setCellValue('E11' , $v["NAMA_INSTANSI"]);
            $objPHPExcel->getActiveSheet()->setCellValue('E12' , $v["ALAMAT"]);
            $objPHPExcel->getActiveSheet()->setCellValue('E14' , $v["NPWP"].' (Terlampir)');
            }
            $objPHPExcel->getActiveSheet()->setCellValue('E15' , $v["NO_SPK"]);
            $objPHPExcel->getActiveSheet()->setCellValue('E16' , $v["NAMA_CONTOH"]);  
            $objPHPExcel->getActiveSheet()->setCellValue('E17' , $v["ASAL_CONTOH"]);  
            $objPHPExcel->getActiveSheet()->setCellValue('E18' , $v["JUMLAH"]);
            $objPHPExcel->getActiveSheet()->setCellValue('E21' ,'Rp. '.number_format($v["BIAYA"],0,',','.'));
            $objPHPExcel->getActiveSheet()->setCellValue('E22' ,'Rp. '.number_format($v["BIAYA"]/100 * 2,0,',','.'));
            $objPHPExcel->getActiveSheet()->setCellValue('E23' ,'Rp. '.number_format($v["BIAYA"]/100 * 10,0,',','.'));
            $objPHPExcel->getActiveSheet()->setCellValue('E24' ,'Rp. '.number_format($v["BIAYA"] + $v["BIAYA"]/100 * 10,0,',','.'));  
           
                $no++;
                $urutan++;

        } 

        
        $objPHPExcel->createSheet();
        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(1);
        //$objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        //$objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1','Rincian Biaya : ');
        
        //$objPHPExcel->getActiveSheet()->setCellValue('H3', 'Total Semua');

        
        
        $objPHPExcel->getActiveSheet()->getStyle('B3:G3')->applyFromArray($style_all);
        $objPHPExcel->getActiveSheet()->getStyle('E14')->applyFromArray($style_left);
        $objPHPExcel->getActiveSheet()->getStyle('A1:J22')->applyFromArray($style_center);
        //$objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray($style_bg);
        //$objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray($style_all);
         
        
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Kwitansi');
 
       
        $no = 4;
        $urutan = 2;
        $data2 = $this->model->get_excel2($id);

        foreach ($data2 as $vb => $va) {
            $objPHPExcel->getActiveSheet()->SetCellValue('D'. $urutan,$va["NM_KATEGORI"]);
            $objPHPExcel->getActiveSheet()->getStyle("B{$no}:G{$no}")->applyFromArray($style_all);
            $no++;
            $urutan++;
                
                 $objPHPExcel->getActiveSheet()->SetCellValue('B'. $urutan,'UJI');
                $objPHPExcel->getActiveSheet()->setCellValue('C'. $urutan, 'Jumlah');
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $urutan, 'Biaya @');
                $objPHPExcel->getActiveSheet()->setCellValue('E'. $urutan, 'Jumlah');
                $objPHPExcel->getActiveSheet()->setCellValue('F'. $urutan, 'PPN 10');
                $objPHPExcel->getActiveSheet()->setCellValue('G'. $urutan, 'Total');
                $objPHPExcel->getActiveSheet()->getStyle("B{$no}:G{$no}")->applyFromArray($style_all);
            $no++;
            $urutan++;
            $data3 = $this->model->get_excel3($id, $va['ID_KATEGORI']);
                foreach ($data3 as $key => $v) {
                
                $objPHPExcel->getActiveSheet()->setCellValue('B'. $urutan, $v["NAMA_UJI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C'. $urutan , $v["JUMLAH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D'. $urutan, 'Rp. '.number_format($v["HARGA"],0,',','.'));
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $urutan, 'Rp. '.number_format($v["HARGA"]* $v["JUMLAH"],0,',','.'));
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $urutan, 'Rp. '.number_format($v["HARGA"]* $v["JUMLAH"] /100*10,0,',','.'));
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $urutan, 'Rp. '.number_format(($v["HARGA"]* $v["JUMLAH"]) +  ($v["HARGA"] * $v["JUMLAH"] /100*10),0,',','.'));
                //$objPHPExcel->getActiveSheet()->setCellValue('H' . $urutan, 'Rp. '.number_format(($v["HARGA"]* $v["JUMLAH"]) +  ($v["HARGA"] * $v["JUMLAH"] /100*10) + ($v["HARGA"]* $v["JUMLAH"]) +  ($v["HARGA"] * $v["JUMLAH"] /100*10)+ ($v["HARGA"]* $v["JUMLAH"]) +  ($v["HARGA"] * $v["JUMLAH"] /100*10),0,',','.'));
                
                $no++;
                $urutan++;
                
                 }
                                
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Print_SPB.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    public function loginss()
    {
    	// return  "oke";
    	// exit;
    	require_once APPPATH."/third_party/mpdf/mpdf.php"; // mpdf6
        $mpdf = new mPDF();// mpdf6
    	// $this->load->view('login');
        $html = $this->load->view('printpdf/spuc','',true);
        // echo json_encode($html);exit;
        // $pdfFilePath = "LLP APU .pdf";
        $mpdf->WriteHTML($html);
        $mpdf->Output();
     //    exit;
    }

    private function text($keys,$datas)
    {
    	$text="";

    	foreach ($keys as $key ) {
    		# code...
    		$text=$text.$datas[$key]['NAMA_UJI']."<br>";
    	}

    	return $text."<br>";
    }
    
}
