
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_kategori');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{

		$data['title'] = "Halaman Utama";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/kategori', $data);
	}

	public function ajax_list()
    {
        $list = $this->M_kategori->getdata();
        echo json_encode(['data'=>$list]);
			// 	exit;
			//
      //   $data = array();
      //   $no = $_POST['start'];
      //   foreach ($list as $arr) {
      //       $no++;
      //       $row = array();
      //       $row[] = $no;
      //       $row[] = $arr->NM_KATEGORI;
      //       $row[] = "<center><button class='btn btn-primary btn-xs waves-effect' onclick='edit(".$arr->ID_KATEGORI.")'><span class='fa fa-pencil'></span></button>
      //                 <button class='btn btn-danger btn-xs waves-effect' onclick='hapus(".$arr->ID_KATEGORI.")'><span class='fa fa-trash-o'></span></button></center>";
      //       $data[] = $row;
      //   }
      //   $output = array(
			// "draw" => $_POST['draw'],
			// "recordsTotal" => $this->M_kategori->count_all(),
			// "recordsFiltered" => $this->M_kategori->count_filtered(),
			// "data" => $data,
      //   );
      //   echo json_encode($output);
    }

	public function tambah()
	{
		$nama = $this->input->post('nm_kategori');
        $data = array('NM_KATEGORI' =>$nama
						  );
			$this->M_kategori->add($data);
			echo json_encode(array('status'=>TRUE));

	}
	public function hapusData(){
		$id = $this->input->post('id');
		$this->M_kategori->hapus($id);

		echo json_encode(array('status'=>true));
	}
	public function edit($id){
		$sql = $this->M_kategori->get_id($id);
		echo json_encode($sql);
	}
	public function update()
	{
		$id = $this->input->post('id_kategori');
		$nama = $this->input->post('nm_kategori');

			$data = array('NM_KATEGORI' =>$nama
						  );
			$where = array('ID_KATEGORI'=>$id);
			$sql = $this->M_kategori->updateData($where, $data, 'ELAB_M_KATEGORI');

			$status = false;
			if($sql > 0){
				$status = TRUE;
			}
			echo json_encode(array('status'=>$status));
	}
	public function excel(){


		require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_kategori->get_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel();

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Nama Kategori');


		$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->applyFromArray($style_bg);

        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Kategori');


        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NM_KATEGORI"]);


                $no++;
                $urutan++;
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Kategori.xlsx"');
        $objWriter->save('php://output');

	}
}
