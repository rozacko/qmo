
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_Laboratorium');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Laboratorium";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/laboratorium', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_Laboratorium->get_data();
		echo json_encode($detail_data);

    }
     
	public function refreshLokasi() {
        $id_company = $_POST["id_company"];
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Lokasi --</option>";
		$data = $this->M_Laboratorium->getLokasi($id_company);
		foreach($data as $p) {
			echo "<option value='{$p["ID_PLANT"]}'>".htmlspecialchars($p["NM_PLANT"])."</option>";
		}
	}
    
	public function refreshCompany() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>";
		$data = $this->M_Laboratorium->getCompany();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPANY"]}' id_company='{$p["ID_COMPANY"]}'>".htmlspecialchars($p["NM_COMPANY"])."</option>";
		}
	} 
    
	public function simpan() {
		if(isset($_POST["nm_lab"]) ){
			$column = array(
				'KD_LAB','NM_LAB', 'ID_COMPANY', 'ID_PLANT'
			);
			
			$data = array(
				"'".$_POST["kd_lab"]."'", "'".$_POST["nm_lab"]."'", "'".$_POST["company"]."'", "'".$_POST["lokasi"]."'"
			);
			 
			$q = $this->M_Laboratorium->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_lab"]) 
		) {
			$id = intval($_POST["id"]);
			
			$column = array(
				'KD_LAB','NM_LAB', 'ID_COMPANY', 'ID_PLANT'
			);
			
			$data = array(
				"'".$_POST["kd_lab"]."'", "'".$_POST["nm_lab"]."'", "'".$_POST["company"]."'", "'".$_POST["lokasi"]."'"
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_Laboratorium->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_Laboratorium->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Kode Lab');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Lab');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Company');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Plant');
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Kelompok');
 
        $data = $this->M_Laboratorium->get_data();
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["KD_LAB"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NM_LAB"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NM_COMPANY"]);
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["NM_PLANT"]);

                $no++;
                $urutan++;
        }  
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Laboratorium.xlsx"');
        $objWriter->save('php://output');
         
	}
    

    
}
