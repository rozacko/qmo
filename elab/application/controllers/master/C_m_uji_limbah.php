
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_m_uji_limbah extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/Model_m_uji_limbah');
		
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Halaman Utama";
		$data['tampilkan'] = $this->Model_m_uji_limbah->tampil();
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/m_uji_limbah', $data);
	}
	public function tambah(){
		if (isset($_POST['tambah'])) {
			
		$id = $this->input->post('ID_UJI');
		$kode_uji = $this->input->post('KODE_UJI');
		$nama_uji = $this->input->post('NAMA_UJI');
		$kelompok = $this->input->post('KELOMPOK');
		$satuan = $this->input->post('SATUAN');
		$simbol = $this->input->post('SIMBOL');

		$data = array('ID_UJI' =>$id,
					  'KODE_UJI' =>$kode_uji,
					  'NAMA_UJI' =>$nama_uji,
					  'KELOMPOK'=>$kelompok,
					  'SATUAN'=>$satuan,
					  'SIMBOL'=>$simbol);
		$this->Model_m_uji_limbah->add($data);
		redirect('master/c_m_uji_limbah');
		}
		$this->load->view('master/m_uji_limbah');
	}
	public function edit($id){
		$sql = $this->Model_m_uji_limbah->editData($id);
		echo json_encode($sql);
	}
	public function hapus($id){
		$this->Model_m_uji_limbah->hapusData($id);
		echo json_encode(array('status'=>FALSE));
	}
	
	public function update(){
		if(isset($_POST['update'])){
			$id = $this->input->post('ID_UJI');
			$kode_uji = $this->input->post('KODE_UJI');
			$nama_uji = $this->input->post('NAMA_UJI');
			$kelompok = $this->input->post('KELOMPOK');
			$satuan = $this->input->post('SATUAN');
			$simbol = $this->input->post('SIMBOL');
			$data = array('KODE_UJI' =>$kode_uji,
					  'NAMA_UJI' =>$nama_uji,
					  'KELOMPOK'=>$kelompok,
					  'SATUAN'=>$satuan,
					  'SIMBOL'=>$simbol);
						$where = array('ID_UJI'=>$id);
						$sql = $this->Model_m_uji_limbah->updateData($where, $data, 'ELAB_M_UJI_LIMBAH');
						redirect('master/c_m_uji_limbah');
						}
					
		$this->load->view('master/m_uji_limbah');
		
	}
}
