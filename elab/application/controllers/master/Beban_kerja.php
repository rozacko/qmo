
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beban_kerja extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_bebankerja');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Beban Kerja";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/beban_kerja', $data);
	}
     
	 public function get_data()
    {  
            $id_lab = $_POST['id_lab'];
            $date = str_replace('-', '.', $_POST['date']);
            $detail_data = $this->M_bebankerja->get_data();
             $getdata = array();  
            foreach($detail_data as $val){
                $data_spuc = $this->M_bebankerja->get_spuc($val['ID_KATEGORI'], $id_lab, $date);
                $spuc=    count($data_spuc); 
                $jsampel= 0;
                $id_transaksi = '';
                foreach($data_spuc as $s){
                    $jsampel += $s['JUMLAH'];
                    $id_transaksi .= $s['ID_TRANSAKSI'].",";
                } 
                 if($id_transaksi==''){
                     $selesai = 0 ;
                 }else{
                    $selesai = $this->M_bebankerja->get_selesai(rtrim($id_transaksi, ","), $val['ID_KATEGORI']); 
                 }   
                       
                $sampel=   $jsampel;   
                $belum = $sampel - $selesai['SELESAI'];       
                $overload = $sampel - $val['BEBAN_KERJA'];    
                $overload = ($overload > 0 ? $overload : 0);        
                $data['NM_KATEGORI'] = $val['NM_KATEGORI'];
                $data['BEBAN_KERJA'] = $val['BEBAN_KERJA'];  
                $data['SPUC'] = $spuc;  
                $data['SAMPEL'] = $sampel;  
                $data['SELESAI'] = $selesai['SELESAI'];  
                $data['BELUM'] = $belum;  
                $data['OVERLOAD'] = $overload;  
                $getdata[] = $data; 
                
            }  
		echo json_encode($getdata);

    }
    
	public function refreshLab() {
        
        	$sesionUser = $this->session->userdata("USERLAB"); 
       
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>";
        foreach($sesionUser as $v){
            $nama = $this->M_bebankerja->get_nmlab($v['ID_LAB']);
            echo "<option value='{$v["ID_LAB"]}'>".htmlspecialchars($nama['NM_LAB'])."</option>"; 
        }  
	} 
    
    
	public function simpan() {
		if(isset($_POST["nM_bebankerja"]) ){
			$column = array(
				'NAMA_CONTOH','ID_KELOMPOK' 
			);
			
			$data = array(
				"'".$_POST["nM_bebankerja"]."'", "'".$_POST["kelompok"]."'" 
			);
			 
			$q = $this->M_bebankerja->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nM_bebankerja"]) 
		) {
			$id = intval($_POST["id"]);
			
			$column = array(
				'NAMA_CONTOH','ID_KELOMPOK' 
			);
			
			$data = array(
				"'".$_POST["nM_bebankerja"]."'", "'".$_POST["kelompok"]."'" 
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_bebankerja->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_bebankerja->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_bebankerja->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' ID Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Nama Contoh');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Kelompok'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Contoh');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["ID_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_CONTOH"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NAMA_KELOMPOK"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Contoh.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
