
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Biaya extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_biaya');

	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['tampilkan'] = $this->M_biaya->tampil();
		$data['standart'] = $this->M_biaya->standart();
		$data['lab'] = $this->M_biaya->lab();
		$data['kategori'] = $this->M_biaya->kategori();
		$data['all'] = $this->M_biaya->all();
		$data['title'] = "Halaman Utama";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/biaya', $data);
	}

	public function ajax_list()
    {
        $list = $this->M_biaya->get_datatables();
        echo json_encode(['data'=>$list]);
   //      $data = array();
   //      $no = $_POST['start'];
   //      foreach ($list as $arr) {
   //          $no++;
   //          $row = array();
   //          $row[] = $no;
   //          $row[] = $arr->NAMA_UJI;
   //          $row[] = $arr->NAMA_STANDART;
   //          $row[] = $arr->NM_KATEGORI;
   //          $row[] = $arr->NM_LAB;
   //          $row[] = 'Rp. '.number_format($arr->BIAYA,0,',','.');
   //          $row[] = "<button class='btn btn-primary btn-xs waves-effect' onclick='edit(".$arr->ID_BIAYA.")'><span class='fa fa-pencil'></span></button>
   //                    <button class='btn btn-danger btn-xs waves-effect' onclick='hapus(".$arr->ID_BIAYA.")'><span class='fa fa-trash-o'></span></button>";
   //          $data[] = $row;
   //      }
   //      $output = array(
			// "draw" => $_POST['draw'],
			// "recordsTotal" => $this->M_biaya->count_all(),
			// "recordsFiltered" => $this->M_biaya->count_filtered(),
			// "data" => $data,
   //      );
   //      echo json_encode($output);
    }

	public function tambah()
	{
		$parameter = $this->input->post('par_uji');
		
		$standart = $this->input->post('standart_uji');
		
		$metode = $this->input->post('metode');
		$lab = $this->input->post('lab');
		$biaya = $this->input->post('biaya');

		

		
			$data = array('ID_UJI' =>$parameter,
						  
						  'STANDART'=>$standart,
						  'ID_KATEGORI'=>$metode,
						  'LAB'=>$lab,
							'BIAYA'=>$biaya);
			$this->M_biaya->add($data);
			echo json_encode(array('status'=>TRUE));
		
	}
	public function hapusData(){
		$id = $this->input->post('id');
		$this->M_biaya->hapus($id);

		echo json_encode(array('status'=>true));
	}
	public function edit($id){
		$sql = $this->M_biaya->get_id($id);
		echo json_encode($sql);
	}
	public function update()
	{
		$id = $this->input->post('id_biaya');
		$parameter = $this->input->post('par_uji');
		
		$standart = $this->input->post('standart_uji');
		
		$metode = $this->input->post('metode');
		$lab = $this->input->post('lab');
		$biaya = $this->input->post('biaya');

		
			$data = array('ID_UJI' =>$parameter,
						  
						  'STANDART'=>$standart,
						  'ID_KATEGORI'=>$metode,
						  'LAB'=>$lab,
							'BIAYA'=>$biaya);
			$where = array('ID_BIAYA'=>$id);
			$sql = $this->M_biaya->updateData($where, $data, 'ELAB_M_BIAYA');

			$status = false;
			if($sql > 0){
				$status = TRUE;
			}
			echo json_encode(array('status'=>$status));
	}
	public function excel(){

		
		require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_biaya->get_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Standart Metode Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Metode Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Laboratorium');
        $objPHPExcel->getActiveSheet()->setCellValue('F1', ' Biaya'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Biaya');
 
	
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) {
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NAMA_UJI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NAMA_STANDART"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["NM_KATEGORI"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["NM_LAB"]);
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $no, $v["BIAYA"]); 

                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Biaya.xlsx"');
        $objWriter->save('php://output');
         
	}
}
