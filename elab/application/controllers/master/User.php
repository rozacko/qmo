
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('M_user');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master User";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/user', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_user->get_data($this->session->userdata("USER")->TYPE);
        $getdata = array(); 
        foreach($detail_data as $val){
            $cek_userlab = $this->M_user->cek_userlab($val['ID_USER']);
            $tot = count($cek_userlab);
            $lab = ''; $role = '';$plant = '';
             foreach($cek_userlab as $v){ 
                     $koma = '';
                     if($tot>1){
                         $koma = '<br />';
                     }
                     if ($v['ID_COMPANY']=='9999'&&$v['ID_PLANT']=='9999') {
                     	$plant .= 'All Plant';
                     }elseif ($v['ID_COMPANY']=='9998'&&$v['ID_PLANT']=='9998') {
                     	$plant .= 'ALL SBI DAN SBA';
                     }elseif ($v['ID_COMPANY']=='9997'&&$v['ID_PLANT']=='9997') {
                      $plant .= 'ALL SEMEN INDONESIA';
                     }elseif ($v['ID_COMPANY']=='9996'&&$v['ID_PLANT']=='9996') {
                      $plant .= 'ALL SEMEN PADANG';
                     }else{
                     	$plant .=  $v['NM_PLANT']."".$koma;
                     }
                    /* $plant .= ($v['ID_COMPANY']=='9999'&&$v['ID_PLANT']=='9999' ? 'All Plant ' : $v['NM_PLANT']."".$koma);*/
                     // $plant .= $v['NM_PLANT']."".$koma;
                     $lab .= $v['NM_LAB']."".$koma;
                     $role .= $v['NAMA_ROLE']."".$koma; 
             }
             $status =  ($val['LDAP'] == 'Y' ? 'YES' : 'NO');
            $data['FULLNAME'] = $val['FULLNAME'];
            $data['USERNAME'] = $val['USERNAME'];
            $data['EMAIL'] = $val['EMAIL'];
            $data['STATUS'] = $status;
            $data['NM_LAB'] = $lab;
            $data['NM_PLANT'] = $plant;
            $data['NAMA_ROLE'] = $role;
            $data['ID_USER'] = $val['ID_USER'];
            $data['ID_ROLE'] = $val['ID_ROLE'];
            $data['LDAP'] = $val['LDAP'];
            $data['ID_LAB'] = $val['ID_LAB'];
            $getdata[] = $data;
        }
		echo json_encode($getdata);

    }
    
    
	 public function get_userlab()
    { 
        $id_user = $this->input->post("id_user");
        $detail_data = $this->M_user->cek_userlab($id_user);
        $getdata = array(); 
        foreach($detail_data as $val){    
            $data['ID_ROLE'] = $val['ID_ROLE'];
            $data['ID_LAB'] = $val['ID_LAB'];
            $data['NM_LAB'] = $val['NM_LAB'];
            $data['NAMA_ROLE'] = $val['NAMA_ROLE'];
            $data['ID_KATEGORI'] = $val['ID_KATEGORI'];
            $data['NM_KATEGORI'] = $val['NM_KATEGORI'];
            $data['ID_COMPANY'] = $val['ID_COMPANY'];
            if ($val['ID_COMPANY']=='9999') {
            	$data['NM_COMPANY'] = 'All Company';
            }elseif ($val['ID_COMPANY']=='9998') {
            	$data['NM_COMPANY'] = 'All SBI';
            }elseif ($val['ID_COMPANY']=='9997') {
              $data['NM_COMPANY'] = 'ALL SEMEN INDONESIA';
            }elseif ($val['ID_COMPANY']=='9996') {
              $data['NM_COMPANY'] = 'ALL SEMEN PADANG';
            }else{
            	$data['NM_COMPANY'] = $val['NM_COMPANY'];
            }
            /*$data['NM_COMPANY'] = ($val['ID_COMPANY']=='9999' ? 'All Company' : $val['NM_COMPANY']);*/
            $data['ID_PLANT'] = $val['ID_PLANT'];
            if ($val['ID_PLANT']=='9999') {
            	$data['NM_PLANT'] = 'All Plant';
            }elseif ($val['ID_PLANT']=='9998') {
            	$data['NM_PLANT'] = 'SBI DAN SBA';
            }elseif ($val['ID_PLANT']=='9997') {
              $data['NM_PLANT'] = 'TUBAN-SI,CIGADING,GRESIK';
            }elseif ($val['ID_PLANT']=='9996') {
              $data['NM_PLANT'] = 'INDARUNG DAN DUMAI';
            }else{
            	$data['NM_PLANT'] = $val['NM_PLANT'];
            }
           /* $data['NM_PLANT'] = ($val['ID_PLANT']=='9999' ? 'All Plant' : $val['NM_PLANT']);*/
            $getdata[] = $data;
        }
		echo json_encode($getdata);

    }
    
	public function refreshCompany() {
		echo "<option value='' selected='selected'>-- Pilih Company --</option>";  
        echo "<option value='9999'  nama='All Plant'> All Company </option>";
        echo "<option value='9998'  nama='All SBI'> All SBI </option>";  
        echo "<option value='9997'  nama='ALL SEMEN INDONESIA'> ALL SEMEN INDONESIA </option>";  
        echo "<option value='9996'  nama='ALL SEMEN PADANG'> ALL SEMEN PADANG </option>";  
		$data = $this->M_user->getCompany();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPANY"]}' nama='{$p["NM_COMPANY"]}'>".htmlspecialchars($p["NM_COMPANY"])."</option>";
		}
	}
    
	public function refreshPlant() {
        $company = $this->input->post("id");
		echo "<option value='' selected='selected'>-- Pilih Plant --</option>";  
        if($company == '9999'){
            echo "<option value='9999'  nama='All Plant'> All Plant </option>";  
        }elseif ($company == '9998') {
        	echo "<option value='9998'  nama='SBI DAN SBA'> SBI DAN SBA </option>";  
        }elseif ($company == '9997') {
          echo "<option value='9997'  nama='TUBAN-SI,CIGADING,GRESIK'> TUBAN-SI,CIGADING,GRESIK </option>";  
        }elseif ($company == '9996') {
          echo "<option value='9996'  nama='INDARUNG DAN DUMAI'> INDARUNG DAN DUMAI </option>";  
        }
		$data = $this->M_user->getPlant($company);
		foreach($data as $p) {
			echo "<option value='{$p["ID_PLANT"]}' nama='{$p["NM_PLANT"]}'>".htmlspecialchars($p["NM_PLANT"])."</option>";
		}
	}
    
	public function refreshKategori() {
		echo "<option value='' selected='selected'>-- Pilih Kategori --</option>"; 
		$data = $this->M_user->getKategori();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KATEGORI"]}' nama='{$p["NM_KATEGORI"]}'>".htmlspecialchars($p["NM_KATEGORI"])."</option>";
		}
	}
    
	public function refreshLab() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Laboratorium --</option>";
		$data = $this->M_user->getLab();
		foreach($data as $p) {
			echo "<option value='{$p["ID_LAB"]}' nama='{$p["NM_LAB"]}'>".htmlspecialchars($p["NM_LAB"])."</option>";
		}
	}
    
	public function refreshRole() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Role --</option>";
		$data = $this->M_user->getRole();
		foreach($data as $p) {
			echo "<option value='{$p["ID_ROLE"]}' nama='{$p["NAMA_ROLE"]}'>".htmlspecialchars($p["NAMA_ROLE"])."</option>";
		}
	}
     
	public function simpan() {
            $header = $this->input->post("header");
            $isi = $this->input->post("isi_data");
		if(isset($header["fullname"]) && isset($header["username"])){ 
			$column = array(
				'FULLNAME','USERNAME','EMAIL', 'LDAP','CREATED_AT','TYPE'
			);
			
			$data = array(
				"'".$header["fullname"]."'", "'".$header["username"]."'", "'".$header["email"]."'", "'".$header["ldap"]."'",  'SYSDATE', "'".$header["tipe"]."'"
			);
            
			$id_user = $this->M_user->simpan($column, $data);
                    $i = 0;
            foreach($isi as $key=>$val)
            { 
            
                  $datas[$i]['ID_USER'] = $id_user; 
                  $datas[$i]['ID_ROLE'] = $val['role'];  

                    if($header["tipe"]==0){ 
                          $datas[$i]['ID_LAB'] = $val['lab'];
                          $datas[$i]['ID_KATEGORI'] = $val['kategori']; 
                      }else{
                          $datas[$i]['ID_COMPANY'] = $val['company'];
                          $datas[$i]['ID_PLANT'] = $val['plant'];  
                      }                  
                  $i++;
            }  
			 $q = $this->M_user->addSubrole('ELAB_M_USERLAB', $datas);
             
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan dataq.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan dataw.')); 
		}
	}
	
	public function update() {
        
            $header = $this->input->post("header");
            $isi = $this->input->post("isi_data");
	
		if(isset($header["fullname"]) && isset($header["username"])){ 
			$id = intval($header["id"]);
			
            $column = array(
				'FULLNAME','USERNAME','EMAIL', 'LDAP', 'CREATED_AT'
			);
			
			$data = array(
				"'".$header["fullname"]."'", "'".$header["username"]."'", "'".$header["email"]."'", "'".$header["ldap"]."'",  'SYSDATE'
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_user->update($id, $data_baru);
            
            
            ////////////////// UPDATE USERLAB \\\\\\\\\\\\\\\\\\\\\\\\\
			$q = $this->M_user->deleteSubrole($header["id"]);
            $i = 0;
            foreach($isi as $key=>$val)
            {  
                  $datas[$i]['ID_USER'] = $id ;
                  if($header["tipe"]==0){ 
                      $datas[$i]['ID_LAB'] = $val['lab'];
                      $datas[$i]['ID_KATEGORI'] = $val['kategori']; 
                  }else{
                      $datas[$i]['ID_COMPANY'] = $val['company'];
                      $datas[$i]['ID_PLANT'] = $val['plant'];  
                  }
                  $datas[$i]['ID_ROLE'] = $val['role']; 
                  $i++;
            }  
			 $q = $this->M_user->addSubrole('ELAB_M_USERLAB', $datas);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_user->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
}
