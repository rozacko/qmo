
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_m_uji extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/Model_m_uji');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Parameter Uji";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js(); 

		$this->layout->render('master/m_uji', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->Model_m_uji->get_data();
		echo json_encode($detail_data);

    }
	public function refreshComponent() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Component --</option>";
		$data = $this->Model_m_uji->getComponent();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPONENT"]}'>".htmlspecialchars($p["NM_COMPONENT"])."</option>";
		}
	}
    
	public function refreshExisting() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Existing --</option>";
		$data = $this->Model_m_uji->get_data();
		foreach($data as $p) {
			echo "<option value='{$p["ID_COMPONENT"]}' nama='{$p["NAMA_UJI"]}' kategori='{$p["ID_KATEGORI"]}' satuan='{$p["SATUAN"]}' simbol='{$p["SIMBOL"]}' company='{$p["KD_COMPANY"]}'>".htmlspecialchars($p["NAMA_UJI"])."</option>";
		}
	}
	public function refreshKategori() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Kategori --</option>";
		$data = $this->Model_m_uji->getKategori();
		foreach($data as $p) {
			echo "<option value='{$p["ID_KATEGORI"]}'>".htmlspecialchars($p["NM_KATEGORI"])."</option>";
		}
	}
	public function refreshCompany() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Company --</option>";
		$data = $this->Model_m_uji->getCompany();
		foreach($data as $p) {
			echo "<option value='{$p["KD_COMPANY"]}'>".htmlspecialchars($p["NM_COMPANY"])."</option>";
		}
	} 
	public function simpan() {

		if(isset($_POST["nm_uji"]) ){
			$column = array(
				// 'ID_COMPONENT','NAMA_UJI','ID_KATEGORI' 
				 'NAMA_UJI','ID_KATEGORI','SATUAN' ,'SIMBOL' 
			);
			
			$data = array(
				 "'".$_POST["nm_uji"]."'", "'".$_POST["Kategori"]."'", "'".$_POST["satuan"]."'", "'".$_POST["simbol"]."'"    
			);
			$q = $this->Model_m_uji->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_uji"]) ){
			$id = intval($_POST["id"]);
			
            $column = array(
            // 'ID_COMPONENT','NAMA_UJI','ID_KATEGORI' 
             'NAMA_UJI','ID_KATEGORI','SATUAN' ,'SIMBOL' 
			);
			
			$data = array(
				 "'".$_POST["nm_uji"]."'", "'".$_POST["Kategori"]."'", "'".$_POST["satuan"]."'", "'".$_POST["simbol"]."'"    
			);
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->Model_m_uji->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->Model_m_uji->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->Model_m_uji->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Kategori');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Satuan'); 
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Symbol'); 
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Parameter Uji'); 
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) { 
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no, $v["NAMA_UJI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no, $v["NM_KATEGORI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["SATUAN"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["SIMBOL"]);  
                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Parameter_Uji.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
