<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_role');
	}
	
	public function index()
	{
		$data['title'] = "Role User";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->icheck_plugin();
		$this->layout->render("master/role", $data);
	}
    
    
	 public function get_data()
    { 
        $detail_data = $this->M_role->get_data();
		echo json_encode($detail_data);

    }
	
	public function ambil() {
		$s = isset($_POST["search"]) ? escape($_POST["search"]["value"], true) : "";
		$p = isset($_POST["start"]) && $_POST["start"] != "" && is_numeric($_POST["start"]) ? intval($_POST["start"]) : 0;
		$e = isset($_POST["length"]) && $_POST["length"] != "" && is_numeric($_POST["length"]) ? intval($_POST["length"]) : 10;
		$c = array('`nama_role`');
		$o = isset($_POST["order"]) ? array($c[intval($_POST["order"][0]["column"])], strtoupper($_POST["order"][0]["dir"])) : array($c[0], "ASC");
		
		$result["draw"] = isset($_POST["draw"]) ? intval($_POST["draw"]) : 1;
		$hasil = $this->M_role_user->ambil($s, $p, $e, $o);
		
		$result["recordsTotal"] = intval($hasil[0]);
		$result["recordsFiltered"] = intval($hasil[1]);
		$result["data"] = array();
		$baris = 0;
		
		foreach($hasil[2] as $r) {
			$tombol = array(
				"<button class='btn btn-success btn-tooltip btn-aksi-sm-left' onclick='edit({$baris});' title='Edit'><i class='fa fa-pencil-alt'></i></button>",
				"<button class='btn btn-warning btn-tooltip btn-aksi-sm-left' onclick='akses({$baris});' title='Hak Akses'><i class='fa fa-wrench'></i></button>",
				"<button class='btn btn-danger btn-tooltip btn-aksi-sm-right' onclick='konfirmasi({$baris});' title='Hapus'><i class='fa fa-trash'></i></button>"
			);
			$result["data"][] = array(
				htmlspecialchars($r["nama_role"]),
				"<center><div style='width:100px;'>{$tombol[0]}{$tombol[1]}{$tombol[2]}</div></center>",
				$r["id_role"]
			);
			$baris++;
		}
		echo json_encode($result);
	}
	
	public function simpan() {
		if(isset($_POST["nama_role"]) ) {
			$column = array(
				'NAMA_ROLE', 'PERMISSION'
			);
			
			$data = array(
				"'".$_POST["nama_role"]."'", "'".$_POST["permission"]."'" 
			);
			
			$q = $this->M_role->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["id_role"]) && isset($_POST["nama_role"])  ) {
			$id_role = intval($_POST["id_role"]);
			
			$column = array(
				'NAMA_ROLE', 'PERMISSION'
			);
			
			$data = array(
				"'".$_POST["nama_role"]."'", "'".$_POST["permission"]."'" 
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_role->update($id_role, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function hapus() {
		if(isset($_POST["id_role"])) {
			$q = $this->M_role->hapus($_POST["id_role"]);
			
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function ambil_akses() {
		if(isset($_POST["id_role"])) {
			$subrole = $this->M_role->getSubrole($_POST["id_role"]);
			$hak = array();
			$read = array();
			$write = array();
			foreach($subrole as $sr) {
				$hak[$sr["ID_MENU"]] = 1;
				$read[$sr["ID_MENU"]] = $sr["READ"];
				$write[$sr["ID_MENU"]] = $sr["WRITE"];
			}
            // echo json_encode ($hak)."<br>";
            // echo json_encode ($read)."<br>";
            // echo json_encode ($write);
			$menu = $this->M_role->getHalaman();
			$arr = array();
			foreach($menu as $m) {
				if($m["PARENT_MENU"] == "") {
					$arr[$m["ID_MENU"]] = array($m, array());
					if(isset($arr[$m["ID_MENU"]])) {
						$arr[$m["ID_MENU"]][0] = $m;
					} else {
						$arr[$m["ID_MENU"]] = array($m, array());
					}
				} else {
					if(isset($arr[$m["PARENT_MENU"]])) {
						$arr[$m["PARENT_MENU"]][1][] = $m;
					} else {
						$arr[$m["PARENT_MENU"]] = array(null, array());
						$arr[$m["PARENT_MENU"]][1][] = $m;
					}
				}
			}
			echo "<form id='form_akses'>";
			echo "<input type='hidden' style='display:none;' name='id_role' value='".intval($_POST["id_role"])."'>";
			echo "<table cellspacing='0' cellpadding='0' border='0' style='width:100%;'>";
			$urutan = 1;
			foreach($arr as $r) {
				echo $urutan === 1 ? "<tr>" : "";
				if(count($r[1]) == 0) {
					echo"
						<td style='width:50%;padding-bottom:10px;'>
							<label class='label label-primary check-label ' style='font-size: 15px;'><i class='fa fa-angle-right'></i>&nbsp;&nbsp;<input type='checkbox' name='akses[]' value='{$r[0]["ID_MENU"]}'".(isset($hak[$r[0]["ID_MENU"]]) ? " checked='checked'" : "").">&nbsp;".htmlspecialchars($r[0]["NAMA_MENU"])."</label>
						</td>
						<td style='width:50%;padding-bottom:10px;padding-left:8px'> 
                            <label class='check-label' ><i class=' '></i>&nbsp;&nbsp;<input type='checkbox' name='read[{$r[0]["ID_MENU"]}]' value='Y' ".(isset($hak[$r[0]["ID_MENU"]]) && $read[$r[0]["ID_MENU"]]=="Y" ? " checked='checked'" : "").">&nbsp;READ</label>
							<label class='check-label' style='margin-left:12px'><i class=' '></i>&nbsp;&nbsp;<input type='checkbox' name='write[{$r[0]["ID_MENU"]}]' value='Y' ".(isset($hak[$r[0]["ID_MENU"]]) && $write[$r[0]["ID_MENU"]]=="Y"  ? " checked='checked'" : "").">&nbsp;WRITE</label>
						</td>
                        </tr>
					";
				} else {
					echo"
						<td style='width:50%;padding-bottom:10px;'>
							<label class='label label-primary' style='font-size: 15px;'><i class='fa fa-angle-right'></i>&nbsp;&nbsp;".htmlspecialchars($r[0]["NAMA_MENU"])."</label><br/>
						</td>
					";
					foreach($r[1] as $sm) {
						echo $urutan === 1 ? "<tr>" : "<tr> ";
						echo "<td style='width:50%;padding-bottom:10px;padding-left:65px'>
                        <label class='label label-success check-label' style='margin-right:70px;font-size: 15px;'><input type='checkbox' name='akses[]' value='{$sm["ID_MENU"]}'".(isset($hak[$sm["ID_MENU"]]) ? " checked='checked'" : "").">&nbsp;".htmlspecialchars($sm["NAMA_MENU"])."</label>
                        </td>
                        <td style='width:50%;padding-bottom:10px;padding-left:16px;'>
                            <label class='check-label'><input type='checkbox' name='read[{$sm["ID_MENU"]}]' value='Y' ".(isset($hak[$sm["ID_MENU"]]) && $read[$sm["ID_MENU"]]=="Y"   ? " checked='checked'" : "").">&nbsp;READ</label> 
                         <label class='check-label' style='margin-left:20px'><input type='checkbox' name='write[{$sm["ID_MENU"]}]' value='Y' ".(isset($hak[$sm["ID_MENU"]]) && $write[$sm["ID_MENU"]]=="Y"  ? " checked='checked'" : "").">&nbsp;WRITE</label>
                        </td>";
						echo $urutan === 1 ? "<td></td></tr>" : "</tr>";
					}
					$urutan = $urutan == 1 ? 2 : 1;
				}
				echo $urutan === 2 ? "</tr>" : "";
				$urutan = $urutan == 1 ? 2 : 1;
			}
			echo "</table>";
			echo "</form>";
		} else {
			echo "Gagal mengambil data.";
		}
	}
	
	public function simpan_akses() {
                    error_reporting(0);
		if(isset($_POST["id_role"])) {
			$q = $this->M_role->deleteSubrole($_POST["id_role"]);
			if($q) {
				if(isset($_POST["akses"]) && count($_POST["akses"]) > 0) {
					// $akses = array();
					// foreach($_POST["akses"] as $a) {
						// $akses[] = "(DEFAULT, {$_POST["id_role"]}, {$a})";
					// }
                    
                    $i = 0;
                    $read = isset($_POST["read"]) ? $_POST["read"] : "";
                    $write =isset($_POST["write"]) ? $_POST["write"] : "";  
                    foreach($_POST["akses"] as $key=>$val)
                    {
                        $read_v = isset($read[$val]) ? "Y" : "";
                        $write_v = isset($write[$val]) ? "Y" : "";
                          $data[$i]['ID_ROLE'] = $_POST["id_role"];
                          $data[$i]['ID_MENU'] = $val;
                          $data[$i]['READ'] = $read_v;
                          $data[$i]['WRITE'] = $write_v;
                          $i++;
                    }
                    
					$q = $this->M_role->addSubrole('ELAB_M_SUBROLE', $data);
				} else {
					$q = true;
				}
				if($q) {
					echo "sukses";
				} else {
                    echo "Gagal menyimpan data.";
				}
			} else {
                    echo "Gagal menyimpan data.";
			}
		} else {
                    echo "Gagal menyimpan data.";
		}
         
	}
	
	private function escape($value, $quotes=true)
	{
		$search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
		$replace = array("\\\\","\\0","\\n", "\\r", "''", '\"', "\\Z");
		
		return $quotes ? "'".str_replace($search, $replace, $value)."'" : str_replace($search, $replace, strtolower($value));
	}
}
