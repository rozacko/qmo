
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Component extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance();
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_component');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Component";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();

		$this->layout->render('master/component', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_component->get_data();
		echo json_encode($detail_data);

    }
    
    
	public function simpan() {
		if(isset($_POST["nm_component"])){
			$column = array(
				'KD_COMPONENT','NM_COMPONENT','SIMBOL','SATUAN' 
			);
			
			$data = array(
				"'".$_POST["kd_component"]."'","'".$_POST["nm_component"]."'","'".$_POST["simbol"]."'","'".$_POST["satuan"]."'" 
			);
			 
			$q = $this->M_component->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["nm_component"]))  {
			$id = intval($_POST["id"]);
			
			$column = array(
				'KD_COMPONENT','NM_COMPONENT','SIMBOL','SATUAN' 
			);
			
			$data = array(
				"'".$_POST["kd_component"]."'","'".$_POST["nm_component"]."'","'".$_POST["simbol"]."'","'".$_POST["satuan"]."'" 
			);
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_component->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_component->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
}
