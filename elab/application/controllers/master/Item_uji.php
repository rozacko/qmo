
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_uji extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->ci = &get_instance(); 
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_item_uji');
	}
	//fungsi yang digunakan untuk pemanggilan halaman home
	public function index()
	{
		$data['title'] = "Master Item Uji";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
        $this->htmllib->cleave_js();

		$this->layout->render('master/item_uji', $data);
	}
     
	 public function get_data()
    { 
        $detail_data = $this->M_item_uji->get_data();
		echo json_encode($detail_data);

    } 
	public function refreshExisting() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Existing --</option>";
		$data = $this->M_item_uji->get_data();
		foreach($data as $p) {
            if($p["NAMA_UJI"]=='Ya'){
                $nama = $p["NM_COMPONENT"];
                
            }else{
                $nama = $p["NM_COMPONENT"] ." ".$p["NAMA_UJI"];
            }
			echo "<option value='{$p["ID_UJI"]}' standart='{$p["ID_STANDART"]}' min='{$p["MIN"]}' max='{$p["MAX"]}' cetak='{$p["NILAI_CETAK"]}' metode='{$p["METODE_UJI"]}' biaya='{$p["BIAYA"]}'>".htmlspecialchars($nama." -> ".$p["NAMA_STANDART"])."</option>";
		}
	}
	public function refreshItem() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Item --</option>";
		$data = $this->M_item_uji->getUji();
		foreach($data as $p) {
            if($p["NAMA_UJI"]=='Ya'){
                $nama = $p["NM_COMPONENT"];
                
            }else{
                $nama = $p["NM_COMPONENT"] ." ".$p["NAMA_UJI"];
            }
			echo "<option value='{$p["ID_UJI"]}'>".htmlspecialchars($nama)."</option>";
		}
	}
	public function refreshStandart() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Standart --</option>";
		$data = $this->M_item_uji->getStandart();
		foreach($data as $p) {
			echo "<option value='{$p["ID_STANDART"]}'>".htmlspecialchars($p["NAMA_STANDART"])."</option>";
		}
	}
    
	public function simpan() {
		if(isset($_POST["uji"]) && isset($_POST["standart"]) && isset($_POST["min"]) && isset($_POST["max"]) && isset($_POST["cetak"])  ){
			$column = array(
				'ID_UJI','ID_STANDART','MIN','MAX','NILAI_CETAK','SPESIFIKASI' 
			);
			
			$data = array(
				"'".$_POST["uji"]."'", "'".$_POST["standart"]."'", "'".$_POST["min"]."'", "'".$_POST["max"]."'", "'".$_POST["cetak"]."'", "'".$_POST["metode"]."'" 
			);
			 
			$q = $this->M_item_uji->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["uji"]) && isset($_POST["standart"]) && isset($_POST["min"]) && isset($_POST["max"]) && isset($_POST["cetak"])  ){
			$id = intval($_POST["id"]);
			
			$column = array(
				'ID_UJI','ID_STANDART','MIN','MAX','NILAI_CETAK','SPESIFIKASI' 
			);
			
			$data = array(
				"'".$_POST["uji"]."'", "'".$_POST["standart"]."'", "'".$_POST["min"]."'", "'".$_POST["max"]."'", "'".$_POST["cetak"]."'", "'".$_POST["metode"]."'" 
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_item_uji->update($id, $data_baru);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
    
	public function hapus() {
		if(isset($_POST["id"])) {
			$q = $this->M_item_uji->hapus($_POST["id"]);
			
            if($q) {
                echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
            } else {
                echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
            }
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
    
    
	public function export_xls(){
        require_once APPPATH."/third_party/PHPExcel.php";


        $data = $this->M_item_uji->get_data_excel();
        $style_border = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('rgb' => '000000')
                        )
                    )
                );

        $style_center_title = array('font' => array(
                    'bold' => true,
                    'size' => 40
                ),
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );

        $style_bg = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e3e62e')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );
        $style_blue = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '565daf')
                ),
                'font' => array(
                    'color' => array('rgb' => '000000')
                )
            );

        $style_bold = array('font' => array(
                                'bold' => true,
                                'size' => 12
                              )
                            );

      	$objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = new PHPExcel(); 

        // Create a first sheet, representing sales data
        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', ' No');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', ' Parameter Uji');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', ' Spesifikasi Standar Produk');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', ' Min'); 
        $objPHPExcel->getActiveSheet()->setCellValue('E1', ' Max'); 
        $objPHPExcel->getActiveSheet()->setCellValue('F1', ' Nilai Cetak'); 
        $objPHPExcel->getActiveSheet()->setCellValue('G1', ' Standar Metode Uji');  
         
		$objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($style_bg); 
        
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Master Item Uji');
  
        $no = 2;
        $urutan = 1;
        foreach ($data as $va => $v) { 
                $objPHPExcel->getActiveSheet()->setCellValue('A' . $no, $urutan);
                $objPHPExcel->getActiveSheet()->setCellValue('B' . $no,  $v["NAMA_UJI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('C' . $no,  $v["SPESIFIKASI"]);
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $no, $v["MIN"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('E' . $no, $v["MAX"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('F' . $no, $v["NILAI_CETAK"]); 
                $objPHPExcel->getActiveSheet()->setCellValue('G' . $no,$v["NAMA_STANDART"]);  
                $no++;
                $urutan++;
        } 
 
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Master_Item_Uji.xlsx"');
        $objWriter->save('php://output');
         
	}
    
    
}
