<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('Layout');
		$this->load->library('Htmllib');
		$this->load->model('master/M_menu');
	}
	
	public function index()
	{
		$data['title'] = "Menu";
		$this->htmllib->set_table_js();
		$this->htmllib->set_table_cs();
		$this->htmllib->set_graph_js();
		$this->htmllib->_set_form_css();
        $this->htmllib->_set_form_js();
		$this->layout->render("master/menu", $data);
	}
	
	 public function get_data()
    { 
        $detail_data = $this->M_menu->get_data();
		echo json_encode($detail_data);

    }
	
	public function simpan() {
		if(isset($_POST["posisi_menu"]) && isset($_POST["nama_menu"])) {			
			$column = array(
				'nama_menu',
				'link_menu',
				'controller_menu',
				'icon_menu',
				'icon_color',
				'posisi_menu',
				'urutan_menu',
				'parent_menu'
			);
			
			$data = array(
            
				"'".$_POST["nama_menu"]."'",
				"'".$_POST["link_menu"]."'",
				"'".$_POST["controller_menu"]."'",
				$_POST["icon_menu"] == "" ? "NULL" : "'".$_POST["icon_menu"]."'",
				$_POST["icon_color"] == "" ? "NULL" : "'".$_POST["icon_color"]."'",
				intval($_POST["posisi_menu"]),
				intval($_POST["urutan_menu"]),
				$_POST["parent_menu"] == "" ? "NULL" : "'".$_POST["parent_menu"]."'"
			);
			
			$q = $this->M_menu->simpan($column, $data);
			
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function update() {
		if(isset($_POST["id_menu"]) && isset($_POST["posisi_menu"]) && isset($_POST["nama_menu"])) {
			$id_menu = intval($_POST["id_menu"]);
			
			$column = array(
				'nama_menu',
				'link_menu',
				'controller_menu',
				'icon_menu',
				'icon_color',
				'posisi_menu',
				'urutan_menu',
				'parent_menu'
			);
			
			$data = array(
				"'".$_POST["nama_menu"]."'",
				"'".$_POST["link_menu"]."'",
				"'".$_POST["controller_menu"]."'",
				$_POST["icon_menu"] == "" ? "NULL" : "'".$_POST["icon_menu"]."'",
				$_POST["icon_color"] == "" ? "NULL" : "'".$_POST["icon_color"]."'",
				intval($_POST["posisi_menu"]),
				intval($_POST["urutan_menu"]),
				$_POST["parent_menu"] == "" ? "NULL" : "'".$_POST["parent_menu"]."'"
			);
			
			$jml_kolom = count($column);
			$data_baru = array();
			
			for($i = 0; $i < $jml_kolom; $i++) {
				$data_baru[] = $column[$i]."=".$data[$i];
			}
			
			$q = $this->M_menu->update($id_menu, $data_baru);
			
		
			if($q) {
				echo json_encode(array('notif' => '1', 'message' => 'Berhasil'));
			} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.'));
			}
		} else {
				echo json_encode(array('notif' => '0', 'message' => 'Gagal menyimpan data.')); 
		}
	}
	
	public function hapus() {
		if(isset($_POST["id_menu"])) {
			$q = $this->M_menu->hapus($_POST["id_menu"]);
			
			if($q) {
				echo "berhasil";
			} else {
				echo "Gagal menghapus data.";
			}
		} else {
			echo "Gagal menghapus data.";
		}
	}
	
	public function refreshParent() {
		echo "<option value='' data-hidden='true' selected='selected'>-- Pilih Parent Menu --</option>";
		$parent = $this->M_menu->getParentMenu();
		foreach($parent as $p) {
			echo "<option value='{$p["ID_MENU"]}'>".htmlspecialchars($p["NAMA_MENU"])."</option>";
		}
	}
}
