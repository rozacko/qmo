<?php 
	include_once("../function.php");

	function m_cronjob_get_data_area($tanggal, $ID_GROUPAREA = '4'){ 
		$sql = "SELECT A.ID_AREA  FROM T_CEMENT_HOURLY A
						JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
						JOIN M_AREA C ON A.ID_AREA = C.ID_AREA
						JOIN M_GROUPAREA D ON C.ID_GROUPAREA = D.ID_GROUPAREA
						WHERE to_char(A.DATE_DATA,'DD/MM/YYYY') = '{$tanggal}'
							AND NILAI IS NOT NULL  AND D.ID_GROUPAREA = $ID_GROUPAREA 
						GROUP BY A.ID_AREA 
						ORDER BY A.ID_AREA ASC";
		return $sql;
	}
	function m_cronjob_get_data_daily($area, $tanggal, $ID_GROUPAREA = '4'){
		$sql = "SELECT  ROUND(AVG(B.NILAI), 2) AS NILAI_AVG, B.ID_COMPONENT, E.KD_COMPONENT FROM T_CEMENT_HOURLY A
						JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
						JOIN M_AREA C ON A.ID_AREA = C.ID_AREA
						JOIN M_GROUPAREA D ON C.ID_GROUPAREA = D.ID_GROUPAREA
						JOIN M_COMPONENT E ON B.ID_COMPONENT = E.ID_COMPONENT
						WHERE to_char(A.DATE_DATA,'DD/MM/YYYY') = '{$tanggal}'
							AND A.ID_AREA = {$area}  AND D.ID_GROUPAREA = $ID_GROUPAREA
						GROUP BY A.ID_AREA, B.ID_COMPONENT, E.KD_COMPONENT
						ORDER BY  A.ID_AREA";
		return $sql;
	}

	function m_cronjob_get_data_terjadwal(){ // izza 30.03.2021
		$data_fix['DELETE_MARK'] = '0'; // data masih ada
		$data_fix['STATUS'] = '0'; // data blm pernah di sinkron
		$sql = setSelect("M_JADWAL", $data_fix);
		$sql .= " ORDER BY TGL_JADWAL";
		return $sql;
	}

	function m_cronjob_update_jadwal($data) {
		$data_fix["STATUS"] = $data["STATUS"];
		$data_fix["INSERT_HEADER"] = $data["INSERT_HEADER"];
		$data_fix["UPDATE_HEADER"] = $data["UPDATE_HEADER"];

		$where["ID_JADWAL"] = $data["ID_JADWAL"];

		$sql = setUpdate("M_JADWAL", $data_fix, $where);
		return $sql;
	}
	
?>