<?php 
	include_once("../function.php");
	function t_cement_daily_get_id_daily($ID_AREA, $ID_PRODUCT, $DATE_DATA){
		$sql = "SELECT * FROM T_CEMENT_DAILY 
						WHERE ID_AREA = '$ID_AREA' AND FROM_HOURLY = 'Y' AND TO_CHAR(DATE_DATA,'DD/MM/YYYY') = '{$DATE_DATA}' 
		";
		if ($ID_PRODUCT !='') $sql .= " ID_PRODUCT = '$ID_PRODUCT'";
		return $sql;
	}
	function t_cement_daily_insert($data){
		// $this->db->select("COBA_AJA_SEQ.NEXTVAL as DATAID",FALSE);
		// $ID_CEMENT_DAILY = $this->db->get("DUAL")->row()->DATAID;

		$data_fix["ID_CEMENT_DAILY"] = $data['ID_CEMENT_DAILY'];
		$data_fix["ID_AREA"] = $data['ID_AREA'];
		$data_fix["ID_PRODUCT"] = $data['ID_PRODUCT'];
		$data_fix["ID_MESIN_STATUS"] = $data['ID_MESIN_STATUS'];
		$data_fix["MESIN_REMARK"] = $data['MESIN_REMARK'];
		// $data_fix["DATE_DATA"] = "to_date('".$data['DATE_DATA']."','dd/mm/yyyy')";
		// $data_fix["DATE_ENTRY"] = "to_date('".$data['DATE_ENTRY']."','dd/mm/yyyy')";
		$data_fix["DATE_DATA"] = $data['DATE_DATA'];
		$data_fix["DATE_ENTRY"] = $data['DATE_ENTRY'];
		$data_fix["USER_ENTRY"] = $data['USER_ENTRY'];
		if(ISSET($data['FROM_HOURLY'])){
			$data_fix["FROM_HOURLY"] = $data['FROM_HOURLY'];
		}

		$sql = setInsert('T_CEMENT_DAILY', $data_fix);
		return $sql;

	}
	function t_cement_daily_update($data,$ID_CEMENT_DAILY){
		$data_fix["ID_MESIN_STATUS"] = $data['ID_MESIN_STATUS'];
		$data_fix["MESIN_REMARK"] = $data['MESIN_REMARK'];
		// $data_fix["DATE_ENTRY"] = "to_date('".$data['DATE_ENTRY']."','dd/mm/yyyy')";
		$data_fix["DATE_ENTRY"] = $data['DATE_ENTRY'];
		$data_fix["USER_UPDATE"] = $data['USER_UPDATE'];

		$where["ID_CEMENT_DAILY"] = $ID_CEMENT_DAILY;

		$sql = setUpdate('T_CEMENT_DAILY', $data_fix, $where);
		return $sql;
	}
	function t_cement_daily_d_exists($data){
		$data_fix["ID_CEMENT_DAILY"] = $data['ID_CEMENT_DAILY'];
		$data_fix["ID_COMPONENT"] = $data['ID_COMPONENT'];
		$sql = setSelect("D_CEMENT_DAILY", $data_fix);
		return $sql;
	}
	function t_cement_daily_d_update($data){
		$data_fix["NILAI"] = $data['NILAI'];
		$data_fix["NO_FIELD"] = $data['NO_FIELD'];
		
		$where["ID_CEMENT_DAILY"] = $data['ID_CEMENT_DAILY'];
		$where["ID_COMPONENT"] = $data['ID_COMPONENT'];

		$sql = setUpdate('D_CEMENT_DAILY', $data_fix, $where);
		return $sql;
	}



?>