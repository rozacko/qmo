<?php 
  date_default_timezone_set('Asia/Jakarta');
  include 'function.php';
  include 'model/m_cronjob.php';
  include 'model/t_cement_daily.php';
  // ini kode nya hampir sama dengan yg ada di qmonline/Cronjob_convert/run. tapi yang ini versi native
  
  // ----------------------------- start run -----------------------------
    // ini buat ngubah ke format TGL_JADWAL dd/mm/yyyy
    $JAM = date('H');
    $TGL_JADWAL = date('d/m/Y');
    if(isset($_GET)){
      if(isset($_GET['JAM'])) {$JAM = $_GET['JAM'];}
      if(isset($_GET['TGL'])) {
        $TGL = $_GET['TGL']; 
        $tanggal = $TGL[2].$TGL[3].'/'.$TGL[0].$TGL[1].'/'.$TGL[4].$TGL[5].$TGL[6].$TGL[7];
        $TGL_JADWAL = date('d/m/Y', strtotime($tanggal));
      }
      if(isset($_GET['TES'])){ // ini kalau bukan testing. (KALAU TESTING PAKAI ID_JADWAL 104 aja)
        $TGL_JADWAL = '09/03/2021';
        $JAM = '17';
      }
    } 
    // $data = $this->eksekusi($JAM,$TGL_JADWAL);
    // ----------------------------- start eksekusi -----------------------------
      $data = array(); 
      $detik = 24*60*60;
      
      if($JAM && $TGL_JADWAL){ // untuk pastikan jam dan tgl_jadwal terisi
        
        // $jadwals = $this->m_cronjob->get_data_terjadwal();
        $jadwals = array();
        $sql = m_cronjob_get_data_terjadwal();
        
        $query = oci_parse($conn, $sql); oci_execute($query); while($data2=oci_fetch_assoc($query)){
          $jadwals[] = $data2;
        }
        

        foreach ($jadwals as $jadwal) { // nyesuaikan jadwal yang blm tereksekusi
          $tot_insert = 0; $tot_update = 0;
          if(date('d/m/Y', strtotime($jadwal['TGL_JADWAL'])) == $TGL_JADWAL && $jadwal['JAM_JADWAL'] == $JAM ){ // sesuai jadwal
            $jml_hari = ( (strtotime($jadwal['TGL_DATA_END']) - strtotime($jadwal['TGL_DATA_START'])) / ($detik) ) + 1; 
            for ($i=0; $i < $jml_hari; $i++) { 
              $tanggal = date('d/m/Y', strtotime($jadwal['TGL_DATA_START']) + ($detik*$i));
              // $temp = $this->insert_daily_terjadwal($tanggal);

              // ----------------------------- start insert_daily_terjadwal -----------------------------
                // tanggal yg dipakai fungsi ini pakai tanggal date_data
                // $tanggal = '05/03/2021'; // DUMMY

                $arrInputData = array(); $arrInputHeader = array();
                $arrInputDataUpdate = array(); $arrInputHeaderUpdate = array();
                
                // $get_area = $this->m_cronjob->get_data_area($tanggal); 
                $get_area = array();
                $sql = m_cronjob_get_data_area($tanggal);     
                $query = oci_parse($conn, $sql); oci_execute($query); while($data2=oci_fetch_assoc($query)){
                  $get_area[] = $data2;
                }
                
                foreach($get_area as $val){
                    // $get_data_hourly =  $this->m_cronjob->get_data_daily($val['ID_AREA'], $tanggal);
                    $get_data_hourly = array();
                    $sql = m_cronjob_get_data_daily($val['ID_AREA'], $tanggal);     
                    $query = oci_parse($conn, $sql); oci_execute($query); while($data2=oci_fetch_assoc($query)){
                      $get_data_hourly[] = $data2;
                    }
                    $cek_ada_header = 1; // Hanya Data awal perulangan
                    $no_field = 0; // DI CEMENT DAILY MULAI DARI 0
                    foreach($get_data_hourly as $nil){
                      // $ID_CEMENT_DAILY = false;
                      if($cek_ada_header==1){ 
                        $tdata = array();
                        // PROSES INSERT HEADER T_CEMENT_DAILY
                        $tdata['ID_AREA'] = $val['ID_AREA'];
                        // $tdata['ID_PRODUCT'] = $nil['ID_PRODUCT'];
                        $tdata['ID_PRODUCT'] = ""; // KILN DATA PRODUCT NUll
                        $tdata['ID_MESIN_STATUS'] = "";
                        $tdata['MESIN_REMARK'] =  "";
                        $tdata['DATE_DATA'] =  $tanggal;  
                        
                        // $ID_CEMENT_DAILY = $this->t_cement_daily->get_id_daily($val['ID_AREA'],"",$tanggal); 
                        
                        $return = array();
                        $sql = t_cement_daily_get_id_daily($val['ID_AREA'],"",$tanggal); 
                        $query = oci_parse($conn, $sql); oci_execute($query); while($data2=oci_fetch_assoc($query)){
                          $return[] = $data2['ID_CEMENT_DAILY'];
                          $ID_CEMENT_DAILY = $data2['ID_CEMENT_DAILY'];;
                        }
                        if(count($return)==0){  // header kosong. jadi insert header
                          // echo 'insert header';
                          $tdata['DATE_ENTRY'] = date("d/m/Y");
                          $tdata['USER_ENTRY'] = "1234567890";
                          $tdata['FROM_HOURLY'] = "Y";

                          // $ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
                          $sql = "SELECT SEQ_ID_CEMENT_DAILY.NEXTVAL as DATAID FROM DUAL";
                          $ID_CEMENT_DAILY='';
                          $query = oci_parse($conn, $sql); $exec = oci_execute($query); while($data2=oci_fetch_assoc($query)){
                            $ID_CEMENT_DAILY = $data2['DATAID'];
                          //   $ID_CEMENT_DAILY = $ID_CEMENT_DAILY2;
                          }
                          $tdata['ID_CEMENT_DAILY'] = $ID_CEMENT_DAILY;

                          $sql = t_cement_daily_insert($tdata);
                          $query = oci_parse($conn, $sql); $exec = oci_execute($query); 
                          $arrInputHeader[] = $tdata;
                          // array_push($arrInputHeader, $tdata);
                        }
                        else{
                          // echo 'update header';
                          $tdata['DATE_ENTRY'] = date("d/m/Y");
                          $tdata['USER_UPDATE'] = "1234567890";

                          // $this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);
                          $sql = t_cement_daily_update($tdata, $ID_CEMENT_DAILY);
                          $query = oci_parse($conn, $sql); $exec = oci_execute($query); 

                          // array_push($arrInputHeaderUpdate, $tdata);
                          $arrInputHeaderUpdate[] = $tdata;
                        }
                      }
                      $cek_ada_header +=1;
                      
                      // detail
                      if($ID_CEMENT_DAILY!=""){
                        // PROSES INSERT DETAIL D_CEMENT_DAILY
                        $ddata = array();
                        $ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
                        $ddata['ID_COMPONENT']		  = $nil['ID_COMPONENT'];
                        $ddata['NILAI']				      = $nil['NILAI_AVG'];
                        $ddata['NO_FIELD']			    = $no_field;
                        $no_field  += 1 ;

                        $cek = array();
                        $sql = t_cement_daily_d_exists($ddata);
                        $query = oci_parse($conn, $sql); oci_execute($query); while($data2=oci_fetch_assoc($query)){
                          $cek[] = $data2;
                        }
                        
                        if(count($cek) == 0){ // CEK DETAIL ADA TIDAK
                          $arrInputData[] = $ddata; 
                        }
                        else{
                          $arrInputDataUpdate[] = $ddata;
                        }
                      }


                    }

                }
                $data['tanggal'] 	= $tanggal;
                if(count($arrInputHeader)>0){
                  $data['keterangan'] = 'Create';
                }
                if(count($arrInputHeaderUpdate)>0){
                  $data['keterangan'] = 'Update';
                }

                if(count($arrInputData)>0){ 
                  // insert detail
                  // $this->t_cement_daily->insert_dcement($arrInputData); // SIMPAN ARRAY HASIL PUSH DIATAS
                  foreach ($arrInputData as $data_detail) {
                    $sql = setInsert('D_CEMENT_DAILY', $data_detail);
                    $query = oci_parse($conn, $sql); $exec = oci_execute($query);
                  }
                }

                if(count($arrInputDataUpdate)>0){
                  // update detail
                  foreach ($arrInputDataUpdate as $data_detail) {
                    $sql = t_cement_daily_d_update($data_detail);
                    $query = oci_parse($conn, $sql); $exec = oci_execute($query);
                  }
                }

                $data["insert_header"] = count($arrInputHeader);
                $data["insert_detail"] = count($arrInputData);
                $data["update_header"] = count($arrInputHeaderUpdate);
                $data["update_detail"] = count($arrInputDataUpdate);

              // ----------------------------- end insert_daily_terjadwal -----------------------------
                
              $temp_perhari = $data;
              if($temp_perhari['keterangan'] == 'Create'){$tot_insert++;}
              elseif($temp_perhari['keterangan'] == 'Update'){$tot_update++;}
              $temp[] = $temp_perhari;
            }
            
            // $data_upd_jadwal["STATUS"] = 0;
            $data_upd_jadwal["STATUS"] = 1;
            $data_upd_jadwal["INSERT_HEADER"] = $tot_insert;
            $data_upd_jadwal["UPDATE_HEADER"] = $tot_update;
            $data_upd_jadwal["ID_JADWAL"] = $jadwal['ID_JADWAL'];

            $sql = m_cronjob_update_jadwal($data_upd_jadwal);
            $query = oci_parse($conn, $sql); $exec = oci_execute($query);
          } 
          
        }

        // PELAPORAN
        $insert = 0; $update = 0;
        foreach ($temp as $temporary) {
          $insert += $temporary["insert_header"];
          $update += $temporary["update_header"];
        }
        $waktu = date('d/m/Y H:i:s');

        

        // ------------------------- LOGGING -------------------------
        $filename = "log/qmo_convert_hourly.txt";
        
        $log1 = "PROSES TARIK DATA - ";
        $log1 .=  'Waktu = ' . $waktu . ' | '
                . 'Insert = ' . $insert . ' | '
                . 'Update = ' . $update;
        $history = file_put_contents($filename, $log1 . "\r\n", FILE_APPEND | LOCK_EX); // Ini buat naruh ke file logging nya
        

        $jml['filename'] = $filename;
        $jml['waktu'] = $waktu;
        $jml['insert'] = $insert;
        $jml['update'] = $update;

        $HASIL['history'] = $history;
        $HASIL['jml'] = $jml;
        $HASIL['result'] = $temp;
        echo json_encode($HASIL);
        exit;
      }
        
    
    // ----------------------------- end eksekusi -----------------------------

  // ----------------------------- end run -----------------------------
  



?>