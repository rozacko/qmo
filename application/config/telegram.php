<?php defined('BASEPATH') OR exit('No direct script access allowed.');

$config['default'] = [
  // below are config for TelegramLib
  'api_key' => '',
  'username' => '',
  'webhook_token' => '',
  'webhook_url' => '',
  'commands_paths' => [
      APPPATH . 'libraries/TelegramBotCommands'
  ],

  // below are config for TelegramWsdl
  'service_url' => 'http://10.15.5.242/tele/Telegram.php?wsdl',
  'service_type' => 'wsdl',
];
