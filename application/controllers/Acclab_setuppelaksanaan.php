<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acclab_setuppelaksanaan extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_acclab_setuppelaksanaan","setup");
		
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_data = $this->setup->get_list();
		// $this->status = $this->setup->get_status();
		$this->atasan = $this->setup->data_pic();
		// echo "<pre>"; print_r($this->list_data);
		// print_r($this->db->last_query());
		// exit();
		$this->template->adminlte("v_acclab_setuppelaksanaan");
    }
	
	public function pelaksanaan($id_pp){
		$this->list_data = $this->setup->get_list($id_pp);
		// $this->status = $this->setup->get_status();
		$this->atasan = $this->setup->data_pic();
		$this->template->adminlte("v_acclab_setuppelaksanaan");
	}
    
    public function add(){
		$this->list_periode = $this->setup->data_periode();
		// $this->list_comodity = $this->setup->data_comodity();
		$this->list_lab = $this->setup->data_lab();
		$this->list_eval = $this->setup->data_evaluasi();
		$this->list_pic_obs = $this->setup->data_pic();
		$this->list_pic_plant = $this->setup->data_pic();
		$this->template->adminlte("v_acclab_setuppelaksanaan_add");
	}
	
	public function create(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pp 		  = $this->input->post('ID_PP');
		$jenis 		  = $this->input->post('JENIS');
		$start_date	  = $this->input->post('START_DATE');
		$end_date	  = $this->input->post('END_DATE');
		$id_lab 	  = $this->input->post('ID_LAB');
		$id_pic_obs	  = $this->input->post('ID_PIC_OBS');
		$id_pic_plant = $this->input->post('ID_PIC_PLANT');
		$date_now 	 = date('m-d-Y');
		$create_by_now = $user_in->FULLNAME;
		
		$save = $this->setup->save_proficiency(); 
		
		//send email
		$user_obs = $this->setup->get_email_pic($id_pic_obs);
		$user_plant = $this->setup->get_email_pic($id_pic_plant);
		$priode = $this->setup->get_priode_pro($id_pp);
		
		$pp = $priode->NAMA." - ".$priode->TAHUN;
		
		// $this->send_email($user_obs->EMAIL, $pp, $jenis, $user_obs->FULLNAME);
		// $this->send_email($user_plant->EMAIL, $pp, $jenis, $user_plant->FULLNAME);
		
		if($save){
			$this->notice->success("Data Created.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error($this->setup->error());
			redirect("acclab_setuppelaksanaan/add");
		}
	}

	public function send_notif()
	{
		$id = $this->input->post('id_setup');
		$id_atasan = $this->input->post('atasan');
		$data = $this->setup->get_data_by_id($id);
		$user_atasan = $this->setup->get_email_pic($id_atasan);
		$priode = $this->setup->get_priode_pro($data->FK_ID_PERIODE);
		
		$pp = $priode->NAMA." - ".$priode->TAHUN;
		$send = $this->send_email_atasan($user_atasan->EMAIL, $pp, $user_atasan->FULLNAME, $id, $user_atasan->USERNAME);
		if($send){
			$this->notice->success("Email Sent.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error("Failed to Send Email.");
			redirect("acclab_setuppelaksanaan");
		}
	}

	public function send_notif_pic($id)
	{
		$data = $this->setup->get_data_by_id($id);
		$user_obs = $this->setup->get_email_pic($data->PIC_OBSERVASI);
		$user_plant = $this->setup->get_email_pic($data->PIC_PLANT);
		$priode = $this->setup->get_priode_pro($data->FK_ID_PERIODE);
		
		$pp = $priode->NAMA." - ".$priode->TAHUN;
		$send1 = $this->send_email($user_obs->EMAIL, $pp, $data->JENIS, $user_obs->FULLNAME);
		$send2 = $this->send_email($user_plant->EMAIL, $pp, $data->JENIS, $user_plant->FULLNAME);
		if($send2){
			$this->notice->success("Email Sent to PIC.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error("Failed to Send Email.");
			redirect("acclab_setuppelaksanaan");
		}
	}

	public function approve($id)
	{
		$update = $this->setup->update_status($id, 'OPEN'); 
		
		if($update){ 
			$this->notice->success("Data Approved.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error($this->setup->error());
			redirect("acclab_setuppelaksanaan");
		}
	}

	public function reject($id, $username)
	{
		$update = $this->setup->update_status($id, 'REJECTED'); 
		
		if($update){ 
			$this->notice->success("Data Rejected.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error($this->setup->error());
			redirect("acclab_setuppelaksanaan");
		}
	}
	
	public function get_list(){
		$list = $this->setup->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $column->ID_PROFICIENCY;
			$row[] = $column->NAMA_SAMPLE;
			$row[] = $column->NAMA_LAB;
			$row[] = $column->FULLNAME;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->setup->count_all(),
            "recordsFiltered" => $this->setup->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}	
	
	public function edit($id){
		$this->data_detil = $this->setup->get_data_by_id($id);
		$this->list_periode = $this->setup->data_periode();
		// $this->list_comodity = $this->setup->data_comodity();
		$this->list_lab = $this->setup->data_lab();
		$this->list_eval = $this->setup->data_evaluasi();
		$this->list_pic_obs = $this->setup->data_pic();
		$this->list_pic_plant = $this->setup->data_pic();
		$this->template->adminlte("v_acclab_setuppelaksanaan_add");
	}
	
	public function update(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id = $this->input->post('ID');
		
		$update = $this->setup->update_proficiency(); 
		
		if($update){ 
			$this->notice->success("Data Edited.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error($this->setup->error());
			redirect("acclab_setuppelaksanaan/edit/".$id);
		}
	}
	
	public function hapus($id){
		$hapus = $this->setup->hapus_proficiency($id); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("acclab_setuppelaksanaan");
		} else {
			$this->notice->error($this->setup->error());
			redirect("acclab_setuppelaksanaan");
		}
	}
	
	public function create_activity(){
		$id_proficiency = $this->input->post('PROFICIENCY');
		
		$save_act = $this->setup->save_activity($id_proficiency);
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} 
	}
	
	public function do_edit_activity(){
		$id_proficiency = $this->input->post('ID_PRO');
		$update_act = $this->setup->update_activity();
		//print_r($this->db->last_query());
		//exit();
		
		if($update_act){
			$this->notice->success("Data Edited.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} 
		
	}
	
	public function do_hapus_activity($id_activity, $id_proficiency){
		$hapus = $this->setup->hapus_activity($id_activity); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("Uji_proficiency_setup/edit/".$id_proficiency);
		}
	}
	
	public function timeline_act($id_proficiency){
		$list = $this->setup->get_data_activity($id_proficiency);
		//print_r($list);
		//exit();
		
		$data = array();
		
		foreach($list as $dt){
			
			//plan
			$date1 = $dt->PLAN_DATE_START;
			$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
			$out1 = $dateObj1->format('Y/m/d');
			$start = (strtotime("{$out1}") * 1000);
			
			$date2 = $dt->PLAN_DATE_END;
			$dateObj2 = DateTime::createFromFormat('d-M-y', $date2);
			$out2 = $dateObj2->format('Y/m/d');
			$end = (strtotime("{$out2}") * 1000);
			
			$values[] = array ("from" => $start, "to" => $end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttBlue");
			$hasil = array ("name"	 => $dt->ACTIVITY_NAME, "desc"	 => "Plan", "values" => $values);
			
			unset($values);
			array_push($data, $hasil);
			
			
			//realisasi
			if($dt->ACT_DATE_START != null and $dt->ACT_DATE_END != null){
				$date3 = $dt->ACT_DATE_START;
				$dateObj3 = DateTime::createFromFormat('d-M-y', $date3);
				$out3 = $dateObj3->format('Y/m/d');
				$real_start = (strtotime("{$out3}") * 1000);
				
				$date4 = $dt->ACT_DATE_END;
				$dateObj4 = DateTime::createFromFormat('d-M-y', $date4);
				$out4 = $dateObj4->format('Y/m/d');
				$real_end = (strtotime("{$out4}") * 1000);
				
				$values2[] = array ("from" => $real_start, "to" => $real_end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil2 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values2);
				array_push($data, $hasil2);
			} else {
				$values3[] = array ("from" => '', "to" => '', "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil3 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values3);
				array_push($data, $hasil3);
			}
			
			
		}
		
		//return json_encode($data);
		
		echo (json_encode($data));
	}
	
	public function send_email($to, $title, $jenis, $name){
		$from = "qmo.noreply@semenindonesia.com";
		$to = "qm.developer20@gmail.com";
		$subject = "Penunjukan PIC ".$title;
		$message = "
Kepada Yth.
".$name."

Dengan hormat,

	Anda telah ditunjuk sebagai PIC ".ucwords($jenis)." ".$title.". Harap melakukan update terkait sebagai PIC di qmo.semenindonesia.com serta menyampaikan petunjuk teknis pelaksanaan akurasi laboratorium melalui email. Demikian terima kasih

Regards,
		
Koordinator Pelaksanaan ".$title."
		";
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}

	public function send_email_atasan($to, $title, $name, $id, $username){
		$from = "qmo.noreply@semenindonesia.com";
		$to = "qm.developer20@gmail.com";
		$subject = "Approval Pelaksanaan Observasi ".$title;
		$message = "
<html>
<head>
<link href=".base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css")." rel='stylesheet'>
</head>
<body>
<table border='0' width='90%'>
<tr>
<td colspan='3'>Kepada Yth.</td>
</tr>
<tr>
<td colspan='3'>".$name."</td>
</tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr>
<td colspan='3'>Dengan hormat,</td>
</tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr>
<td width='10px'>&ensp;</td>
<td colspan='2'>Dimohon Approvalnya untuk Pelaksanaan Observasi ".$title." dengan menekan tombol dibawah ini.</td>
</tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr>
<td width='10px'>&ensp;</td>
<td style='text-align:right'><a href='".site_url("login/redirect_login/")."?username=".base64_encode($username)."&url=".site_url("acclab_setuppelaksanaan/approve/").$id."' class='btn btn-primary btn-sm'>Approve</a></td>
<td style='text-align:left'><a href='".site_url("login/redirect_login/")."?username=".base64_encode($username)."&url=".site_url("acclab_setuppelaksanaan/reject/").$id."' class='btn btn-danger btn-sm'>Reject</a></td>
</tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr><td colspan='3'>Regards,</td></tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr><td colspan='3'>Koordinator Pelaksanaan ".$title."</td></tr>
</table>
</body>
</html>
		";
		$headers = "From:" . $from . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		if (mail($to,$subject,$message, $headers)) {
			return true;
		}else{
			return false;
		}
		return false;
	}
}

?>