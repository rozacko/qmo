<?php

class Product_assignment_read extends QMUser {
	
	public $list_company 	= array();
	public $list_plant 		= array();
	public $list_area 		= array();
	public $list_product 	= array();
	public $post 	= array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $ID_AREA;
	public $data_area;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_area");
		$this->load->model("c_product_view");
		$this->post = @$this->input->post();
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_product_assign_read");
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
		to_json($area);
	}

	public function get_list(){

		$list = $this->c_product_view->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $tabel) {
			$no++;
			$row = array();
			$row[] = $tabel->ID_AREA;
			$row[] = $no;
			$row[] = $tabel->NM_COMPANY;
			$row[] = $tabel->NM_PLANT;
			$row[] = $tabel->NM_AREA;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->c_product_view->count_all(),
            "recordsFiltered" => $this->c_product_view->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
	
	public function view(){
		$id = (empty($this->session->flashdata('id_area'))) ? $this->input->post('id_area'):$this->session->flashdata('id_area');
		$this->ID_AREA = $id;
		
		$this->ID_PLANT = $this->m_area->get_data_by_id($this->ID_AREA)->ID_PLANT;
		$this->ID_COMPANY = $this->m_plant->get_data_by_id($this->ID_PLANT)->ID_COMPANY;
		
		$this->NM_PLANT = $this->m_plant->get_data_by_id($this->ID_PLANT)->NM_PLANT;
		
		$this->list_company = $this->m_company->datalist();
		foreach($this->list_company as $comp){
			if($comp->ID_COMPANY == $this->ID_COMPANY){
				$this->NM_COMPANY = $comp->NM_COMPANY;
			}
		}
		
		$this->template->adminlte("v_product_assign_view");
	}

	public function get_list_product(){
		$list = $this->c_product_view->get_list_product();
		$data = array();
		$no = $this->input->post('start');
 
		foreach ($list as $tabel) {
			$no++;
			$row = array();
			$row[] = $tabel->ID_C_PRODUCT;
			$row[] = $no;
			$row[] = $tabel->KD_PRODUCT;
			$row[] = $tabel->NM_PRODUCT;
			$data[] = $row;
		}
		
		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->c_product_view->count_all_product(),
            "recordsFiltered" => $this->c_product_view->count_filtered_product(),
            "data" => $data,
        );

		to_json($output);
	}
	
	public function create(){
		$ID_AREA = $this->input->post("HID_AREA");
		$this->c_product_view->clean($ID_AREA);		
		foreach($this->input->post("OPT_PRODUCT") as $ID_PRODUCT){
			$data = false;
			$data['ID_AREA'] = $ID_AREA;
			$data['ID_PRODUCT'] = $ID_PRODUCT;
			
			
			$cek = $this->c_product_view->get_where($data); 
// echo $cek;
// exit();
			if ($cek<1) { 
				 @$this->c_product_view->insert($data); 
			}
			
			#echo $this->db->last_query() . "\n\n";
		}
		// $this->session->set_flashdata("id_area",$ID_AREA);
		// $this->notice->success($ID_AREA);
		// $_SESSION['id_area'] = $ID_AREA;
		// $this->session->mark_as_flash('id_area');
		$this->notice->success("Assignment Success");
		redirect("product_assignment_read");
	}
	
	public function edit(){
		$id = (empty($this->session->flashdata('id_area'))) ? $this->input->post('id_area'):$this->session->flashdata('id_area');
		
		$this->load->model("m_product");
		$this->ID_AREA = $id;
		$this->ID_PLANT = $this->m_area->get_data_by_id($this->ID_AREA)->ID_PLANT;
		$this->ID_COMPANY = $this->m_plant->get_data_by_id($this->ID_PLANT)->ID_COMPANY;
		
		$this->NM_PLANT = $this->m_plant->get_data_by_id($this->ID_PLANT)->NM_PLANT;
		
		$this->list_company = $this->m_company->datalist();
		foreach($this->list_company as $comp){
			if($comp->ID_COMPANY == $this->ID_COMPANY){
				$this->NM_COMPANY = $comp->NM_COMPANY;
			}
		}
		
		$this->list_product = $this->m_product->datalist();
		$this->template->adminlte("v_product_assign_read_edit");
	}
	
	public function async_product($ID_AREA){
		$ID_PLANT = $this->m_area->get_data_by_id($ID_AREA)->ID_PLANT;
		$ID_COMPANY = $this->m_plant->get_data_by_id($ID_PLANT)->ID_COMPANY;
		$output = $this->c_product_view->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($output);
	}

	public function update($ID_AREA){
		$this->m_area->update($this->input->post(),$ID_AREA);
		if($this->m_area->error()){
			$this->notice->error($this->m_area->error());
			redirect("area/edit/".$ID_AREA);
		}
		else{
			$this->notice->success("Area Data Updated.");
			redirect("area/by_plant/".$this->input->post("ID_PLANT"));
		}
	}
	
	public function delete($ID_AREA,$ID_PLANT=NULL){
		$this->m_area->delete($ID_AREA);
		if($this->m_area->error()){
			$this->notice->error($this->m_area->error());
		}
		else{
			$this->notice->success("Area Data Removed.");
		}
		redirect("area/by_plant/".$ID_PLANT);
	}
}	

?>
