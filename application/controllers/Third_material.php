<?php

class Third_material extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_thirdmaterial");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->list_component = $this->m_thirdmaterial->list_component();
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);	
		$this->template->adminlte("v_third_material", $data);
	}

	public function save_third_material(){
		
		$result['msg'] = 'Cannot Insert New Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['KODE_MATERIAL'] = $post['KODE_MATERIAL'];
		$data['NAME_MATERIAL'] = $post['NAME_MATERIAL'];
		$data['H2O_PERCENTAGE'] = $post['H2O_PERCENTAGE'];
		$data['ID_COMPONENT'] = $post['ID_COMPONENT'];
		$data['COMP_PERCENTAGE'] = $post['COMP_PERCENTAGE'];
		$data['CREATE_BY'] = $post['user'];

		$isexsist_product = $this->m_thirdmaterial->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_thirdmaterial->sample_insert($data);
			if ($isertd_product) {
				// code...
				$result['msg'] = 'Insert New Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}
		
		to_json($result);
	}

	public function update_third_material(){
		
		$result['msg'] = 'Cannot Update Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_MATERIAL'] = $post['ID_MATERIAL'];
		$data['KODE_MATERIAL'] = $post['KODE_MATERIAL'];
		$data['NAME_MATERIAL'] = $post['NAME_MATERIAL'];
		$data['H2O_PERCENTAGE'] = $post['H2O_PERCENTAGE'];
		$data['ID_COMPONENT'] = $post['ID_COMPONENT'];
		$data['COMP_PERCENTAGE'] = $post['COMP_PERCENTAGE'];
		$data['MODIFIED_BY'] = $post['user'];

		$isexsist_product = $this->m_thirdmaterial->is_sample_exists($data);

		if ($isexsist_product >= 0) {
			# code...

			$isertd_product = $this->m_thirdmaterial->sample_update($data);

			if ($isertd_product) {
				// code...
				$result['msg'] = 'Update Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}

		to_json($result);
	}

	public function delete_third_material(){
		
		$result['msg'] = 'Cannot Delete Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_MATERIAL'] = $post['ID_MATERIAL'];
		$data['DELETE_BY'] = $post['user'];

		$isertd_product = $this->m_thirdmaterial->sample_delete($data);
		if ($isertd_product) {
			// code...
			$result['msg'] = 'Delete Product Success ...';
			$result['status'] = true;
		}

		to_json($result);
	}

	public function third_material_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		// $data	= $this->Job_m->get($key);

      	$data	= $this->m_thirdmaterial->get_data($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_thirdmaterial->recFil($key),
  			'recordsTotal'		=>	$this->m_thirdmaterial->recTot($key)
  		);

  		echo json_encode($return);
    }

    public function product_list_scm(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		// $data	= $this->Job_m->get($key);

      	$data	= $this->m_thirdmaterial->get_data_scm($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_thirdmaterial->recFil_scm($key),
  			'recordsTotal'		=>	$this->m_thirdmaterial->recTot_scm($key)
  		);

  		echo json_encode($return);
    }

    public function get_third_material_percentage(){
		
		$post = $this->input->post();

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		// $ColOpt['type'] = 'text';
		// $ColOpt['readOnly'] = true;

		$ColOpt = array('type' => 'text', 'readOnly' => true, );

		$header[] = 'Third Material';
		$ColWidhts[] = 200;
		$columOptions[] = $ColOpt;
		$header[] = 'H2O';
		$ColWidhts[] = 70;
		$columOptions[] = $ColOpt;

		$BodyData = array();


		$third_material_list = $this->m_thirdmaterial->get_third_material_list();

		if ($third_material_list) {
			# code...

			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			$component_relation	= $this->m_thirdmaterial->get_component_relation();

			foreach ($component_relation as $key => $value) {
				# code...
				$header[] = $value['KD_COMPONENT'];
				$ColWidhts[] = 70;
				$columOptions[] = $ColOpt;
			}

			foreach ($third_material_list as $key => $value) {
				# code...
				$tbody = array();

				$tbody[] = $value['NAME_MATERIAL'];
				$tbody[] = $value['H2O_PERCENTAGE'].' %';

				foreach ($component_relation as $ind => $val) {
					# code...
					if ($value['ID_COMPONENT'] == $val['ID_COMPONENT'] && $value['ID_MATERIAL'] == $val['ID_MATERIAL']) {
						# code...
						$tbody[] = $val['COMP_PERCENTAGE'].' %';
					} else {
						# code...
						$tbody[] = '';
					}

				}

				$BodyData[] = $tbody;
			}


			$return['Data']['Third Material'] = $third_material_list;			
			$return['Data']['Component'] = $component_relation;
			$return['Data']['ColWidhts'] = $ColWidhts;	
			$return['Data']['Header'] = $header;			
			$return['Data']['Body'] = $BodyData;
			$return['Data']['Col Options'] = $columOptions;
		}

		to_json($return);
	}


	public function get_componen_sni_standard_product(){
		
		$post = $this->input->post();


		$data['ID_TYPE_PRODUCT'] = $post['ID_TYPE_PRODUCT'];
		$data['USER_LOGIN'] = $post['user'];

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$ColOptcomp = array('type' => 'text', 'readOnly' => true , );
		$ColOpt = array('type' => 'text', 'readOnly' => false , );

		$header[] = 'Component';
		$ColWidhts[] = 150;
		$columOptions[] = $ColOptcomp;


		$header[] = 'SNI Min Value';
		$ColWidhts[] = 100;
		$columOptions[] = $ColOpt;


		$header[] = 'SNI Max Value';
		$ColWidhts[] = 100;
		$columOptions[] = $ColOpt;

		$BodyData = array();
		$ComponentData = array();


		$component_list = $this->m_thirdmaterial->get_all_component_list();

		if ($component_list) {
			# code...

			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			foreach ($component_list as $key => $value) {
				# code...

				$ComponentData[] = (int) $value['ID_COMPONENT'];

				$tbody = array();

				$tbody[] = preg_replace('/\s+/', '', $value['KD_COMPONENT']); 

				$sni_standard = $this->m_thirdmaterial->get_SNI_standard_component((int) $data['ID_TYPE_PRODUCT'], (int) $value['ID_COMPONENT']);

				if ($sni_standard) {
					# code...

					$min_sni = round((double) $sni_standard['MIN_VALUE'], 1);
					$max_sni = round((double) $sni_standard['MAX_VALUE'], 1);

					if ((int) $min_sni == 0) {
						# code...
						$tbody[] = null;
					} else {
						# code...
						$tbody[] = round((double) $sni_standard['MIN_VALUE'], 1);
					}

					if ((int) $max_sni == 0) {
						# code...
						$tbody[] = null;
					} else {
						# code...
						$tbody[] = round((double) $sni_standard['MAX_VALUE'], 1);
					}
					
					
				} else {
					# code...
					$tbody[] = null;
					$tbody[] = null;
				}	

				$BodyData[] = $tbody;

			}


			$return['Data']['Third Material'] = $third_material_list;			
			$return['Data']['Component'] = $component_relation;		
			$return['Data']['ComponentID'] = $ComponentData;
			$return['Data']['ColWidhts'] = $ColWidhts;	
			$return['Data']['Header'] = $header;			
			$return['Data']['Body'] = $BodyData;
			$return['Data']['Col Options'] = $columOptions;
		}

		to_json($return);
	}

	public function save_sni_standard_component(){
		
		$post = $this->input->post();


		$data['ID_TYPE_PRODUCT'] = $post['ID_TYPE_PRODUCT'];
		$data['SNI_DATA'] = $post['SNI_DATA'];
		$data['ID_COMPONENT'] = $post['COMPONENT_DATA'];
		$data['USER_LOGIN'] = $post['user'];

		$return['Status'] = false;
		$return['Message'] = 'Cannot Process Request';
		$return['Data'] = null;

		$action = 0;
		$datainserted = array();
		$dataupdated = array();
		$datadeleted = array();

		for ($i=0; $i < count($data['ID_COMPONENT']) ; $i++) { 
			# code...

			$sni_standard_exist = $this->m_thirdmaterial->is_SNI_exists((int) $data['ID_TYPE_PRODUCT'], (int) $data['ID_COMPONENT'][$i], $data['USER_LOGIN']);

			if (!$sni_standard_exist && ($data['SNI_DATA'][$i][1] || $data['SNI_DATA'][$i][2])) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->SNI_insert((int) $data['ID_TYPE_PRODUCT'], (int) $data['ID_COMPONENT'][$i], $data['SNI_DATA'][$i], $data['USER_LOGIN']);
				$action++;
				$datainserted[] = $data['SNI_DATA'][$i];
			} else if ($sni_standard_exist && ($data['SNI_DATA'][$i][1] || $data['SNI_DATA'][$i][2])) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->SNI_update((int) $data['ID_TYPE_PRODUCT'], (int) $data['ID_COMPONENT'][$i], $data['SNI_DATA'][$i], $data['USER_LOGIN']);
				$action++;
				$dataupdated[] = $data['SNI_DATA'][$i];
			} else if ($sni_standard_exist && (!$data['SNI_DATA'][$i][1] && !$data['SNI_DATA'][$i][2])) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->SNI_delete((int) $data['ID_TYPE_PRODUCT'], (int) $data['ID_COMPONENT'][$i]);
				$action++;
				$datadeleted[] = $data['SNI_DATA'][$i];
			}
			
		}

		if ($action > 0) {
			# code...

			$return['Status'] = true;
			$return['Message'] = count($datainserted) . ' data inserted & '. count($dataupdated) . ' data updated & '. count($datadeleted) . ' data deleted';
			$return['Data'] = $data['SNI_DATA'];

		}

		to_json($return);
	}

	public function get_config_precission(){
		
		$post = $this->input->post();

		$data['USER_LOGIN'] = $post['user'];

		$return['Status'] = false;
		$return['Message'] = 'No Record Found';
		$return['Data'] = null;

		$ColOptcomp = array('type' => 'text', 'readOnly' => true , );
		$ColOpt = array('type' => 'text', 'readOnly' => false , );

		$header[] = 'Component';
		$ColWidhts[] = 150;
		$columOptions[] = $ColOptcomp;


		$header[] = 'Component Precission (-,00)';
		$ColWidhts[] = 175;
		$columOptions[] = $ColOpt;


		$BodyData = array();
		$ComponentData = array();


		$component_list = $this->m_thirdmaterial->get_all_component_list();

		if ($component_list) {
			# code...

			$return['Status'] = true;
			$return['Message'] = 'Record Found';

			foreach ($component_list as $key => $value) {
				# code...

				$ComponentData[] = (int) $value['ID_COMPONENT'];

				$tbody = array();

				$tbody[] = $value['KD_COMPONENT'];

				$sni_standard = $this->m_thirdmaterial->get_config_precission_component((int) $value['ID_COMPONENT']);

				if ($sni_standard) {
					# code...
					$tbody[] = $sni_standard['PRECISSION'];
				} else {
					# code...
					$tbody[] = null;
				}	

				$BodyData[] = $tbody;

			}


			$return['Data']['Third Material'] = $third_material_list;			
			$return['Data']['Component'] = $component_relation;		
			$return['Data']['ComponentID'] = $ComponentData;
			$return['Data']['ColWidhts'] = $ColWidhts;	
			$return['Data']['Header'] = $header;			
			$return['Data']['Body'] = $BodyData;
			$return['Data']['Col Options'] = $columOptions;
		}

		to_json($return);
	}

	public function save_config_precission(){
		
		$post = $this->input->post();


		$data['ID_TYPE_PRODUCT'] = $post['ID_TYPE_PRODUCT'];
		$data['SNI_DATA'] = $post['SNI_DATA'];
		$data['ID_COMPONENT'] = $post['COMPONENT_DATA'];
		$data['USER_LOGIN'] = $post['user'];

		$return['Status'] = false;
		$return['Message'] = 'Cannot Process Request';
		$return['Data'] = null;

		$action = 0;
		$datainserted = array();
		$dataupdated = array();
		$datadeleted = array();

		for ($i=0; $i < count($data['ID_COMPONENT']) ; $i++) { 
			# code...

			$sni_standard_exist = $this->m_thirdmaterial->is_config_precission_exists((int) $data['ID_COMPONENT'][$i], $data['USER_LOGIN']);

			if (!$sni_standard_exist && $data['SNI_DATA'][$i][1] != '' ) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->config_precission_insert((int) $data['ID_COMPONENT'][$i], $data['SNI_DATA'][$i], $data['USER_LOGIN']);
				$action++;
				$datainserted[] = $data['SNI_DATA'][$i];
			} else if ($sni_standard_exist && $data['SNI_DATA'][$i][1] != '' ) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->config_precission_update((int) $data['ID_COMPONENT'][$i], $data['SNI_DATA'][$i], $data['USER_LOGIN']);
				$action++;
				$dataupdated[] = $data['SNI_DATA'][$i];
			} else if ($sni_standard_exist && !$data['SNI_DATA'][$i][1] != '' ) {
				# code...
				$sni_standard_exist = $this->m_thirdmaterial->config_precission_delete((int) $data['ID_COMPONENT'][$i]);
				$action++;
				$datadeleted[] = $data['SNI_DATA'][$i];
			}
			
		}

		if ($action > 0) {
			# code...

			$return['Status'] = true;
			$return['Message'] = count($datainserted) . ' data inserted & '. count($dataupdated) . ' data updated & '. count($datadeleted) . ' data deleted';
			$return['Data'] = $data['SNI_DATA'];

		}

		to_json($return);
	}

}	

?>
