<?php

class Sync_tis extends QMUser {

    public function __construct(){
		parent::__construct();
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->helper("string");
		$this->load->model("M_sync_tis");
		$this->load->model("M_tis_excel_config");
    }

    public function index(){
    	$this->template->adminlte("v_sync_tis");
    }

    public function do_sync(){
        $sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
        if(isset($_FILES['DATA_EXCEL'])){ //JIKA UPLOAD MANUAL
			$config['upload_path']= './assets/uploads/tis/';
        	$config['allowed_types'] = 'jpg|jpeg|png|xls|xlsx|csv|pdf';
			
			$this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('DATA_EXCEL')){
                $DATA_EXCEL = $this->upload->data();
                $filename = $DATA_EXCEL["file_name"];
                $type_sync = "UPLOAD ULANG";
            }
            $this->read_excel_to_sync($config['upload_path'].$filename, $type_sync, $user_in);
        } else { // MANUAL CRONJOB
            $type_sync = "MANUAL SYNC";
            $get_config_excel = $this->M_tis_excel_config->datalist();
            foreach($get_config_excel as $conf){
				file_put_contents('./assets/uploads/tis/tmp/filename.xlsx', 
					file_get_contents($conf['URL'])
				);
                $filename = './assets/uploads/tis/tmp/filename.xlsx';
                $this->read_excel_to_sync($filename, $type_sync, $user_in);
            }
        }
    }

    public function read_excel_to_sync($inputFileName, $type_sync, $user_in = ''){
        try {
            $inputFileType = IOFactory::identify($inputFileName);
			$objReader = IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestDataRow('A');
		$highestColumn = $sheet->getHighestColumn();

		$company = $this->M_sync_tis->get_company();
		$plant = $this->M_sync_tis->get_plant();
		$group_area = $this->M_sync_tis->get_group_area();
		$area = $this->M_sync_tis->get_area();
		$product = $this->M_sync_tis->get_product();
		$component = $this->M_sync_tis->get_component();
		$insert = 0;
		$update = 0;

		$headerData = $sheet->rangeToArray('E1:'.$highestColumn.'1', NULL, TRUE, FALSE); //GET COMPONENT IN EXCEL

		$i = 1;
		for ($row = 2; $row <= $highestRow; $row++){

			$j = 1;
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

			$date_data = $objPHPExcel->getSheet(0)->getCell('G'.$row)->getCalculatedValue();
            $date_data = str_replace(".", "-", $date_data); // CHANGE DATE EXCEL FROM 01.01.2020 TO 01-01-2020
			$date_data = date('Y-m-d H:m:s', strtotime($date_data));

            $c = $this->searchForId(trim($rowData[0][0]), 'NM_COMPANY', $company);
			$p = $this->searchForId(trim($rowData[0][1]), 'NM_PLANT', $plant);
			$ga = $this->searchForId(trim($rowData[0][2]), 'NM_GROUPAREA', $group_area);
			$a = $this->searchForId(trim($rowData[0][3]), 'NM_AREA', $area);
            
            if($rowData[0][0] != ''){
				$data = array(
					"ID_COMPANY" => $company[$c]['ID_COMPANY'],
					"ID_PLANT" => $plant[$p]['ID_PLANT'],
					"ID_GROUPAREA" => $group_area[$ga]['ID_GROUPAREA'],
					"ID_AREA" => $area[$a]['ID_AREA'],
					"TYPE" => $rowData[0][5]
				);
			}

			$check_existing = $this->M_sync_tis->get_data_t_prod_tis($data, $date_data);
			if($check_existing){
				$update++;
				$do_insert_t_prod_tis = $check_existing[0]['ID_PRODUCTION_TIS'];
				$do_update_t_prod_tis = $this->M_sync_tis->update_data_t_prod_tis($data, $date_data, $check_existing[0]['ID_PRODUCTION_TIS']);

				$id = $do_update_t_prod_tis;
			} else {
				$insert++;
				$do_insert_t_prod_tis = $this->M_sync_tis->add_data_t_prod_tis($data, $date_data);
				$id = $do_insert_t_prod_tis->ID_PRODUCTION_TIS;
			}

			$this->M_sync_tis->delete_data_d_prod_tis($id);
			for($col = 'F'; $col <= $highestColumn; $col++){
				if($j < (count($component) + 1)){
					$value_compt = $objPHPExcel->getSheet(0)->getCell($col.$row)->getCalculatedValue();
					$comp = $this->searchForId(trim(strtoupper($headerData[0][$j])), 'KD_COMPONENT', $component);
					if($comp != null){
						$data_d = array(
							"ID_PRODUCTION_TIS" => $id,
							"ID_COMPONENT" => $component[$comp]['ID_COMPONENT'],
							"NILAI" => (double) round($value_compt, 4),
							"NO_FIELD" => ($j - 1)
						);
						$collect_data_d[] = $data_d;
					}                    	
				}
				$j++;
			}
			$i++;
		}

		$do_insert_d_prod_tis = $this->M_sync_tis->add_data_d_prod_tis($collect_data_d);

		$data_sync = array(
			"TYPE" => $type_sync,
			"USER_SYNC" => $user_in->FULLNAME, 
			"TOTAL_ADD_DATA" => $insert,
			"TOTAL_UPDATE_DATA" => $update
		);

		$do_insert_sync_tis = $this->M_sync_tis->add_data_sync_tis($data_sync);

		if($do_insert_sync_tis){
			to_json(array("status" => "200", "data" => $data_sync));
		} else {
			to_json(array("status" => "404"));
		}
    }

    public function searchForId($id, $colum, $array) {
    	foreach ($array as $key => $val) {
    		if ($val[$colum] === $id) {
    			return $key;
    		}
    	}
    	return null;
    }
}