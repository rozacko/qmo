<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup_pelaksanaan_up extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_setup_pelaksanaan_up","setup");
		
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
        $user_in = $sesi_user['USER'];
		
        $this->list_pic = $this->setup->data_pic();
        $this->list_periode = $this->setup->data_periode();
		$this->list_comodity = $this->setup->data_comodity();
		
		//print_r($this->db->last_query());
		//exit();

		$this->list_data = $this->setup->get_list();
		
		$this->template->adminlte("v_setup_pelaksanaan_up");
    }

    public function pelaksanaan($id_pp){
        $this->list_periode = $this->setup->data_periode();
		$this->list_comodity = $this->setup->data_comodity();
		$this->list_data = $this->setup->get_list($id_pp);
		$this->template->adminlte("v_setup_pelaksanaan_up");
    }
    
    public function create(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_pic		 = $this->input->post('ID_PIC');
		$id_pp 		 = $this->input->post('ID_PP');
		$id_comodity = $this->input->post('ID_KOMODITI');
		
		$save = $this->setup->save_proficiency(); 
		
		if($save){
			$this->notice->success("Data Created.");
			redirect("setup_pelaksanaan_up");
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up");
		} 
	}
	
	public function do_edit(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$update = $this->setup->update_proficiency(); 
		
		if($update){ 
			$this->notice->success("Data Edited.");
			redirect("setup_pelaksanaan_up");
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up");
		}
	}
	
	public function hapus($id_proficiency){
		$hapus = $this->setup->hapus_proficiency($id_proficiency); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("setup_pelaksanaan_up");
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up");
		}
	}
	
	public function activity($id_pro){
		$this->data_detil = $this->setup->get_data_by_id($id_pro);

		$this->data_activity = $this->setup->get_data_activity($id_pro);
		$this->template->adminlte("v_activity_up");
	}
	
	public function create_activity(){
		$id_proficiency = $this->input->post('PROFICIENCY');
		
		$save_act = $this->setup->save_activity($id_proficiency);
		if($save_act){
			$this->notice->success("Data Created.");
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		} 
	}
	
	public function do_edit_activity(){
		$id_proficiency = $this->input->post('ID_PRO');
		$update_act = $this->setup->update_activity();
		//print_r($this->db->last_query());
		//exit();
		
		if($update_act){
			$this->notice->success("Data Edited.");
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		} 
		
	}
	
	public function do_hapus_activity($id_activity, $id_proficiency){
		$hapus = $this->setup->hapus_activity($id_activity); 
		
		if($hapus){ 
			$this->notice->success("Data Deleted.");
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		} else {
			$this->notice->error($this->setup->error());
			redirect("setup_pelaksanaan_up/activity/".$id_proficiency);
		}
	}

}

?>