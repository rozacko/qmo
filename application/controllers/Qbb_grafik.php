<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qbb_grafik extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("M_qqb_grafik");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];

		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_qbb_grafik");
    }

    public function ajax_get_plant_by_company(){
    	$company_ids = (NULL==$this->input->get('company_ids'))? [0]:$this->input->get('company_ids');

        $c_plant = $this->M_qqb_grafik->data_plant_company(null, null);
		to_json($c_plant);
    }

    public function get_chart(){
        $month_start = $this->input->post('month_start');
        $year_start = $this->input->post('year_start');
        $month_end = $this->input->post('month_end');
        $year_end = $this->input->post('year_end');
        
        $month_start = str_pad($month_start, 2, '0', STR_PAD_LEFT);
        $month_end = str_pad($month_end, 2, '0', STR_PAD_LEFT);

        $date_start = date('d/m/Y', strtotime("01-{$month_start}-{$year_start}"));
        $date_end = date('t/m/Y', strtotime("01-{$month_end}-{$year_end}"));

        $company = $this->input->post('companys');
        $plant = $this->input->post('plants');

    	$get_data = $this->M_qqb_grafik->get_data_qbb_chart($plant, $date_start, $date_end);
        $get_plant = $this->M_qqb_grafik->data_plant_company(null, $plant);

    	if($get_data){
            $status = "200";
    		$dt_y_axis = array();
    		foreach($get_data as $dt){
    			array_push($dt_y_axis, $dt['PERIODE']);
    		}
    		$dt_y_axis = array_values(array_unique($dt_y_axis));

    		$component = array("TOTAL MOISTURE", "INHERENT MOISTURE", "ASH CONTENT", "CALORI AR", "CALORI ADB");
            foreach ($get_plant as $dt_p) {
                $data['title'] = $dt_p['NAMA_PLANT'];
                $data['value'] = array();

                foreach ($component as $dt_c) {
                    $data_val['name'] = $dt_c;
                    $data_val['data'] = array();
                    $value = 0;
                    foreach ($dt_y_axis as $dt_y) {
                        foreach ($get_data as $dt) {
                            if($dt_y == $dt['PERIODE'] && $dt['KD_PLANT'] == $dt_p['KD_PLANT']){
                                $value = round((double)$dt[str_replace(" ", "_", $dt_c)], 2);
                            }
                        }
                        array_push($data_val['data'], $value);
                    }
                    array_push($data['value'], $data_val);
                }
                $json[] = $data;
            }
    		
    	} else {
    		$status = "404";
    		$dt_y_axis = array();
    		$json = array();
    	}

    	to_json(array("status" => $status, "data" => $json, "dt_y_axis" => $dt_y_axis));
    }
}

?>