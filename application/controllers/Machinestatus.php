<?php

class Machinestatus extends QMUser {
	
	public $list_data = array();
	public $data_machinestatus;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_machinestatus");
	}
	
	public function index(){
		$this->list_machinestatus = $this->m_machinestatus->datalist();
		$this->template->adminlte("v_machinestatus");
	}
	
	public function add(){
		$this->template->adminlte("v_machinestatus_add");
	}
	
	public function create(){
		$this->m_machinestatus->insert($this->input->post());
		if($this->m_machinestatus->error()){
			$this->notice->error($this->m_machinestatus->error());
			redirect("machinestatus/add");
		}
		else{
			$this->notice->success("Machinestatus Data Saved.");
			redirect("machinestatus");
		}
	}
	
	public function edit($ID_MACHINESTATUS){
		$this->data_machinestatus = $this->m_machinestatus->get_data_by_id($ID_MACHINESTATUS);
		$this->template->adminlte("v_machinestatus_edit");
	}
	
	public function update($ID_MACHINESTATUS){
		$this->m_machinestatus->update($this->input->post(),$ID_MACHINESTATUS);
		if($this->m_machinestatus->error()){
			$this->notice->error($this->m_machinestatus->error());
			redirect("machinestatus/edit/".$ID_MACHINESTATUS);
		}
		else{
			$this->notice->success("Machinestatus Data Updated.");
			redirect("machinestatus");
		}
	}
	
	public function delete($ID_MACHINESTATUS){
		$this->m_machinestatus->delete($ID_MACHINESTATUS);
		if($this->m_machinestatus->error()){
			$this->notice->error($this->m_machinestatus->error());
		}
		else{
			$this->notice->success("Machinestatus Data Removed.");
		}
		redirect("machinestatus");
	}

	public function get_list(){
		$list = $this->m_machinestatus->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_MESIN_STATUS;
			$row[] = $no;
			$row[] = $column->KD_MESIN_STATUS;
			$row[] = $column->NM_MESIN_STATUS;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_machinestatus->count_all(),
            "recordsFiltered" => $this->m_machinestatus->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
