<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skoring extends QMUser {

	public $LIST_BATASAN;
	public $LIST_COMPANY;
	public $LIST_INDIKATOR;
	public $LIST_INPUT;
	public $COUNT_COMPANY;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_aspek');
		$this->load->model('M_skoring');
		$this->load->model('M_company');
		$this->load->model('M_indikator');
		$this->load->model('M_batasan');
		$this->load->model('T_skoring_ramah');
	}
	
	public function index(){
		//$this->LIST_COMPANY = $this->M_company->list_company_auth($this->USER->ID_COMPANY);
		$this->LIST_COMPANY = $this->M_company->list_company();
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT = $this->M_skoring->get_list();
		#echo $this->db->last_query();die();
		$this->template->adminlte("v_skoring");
	}

	public function input(){
		exit();
		$id_batasan = $this->input->post('ID_BATASAN');
		$this->LIST_BATASAN = $this->M_skoring->get_by_id($id_batasan);
		$this->LIST_COMPANY = $this->M_company->list_company();
		$this->template->adminlte("v_skoring_input");
	}

	public function save(){
		$batasan  = $this->input->post('ID_BATASAN');
		$this->LIST_COMPANY = $this->M_company->list_company_auth($this->USER->ID_COMPANY);

		foreach ($batasan as $id_batasan) {
			foreach ($this->LIST_COMPANY as $company) {
				$date = $this->input->post('MONTH') . "/" . $this->input->post('YEAR');
				$data['NILAI_ASPEK'] 	= $this->input->post("NILAI_" . $id_batasan . $company->ID_COMPANY);
				$data['NILAI_ASPEK'] 	= ($data['NILAI_ASPEK']=='') ? NULL : $data['NILAI_ASPEK'];
				$data['NILAI_SKORING']  = $this->M_indikator->get_skor($id_batasan, $data['NILAI_ASPEK']);

				$data['ID_COMPANY']		= $company->ID_COMPANY;
				$data['ID_BATASAN']		= $id_batasan;

				$exist = $this->T_skoring_ramah->check_exists($id_batasan, $company->ID_COMPANY, $date);

				if($exist===0) {
					$this->T_skoring_ramah->insert($data, $date);
				}else{
					$where['ID_BATASAN'] 	= $id_batasan;
					$where['ID_COMPANY']	= $company->ID_COMPANY;

					$upd['NILAI_ASPEK']		= $data['NILAI_ASPEK'];
					$upd['NILAI_SKORING']	= $data['NILAI_SKORING'];
					$this->T_skoring_ramah->update_where($where, $upd, $date);
				}
			}
		}
		
		$this->notice->success("Input Saved.");
		redirect("skoring");
	}

	public function get_list(){
		$list = $this->M_skoring->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_BATASAN;
			$row[] = $no;
			$row[] = $column->KRITERIA;
			$row[] = $column->BATASAN;
			$row[] = $column->ASPEK;
			$row[] = $column->JENIS_ASPEK;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_skoring->count_all(),
            "recordsFiltered" => $this->M_skoring->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function get_nilai($date){
		$date = str_replace("-", "/", $date);
		$nilai = $this->T_skoring_ramah->get_nilai("ALL", $date);
		to_json($nilai);
	}	

}

/* End of file Skoring.php */
/* Location: ./application/controllers/Skoring.php */
?>
