<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_table extends QMUser
{

    public $data;
    public $NM_AREA;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper("string");
        $this->load->model("m_company");
        $this->load->model("c_parameter");
        $this->load->model("m_component");
        $this->load->model("m_machinestatus");
        $this->load->model("t_cement_daily");
        $this->load->model("t_cement_hourly");
        $this->load->model("t_production_daily");
        $this->load->model("t_production_hourly");
        $this->load->model("d_cement_daily");
        $this->load->model("d_cement_hourly");
        $this->load->model("d_production_daily");
        $this->load->model("d_production_hourly");
        $this->load->model("m_area");
        $this->load->model("m_plant");
        $this->load->model("c_product");
        $this->load->model("m_clinker_factor");
        $this->load->model("c_range_qaf");
        $this->load->model("m_index");
        //updated by rudi
        $this->load->model("t_production_tis");
        $this->load->model("d_production_tis");
    }

    public function index()
    {
        $this->libExternal('datepicker');
        $this->libExternal('select2');
        $this->list_company = $this->m_company->datalist();
        $this->template->adminlte("v_data_table", $data);
    }

    private function data_daily()
    {

        $data = $this->input->post();

        $json = null;

        $this->load->model("m_area");
        $this->load->model("m_grouparea");
        $this->load->model("c_parameter");

        //get th
        $cpar['ID_PLANT']         = $data['ID_PLANT'];
        $cpar['DISPLAY']        = 'D';
        $cpar['ID_GROUPAREA']     = $this->m_area->get_data_by_id($data['ID_AREA'])->ID_GROUPAREA;
        $nm_grouparea            = $this->m_grouparea->get_data_by_id($cpar['ID_GROUPAREA'])->NM_GROUPAREA;
        $json['th']             = $this->c_parameter->data($cpar); #die(var_dump($this->c_parameter->get_sql()));
        //$mod = (trim($nm_grouparea)=="FINISH MILL" || trim($nm_grouparea)=="CLINKER") ? "t_cement_daily":"t_production_daily";
        //var_dump($json['th']);

        //get tr


        $tproduction = (in_array($cpar['ID_GROUPAREA'], array(1, 4))) ? "t_cement_daily" : "t_production_daily";
        echo $tproduction;
        $month = "'" . $data['MONTH'] . "/" . $data['YEAR'] . "'";
        //$d = $this->{$mod}->table($month,$data['ID_AREA']);
        $d = $this->$tproduction->table($month, $data['ID_AREA']);
        #echo $this->t_production_daily->get_sql();
        #var_dump($d);
        $n = array();
        foreach ($d as $i => $r) {
            $n[$r->DATE_DATA][$r->ID_COMPONENT] = $r->NILAI;
        }

        foreach ($d as $i => $r) {
            $n[$r->DATE_DATA]['USER_ENTRY']   = $r->USER_ENTRY;
            $n[$r->DATE_DATA]['USER_UPDATE']  = $r->USER_ENTRY;
            $n[$r->DATE_DATA]['DATE_ENTRY']   = $r->DATE_ENTRY;
            $n[$r->DATE_DATA]['DATE_DATA']    = $r->DATE_DATA;
            $n[$r->DATE_DATA]['MESIN_STATUS'] = $r->NM_MESIN_STATUS;
            $n[$r->DATE_DATA]['MESIN_REMARK'] = $r->MESIN_REMARK;
        }

        $json['tr'] = (object)$n;
        //var_dump($json['tr']);
        return $json;
    }

    private function data_hourly()
    {
        $this->load->model("t_production_hourly");

        $data = $this->input->post();

        #var_dump($data);

        $json = null;

        $this->load->model("m_area");

        //get th
        $cpar['ID_PLANT']         = $data['ID_PLANT'];
        $cpar['ID_GROUPAREA']     = $this->m_area->get_data_by_id($data['ID_AREA'])->ID_GROUPAREA;
        $this->load->model("c_parameter");
        $json['th'] = $this->c_parameter->data($cpar);


        //get tr
        $data['TANGGAL'] = "'" . $data['TANGGAL'] . "'";
        $this->load->model("t_production_daily");
        $d = $this->t_production_hourly->table($data['TANGGAL'], $data['ID_AREA']);


        $n = array();
        foreach ($d as $i => $r) {
            $n[$r->JAM_DATA][$r->ID_COMPONENT] = $r->NILAI;
        }

        foreach ($d as $i => $r) {
            $n[$r->JAM_DATA]['MESIN_STATUS'] = $r->NM_MESIN_STATUS;
            $n[$r->JAM_DATA]['MESIN_REMARK'] = $r->MESIN_REMARK;
        }

        $json['tr'] = (object)$n;
        //var_dump($json['tr']);
        return $json;
    }

    public function daily()
    {
        // ---------- start 03.04.2021 izza ----------
        $post = $this->input->post();
        $check_use_tis = $this->m_company->get_company_use_detail_plant($this->input->post("ID_COMPANY")); // updated by rudi
        if ($check_use_tis) {
            $data = $this->load_tis();
        } else {
            if (in_array($this->input->post("ID_GROUPAREA"), array(1, 4, 81))) {
                $jenis = 'CEMENT';
            } else {
                $jenis = 'PRODUCTION';
            }
            $data = $this->load_daily_cement_production($jenis, $post);
        }
        echo ret_json($data);

        // if(in_array($this->input->post("ID_GROUPAREA"),array(1,4, 81))){
        // 	$data = $this->load_cement_daily();
        // }
        // else{
        // 	$data = $this->load_table_daily();
        // }
        // echo $data;
        // ---------- end 03.04.2021 izza ----------
    }

    public function hourly()
    {
        if (in_array($this->input->post("ID_GROUPAREA"), array(1, 4, 81))) {
            $data = $this->load_cement_hourly();
        } else {
            $data = $this->load_table_hourly();
        }
        echo $data;
    }

    public function load_tis()
    {
        $this->load->model("m_user");
        $post = $tmp = $this->input->post();
        $form = (object)$tmp;

        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";

        $colHeader     = array();
        $data         = array();
        $data_footer = array();
        $footer = array();
        $tgl["START"] = $form->START;
        $tgl["END"] = $form->END;

        $t_prod = $this->t_production_tis->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ")");
        $colHeader = $this->get_component_tis('D', TRUE, $form->START, $form->END, $idarea);

        $ctHead = count($colHeader['id_comp']) - 9;

        $result['last_query_tprod'] = $this->db->last_query();
        $result['t_prod'] = $t_prod;

        foreach ($colHeader['colHeader'] as $key) {
            $dummy[trim($key)] = '';
        }

        if (!empty($t_prod)) {
            $index = 0; // ini buat ganti nilai nya sesuai db
            foreach ($t_prod as $key => $row) { #print_r($row);
                $data[$index] = $dummy; // ini inisialisasi biar data nya kosong
                $d_prod = $this->d_production_tis->get_by_id($row->ID_PRODUCTION_TIS);
                $temp_d[] = $d_prod;
                foreach ($d_prod as $k => $vl) {
                    $KD_COMPONENT = trim(strtoupper($vl->KD_COMPONENT));
                    // $data[$index][$KD_COMPONENT] = '';
                    if (in_array($KD_COMPONENT, $colHeader['colHeader'])) {
                        $NILAI = ($vl->NILAI == '' || $vl->NILAI == NULL) ? '' : (float) $vl->NILAI;
                        $data[$index][$KD_COMPONENT] = $NILAI;
                        $data_footer[$KD_COMPONENT][] = $NILAI == '' ? 0 : $NILAI;
                    }
                }
                $machine_status = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $USER_ENTRY = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $USER_UPDATE = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;

                $data[$index]['DATE'] = $row->DATE_DATA;
                $data[$index]['MACHINE STATUS'] = $machine_status == NULL ? '' : $machine_status;
                $data[$index]['TYPE'] = $row->KD_PRODUCT == NULL ? '' : $row->KD_PRODUCT;
                $data[$index]['PLANT'] = $row->NM_PLANT;
                $data[$index]['AREA'] = $row->NM_AREA;
                $data[$index]['REMARK'] = $row->MESIN_REMARK == NULL ? '' : $row->MESIN_REMARK;
                $data[$index]['DATE ENTRY'] = $row->DATE_ENTRY;
                $data[$index]['USER ENTRY'] = $USER_ENTRY == NULL ? 'SYSTEM' : $USER_ENTRY;
                $data[$index]['USER UPDATE'] = $USER_UPDATE == NULL ? '' : $USER_UPDATE;

                $index++;
            }
        }

        $min = $max = $avg = $dev = $dummy;
        $min['DATE'] = 'MIN';
        $max['DATE'] = 'MAX';
        $avg['DATE'] = 'AVG';
        $dev['DATE'] = 'DEV';
        $min_temp = array();

        if (count($data_footer) > 0) {

            foreach ($data_footer as $key => $value) {
                foreach ($value as $value2) {
                    if ($value2 != 0) $min_temp[$key][] = $value2;
                }
                $min[$key] = number_format(min($min_temp[$key]), 2);

                $max[$key] = number_format(max($value), 2);
                // avg
                $jml = 0;
                foreach ($value as $key2) {
                    $jml += $key2;
                }
                $rata = ($jml) / count($min_temp[$key]);
                if (is_nan($rata)) $rata = 0;
                $avg[$key] = number_format($rata, 2);

                $dev_temp = 0;
                foreach ($min_temp[$key] as $key2) {
                    $dev_temp += (($key2 - $rata) * ($key2 - $rata));
                }
                $dev_temp = $dev_temp / (count($min_temp[$key]) - 1);
                $dev_temp = $dev_temp == 0 ? 0 : sqrt($dev_temp);
                if (is_nan($dev_temp) || $rata == 0) $dev[$key] = '-';
                else $dev[$key] = number_format($dev_temp, 2);
            }
        }
        $footer[] = $min;
        $footer[] = $max;
        $footer[] = $avg;
        $footer[] = $dev;

        $result['tgl']      = $tgl;
        $result['header']   = $colHeader;
        $result['footer']   = $footer;
        $result['data']     = $data;
        return $result;
    }

    private function get_component_tis($display = '', $tipe = FALSE, $date_start = '', $date_end = '', $area = '')
    {
        $header[] = 'DATE';
        if ($display == 'H') {
            $header[] = 'TIME';
        }
        $comp = $this->m_component->get_component_from_tis($date_start, $date_end, $area);
        $id_comp[] = '_date';
        foreach ($comp as $c) {
            $id_comp[] = $c->KD_COMPONENT;
            $header[] = trim(strtoupper($c->KD_COMPONENT));
        }

        //Tambahkan Remark
        if (!empty($tipe))
            $header[] = 'MACHINE STATUS';
        $header[] = 'TYPE';
        $header[] = 'PLANT';
        $header[] = 'AREA';
        $header[] = 'REMARK';
        $header[] = 'DATE ENTRY';
        $header[] = 'USER ENTRY';
        $header[] = 'DATE UPDATE';
        $header[] = 'USER UPDATE';



        if (!empty($tipe)) $id_comp[] = '_machine_status';
        $id_comp[] = '_type';
        $id_comp[] = '_plant';
        $id_comp[] = '_area';
        $id_comp[] = '_remark';
        $id_comp[] = '_date_entry';
        $id_comp[] = '_user_entry';
        $id_comp[] = '_date_update';
        $id_comp[] = '_user_update';

        //Var
        $data['colHeader']     = $header;    //Set header
        $data['id_comp']     = $id_comp;    //Set header
        // var_dump($data);
        return $data;
    }

    public function export()
    {
        // $this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
        if ($this->input->post("DISPLAY") == 'daily') {
            $this->export_daily();
        } else {
            $this->export_hourly();
        }
    }

    private function export_daily()
    {

        // ---------- start 03.04.2021 izza ----------
        // if(in_array($this->input->post("ID_GROUPAREA"),array(1,4, 81))){
        // 	// $data = $this->load_cement_daily();
        // 	$data = $this->load_cement_daily_old();
        // }
        // else{
        // 	$data = $this->load_table_daily();
        // }
        // $this->data = $data;
        // $this->template->nostyle('data_table_daily');

        $post = $this->input->post();

        if (in_array($this->input->post("ID_GROUPAREA"), array(1, 4, 81))) {
            $jenis = 'CEMENT';
        } else {
            $jenis = 'PRODUCTION';
        }

        $data = $this->load_daily_cement_production($jenis, $post);
        echo ret_json($data);
        $this->load->view("nostyle/export_data_table_daily", $data);
        // ---------- end 03.04.2021 izza ----------

    }

    private function export_hourly()
    {
        if (in_array($this->input->post("ID_GROUPAREA"), array(1, 4, 81))) {
            $data = $this->load_cement_hourly();
        } else {
            $data = $this->load_table_hourly();
        }
        $this->data = $data;
        $this->template->nostyle('data_table_hourly');
    }

    public function ajax_get_plant($ID_COMPANY = NULL)
    {
        $this->load->model("m_plant");
        $plant = $this->m_plant->datalist($ID_COMPANY);
        to_json($plant);
    }

    public function ajax_get_grouparea()
    {
        $this->load->model("m_area");
        $ID_COMPANY   = $this->input->post('id_company');
        $ID_PLANT   = $this->input->post('plant');
        // $area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
        $area = $this->m_area->grouplist_multi($ID_COMPANY, $ID_PLANT, $this->USER->ID_AREA);
        to_json($area);
    }

    public function ajax_get_area()
    {
        $this->load->model("m_area");
        $ID_COMPANY   = $this->input->post('id_company');
        $ID_PLANT   = $this->input->post('plant');
        $ID_GROUPAREA   = $this->input->post('grouparea');
        $area = $this->m_area->datalist_multi($ID_COMPANY, $ID_PLANT, $ID_GROUPAREA); #echo $this->m_area->get_sql();
        // $area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
        to_json($area);
    }


    public function load_table_daily()
    {
        $this->load->model("m_user");

        $post = $tmp = $this->input->post();

        //Convert array to object
        $form = (object)$tmp;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";
        //Load from T_production_daily
        $colHeader     = array();
        $data         = array();
        $t_prod     = $this->t_production_daily->data_where("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy')  AND a.ID_AREA in (" . $idarea . ") ");
        $colHeader     = $this->get_component('D', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
        $ctHead        = count($colHeader['id_comp']) - 9;

        if (!empty($t_prod)) {
            foreach ($t_prod as $key => $row) {
                $data[$key][] =  strtoupper(date('d-M-Y', strtotime($row->TANGGAL)));
                $d_prod = $this->d_production_daily->get_by_id($row->ID_PRODUCTION_DAILY);
                foreach ($d_prod as $k => $vl) {
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $data[$key][] = ($vl->NILAI == '') ? '&nbsp;' : (float) $vl->NILAI;
                    }
                }
                if ($ctHead > count($d_prod)) {
                    for ($i = 0; $i < (($ctHead) - (count($d_prod))); $i++) {
                        $data[$key][] = "";
                    }
                }
                $data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $data[$key][] = $row->KD_PRODUCT;
                $data[$key][] = $row->NM_PLANT;
                $data[$key][] = $row->NM_AREA;
                $data[$key][] = $row->MESIN_REMARK;
                $data[$key][] = $row->DATE_ENTRY;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE) ? $row->DATE_ENTRY : $row->DATE_UPDATE;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
            }
        }

        $result['header'] = $colHeader;
        $result['data']   = $data;
        return ret_json($result);
    }

    public function load_table_hourly()
    {
        $this->load->model("m_user");

        $post = $tmp = $this->input->post();
        //Convert array to object
        $form = (object)$tmp;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";

        //Load from T_production_hourly
        $colHeader     = array();
        $data         = array();
        // $t_prod 	= $this->t_production_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA in (" .$idarea.") ");
        $t_prod     = $this->t_production_hourly->data_where("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy')  AND a.ID_AREA in (" . $idarea . ") "); // izza

        $colHeader     = $this->get_component('H', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
        $ctHead        = count($colHeader['id_comp']) - 9;

        if (!empty($t_prod)) {
            foreach ($t_prod as $key => $row) {
                $data[$key][] = $row->DATE_DATA;
                $data[$key][] = $row->JAM_DATA;
                $d_prod = $this->d_production_hourly->get_by_id($row->ID_PRODUCTION_HOURLY);
                foreach ($d_prod as $k => $vl) {
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $data[$key][] = ($vl->NILAI == '') ? '' : (float) $vl->NILAI;
                    }
                }
                if ($ctHead > count($d_prod)) {
                    for ($i = 0; $i < (($ctHead) - (count($d_prod))); $i++) {
                        $data[$key][] = "";
                    }
                }
                $data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $data[$key][] = $row->KD_PRODUCT;
                $data[$key][] = $row->NM_PLANT;
                $data[$key][] = $row->NM_AREA;
                $data[$key][] = $row->MESIN_REMARK;
                $data[$key][] = $row->DATE_ENTRY;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE) ? $row->DATE_ENTRY : $row->DATE_UPDATE;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
            }
        }

        $result['header'] = $colHeader;
        $result['data']   = $data;
        return ret_json($result);
    }

    public function load_cement_daily()
    {
        $this->load->model("m_user");
        $post = $tmp = $this->input->post();
        //Convert array to object
        $form = (object)$tmp;
        $result["tgl"]["START"] = $form->START;
        $result["tgl"]["END"] = $form->END;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";
        //Load from T_cement_daily
        $colHeader     = array();
        $data         = array();
        if ($form->ID_GROUPAREA == 4) {
            //$t_prod = $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."'");
            // $t_prod = $this->t_cement_daily->data_where_prod("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."'");
            $t_prod = $this->t_cement_daily->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ")");
        } else {
            $prod = ($form->ID_PRODUCT == 'ALL') ? "" : "AND a.ID_PRODUCT='" . $form->ID_PRODUCT . "'";
            //$t_prod = $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
            // $t_prod = $this->t_cement_daily->data_where_prod("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."' $prod");
            $t_prod = $this->t_cement_daily->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy')  AND a.ID_AREA in (" . $idarea . ") $prod");
        }

        // echo $this->db->last_query();
        // die;

        $colHeader     = $this->get_component('D', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
        // echo $this->db->last_query();

        $ctHead        = count($colHeader['id_comp']) - 9;


        if (!empty($t_prod)) {
            foreach ($t_prod as $key => $row) { #print_r($row);
                $data[$key][] =  strtoupper(date('d-M-Y', strtotime($row->TANGGAL)));
                $d_prod = $this->d_cement_daily->get_by_id($row->ID_CEMENT_DAILY);
                // echo $this->db->last_query();
                foreach ($d_prod as $k => $vl) {
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $data[$key][] = ($vl->NILAI == '' and $vl->ID_COMPONENT == '18') ? '&nbsp;' : (float) $vl->NILAI;
                        // $data[$key][] = (float) $vl->NILAI;
                        // if($vl->ID_COMPONENT == '18'){
                        // 	$ctHead = $ctHead1 - 8;	
                        // }else{
                        // 	$ctHead = $ctHead1 - 9;	
                        // }
                    }
                }
                // echo $row->ID_COMPONENT.'<br>';
                // echo $ctHead .'>='. count($d_prod);
                if ($ctHead >= count($d_prod)) {
                    for ($i = 0; $i < (($ctHead) - (count($d_prod))); $i++) {
                        $data[$key][] = "";
                    }
                }

                // $machine_status = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS);
                $data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $data[$key][] = $row->KD_PRODUCT;
                $data[$key][] = $row->NM_PLANT;
                $data[$key][] = $row->NM_AREA;
                $data[$key][] = $row->MESIN_REMARK;
                $data[$key][] = $row->DATE_DATA;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $data[$key][] = $row->DATE_ENTRY;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
            }
        }

        $result['header'] = $colHeader;
        $result['data']   = $data;
        return ret_json($result);
    }

    public function load_cement_hourly()
    {
        $this->load->model("m_user");

        $post = $tmp = $this->input->post();

        //Convert array to object
        $form = (object)$tmp;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";

        //Load from T_cement_hourly
        $colHeader     = array();
        $data         = array();
        if ($form->ID_GROUPAREA == 4) {
            // $t_prod 	= $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA in (" .$idarea.") ");
            $t_prod     = $this->t_cement_hourly->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ") AND a.ID_PRODUCT IS NOT NULL"); // izza
            #$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
        } else {
            $prod     = ($form->ID_PRODUCT == 'ALL') ? "" : "AND a.ID_PRODUCT='" . $form->ID_PRODUCT . "'";
            // $t_prod = $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA in (" .$idarea.") $prod");	
            $t_prod = $this->t_cement_hourly->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ") $prod"); // izza
            #$t_prod = $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
        }
        // echo $this->db->last_query();
        $colHeader     = $this->get_component('H', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
        $ctHead        = count($colHeader['id_comp']) - 9;
        // echo '<pre>';
        // print_r($t_prod);
        // echo '</pre>';
        if (!empty($t_prod)) {
            foreach ($t_prod as $key => $row) {
                $data[$key][] = $row->DATE_DATA;
                $data[$key][] = $row->JAM_DATA;
                $d_prod = $this->d_cement_hourly->get_by_id2($row->ID_CEMENT_HOURLY, $form->ID_GROUPAREA);
                foreach ($d_prod as $k => $vl) {
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $data[$key][] = ($vl->NILAI == '') ? '' : (float) $vl->NILAI;
                    }
                }
                if ($ctHead > count($d_prod)) {
                    for ($i = 0; $i < (($ctHead) - (count($d_prod))); $i++) {
                        $data[$key][] = "";
                    }
                }

                $data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $data[$key][] = $row->KD_PRODUCT;
                $data[$key][] = $row->NM_PLANT;
                $data[$key][] = $row->NM_AREA;
                $data[$key][] = $row->MESIN_REMARK;
                $data[$key][] = $row->DATE_ENTRY;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE) ? $row->DATE_ENTRY : $row->DATE_UPDATE;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
            }
        }

        $result['header'] = $colHeader;
        $result['data']   = $data;
        return ret_json($result);
    }

    // start izza
    public function coba()
    {
        // dev
        // dev = sum( (dataKEi - avg)^2 ) ------ 1
        // dev = dev / (n-1) ------ 2
        // dev = (dev)^(1/2) ------ 3

        $dev = 0; // 1
        $n = 1;
        $hasil = $dev / ($n - 1);
        // if($hasil == (float)NAN) $hasil =0;
        if (is_nan($hasil)) $hasil = 0;
        var_dump($hasil);
    }
    public function load_daily_cement_production($jenis = 'CEMENT', $array_temp = array())
    {
        $this->load->model("m_user");

        // --------- start dumy ---------
        // if($tes){
        //     $tmp["DISPLAY"] = "daily";
        //     $tmp["ID_COMPANY"] = "10";
        //     $tmp["ID_PLANT"] = "14";
        //     $tmp["ID_GROUPAREA"] = "4";
        //     $tmp["ID_AREA"][] = "67";
        //     $tmp["DATA_JAM"] = "Y";
        //     $tmp["START"] = "01/03/2021";
        //     $tmp["END"] = "31/03/2021";
        //     $tmp["ID_PRODUCT"] = "00";
        // } else{
        //     $post = $tmp = $this->input->post();
        // }
        // --------- end dumy ---------
        $tmp = $array_temp;
        //Convert array to object
        $form = (object)$tmp;
        $tgl["START"] = $form->START;
        $tgl["END"] = $form->END;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";
        //Load from T_cement_daily
        $colHeader     = array();
        $data         = array();
        $data_footer = array();
        $footer = array();
        if ($jenis == 'CEMENT') {
            if ($form->ID_GROUPAREA == 4) {
                $where2 = ($form->DATA_JAM == 'Y') ? "AND a.FROM_HOURLY IS NOT NULL" : "AND a.FROM_HOURLY IS NULL";
            } else {
                $where2 = ($form->ID_PRODUCT == 'ALL') ? "" : "AND a.ID_PRODUCT='" . $form->ID_PRODUCT . "'";
            }
            $where = "DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy')  AND a.ID_AREA in (" . $idarea . ") $where2";
            $t_prod = $this->t_cement_daily->data_where_prod($where);
        } elseif ($jenis == 'PRODUCTION') {
            $where = "DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy')  AND a.ID_AREA in (" . $idarea . ") ";
            $t_prod     = $this->t_production_daily->data_where($where);
        }
        $result['last_query_tprod'] = $this->db->last_query();

        $result['t_prod'] = $t_prod;

        $colHeader     = $this->get_component2('D', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);

        foreach ($colHeader['colHeader'] as $key) {
            $dummy[$key] = '';
        }
        if (!empty($t_prod)) {
            $index = 0; // ini buat ganti nilai nya sesuai db
            foreach ($t_prod as $key => $row) { #print_r($row);
                $data[$index] = $dummy; // ini inisialisasi biar data nya kosong
                if ($jenis == 'CEMENT') {
                    $d_prod = $this->d_cement_daily->get_by_id($row->ID_DATA);
                } elseif ($jenis == 'PRODUCTION') {
                    $d_prod = $this->d_production_daily->get_by_id($row->ID_DATA);
                }
                $temp_d[] = $d_prod;
                foreach ($d_prod as $k => $vl) {
                    $KD_COMPONENT = trim(strtoupper($vl->KD_COMPONENT));
                    // $data[$index][$KD_COMPONENT] = '';
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $NILAI = ($vl->NILAI == '' || $vl->NILAI == NULL) ? '' : (float) $vl->NILAI;
                        $data[$index][$KD_COMPONENT] = $NILAI;
                        $data_footer[$KD_COMPONENT][] = $NILAI == '' ? 0 : $NILAI;
                    }
                }
                $machine_status = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $USER_ENTRY = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $USER_UPDATE = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;

                $data[$index]['DATE'] = $row->DATE_DATA;
                $data[$index]['MACHINE STATUS'] = $machine_status == NULL ? '' : $machine_status;
                $data[$index]['TYPE'] = $row->KD_PRODUCT == NULL ? '' : $row->KD_PRODUCT;
                $data[$index]['PLANT'] = $row->NM_PLANT;
                $data[$index]['AREA'] = $row->NM_AREA;
                $data[$index]['REMARK'] = $row->MESIN_REMARK == NULL ? '' : $row->MESIN_REMARK;
                $data[$index]['DATE ENTRY'] = $row->DATE_ENTRY;
                $data[$index]['USER ENTRY'] = $USER_ENTRY == NULL ? 'SYSTEM' : $USER_ENTRY;
                $data[$index]['USER UPDATE'] = $USER_UPDATE == NULL ? '' : $USER_UPDATE;

                $index++;
            }
        }


        $min = $max = $avg = $dev = $dummy;
        $min['DATE'] = 'MIN';
        $max['DATE'] = 'MAX';
        $avg['DATE'] = 'AVG';
        $dev['DATE'] = 'DEV';
        $min_temp = array();

        if (count($data_footer) > 0) {

            foreach ($data_footer as $key => $value) {
                // $min[$key] = number_format(min($value), 2);
                foreach ($value as $value2) {
                    if ($value2 != 0) $min_temp[$key][] = $value2;
                }
                $min[$key] = number_format(min($min_temp[$key]), 2);

                $max[$key] = number_format(max($value), 2);
                // avg
                $jml = 0;
                foreach ($value as $key2) {
                    $jml += $key2;
                }
                // $rata = ($jml) / count($value);
                $rata = ($jml) / count($min_temp[$key]);
                if (is_nan($rata)) $rata = 0;
                $avg[$key] = number_format($rata, 2);

                // dev
                // dev = sum( (dataKEi - avg)^2 ) ------ 1
                // dev = dev / (n-1) ------ 2
                // dev = (dev)^(1/2) ------ 3
                $dev_temp = 0;
                // foreach ($value as $key2) {
                foreach ($min_temp[$key] as $key2) {
                    $dev_temp += (($key2 - $rata) * ($key2 - $rata));
                }
                // $dev_temp = $dev_temp / (count($value)-1);
                $dev_temp = $dev_temp / (count($min_temp[$key]) - 1);
                $dev_temp = $dev_temp == 0 ? 0 : sqrt($dev_temp);
                if (is_nan($dev_temp) || $rata == 0) $dev[$key] = '-';
                else $dev[$key] = number_format($dev_temp, 2);
            }
        }
        // $min = array_merge($min, $dummy);
        // $max = array_merge($max, $dummy);
        // $avg = array_merge($avg, $dummy);
        // $dev = array_merge($dev, $dummy);
        $footer[] = $min;
        $footer[] = $max;
        $footer[] = $avg;
        $footer[] = $dev;

        $result['header'] = $colHeader;
        $result['data']   = $data;
        $result['footer'] = $footer;
        $result['tgl'] = $tgl;

        // if($tes) echo json_encode($result);
        // else return ret_json($result);
        return $result;
    }
    public function load_hourly_cement_production()
    {
        $tes = 1;
        $this->load->model("m_user");

        // $post = $tmp = $this->input->post();
        // --------- start dumy ---------
        if ($tes) {
            $tmp["DISPLAY"] = "daily";
            $tmp["ID_COMPANY"] = "10";
            $tmp["ID_PLANT"] = "14";
            $tmp["ID_GROUPAREA"] = "4";
            $tmp["ID_AREA"][] = "67";
            // $tmp["ID_AREA"][] = "60";
            $tmp["DATA_JAM"] = "Y";
            $tmp["START"] = "01/03/2021";
            $tmp["END"] = "31/03/2021";
            $tmp["ID_PRODUCT"] = "00";
        } else {
            $post = $tmp = $this->input->post();
        }
        // --------- end dumy ---------
        //Convert array to object
        $form = (object)$tmp;
        $temp_reg = array();
        foreach ($form->ID_AREA as $key => $val) {
            $reg = $val;
            $temp_reg[] = $reg;
        }
        $idarea = "'" . join("','", $temp_reg) . "'";

        //Load from T_cement_hourly
        $colHeader     = array();
        $data         = array();
        if ($form->ID_GROUPAREA == 4) {
            // $t_prod 	= $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA in (" .$idarea.") ");

            $t_prod     = $this->t_cement_hourly->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ") ");

            #$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
        } else {
            $prod     = ($form->ID_PRODUCT == 'ALL') ? "" : "AND a.ID_PRODUCT='" . $form->ID_PRODUCT . "'";
            // $t_prod = $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA in (" .$idarea.") $prod");	
            $t_prod = $this->t_cement_hourly->data_where_prod("DATE_DATA BETWEEN TO_DATE ('" . $form->START . "', 'dd/mm/yyyy') AND TO_DATE ('" . $form->END . "', 'dd/mm/yyyy') AND a.ID_AREA in (" . $idarea . ") $prod");
            #$t_prod = $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
        }
        // echo $this->db->last_query();
        $colHeader     = $this->get_component('H', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
        $ctHead        = count($colHeader['id_comp']) - 9;
        // echo '<pre>';
        // print_r($t_prod);
        // echo '</pre>';
        if (!empty($t_prod)) {
            foreach ($t_prod as $key => $row) {
                $data[$key][] = $form->TANGGAL;
                $data[$key][] = $row->JAM_DATA;
                $d_prod = $this->d_cement_hourly->get_by_id2($row->ID_CEMENT_HOURLY, $form->ID_GROUPAREA);
                foreach ($d_prod as $k => $vl) {
                    if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
                        $data[$key][] = ($vl->NILAI == '') ? '' : (float) $vl->NILAI;
                    }
                }
                if ($ctHead > count($d_prod)) {
                    for ($i = 0; $i < (($ctHead) - (count($d_prod))); $i++) {
                        $data[$key][] = "";
                    }
                }

                $data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
                $data[$key][] = $row->KD_PRODUCT;
                $data[$key][] = $row->NM_PLANT;
                $data[$key][] = $row->NM_AREA;
                $data[$key][] = $row->MESIN_REMARK;
                $data[$key][] = $row->DATE_ENTRY;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
                $data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE) ? $row->DATE_ENTRY : $row->DATE_UPDATE;
                $data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
            }
        }

        $result['header'] = $colHeader;
        $result['data']   = $data;
        $result['t_prod']   = $t_prod;
        // return ret_json($result);
        echo json_encode($result);
    }
    // end izza

    public function ajax_get_product()
    {
        $ID_COMPANY   = $this->input->post('id_company');
        $ID_PLANT   = $this->input->post('plant');
        $ID_AREA   = $this->input->post('area');
        $product = $this->c_product->datalist_multi($ID_AREA, $ID_PLANT, $ID_COMPANY);
        to_json($product);
    }
    // start create izza jan 2021
    public function ajax_get_product_read()
    { // izza
        $ID_COMPANY   = $this->input->post('id_company');
        $ID_PLANT   = $this->input->post('plant');
        $ID_AREA   = $this->input->post('area');
        // $product= $this->c_product->datalist_multi($ID_AREA,$ID_PLANT,$ID_COMPANY);
        $product = $this->c_product->datalist_multi_read($ID_AREA, $ID_PLANT, $ID_COMPANY);
        to_json($product);
    }
    // end create izza jan 2021

    private function get_component($display = '', $id_plant = '', $id_grouparea = '', $tipe = FALSE)
    {
        $param = $this->c_parameter->configuration_multi($id_plant, $id_grouparea, $display);
        $header[]['title'] = 'DATE';
        if ($display == 'H') {
            $header[]['title'] = 'TIME';
        }
        #echo $this->db->last_query();die();
        foreach ($param as $col) {
            $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
            $id_comp[] = $cmp->ID_COMPONENT;
            // echo $cmp->ID_COMPONENT;
            $header[]['title'] = trim(strtoupper($cmp->KD_COMPONENT));
        }

        //Tambahkan Remark
        if (!empty($tipe))
            $header[]['title'] = 'MACHINE STATUS';
        $header[]['title'] = 'TYPE';
        $header[]['title'] = 'PLANT';
        $header[]['title'] = 'AREA';
        $header[]['title'] = 'REMARK';
        $header[]['title'] = 'DATE ENTRY';
        $header[]['title'] = 'USER ENTRY';
        $header[]['title'] = 'DATE UPDATE';
        $header[]['title'] = 'USER UPDATE';



        if (!empty($tipe)) $id_comp[] = '_machine_status';
        $id_comp[] = '_type';
        $id_comp[] = '_plant';
        $id_comp[] = '_area';
        $id_comp[] = '_remark';
        $id_comp[] = '_date_entry';
        $id_comp[] = '_user_entry';
        $id_comp[] = '_date_update';
        $id_comp[] = '_user_update';

        //Var
        $data['colHeader']     = $header;    //Set header
        $data['id_comp']     = $id_comp;    //Set header
        // var_dump($data);
        return $data;
    }
    private function get_component2($display = '', $id_plant = '', $id_grouparea = '', $tipe = FALSE)
    {
        $param = $this->c_parameter->configuration_multi($id_plant, $id_grouparea, $display);
        $header[] = 'DATE';
        $id_comp[] = '_date';
        #echo $this->db->last_query();die();
        foreach ($param as $col) {
            $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
            $id_comp[] = $cmp->ID_COMPONENT;
            // echo $cmp->ID_COMPONENT;
            $header[] = trim(strtoupper($cmp->KD_COMPONENT));
        }

        //Tambahkan Remark
        if (!empty($tipe)) $header[] = 'MACHINE STATUS';
        $array2 = ['TYPE', 'PLANT', 'AREA', 'REMARK', 'DATE ENTRY', 'USER ENTRY', 'USER UPDATE'];
        $header = array_merge($header, $array2);

        if (!empty($tipe)) $id_comp[] = '_machine_status';
        $array2 = ['_type', '_plant', '_area', '_remark', '_date_entry', '_user_entry', '_user_update'];
        $id_comp = array_merge($id_comp, $array2);

        //Var
        $data['colHeader']     = $header;    //Set header
        $data['id_comp']     = $id_comp;    //Set header
        // var_dump($data);
        return $data;
    }


    public function clinker_factor()
    {
        $this->libExternal("dataTables");
        $this->libExternal('datepicker');
        $this->libExternal("highcharts");
        $this->template->adminlte("v_clinker_factor");
    }


    public function table_clinker_factor()
    {

        $start   = '01/' . $this->input->post('start');
        $end   = $this->input->post('end');
        $get_company = $this->m_clinker_factor->company();
        $get_bulan = $this->t_cement_daily->bulan_where_prod_cf("DATE_DATA BETWEEN TO_DATE ('" . $start . "', 'dd/mm/yyyy') AND LAST_DAY(TO_DATE ('" . $end . "', 'mm/yyyy')) ");
        $awal = array();
        $akhir = array();
        $tigahari = array();
        $tujuhhari = array();
        $dualapanhari = array();
        $awal_non = array();
        $akhir_non = array();
        $tigahari_non = array();
        $tujuhhari_non = array();
        $dualapanhari_non = array();

        $json_3d =  array();
        $Chart_3d = array();
        $data3d['data'] = array();

        $json_7d =  array();
        $Chart_7d = array();
        $data7d['data'] = array();

        $json_28d =  array();
        $Chart_28d = array();
        $data28d['data'] = array();

        $json_3d_non =  array();
        $Chart_3d_non = array();
        $data3d_non['data'] = array();

        $json_7d_non =  array();
        $Chart_7d_non = array();
        $data7d_non['data'] = array();

        $json_28d_non =  array();
        $Chart_28d_non = array();
        $data28d_non['data'] = array();

        // $json_3d_std =  array();
        // $Chart_3d_std = array();
        // $data3d_std['data'] = array();

        // $json_7d_std =  array();
        // $Chart_7d_std = array();
        // $data7d_std['data'] = array();

        // $json_28d_std =  array();
        // $Chart_28d_std = array();
        // $data28_std['data'] = array();


        $json_index =  array();
        $Chart_index = array();
        $dataindex['data'] = array();

        $json_index_non =  array();
        $Chart_index_non = array();
        $dataindex_non['data'] = array();

        $company_ar =  array();

        $data['kategori'] = array();
        $a_opc = array();
        $a_ppc = array();
        foreach ($get_company as $val_com) {
            // $bulan= '12/2017';
            $companys = short_opco($val_com['KD_COMPANY']);
            array_push($company_ar, $companys);
            foreach ($get_bulan as $val_bulan) {

                $bulan = $val_bulan['BULAN'];
                $Chart_3d['name'] = "3D";
                $Chart_3d['type'] = 'spline';
                $Chart_7d['name'] = "7D";
                $Chart_7d['type'] = 'spline';
                $Chart_28d['name'] = "28D";
                $Chart_28d['type'] = 'spline';
                $Chart_index['name'] = "Index";
                $Chart_index['type'] = 'spline';
                $Chart_index['yAxis'] = 1;


                $Chart_3d_non['name'] = "3D";
                $Chart_3d_non['type'] = 'spline';
                $Chart_7d_non['name'] = "7D";
                $Chart_7d_non['type'] = 'spline';
                $Chart_28d_non['name'] = "28D";
                $Chart_28d_non['type'] = 'spline';
                $Chart_index_non['name'] = "Index";
                $Chart_index_non['type'] = 'spline';
                $Chart_index_non['yAxis'] = 1;

                $get_area = $this->m_clinker_factor->area($val_com['KD_COMPANY']);
                foreach ($get_area as $key => $val) {
                    $reg = $val['ID_AREA'];
                    $temp_reg[] = $reg;
                }
                $idarea = "'" . join("','", $temp_reg) . "'";
                $prod_opc = 'OPC';
                if ($val_com['KD_COMPANY'] == '6000') {
                    $prod_opc = 'PC50';
                }
                $g_awal = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}' AND KD_COMPONENT = 'INITIAL'");
                $g_akhir = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'FINAL'");
                $g_tigahari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS3D'");
                $g_tujuhhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS7D'");
                $g_dualapanhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS28D'");

                //3D
                $no = 0;
                $nil_tigahari = 0;
                foreach ($g_tigahari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tigahari += (int)$val['NILAI'];
                    }
                }
                $h_tigahari = $nil_tigahari / $no;
                array_push($data3d['data'], floatval(number_format($h_tigahari, 2)));
                //7D
                $no = 0;
                $nil_tujuhhari = 0;
                foreach ($g_tujuhhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tujuhhari += (int)$val['NILAI'];
                    }
                }
                $h_tujuhhari = $nil_tujuhhari / $no;
                array_push($data7d['data'], floatval(number_format($h_tujuhhari, 2)));
                //AWAL
                $no = 0;
                $nil_dualapanhari = 0;
                foreach ($g_dualapanhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_dualapanhari += (int)$val['NILAI'];
                    }
                }
                $h_dualapanhari = $nil_dualapanhari / $no;
                array_push($data28d['data'], floatval(number_format($h_dualapanhari, 2)));



                ///NON OPC ||||||
                $prod_opc = "'PPC', 'PCC'";
                if ($val_com['KD_COMPANY'] == '6000') {
                    $prod_opc = "'PCB40'";
                }
                $g_awal = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc}) AND KD_COMPONENT = 'INITIAL'");
                $g_akhir = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc})  AND KD_COMPONENT = 'FINAL'");
                $g_tigahari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT  in ({$prod_opc}) AND KD_COMPONENT = 'CS3D'");
                $g_tujuhhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT  in ({$prod_opc})  AND KD_COMPONENT = 'CS7D'");
                $g_dualapanhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc}) AND KD_COMPONENT = 'CS28D'");

                //3D
                $no = 0;
                $nil_tigahari = 0;
                foreach ($g_tigahari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tigahari += (int)$val['NILAI'];
                    }
                }
                $h_tigahari_non = $nil_tigahari / $no;
                array_push($data3d_non['data'], floatval(number_format($h_tigahari_non, 2)));
                //7D
                $no = 0;
                $nil_tujuhhari = 0;
                foreach ($g_tujuhhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tujuhhari += (int)$val['NILAI'];
                    }
                }
                $h_tujuhhari_non = $nil_tujuhhari / $no;
                array_push($data7d_non['data'], floatval(number_format($h_tujuhhari_non, 2)));
                //AWAL
                $no = 0;
                $nil_dualapanhari = 0;
                foreach ($g_dualapanhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_dualapanhari += (int)$val['NILAI'];
                    }
                }
                $h_dualapanhari_non = $nil_dualapanhari / $no;
                array_push($data28d_non['data'], floatval(number_format($h_dualapanhari_non, 2)));

                $temp_reg = '';
                $plant =  $this->m_index->get_plantin($val_com['KD_COMPANY']);
                $tanggal = "21/" . $val_bulan['BULAN'];
                $myDateTime = DateTime::createFromFormat('d/m/Y', $tanggal);
                $date = $myDateTime->format('Y.m');
                $m_ppc = "'121-302-0020'";
                // $c_opc = "'121-302-0010', '121-200-0010'";
                $c_opc = "'121-302-0010'";
                $temp_reg = array();
                $bagian = 0;
                $total = 0;
                foreach ($plant as $val) {
                    $sum = 0;
                    $angka =  $this->m_index->get_value($val["KD_COMPANY"], $c_opc, $date, $val['KD_PLANT']);
                    if (count($angka) != 0) {
                        $bagi = 0;
                        foreach ($angka as $v) {
                            $bagi += 1;
                            $sum += $v['INDEX1'];
                        }
                        $angka = $sum / $bagi;
                        $total += $angka;
                    } else {
                        $total += 0;
                    }
                    $bagian += 1;
                }
                $hasil = $total / $bagian;
                array_push($dataindex['data'], floatval(number_format($hasil * 100, 2)));

                $bagian = 0;
                $total = 0;
                foreach ($plant as $val) {
                    $sum = 0;
                    $angka =  $this->m_index->get_value($val["KD_COMPANY"], $m_ppc, $date, $val['KD_PLANT']);
                    if (count($angka) != 0) {
                        $bagi = 0;
                        foreach ($angka as $v) {
                            $bagi += 1;
                            $sum += $v['INDEX1'];
                        }
                        $angka = $sum / $bagi;
                        $total += $angka;
                    } else {
                        $total += 0;
                    }
                    $bagian += 1;
                }
                $hasil = $total / $bagian;
                array_push($dataindex_non['data'], floatval(number_format($hasil * 100, 2)));
            }

            $Chart_3d['data'] = $data3d['data'];
            $json_3d[] = $Chart_3d;
            $Chart_7d['data'] = $data7d['data'];
            $json_7d[] = $Chart_7d;
            $Chart_28d['data'] = $data28d['data'];
            $json_28d[] = $Chart_28d;
            $Chart_3d_non['data'] = $data3d_non['data'];
            $json_3d_non[] = $Chart_3d_non;
            $Chart_7d_non['data'] = $data7d_non['data'];
            $json_7d_non[] = $Chart_7d_non;
            $Chart_28d_non['data'] = $data28d_non['data'];
            $json_28d_non[] = $Chart_28d_non;
            $Chart_index['data'] = $dataindex['data'];
            $json_index[] = $Chart_index;
            $Chart_index_non['data'] = $dataindex_non['data'];
            $json_index_non[] = $Chart_index_non;
            array_splice($data3d['data'], 0);
            array_splice($data7d['data'], 0);
            array_splice($data28d['data'], 0);
            array_splice($data3d_non['data'], 0);
            array_splice($data7d_non['data'], 0);
            array_splice($data28d_non['data'], 0);
            array_splice($dataindex['data'], 0);
            array_splice($dataindex_non['data'], 0);
        }

        foreach ($get_bulan as $val_bulan) {
            $bulan = $val_bulan['BULAN'];
            // array_push($data['kategori'], $bulan);
            // $bulan= '12/2017';
            foreach ($get_company as $val_com) {


                $get_area = $this->m_clinker_factor->area($val_com['KD_COMPANY']);
                foreach ($get_area as $key => $val) {
                    $reg = $val['ID_AREA'];
                    $temp_reg[] = $reg;
                }
                $idarea = "'" . join("','", $temp_reg) . "'";
                $prod_opc = 'OPC';
                if ($val_com['KD_COMPANY'] == '6000') {
                    $prod_opc = 'PC50';
                }
                $g_awal = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}' AND KD_COMPONENT = 'INITIAL'");
                $g_akhir = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'FINAL'");
                $g_tigahari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS3D'");
                $g_tujuhhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS7D'");
                $g_dualapanhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT = '{$prod_opc}'  AND KD_COMPONENT = 'CS28D'");
                //AWAL
                $no = 0;
                $nil_awal = 0;
                foreach ($g_awal as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_awal += (int)$val['NILAI'];
                    }
                }
                $h_awal = $nil_awal / $no;
                //AKHIR
                $no = 0;
                $nil_akhir = 0;
                foreach ($g_akhir as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_akhir += (int)$val['NILAI'];
                    }
                }
                $h_akhir = $nil_akhir / $no;
                //3D
                $no = 0;
                $nil_tigahari = 0;
                foreach ($g_tigahari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tigahari += (int)$val['NILAI'];
                    }
                }
                $h_tigahari = $nil_tigahari / $no;
                //7D
                $no = 0;
                $nil_tujuhhari = 0;
                foreach ($g_tujuhhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tujuhhari += (int)$val['NILAI'];
                    }
                }
                $h_tujuhhari = $nil_tujuhhari / $no;
                //AWAL
                $no = 0;
                $nil_dualapanhari = 0;
                foreach ($g_dualapanhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_dualapanhari += (int)$val['NILAI'];
                    }
                }
                $h_dualapanhari = $nil_dualapanhari / $no;

                if ($val_com['KD_COMPANY'] == "6000") {
                    $h_tigahari = $h_tigahari * 0.6393 * 10.197;
                    $h_tujuhhari = $h_tujuhhari * 0.691 * 10.197;
                    $h_dualapanhari = $h_dualapanhari * 0.7238 * 10.197;
                }

                // echo $h_awal.' - '.$h_akhir.' - '.$h_tigahari.' - '.$h_tujuhhari.' - '.$h_dualapanhari.'<br>';
                array_push($awal, is_nan($h_awal) ? 0 :  number_format($h_awal, 2));
                array_push($akhir, is_nan($h_akhir) ? 0 : number_format($h_akhir, 2));
                array_push($tigahari, is_nan($h_tigahari) ? 0 : number_format($h_tigahari, 2));
                array_push($tujuhhari, is_nan($h_tujuhhari) ? 0 : number_format($h_tujuhhari, 2));
                array_push($dualapanhari, is_nan($h_dualapanhari) ? 0 : number_format($h_dualapanhari, 2));

                ///NON OPC ||||||
                $prod_opc = "'PPC', 'PCC'";
                if ($val_com['KD_COMPANY'] == '6000') {
                    $prod_opc = "'PCB40'";
                }
                $g_awal = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc}) AND KD_COMPONENT = 'INITIAL'");
                $g_akhir = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc})  AND KD_COMPONENT = 'FINAL'");
                $g_tigahari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT  in ({$prod_opc}) AND KD_COMPONENT = 'CS3D'");
                $g_tujuhhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT  in ({$prod_opc})  AND KD_COMPONENT = 'CS7D'");
                $g_dualapanhari = $this->t_cement_daily->data_where_prod_cf("TO_CHAR(DATE_DATA, 'MM/YYYY')='{$bulan}' AND a.ID_AREA in (" . $idarea . ") AND b.KD_PRODUCT in ({$prod_opc}) AND KD_COMPONENT = 'CS28D'");
                //AWAL
                $no = 0;
                $nil_awal = 0;
                foreach ($g_awal as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_awal += (int)$val['NILAI'];
                    }
                }
                $h_awal_non = $nil_awal / $no;
                //AKHIR
                $no = 0;
                $nil_akhir = 0;
                foreach ($g_akhir as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_akhir += (int)$val['NILAI'];
                    }
                }
                $h_akhir_non = $nil_akhir / $no;
                //3D
                $no = 0;
                $nil_tigahari = 0;
                foreach ($g_tigahari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tigahari += (int)$val['NILAI'];
                    }
                }
                $h_tigahari_non = $nil_tigahari / $no;
                //7D
                $no = 0;
                $nil_tujuhhari = 0;
                foreach ($g_tujuhhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_tujuhhari += (int)$val['NILAI'];
                    }
                }
                $h_tujuhhari_non = $nil_tujuhhari / $no;
                //AWAL
                $no = 0;
                $nil_dualapanhari = 0;
                foreach ($g_dualapanhari as $val) {
                    if ($val['NILAI'] != '') {
                        $no += 1;
                        $nil_dualapanhari += (int)$val['NILAI'];
                    }
                }
                $h_dualapanhari_non = $nil_dualapanhari / $no;

                if ($val_com['KD_COMPANY'] == "6000") {
                    $h_tigahari_non = $h_tigahari_non * 0.5977 * 10.197;
                    $h_tujuhhari_non = $h_tujuhhari_non * 0.6285 * 10.197;
                    $h_dualapanhari_non = $h_dualapanhari_non * 0.6726 * 10.197;
                }

                // echo $h_awal.' - '.$h_akhir.' - '.$h_tigahari.' - '.$h_tujuhhari.' - '.$h_dualapanhari.'<br>';
                array_push($awal_non, is_nan($h_awal_non) ? 0 : number_format($h_awal_non, 2));
                array_push($akhir_non,  is_nan($h_akhir_non) ? 0 : number_format($h_akhir_non, 2));
                array_push($tigahari_non,  is_nan($h_tigahari_non) ? 0 : number_format($h_tigahari_non, 2));
                array_push($tujuhhari_non,  is_nan($h_tujuhhari_non) ? 0 : number_format($h_tujuhhari_non, 2));
                array_push($dualapanhari_non,  is_nan($h_dualapanhari_non) ? 0 : number_format($h_dualapanhari_non, 2));
                $temp_reg = '';
                $plant =  $this->m_index->get_plantin($val_com['KD_COMPANY']);
                $tanggal = "21/" . $val_bulan['BULAN'];
                $myDateTime = DateTime::createFromFormat('d/m/Y', $tanggal);
                $date = $myDateTime->format('Y.m');
                $m_ppc = "'121-302-0020'";
                // $c_opc = "'121-302-0010', '121-200-0010'";
                $c_opc = "'121-302-0010'";
                $temp_reg = array();
                $bagian = 0;
                $total = 0;
                foreach ($plant as $val) {
                    $sum = 0;
                    $angka =  $this->m_index->get_value($val["KD_COMPANY"], $c_opc, $date, $val['KD_PLANT']);
                    if (count($angka) != 0) {
                        $bagi = 0;
                        foreach ($angka as $v) {
                            $bagi += 1;
                            $sum += $v['INDEX1'];
                        }
                        $angka = $sum / $bagi;
                        $total += $angka;
                    } else {
                        $total += 0;
                    }
                    $bagian += 1;
                }
                $hasil = $total / $bagian;
                array_push($a_opc, floatval(number_format($hasil * 100, 2)));

                $bagian = 0;
                $total = 0;
                foreach ($plant as $val) {
                    $sum = 0;
                    $angka =  $this->m_index->get_value($val["KD_COMPANY"], $m_ppc, $date, $val['KD_PLANT']);
                    if (count($angka) != 0) {
                        $bagi = 0;
                        foreach ($angka as $v) {
                            $bagi += 1;
                            $sum += $v['INDEX1'];
                        }
                        $angka = $sum / $bagi;
                        $total += $angka;
                    } else {
                        $total += 0;
                    }
                    $bagian += 1;
                }
                $hasil = $total / $bagian;
                array_push($a_ppc, floatval(number_format($hasil * 100, 2)));
            }
        }


        // foreach($get_company as $val_com){
        // array_push($data['kategori'], short_opco($val_com['KD_COMPANY']));
        // }
        // echo print_r($awal);
        // echo print_r($akhir);
        // echo print_r($tigahari);
        // echo print_r($tujuhhari);
        // echo print_r($dualapanhari);
        // echo print_r($awal_non);
        // echo print_r($akhir_non);
        // echo print_r($tigahari_non);
        // echo print_r($tujuhhari_non);
        // echo print_r($dualapanhari_non);

        // exit();

        //Get Max Min opc
        $s_awal = $this->c_range_qaf->configuration_component_report('10', '1', '2', 'INITIAL');
        $s_akhir = $this->c_range_qaf->configuration_component_report('10', '1', '2', 'FINAL');
        $s_tigahari = $this->c_range_qaf->configuration_component_report('10', '1', '2', 'CS3D');
        $s_tujuhhari = $this->c_range_qaf->configuration_component_report('10', '1', '2', 'CS7D');
        $s_dualapan = $this->c_range_qaf->configuration_component_report('10', '1', '2', 'CS28D');
        $standart_awal = $s_awal['V_MIN'] . ' - ' . $s_awal['V_MAX'];
        $standart_akhir = $s_akhir['V_MIN'] . ' - ' .  $s_akhir['V_MAX'];
        $standart_tigahari = $s_tigahari['V_MIN'] . ' - ' . $s_tigahari['V_MAX'];
        $standart_tujuhhari =  $s_tujuhhari['V_MIN'] . ' - ' .  $s_tujuhhari['V_MAX'];
        $standart_dualapan =  $s_dualapan['V_MIN'] . ' - ' .  $s_dualapan['V_MAX'];
        // Get Max Min NON OPC
        $s_awalnon = $this->c_range_qaf->configuration_component_report('10', '1', '3', 'INITIAL');
        $s_akhirnon = $this->c_range_qaf->configuration_component_report('10', '1', '3', 'FINAL');
        $s_tigaharinon = $this->c_range_qaf->configuration_component_report('10', '1', '3', 'CS3D');
        $s_tujuhharinon = $this->c_range_qaf->configuration_component_report('10', '1', '3', 'CS7D');
        $s_dualapannon = $this->c_range_qaf->configuration_component_report('10', '1', '3', 'CS28D');
        $standart_awalnon =  $s_awalnon['V_MIN'] . ' - ' .  $s_awalnon['V_MAX'];
        $standart_akhirnon = $s_akhirnon['V_MIN'] . ' - ' .  $s_akhirnon['V_MAX'];
        $standart_tigaharinon =  $s_tigaharinon['V_MIN'] . ' - ' .  $s_tigaharinon['V_MAX'];
        $standart_tujuhharinon =  $s_tujuhharinon['V_MIN'] . ' - ' .  $s_tujuhharinon['V_MAX'];
        $standart_dualapannon = $s_dualapannon['V_MIN'] . ' - ' . $s_dualapannon['V_MAX'];


        $bulXcom = count($get_bulan) * count($get_company);
        $tabOpc = '';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td colspan="2" rowspan="3" style="vertical-align : middle;text-align:center;"></td>';
        $tabOpc .= '<td rowspan="3" style="vertical-align : middle;text-align:center;">SMIG STANDARD</td>';
        $tabOpc .= '<td colspan="' . $bulXcom . '" style="vertical-align : middle;text-align:center;">RATA-RATA KUALITAS</td>';
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';

        foreach ($get_bulan as $val_bul) {
            $bulan = $val_bul['BULAN'];
            array_push($data['kategori'], $bulan);
            $tabOpc .= '<td colspan="' . count($get_company) . '" style="vertical-align : middle;text-align:center;">' . $bulan . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        foreach ($get_bulan as $val_bul) {
            foreach ($get_company as $val_com) {
                $tabOpc .= '<td style="vertical-align : middle;text-align:center;">' . short_opco($val_com['KD_COMPANY']) . '</td>';
            }
        }
        $tabOpc .= '</tr>';


        $bulXcom3 = $bulXcom + 3;
        $tabOpc .= '<tr>';
        $tabOpc .= '<td colspan="' . $bulXcom3 . '">OPC</td>';
        $tabOpc .= '</tr>';
        $tabOpc .= '<td nowrap><span style="margin-left: 25px;"></span>CLINKER FACTOR</td>';
        $tabOpc .= '<td>%</td>';
        $tabOpc .= '<td>-</td>';
        $lebar = count($a_opc);
        $nom = 1;
        foreach ($a_opc as $i) {
            $tabOpc .= '<td>' . $i . '</td>';
            $nom += 1;
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr><td  colspan="' . $bulXcom3 . '"><span style="margin-left: 25px;"></span>SETTING TIME</td></tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-Awal</td>';
        $tabOpc .= '<td>Minute</td>';
        $tabOpc .= '<td>' . $standart_awal . '</td>';
        foreach ($awal as $val) {
            if ($val >= $s_awal['V_MIN'] && $val <= $s_awal['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-AKHIR</td>';
        $tabOpc .= '<td>Minute</td>';
        $tabOpc .= '<td>' . $standart_akhir . '</td>';
        foreach ($akhir as $val) {
            if ($val >= $s_akhir['V_MIN'] && $val <= $s_akhir['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr><td  colspan="' . $bulXcom3 . '"><span style="margin-left: 25px;"></span>KUAT TEKAN</td></tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-3 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_tigahari . '</td>';
        foreach ($tigahari as $val) {
            if ($val >= $s_tigahari['V_MIN'] && $val <= $s_tigahari['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-7 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_tujuhhari . '</td>';
        foreach ($tujuhhari as $val) {
            if ($val >= $s_tujuhhari['V_MIN'] && $val <= $s_tujuhhari['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-28 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_dualapan . '</td>';
        foreach ($dualapanhari as $val) {
            if ($val >= $s_dualapan['V_MIN'] && $val <= $s_dualapan['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';

        //////////////   NON OPC    |||||||||||||||||

        $tabOpc .= '<tr>';
        $tabOpc .= '<td colspan="' . $bulXcom3 . '">NON OPC</td>';
        $tabOpc .= '</tr>';
        $tabOpc .= '<td nowrap><span style="margin-left: 25px;"></span>CLINKER FACTOR</td>';
        $tabOpc .= '<td>%</td>';
        $tabOpc .= '<td>-</td>';
        foreach ($a_ppc as $i) {
            $tabOpc .= '<td>' . $i . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr><td  colspan="' . $bulXcom3 . '"><span style="margin-left: 25px;"></span>SETTING TIME</td></tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-Awal</td>';
        $tabOpc .= '<td>Minute</td>';
        $tabOpc .= '<td>' . $standart_awalnon . '</td>';
        foreach ($awal_non as $val) {
            if ($val >= $s_awalnon['V_MIN'] && $val <= $s_awalnon['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-AKHIR</td>';
        $tabOpc .= '<td>Minute</td>';
        $tabOpc .= '<td>' . $standart_akhirnon . '</td>';
        foreach ($akhir_non as $val) {
            if ($val >= $s_akhirnon['V_MIN'] && $val <= $s_akhirnon['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr><td  colspan="' . $bulXcom3 . '"><span style="margin-left: 25px;"></span>KUAT TEKAN</td></tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-3 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_tigaharinon . '</td>';
        foreach ($tigahari_non as $val) {
            if ($val >= $s_tigaharinon['V_MIN'] && $val <= $s_tigaharinon['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-7 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_tujuhharinon . '</td>';
        foreach ($tujuhhari_non as $val) {
            if ($val >= $s_tujuhharinon['V_MIN'] && $val <= $s_tujuhharinon['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';
        $tabOpc .= '<tr>';
        $tabOpc .= '<td><span style="margin-left: 25px;"></span>-28 Hari</td>';
        $tabOpc .= '<td>Kg/cm2</td>';
        $tabOpc .= '<td>' . $standart_dualapannon . '</td>';
        foreach ($dualapanhari_non as $val) {
            if ($val >= $s_dualapannon['V_MIN'] && $val <= $s_dualapannon['V_MAX']) {
                $warna = '';
            } else {
                $warna = 'red';
            }
            $tabOpc .= '<td style="color:' . $warna . '">' . $val . '</td>';
        }
        $tabOpc .= '</tr>';

        echo json_encode(
            array(
                'tabel' => $tabOpc,
                'min_3d' => floatval($s_tigahari['V_MIN']),
                'min_7d' => floatval($s_tujuhhari['V_MIN']),
                'min_28d' => floatval($s_dualapan['V_MIN']),
                'min_3d_non' =>  floatval($s_tigaharinon['V_MIN']),
                'min_7d_non' => floatval($s_tujuhharinon['V_MIN']),
                'min_28d_non' => floatval($s_dualapannon['V_MIN']),
                '3d' => $json_3d,
                '7d' => $json_7d,
                '28d' => $json_28d,
                '3d_non' => $json_3d_non,
                '7d_non' => $json_7d_non,
                '28d_non' => $json_28d_non,
                'index' => $json_index,
                'index_non' => $json_index_non,
                'kategori' => $data['kategori'],
                'company'   => $company_ar,
                'lebar' =>  $lebar
            )
        );
    }


    public function index_clinker()
    {
        $this->libExternal("dataTables");
        $this->libExternal('datepicker');
        $this->libExternal("highcharts");
        $this->list_company = $this->m_company->datalist();
        $this->template->adminlte("v_index_clinker");
    }
    public function tabel_index_clinker()
    {
        $tahun =  $this->input->post('tahun');
        if ($tahun == date("Y")) {
            $c_bulan = floatval(date("m"));
            $c_bulan = 2;
        } else {
            $c_bulan = 12;
        }
        $company =   $this->input->post('company');
        $opco = short_opco($company);
        $m_campur = "'121-302-0010', '121-200-0010', '121-302-0020'";
        $m_opc = "'121-302-0010'";
        $m_opc_k = "'121-200-0010'";
        $m_ppc = "'121-302-0020'";
        $c_opc = "'121-302-0010', '121-200-0010'";
        $plant =  $this->m_index->get_plantin($company);
        $t_plant = count($plant) + 1;
        $tabel = '';
        $tabel .= '<tr>';
        $tabel .= '<td rowspan="2"></td>';
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = $tahun . "-" . str_pad($i, 2, "0", STR_PAD_LEFT) . '-21';
            $tanggal = date("F", strtotime($bulan));
            $date = $tahun . '.' . $bulan;
            $tabel .= '<td colspan="' . $t_plant . '" style="vertical-align : middle;text-align:center;">' . $tanggal . '</td>';
        }
        $tabel .= '</tr>';

        $tabel .= '<tr>';
        $total_kolom = 0;
        for ($i = 1; $i <= $c_bulan; $i++) {
            // $bulan = str_pad($i,2,"0",STR_PAD_LEFT);
            // $date= $tahun.'.'.$bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $tabel .= '<td style="vertical-align : middle;text-align:center;">' . $val['NM_PLANT'] . '</td>';
                $total_kolom += 1;
            }
            $tabel .= '<td>ALL ' . $opco . '</td>';
            $total_kolom += 1;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr>';
        $tabel .= '<td>Clinker OPC</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $sum = 0;
                $angka =  $this->m_index->get_value($company, $m_opc_k, $date, $val['KD_PLANT']);
                if (count($angka) != 0) {
                    foreach ($angka as $v) {
                        $sum += $v['V1'];
                    }
                    $angka = $sum;
                } else {
                    $angka = 0;
                }
                $total += $angka;
                $tabel .= '<td class="clinker_' . $no . '">' . str_replace(",", ".", number_format($angka)) . '</td>';
                $no += 1;
            }
            $tabel .= '<td class="clinker_' . $no . '">' . str_replace(",", ".", number_format($total)) . '</td>';
            $total = 0;
            $no += 1;
        }

        $tabel .= '</tr>';

        $tabel .= '<tr>';
        $tabel .= '<td>Clinker PPC</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $angka =  $this->m_index->get_value($company, $m_opc_k, $date, $val['KD_PLANT']);
                // if(count($angka)!=0){
                // $angka = $angka['V1'];
                // }else{
                $angka = 0;
                // }
                $total += $angka;
                $tabel .= '<td class="clinker_' . $no . '">' . str_replace(",", ".", number_format($angka)) . '</td>';
                $no += 1;
            }
            $tabel .= '<td class="clinker_' . $no . '">' . str_replace(",", ".", number_format($total)) . '</td>';
            $total = 0;
            $no += 1;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr style="background-color: lavender;">';
        $tabel .= '<td>TOTAL PRODUKSI Clinker</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $tabel .= '<td  class="clinkertot_' . $no . '"></td>';
                $no += 1;
            }
            $tabel .= '<td  class="clinkertot_' . $no . '"></td>';
            $no += 1;
        }
        $tabel .= '</tr>';


        $tabel .= '<tr>';
        $tabel .= '<td>Produksi Semen OPC</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $sum = 0;
                $angka =  $this->m_index->get_value($company, $m_opc, $date, $val['KD_PLANT']);
                if (count($angka) != 0) {
                    foreach ($angka as $v) {
                        $sum += $v['V1'];
                    }
                    $angka = $sum;
                } else {
                    $angka = 0;
                }
                $total += $angka;
                $tabel .= '<td class="semen_' . $no . '">' . str_replace(",", ".", number_format($angka)) . '</td>';
                $no += 1;
            }
            $tabel .= '<td class="semen_' . $no . '">' . str_replace(",", ".", number_format($total)) . '</td>';
            $total = 0;
            $no += 1;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr>';
        $tabel .= '<td>Produksi Semen PPC</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $sum = 0;
                $angka =  $this->m_index->get_value($company, $m_ppc, $date, $val['KD_PLANT']);
                if (count($angka) != 0) {
                    foreach ($angka as $v) {
                        $sum += $v['V1'];
                    }
                    $angka = $sum;
                } else {
                    $angka = 0;
                }
                $total += $angka;
                $tabel .= '<td class="semen_' . $no . '">' . str_replace(",", ".", number_format($angka)) . '</td>';
                $no += 1;
            }
            $tabel .= '<td class="semen_' . $no . '">' . str_replace(",", ".", number_format($total)) . '</td>';
            $total = 0;
            $no += 1;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr style="background-color: lavender;">';
        $tabel .= '<td>TOTAL PRODUKSI SEMEN</td>';
        $no = 1;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $tabel .= '<td  class="sementot_' . $no . '"></td>';
                $no += 1;
            }
            $tabel .= '<td  class="sementot_' . $no . '"></td>';
            $no += 1;
        }
        $tabel .= '</tr>';


        $tabel .= '<tr>';
        $tabel .= '<td>Index Clinker OPC</td>';
        $no = 1;
        $raw = 0;

        $json_builder =  array();
        $dataChart = array();
        $data['data'] = array();
        $json_buildernon =  array();
        $dataChartnon = array();
        $datanon['data'] = array();
        $data['kategori'] = array();

        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;

            $bulann = $tahun . "-" . str_pad($i, 2, "0", STR_PAD_LEFT) . '-21';
            $tanggal = date("F", strtotime($bulann));
            $dataChart['name'] = $tanggal;
            $dataChart['type'] = 'column';
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $sum = 0;
                $angka =  $this->m_index->get_value($company, $m_opc, $date, $val['KD_PLANT']);
                if (count($angka) != 0) {
                    $bagi = 0;
                    foreach ($angka as $v) {
                        $bagi += 1;
                        $sum += $v['INDEX1'];
                    }
                    $angka = $sum / $bagi;
                    $total += $angka;
                } else {
                    $angka = 0;
                }
                $tabel .= '<td class="index_' . $no . '">' . number_format($angka * 100, 2) . ' %</td>';
                $no += 1;
                $raw += 1;
                array_push($data['data'], floatval(number_format($angka * 100, 2)));
            }

            $dataChart['data'] = $data['data'];
            $json_builder[] = $dataChart;
            array_splice($data['data'], 0);
            $avg = $total / $raw;
            $tabel .= '<td class="index_' . $no . '">' . number_format($avg * 100, 2) . ' %</td>';
            $total = 0;
            $no += 1;
            $raw = 0;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr>';
        $tabel .= '<td>Index Clinker PPC</td>';
        $no = 1;
        $raw = 0;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;

            $bulann = $tahun . "-" . str_pad($i, 2, "0", STR_PAD_LEFT) . '-21';
            $tanggal = date("F", strtotime($bulann));
            $dataChartnon['name'] = $tanggal;
            $dataChartnon['type'] = 'column';
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $sum = 0;
                $angka =  $this->m_index->get_value($company, $m_ppc, $date, $val['KD_PLANT']);
                if (count($angka) != 0) {
                    $bagi = 0;
                    foreach ($angka as $v) {
                        $bagi += 1;
                        $sum += $v['INDEX1'];
                    }
                    $angka = $sum / $bagi;
                    $total += $angka;
                } else {
                    $angka = 0;
                }
                $tabel .= '<td class="index_' . $no . '">' . number_format($angka * 100, 2) . ' %</td>';
                $no += 1;
                $raw += 1;

                array_push($datanon['data'], floatval(number_format($angka * 100, 2)));
            }

            $dataChartnon['data'] = $datanon['data'];
            $json_buildernon[] = $dataChartnon;
            array_splice($datanon['data'], 0);

            $avg = $total / $raw;
            $tabel .= '<td class="index_' . $no . '">' . number_format($avg * 100, 2) . ' %</td>';
            $total = 0;
            $no += 1;
            $raw = 0;
        }
        $tabel .= '</tr>';

        $tabel .= '<tr style="background-color: lavender;">';
        $tabel .= '<td>Indeks Clinker All Type</td>';
        $no = 1;
        $raw = 0;
        for ($i = 1; $i <= $c_bulan; $i++) {
            $bulan = str_pad($i, 2, "0", STR_PAD_LEFT);
            $date = $tahun . '.' . $bulan;
            // $plant =  $this->m_index->get_plant($company, $m_campur, $date);
            foreach ($plant as $val) {
                $tabel .= '<td>-</td>';
                $no += 1;
                $raw += 1;
            }
            $tabel .= '<td class="indextot_' . $no . '">' . number_format($avg * 100, 2) . ' %</td>';
            $total = 0;
            $no += 1;
            $raw = 0;
        }
        $tabel .= '</tr>';


        foreach ($plant as $val) {
            array_push($data['kategori'], $val['NM_PLANT']);
        }


        echo json_encode(
            array(
                'tabel' => $tabel,
                'total_kolom' => $total_kolom,
                'dataopc' => $json_builder,
                'datanon' => $json_buildernon,
                'kategori' => $data['kategori'],
            )
        );
    }
}
