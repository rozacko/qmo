<?php

class Acclab_tindaklanjutblindtest extends QMUser {	
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_blindtest");
		$this->load->model("m_company");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_component = $this->m_blindtest->list_component();
		$this->list_stdevaluasi = $this->m_blindtest->list_stdevaluasi();
		$mcomp = array();
		foreach ($this->m_blindtest->componentmaster() as $key => $value) {
			# code...
			$mcomp[] = $value['NM_COMPONENT'];
		}
		$this->m_component = $mcomp;
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);	
		$this->template->adminlte("v_tindaklanjutblindtest", $data);
	}

	public function blindtest_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
		$sesi_user = $this->session->userdata();
		$user_in = json_decode(json_encode($sesi_user['USER']), true); 
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		if ($user_in["ID_USERGROUP"] && (int) $user_in["ID_USERGROUP"] == 1) {
  			# code...
  		} else {
  			# code...
  			$key['PIC_OBSERVASI'] = $user_in["ID_USER"];
  			$key['PIC_PLANT'] = $user_in["ID_USER"];
  		}

      	$data	= $this->m_blindtest->get_tindaklanjut($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_blindtest->recFil_tindaklanjut($key),
  			'recordsTotal'		=>	$this->m_blindtest->recTot_tindaklanjut($key)
  		);

  		echo json_encode($return);
    }

    public function send_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Send Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENGIRIM'] = $post['user'];
		$data['CREATE_BY'] = $post['user'];

		$isertd_sample = $this->m_blindtest->sample_send($data);
		if ($isertd_sample) {
			// code...
			$result['msg'] = 'Send Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function receipt_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Receipt Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENERIMA'] = $post['user'];
		$data['STATUS_ENTRY'] = 1;
		$data['UPDATE_BY'] = $post['user'];

		$updted_sample = $this->m_blindtest->sample_receipt($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Receipt Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function evaluate_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Evaluate Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data = $post;
		$data['UPDATE_BY'] = $post['user'];
		unset($data['user']);

		$updted_sample = $this->m_blindtest->sample_evaluate($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Evaluate Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function submit_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Submit Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['ID'];
		$data['STATUS_ENTRY'] = 2;
		$data['UPDATE_BY'] = $post['user'];

		$updted_sample = $this->m_blindtest->sample_submit($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Submit Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function save_tindaklanjutblindtest() {
    	# code...
    	$result['msg'] = 'Cannot Save Tindak Lanjut Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();

		$data = $post;
		unset($data['ID_BLIND']);
		
    	$config['upload_path']= './assets/uploads/tindaklanjutblindtest/';
        $config['allowed_types'] = 'jpg|jpeg|png|xls|xlsx|csv';
        $config['max_size'] = 2048;

        // if(isset($_FILES['VERIFIKASI'])){
        //     $this->load->library('upload', $config);
        //     $this->upload->initialize($config);
        //     if ($this->upload->do_upload('VERIFIKASI')){
        //         $VERIFIKASI = $this->upload->data();
        //         $data["VERIFIKASI"] = $VERIFIKASI["file_name"];
        //     }
        // }

        if(isset($_FILES['VERIFIKASI'])){
        	$nameuploadverifikasi = array();
        	foreach ($_FILES['VERIFIKASI']['name'] as $key => $value) {
        			# code...
	        		$_FILES['UPLOADVERIFIKASI']['name'] = $_FILES['VERIFIKASI']['name'][$key];
					$_FILES['UPLOADVERIFIKASI']['type'] = $_FILES['VERIFIKASI']['type'][$key];
					$_FILES['UPLOADVERIFIKASI']['tmp_name'] = $_FILES['VERIFIKASI']['tmp_name'][$key];
					$_FILES['UPLOADVERIFIKASI']['error'] = $_FILES['VERIFIKASI']['error'][$key];
					$_FILES['UPLOADVERIFIKASI']['size'] = $_FILES['VERIFIKASI']['size'][$key];
		            $this->load->library('upload', $config);
		            $this->upload->initialize($config);
		            if ($this->upload->do_upload('UPLOADVERIFIKASI')){
		                $UPLOADVERIFIKASI = $this->upload->data();
		                $nameuploadverifikasi[] = $UPLOADVERIFIKASI["file_name"];
		            }
	        }
	        $data["VERIFIKASI"] = join("@!@~$", $nameuploadverifikasi);
        }
		$data['UPDATE_BY'] = $post['user'];
		$updted_sample = $this->m_blindtest->ttl_submit($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Save Tindak Lanjut Blind Test Success ...';
			$result['status'] = true;
			$param['ID'] = $post['ID_BLIND'];
			$param['STATUS'] = 'TINDAK LANJUT EVALUASI';
			$param['UPDATE_BY'] = $post['user'];
			$isertd_sample = $this->m_blindtest->update_status($param);
			// echo $this->db->last_query();
		}
		
		to_json($result);
    }

    public function delete_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Delete Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['ID'] = $post['ID'];

		$delete_sample = $this->m_blindtest->blind_delete($post['ID']);
		if ($delete_sample) {
			// code...
			$result['msg'] = 'Delete Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

	public function ajax_get_type_product($all = null){
		$sample_area= $this->m_blindtest->type_productlist($all);
		foreach ($sample_area as $key => $value) {
			$sample_area[$key]['KD_PRODUCT'] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
		}
		to_json($sample_area);
	}

	public function submit_evaluasiblindtest(){
		$datablindtest = array();

		$post = $this->input->post();

		$idstup = $post['ID'];

		$sample_blindtest= $this->m_blindtest->evaluasisampleblindtest($post['ID']);

		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$param = array();
			$tdatablindtest[0] = $value['NM_COMPONENT'];
			$tdatablindtest[1] = (double) $value['TESTING_1'];
			$tdatablindtest[2] = (double) $value['TESTING_2'];
			$tdatablindtest[3] = ((double) $value['TESTING_1'] + (double) $value['TESTING_2']) / 2;
			$tdatablindtest[4] = (double) $value['DATA'] == 0 ? '-' : (double) $value['DATA'];
			$tdatablindtest[5] = (double) $value['SD'] == 0 ? '-' : (double) $value['SD'];
			$hasilperhitunganevaluasi = 0;
			$scoreevaluasi = 0;
			if (((double) $tdatablindtest[3] == 0 && (double) $value['DATA'] == 0) ||  (double) $value['SD'] == 0) {
				# code...
				$hasilperhitunganevaluasi = 0;
				$scoreevaluasi = 0;
			} else {
				$hasilperhitunganevaluasi = abs(((double) $tdatablindtest[3] - (double) $value['DATA']) /  (double) $value['SD']);
				if ($hasilperhitunganevaluasi > 0 && $hasilperhitunganevaluasi <= 1 ) {
					# code...
					$scoreevaluasi = 5;
				} else if ($hasilperhitunganevaluasi > 1 && $hasilperhitunganevaluasi <= 1.5 ) {
					# code...
					$scoreevaluasi = 4;
				} else if ($hasilperhitunganevaluasi > 1.5 && $hasilperhitunganevaluasi <= 2 ) {
					# code...
					$scoreevaluasi = 3;
				} else if ($hasilperhitunganevaluasi > 2 && $hasilperhitunganevaluasi <= 2.5 ) {
					# code...
					$scoreevaluasi = 2;
				} else if ($hasilperhitunganevaluasi > 2.5) {
					# code...
					$scoreevaluasi = 1;
				}
			}
			// $tdatablindtest[6] = (double) $tdatablindtest[3] == 0 ? '-' : (double) $tdatablindtest[3];
			$tdatablindtest[6] = (double) $hasilperhitunganevaluasi;
			$tdatablindtest[7] = (double) $scoreevaluasi == 0 ? '-' : (double) $scoreevaluasi;

			// $tdatablindtest[7] = '<button title="Delete" class="btDeleteBlind btn btn-danger btn-xs delete" '.$display.' type="button" onclick="deletesample('.$value['ID'].')"><i class="fa fa-trash-o"></i> Delete</button>';
			$datablindtest[] = $tdatablindtest;		
			$post['ID'] = $value['ID_BLINDTEST'];
			$param['FK_ID_BLIND'] = $value['ID_BLINDTEST'];
			$param['FK_ID_COMPONENT'] = $value['FK_ID_COMPONENT'];
			$param['N_EVALUASI'] = $hasilperhitunganevaluasi;
			$param['SCORE_EVALUASI'] = $scoreevaluasi;	
			$param['CREATE_BY'] = $post['user'];	
			$evalted_sample = $this->m_blindtest->evaluate_blindtest($param);
		}

		// $post = $this->input->post();
		$data['ID'] = $post['ID'];
		$data['STATUS_EVALUASI'] = 1;
		$data['UPDATE_BY'] = $post['user'];
		$updted_sample = $this->m_blindtest->sample_evaluate($data);

		$mparam = array();
		$mparam['ID'] = $idstup;
		$mparam['STATUS'] = 'EVALUASI BLIND TEST';
		$mparam['UPDATE_BY'] = $post['user'];
		$isertd_sample = $this->m_blindtest->update_status($mparam);

		// echo $this->db->last_query();
		// exit();

		// to_json($datablindtest);
		$return	= array(
  			'status'			=>	true,
  			'data'				=>	$datablindtest,
  			'msg'				=>	'Evaluate Blind Test Success',
  		);

  		echo json_encode($return);
	}
	
	public function ttl_blindtest($idsetup, $action = 'true'){
		$datablindtest = array();
		$display = '';
		$sample_blindtest= $this->m_blindtest->valuetindaklanjutblindtest($idsetup);
		// echo $this->db->last_query();
		// exit();
		// foreach ($sample_blindtest as $key => $value) {
			// $sample_blindtest['VERIFIKASI'] = empty($sample_blindtest['VERIFIKASI']) ? $sample_blindtest['VERIFIKASI'] : '<a class="btn btn-success btn-xs" href="'.site_url("./assets/uploads/tindaklanjutblindtest/".$sample_blindtest['VERIFIKASI']).'" download><i class="fa fa-download"></i> Download Attachment</a>';

			$sample_blindtest['VERIFIKASI'] = empty($sample_blindtest['VERIFIKASI']) ? $sample_blindtest['VERIFIKASI'] : $this->splitDownloadFile($sample_blindtest['VERIFIKASI']);
		// }
		$return	= array(
  			'status'			=>	true,
  			'data'				=>	$sample_blindtest,
  			// 'msg'				=>	'Evaluate Blind Test Success',
  		);

  		echo json_encode($return);
	}

	public function splitDownloadFile($filename='') {
		# code...
		$downloadbtn = array();
		$listfile = explode("@!@~$",$filename);
		foreach ($listfile as $key => $value) {
			# code...
			$downloadbtn[] = '<a class="btn btn-success btn-xs" href="'.site_url("./assets/uploads/tindaklanjutblindtest/".$value).'" download><i class="fa fa-download"></i> Download '.$value.'</a>';
		}
		return join(" ", $downloadbtn);
	}


	public function ajax_get_blindtest($idsetup, $action = 'true'){
		$datablindtest = array();
		$display = '';
		// if ($action == 'false') {
		// 	# code...
		// 	$display = ' style="display : none;" ';
		// }
		$sample_blindtest= $this->m_blindtest->tindaklanjutblindtest($idsetup);

		// echo $this->db->last_query();
		// exit();
		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$tdatablindtest[0] = empty($value['ORDER_COMP']) ? '-' : $value['ORDER_COMP'];
			$tdatablindtest[1] = empty($value['NM_COMPONENT']) ? '-' : $value['NM_COMPONENT'];
			$tdatablindtest[2] = empty($value['SCORE_EVALUASI']) ? '-' : $value['SCORE_EVALUASI'];
			$tdatablindtest[3] = empty($value['I_MAN']) ? '-' : $value['I_MAN'];
			$tdatablindtest[4] = empty($value['I_METHODE']) ? '-' : $value['I_METHODE'];
			$tdatablindtest[5] = empty($value['I_MACHINE']) ? '-' : $value['I_MACHINE'];
			$tdatablindtest[6] = empty($value['I_ENVIRONMENT']) ? '-' : $value['I_ENVIRONMENT'];
			$tdatablindtest[7] = empty($value['P_MAN']) ? '-' : $value['P_MAN'];
			$tdatablindtest[8] = empty($value['P_METHODE']) ? '-' : $value['P_METHODE'];
			$tdatablindtest[9] = empty($value['P_MACHINE']) ? '-' :  $value['P_MACHINE'];
			$tdatablindtest[10] = empty($value['P_ENVIRONMENT']) ? '-' : $value['P_ENVIRONMENT'];
			// $tdatablindtest[11] = empty($value['VERIFIKASI']) ? '-' : '<a class="btn btn-success btn-xs" href="'.site_url("./assets/uploads/tindaklanjutblindtest/".$value['VERIFIKASI']).'" download><i class="fa fa-download"></i> Download Attachment</a>';
			$tdatablindtest[11] = empty($value['VERIFIKASI']) ? '-' : $this->splitDownloadFile($value['VERIFIKASI']);
			// $tdatablindtest[12] = '<button title="Isi Tindak Lanjut" class="btTindakLanjut btn btn-warning btn-xs edit" '.$display.' type="button" onclick="tindaklanjut('.$value['ID'].')" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Isi Tindak Lanjut</button>';
			$tdatablindtest[12] = $this->PERM_WRITE ? '<button title="Isi Tindak Lanjut" class="btTindakLanjut btn btn-warning btn-xs edit" '.$display.' type="button" onclick="get_tindaklanjut('.$value['ID_EVALUASI'].','.$value['ID_BLINDTEST'].')" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Isi Tindak Lanjut</button>' : '';
			$datablindtest[] = $tdatablindtest;			
		}
		// to_json($datablindtest);
		$return	= array(
  			'data'				=>	$datablindtest,
  		);

  		echo json_encode($return);
	}

	public function ajax_get_component_display(){
			$componentlist = $this->m_blindtest->component_checklist_order();
			to_json($componentlist);
	}

	public function get_component(){
		$sample_area= $this->m_blindtest->componentlist();
		to_json($sample_area);
	}

	public function save_sample_data_blindtest() {
		# code...
		$componenmaster = $this->m_blindtest->componentmaster();

		$componentlist = array();

		foreach ($componenmaster as $key => $value) {
			# code...
			$componentlist[$value['NM_COMPONENT']] = $value['ID_COMPONENT'];
		}

		$post = $this->input->post();
		$databindtest = $post['data'];
		foreach ($databindtest as $key => $value) {
			# code...
			$param = array();
			$param['FK_ID_SETUP'] = (int) $post['id_setup'];
			$param['FK_ID_COMPONENT'] = empty($componentlist[$value[0]]) ? 0 : (int) $componentlist[$value[0]];
			$param['TESTING_1'] = empty($value[1]) ? 0 : (float) $value[1];
			$param['TESTING_2'] = empty($value[2]) ? 0 : (float) $value[2];
			$param['AVR'] = ($param['TESTING_1'] + $param['TESTING_2']) / 2;
			$param['USER'] = $post['user'];

			$inp_sample = $this->m_blindtest->input_blindtest($param);

		}

		$result['msg'] = 'success';
		$result['status'] = true;

		to_json($result);
	}

}	

?>
