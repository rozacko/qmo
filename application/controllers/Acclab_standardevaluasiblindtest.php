<?php

class Acclab_standardevaluasiblindtest extends QMUser {	
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_blindtest");
		$this->load->model("m_company");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_component = $this->m_blindtest->list_component();
		$this->list_stdevaluasi = $this->m_blindtest->list_stdevaluasi();
		$mcomp = array();
		foreach ($this->m_blindtest->componentmaster() as $key => $value) {
			# code...
			$mcomp[] = $value['NM_COMPONENT'];
		}
		$this->m_component = $mcomp;
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);	
		$this->template->adminlte("v_standardevaluasiblindtest", $data);
	}

	public function blindtest_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
		$sesi_user = $this->session->userdata();
		$user_in = json_decode(json_encode($sesi_user['USER']), true); 
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		if ($user_in["ID_USERGROUP"] && (int) $user_in["ID_USERGROUP"] == 1) {
  			# code...
  		} else {
  			# code...
  			$key['PIC_OBSERVASI'] = $user_in["ID_USER"];
  			$key['PIC_PLANT'] = $user_in["ID_USER"];
  		}

      	$data	= $this->m_blindtest->get_standardevaluasi($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_blindtest->recFil_standardevaluasi($key),
  			'recordsTotal'		=>	$this->m_blindtest->recTot_standardevaluasi($key)
  		);

  		echo json_encode($return);
    }

    public function send_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Send Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENGIRIM'] = $post['user'];
		$data['CREATE_BY'] = $post['user'];

		$isertd_sample = $this->m_blindtest->sample_send($data);
		if ($isertd_sample) {
			// code...
			$result['msg'] = 'Send Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function receipt_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Receipt Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENERIMA'] = $post['user'];
		$data['STATUS_ENTRY'] = 1;
		$data['UPDATE_BY'] = $post['user'];

		$updted_sample = $this->m_blindtest->sample_receipt($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Receipt Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function save_stdblindtest() {
    	# code...
    	$result['msg'] = 'Cannot Save Standard Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data = $post;
		unset($data['user']);
		$data['CREATE_BY'] = $post['user'];
		if (empty($data['ID'])) {
			# code...
			unset($data['ID']);
			$updted_sample = $this->m_blindtest->stdsample_submit($data);
		} else {

			unset($data['CREATE_BY']);
			$data['UPDATE_BY'] = $post['user'];
			$updted_sample = $this->m_blindtest->stdsample_update($data);
		}
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Save Standard Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function submit_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Submit Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['ID'];
		$data['STATUS_ENTRY'] = 2;
		$data['UPDATE_BY'] = $post['user'];

		$updted_sample = $this->m_blindtest->sample_submit($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Submit Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function delete_stdblindtest() {
    	# code...
    	$result['msg'] = 'Cannot Delete Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['ID'] = $post['ID'];

		$delete_sample = $this->m_blindtest->stdblind_delete($post['ID']);
		if ($delete_sample) {
			// code...
			$result['msg'] = 'Delete Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function delete_stddetailblindtest() {
    	# code...
    	$result['msg'] = 'Cannot Delete Sample Detail Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['ID'] = $post['ID'];

		$delete_sample = $this->m_blindtest->stddetailblind_delete($post['ID']);
		if ($delete_sample) {
			// code...
			$result['msg'] = 'Delete Sample Detail Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

	public function ajax_get_type_product($all = null){
		$sample_area= $this->m_blindtest->type_productlist($all);
		foreach ($sample_area as $key => $value) {
			$sample_area[$key]['KD_PRODUCT'] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
		}
		to_json($sample_area);
	}

	public function get_dataformcomponent($idstd){		
		$datablindtest = array();
		$sample_blindtest= $this->m_blindtest->compstdsampleblindtest($idstd);
		// echo $this->db->last_query(); 
		// exit();
		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$tdatablindtest[0] = $value['NM_COMPONENT'];
			$tdatablindtest[1] = empty($value['DATA']) ? '' : (double) $value['DATA'];
			$tdatablindtest[2] = empty($value['SD']) ? '' : (double) $value['SD'];
			$tdatablindtest[3] = $value['SATUAN'];
			$datablindtest[] = $tdatablindtest;			
		}
		to_json($datablindtest);
	}


	public function ajax_get_stdblindtest($idsetup, $action = 'true'){
		$datablindtest = array();
		$display = '';
		if ($action == 'false') {
			# code...
			$display = ' style="display : none;" ';
		}
		$sample_blindtest= $this->m_blindtest->stdsampleblindtest($idsetup);
		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$tdatablindtest[0] = $value['NM_COMPONENT'];
			$tdatablindtest[1] = (double) $value['DATA'];
			$tdatablindtest[2] = (double) $value['SD'];
			$tdatablindtest[3] = '';
			// $tdatablindtest[3] = '<button title="Delete" class="btDeleteBlind btn btn-danger btn-xs delete" '.$display.' type="button" onclick="deletesample('.$value['ID'].')"><i class="fa fa-trash-o"></i> Delete</button>';
			$datablindtest[] = $tdatablindtest;			
		}
		// to_json($datablindtest);
		$return	= array(
  			'data'				=>	$datablindtest,
  		);

  		echo json_encode($return);
	}

	public function ajax_get_component_display(){
			$componentlist = $this->m_blindtest->component_checklist_order();
			to_json($componentlist);
	}

	public function get_component(){
		$sample_area= $this->m_blindtest->componentlist();
		to_json($sample_area);
	}

	public function save_standard_data_blindtest() {
		# code...
		$componenmaster = $this->m_blindtest->componentmaster();

		$componentlist = array();

		foreach ($componenmaster as $key => $value) {
			# code...
			$componentlist[$value['NM_COMPONENT']] = $value['ID_COMPONENT'];
		}

		$post = $this->input->post();
		$databindtest = $post['data'];
		foreach ($databindtest as $key => $value) {
			# code...
			$param = array();
			$param['FK_ID_STD'] = (int) $post['FK_ID_STD'];
			$param['FK_ID_COMPONENT'] = empty($componentlist[$value[0]]) ? 0 : (int) $componentlist[$value[0]];
			$param['DATA'] = empty($value[1]) ? 0 : (float) $value[1];
			$param['SD'] = empty($value[2]) ? 0 : (float) $value[2];
			$param['USER'] = $post['user'];

			$inp_sample = $this->m_blindtest->input_stdblindtest($param);

		}

		$data['ID'] = $post['FK_ID_STD'];
		$data['IS_ACTIVE'] = 1;
		$data['UPDATE_BY'] = $post['user'];
		$updted_sample = $this->m_blindtest->stdsample_update($data);

		$result['msg'] = 'success';
		$result['status'] = true;

		to_json($result);
	}

	public function save_sample_data_blindtest() {
		# code...
		$componenmaster = $this->m_blindtest->componentmaster();

		$componentlist = array();

		foreach ($componenmaster as $key => $value) {
			# code...
			$componentlist[$value['NM_COMPONENT']] = $value['ID_COMPONENT'];
		}

		$post = $this->input->post();
		$databindtest = $post['data'];
		foreach ($databindtest as $key => $value) {
			# code...
			$param = array();
			$param['FK_ID_SETUP'] = (int) $post['id_setup'];
			$param['FK_ID_COMPONENT'] = empty($componentlist[$value[0]]) ? 0 : (int) $componentlist[$value[0]];
			$param['TESTING_1'] = empty($value[1]) ? 0 : (float) $value[1];
			$param['TESTING_2'] = empty($value[2]) ? 0 : (float) $value[2];
			$param['AVR'] = ($param['TESTING_1'] + $param['TESTING_2']) / 2;
			$param['USER'] = $post['user'];

			$inp_sample = $this->m_blindtest->input_blindtest($param);

		}

		$result['msg'] = 'success';
		$result['status'] = true;

		to_json($result);
	}

}	

?>
