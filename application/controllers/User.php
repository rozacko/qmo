<?php

class User extends QMUser {
	
	public $list_company = array();
	public $list_plant = array();
	public $list_user = array();
	public $comp = '';
	public $list_usergroup = array();
	public $list_jobgrup = array();
	
	public $ID_COMPANY;
	public $ID_USER;
	public $data_user;
	public $dtController = 'user';
	public $dtTitle = 'Master Data User';
	public $viewer;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_user");
		$this->load->model("m_usergroup");
		$this->load->model("employee");
		$this->load->model("m_roles");
	}
	
	public function index(){
	//	$this->list_company   = $this->m_company->datalist();
	//	$this->list_plant 	  = $this->m_plant->datalist($this->ID_COMPANY); #echo $this->m_plant->get_sql();
	//	$this->list_usergroup = $this->m_usergroup->datalist();
		$this->comp = $this->USER->ID_COMPANY;
		$this->viewer = $this->getViewer();
		$this->list_user 	  = $this->m_user->datalist($this->USER->ID_COMPANY); #echo $this->m_plant->get_sql();
		$this->template->adminlte("v_user");
	}

	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}	

	public function by_plant($ID_USER=NULL){
		$this->ID_USER   = $ID_USER;
		$this->ID_COMPANY = $this->m_plant->data("ID_USER='$ID_USER'")->ID_COMPANY;
		$this->index();
	}
	
	public function add(){
		$this->libExternal('select2');

		$this->ID_COMPANY 	= $ID_COMPANY;
		$this->ID_USER   	= $ID_USER;

		$list_company = $this->m_company->list_company($this->session->userdata()['USER']->ID_COMPANY);
		$this->list_company = array();
		if($this->session->userdata()['USER']->ID_COMPANY){
			foreach ($list_company as $key => $value) {
				if($this->session->userdata()['USER']->ID_COMPANY == $value->ID_COMPANY){
					$this->list_company[] = $value;
				}
			}
		}else{
			$this->list_company = $list_company;
		}
		$this->adm = $this->getAccess('ADM');

		// var_dump($this->list_company);
		// exit;
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_usergroup = $this->m_usergroup->datalist();		
		$this->list_jobgrup = $this->get_jobgrup();
		$this->template->adminlte("v_user_add2");
	}
	public function add_lama(){
		$this->libExternal('select2');

		$this->ID_COMPANY 	= $ID_COMPANY;
		$this->ID_USER   	= $ID_USER;
		$this->list_company = $this->m_company->list_company();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->template->adminlte("v_user_add");
	}
	
	public function create(){
	//	var_dump($this->input->post()); exit;
		//check user exists
		$uname = array('USERNAME'=> trim(strtolower($this->input->post("USERNAME"))));
		$cek = $this->m_user->data($uname);
		if($cek){
			$this->notice->error("USERNAME ALREADY EXISTS");
			redirect("user");
		}
		else{
			$this->m_user->insert($this->input->post()); #die($this->m_user->get_sql());
			if($this->m_user->error()){
				$this->notice->error($this->m_user->error());
				redirect("user/add");
			}
			else{
				$this->notice->success("Users Data Saved.");
				redirect("user");
			}
		}
	}


	public function createProcess(){
		// var_dump($this->input->post());
		$status = 'error';
		$uname = array('USERNAME'=> trim(strtolower($this->input->post("USERNAME"))));
		$cek = $this->m_user->data($uname);
		if($cek){
			$this->response(array('status'=>$status, 'message' => 'Sorry, username already exists!'));
			exit;
		}

		$data = $this->input->post();
		$rolesPost = $data['ID_USERGROUP'];
		unset($data['ID_USERGROUP']);
		if($rolesPost == 'null'){
			$this->response(array('status'=>$status, 'message' => 'Sorry, roles cannot be empty!'));
			exit;
		}

		$data['ID_COMPANY'] = $this->input->post("ID_COMPANY");
		$data['ID_PLANT'] = $this->input->post("ID_PLANT");
		$data['ID_AREA'] = $this->input->post("ID_AREA");
		$data['FLAG_GRUP'] = $this->input->post("FLAG_GRUP");
		$data['ISACTIVE'] = 'Y';
		$encript = 'AGREE-'.password_hash((md5($data['USERNAME'].$data['EMAIL'].strtotime(date('Ymd')))), PASSWORD_DEFAULT);
		$data['VERCODE'] = $encript;

		$this->m_user->insert($data); #die($this->m_user->get_sql());
		// echo $this->db->last_query();
		if($this->m_user->error()){
			$message = $this->m_user->error();
		}
		else{
			$lastId = $this->m_user->maxId(); #die($this->m_user->get_sql());
			$roles = array();
			$rolesPost = explode(',', $rolesPost);
			foreach ($rolesPost as $i => $v) {
				$roles[$i]['ID_USER'] = $lastId;
				$roles[$i]['ID_USERGROUP'] = $v;
			}

			$insBatch = $this->m_roles->insertBatch($roles); #die($this->m_user->get_sql());

			$status = 'success';
			$message = 'Users Data Saved';
		}

		$this->response(array('status'=>$status, 'message' => $message));


	}
	

	
	public function edit($ID_USER){
		$this->libExternal('select2');
		$this->data_user 	= $this->m_user->get_data_by_id($ID_USER);
		$this->data_user->USERGROUP 	= $this->m_roles->get_data_role_by_iduser($ID_USER);

		// var_dump($this->data_user);
		// exit;
		$this->ID_COMPANY 	= $ID_COMPANY;
		$this->ID_USER   	= $ID_USER;
		$list_company = $this->m_company->list_company($this->session->userdata()['USER']->ID_COMPANY);
		$this->list_company = array();
		if($this->session->userdata()['USER']->ID_COMPANY){
			foreach ($list_company as $key => $value) {
				if($this->session->userdata()['USER']->ID_COMPANY == $value->ID_COMPANY){
					$this->list_company[] = $value;
				}
			}
		}else{
			$this->list_company = $list_company;
		} 
		$this->adm = $this->getAccess('ADM');

		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->list_jobgrup = $this->get_jobgrup();
		$this->template->adminlte("v_user_edit2");
	}
	
	public function update($ID_USER){
		if($this->m_user->data_except_id(array('USERNAME'=>$this->input->post("USERNAME")),$ID_USER)){
			$this->notice->error("USERNAME ALREADY EXISTS");
		}
		else{
			$data = $this->input->post();
			$data['ID_COMPANY'] = $this->input->post("ID_COMPANY");
			$data['ID_PLANT'] = $this->input->post("ID_PLANT");
			$data['ID_AREA'] = $this->input->post("ID_AREA");
			$this->m_user->update($data,$ID_USER); #die($this->m_user->get_sql());
			if($this->m_user->error()){
				$this->notice->error($this->m_user->error());
				redirect("user/edit/".$ID_USER);
			}
			else{
				$this->notice->success("Users Data Updated.");
				redirect("user");
			}
		}
	}

	public function updateProcess($ID_USER){
		$status = 'error';
		if($this->m_user->data_except_id(array('USERNAME'=>$this->input->post("USERNAME")),$ID_USER)){
			$message = 'Username already exists!';
			$this->response(array('status'=>$status, 'message' => $message));
			exit;
		}
		$data_user 	= $this->m_user->get_data_by_id($ID_USER);
		if($data_user->DELETED == '1'){
			$this->response(array('status'=>$status, 'message' => 'Sorry, this user has been deleted, cannot be edited!'));
			exit;
		}

		$data = $this->input->post();
		$rolesPost = $data['ID_USERGROUP'];
		unset($data['ID_USERGROUP']);
		if($rolesPost == 'null'){
			$this->response(array('status'=>$status, 'message' => 'Sorry, roles cannot be empty!'));
			exit;
		}

		$data['ID_COMPANY'] = $this->input->post("ID_COMPANY");
		$data['ID_PLANT'] = $this->input->post("ID_PLANT");
		$data['ID_AREA'] = $this->input->post("ID_AREA");
		$data['FLAG_GRUP'] = $this->input->post("FLAG_GRUP");
		$data['ISACTIVE'] = 'Y';
		$encript = 'AGREE-'.password_hash((md5($data['USERNAME'].$data['EMAIL'].strtotime(date('Ymd')))), PASSWORD_DEFAULT);
		$data['VERCODE'] = $encript;


		$this->m_user->update($data,$ID_USER); #die($this->m_user->get_sql());
		// echo $this->db->last_query();
		if($this->m_user->error()){
			 
			$message = $this->m_user->error();
		}
		else{ 
			$roles = array();
			$rolesPost = explode(',', $rolesPost);
			foreach ($rolesPost as $i => $v) {
				$roles[$i]['ID_USER'] = $ID_USER;
				$roles[$i]['ID_USERGROUP'] = $v;
			}

			$insBatch = $this->m_roles->insertBatch($roles); #die($this->m_user->get_sql());

			$status = 'success';
			$message = 'Users Data Updated';
		}

		$this->response(array('status'=>$status, 'message' => $message));
		// echo 999;
		// exit;

	}
	
	public function delete($ID_USER){
		$this->m_user->delete($ID_USER);
		if($this->m_user->error()){
			$this->notice->error($this->m_user->error());
		}
		else{
			$this->notice->success("Users Data Removed.");
		}
		redirect("user");
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}
	public function get_jobgrup(){		
		$jobgrup = [ ['FLAG_GRUP' => 'QA', 'NM_JOBGRUP'=>'QA'],
						['FLAG_GRUP' => 'QC', 'NM_JOBGRUP'=>'QC'],
						['FLAG_GRUP' => 'NON_QA_QC', 'NM_JOBGRUP'=>'NON QA QC']	
					];
		$obj = (object) $jobgrup;
		return $obj;
	}
	public function get_list($company = null){
		$viewer = $this->getViewer();
		if($viewer){
			$list = $this->m_user->get_list();
		}else{
			$list = $this->m_user->get_list($company);
		}
		$data = array();
		$no   = $this->input->post('start');

// var_dump($list);
// exit;
		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USER;
			$listUserGroup = $this->m_roles->get_data_role_by_iduser($column->ID_USER);
			$rowsUG = array();
			foreach ($listUserGroup as $key => $value) {
				$rowsUG[] = $value->NM_USERGROUP;
			}
			$rowsUG = join($rowsUG, ', ');
			// exit;
			$row[] = $no;
			if($viewer){
				$row[] = $column->NM_COMPANY;
			}
			
			if($column->ISACTIVE == 'Y'){
				$status = 'Active';
			}else{
				$status = 'Deactivate';
			}

			$row[] = $column->FULLNAME;
			$row[] = $column->USERNAME;
			$row[] = $rowsUG;
			$row[] = $status;
			
			// $row[] = $column->NM_USERGROUP;
			$btn = '';
			if($this->PERM_WRITE){ 
				if($this->session->userdata()['USER']->ID_COMPANY){
					if($column->ID_COMPANY == $this->session->userdata()['USER']->ID_COMPANY){
						$btn = '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';
					}
				}else{
					$btn = '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';
				}
			}
			$row[] = $btn;

			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_user->count_all(),
            "recordsFiltered" => $this->m_user->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	function activate($id = null, $status = null){
		$this->m_user->activate($id, $status);
		if($this->m_user->error()){
			$this->response(array('status' => 'error', 'message' => $this->m_user->error()));
		}
		else{
			$this->response(array('status' => 'success', 'message' => 'Successful status change'));
		}

	}

}	

?>
