<?php

class Detail_plant extends QMUser {
	
	public $list_data = array();
	public $data_company;
	public $data_detail_plant;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_detail_plant");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->template->adminlte("v_detail_plant");
	}
	
	public function add(){
		$this->data_company = $this->m_company->get_company_use_detail_plant();
		$this->template->adminlte("v_detail_plant_add");
	}

	public function ajax_get_plant_by_company($ID_COMPANY){
		$data_plant = $this->m_detail_plant->get_plant_company_use_detail($ID_COMPANY);
		to_json($data_plant);
	}
	
	public function create(){
		
		$this->load->library('form_validation');
		
		$this->m_detail_plant->insert($this->input->post());
		if($this->m_detail_plant->error()){
			$this->notice->error($this->m_detail_plant->error());
			redirect("detail_plant/add");
		}
		else{
			$this->notice->success("Data Detail Plant Saved.");
			redirect("detail_plant");
		}
	}
	
	public function edit($ID_DETAIL_PLANT){
		$this->data_company = $this->m_company->get_company_use_detail_plant();
		$this->data_detail_plant = $this->m_detail_plant->get_data_by_id($ID_DETAIL_PLANT);
		$this->template->adminlte("v_detail_plant_edit");
	}
	
	public function update($ID_COMPANY){
		$this->m_detail_plant->update($this->input->post(),$ID_COMPANY);
		if($this->m_detail_plant->error()){
			$this->notice->error($this->m_detail_plant->error());
			redirect("detail_plant/edit/".$ID_COMPANY);
		}
		else{
			$this->notice->success("Data Detail Plant Updated.");
			redirect("detail_plant");
		}
	}
	
	public function delete($ID_DETAIL_PLANT){
		$this->m_detail_plant->delete($ID_DETAIL_PLANT);
		if($this->m_detail_plant->error()){
			$this->notice->error($this->m_detail_plant->error());
		}
		else{
			$this->notice->success("Company Data Removed.");
		}
		redirect("detail_plant");
	}

	public function get_list(){
		$list = $this->m_detail_plant->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row['NO'] = $no;
			$row['ID_DETAIL_PLANT'] = $column->ID_DETAIL_PLANT;
			$row['NM_COMPANY'] = $column->NM_COMPANY;
			$row['NM_PLANT'] = $column->NM_PLANT;
			$row['KD_DETAIL_PLANT'] = $column->KD_DETAIL_PLANT;
			$row['NM_DETAIL_PLANT'] = $column->NM_DETAIL_PLANT;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_detail_plant->count_all(),
            "recordsFiltered" => $this->m_detail_plant->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
