<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View_sample_quality extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_quality_comparison_report");
		$this->load->model("m_sample_quality");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("C_range_component");
		$this->load->library("excel");
	}

	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_view_sample_quality", $data);
	}

	public function excel_create($id_company, $tanggal, $id_typeproduct){
		# code...

		$tanggal = str_replace("-", "/", $tanggal);
		$tanggal = str_replace("~", " - ", $tanggal);

		$periode = explode(" - ", $tanggal);
		$type_product = explode("~", $id_typeproduct);

		$sample_data = $this->load_sample_quality_data_to_excel($id_company, $type_product, $periode[0] , $periode[1]);

		$company_detail = $this->m_quality_comparison_report->get_competitor_detail_scm($id_company);

		$default_border = array(
	    	'style' => PHPExcel_Style_Border::BORDER_THIN,
	    	'color' => array('rgb'=>'0a0a0a')
	    );

	    $style_text = array(
	        'alignment' => array(
	            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	            'vertical' => PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY,
	        )
	    );

	    $style_header = array(
	    	'borders' => array(
	    		'bottom' => $default_border,
	    		'left' => $default_border,
	    		'top' => $default_border,
	    		'right' => $default_border,
	    	),
	    	'fill' => array(
	    		'type' => PHPExcel_Style_Fill::FILL_SOLID,
	    		'color' => array('rgb'=>'00bfff'),
	    	),
	    	'font' => array(
	    		'bold' => true,
	    	)
	    );

		if ($sample_data['data']) {
			# code...

			$sheet = $this->excel->getActiveSheet();

			$indexsheet = 0;
			$charx = 'A';
			$lastchar = 'A';
			$objWorkSheet = $this->excel->createSheet($indexsheet);
			$objWorkSheet->setTitle('Sample Data List');

			foreach ($sample_data['header']['colHeader'] as $key => $value) {
				# code...
			    $objWorkSheet->getColumnDimension($charx)->setAutoSize(true);
				$objWorkSheet->setCellValue($charx.'1', $value['title']);
				$lastchar = $charx;
				$charx++;
			}

			$charx--;
			
			$objWorkSheet->getStyle('A1:'.$lastchar.'1')->applyFromArray( $style_header );

			$inddata = 2;
			foreach ($sample_data['data'] as $key => $value) {
				# code...				
				$charx = 'A';
				foreach ($value as $ind => $val) {
					# code...
					$objWorkSheet->setCellValue($charx.$inddata, $val);
					$charx++;
				}
				$inddata++;
			}

		}

		$company_name = $company_detail['NAMA_PERUSAHAAN'].' ';

		$filename = 'Sample Quality '.$company_name.' Periode : '.$tanggal.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		            
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD


		$objWriter->save("php://output");

	}

	public function load_sample_quality_data_to_excel($id_company, $id_typeproduct, $startdatetxt , $enddatetxt){
		$id_company = $id_company;
		$id_typeproduct = $id_typeproduct;
		$startdate = date("Y-m-d", strtotime($startdatetxt));
		$enddate = date("Y-m-d", strtotime($enddatetxt));

		$colHeader 	= $this->get_component_list();
		$data = array();

		$gsampledata = $this->m_sample_quality->get_data_sample_group_scm($id_typeproduct, $id_company, $startdate, $enddate);

		foreach ($gsampledata as $key => $value) {
			$gsamplearea = $this->m_sample_quality->get_area_sample_list_scm($value);
			if ($gsamplearea) {
				 $gsampledata[$key]['ID_M_KOTA'] = $gsamplearea['ID_M_KOTA'];
				 $gsampledata[$key]['KD_PROP'] = $gsamplearea['KD_PROP'];
				 $gsampledata[$key]['KD_AREA'] = $gsamplearea['KD_AREA'];
				 $gsampledata[$key]['KD_KOTA'] = $gsamplearea['KD_KOTA'];
				 $gsampledata[$key]['NM_KOTA'] = $gsamplearea['NM_KOTA'];
			}
		}

		foreach ($gsampledata as $key => $value) {
			$gcompetitor = $this->m_sample_quality->get_competitor_detail_scm($value['ID_COMPETITOR']);
			if ($gcompetitor) {
				 $gsampledata[$key]['NAMA_PERUSAHAAN'] = $gcompetitor['NAMA_PERUSAHAAN'];
				 $gsampledata[$key]['KODE_PERUSAHAAN'] = $gcompetitor['KODE_PERUSAHAAN'];
				 $gsampledata[$key]['INISIAL'] = $gcompetitor['INISIAL'];
				 $gsampledata[$key]['PRODUK'] = $gcompetitor['PRODUK'];
				 $gsampledata[$key]['KELOMPOK'] = $gcompetitor['KELOMPOK'];
			}
		}

		foreach ($gsampledata as $key => $value) {

			$tdata = array();
			$tdata[] = date("d/m/Y", strtotime($value['DATE_COLLECT']));
			$tdata[] = $value['NM_KOTA'];
			$tdata[] = $value['PRODUK'];
			$tdata[] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
			// # code...
			for ($i=4; $i < count($colHeader['id_comp']) ; $i++) { 
				# code...
				$sampledata = $this->m_sample_quality->get_data_sample_list($value, $colHeader['id_comp'][$i], strtolower($colHeader['colHeader'][$i]['title']), date("Y-m-d", strtotime($value['DATE_COLLECT'])));

				$mathround = -1;

				$precissio_conf = $this->m_sample_quality->component_precission(0, (int) $colHeader['id_comp'][$i], 0);

				if ($precissio_conf) {
					# code...
					$mathround = (int) $precissio_conf['PRECISSION'];
				}

				if ($sampledata) {
					# code...

					$cv = 0;

					foreach ($sampledata as $ind => $val) {
						if ($value['DATE_COLLECT'] == $val['DATE_COLLECT']) {
							if ($mathround < 0) {
								# code...
								$tdata[] = (double) $val['COMPONENT_VALUE'];
							} else if ($mathround == 0) {
								# code...
								$tdata[] = (int) $val['COMPONENT_VALUE'];
							} else {
								# code...
								// $tdata[] = round((double) $val['COMPONENT_VALUE'], $mathround);								
								$tdata[] = number_format($val['COMPONENT_VALUE'], $mathround, '.', '');
							}
							
							$cv++;
						}
					}

					if ($cv == 0) {
						$tdata[] = '';
					}

				} else {
					# code...
					$tdata[] = '';
				}
			}

			$data[] = $tdata;
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;

		return $result;
	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY, $this->USER->ID_PLANT);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
		to_json($area);
	}

	private function get_component_list(){
		$param = $this->m_sample_quality->component_checklist_order();
		$id_comp[] = '';
		$header[]['title'] = 'Date';
		$id_comp[] = '';
		$header[]['title'] = 'Area';
		$id_comp[] = '';
		$header[]['title'] = 'Competitor';
		$id_comp[] = '';
		$header[]['title'] = 'Type Product';

		foreach ($param as $key => $value) {

			# code...
			if ((int)$value['STATUS_CHECKLIST'] == 1) {
				# code...
				$id_comp[] = $value['ID_COMPONENT'];
				$header[]['title'] = strtoupper($value['KD_COMPONENT']);
			}
		}

		// exit;

		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}


	public function load_sample_quality_data(){
		$post = $this->input->post();

		$id_company = $post['id_company'];
		$name_company = $post['name_company'];
		$id_typeproduct = $post['id_typeproduct'];
		$type_product = $this->str_clean($post['type_product']);
		$startdate = date("Y-m-d", strtotime($post['startdate']));
		$enddate = date("Y-m-d", strtotime($post['enddate']));

		$colHeader 	= $this->get_component_list();
		$data = array();

		$gsampledata = $this->m_sample_quality->get_data_sample_group_scm($id_typeproduct, $id_company, $startdate, $enddate);

		foreach ($gsampledata as $key => $value) {
			$gsamplearea = $this->m_sample_quality->get_area_sample_list_scm($value);
			if ($gsamplearea) {
				 $gsampledata[$key]['ID_M_KOTA'] = $gsamplearea['ID_M_KOTA'];
				 $gsampledata[$key]['KD_PROP'] = $gsamplearea['KD_PROP'];
				 $gsampledata[$key]['KD_AREA'] = $gsamplearea['KD_AREA'];
				 $gsampledata[$key]['KD_KOTA'] = $gsamplearea['KD_KOTA'];
				 $gsampledata[$key]['NM_KOTA'] = $gsamplearea['NM_KOTA'];
			}
		}

		foreach ($gsampledata as $key => $value) {
			$gcompetitor = $this->m_sample_quality->get_competitor_detail_scm($value['ID_COMPETITOR']);
			if ($gcompetitor) {
				 $gsampledata[$key]['NAMA_PERUSAHAAN'] = $gcompetitor['NAMA_PERUSAHAAN'];
				 $gsampledata[$key]['KODE_PERUSAHAAN'] = $gcompetitor['KODE_PERUSAHAAN'];
				 $gsampledata[$key]['INISIAL'] = $gcompetitor['INISIAL'];
				 $gsampledata[$key]['PRODUK'] = $gcompetitor['PRODUK'];
				 $gsampledata[$key]['KELOMPOK'] = $gcompetitor['KELOMPOK'];
			}
		}		

		foreach ($gsampledata as $key => $value) {
			$tdata = array();
			$tdata[] = date("d/m/Y", strtotime($value['DATE_COLLECT']));
			$tdata[] = $value['NM_KOTA'];
			$tdata[] = $value['PRODUK'];
			$tdata[] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
			// # code...
			for ($i=4; $i < count($colHeader['id_comp']) ; $i++) { 
				# code...
				$sampledata = $this->m_sample_quality->get_data_sample_list($value, $colHeader['id_comp'][$i], strtolower($colHeader['colHeader'][$i]['title']), date("Y-m-d", strtotime($value['DATE_COLLECT'])));

				$mathround = -1;

				$precissio_conf = $this->m_sample_quality->component_precission(0, (int) $colHeader['id_comp'][$i], 0);
				
				if ($precissio_conf) {
					# code...
					$mathround = (int) $precissio_conf['PRECISSION'];
				}

				if ($sampledata) {
					# code...

					$cv = 0;

					foreach ($sampledata as $ind => $val) {
						if ($value['DATE_COLLECT'] == $val['DATE_COLLECT']) {
							if ($mathround < 0) {
								# code...
								$tdata[] = (double) $val['COMPONENT_VALUE'];
							} else if ($mathround == 0) {
								# code...
								$tdata[] = (int) $val['COMPONENT_VALUE'];
							} else {
								# code...
								// $tdata[] = round((double) $val['COMPONENT_VALUE'], $mathround);
								$tdata[] = number_format($val['COMPONENT_VALUE'], $mathround, '.', '');
							}
							
							$cv++;
						}
					}

					if ($cv == 0) {
						$tdata[] = '';
					}

				} else {
					# code...
					$tdata[] = '';
				}
			}

			$data[] = $tdata;
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;

		to_json($result);
	}

	private function get_component($id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'D');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->KD_COMPONENT);
		}

		//Tambahkan Remark
		if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';

		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_daily
		$colHeader 	= array();
		$data 		= array();
		$t_prod 	= $this->t_production_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-2;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_daily->get_by_id($row->ID_PRODUCTION_DAILY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

	public function load_cement(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_daily
		$colHeader 	= array();
		$data 		= array();
		$testing = null;
		if ($form->ID_GROUPAREA==4) {
			$t_prod 	= $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		}else{
			$t_prod 	= $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND ID_PRODUCT='".$form->ID_PRODUCT."'");
		}

		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-2;
		$test_array = array();
		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				array_push($test_array, $row->ID_CEMENT_DAILY);
				$d_prod = $this->d_cement_daily->get_by_id($row->ID_CEMENT_DAILY);
				$tes_d_prod_1 = $this->d_cement_daily->tes($row->ID_CEMENT_DAILY,$test_array);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
				$tes_d_prod_2 = $this->d_cement_daily->tes2($test_array);
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		$result['test1'] = $t_prod;
		$result['test2'] = $tes_d_prod_1;
		$result['test3'] = $tes_d_prod_2;
		to_json($result);
	}

	public function save_table(){
		$hanClean = array();
		$kolom 	  = 0;
		$rows 	  = array();
		$hanData  = $this->input->post('data');
		$formData = $this->input->post('formData');
		$id_comp  = $this->input->post('id_comp');
		$ct_cmp	  = count($id_comp);


		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;

		//T_PRODUCTION_DAILY (1): ID_PRODUCTION_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION_DAILY, ID_COMPONENT, NILAI

		//read line by line
		$tgl_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_date			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status", $comp);
			$i_remark		= array_search("_remark", $comp);

			#if(!$row[$i_date]) continue;

			//T
			$tdata['ID_AREA']					= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['DATE_DATA']				= str_pad($tgl_auto,2,'0',STR_PAD_LEFT)."/".$form->TANGGAL; # $row[$i_date] dd/mm/yyyy
			//$tdata['DATE_SET']			= date("d/m/Y"); //if entrydate exists then update_date else entry_date
			//$tdata['USER_SET']			= $this->USER->ID_USER; //if entryuser exists then update user else entry user
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$status_mesin 						= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save T
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if ($mati == $t_kolom) {
				$tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			$ID_PRODUCTION_DAILY = $this->t_production_daily->get_id($tdata[ID_AREA],$tdata[DATE_DATA]);

			if(!$ID_PRODUCTION_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_PRODUCTION_DAILY = $this->t_production_daily->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_production_daily->update($tdata,$ID_PRODUCTION_DAILY);
			}

			$tgl_auto++;

			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_DAILY'] 	= $ID_PRODUCTION_DAILY;
				$ddata['ID_COMPONENT']			= $comp[$x];
				$ddata['NILAI']					= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']				= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_production_daily->d_exists($ddata)){
						$this->t_production_daily->d_insert($ddata);
					}
					else{
						$this->t_production_daily->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_production_daily->d_exists($ddata)){
							$this->t_production_daily->d_insert($ddata);
						}
						else{
							$this->t_production_daily->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}
				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}

				if(!$this->t_production_daily->d_exists($ddata)){
					$this->t_production_daily->d_insert($ddata);
				}
				else{
					$this->t_production_daily->d_update($ddata);
					//echo $this->db->last_query() . "\n";
				}
			}

			//exit;
			$ct_row++;

		}

		to_json(array("result" => 'ok'));
	}

	public function save_table_cement(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;
		$bulan = explode("/", $form->TANGGAL);
		$form->BULAN = $bulan[0];

		//T_PRODUCTION_DAILY (1): ID_CEMENT_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_UPDATE,JAM_UPDATE,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_CEMENT_DAILY, ID_COMPONENT, NILAI

		//read line by line
		$tgl_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_date	  		= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status", $comp);
			$i_remark 		= array_search("_remark", $comp);

			#if(!$row[$i_date]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= str_pad($tgl_auto,2,'0',STR_PAD_LEFT)."/".$form->TANGGAL; # $row[$i_date] dd/mm/yyyy
			$status_mesin 				= strtoupper($row[$i_mesin_status]);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if ($mati == $t_kolom) {
				$tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			$ID_CEMENT_DAILY = $this->t_cement_daily->get_id($tdata[ID_AREA],$tdata[ID_PRODUCT],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			#echo $this->db->last_query() . "\n\n";
			if(!$ID_CEMENT_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);

			}


			#echo $this->db->last_query()."\n\n";
			//D
			for($x=0;$x<$i_mesin_status;$x++){


				// echo '<pre>';
				// var_dump($row[$x]);
				// echo '</pre>';


				$ddata = null;
				$ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
				$ddata['ID_COMPONENT']		= $comp[$x];
				$ddata['NILAI']				= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']			= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_cement_daily->d_exists($ddata)){
						$this->t_cement_daily->d_insert($ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
						$tes_tes_update = $this->t_cement_daily->tes_update($ddata);
					}
					continue;
				}
					$result['tes_tes_update'] = $tes_tes_update;

				/* Check IF CS */
				$str = strtoupper($this->m_component->get_data_by_id($ddata['ID_COMPONENT'])->KD_COMPONENT);
				if (substr_compare($str, 'CS', 0, 2)===0) {
					$ddata['NILAI']	= ($row[$x]=='') ? '':$row[$x];
					if(!$this->t_cement_daily->d_exists($ddata)){
						$this->t_cement_daily->d_insert($ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_cement_daily->d_exists($ddata)){
							$this->t_cement_daily->d_insert($ddata);
						}
						else{
							$this->t_cement_daily->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}

				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}



				if(!$this->t_cement_daily->d_exists($ddata)){
					$this->t_cement_daily->d_insert($ddata);
					#echo $this->db->last_query();die();
				}
				else{
					$this->t_cement_daily->d_update($ddata);
				}
				#echo $this->db->last_query().";".PHP_EOL;
			}

			$tgl_auto++;
			$ct_row++;

		}
		// to_json(array("result" => 'ok'));
		to_json($result);
				// exit;
	}

	private function str_clean($chr){
		// $str ='([^.0-9-]+)';
		// return preg_replace($str, '', $chr);
		return preg_replace('/\s+/', '', $chr);
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color 				= getFixColor($i);
			$nilai 				= array_filter($trace[$i], 'is_numeric');

			if (empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= trim($comp[$i]->title);
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;


			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
