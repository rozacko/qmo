<?php

class Master_sni_std_product extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_thirdmaterial");
		$this->load->model("m_company");
		// $this->load->library("excel");
	}
	
	public function index(){
		$this->list_component = $this->m_thirdmaterial->datalist();
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_std_sni_product", $data);
	}

	public function excel_create(){
		# code...
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		            
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		
		$result['msg'] = 'Cannot Insert New Product ...';
		$result['status'] = false;
		
		to_json($result);
	}

	public function save_third_material(){
		
		$result['msg'] = 'Cannot Insert New Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['KODE_MATERIAL'] = $post['KODE_MATERIAL'];
		$data['NAME_MATERIAL'] = $post['NAME_MATERIAL'];
		$data['CREATE_BY'] = $post['user'];

		$isexsist_product = $this->m_thirdmaterial->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_thirdmaterial->sample_insert($data);
			if ($isertd_product) {
				// code...
				$result['msg'] = 'Insert New Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}
		
		to_json($result);
	}

	public function update_third_material(){
		
		$result['msg'] = 'Cannot Update Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_MATERIAL'] = $post['ID_MATERIAL'];
		$data['KODE_MATERIAL'] = $post['KODE_MATERIAL'];
		$data['NAME_MATERIAL'] = $post['NAME_MATERIAL'];
		$data['MODIFIED_BY'] = $post['user'];

		$isexsist_product = $this->m_thirdmaterial->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_thirdmaterial->sample_update($data);

			if ($isertd_product) {
				// code...
				$result['msg'] = 'Update Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}

		to_json($result);
	}

	public function delete_third_material(){
		
		$result['msg'] = 'Cannot Delete Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_MATERIAL'] = $post['ID_MATERIAL'];
		$data['DELETE_BY'] = $post['user'];

		$isertd_product = $this->m_thirdmaterial->sample_delete($data);
		if ($isertd_product) {
			// code...
			$result['msg'] = 'Delete Product Success ...';
			$result['status'] = true;
		}

		to_json($result);
	}

	public function third_material_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		// $data	= $this->Job_m->get($key);

      	$data	= $this->m_thirdmaterial->get_data($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_thirdmaterial->recFil($key),
  			'recordsTotal'		=>	$this->m_thirdmaterial->recTot($key)
  		);

  		echo json_encode($return);
    }

    public function product_list_scm(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		// $data	= $this->Job_m->get($key);

      	$data	= $this->m_thirdmaterial->get_data_scm($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_thirdmaterial->recFil_scm($key),
  			'recordsTotal'		=>	$this->m_thirdmaterial->recTot_scm($key)
  		);

  		echo json_encode($return);
    }

}	

?>
