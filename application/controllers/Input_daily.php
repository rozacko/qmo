<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_daily extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_parameter_order");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_daily"); 
		$this->load->model("d_production_daily");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily"); 
		$this->load->model("C_range_component");
	}

	public function index(){
        
        $this->libExternal('datepicker');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_daily", $data);
	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY, $this->USER->ID_PLANT);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
		to_json($area);
	}

	private function get_component($id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'D');
		// $param = $this->c_parameter_order->get_component($id_grouparea,'D');
		
		// echo $this->db->last_query();die();
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			// $header[]['title'] = strtoupper($cmp->KD_COMPONENT);
			$header[] = strtoupper($cmp->KD_COMPONENT);
		}

		// ---- start izza ----
		// //Tambahkan Remark
		// if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		// $header[]['title'] = 'REMARK';

		// if(!empty($tipe)) $id_comp[] = '_machine_status';
		// $id_comp[] = '_remark';
		
		//Tambahkan Remark
		if(!empty($tipe)) $header[] = 'MACHINE STATUS';
		$header[] = 'REMARK';

		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';
		// ---- end izza ----

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_daily
		$t_prod 	= $this->t_production_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		
		$data 		= array();
		$ctHead		= count($colHeader['id_comp'])-2;
		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_daily->get_by_id($row->ID_PRODUCTION_DAILY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

	public function load_cement(){
		$post = $this->input->post();
		//print_r($post);
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_daily
		if ($form->ID_GROUPAREA==4) {
			$where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'";
		}else{
			$where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND ID_PRODUCT='".$form->ID_PRODUCT."'";
		}
		$t_prod 	= $this->t_cement_daily->data_where($where);
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		
		$data 		= array();
		$ctHead		= count($colHeader['id_comp'])-2;
		$test_array = array();
		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				array_push($test_array, $row->ID_CEMENT_DAILY);
				$d_prod = $this->d_cement_daily->get_by_id($row->ID_CEMENT_DAILY);
				$tes_d_prod_1 = $this->d_cement_daily->tes($row->ID_CEMENT_DAILY,$test_array);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
				$tes_d_prod_2 = $this->d_cement_daily->tes2($test_array);
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		$result['test1'] = $t_prod;
		$result['test2'] = $tes_d_prod_1;
		$result['test3'] = $tes_d_prod_2;
		to_json($result);
	}

	// start izza 
	public function coba(){
		$form["TANGGAL"] = '04/2021';
		if($form["TANGGAL"] == date('m/Y')){ $jml_hari = (int)date('d');}
		else { $jml_hari = $this->jml_hari((int)$blTh[0], $blTh[1]);}
		var_dump($jml_hari);
	}
	public function jml_hari($bln, $thn){
		$month = $bln-1;
		$isLeap = (($thn % 4) == 0 && (($thn % 100) != 0 || ($thn % 400) == 0));
		$hari = [31, ($isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][$month];
    // echo json_encode($hari);
		return $hari;

	}
	public function load_data_daily($jenis = 'CEMENT'){ // $jenis ada 2 CEMENT dan PRODUCTION
		$post = $this->input->post();
		//print_r($post);
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}
		// // --------- start dumy ---------
		// $tmp["ID_COMPANY"] = "11";
		// $tmp["ID_PLANT"] = "20";
		// $tmp["ID_GROUPAREA"] = "1";
		// $tmp["ID_AREA"] = "60";
		// // $tmp["TANGGAL"] = "11/2020";
		// $tmp["TANGGAL"] = "04/2021";
		// $tmp["ID_PRODUCT"] = "6";
		// // --------- end dumy ---------
		//Convert array to object
		$form = (object)$tmp;

		if($jenis == 'CEMENT'){
			//Load from T_cement_daily
			if ($form->ID_GROUPAREA==4) { $where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND FROM_HOURLY IS NULL";} // untuk kiln
			// if ($form->ID_GROUPAREA==4) { $where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'";} // untuk kiln
			else{ $where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND ID_PRODUCT='".$form->ID_PRODUCT."'";}
			$t_prod 	= $this->t_cement_daily->data_where($where);
		} elseif($jenis == 'PRODUCTION'){
			//Load from T_production_daily
			$where = "TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND a.ID_AREA='" .$form->ID_AREA."'";
			$t_prod 	= $this->t_production_daily->data_where($where);
		}

		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		
		$data 		= array();
		// ------------------------------- start izza 01.04.2021 -------------------------------
		// $ctHead		= count($colHeader['id_comp'])-2;
		// $test_array = array();
		// if (!empty($t_prod)) {
		// 	foreach ($t_prod as $key => $row) {
		// 		array_push($test_array, $row->ID_CEMENT_DAILY);
		// 		$d_prod = $this->d_cement_daily->get_by_id($row->ID_CEMENT_DAILY);
		// 		$tes_d_prod_1 = $this->d_cement_daily->tes($row->ID_CEMENT_DAILY,$test_array);
		// 		foreach ($d_prod as $k => $vl) {
		// 			if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
		// 				$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
		// 			}
		// 		}
		// 		if ($ctHead > count($d_prod)) {
		// 			for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
		// 				$data[$key][] = "";
		// 			}
		// 		}
		// 		$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
		// 		$data[$key][] = $row->MESIN_REMARK;
		// 	}
		// 		$tes_d_prod_2 = $this->d_cement_daily->tes2($test_array);
		// }

		// $result['header'] = $colHeader;
		// $result['data']   = $data;
		// $result['test1'] = $t_prod;
		// $result['test2'] = $tes_d_prod_1;
		// $result['test3'] = $tes_d_prod_2;

		// ------------------------------- BATAS -------------------------------
		$temp = array();
		$temp_d = array();
		$blTh = explode("/", $form->TANGGAL);

		if($form->TANGGAL == date('m/Y')){ $jml_hari = (int)date('d');}
		else { $jml_hari = $this->jml_hari((int)$blTh[0], $blTh[1]);}
		
		foreach ($colHeader['colHeader'] as $head) { $temp[$head] = null;}

		for ($i = 0; $i < (int)$jml_hari; $i++) { $data[] = $temp; }

		foreach ($t_prod as $key => $row) {
			$index = (int)$row->TANGGAL-1; // ini buat ganti nilai nya sesuai db

			if($jenis == 'CEMENT'){ $d_prod = $this->d_cement_daily->get_by_id($row->ID_DATA); }
			elseif($jenis == 'PRODUCTION'){ $d_prod = $this->d_production_daily->get_by_id($row->ID_DATA); }
			$temp_d[] = $d_prod;

			foreach ($d_prod as $k => $vl) {
				$KD_COMPONENT = strtoupper($vl->KD_COMPONENT);
				if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
					$data[$index][$KD_COMPONENT] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
				}
			}
			$data[$index]["MACHINE STATUS"] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
			$data[$index]["REMARK"] = $row->MESIN_REMARK;
		}

		$result['jenis'] = $jenis;
		$result['where'] = $where;
		$result['header'] = $colHeader;
		$result['data']   = $data;
		$result['t_prod'] = $t_prod;
		$result['d_prod'] = $temp_d;
		// ------------------------------- end izza 01.04.2021 -------------------------------
		to_json($result);
	}
	// end izza

	public function save_table(){
		$hanClean = array();
		$kolom 	  = 0;
		$rows 	  = array();
		$hanData  = $this->input->post('data');
		$formData = $this->input->post('formData');
		$id_comp  = $this->input->post('id_comp');
		$ct_cmp	  = count($id_comp);


		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];


		#var_dump($comp); exit;
		/*
		 *     *--->x
		 *     |
		 *     y
		 * */

		//clean form
		//$tmp = $form;

		foreach($form as $r){
			$tmp[$r['name']] = $r['value'];
		}

		$form = (object)$tmp;

		//T_PRODUCTION_DAILY (1): ID_PRODUCTION_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION_DAILY, ID_COMPONENT, NILAI

		//read line by line
		$tgl_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_date			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status", $comp);
			$i_remark		= array_search("_remark", $comp);

			#if(!$row[$i_date]) continue;

			//T
			$tdata['ID_AREA']					= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['DATE_DATA']				= str_pad($tgl_auto,2,'0',STR_PAD_LEFT)."/".$form->TANGGAL; # $row[$i_date] dd/mm/yyyy
			//$tdata['DATE_SET']			= date("d/m/Y"); //if entrydate exists then update_date else entry_date
			//$tdata['USER_SET']			= $this->USER->ID_USER; //if entryuser exists then update user else entry user
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$status_mesin 						= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save T
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}
			if($row[($t_kolom-2)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}

			if ($mati == $t_kolom) {
				// $tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}
			// print_r($tdata);
			$ID_PRODUCTION_DAILY = $this->t_production_daily->get_id($tdata[ID_AREA],$tdata[DATE_DATA]);

			if(!$ID_PRODUCTION_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_PRODUCTION_DAILY = $this->t_production_daily->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_production_daily->update($tdata,$ID_PRODUCTION_DAILY);
			}

			$tgl_auto++;

            $arrInputData = array();
			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_DAILY'] 	= $ID_PRODUCTION_DAILY;
				$ddata['ID_COMPONENT']			= $comp[$x];
				$ddata['NILAI']					= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']				= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_production_daily->d_exists($ddata)){
						// $this->t_production_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
					}
					else{
						$this->t_production_daily->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_production_daily->d_exists($ddata)){
                            array_push($arrInputData, $ddata);
							// $this->t_production_daily->d_insert($ddata);
						}
						else{
							$this->t_production_daily->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}
				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}

				if(!$this->t_production_daily->d_exists($ddata)){
					// $this->t_production_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
				}
				else{
					$this->t_production_daily->d_update($ddata);
					//echo $this->db->last_query() . "\n";
				}
			}
            
            if(count($arrInputData)>0){
                $this->t_production_daily->insert_production($arrInputData);
            }

			//exit;
			$ct_row++;

		}
        // HITUNG QAF
        $this->t_cement_daily->calculate_qaf_cs(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		$this->t_cement_daily->calculate_qaf_st(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		$this->t_cement_daily->calculate_qaf_cement(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		if($form->ID_GROUPAREA == 4){
			$this->t_cement_daily->calculate_qaf_clinker(intval($form->BULAN), $form->TAHUN,$form->ID_AREA);
		}
        

		to_json(array("result" => 'ok'));
	}

	public function save_table_cement(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		// print_r($data);
		// exit;
		foreach($form as $r){
			$tmp[$r['name']] = $r['value'];
		}

		$form = (object)$tmp;
		$bulan = explode("/", $form->TANGGAL);
		$form->BULAN = $bulan[0];
		$form->TAHUN = $bulan[1];

		//T_PRODUCTION_DAILY (1): ID_CEMENT_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_UPDATE,JAM_UPDATE,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_CEMENT_DAILY, ID_COMPONENT, NILAI

		//read line by line
		$tgl_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index
			//sub index
			$i_date	  		= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status", $comp);
			$i_remark 		= array_search("_remark", $comp);

			#if(!$row[$i_date]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= str_pad($tgl_auto,2,'0',STR_PAD_LEFT)."/".$form->TANGGAL; # $row[$i_date] dd/mm/yyyy
			$status_mesin 				= strtoupper($row[$i_mesin_status]);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if($row[($t_kolom-2)] == 'OFF'){
				$tdata['ID_MESIN_STATUS'] = 3;
			}else{
				if ($mati == $t_kolom) {
					$tdata['ID_MESIN_STATUS'] = 0;
				}
			}
			if ($mati == $t_kolom) {

				//BEFORE
				// $tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			$ID_CEMENT_DAILY = $this->t_cement_daily->get_id($tdata[ID_AREA],$tdata[ID_PRODUCT],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			// $ID_CEMENT_DAILY = $this->t_cement_daily->get_id($tdata['ID_AREA'],$tdata['ID_PRODUCT'],$tdata['DATE_DATA'],$tdata['JAM_DATA']); // izza 30.03.2021
			//echo $this->db->last_query() . "\n\n";
			if(!$ID_CEMENT_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);

			}

			#echo $this->db->last_query()."\n\n";
            $arrInputData = array();
			//D
			for($x=0;$x<$i_mesin_status;$x++){


				// echo '<pre>';
				// var_dump($row[$x]);
				// echo '</pre>';


				$ddata = null;
				$ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
				$ddata['ID_COMPONENT']		= $comp[$x];
				$ddata['NILAI']				= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']			= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_cement_daily->d_exists($ddata)){
						// $this->t_cement_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
						$tes_tes_update = $this->t_cement_daily->tes_update($ddata);
					}
					continue;
				}
					$result['tes_tes_update'] = $tes_tes_update;

				/* Check IF CS */
				$str = strtoupper($this->m_component->get_data_by_id($ddata['ID_COMPONENT'])->KD_COMPONENT);
				if (substr_compare($str, 'CS', 0, 2)===0) {
					$ddata['NILAI']	= ($row[$x]=='') ? '':trim($row[$x]);
					if(!$this->t_cement_daily->d_exists($ddata)){
						// $this->t_cement_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
					}
					else{
						$this->t_cement_daily->d_update($ddata);
					} 
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_cement_daily->d_exists($ddata)){
							// $this->t_cement_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
						}
						else{
							$this->t_cement_daily->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}

				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}



				if(!$this->t_cement_daily->d_exists($ddata)){
					// $this->t_cement_daily->d_insert($ddata);
                            array_push($arrInputData, $ddata);
					#echo $this->db->last_query();die();
				}
				else{
					$this->t_cement_daily->d_update($ddata);
				}
				#echo $this->db->last_query().";".PHP_EOL;
			}

            if(count($arrInputData)>0){
                $this->t_cement_daily->insert_dcement($arrInputData);
            }
			$tgl_auto++;
			$ct_row++;

		}
        
        // HITUNG QAF
        $this->t_cement_daily->calculate_qaf_cs(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		$this->t_cement_daily->calculate_qaf_st(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		$this->t_cement_daily->calculate_qaf_cement(intval($form->BULAN), $form->TAHUN,$form->ID_COMPANY);
		if($form->ID_GROUPAREA == 4){
			$this->t_cement_daily->calculate_qaf_clinker(intval($form->BULAN), $form->TAHUN,$form->ID_AREA);
		}
        
		// to_json(array("result" => 'ok'));
		to_json($result);
				// exit;
	}

	private function str_clean($chr=''){
		$str ='([^.0-9-]+)';
		return preg_replace($str, '', $chr);
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color 				= getFixColor($i);
			$nilai 				= array_filter($trace[$i], 'is_numeric');

			if (empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= trim($comp[$i]->title);
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;


			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}

}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
