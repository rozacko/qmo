<?php

class Acclab_ujisampleblindtest extends QMUser {	
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_blindtest");
		$this->load->model("m_company");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$this->list_component = $this->m_blindtest->list_component();
		$mcomp = array();
		foreach ($this->m_blindtest->componentmaster() as $key => $value) {
			# code...
			$mcomp[] = $value['NM_COMPONENT'];
		}
		$this->m_component = $mcomp;
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);	
		$this->template->adminlte("v_ujisampleblindtest", $data);
	}

	public function blindtest_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
		$sesi_user = $this->session->userdata();
		$user_in = json_decode(json_encode($sesi_user['USER']), true); 
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

  		if ($user_in["ID_USERGROUP"] && (int) $user_in["ID_USERGROUP"] == 1) {
  			# code...
  		} else {
  			# code...
  			$key['PIC_OBSERVASI'] = $user_in["ID_USER"];
  			$key['PIC_PLANT'] = $user_in["ID_USER"];
  		}

      	$data	= $this->m_blindtest->get_receipt($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_blindtest->recFil_receipt($key),
  			'recordsTotal'		=>	$this->m_blindtest->recTot_receipt($key)
  		);

  		echo json_encode($return);
    }

    public function send_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Send Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENGIRIM'] = $post['user'];
		$data['CREATE_BY'] = $post['user'];

		$isertd_sample = $this->m_blindtest->sample_send($data);
		if ($isertd_sample) {
			// code...
			$result['msg'] = 'Send Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

    public function receipt_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Receipt Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['FK_ID_PERIODE'];
		$data['PENERIMA'] = $post['user'];
		$data['STATUS_ENTRY'] = 1;
		$data['UPDATE_BY'] = $post['user'];
		// print_r($data);
		// exit();
		$updted_sample = $this->m_blindtest->sample_receipt($data);
		// echo $this->db->last_query();
		// exit();
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Receipt Sample Blind Test Success ...';
			$result['status'] = true;
		
			$param['ID'] = $post['FK_ID_PERIODE'];
			$param['STATUS'] = 'TERIMA SAMPLE';
			$param['UPDATE_BY'] = $post['user'];
			// print_r($param);
			$isertd_sample = $this->m_blindtest->update_status($param);
			// echo $this->db->last_query(); 
			// exit();
		}
		
		to_json($result);
    }

	public function get_dataformcomponent($idsetup){		
		$datablindtest = array();
		$sample_blindtest= $this->m_blindtest->compdatasampleblindtest($idsetup);
		// echo $this->db->last_query();
		// exit();
		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$tdatablindtest[0] = $value['NM_COMPONENT'];
			$tdatablindtest[1] = empty($value['TESTING_1']) ? '' : (double) $value['TESTING_1'];
			$tdatablindtest[2] = empty($value['TESTING_2']) ? '' : (double) $value['TESTING_2'];
			$tdatablindtest[3] = $value['SATUAN'];
			$datablindtest[] = $tdatablindtest;			
		}
		to_json($datablindtest);
	}

    public function submit_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Submit Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['FK_ID_PERIODE'] = $post['ID'];
		$data['STATUS_ENTRY'] = 2;
		$data['UPDATE_BY'] = $post['user'];

		$updted_sample = $this->m_blindtest->sample_submit($data);
		if ($updted_sample) {
			// code...
			$result['msg'] = 'Submit Sample Blind Test Success ...';
			$result['status'] = true;

			$param['ID'] = $post['ID'];
			$param['STATUS'] = 'ENTRY BLIND TEST';
			$param['UPDATE_BY'] = $post['user'];
			$isertd_sample = $this->m_blindtest->update_status($param);
		}
		
		to_json($result);
    }

    public function delete_blindtest() {
    	# code...
    	$result['msg'] = 'Cannot Delete Sample Blind Test ...';
		$result['status'] = false; 

		$post = $this->input->post();
		$data['ID'] = $post['ID'];

		$delete_sample = $this->m_blindtest->blind_delete($post['ID']);
		if ($delete_sample) {
			// code...
			$result['msg'] = 'Delete Sample Blind Test Success ...';
			$result['status'] = true;
		}
		
		to_json($result);
    }

	public function ajax_get_type_product($all = null){
		$sample_area= $this->m_blindtest->type_productlist($all);
		foreach ($sample_area as $key => $value) {
			$sample_area[$key]['KD_PRODUCT'] = preg_replace('/\s+/', '', $value['KD_PRODUCT']);
		}
		to_json($sample_area);
	}

	public function ajax_get_blindtest($idsetup, $action = 'true'){
		$datablindtest = array();
		$display = '';
		if ($action == 'false') {
			# code...
			$display = ' style="display : none;" ';
		}
		$sample_blindtest= $this->m_blindtest->sampleblindtest($idsetup);
		foreach ($sample_blindtest as $key => $value) {
			$tdatablindtest = array();
			$tdatablindtest[0] = $value['NM_COMPONENT'];
			$tdatablindtest[1] = (double) $value['TESTING_1'];
			$tdatablindtest[2] = (double) $value['TESTING_2'];
			$tdatablindtest[3] = '<button title="Delete" class="btDeleteBlind btn btn-danger btn-xs delete" '.$display.' type="button" onclick="deletesample('.$value['ID'].')"><i class="fa fa-trash-o"></i> Delete</button>';
			$datablindtest[] = $tdatablindtest;			
		}
		// to_json($datablindtest);
		$return	= array(
  			'data'				=>	$datablindtest,
  		);

  		echo json_encode($return);
	}

	public function ajax_get_component_display(){
			$componentlist = $this->m_blindtest->component_checklist_order();
			to_json($componentlist);
	}

	public function get_component(){
		$sample_area= $this->m_blindtest->componentlist();
		to_json($sample_area);
	}

	public function save_sample_data_blindtest() {
		# code...
		$componenmaster = $this->m_blindtest->componentmaster();

		$componentlist = array();

		foreach ($componenmaster as $key => $value) {
			# code...
			$componentlist[$value['NM_COMPONENT']] = $value['ID_COMPONENT'];
		}

		$post = $this->input->post();
		$databindtest = $post['data'];
		foreach ($databindtest as $key => $value) {
			# code...
			$param = array();
			$param['FK_ID_SETUP'] = (int) $post['id_setup'];
			$param['FK_ID_COMPONENT'] = empty($componentlist[$value[0]]) ? 0 : (int) $componentlist[$value[0]];
			$param['TESTING_1'] = empty($value[1]) ? 0 : (float) $value[1];
			$param['TESTING_2'] = empty($value[2]) ? 0 : (float) $value[2];
			$param['AVR'] = ($param['TESTING_1'] + $param['TESTING_2']) / 2;
			$param['USER'] = $post['user'];

			$inp_sample = $this->m_blindtest->input_blindtest($param);

		}

		$result['msg'] = 'success';
		$result['status'] = true;

		to_json($result);
	}

}	

?>
