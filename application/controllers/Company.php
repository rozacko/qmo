<?php

class Company extends QMUser {
	
	public $list_data = array();
	public $data_company;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_company");
	}
	
	public function add(){
		$this->template->adminlte("v_company_add");
	}
	
	public function create(){
		
		$this->load->library('form_validation');
		$this->load->helper('url');

		$slug = "company-img_".url_title($this->input->post('KD_COMPANY')."_".$this->input->post('NM_COMPANY'));

		$folder = "./assets/uploads/profile/";
		$path = $_FILES['LOGO']['name'];
		$ext = pathinfo($path, PATHINFO_EXTENSION);

		$file_path = $folder.$slug.".".$ext;
		move_uploaded_file($_FILES['LOGO']['tmp_name'], $file_path);
		$path = "/assets/uploads/profile/".$slug.".".$ext;
		
		$this->m_company->insert_in($path);
		if($this->m_company->error()){
			$this->notice->error($this->m_company->error());
			redirect("company/add");
		}
		else{
			$this->notice->success("Company Data Saved.");
			redirect("company");
		}
	}
	
	public function edit($ID_COMPANY){
		$this->data_company = $this->m_company->get_data_by_id($ID_COMPANY);
		$this->template->adminlte("v_company_edit");
	}
	
	public function update($ID_COMPANY){
		$this->m_company->update($this->input->post(),$ID_COMPANY);
		if($this->m_company->error()){
			$this->notice->error($this->m_company->error());
			redirect("company/edit/".$ID_COMPANY);
		}
		else{
			$this->notice->success("Company Data Updated.");
			redirect("company");
		}
	}
	
	public function update_in($ID_COMPANY){ 
		if(empty($_POST["LOGO"])){
			unlink(".".$this->input->post('path'));
			$slug = "company-img_".url_title($this->input->post('KD_COMPANY')."_".$this->input->post('NM_COMPANY'));
			$folder = "./assets/uploads/profile/";
			$path = $_FILES['LOGO']['name'];
			$ext = pathinfo($path, PATHINFO_EXTENSION);
			$file_path = $folder.$slug.".".$ext;
			move_uploaded_file($_FILES['LOGO']['tmp_name'], $file_path);
			$path = "/assets/uploads/profile/".$slug.".".$ext;
		} else {
			$path = "404";
		}
		
		$this->m_company->update_in($ID_COMPANY, $path);
		if($this->m_company->error()){
			$this->notice->error($this->m_company->error());
			redirect("company/edit/".$ID_COMPANY);
		}
		else{
			$this->notice->success("Company Data Updated.");
			redirect("company");
		}
	}
	
	public function delete($ID_COMPANY){
		$company = $this->m_company->get_data_by_id($ID_COMPANY);
		unlink(".".$company->LOGO);
		
		$this->m_company->delete($ID_COMPANY);
		if($this->m_company->error()){
			$this->notice->error($this->m_company->error());
		}
		else{
			$this->notice->success("Company Data Removed.");
		}
		redirect("company");
	}

	public function get_list(){
		$list = $this->m_company->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			
			$row[] = $column->ID_COMPANY;
			$row[] = $no;
			$row[] = "<img src='".site_url($column->LOGO)."' class='img-responsive' >";
			$row[] = $column->KD_COMPANY;
			$row[] = $column->NM_COMPANY;
			$row[] = $column->URUTAN;
			$row[] = "<center>".($column->USE_DETAIL_PLANT == "Y" ? "<i class='fa fa-check'></i>" : "<i class='fa fa-close'></i>")."<center>";
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_company->count_all(),
            "recordsFiltered" => $this->m_company->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
