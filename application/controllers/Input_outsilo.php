<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_outsilo extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("c_product");
		$this->load->model("m_product");
		$this->load->model("t_production_outsilo");
		$this->load->model("d_production_outsilo");
		$this->load->model("qaf_daily");				
	}
	
	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_outsilo", $data);
	}

	// public function ajax_get_area_cement($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
	// 	$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
	// 	//var_dump($area);exit;
	// 	to_json($area);
	// }

	// public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
	// 	$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA, TRUE);#echo $this->m_area->get_sql();
	// 	to_json($area);
	// }
	
	// public function ajax_get_plant($ID_COMPANY=NULL){
	// 	$plant= $this->m_plant->datalist($ID_COMPANY);
	// 	to_json($plant);
	// }

	public function ajax_get_areaproduct($id_company=''){
		//HandsonTable Column Header
		//$prod = $this->m_area->datalistOutsilo( $id_company);
		$area_silo = $this->get_area_nm($id_company);		
		//$no=1;
		foreach ($area_silo as $val):
			$area[] = $val;	
			// $no++;	
		endforeach;
		

		$header[]['title'] = 'AREA';
		$header[]['title'] = 'CODE';
		$prod_area = $this->get_product_nm($id_company);
		//$no =2;
		foreach ($prod_area as $val):
			$header[]['title']= $val[KD_PRODUCT];		
			//$no++;	
		endforeach;

		//$header[]['title'] = 'PRODUCT TYPE';		
		// $header[]['title'] = 'PRODUCTION';
		$data['colHeader'] = $header;
		$data['rows'] 	   = count($area_silo);
		$data['area']   = $area;
		
		to_json($data);
	}

	public function ajax_get_product($id_company=''){
		//HandsonTable Column Header
		//$prod = $this->m_area->datalistOutsilo( $id_company);
		/*
		$area_silo = $this->get_area_nm($id_company);		
		//$no=1;
		foreach ($area_silo as $val):
			$area[] = $val;	
			// $no++;	
		endforeach;
		*/
		
		$prod_area = $this->get_product_nm($id_company);
		//$no =2;
		foreach ($prod_area as $val):
			//$idproduk[]= $val[ID_PRODUCT];
			$kdproduk[]= $val[KD_PRODUCT];	
			//$no++;	
		endforeach;
		
		$header[]['title'] = 'PRODUCT TYPE';
		//$header[]['title'] = 'Code Product';
		$header[]['title'] = 'RELEASE OUTSILO';	
		$data['colHeader'] = $header;
		$data['rows'] 	   = count($prod_area);
		$data['product']   = $kdproduk;
		//$data['kdproduct']   = $produk;
		to_json($data);
	}

	public function get_area_nm($id_company=''){
		$data = array();
		$param = $this->m_area->datalistOutsilo( $id_company);
		$n = 0;
		foreach ($param as $col):
			$data[$n][ID_AREA] = $col->ID_AREA;
			$data[$n][NM_AREA] = $col->NM_AREA;
			$data[$n][KD_AREA] = $col->KD_AREA;
			$n++;
		endforeach;
		
		return $data;
	}

	public function get_product_nm($id_company=''){
		$data = array();
		$param = $this->m_area->datalistProdukOutsilo($id_company);
		$n = 0;
		foreach ($param as $col):
			$data[$n][ID_PRODUCT] = $col->ID_PRODUCT;
			$data[$n][KD_PRODUCT] = $col->KD_PRODUCT;
			$data[$n][NM_PRODUCT] = $col->NM_PRODUCT;
			$n++;
		endforeach;
		
		return $data;
	}

	// public function get_product_id($id_company='', $id_plant='', $id_area=''){
	// 	$data = array();
	// 	$param = $this->c_product->datalist($id_area, $id_plant, $id_company);

	// 	foreach ($param as $col):
	// 		$data[] = $col->ID_PRODUCT;
	// 	endforeach;
		
	// 	return $data;
	// }

	public function load_table_sum(){
		error_reporting(E_ALL);
		$data = array();
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];

		//Load from T_production_outsilo
		$data = array();
		$t_prod = $this->t_production_outsilo->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."' 
		AND ID_COMPANY='" .$form->ID_COMPANY."' AND FLAG_SUM=1 ");
		//echo $this->db->last_query();die();
		
		if (empty($t_prod)) {
			$prod = $this->get_product_nm($form->ID_COMPANY);
			if(!$prod) $prod[0] = "-";
			$id = 1;
			foreach ($prod as $val):
				//$val->NILAI = NULL;		
				$data[]= array($val[KD_PRODUCT],null);
				// $data[$id]->KD_AREA = $val->KD_AREA;		
				// $data[$id]->NILAI = Null;
				$id++;
			endforeach;
		}else{
			$prod = $this->get_product_nm($form->ID_COMPANY); 
			
			//if($form->ID_GROUPAREA == 4) $prod = "(NO TYPE)";
			if(!$prod) $prod[0] = "-";
			$id_tprod = $t_prod[0]->ID_PRODUCTION_OUTSILO;
			$id = 1;
			foreach ($prod as $val):				
				$nilai  = $this->d_production_outsilo->get_nilai($id_tprod, $id, 1,null,$val[ID_PRODUCT]);				
				$data[] = array($val[KD_PRODUCT],$nilai->NILAI);
				$id++;
			endforeach;
		}
		to_json($data);
	}
	
	public function load_table(){
		error_reporting(E_ALL);
		$data = array();
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];

		//Load from T_production
		$data = array();
		$t_prod = $this->t_production_outsilo->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."'
		 AND ID_COMPANY='" .$form->ID_COMPANY."' AND FLAG_SUM=0 ");
		//echo $this->db->last_query();die();

		$area = $this->get_area_nm($form->ID_COMPANY); 
		$prod = $this->get_product_nm($form->ID_COMPANY); 
		$reseult = $this->qaf_daily->get_area_by_group_outsilo(null, 81, $form->ID_COMPANY, null); 
		$produkarea = array();
		//mencari produk area
		
		foreach ($reseult as $pv):
			$produkarea[$pv->ID_AREA.$pv->ID_PRODUCT] = "available";
		endforeach;
		//print_r($prod);
		
		if (empty($t_prod)) {			
			if(!$area) $area[0] = "-";
			
			foreach ($area as $val):				
				//$data[]= array($val[NM_AREA],$val[KD_AREA]);
				$ar_tmp = array();	
				$ar_tmp[] = $val[NM_AREA];
				$ar_tmp[] = $val[KD_AREA];
				foreach ($prod as $pv):
					//echo $val[ID_AREA].$pv[ID_PRODUCT];
					if($produkarea[$val[ID_AREA].$pv[ID_PRODUCT]] == 'available')
					{
						//$nilai  = $this->d_production_outsilo->get_nilai($id_tprod, $id, 0,$val[ID_AREA],$pv->ID_PRODUCT);
						$ar_tmp[] = null;
					}else{
						$ar_tmp[] = 'None';
					}				
				endforeach;

				$data[] = $ar_tmp;
			endforeach;
		}else{
			//if($form->ID_GROUPAREA == 4) $prod = "(NO TYPE)";
			if(!$area) $area[0] = "-";
			$id_tprod = $t_prod[0]->ID_PRODUCTION_OUTSILO;
			$id = 1;
			foreach ($area as $val):
				$ar_tmp = array();	
				$ar_tmp[] = $val[NM_AREA];
				$ar_tmp[] = $val[KD_AREA];
				foreach ($prod as $pv):
					if($produkarea[$val[ID_AREA].$pv[ID_PRODUCT]] == 'available')
					{
						$nilai  = $this->d_production_outsilo->get_nilai($id_tprod, $id, 0,$val[ID_AREA],$pv[ID_PRODUCT]);
						$ar_tmp[] = $nilai->NILAI;
					}else{
						$ar_tmp[] = 'None';
					}				
				endforeach;

				// $nilai  = $this->d_production_outsilo->get_nilai($id_tprod, $id, 0);				
				// $data[] = array($val[NM_AREA],$val[KD_AREA],$nilai->NILAI);
				$data[] = $ar_tmp;
				$id++;
			endforeach;
		}
		// if (!empty($t_prod)) {
		// 	$prod = $this->get_product_nm($form->ID_COMPANY); 
		// 	if(!$prod) $prod[0] = "-";
		// 	$id_tprod = $t_prod[0]->ID_PRODUCTION_OUTSILO;
		// 	$id = 1;
		// 	foreach ($prod as $val):
		// 		$nilai  = $this->d_production_outsilo->get_nilai($id_tprod, $id);
		// 		$data[] = array($val,$nilai);
		// 		$id++;
		// 	endforeach;
		// }

		to_json($data);
	}

	public function save_table(){
		$post  = $this->input->post();
		$form  = $post['formData'];
		$data  = $post['data'];
		$headertbl  = $post['headerlist'];
		
		$flag_sum  = $post['jenis'];
		
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];
			
		//T_PRODUCTION_DAILY (1): ID_PRODUCTION,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION, ID_COMPONENT, NILAI
		$tdata['BULAN']	= $form->BULAN;
		$tdata['TAHUN']	= $form->TAHUN;
		$tdata['ID_COMPANY']	= $form->ID_COMPANY;
		//$tdata['KD_AREA']	= $form->KD_AREA;


		
		$ID_PRODUCTION  = $this->t_production_outsilo->get_id($tdata['BULAN'],$tdata['TAHUN'],$form->ID_COMPANY,$flag_sum);

		if(!$ID_PRODUCTION){
			$tdata['DATE_ENTRY'] = date("d/m/Y");
			$tdata['USER_ENTRY'] = $this->USER->ID_USER;
			$tdata['FLAG_SUM'] = $flag_sum;
			//$tdata['KD_AREA']	= $form->ID_AREA;
			$ID_PRODUCTION = $this->t_production_outsilo->insert($tdata);
		}
		else{
			$tdata['DATE_UPDATE'] = date("d/m/Y");
			$tdata['USER_UPDATE'] = $this->USER->ID_USER;
			//$tdata['KD_AREA']	= $form->ID_AREA;
			$this->t_production_outsilo->update($ID_PRODUCTION,$tdata);
		}

		if($flag_sum == 1){
			//read line by line
			$ctr = 1;
			foreach($data as $y => $row){ //y index	
							
				$ddata = null;
				$ddata['ID_PRODUCTION_OUTSILO'] = $ID_PRODUCTION;
				$ddata['ID_COMPANY'] = $form->ID_COMPANY;		
				//$idarea = $this->m_area->getidbykode(trim($row[1]));
				//print_r($idarea);
				//$ddata['ID_AREA']		= $idarea[0]->ID_AREA;
				$ddata['ID_PRODUCT']	= $this->m_product->get_id_by_kode(trim($row[0]));
				$ddata['KD_PRODUCT']	= trim($row[0]);
				$ddata['NILAI']			= ((float)$row[1] == 0) ?  null : (float)$row[1];
				$ddata['NO_FIELD']		= $ctr;
				$ddata['FLAG_SUM']		= 1;
				#save 
				//print_r($ddata);
				if(!$this->d_production_outsilo->exists_sum($ddata)){
					$this->d_production_outsilo->insert($ddata);
					//echo $this->db->last_query() . "\n";
				}
				else{
					$where['ID_PRODUCTION_OUTSILO'] = $ID_PRODUCTION;
					$where['ID_COMPANY']    = $form->ID_COMPANY;
					$where['ID_PRODUCT']		= $ddata['ID_PRODUCT'];
					#echo $this->db->last_query() . "\n";
					$this->d_production_outsilo->update_where($where,$ddata);
					#echo $this->db->last_query() . "\n";
				}
				
				$ctr++;
			}
		}else{ //simpan per area per product
			$ctr = 1;
			foreach($data as $y => $row){ //y index	
							
				$ddata = null;
				$ddata['ID_PRODUCTION_OUTSILO'] = $ID_PRODUCTION;
				$ddata['ID_COMPANY'] = $form->ID_COMPANY;		
				$idarea = $this->m_area->getidbykode(trim($row[1]));				
				$ddata['ID_AREA']		= $idarea[0]->ID_AREA;
				$ddata['KD_AREA']		= trim($row[1]);
				$ddata['FLAG_SUM']		= 0;
				$ddata['NO_FIELD']		= $ctr;

				for($i =2; $i<count($headertbl); $i++){
					$ddata['ID_PRODUCT']	= $this->m_product->get_id_by_kode(trim($headertbl[$i]));
					//echo $this->m_product->get_sql();

					$ddata['KD_PRODUCT']	= trim($headertbl[$i]);
					$ddata['NILAI']			= ((float)$row[$i] == 0) ?  null : (float)$row[$i];

					//print_r($ddata);
					//save satu persatu
					if(!$this->d_production_outsilo->exists($ddata)){
						$this->d_production_outsilo->insert($ddata);
						//echo $this->db->last_query() . "\n";
					}
					else{
						$where['ID_PRODUCTION_OUTSILO'] = $ID_PRODUCTION;
						$where['ID_COMPANY']    = $form->ID_COMPANY;
						$where['ID_PRODUCT']	= $ddata['ID_PRODUCT'];
						$where['ID_AREA']		= $ddata['ID_AREA'];
						$where['FLAG_SUM']		= $ddata['FLAG_SUM'];
						#echo $this->db->last_query() . "\n";
						$this->d_production_outsilo->update_where($where,$ddata);
						#echo $this->db->last_query() . "\n";
					}
				}								
				
								
				$ctr++;
			}
		}
		
		to_json(array("result" => 'ok'));
	}
}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
