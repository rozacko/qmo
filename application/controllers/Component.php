<?php

class Component extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
	}
	
	public function index(){
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_component");
	}
	
	public function add(){
		$this->template->adminlte("v_component_add");
	}
	
	public function create(){
		$this->m_component->insert($this->input->post());
		if($this->m_component->error()){
			$this->notice->error($this->m_component->error());
			redirect("component/add");
		}
		else{
			$this->notice->success("Component Data Saved.");
			redirect("component");
		}
	}
	
	public function edit($ID_COMPONENT){
		$this->data_component = $this->m_component->get_data_by_id($ID_COMPONENT);
		$this->template->adminlte("v_component_edit");
	}
	
	public function update($ID_COMPONENT){
		$this->m_component->update($this->input->post(),$ID_COMPONENT);
		if($this->m_component->error()){
			$this->notice->error($this->m_component->error());
			redirect("component/edit/".$ID_COMPONENT);
		}
		else{
			$this->notice->success("Component Data Updated.");
			redirect("component");
		}
	}
	
	public function delete($ID_COMPONENT){
		$this->m_component->delete($ID_COMPONENT);
		if($this->m_component->error()){
			$this->notice->error($this->m_component->error());
		}
		else{
			$this->notice->success("Component Data Removed.");
		}
		redirect("component");
	}

	public function get_list(){
		$list = $this->m_component->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_COMPONENT;
			$row[] = $no;
			$row[] = $column->KD_COMPONENT;
			$row[] = $column->NM_COMPONENT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_component->count_all(),
            "recordsFiltered" => $this->m_component->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
