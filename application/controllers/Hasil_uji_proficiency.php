<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hasil_uji_proficiency extends QMUser {

    public $proficiency = array();
    public $list_pertanyaan = array();
    public $list_pertanyaan_fisika = array();
    public $list_pertanyaan_kimia = array();
    public $list_lab = array();
    public $header = array();
    public $list_hasil_uji = array();

	public function __construct(){
		parent::__construct();
        $this->load->helper("string");
        $this->load->model("M_hasil_uji_proficiency");
        $this->load->model("M_sampling");
        $this->load->model("M_evaluasi_proficiency");
    }

    public function index(){
        $this->template->adminlte("v_hasil_uji_proficiency");
    }

    public function add(){
        $this->libExternal('select2');

        $komoditi = $this->input->get("komoditi");
        $proficiency = $this->input->get("proficiency");
        $penerima = $this->input->get("penerima");
        $analis = $this->input->get("analis");

        $this->proficiency = $this->M_sampling->detail($proficiency);
        if($komoditi == 52 || $komoditi == 51){
            $this->list_pertanyaan_fisika = $this->M_hasil_uji_proficiency->get_list_pertanyaan($komoditi, 'FISIKA');
            $this->list_pertanyaan_kimia = $this->M_hasil_uji_proficiency->get_list_pertanyaan($komoditi, 'KIMIA');
        } else {
            $this->list_pertanyaan = $this->M_hasil_uji_proficiency->get_list_pertanyaan($komoditi, '', $analis);
        }

        // IF HASIL UJI DISAVE AS DRAFT
        if($analis != ""){
            $this->dtl_hasil_uji = $this->M_hasil_uji_proficiency->get_dtl_hasil_uji_analis($analis);
        }
        
        $this->list_lab = $this->M_hasil_uji_proficiency->get_list_lab();
        $this->list_approver = $this->M_sampling->data_pic();
        $this->template->adminlte("v_hasil_uji_proficiency_add");
    }

    public function view(){
        $user_id = $this->session->userdata("USER")->ID_USER;
        $user_role = $this->session->userdata("ROLE");
        $role = $this->recursive_array_search("ADMINISTRATOR", $user_role);
        $akses = "none";
        if($role == true){
            $akses = "all";
        } else {
            $role = $this->recursive_array_search("Opco (Peserta)", $user_role);
            if($role == true){
                $akses = "peserta";
            }
        }

        $komoditi = $this->input->get("komoditi");
        $proficiency = $this->input->get("proficiency");

        $this->kd_komoditi = $this->M_evaluasi_proficiency->get_kode_komoditi($komoditi);
        $this->proficiency = $this->M_sampling->detail($proficiency);
        $this->header = $this->M_hasil_uji_proficiency->get_list_pertanyaan($komoditi);
        $hasil_uji = $this->M_hasil_uji_proficiency->get_hasil_uji($proficiency, $akses, $user_id);
        $data = array();
        if(count($hasil_uji) > 0){
            foreach($hasil_uji as $hu){
                $data["HASIL_UJI"] = $hu;
                $data["DETAIL"] = array();
    
                $DTL_HASIL_UJI = $this->M_hasil_uji_proficiency->get_dtl_hasil_uji($hu['ID_HASIL_UJI']);
                array_push( $data["DETAIL"], $DTL_HASIL_UJI);
    
                $respons[] = $data;
            }
        } else {
            $respons = array();
        }
        
        $this->list_hasil_uji = $respons;
        // echo "<pre>";
        // print_r($this->list_hasil_uji);exit();

        
        $penerima = $this->M_sampling->list_penerima($proficiency, $akses, $user_id);
        $dt_penerima = array();
        foreach($penerima as $pp){
            $dt_penerima["PENERIMA"] = $pp;
            $dt_penerima["DETAIL"] = array();

            $dtl_penerima = $this->M_hasil_uji_proficiency->get_dtl_hasil_uji_penerima($pp['ID_PENERIMA']);
            array_push($dt_penerima["DETAIL"], $dtl_penerima);

            $response_penerima[] = $dt_penerima;
        }
        $this->penerima = $response_penerima;
        $this->template->adminlte("v_hasil_uji_proficiency_data");
    }

    public function recursive_array_search($category, $allProjects) {
        foreach ($allProjects as $projectName => $categories) {
            $categoryIndex = array_search($category, $categories);
            if ($categoryIndex !== false) {
                return true;
            }
        }
        return false;
    }

    public function get_list(){
        $list = $this->M_hasil_uji_proficiency->get_list();
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["ID_PP"] = $column->ID_PP;
			$row["TITLE_PP"] = $column->TITLE_PP;
			$row["YEAR_PP"] = $column->YEAR_PP;
			$row["ID_PROFICIENCY"] = $column->ID_PROFICIENCY;
			$row["ID_KOMODITI"] = $column->ID_KOMODITI;
			$row["NAMA_SAMPLE"] = $column->NAMA_SAMPLE;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_hasil_uji_proficiency->count_all(),
            "recordsFiltered" => $this->M_hasil_uji_proficiency->count_filtered(),
            "data" => $data,
        );

		to_json($output);
    }

    public function action_input($ID_PROFICIENCY, $ID_PENERIMA){

        $ID_LABORATORIUM = $this->input->post("ID_LABORATORIUM");
        $ANALIS = $this->input->post("ANALIS");
        $QUESTION = $this->input->post("QUESTION");
        $QUEST_TYPE = $this->input->post("QUEST_TYPE");
        $ANSWER = $this->input->post("ANSWER");
        $STATUS = $this->input->post("STATUS");
        $APPROVER = $this->input->post("APPROVER");
        $APPROVE_VALUE = $this->input->post("APPROVE_VALUE");
        $APPROVE_NOTE = $this->input->post("APPROVE_NOTE");

        $ID_HASIL_UJI = $this->input->post("ID_HASIL_UJI");
        $ID_DTL_HASIL_UJI = $this->input->post("ID_DTL_HASIL_UJI");

        if($APPROVE_VALUE == "Y"){
            $STATUS = "APPROVED";
        } else if($APPROVE_VALUE == "N"){
            $STATUS = "REJECTED";
        }

        $data = array(
            "ID_PROFICIENCY" => $ID_PROFICIENCY,
            "ID_LABORATORIUM" => $ID_LABORATORIUM,
            "ID_PENERIMA_SAMPLE" => $ID_PENERIMA,
            "ANALIS" => $ANALIS,
            "STATUS" => $STATUS,
            "APPROVE_NOTE" => $APPROVE_NOTE,
        );

        if($APPROVER != ""){
            $data["APPROVER"] = $APPROVER;
        }

        if($APPROVE_VALUE == "Y" || $APPROVE_VALUE == "N"){
            $data["APPROVE_AT"] = date("Y-m-d H:i:s");
            $data["APPROVE_VALUE"] = $APPROVE_VALUE;
        }

        if($STATUS == "WAITING APPROVAL"){
            $proficiency = $this->M_sampling->detail($ID_PROFICIENCY);
            $get_approver = $this->M_sampling->data_pic($APPROVER);
            $title = $proficiency->TITLE_PP." ".$proficiency->YEAR_PP;
            $this->send_email($title, $get_approver[0]->EMAIL, $get_approver[0]->FULLNAME);
        }

        if($ID_HASIL_UJI == ""){
            $act = $this->M_hasil_uji_proficiency->add_hasil_uji($data);
        } else {
            $act = $this->M_hasil_uji_proficiency->update_hasil_uji($data, $ID_HASIL_UJI);
            if($APPROVE_VALUE != ""){ //JIKA DILAKUKAN APPROVER / REJECT MAKA PROSES SIMPAN DTL HASIL UJI TIDAK DILAKUKAN
                $status = true;
                echo $status;
                exit();
            }
        }
        

        $i = 0;
        $f = 0;
        $data_dtl = array();

        $config['upload_path']= './assets/uploads/proficiency/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 2048;
        foreach($QUESTION as $q){
            if($QUEST_TYPE[$i] == "FILE"){
                $_FILES['file']['name'] = $_FILES['ANSWER']['name'][$f];
                $_FILES['file']['type'] = $_FILES['ANSWER']['type'][$f];
                $_FILES['file']['tmp_name'] = $_FILES['ANSWER']['tmp_name'][$f];
                $_FILES['file']['error'] = $_FILES['ANSWER']['error'][$f];
                $_FILES['file']['size'] = $_FILES['ANSWER']['size'][$f];
                $f++;

                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')){
                    $FILE_ANSWER = $this->upload->data();
                    
                    // JIKA UPLOAD FILE
                    if($ID_HASIL_UJI == ""){ // JIKA ADD
                        $dtl = array(
                            "ID_HASIL_UJI" => $act->MAX_ID,
                            "ID_QUESTION" => $QUESTION[$i],
                            "ANSWER" => $FILE_ANSWER["file_name"]
                        );
                    } else { // JIKA UPDATE
                        $dtl = array(
                            "ID_HASIL_UJI" => (int)$ID_HASIL_UJI,
                            "ID_DTL_HASIL_UJI" => (int)$ID_DTL_HASIL_UJI[$i],
                            "ID_QUESTION" => (int)$QUESTION[$i],
                            "ANSWER" => $FILE_ANSWER["file_name"]
                        );
                    }
                } else {
                    // JIKA TIDAK UPLOAD FILE
                    if($ID_HASIL_UJI == ""){ // JIKA ADD
                        $dtl = array(
                            "ID_HASIL_UJI" => $act->MAX_ID,
                            "ID_QUESTION" => $QUESTION[$i],
                            "ANSWER" => ""
                        );
                    } else { // JIKA UPDATE
                        $dtl = array(
                            "ID_HASIL_UJI" => (int)$ID_HASIL_UJI,
                            "ID_DTL_HASIL_UJI" => (int)$ID_DTL_HASIL_UJI[$i],
                            "ID_QUESTION" => (int)$QUESTION[$i],
                        );
                    }
                    
                }
            } else {
                // JIKA YG DIUPLOAD TEXT BIASA

                if($ID_HASIL_UJI == ""){ // JIKA UPDATE
                    $dtl = array(
                        "ID_HASIL_UJI" => $act->MAX_ID,
                        "ID_QUESTION" => $QUESTION[$i],
                        "ANSWER" => $ANSWER[$i]
                    );
                } else { //JIKA UPDATE
                    $dtl = array(
                        "ID_HASIL_UJI" => (int)$ID_HASIL_UJI,
                        "ID_DTL_HASIL_UJI" => (int)$ID_DTL_HASIL_UJI[$i],
                        "ID_QUESTION" => (int)$QUESTION[$i],
                        "ANSWER" => $ANSWER[$i]
                    );
                }
                
            }
            
            array_push($data_dtl, $dtl);
            $i++;
        }

        if($ID_HASIL_UJI == ""){ //JIKA DD
            $act = $this->M_hasil_uji_proficiency->add_dtl_hasil_uji($data_dtl);
        } else { // JIKA UPDATE
            $act = $this->M_hasil_uji_proficiency->update_dtl_hasil_uji($data_dtl);
        }
        
        if($this->M_hasil_uji_proficiency->error()){
            $status = false;
		}
		else{
            $status = true;
		}
        echo $status;
    }

    public function send_email($title, $to, $name){
        $from = "qmo@sig.id";
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        if($uri['1'] == 'DEV'){
            $to = "muh.rudi.hariyanto@gmail.com";
        }
        $subject = "Permohonan Approval Input Hasil ".$title;
        $message = "Kepada Yth.
        ".$name."
        
        Dengan hormat,
        Kami telah melakukan pengujian atas sample yang sudah dikirim, dan sudah menginputkan hasil uji pada sistem qmo.semenindonesia.com
        Oleh karena itu kami mohon kesediaannya untuk dilakukan validasi dan pemberian feedback atas hasil yang sudah kami isikan.
        
        Regards,
        Koordinator Pelaksanaan ".$title;
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}

    public function export_to_excel($ID_PROFICIENCY, $KOMODITI){
        $objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);
		$style = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
        );

        $parameter = $this->M_hasil_uji_proficiency->get_parameter($KOMODITI);
        $start = 1;
        foreach ($parameter as $param) {
            $data = $this->M_hasil_uji_proficiency->detail_answer($ID_PROFICIENCY, $param['PARAMETER']);

            $objPHPExcel->getActiveSheet()->setCellValue('A'.$start, $param['PARAMETER'].' (%, db)')->mergeCells('A'.$start.':L'.$start);
            $objPHPExcel->getActiveSheet()->setCellValue('A'.($start + 1), 'LAB CODE')->mergeCells('A'.($start + 1).':A'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($start + 1), 'Lab Value')->mergeCells('B'.($start + 1).':E'.($start + 1));
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($start + 2), 'Uji ke-1');
            $objPHPExcel->getActiveSheet()->setCellValue('C'.($start + 2), 'Uji ke-2');
            $objPHPExcel->getActiveSheet()->setCellValue('D'.($start + 2), 'AVG');
            $objPHPExcel->getActiveSheet()->setCellValue('E'.($start + 2), 'U');
            $objPHPExcel->getActiveSheet()->setCellValue('F'.($start + 1), 'Assigned Value (Xpt)')->mergeCells('F'.($start + 1).':F'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('G'.($start + 1), 'SDPA')->mergeCells('G'.($start + 1).':G'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('H'.($start + 1), 'U Assigned')->mergeCells('H'.($start + 1).':H'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('I'.($start + 1), 'Zscore')->mergeCells('I'.($start + 1).':I'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('J'.($start + 1), 'Comment')->mergeCells('J'.($start + 1).':J'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('K'.($start + 1), 'Data - AVG SD')->mergeCells('K'.($start + 1).':K'.($start + 2));
            $objPHPExcel->getActiveSheet()->setCellValue('L'.($start + 1), 'Rating')->mergeCells('L'.($start + 1).':L'.($start + 2));
            $objPHPExcel->getActiveSheet()->getStyle('A'.($start + 1).':L'.($start + 2))->getAlignment()->setWrapText(true); 

            $worksheet = $objPHPExcel->setActiveSheetIndex(0);
            $row = $start + 3;
            $a = 1;
            foreach ($data as $dt) {
                $worksheet->setCellValue('A'.$row, "BB_".(strlen($a) > 0 ? "0".$a : $a)); 
                $worksheet->setCellValue('B'.$row, $dt['UJI_1']);
                $worksheet->setCellValue('C'.$row, $dt['UJI_2']);
                $worksheet->setCellValue('D'.$row, $dt['UJI_AVG']);
                $row++;
                $a++;
            }
            $objPHPExcel->getActiveSheet()->getStyle("A".$start.":L".($row - 1))->applyFromArray($style);
            $start = $start + count($data) + 6;
        }

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=DATA_UJI_PROFICIENCY.xlsx");
        $objWriter->save("php://output");
    }
}