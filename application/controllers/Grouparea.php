<?php

class Grouparea extends QMUser {
	
	public $list_data = array();
	public $data_grouparea;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_grouparea");
	}
	
	public function index(){
		$this->list_grouparea = $this->m_grouparea->datalist();
		$this->template->adminlte("v_grouparea");
	}
	
	public function add(){
		$this->template->adminlte("v_grouparea_add");
	}
	
	public function create(){
		$this->m_grouparea->insert($this->input->post());
		if($this->m_grouparea->error()){
			$this->notice->error($this->m_grouparea->error());
			redirect("grouparea/add");
		}
		else{
			$this->notice->success("Group Area Data Saved.");
			redirect("grouparea");
		}
	}
	
	public function edit($ID_GROUPAREA){
		$this->data_grouparea = $this->m_grouparea->get_data_by_id($ID_GROUPAREA);
		$this->template->adminlte("v_grouparea_edit");
	}
	
	public function update($ID_GROUPAREA){
		$this->m_grouparea->update($this->input->post(),$ID_GROUPAREA);
		if($this->m_grouparea->error()){
			$this->notice->error($this->m_grouparea->error());
			redirect("grouparea/edit/".$ID_GROUPAREA);
		}
		else{
			$this->notice->success("Group Area Data Updated.");
			redirect("grouparea");
		}
	}
	
	public function delete($ID_GROUPAREA){
		$this->m_grouparea->delete($ID_GROUPAREA);
		if($this->m_grouparea->error()){
			$this->notice->error($this->m_grouparea->error());
		}
		else{
			$this->notice->success("Group Area Data Removed.");
		}
		redirect("grouparea");
	}
	
	/* ajax */
	public function async_grouparea_list_config($ID_PLANT){
		$list_data = $this->list_grouparea = $this->m_grouparea->list_by_plant($ID_PLANT);
		$this->template->ajax_response("list_group_area_config_component");
	}

	public function get_list(){
		$list = $this->m_grouparea->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_GROUPAREA;
			$row[] = $no;
			$row[] = $column->KD_GROUPAREA;
			$row[] = $column->NM_GROUPAREA;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_grouparea->count_all(),
            "recordsFiltered" => $this->m_grouparea->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}	
	
}	

?>
