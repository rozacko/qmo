<?php

class NcqrList extends QMUser {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");		
		$this->load->model("m_company");
		$this->load->model("m_incident");
		$this->load->model("m_plant");
	}
	
	public function index(){
		$this->libExternal('dataTables,select2,datepicker,momentjs');
      	$this->receivAct = $this->getAccess('RECEIVER-ACT');

		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);

		// $this->list_table = $this->tableNcqrList();

		$this->template->adminlte("v_ncqr_list");
	}

	public function getData(){
		return "string";
	}


	function tableNcqrList($export = null){ 

		if($export == 'export'){
			$post = $this->input->get();

			$post['ID_PLANT'] = explode('-', $post['ID_PLANT']);
			$post['STATUS'] = explode('-', $post['STATUS']);

			// $post['PERIODE'] = '10/2018';
			// $post['ID_COMPANY'] = 8;
			// $post['ID_PLANT'] = array(5);
			// $post['STATUS'] = array(1,2,3,'Y');

			header("Content-type: application/vnd-ms-excel");
			header("Content-Disposition: attachment; filename=NCQR List (".$post['PERIODE'].").xls");
 		}else{
			$post = $this->input->post();
		}

		if(!$post['ID_COMPANY']){
			echo json_encode(array('status' => false, 'msg' => 'Sorry, the company can\'t be empty!'));
			exit;
		}
		if(!$post['PERIODE']){
			echo json_encode(array('status' => false, 'msg' => 'Sorry, the date can\'t be empty!'));
			exit;
		}

		if(!is_array($post['ID_PLANT'])){
			echo json_encode(array('status' => false, 'msg' => 'Sorry, the plant can\'t be empty!'));
			exit;
		}

		if(!is_array($post['STATUS'])){
			echo json_encode(array('status' => false, 'msg' => 'Sorry, the status can\'t be empty!'));
			exit;
		}
	// var_dump($post);	
// exit;
		$status = array();
      	$this->receivAct = $this->getAccess('RECEIVER-ACT');
		foreach ($post['STATUS'] as $key => $value) {
			if($this->receivAct){
				if($value != '2'){
					$status[] = $value;
				}
			}else{
				$status[] = $value;
			}

		}
		$post['STATUS'] = $status;
		// echo "<pre>";
		// print_r($post['STATUS']);
		// echo "</pre>";


		// exit;
		

		$paramComp['ID_COMPANY'] = $post['ID_COMPANY'];
		$paramPlant = join($post['ID_PLANT'], ',');
		$periode = $post['PERIODE'];
		$periode = explode('/', $periode);

		$month = $periode[0]; //Mengambil bulan saat ini
		$year = $periode[1]; //Mengambil tahun saat ini
		$jmlhari = (int)cal_days_in_month(CAL_GREGORIAN, $month, $year);

		$m_groupplant = $this->m_incident->getGroupPlant($paramComp);

		// var_dump($m_groupplant);
		// exit;
		$merge = 0;
		$mergeFm = 0;
		foreach ($m_groupplant as $i => $iv) {
			$m_grouparea = $this->m_incident->getGroupArea();
			foreach ($m_grouparea as $j => $jv) {
				$m_groupplant[$i]['GROUPAREA'][] = $jv;
				// $iwhere['ID_GROUP_PLANT'] = $iv['ID_GROUP_PLANT'];

				$id_grouparea = $jv['ID_GROUPAREA'];
				$id_group_plant = $iv['ID_GROUP_PLANT'];

				$m_plant = $this->m_incident->getPlant($id_grouparea, $id_group_plant, $paramPlant);

				foreach ($m_plant as $k => $kv) {
					if($id_grouparea == 1){
						$m_groupplant[$i]['GROUPAREA'][$j]['PLANT'][] = $kv;
						
						$id_plant = $kv['ID_PLANT'];
						$m_area = $this->m_incident->getArea($id_plant, $id_grouparea);
						
						foreach ($m_area as $l => $lv) {
							$m_groupplant[$i]['GROUPAREA'][$j]['PLANT'][$k]['AREA'][] = $lv;
							$merge +=1;
							$mergeFm +=1;
						}
					}else{
						$merge +=1;
						print_r($kv);
						$m_groupplant[$i]['GROUPAREA'][$j]['PLANT'][] = $kv;
						$m_groupplant[$i]['GROUPAREA'][$j]['PLANT'][$k]['AREA'] = array();
						
					}
				}

			}
		}
		
		$jmlStatus = count($post['STATUS']);
		switch ($jmlStatus) {
		    case 1:
		    	$colspanMonth_Status = 2;
		    	$colspanFoot = 3;
		        break;
		    case 2:
		    	$colspanMonth_Status = 4;
		    	$colspanFoot = 5;
		        break;
		    case 3:
		    	$colspanMonth_Status = 6;
		    	$colspanFoot = 7;
		        break;
		    default:
		    	$colspanMonth_Status = 8;
		    	$colspanFoot = 9;
		}

		$TH = "";
		$TH_ = "";
		$TH__ = "";
		if(in_array(1, $post['STATUS'])){
			$TH .= "<th colspan='2' style='text-align: center'>IN PROGRESS</th>";
			$TH_ .= "<th style='text-align: center'>JML</th><th style='text-align: center'>NILAI</th>";
			// $TH__ .= "<th colspan='2' style='text-align: center'>5</th>";
		}
		if(in_array(2, $post['STATUS'])){
			$TH .= "<th colspan='2' style='text-align: center'>NORMAL BY SYSTEM</th>";
			$TH_ .= "<th style='text-align: center'>JML</th><th style='text-align: center'>NILAI</th>";
			// $TH__ .= "<th colspan='2' style='text-align: center'>4</th>";
		}
		if(in_array(3, $post['STATUS'])){
			$TH .= "<th colspan='2' style='text-align: center'>RECEIVER ACTION</th>";
			$TH_ .= "<th style='text-align: center'>JML</th><th style='text-align: center'>NILAI</th>";
			// $TH__ .= "<th colspan='2' style='text-align: center'>3</th>";
		}
		if(in_array('Y', $post['STATUS'])){
			$TH .= "<th colspan='2' style='text-align: center'>CLOSED</th>";
			$TH_ .= "<th style='text-align: center'>JML</th><th style='text-align: center'>NILAI</th>";
			// $TH__ .= "<th colspan='2' style='text-align: center'>2</th>";
		}

		$tbl = '';
		foreach ($m_groupplant as $i => $iv) {
		$tbl .= "<table class='table table-striped table-bordered table-hover id='tblNcqr' border='1' style='width:100%'>
				<thead>
				<tr>
					<th width='200' rowspan='4' width='100' style='vertical-align:middle;text-align:center'>GROUPAREA</th>
					<th colspan='3' rowspan='4' style='vertical-align:middle;text-align:center'>AREA</th>
					<th colspan='2' style='text-align: center'>NILAI</th>
					<th colspan='".$colspanMonth_Status."' style='text-align: center'>BULAN</th>
					<th rowspan='4' style='vertical-align:middle;text-align:center'>TOTAL<br> NILAI</th>
					<th rowspan='4' style='vertical-align:middle;text-align:center'>TOTAL<br> POINT</th>
				</tr>
				<tr>
					<th rowspan='3' style='vertical-align:middle;text-align:center'>MAX</th>
					<th rowspan='3' style='vertical-align:middle;text-align:center'>MAX <br>Kejadian</th>
					<th colspan='".$colspanMonth_Status."' style='text-align: center'>STATUS</th>
				</tr>
				<tr>
					".$TH."
				</tr>";
				// <tr>
				// 	".$TH__."
				// </tr>
			$tbl .= "<tr>
					".$TH_."
				</tr>

				</thead>
				<tbody>";

				$jmlGa = count($iv['GROUPAREA']);
				$center = "text-align: center;";

				$totalKL = 0;
				$totalFM = 0;
				$totJml = 0;
				$totJmlKL = 0;
				$totJmlFM = 0;
				foreach ($iv['GROUPAREA'] as $j => $jv) {
					$jmlP = count($m_groupplant[$i]['GROUPAREA'][$j]['PLANT']);

					foreach ($jv['PLANT'] as $k => $kv) {
						$jmlA = count($m_groupplant[$i]['GROUPAREA'][$j]['PLANT'][$k]['AREA']);
						if($jv['ID_GROUPAREA'] == 4){
							
							$max = (24/3)*$jmlhari;
							$maxIncident = $max;
							$max = $maxIncident*5;

							$totalKL += $max;
							//KILN-----------------------------------
							$jml_inprogress = $this->m_incident->totalIncident($year, $month, $kv['ID_PLANT'], 'N', $jv['ID_GROUPAREA'], 1);
// echo "string";eexit
							// xit;
							$jml_normalsystem = $this->m_incident->totalIncident($year, $month, $kv['ID_PLANT'], 'N', $jv['ID_GROUPAREA'], 2);
							$jml_receiveract = $this->m_incident->totalIncident($year, $month, $kv['ID_PLANT'], 'N', $jv['ID_GROUPAREA'], 3);
							$jml_closed = $this->m_incident->totalIncident($year, $month, $kv['ID_PLANT'], 'Y', $jv['ID_GROUPAREA']);

							if($this->receivAct){
								$jml_inprogress += $jml_normalsystem;
							}

							$attr = " id_plant='".$kv['ID_PLANT']."' nm_plant='".$kv['NM_PLANT']."' id_grouparea='".$jv['ID_GROUPAREA']."' nm_grouparea='".$jv['NM_GROUPAREA']."' ";

							$totJumlah = 0;
							$TD1 = "";
							if(in_array(1, $post['STATUS'])){
								// $attr .= " status='1' ";
								$totJumlah += ($jml_inprogress*5);
		                    	$numStatus = 1;
			                    if($this->receivAct){
			                    	$numStatus = '1,2';
			                    }
								$TD1 .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='".$numStatus."'>".$jml_inprogress."</td><td style='{$center}'>".($jml_inprogress*5)."</td>";
							}
							if(in_array(2, $post['STATUS'])){
								// $attr .= " status='2' ";
								$totJumlah += ($jml_normalsystem*4);
								$TD1 .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='2'>".$jml_normalsystem."</td><td style='{$center}'>".($jml_normalsystem*4)."</td>";
							}
							if(in_array(3, $post['STATUS'])){
								// $attr .= " status='3' ";
								$totJumlah += ($jml_receiveract*3);
								$TD1 .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='3'>".$jml_receiveract."</td><td style='{$center}'>".($jml_receiveract*3)."</td>";
							}
							if(in_array('Y', $post['STATUS'])){
								// $attr .= " status='Y' ";
								$totJumlah += ($jml_closed*2);
								$TD1 .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='Y'>".$jml_closed."</td><td style='{$center}'>".($jml_closed*2)."</td>";
							}

							$totJml += $totJumlah;
							$totJmlKL += $totJumlah;
									
							$tbl .="<tr>";
								if($j == 0 && $k == 0){
									$tbl .= "<td rowspan='".($merge)."' style='vertical-align:middle;'>".$iv['NM_GROUP_PLANT']."</td>";
								}
								if($k == 0){
									$tbl .= "<td rowspan='".$jmlP."' style='vertical-align:middle;'>".$jv['NM_GROUPAREA']."</td>";
								}
								$tbl .= "<td colspan='2' style='vertical-align:middle;'>".$kv['NM_PLANT']."</td>";
								$tbl .="<td style='{$center}'>".$max."</td>
									<td style='".$center."''>".$maxIncident."</td>
									".$TD1."
									<td style='".$center."'>".$totJumlah."</td>
									<td style='".$center."'></td>
								</tr>";
						}else{
							//FINISH MILL -----------------------------------

							foreach ($kv['AREA'] as $l => $lv) {

								$max = 4*$jmlhari*3; // 1hari 12 uji 3 parameter = (4perhari) max 4*30*3param
								// $max = (48/3)*$jmlhari;
								$maxIncident = $max;
								$max = $maxIncident*5;
								$totalFM += $max;

								$jml_inprogress = $this->m_incident->totalIncidentFM($year, $month, $kv['ID_PLANT'], $lv['ID_AREA'], 'N', $jv['ID_GROUPAREA'], 1);
								$jml_normalsystem = $this->m_incident->totalIncidentFM($year, $month, $kv['ID_PLANT'], $lv['ID_AREA'], 'N', $jv['ID_GROUPAREA'], 2);
								$jml_receiveract = $this->m_incident->totalIncidentFM($year, $month, $kv['ID_PLANT'], $lv['ID_AREA'], 'N', $jv['ID_GROUPAREA'], 3);
								$jml_closed = $this->m_incident->totalIncidentFM($year, $month, $kv['ID_PLANT'], $lv['ID_AREA'], 'Y', $jv['ID_GROUPAREA']);
							
								if($this->receivAct){
									$jml_inprogress += $jml_normalsystem;
								}

								// $attr = " id_plant='".$kv['ID_PLANT']."'  id_grouparea='".$jv['ID_GROUPAREA']."' id_area='".$lv['ID_AREA']."' ";
								$attr = " id_plant='".$kv['ID_PLANT']."' nm_plant='".$kv['NM_PLANT']."' id_grouparea='".$jv['ID_GROUPAREA']."' nm_grouparea='".$jv['NM_GROUPAREA']."'  id_area='".$lv['ID_AREA']."' ";

								$totJumlah = 0;
								$TD = "";
								if(in_array(1, $post['STATUS'])){
									$totJumlah += ($jml_inprogress*5);
									$TD .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='1'>".$jml_inprogress."</td><td style='{$center}'>".($jml_inprogress*5)."</td>";
								}
								if(in_array(2, $post['STATUS'])){
									$totJumlah += ($jml_normalsystem*4);
									$TD .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='2'>".$jml_normalsystem."</td><td style='{$center}'>".($jml_normalsystem*4)."</td>";
								}
								if(in_array(3, $post['STATUS'])){
									$totJumlah += ($jml_receiveract*3);
									$TD .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='3'>".$jml_receiveract."</td><td style='{$center}'>".($jml_receiveract*3)."</td>";
								}
								if(in_array('Y', $post['STATUS'])){
									$totJumlah += ($jml_closed*2);
									$TD .= "<td class='btnDetail' style='{$center} cursor: pointer' ".$attr." status='Y'>".$jml_closed."</td><td style='{$center}'>".($jml_closed*2)."</td>";
								}
								$totJml += $totJumlah;
								$totJmlFM += $totJumlah;


								$tbl .="<tr>";
									if($j == 0 && $k == 0){
										// $tbl .= "<td rowspan='".$jmlA."' style='vertical-align:middle;'>1</td>";
									}
									if($k == 0 && $l == 0){
										$tbl .= "<td rowspan='".$mergeFm."' style='vertical-align:middle;'>".$jv['NM_GROUPAREA']."</td>";
									}
									if($l == 0){
										$tbl .= "<td rowspan='".$jmlA."' style='vertical-align:middle;'>".$kv['NM_PLANT']."</td>";
									}

									$tbl .= "<td style='vertical-align:middle;'>".$lv['KD_AREA']."</td>";
									$tbl .="<td style='{$center}' >".$max."</td>
										<td style='{$center}'>".$maxIncident."</td>
										".$TD."
										<td style='{$center}'>".$totJumlah."</td>
										<td style='{$center}'></td>
									</tr>";
							}
						}
					}
				}
				// $tbl .="</tbody>";

				// $tbl .= "<tfoot>";
					$tbl .= "<tr>";
						$tbl .= "<td align='center' colspan='4'>TOTAL NILAI KILN</td>";
						// $tbl .= "<td align='center'>-</td>";
						$tbl .= "<td align='center'>".$totalKL."</td>";
						$tbl .= "<td align='center' colspan='".$colspanFoot."'></td>";
						$tbl .= "<td align='center'>".$totJmlKL."</td>";
						$tbl .= "<td align='center'>".round(((($totalKL-$totJmlKL)/$totalKL)*100),2)."%</td>";
					$tbl .= "</tr>";
					$tbl .= "<tr>";
						$tbl .= "<td align='center' colspan='4'>TOTAL NILAI FINISH MILL</td>";
						$tbl .= "<td align='center'>".$totalFM."</td>";
						// echo "<td align='center'>".$totalFm."</td>";
						$tbl .= "<td align='center' colspan='".$colspanFoot."'></td>";
						$tbl .= "<td align='center'>".$totJmlFM."</td>";
						$tbl .= "<td align='center'>".round(((($totalFM-$totJmlFM)/$totalFM)*100),2)."%</td>";
					$tbl .= "</tr>";
					$tbl .= "<tr>";
						$tbl .= "<td align='center' colspan='4'>TOTAL</td>";
						$tbl .= "<td align='center'>".($totalKL+$totalFM)."</td>";
						$tbl .= "<td align='center' colspan='".$colspanFoot."'></td>";
						$tbl .= "<td align='center'>".$totJml."</td>";
						$tbl .= "<td align='center'>".round(((($totalKL+$totalFM)-$totJml)/($totalKL+$totalFM)*100),2)."%</td>";
					$tbl .= "</tr>";
				// $tbl .= "</tfoot>";
				$tbl .="</tbody>";


			$tbl .= "</table><br><br>";
		}

		if($export == 'export'){
			echo $tbl;
		}else{
			echo json_encode(array('status' => true, 'msg' => 'Success, load data!', 'data'=> $tbl));
		}
	}	

	function getDetail(){
		$post = $this->input->post();
		if($post['ID_NCQR_STATUS'] == 'Y'){
			$post['CLOSED_STATUS'] = 'Y';
			unset($post['ID_NCQR_STATUS']);
		}else{
			$post['CLOSED_STATUS'] = 'N';
		}
		if($post['ID_AREA'] == ''){
			unset($post['ID_AREA']);
		}

		$month = date('m');
		$year = date('Y');
		if($post['PERIODE']){
			$periode = explode('/', $post['PERIODE']);

			$month = $periode[0]; //Mengambil bulan saat ini
			$year = $periode[1]; //Mengambil tahun saat ini
			// $post['EXTRACT(MONTH FROM TANGGAL)'] = $month;
			// $post['EXTRACT(MONTH FROM TANGGAL)'] = $year;

			unset($post['PERIODE']);
		}

		$list = $this->m_incident->getDetailNcqr($post, $month, $year);
		// echo $this->db->last_query(); 
		if(count($list) > 0){
			echo json_encode(array('status' => true, 'msg' => 'Success, load data!', 'data'=> $list));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'Sorry, data not found!', 'data'=> $list));
		}
	}

	public function getPlant($ID_COMPANY=NULL){
		$list_plant = array();
		if($ID_COMPANY){
			$list_plant = $this->m_plant->datalist($ID_COMPANY);
		}
		$list_plant[] = array('ID_PLANT' => '', 'NM_PLANT' => 'Choose Plant...');
		sort($list_plant);
		echo json_encode($list_plant);
	}
	
	public function notif_sent2($ID_INCIDENT){
		$list = $this->m_incident->list_notif($ID_INCIDENT); # echo $this->m_incident->get_sql();
		if(count($list) > 0){
			echo json_encode(array('status' => true, 'msg' => 'Success, load data!', 'data'=> $list));
		}else{
			echo json_encode(array('status' => false, 'msg' => 'Sorry, data not found!', 'data'=> $list));
		}
	}


}	

?>
