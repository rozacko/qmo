<?php

class Telegram_test extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('TelegramNotification');
    }

    public function index() {
        echo "Halo telegram";
    }

    public function sendMessage($chat_id = '')
    {
        $message = $this->input->get('message') ?: 'empty message';
        $chat_id = $chat_id ?: $this->input->get('chat_id');
	$chat_id = 'NQCR-DEV';
	echo "sending to $chat_id";

        $result = $this->TelegramNotification->sendMessage($chat_id, $message);

        echo json_encode($result);
    }

    public function sendOpco($id_area, $id_jabatan) {
        $message = "hello from Opco\nPiye kabare?";

        $result = $this->TelegramNotification->send_by_notification_member($id_area, $id_jabatan, $message);

        echo json_encode($result);
    }

    public function set_webhook() {
        if ( ENVIRONMENT !== 'development' ) {
            show_404();
        }

        $url = $this->input->get('url');

        if ( $this->TelegramNotification->set_webhook($url) ) {
            $text = 'Telegram webhook set to ' . $url;
            log_message('debug', $text);

            echo $text;
        }
        else {
            $text = 'Error set webhook to ' . $url;
            log_message('error', $text);

            echo $text;
        }
    }

    public function get_webhook() {
        echo "hello ".__FUNCTION__;
    }

    public function webhook() {
        $token = $this->input->get('token');
        $this->load->model('TelegramNotification');

        $callable = function($update, $telegram, &$reason) {
            log_message('debug', json_encode($update));

            return true;
        };

        $this->TelegramNotification->handle_webhook_with_token($token, $callable);
    }

    public function notify($id_opco) {
        $this->load->model('M_opco');
        $this->load->model('TelegramNotification');

        $opco = $this->M_opco->get_data_by_id($id_opco);

        
        // $this->TelegramNotification->
    }
}
