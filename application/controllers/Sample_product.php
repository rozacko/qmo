<?php

class Sample_product extends QMUser {
	
	public $list_data = array();
	public $data_component;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_component");
		$this->load->model("m_sampleproduct");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->list_component = $this->m_sampleproduct->datalist();
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_sample_product", $data);
	}

	public function save_product(){
		
		$result['msg'] = 'Cannot Insert New Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['MERK_PRODUCT'] = $post['MERK_PRODUCT'];
		$data['TYPE_PRODUCT'] = $post['TYPE_PRODUCT'];
		$data['INITIAL_COMPETITOR'] = $post['INITIAL_COMPETITOR'];
		if ((int)$post['IS_SMIG'] == 1) {
			# code...
			$data['ID_COMPANY'] = $post['ID_COMPANY'];
			$data['IS_SMIG'] = $post['IS_SMIG'];
		}
		$data['CREATE_BY'] = $post['user'];

		$isexsist_product = $this->m_sampleproduct->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_sampleproduct->sample_insert($data);
			if ($isertd_product) {
				// code...
				$result['msg'] = 'Insert New Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}
		
		to_json($result);
	}

	public function update_product(){
		
		$result['msg'] = 'Cannot Update Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_COMPETITOR'] = $post['ID_COMPETITOR'];
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['MERK_PRODUCT'] = $post['MERK_PRODUCT'];
		$data['TYPE_PRODUCT'] = $post['TYPE_PRODUCT'];
		$data['INITIAL_COMPETITOR'] = $post['INITIAL_COMPETITOR'];
		if ((int)$post['IS_SMIG'] == 1) {
			# code...
			$data['ID_COMPANY'] = $post['ID_COMPANY'];
			$data['IS_SMIG'] = $post['IS_SMIG'];
		}
		$data['MODIFIED_BY'] = $post['user'];

		$isexsist_product = $this->m_sampleproduct->is_sample_exists($data);

		if ($isexsist_product <= 0) {
			# code...

			$isertd_product = $this->m_sampleproduct->sample_update($data);

			if ($isertd_product) {
				// code...
				$result['msg'] = 'Update Product Success ...';
				$result['status'] = true;
			}

		} else {
			# code...
			$result['msg'] = 'Name Product Already Exsist';
			$result['status'] = false;
		}

		to_json($result);
	}

	public function update_product_type(){
		
		$result['msg'] = 'Cannot Update Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['KODE_PERUSAHAAN'] = $post['KODE_PERUSAHAAN'];
		$type_product = json_decode($post['TYPE_PRODUCT']);

		$resettype_product = $this->m_sampleproduct->reset_product_type_exists($data['KODE_PERUSAHAAN']);

		foreach ($type_product as $key => $value) {
			# code...
			$data['CREATE_BY'] = $post['user'];

			$id_type = $this->m_sampleproduct->get_id_product_type($value);
			$data['TYPE_PRODUCT'] = $id_type['ID_PRODUCT'];
			$isertd_product = $this->m_sampleproduct->product_type_insert($data);

			if ($isertd_product) {
				// code...
				$result['msg'] = 'Set Product Type Success ...';
				$result['status'] = true;

			} else {
				# code...
				$result['msg'] = 'Update failed';
				$result['status'] = false;
				$result['type_product'] = $value;
				to_json($result);
				exit;
			}

		}

		to_json($result);
	}

	public function delete_product(){
		
		$result['msg'] = 'Cannot Delete Product ...';
		$result['status'] = false;

		$post = $this->input->post();
		$data['ID_COMPETITOR'] = $post['ID_COMPETITOR'];
		$data['NAME_COMPETITOR'] = $post['NAME_COMPETITOR'];
		$data['MERK_PRODUCT'] = $post['MERK_PRODUCT'];
		$data['TYPE_PRODUCT'] = $post['TYPE_PRODUCT'];
		$data['DELETE_BY'] = $post['user'];

		$isertd_product = $this->m_sampleproduct->sample_delete($data);
		if ($isertd_product) {
			// code...
			$result['msg'] = 'Delete Product Success ...';
			$result['status'] = true;
		}

		to_json($result);
	}

	public function product_list(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

      	$data	= $this->m_sampleproduct->get_data($key);

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_sampleproduct->recFil($key),
  			'recordsTotal'		=>	$this->m_sampleproduct->recTot($key)
  		);

  		echo json_encode($return);
    }

    public function product_list_scm(){

  		$search	= $this->input->post('search');
  		$order	= $this->input->post('order');
  		
  		$key	= array(
  			'search'	=>	$search['value'],
  			'ordCol'	=>	$order[0]['column'],
  			'ordDir'	=>	$order[0]['dir'],
  			'length'	=>	$this->input->post('length'),
  			'start'		=>	$this->input->post('start')
  		);

      	$data	= $this->m_sampleproduct->get_data_scm($key);

      	foreach ($data as $key => $value) {
      		# code...
      		$type_product = $this->m_sampleproduct->get_typeProduct($value);

      		$temptype = array();
      		foreach ($type_product as $ind => $val) {
      			$temptype[preg_replace('/\s+/', '', $val['KD_PRODUCT'])] = $val['ID_PRODUCT'];
      		}

      		$data[$key]['TYPE_PRODUCT'] = $temptype;

      		if ($type_product) {
      			# code...
      		} else {
      			# code...
      		}
      	}

  		$return	= array(
  			'draw'				=>	$this->input->post('draw'),
  			'data'				=>	$data,
  			'recordsFiltered'	=>	$this->m_sampleproduct->recFil_scm($key),
  			'recordsTotal'		=>	$this->m_sampleproduct->recTot_scm($key)
  		);

  		echo json_encode($return);
    }

}	

?>
