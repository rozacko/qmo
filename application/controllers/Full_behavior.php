<?php

class Full_behavior extends Home {
 
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_incident");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_ncqr_product");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
	}

	public function behavior(){  
		// echo $NM_PLANT;
		$jNM_PLANT = $this->input->post('NM_PLANT');
		$jKD_AREA = $this->input->post('ID_AREA');
            if ($jNM_PLANT == '') {
                $NM_PLANT = '';
            } else {
                $NM_PLANT = $this->input->post('NM_PLANT');
            }

            if ($jKD_AREA == '') {
                $ID_AREA = '';
            } else {
                $ID_AREA = $this->input->post('ID_AREA');
            }

		$data['NM_PLANT'] = $ID_AREA;
		$this->load->model("m_company");
		$this->list_company = $this->m_company->datalist();
		$this->list_type = $this->m_incident->type();

            // $taahun = $_POST['taahun'];
		// echo $NM_PLANT;
		// $cek = $this->convert($NM_PLANT, $ID_AREA);
		$get_area 	= $this->t_cement_hourly->get_area_behavior(" ID_AREA='".$ID_AREA."'");
		$get_area_plant 	= $this->t_cement_hourly->get_area_plant($get_area[0]['ID_PLANT']);
			// echo $this->db->last_query();die;
		$this->load->vars($data);
		$plant = $this->m_plant->cek_behavior($get_area[0]['NM_PLANT']);
		 $this->rplant = $plant[0]->NM_PLANT;
		$this->sCompany = $get_area_plant[0]['ID_COMPANY'];
		$this->rID_AREA = $get_area[0]['NM_AREA'];
		$this->rNM_PLANT = $get_area_plant[0]['NM_PLANT'];
		$this->tID_AREA = $get_area[0]['ID_AREA'];
		$this->template->v_full_behavior("v_full_behavior", $data);
	}

	public function ajax_get_plant($ID_COMPANY=NULL, $ID_PLANT=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY, $ID_PLANT);
		to_json($plant);
	}
 
	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->c_ncqr_product->ncqr_area_behavior($ID_COMPANY,$ID_PLANT);# echo $this->m_area->get_sql();
			// echo $this->db->last_query();die;
		echo json_encode($area);
	}

	public function get_ajax_live(){ 
			// var_dump($ID_AREA);die;
			$date = date("d-m-Y");
			// $t_prod 	= $this->t_cement_hourly->data_where_behavior_CCR(" ID_AREA='".$ID_AREA."' AND A.ID_MESIN_STATUS IS NULL" , $date);
			$t_prod 	= $this->t_cement_hourly->data_where_behavior_CCR(" ID_AREA='".$this->input->post('ID_AREA')."' AND A.ID_MESIN_STATUS IS NULL ", $date);
			$area = $this->input->post('NM_AREA');
			// echo $this->db->last_query();die;

			$data = array();
		if (!empty($t_prod)) {
				$nill = 0;
			foreach ($t_prod as $key => $row) {

				
				$d_prod = $this->d_cement_hourly->data_where_behavior_live($row->ID_CEMENT_HOURLY);
			    //echo '<br>';var_dump($d_prod);

				$l = 0;
				$kosong = false;
				foreach ($d_prod as $k => $vl) {
					if($vl->NILAI!=''){
						$l++;
						$data[$nill][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
						$kosong = true;
					}else{
						
					}

					if($nill >= 8){
						break;
					}
				}
				if($kosong){
					$data[$nill][] = $row->DATE_DATA; 
					$data[$nill][] = $row->JAM_DATA; 
						$nill++;
					// $data[$key][] = ($nil=='') ? '': (float) $nil;
				}
			}
		}
 		echo json_encode(array("data" => $data, "nama_area" => $area));
	}
 
 public function convert($NM_PLANT, $KD_AREA){
		$get_area 	= $this->t_cement_hourly->get_area(" b.NM_PLANT='".$NM_PLANT."' AND A.ID_AREA='".$KD_AREA."'");
			
	return $get_area;
 }

}

?>