<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_clinker_prod extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("c_product");
		$this->load->model("m_product");
		$this->load->model("t_production_clinker");
		$this->load->model("d_production_clinker");
		$this->load->model("qaf_daily");				
	}
	
	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_clinker_prod", $data);
	}

	// public function ajax_get_area_cement($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
	// 	$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
	// 	//var_dump($area);exit;
	// 	to_json($area);
	// }

	// public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
	// 	$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA, TRUE);#echo $this->m_area->get_sql();
	// 	to_json($area);
	// }
	
	// public function ajax_get_plant($ID_COMPANY=NULL){
	// 	$plant= $this->m_plant->datalist($ID_COMPANY);
	// 	to_json($plant);
	// }
	
	public function ajax_get_clinkerfactor($id_company=''){
		//HandsonTable Column Header
		$comp = $this->get_company_nm();	
		//print_r($comp_factor);

		$no=1;
		foreach ($comp as $val):
			$hdr_comp[$no][KD_COMPANY] = $val[KD_COMPANY];	
			$hdr_comp[$no][NM_COMPANY] = $val[NM_COMPANY];	
			$no++;
		endforeach;
		

		$header[]['title'] = 'CODE';
		$header[]['title'] = 'COMPANY';
		$header[]['title'] = 'Clinker OPC';	
		$header[]['title'] = 'Clinker NON OPC';	
		$data['colHeader'] = $header;
		$data['rows'] 	   = count($comp);
		$data['area']   = $hdr_comp;
		
		to_json($data);
	}

	// public function ajax_get_areaproduct($id_company=''){
	// 	//HandsonTable Column Header
	// 	//$prod = $this->m_area->datalistOutsilo( $id_company);
	// 	$area_silo = $this->get_area_nm($id_company);		
	// 	//$no=1;
	// 	foreach ($area_silo as $val):
	// 		$area[] = $val;	
	// 		// $no++;	
	// 	endforeach;
		

	// 	$header[]['title'] = 'AREA';
	// 	$header[]['title'] = 'CODE';
	// 	// $prod_area = $this->get_product_nm($id_company);
	// 	// //$no =2;
	// 	// foreach ($prod_area as $val):
	// 	// 	$header[]['title']= $val[KD_PRODUCT];		
	// 	// 	//$no++;	
	// 	// endforeach;

	// 	$header[]['title'] = 'Clinker OPC';	
	// 	$header[]['title'] = 'Clinker NON OPC';	
	// 	//$header[]['title'] = 'PRODUCT TYPE';		
	// 	// $header[]['title'] = 'PRODUCTION';
	// 	$data['colHeader'] = $header;
	// 	$data['rows'] 	   = count($area_silo);
	// 	$data['area']   = $area;
		
	// 	to_json($data);
	// }

	// public function ajax_get_product($id_company=''){
	// 	//HandsonTable Column Header
	// 	//$prod = $this->m_area->datalistOutsilo( $id_company);
		
	// 	$area_silo = $this->get_area_nm($id_company);		
	// 	$no=1;
	// 	foreach ($area_silo as $val):
	// 		$area[$no][KD_AREA] = $val[KD_AREA];	
	// 		$area[$no][NM_AREA] = $val[NM_AREA];	
	// 		$no++;	
	// 	endforeach;
		
		
	// 	$header[]['title'] = 'AREA';
	// 	//$header[]['title'] = 'Code Product';
	// 	$header[]['title'] = 'Clinker OPC';	
	// 	$header[]['title'] = 'Clinker NON OPC';	
	// 	$data['colHeader'] = $header;
	// 	$data['rows'] 	   = count($prod_area);
	// 	$data['area']   = $area;
	// 	//$data['kdproduct']   = $produk;
	// 	to_json($data);
	// }

	public function get_area_nm($id_company=''){
		$data = array();
		$param = $this->m_area->datalist( $id_company , NULL, 4);
		//echo $this->m_area->get_sql();
		$n = 0;
		foreach ($param as $col):
			$data[$n][ID_AREA] = $col->ID_AREA;
			$data[$n][NM_AREA] = $col->NM_AREA;
			$data[$n][KD_AREA] = $col->KD_AREA;
			$n++;
		endforeach;
		
		return $data;
	}

	public function get_company_nm($id_company=''){
		$data = array();
		$param = $this->m_company->list_company();
		//echo $this->m_area->get_sql();
		$n = 0;
		foreach ($param as $col):
			$data[$n][ID_COMPANY] = $col->ID_COMPANY;
			$data[$n][NM_COMPANY] = $col->NM_COMPANY;
			$data[$n][KD_COMPANY] = $col->KD_COMPANY;
			$n++;
		endforeach;
		
		return $data;
	}

	public function load_table(){
		error_reporting(E_ALL);
		$data = array();
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];

		//Load from T_production
		$data = array();
		$t_prod = $this->t_production_clinker->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."' ");
		//echo $this->db->last_query();die();

		$area = $this->get_company_nm(); 
		
		if (empty($t_prod)) {						
			foreach ($area as $val):				
				//$data[]= array($val[NM_AREA],$val[KD_AREA]);
				$ar_tmp = array();	
				$ar_tmp[] = $val[KD_COMPANY];	
				$ar_tmp[] = $val[NM_COMPANY];			
				$data[] = $ar_tmp;
			endforeach;
		}else{
			//if($form->ID_GROUPAREA == 4) $prod = "(NO TYPE)";
			if(!$area) $area[0] = "-";
			$id_tprod = $t_prod[0]->ID_PRODUCTION_CLINKER;
			$id = 1;
			foreach ($area as $val):
				$ar_tmp = array();	
				$ar_tmp[] = $val[KD_COMPANY];
				$ar_tmp[] = $val[NM_COMPANY];
				//print_r($val);
				$rs  = $this->d_production_clinker->get_factorcomp($id_tprod, $id, $val[ID_COMPANY]);
				
				$ar_tmp[] = $rs->OPC;
				$ar_tmp[] = $rs->NON_OPC;				
				$data[] = $ar_tmp;
				$id++;
			endforeach;
		}

		to_json($data);
	}

	public function save_table(){
		$post  = $this->input->post();
		$form  = $post['formData'];
		$data  = $post['data'];
		// var_dump($data); 
		// die();
		$headertbl  = $post['headerlist'];
		
		// $flag_sum  = $post['jenis'];
		
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];
			
		//T_PRODUCTION_DAILY (1): ID_PRODUCTION,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION, ID_COMPONENT, NILAI
		$tdata['BULAN']	= $form->BULAN;
		$tdata['TAHUN']	= $form->TAHUN;
		// $tdata['ID_COMPANY']	= $form->ID_COMPANY;
		//$tdata['KD_AREA']	= $form->KD_AREA;

		$company = $this->get_company_nm();
		$ar_comp = array();
		foreach($company as $v){
			$ar_comp[$v[KD_COMPANY]] =  $v[ID_COMPANY];
		}

		
		$ID_PRODUCTION  = $this->t_production_clinker->get_id($tdata['BULAN'],$tdata['TAHUN']);

		if(!$ID_PRODUCTION){
			$tdata['DATE_ENTRY'] = date("d/m/Y");
			$tdata['USER_ENTRY'] = $this->USER->ID_USER;
			$ID_PRODUCTION = $this->t_production_clinker->insert($tdata);
		}
		else{
			$tdata['DATE_UPDATE'] = date("d/m/Y");
			$tdata['USER_UPDATE'] = $this->USER->ID_USER;
			//$tdata['KD_AREA']	= $form->ID_AREA;
			$this->t_production_clinker->update($ID_PRODUCTION,$tdata);
		}

	
			$ctr = 1;
			foreach($data as $y => $row){ //y index	
							
				$ddata = null;
				$ddata['ID_PRODUCTION_CLINKER'] = $ID_PRODUCTION;
				$ddata['ID_COMPANY'] = $ar_comp[trim($row[0])];//$form->ID_COMPANY;		
				//$idarea = $this->m_area->getidbykode(trim($row[1]));				
				// $ddata['ID_AREA']		= $idarea[0]->ID_AREA;
				// $ddata['KD_AREA']		= trim($row[1]);
				$ddata['NO_FIELD']		= $ctr;

				
				// // $ddata['OPC']		= ((int)$row[2] == 0) ?  null : (int)$row[2];
				// // $ddata['NON_OPC']	= ((int)$row[3] == 0) ?  null : (int)$row[3];
				
				// start izza 09.02.2021 data float
				$ddata['OPC']		= ((float)$row[2] == 0) ?  null : (float)$row[2];
				$ddata['NON_OPC']	= ((float)$row[3] == 0) ?  null : (float)$row[3];
				// end izza 09.02.2021 data float

				//print_r($ddata);
				//save satu persatu
				if(!$this->d_production_clinker->exists($ddata)){
					$this->d_production_clinker->insert($ddata);
					//echo $this->db->last_query() . "\n";
				}
				else{
					$where['ID_PRODUCTION_CLINKER'] = $ID_PRODUCTION;
					$where['ID_COMPANY']    = $ddata['ID_COMPANY'];//$form->ID_COMPANY;
					//$where['ID_PRODUCT']	= $ddata['ID_PRODUCT'];
					$where['ID_AREA']		= $ddata['ID_AREA'];
					#echo $this->db->last_query() . "\n";
					$this->d_production_clinker->update_where($where,$ddata);
					#echo $this->db->last_query() . "\n";
				}
						
				
								
				$ctr++;
			}
		
		
		to_json(array("result" => 'ok'));
	}

	
	// public function load_table(){
	// 	error_reporting(E_ALL);
	// 	$data = array();
	// 	$post = $this->input->post();
	// 	$form = $post['formData'];
	// 	foreach($form as $r){ 
	// 		$tmp[$r[name]] = $r[value];
	// 	}
		
	// 	//Convert array to object
	// 	$form = (object)$tmp;
	// 	$tgl  = explode("/", $form->TANGGAL);
	// 	$form->BULAN = $tgl[0];
	// 	$form->TAHUN = $tgl[1];

	// 	//Load from T_production
	// 	$data = array();
	// 	$t_prod = $this->t_production_clinker->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."'
	// 	 AND ID_COMPANY='" .$form->ID_COMPANY."' ");
	// 	//echo $this->db->last_query();die();

	// 	$area = $this->get_area_nm($form->ID_COMPANY); 
	// 	//$prod = $this->get_product_nm($form->ID_COMPANY); 
	// 	// $reseult = $this->qaf_daily->get_area_by_group(null, 4, $form->ID_COMPANY, null); 
	// 	// $produkarea = array();
	// 	// //mencari produk area
		
	// 	// foreach ($reseult as $pv):
	// 	// 	$produkarea[$pv->ID_AREA.$pv->ID_PRODUCT] = "available";
	// 	// endforeach;
	// 	//print_r($prod);
		
	// 	if (empty($t_prod)) {			
	// 		if(!$area) $area[0] = "-";
			
	// 		foreach ($area as $val):				
	// 			//$data[]= array($val[NM_AREA],$val[KD_AREA]);
	// 			$ar_tmp = array();	
	// 			$ar_tmp[] = $val[NM_AREA];
	// 			$ar_tmp[] = $val[KD_AREA];				
	// 			$data[] = $ar_tmp;
	// 		endforeach;
	// 	}else{
	// 		//if($form->ID_GROUPAREA == 4) $prod = "(NO TYPE)";
	// 		if(!$area) $area[0] = "-";
	// 		$id_tprod = $t_prod[0]->ID_PRODUCTION_CLINKER;
	// 		$id = 1;
	// 		foreach ($area as $val):
	// 			$ar_tmp = array();	
	// 			$ar_tmp[] = $val[NM_AREA];
	// 			$ar_tmp[] = $val[KD_AREA];
	// 			//print_r($val);
	// 			$rs  = $this->d_production_clinker->get_nilai($id_tprod, $id, $val[ID_AREA]);
				
	// 			$ar_tmp[] = $rs->OPC;
	// 			$ar_tmp[] = $rs->NON_OPC;
				

	// 			// $nilai  = $this->d_production_clinker->get_nilai($id_tprod, $id, 0);				
	// 			// $data[] = array($val[NM_AREA],$val[KD_AREA],$nilai->NILAI);
	// 			$data[] = $ar_tmp;
	// 			$id++;
	// 		endforeach;
	// 	}

	// 	to_json($data);
	// }

	// public function save_table(){
	// 	$post  = $this->input->post();
	// 	$form  = $post['formData'];
	// 	$data  = $post['data'];
	// 	$headertbl  = $post['headerlist'];
		
	// 	$flag_sum  = $post['jenis'];
		
	// 	foreach($form as $r){ 
	// 		$tmp[$r[name]] = $r[value];
	// 	}
		
	// 	$form = (object)$tmp;
	// 	$tgl  = explode("/", $form->TANGGAL);
	// 	$form->BULAN = $tgl[0];
	// 	$form->TAHUN = $tgl[1];
			
	// 	//T_PRODUCTION_DAILY (1): ID_PRODUCTION,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
	// 	//D_PRODUCTION_DAILY (M): ID_PRODUCTION, ID_COMPONENT, NILAI
	// 	$tdata['BULAN']	= $form->BULAN;
	// 	$tdata['TAHUN']	= $form->TAHUN;
	// 	$tdata['ID_COMPANY']	= $form->ID_COMPANY;
	// 	//$tdata['KD_AREA']	= $form->KD_AREA;


		
	// 	$ID_PRODUCTION  = $this->t_production_clinker->get_id($tdata['BULAN'],$tdata['TAHUN'],$form->ID_COMPANY);

	// 	if(!$ID_PRODUCTION){
	// 		$tdata['DATE_ENTRY'] = date("d/m/Y");
	// 		$tdata['USER_ENTRY'] = $this->USER->ID_USER;
	// 		//$tdata['KD_AREA']	= $form->ID_AREA;
	// 		$ID_PRODUCTION = $this->t_production_clinker->insert($tdata);
	// 	}
	// 	else{
	// 		$tdata['DATE_UPDATE'] = date("d/m/Y");
	// 		$tdata['USER_UPDATE'] = $this->USER->ID_USER;
	// 		//$tdata['KD_AREA']	= $form->ID_AREA;
	// 		$this->t_production_clinker->update($ID_PRODUCTION,$tdata);
	// 	}

	
	// 		$ctr = 1;
	// 		foreach($data as $y => $row){ //y index	
							
	// 			$ddata = null;
	// 			$ddata['ID_PRODUCTION_CLINKER'] = $ID_PRODUCTION;
	// 			$ddata['ID_COMPANY'] = $form->ID_COMPANY;		
	// 			$idarea = $this->m_area->getidbykode(trim($row[1]));				
	// 			$ddata['ID_AREA']		= $idarea[0]->ID_AREA;
	// 			$ddata['KD_AREA']		= trim($row[1]);
	// 			$ddata['NO_FIELD']		= $ctr;

				
	// 			$ddata['OPC']		= ((int)$row[2] == 0) ?  null : (int)$row[2];
	// 			$ddata['NON_OPC']	= ((int)$row[3] == 0) ?  null : (int)$row[3];

	// 			//print_r($ddata);
	// 			//save satu persatu
	// 			if(!$this->d_production_clinker->exists($ddata)){
	// 				$this->d_production_clinker->insert($ddata);
	// 				//echo $this->db->last_query() . "\n";
	// 			}
	// 			else{
	// 				$where['ID_PRODUCTION_CLINKER'] = $ID_PRODUCTION;
	// 				$where['ID_COMPANY']    = $form->ID_COMPANY;
	// 				//$where['ID_PRODUCT']	= $ddata['ID_PRODUCT'];
	// 				$where['ID_AREA']		= $ddata['ID_AREA'];
	// 				#echo $this->db->last_query() . "\n";
	// 				$this->d_production_clinker->update_where($where,$ddata);
	// 				#echo $this->db->last_query() . "\n";
	// 			}
						
				
								
	// 			$ctr++;
	// 		}
		
		
	// 	to_json(array("result" => 'ok'));
	// }
}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
