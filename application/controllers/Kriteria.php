<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria extends QMUser {

	public $LIST;
	public $ID_JENIS_ASPEK;
	public $JENIS_ASPEK;
	public $ID_ASPEK;
	public $ASPEK;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_aspek');
		$this->load->model('M_kriteria');
	}
	
	public function index(){
		$this->template->adminlte("v_kriteria");
	}

	public function ajax_get_aspek($id_jenis_aspek){
		$aspek = $this->M_aspek->get_where(array("ID_JENIS_ASPEK" => $id_jenis_aspek));
		to_json($aspek);
	}

	public function ajax_get_aspek_bobot($id_jenis_aspek){
		$bobot = $this->M_jenis_aspek->get_by_id($id_jenis_aspek);
		to_json($bobot);
	}

	public function add(){
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->ASPEK = $this->M_aspek->get_list();
		$this->template->adminlte("v_kriteria_add");
	}

	public function edit(){
		$this->LIST = $this->M_kriteria->get_by_id($this->input->post('ID_KRITERIA'));
		$this->ID_ASPEK = $this->M_kriteria->get_where(array("ID_ASPEK" => $this->LIST->ID_ASPEK));
		$this->ID_ASPEK = $this->ID_ASPEK[0]->ID_ASPEK;
		$this->ID_JENIS_ASPEK = $this->M_aspek->get_by_id($this->ID_ASPEK)->ID_JENIS_ASPEK;
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_kriteria_edit", $list);
	}

	public function get_list(){
		$list = $this->M_kriteria->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_KRITERIA;
			$row[] = $column->HIDDEN;
			$row[] = $no;
			$row[] = $column->JENIS_ASPEK;
			$row[] = $column->ASPEK;
			$row[] = $column->BOBOT;
			$row[] = $column->KRITERIA;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_kriteria->count_all(),
            "recordsFiltered" => $this->M_kriteria->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		//$post = array();
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);

		$this->M_kriteria->insert(array_map('strtoupper', $post));
		if($this->M_kriteria->error()){
			$this->notice->error($this->M_kriteria->error());
			redirect("kriteria/add");
		}
		else{
			$this->notice->success("Kriteria Saved.");
			redirect("kriteria");
		}
	}

	public function update(){
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);
		unset($post['ID_KRITERIA']);

		$this->M_kriteria->update($this->input->post('ID_KRITERIA'), $post);

		if($this->M_kriteria->error()){
			$this->notice->error($this->M_kriteria->error());
			redirect("kriteria/edit");
		}
		else{
			$this->notice->success("Kriteria Updated.");
			redirect("kriteria");
		}
	}

	public function remove(){
		$this->M_kriteria->delete($this->input->post('ID_KRITERIA'));
		if($this->M_kriteria->error()){
			$this->notice->error($this->M_kriteria->error());
		}
		else{
			$this->notice->success("Kriteria Removed.");
		}
		redirect("kriteria");
	}

	public function toggle_show(){
		$id = $this->input->post('ID_KRITERIA');
		$show = $this->input->post('SHOW');

		$this->M_kriteria->toggle_show($id,$show);
		if($this->M_kriteria->error()){
			$this->notice->error($this->M_kriteria->error());
		}
		else{
			$this->notice->success("Kriteria Updated.");
		}
		redirect("kriteria");
	}

}

/* End of file Kriteria.php */
/* Location: ./application/controllers/Kriteria.php */
?>