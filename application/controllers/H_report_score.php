<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class H_report_score extends QMUser {

	public $JENIS_ASPEK;
	public $LIST_INPUT;
	public $LIST_COMPANY;
	public $LIST_REPORT;
	public $ASPEK;
	public $NILAI = array();

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model('M_aspek_hebat');
		$this->load->model('M_company');
		$this->load->model('M_kriteria_hebat');
		$this->load->model('M_hebat_report');
		$this->load->model('T_cement_hourly');
	}

	public function index(){
		$this->libExternal('select2');

		$bulan 	= ($this->input->post('MONTH')==NULL) ? date("n"):$this->input->post('MONTH');
		$tahun 	= ($this->input->post('YEAR')==NULL) ? date("Y"):$this->input->post('YEAR');
		$this->ASPEK 		 = ($this->input->post('ASPEK')==NULL ? "ALL":$this->input->post('ASPEK'));
		$this->JENIS_ASPEK 	 = $this->M_aspek_hebat->get_list();
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT 	 = $this->M_kriteria_hebat->get_list($this->ASPEK);

		// var_dump($this->JENIS_ASPEK);
		$this->NILAI 		 = $this->load_nilai($this->ASPEK, $bulan, $tahun);
		 
		// print_r($this->NILAI);
		// exit();
		// var_dump($this->NILAI);
		// $this->db->last_query();
		$this->template->adminlte("v_h_report_score3");
	}

	public function global(){
		$this->libExternal('select2,highcharts');
		$this->template->adminlte("v_h_global_report_svg2");
	}

	public function scoring_bar(){
		$this->libExternal('select2,highcharts');
		$this->template->adminlte("v_h_scoring_bar");
	}

    public function grafik_skor_bar(){
// echo "string";
//     	exit;
	    $data['kategori'] = array();
		
	    $json_builder =  array();
	    $dataChart = array();
			
		$comp = $this->M_company->list_company($this->USER->ID_COMPANY);
		$cek = 0;
        $year = $this->input->post('tahun');
        $periode = $this->input->post('tipe');
           
		$this->ASPEK 		 = 'ALL';
		$this->JENIS_ASPEK 	 = $this->M_aspek_hebat->get_list();
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT 	 = $this->M_kriteria_hebat->get_list($this->ASPEK);

		$data = array();
      	foreach ($this->LIST_COMPANY as $company) {
    		$row['name'] = $company->NM_COMPANY;
			$row['type'] = 'column';

			$valByMonth = array();
        	for ($x = 1; $x <= 12; $x++) {
				$this->NILAI 		 = $this->load_nilai($this->ASPEK, $x, $year);

                $total  = 0;
                $arr_k  = array_keys($this->NILAI['comp']);
                $nilai  = (in_array($company->ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$company->ID_COMPANY]:array(0);
                if ($nilai[0]!==0) {
                  foreach ($nilai as $nl) {
                    $skor = $nl->SCORE*$nl->BOBOT;
                    $total += round($skor,2);
                  }
                }else{
                  $total = 0;
                }
				$valByMonth[] = $total;
          	}
			$row['data'] = $valByMonth;

			$data['grafik'][] = $row;
		}


		if($periode == 1){
	    	for ($x = 1; $x <= 12; $x++) {
				$data['kategori'][] = $x.'-'.$year;
			}
			to_json($data);
		}else{
			$jum_periode = 0;
             $total = 0;
            for ($x = 1; $x <= 12; $x++) {
                $total += 1;
                if($total==$periode){
                  $jum_periode +=1;
                  $data['kategori'][] = "Periode ".$jum_periode;
                  $total = 0;
                }
            }
            
            $json = $data['grafik'];
            $json_builder = array();
            $dataChart = array();
            $jum_periode1 = 0;
            $total1 = 0;
            $all = 0;

        	$row = array();
            foreach($json as $key => $val){ 
                $dataChart['name'] = $val['name'];	
                $dataChart['type'] = 'column';
                
                foreach($json[$key]["data"] as $key1 => $val_tanggal){
                    $total1 += 1;
                    $all += $val_tanggal;
                    if($total1==$periode){
                      $hasil = $all/$periode;  
                      $row['data'][] = floatval(round($hasil,2));
                      $total1 = 0;
                      $hasil = 0;
                      $all = 0;
                    }
                }
				$dataChart['data'] = $row['data'];
				$json_builder[] = $dataChart;
				array_splice($row['data'], 0);                        
            }
	        echo json_encode(
	            array(
	                'grafik' => $json_builder,
	                'kategori' => $data['kategori'],
	            )
	          );
		}


	}
	    

	public function global_json(){
		$bulan 	= ($this->input->post('MONTH')==NULL) ? date("n"):$this->input->post('MONTH');
		$tahun 	= ($this->input->post('YEAR')==NULL) ? date("Y"):$this->input->post('YEAR');
		$this->ASPEK 		 = ($this->input->post('ASPEK')==NULL ? "ALL":$this->input->post('ASPEK'));
		$this->JENIS_ASPEK 	 = $this->M_aspek_hebat->get_list();
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT 	 = $this->M_kriteria_hebat->get_list($this->ASPEK);
		$this->NILAI 		 = $this->load_nilai($this->ASPEK, $bulan, $tahun);

		// print_r($this->LIST_COMPANY);
		// exit;
		$data = array();
		$odd  = 1;
		$actplan = array();
		foreach ($this->LIST_COMPANY  as $key => $company) {
	        $total  = NULL;
	        $action = '';
	        $arr_k  = array_keys($this->NILAI['comp']);
	        $nilai  = (in_array($company->ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$company->ID_COMPANY]:array(0);
// print_r($nilai);
	        if ($nilai[0]!==0) {
	        	$actionSkor = 10000;
		        $actionKriteria = '';
	          	foreach ($nilai as $nl) {
	                if($nl->ID_ASPEK == 1){
                      $skor = $nl->TOTAL_POINT;
                    }else{
                      $skor = $nl->SCORE*$nl->BOBOT;
                    }

		            // $skor = $nl->SCORE*$nl->BOBOT;

		            // echo $nl->SCORE. '-'.$nl->ID_KRITERIA.' - ';
		            $total += round($skor,2);
		            
		            if($nl->ID_KRITERIA == 1){
				    	$actionKriteriaCustom = 'Optimized NCQR Report';
		            }
		            if($nl->ID_KRITERIA == 2){
				    	$actionKriteriaCustom = 'Optimized Data on QM';
		            }
		            if($nl->ID_KRITERIA == 3){
				    	$actionKriteriaCustom = 'Increase QAF of ST';
		            }
		            if($nl->ID_KRITERIA == 4){
				    	$actionKriteriaCustom = 'Increase QAF of CS';
		            }
		            // $action = $actionKriteria;
        			// $actplan[$nl->ID_COMPANY][] = array($nl->SCORE, $actionKriteriaCustom);
		            if($nl->SCORE <= 3){
	            		$actplan[$company->ID_COMPANY][] = ' - ' . $actionKriteriaCustom. ' ('.$nl->SCORE.')';
		            }
            		// $action .= '- '.$actionKriteriaCustom.'<br>';
	         	}
	        }else{
	          $total = NULL;
	        }

	        $data[$key]['TOT_SCORE']  = $total;
	        $data[$key]['ID_COMPANY'] = $company->ID_COMPANY;
	        $data[$key]['NM_COMPANY'] = $company->NM_COMPANY;
	        $data[$key]['KD_COMPANY'] = $company->KD_COMPANY;

	        $listAct = '';
	        foreach ($actplan[$company->ID_COMPANY] as $key => $value) {
		        $listAct .= $value.'<br>';
	        }
	        $data[$key]['ACTION_PLAN'] = $listAct;
	        $data[$key]['POS_X'] = ($odd % 2 == 0) ? 0.85:0.15;
	        $data[$key]['POS_Y'] = ($odd % 2 == 0) ? 1:0.2;

	        $odd++;
	    }

// print_r($data);exit;

	    $tmp['sort'] 		= TRUE;
	    $tmp['textfont'] 	= array('size' => 16);
	    /*$tmp['marker'] 		= array('colors' => array(
	    						0 => 'rgb(255, 0, 0)',
	    						1 => '',
	    						2 => '',
	    						3 => '',
	    					));*/

	    usort($data, function($a, $b) {
		    return (float) $b['TOT_SCORE'] <=> (float) $a['TOT_SCORE'];
			});

	    $rank = 1;
    	$newTmps = array();
    	$newTmpAPs = array();

    	// print_r($data);
    	// exit;
	    foreach ($data as $i => $res) {
	    	$newTmp = array();
	    	$newTmpAP = array();
	    	#$tmp['labels'][] = short_opco($res['KD_COMPANY']) . "<br>" . $res['TOT_SCORE'];
	    	#$tmp['values'][] = 25;
	    	$shortOpco = $this->shortOpco($res['KD_COMPANY']);
	    	$tmp['labels'][]  = $shortOpco;
	    	$tmp['values'][]  = (is_numeric($res['TOT_SCORE'])) ? round($res['TOT_SCORE'],2):$res['TOT_SCORE'];
	    	$tmp['warna'][]   = getRankColor($rank);
	    	$tmp['rank'][]    = $rank;
	    	$tmp['act_plan'][] = $res['ACTION_PLAN'];

	    	$ano['xref'] = 'x';
	    	$ano['yref'] = 'y';
	    	$ano['text'] = "<b>ACTION PLAN</b>" . $res['ACTION_PLAN'];
	    	$ano['y'] = $res['POS_Y'];
	    	$ano['x'] = $res['POS_X'];
	    	$ano['font'] = array('size' => 12);
	    	$ano['showarrow'] = false;


	    	$newTmp['y']  = 5;
	    	$newTmp['name']  = $shortOpco;
	    	$newTmp['value']  = ($res['TOT_SCORE'] != null ? (is_numeric($res['TOT_SCORE'])) ? round($res['TOT_SCORE'],2) : $res['TOT_SCORE'] : 0);

	    	// $newTmp['color']  = 'Highcharts.getOptions().colors['.($i+3).']';
	    	// $newTmp[$i]   = getRankColor($rank);
	    	$newTmps[] = $newTmp;

	        $listAct = '';
	        foreach ($actplan[$res['ID_COMPANY']] as $key => $value) {
		        $listAct .= $value.'<br>';
	        }

    		$newTmpAPs[] = $listAct;

	    	$anno[] = $ano;
	    	$rank++;
	    }


	    $tmp['direction'] 	= 'counterclockwise';
	    $tmp['hoverinfo'] 	= 'label+text';
	    $tmp['textinfo'] 	= 'label';
	    $tmp['type'] 		= 'pie';
	    $tmp['name'] 		= 'VAL';

	    $lay['layout']		= array(
	    	"autosize" => true,
		    "title" => "GLOBAL REPORT",
		    "showlegend" => false,
		    "hovermode" => "closest",
		    "margin"  => array("r" => 0, "l" => 100),
		    "annotations" => $anno
	    );

	    $pie['data'] = array($tmp);
	    $pie['layout'] = $lay;

	    // to_json($pie);

	    $newPie = array();
	    $newPie = array('pie' => $newTmps, 'act_plan' => $newTmpAPs);

	    // exit;
	    to_json($newPie);
	    // to_json($newTmpAPs);

	}

	private function shortOpco($opcoCode){
		$opco = '';
		if($opcoCode == 7000 || $opcoCode == 2000){
			$opco = 'SIBU';
		}elseif($opcoCode == 3000){
			$opco = 'SP';
		}elseif($opcoCode == 4000){
			$opco = 'ST';
		}elseif($opcoCode == 6000){
			$opco = 'TLCC';
		}elseif($opcoCode == 5000){
			$opco = 'SG';
		}

		return $opco;
	}

	public function load_nilai($aspek, $bulan, $tahun){
		$com_id = array();
		$jaspek = array();
		$result = array();
		$cek_data = array();
		$load = $this->M_hebat_report->v_skoring_hebat2($aspek, $bulan, $tahun);
		// echo $this->db->last_query();
		
		$load_tes = $this->M_hebat_report->v_skoring_hebat1($aspek, $bulan, $tahun);
		// echo $this->db->last_query();

		//Group by company
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			if(isset($com_id[$id_com])){
				$com_id[$id_com][] = $data;
			}else{
				$com_id[$id_com] = array($data);
			} 
		}

		//Group by jenis aspek
		foreach ($load as $data) {
			$aspekj = $data->ID_ASPEK;
			if(isset($jaspek[$aspekj])){
				$jaspek[$aspekj][] = $data;
			}else{
				$jaspek[$aspekj] = array($data);
			}
		}

		//Group by cek data inputan
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			$CEK 	= $this->M_hebat_report->get_cek_input($id_com, $bulan, $tahun);
			$cek_data[$id_com] = $CEK;
			// var_dump($CEK);
			// echo $this->db->last_query();
		}

		$result['comp']		= $com_id;
		$result['jaspek']	= $jaspek;
		$result['cek_data']	= $cek_data;
		$result['load']	= $load;
		return $result;
	}
	
	public function qmodashboard(){
		
		$bulan 	= date("n");
		$tahun 	= date("Y");
		$this->ASPEK 		 = 2;
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->NILAI 		 = $this->load_nilai(2, $bulan, $tahun);
		 
				$dtCompany = array();
				$listCompany = array();
                  foreach ($this->LIST_COMPANY as $company) {
                    $total  = 0;
                    $arr_k  = array_keys($this->NILAI['comp']);
                    $nilai  = (in_array($company->ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$company->ID_COMPANY]:array(0);
					//var_dump($nilai);
					
                    if ($nilai[0]!==0) {
                      foreach ($nilai as $nl) {
                        $skor = $nl->SCORE*$nl->BOBOT;
                        $total += round($skor,2);
                      }
                    }else{
                      $total = 0;
                    }
					
					$nilaiskor = ((float)$nilai[0]->SCORE / 5) * (int)$nilai[0]->BOBOT;
					
                    $listCompany = array(
						'COMPANY' 	=> $company->NM_COMPANY,
						'POSISI' 		=> (float)$nilai[0]->SCORE,
						'SKOR' 			=> $nilaiskor
					);
					
					array_push($dtCompany,$listCompany);
                  }
                  ksort($dtCompany);

				  to_json($dtCompany);
	}
	
	function aasort (&$array, $key) {
		$sorter=array();
		$ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
		}
		arsort($sorter);
		foreach ($sorter as $ii => $va) {
			$ret[$ii]=$array[$ii];
		}
		$array=$ret;
		
		return $array;
	}
		

}

/* End of file H_report_score.php */
/* Location: ./application/controllers/H_report_score.php */
?>
