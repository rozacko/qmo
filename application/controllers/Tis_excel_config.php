<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tis_excel_config extends QMUser {

    public function __construct(){
        parent::__construct();
        $this->load->model("m_grouparea");
        $this->load->model("m_plant");
        $this->load->model('m_tis_excel_config');
    }

    public function index(){
        $this->dataconfig = $this->m_tis_excel_config->datalist();
        $this->template->adminlte("v_tis_excel_config");
    }

    public function add(){
        $this->list_plant = $this->m_plant->datalist(array(12, 41));
        $this->list_grouparea = $this->m_grouparea->datalist_auth();
        $this->template->adminlte("v_tis_excel_config_add");
    }

    public function edit($id){
        $this->tis_excel = $this->m_tis_excel_config->detail($id);
        $this->list_plant = $this->m_plant->datalist(array(12, 41));
        $this->list_grouparea = $this->m_grouparea->datalist_auth();
        $this->template->adminlte("v_tis_excel_config_edit");
    }

    public function action_add(){
        $data['ID_PLANT'] = $this->input->post('ID_PLANT');
        $data['ID_GROUPAREA'] = $this->input->post('ID_GROUPAREA');
        $data['URL'] = $this->input->post('URL');

        $save_act = $this->m_tis_excel_config->insert($data);
        if($save_act){
			$this->notice->success("Data Created.");
			redirect("tis_excel_config");  
		} else {
			$this->notice->error($this->m_tis_excel_config->error());
			redirect("tis_excel_config");
		}
    }

    public function action_edit($id){
        $data['ID_PLANT'] = $this->input->post('ID_PLANT');
        $data['ID_GROUPAREA'] = $this->input->post('ID_GROUPAREA');
        $data['URL'] = $this->input->post('URL');

        $save_act = $this->m_tis_excel_config->update($data, $id);
        if($save_act){
			$this->notice->success("Data Updated.");
			redirect("tis_excel_config");  
		} else {
			$this->notice->error($this->m_tis_excel_config->error());
			redirect("tis_excel_config");
		}
    }

    public function action_delete($id){

        $save_act = $this->m_tis_excel_config->delete($id);
        if($save_act){
			$this->notice->success("Data Deleted.");
			redirect("tis_excel_config");  
		} else {
			$this->notice->error($this->m_tis_excel_config->error());
			redirect("tis_excel_config");
		}
    }
}