<?php

class Menu extends QMUser {
	
	public $list_menu = array();
	public $list_groupmenu = array();
	public $data_menu;
	public $id_groupmenu;
	
	public function __construct(){
		parent::__construct();
		error_reporting(0);
		$this->load->helper("string");
		$this->load->model("m_menu");
		$this->load->model("m_groupmenu");
	}
	
	public function index(){
		$this->list_menu = $this->m_menu->datalist(); #var_dump($this->list_menu);
		$this->list_groupmenu = $this->m_groupmenu->datalist(); #var_dump($this->list_menu);
		$this->template->adminlte("v_menu");
	}
	
	public function add($id_grp){
		$this->id_groupmenu = $id_grp;
		$this->list_groupmenu = $this->m_groupmenu->datalist(); #var_dump($this->list_menu);
		$this->template->adminlte("v_menu_add");
	}
	
	public function create(){
		$this->m_menu->insert($this->input->post());
		if($this->m_menu->error()){
			$this->notice->error($this->m_menu->error());
			redirect("menu/add");
		}
		else{
			$this->notice->success("Menu Created");
			redirect("menu");
		}
	}

	public function create_v2(){
		$this->m_menu->insert($this->input->post());
		if($this->m_menu->error()){
			$status = false;
			$msg = $this->m_menu->error();
		}
		else{
			$status = true;
			$msg = 'Sub Menu Data Saved.';
		}

		$output = array(
			'status' => $status,
			'msg' => $msg
		);

		to_json($output);
	}
	
	public function edit($ID_MENU){
		$this->list_groupmenu = $this->m_groupmenu->datalist();
		$this->data_menu = $this->m_menu->get_data_by_id($ID_MENU);
		$this->template->adminlte("v_menu_edit");
	}
	
	public function update($ID_MENU){
		$this->m_menu->update($this->input->post(),$ID_MENU);
		if($this->m_menu->error()){
			$this->notice->error($this->m_menu->error());
			redirect("menu/edit/".$ID_MENU);
		}
		else{
			$this->notice->success("Menu Updated");
			redirect("menu");
		}
	}

	public function update_v2($ID_MENU){
		$this->m_menu->update($this->input->post(),$ID_MENU);
		if($this->m_menu->error()){
			$status = false;
			$msg = $this->m_menu->error();
		}
		else{
			$status = true;
			$msg = 'Sub Menu Data Saved.';
		}

		$output = array(
			'status' => $status,
			'msg' => $msg
		);

		to_json($output);
	}
	
	public function delete($ID_MENU){
		$this->m_menu->delete($ID_MENU);
		if($this->m_menu->error()){
			$this->notice->error($this->m_menu->error());
		}
		else{
			$this->notice->success("Menu Deleted.");
		}
		redirect("menu");
	}

	public function delete_v2($ID_MENU){
		$this->m_menu->delete($ID_MENU);
		if($this->m_groupmenu->error()){
			$status = false;
			$msg = $this->m_groupmenu->error();
		}
		else{
			$status = true;
			$msg = 'Sub Menu Data Saved.';
		}

		$output = array(
			'status' => $status,
			'msg' => $msg
		);

		to_json($output);
	}

	public function get_list(){
		$list = $this->m_menu->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_MENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NM_MENU; 
			$row[] = $column->URL_MENU;
			$row[] = $column->NO_ORDER;
			$row[] = $column->ACTIVE;
			$row[] = $column->ID_GROUPMENU;
			$row[] = $column->PARENT;

			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_menu->count_all(),
            "recordsFiltered" => $this->m_menu->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function setMenu(){
		$post = $this->input->post();

        $menu->PARENT = $post['parent'];
        $menu->CHILD = $post['child'];
        // $menu->PARENT = 1;
        // $menu->CHILD = 1;
// var_dump($menu);
// exit;
        $this->session->set_userdata("MENU");
        $this->session->set_userdata('MENU', $menu);
	}

}	

?>
