<?php

class Sync_tis_auto extends Home {

    public function __construct(){
		parent::__construct();
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
        $this->load->helper("string");
        $this->load->model("M_sync_tis");
    }

    public function do_sync(){

        $type_sync = "CRONJOB";
        $filename = "TEMPLATE_GRABING15.xlsx";
    	$inputFileName = './assets/uploads/proficiency/'.$filename;
    	
    	try {
    		$inputFileType = IOFactory::identify($inputFileName);
    		$objReader = IOFactory::createReader($inputFileType);
    		$objPHPExcel = $objReader->load($inputFileName);
    	} catch(Exception $e) {
    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
    	}

    	$sheet = $objPHPExcel->getSheet(0);
    	$highestRow = $sheet->getHighestRow();
    	$highestColumn = $sheet->getHighestColumn();

    	$company = $this->M_sync_tis->get_company();
    	$plant = $this->M_sync_tis->get_plant();
    	$group_area = $this->M_sync_tis->get_group_area();
    	$area = $this->M_sync_tis->get_area();
    	$product = $this->M_sync_tis->get_product();
    	$component = $this->M_sync_tis->get_component();
        $insert = 0;
        $update = 0;

    	$headerData = $sheet->rangeToArray('G1:'.$highestColumn.'1', NULL, TRUE, FALSE); //GET COMPONENT IN EXCEL
    	$i = 1;
    	for ($row = 2; $row <= $highestRow; $row++){
            $j = 1;
    		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
    		$c = $this->searchForId($rowData[0][0], 'NM_COMPANY', $company);
    		$p = $this->searchForId($rowData[0][1], 'NM_PLANT', $plant);
    		$ga = $this->searchForId($rowData[0][2], 'NM_GROUPAREA', $group_area);
    		$a = $this->searchForId($rowData[0][3], 'NM_AREA', $area);
    		$pr = $this->searchForId($rowData[0][4], 'KD_PRODUCT', $product);


    		$data = array(
    			"ID_COMPANY" => $company[$c]['ID_COMPANY'],
    			"ID_PLANT" => $plant[$p]['ID_PLANT'],
    			"ID_GROUPAREA" => $group_area[$ga]['ID_GROUPAREA'],
    			"ID_AREA" => $area[$a]['ID_AREA'],
    			"ID_PRODUCT" => $product[$pr]['ID_PRODUCT'],
    			"TYPE" => $rowData[0][5]
    		);

            $check_existing = $this->M_sync_tis->get_data_t_prod_tis($data, $rowData[0][6]);
            if($check_existing){
                $update++;
                $do_insert_t_prod_tis = $check_existing[0]['ID_PRODUCTION_TIS'];
                $do_update_t_prod_tis = $this->M_sync_tis->update_data_t_prod_tis($data, $rowData[0][6], $check_existing[0]['ID_PRODUCTION_TIS']);

                $id = $do_update_t_prod_tis;
            } else {
                $insert++;
                $do_insert_t_prod_tis = $this->M_sync_tis->add_data_t_prod_tis($data, $rowData[0][6]);
                $id = $do_insert_t_prod_tis->ID_PRODUCTION_TIS;
            }

            $this->M_sync_tis->delete_data_d_prod_tis($id);

    		for($col = 'H'; $col <= $highestColumn; $col++){
    			if($j < (count($component) + 1)){
    				$value_compt = $objPHPExcel->getActiveSheet()->getCell($col.$row)->getValue();
    				$comp = $this->searchForId(trim(strtoupper($headerData[0][$j])), 'KD_COMPONENT', $component);
    				if($comp != null){
    					$data_d = array(
	    					"ID_PRODUCTION_TIS" => $id,
	    					"ID_COMPONENT" => $component[$comp]['ID_COMPONENT'],
	    					"NILAI" => $value_compt,
	    					"NO_FIELD" => ($j - 1)
	    				);
	                    $collect_data_d[] = $data_d;
    				}                    	
    			}
    			
                $j++;
    		}

            $i++;
    	}

        $do_insert_d_prod_tis = $this->M_sync_tis->add_data_d_prod_tis($collect_data_d);

        $data_sync = array(
            "TYPE" => $type_sync,
            "USER_SYNC" => "OTOMATIS", 
            "TOTAL_ADD_DATA" => $insert,
            "TOTAL_UPDATE_DATA" => $update
        );

        $do_insert_sync_tis = $this->M_sync_tis->add_data_sync_tis($data_sync);

        if($do_insert_sync_tis){
            to_json(array("status" => "200", "data" => $data_sync));
        } else {
            to_json(array("status" => "404"));
        }
        
    }

    public function searchForId($id, $colum, $array) {
    	foreach ($array as $key => $val) {
    		if ($val[$colum] === $id) {
    			return $key;
    		}
    	}
    	return null;
    }
}