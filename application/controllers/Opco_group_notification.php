<?php

class Opco_group_notification extends QMUser {
	
	public $list_data = array();
	public $list_jabatan = array();
	public $data_opco;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_opco");
		$this->load->model("m_company");
		$this->load->model("m_jabatan");
		$this->load->model("employee");
		$this->load->model("m_user");
		$this->load->model("m_roles");
		$this->load->model("NotificationGroup");
		$this->load->library('email');
	}
	
	public function listHris(){
		exit;
		$a = $this->employee->get_list();
		// echo "<pre>";
		// print_r($a);
		// echo "</pre>";

		foreach ($a as $key => $value) {
			if(strlen($value->MK_NOPEG) == 1){
				$value->MK_NOPEG = '0000000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 2){
				$value->MK_NOPEG = '000000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 3){
				$value->MK_NOPEG = '00000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 4){
				$value->MK_NOPEG = '0000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 5){
				$value->MK_NOPEG = '000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 6){
				$value->MK_NOPEG = '00'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 7){
				$value->MK_NOPEG = '0'.$value->MK_NOPEG;
			}

			$this->db->set('EMAIL', "'".$value->mk_email."'", FALSE);
			// $this->db->set('NOPEG', "'".$value->MK_NOPEG."'", FALSE);
			$this->db->where('USERNAME', $value->USERNAME);
			$this->db->update('M_USERS'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
			// echo $this->db->last_query().'<br>';
		}

	}

	public function listAccess(){
		// echo "<pre>";
		// print_r($a);
		// echo "</pre>";

		exit;

		$a = $this->m_user->getData();
		foreach ($a as $i => $v) {

			$roles = array();
			$roles[0]['ID_USER'] = $v['ID_USER'];
			$roles[0]['ID_USERGROUP'] = $v['ID_USERGROUP'];
			$this->m_roles->insertBatch($roles);

		}

	}
	
	public function index(){
		$this->libExternal('dataTables');
		// $this->list_opco = $this->m_opco->datalist()->result();

		$this->template->adminlte("v_opco_group_notification");
	}

	public function getDataList(){
		
		// exit;
		$list = $this->NotificationGroup->datalist()->result_array();
		// echo $this->db->last_query();
		foreach ($list as $k => $v) {
			$list[$k]['NO'] = ($k +1);
		}

		$listData = array('data' => $list);
		echo json_encode($listData);
	}

	public function add(){
		$this->libExternal('dataTables');

		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_group_notification_add");
	}
	
	public function create(){
        $post = $this->input->post();

        if ( TRUE === $this->NotificationGroup->check_duplicate($post['ID_AREA'])) {
            echo "3";
            return;
        }

		$this->NotificationGroup->insert($post);

		$status = 1;

		if($this->NotificationGroup->error()){
			$status = 0;	
		}

		echo $status;
	}
	
	public function edit($ID_OPCO){
		$this->libExternal('dataTables');
		
		$this->data_opco = $this->NotificationGroup->get_data_by_id($ID_OPCO);
		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_group_notification_edit");
	}
	
	public function update($ID_OPCO_NOTIFICATION_GROUP){
        $post = $this->input->post();

        if ( TRUE === $this->NotificationGroup->check_duplicate($post['ID_AREA'], $ID_OPCO_NOTIFICATION_GROUP)) {
            echo "3";
            return;
        }

		$this->NotificationGroup->update($this->input->post(),$ID_OPCO_NOTIFICATION_GROUP);

		if($this->NotificationGroup->error()){
			$this->notice->error($this->NotificationGroup->error());
		}
		else{
			$this->notice->success("Opco Notification Group Data Updated.");
		}

		redirect("opco_group_notification/edit/".$ID_OPCO_NOTIFICATION_GROUP);
	}
	
	public function delete($ID_OPCO){
		$this->NotificationGroup->delete($ID_OPCO);
		if($this->NotificationGroup->error()){
			$this->notice->error($this->NotificationGroup->error());
		}
		else{
			$this->notice->success("Opco Data Removed.");
		}
		redirect("opco_group_notification");
	}
	
	public function notification_member($ID_AREA=NULL,$ID_JABATAN=NULL){
		// echo json_encode($this->NotificationGroup->notification_member($ID_AREA,$ID_JABATAN));
        echo '[]';
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}

	public function get_username_registered(){
		$username = $this->input->post('username');
		$get = $this->m_user->get_username_registered(strtoupper($username));
		to_json($get);
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
		to_json($area);
	}
	
}	

?>
