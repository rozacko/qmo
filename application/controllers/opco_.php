<?php

class opco_ extends QMUser {
	
	public $list_data = array();
	public $list_jabatan = array();
	public $data_opco;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_opco");
		$this->load->model("m_company");
		$this->load->model("m_jabatan");
		$this->load->model("employee");
	}
	
	public function index(){
		$this->list_opco = $this->m_opco->datalist();
		$this->template->adminlte("v_opco");
	}
	
	public function add(){
		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_add");
	}
	
	public function create(){
		$this->m_opco->insert($this->input->post());
		if($this->m_opco->error()){
			$this->notice->error($this->m_opco->error());
			redirect("opco/add");
		}
		else{
			$this->notice->success("Opco Data Saved.");
			redirect("opco");
		}
	}
	
	public function edit($ID_OPCO){
		$this->data_opco = $this->m_opco->get_data_by_id($ID_OPCO);
		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_edit");
	}
	
	public function update($ID_OPCO){
		$this->m_opco->update($this->input->post(),$ID_OPCO);
		if($this->m_opco->error()){
			$this->notice->error($this->m_opco->error());
			redirect("opco/edit/".$ID_OPCO);
		}
		else{
			$this->notice->success("Opco Data Updated.");
			redirect("opco");
		}
	}
	
	public function delete($ID_OPCO){
		$this->m_opco->delete($ID_OPCO);
		if($this->m_opco->error()){
			$this->notice->error($this->m_opco->error());
		}
		else{
			$this->notice->success("Opco Data Removed.");
		}
		redirect("opco");
	}
	
	public function notification_member($ID_AREA=NULL,$ID_JABATAN=NULL){
		echo json_encode($this->m_opco->notification_member($ID_AREA,$ID_JABATAN));
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
		to_json($area);
	}
	
}	

?>
