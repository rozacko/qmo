<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_pengujian extends QMUser {

    public $list_form = array();
    public $list_komoditi = array();

    public function __construct(){
        parent::__construct();
        $this->load->helper("string");
        $this->load->model("m_form_pengujian");
        $this->load->model("m_product");
    }

    public function index(){
        $this->list_komoditi = $this->m_form_pengujian->list_komoditi();
		$this->template->adminlte("v_form_pengujian");
    }
    
    public function get_list(){
        $list = $this->m_form_pengujian->get_list();
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["ID_FORM"] = $column->ID_FORM;
			$row["TITLE"] = $column->TITLE;
			$row["NAMA_SAMPLE"] = $column->NAMA_SAMPLE;
			$row["TIPE"] = $column->TIPE;
			$row["IS_REQUIRED"] = $column->IS_REQUIRED;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_form_pengujian->count_all(),
            "recordsFiltered" => $this->m_form_pengujian->count_filtered(),
            "data" => $data,
        );

		to_json($output);
    }

    public function detail($ID_FORM){
        $list = $this->m_form_pengujian->detail($ID_FORM);
        to_json($list);
    }

    public function create(){
		$this->m_form_pengujian->insert($this->input->post());
		if($this->m_form_pengujian->error()){
			$status = false;
		}
		else{
            $status = true;
        }
        
        echo $status;
    }

    public function update($ID_FORM){
		$this->m_form_pengujian->update($this->input->post(), $ID_FORM);
		if($this->m_form_pengujian->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
	}
    
    public function delete($ID_FORM){
		$this->m_form_pengujian->delete($ID_FORM);
		if($this->m_form_pengujian->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
	}
}