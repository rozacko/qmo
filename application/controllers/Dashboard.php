<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use SAPNWRFC\Connection as SapConnection;
		use SAPNWRFC\Exception as SapException;
class Dashboard extends QMUser {
	
	var $list_company = array();
	var $list_grouparea = array();
	var $list_area = array();
	var $list_data = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_access_log");
		$this->load->model("m_company");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("treport");
		
		// Grafik Ketepatan
		$this->load->model("m_ketepatan"); 
		
	}

	public function index(){
		$this->libExternal('select2,datepicker');
		$this->list_company 	= $this->m_company->list_company();
		$this->list_grouparea 	= $this->m_grouparea->datalist();
		$this->list_area 		= $this->m_area->datalist();
		$this->template->adminlte("v_dashboard2");
		#$this->access_log();
	}
	
	
	
	public function dashboard(){
		
	}
	
	public function get_plant(){
		$list_plant = $this->m_plant->datalist($this->input->post("ID_COMPANY")); #echo $this->m_plant->get_sql();
		echo json_encode($list_plant);
	}
	
	public function daily(){
		
		
		
		$plants = $this->input->post("ID_PLANT");
		$data   = $this->input->post();
		
		if(!$data or !$plants){
			die("No data selected");
		}
		
		foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
			$this->list_grouparea[] = $this->m_grouparea->get_data_by_id($ID_GROUPAREA);
		endforeach;
		
		foreach($plants as $ID_COMPANY => $ID_PLANTS):
			foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
				$i = 0;

				// var_dump($ID_PLANTS);
				foreach($ID_PLANTS as $ID_PLANT):	
					$r = @$this->treport->dashboard_daily($data['STARTDATE'],$data['ENDDATE'],$ID_GROUPAREA,$ID_COMPANY,$ID_PLANT);	
					// echo $this->treport->get_sql().'<br><br>';
					$nm_company	= $this->m_company->get_data_by_id($ID_COMPANY)->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$ID_COMPANY,$ID_PLANT)->JML_AREA_PRODUKSI;

					$status = "D";
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					} 
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($ID_PLANT)->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
					$i++;
					
				endforeach;
			endforeach;
		endforeach; #
		// var_dump($d);
		$this->list_data = $d;
		$this->template->nostyle("v_dashboard_daily");
	}
	
	public function hourly(){		
		
		
		$plants = $this->input->post("ID_PLANT");
		$data   = $this->input->post();
		
		if(!$data or !$plants){
			die("No data selected");
		}
		
		foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
			$this->list_grouparea[] = $this->m_grouparea->get_data_by_id($ID_GROUPAREA);
		endforeach;
		
		foreach($plants as $ID_COMPANY => $ID_PLANTS):
			foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
				$i = 0;
				foreach($ID_PLANTS as $ID_PLANT):	
					$r = @$this->treport->dashboard_hourly($data['STARTDATE'],$data['ENDDATE'],$ID_GROUPAREA,$ID_COMPANY,$ID_PLANT);
					// echo $this->db->last_query().'<br><hr>';
					$nm_company = $this->m_company->get_data_by_id($ID_COMPANY)->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$ID_COMPANY,$ID_PLANT)->JML_AREA_PRODUKSI;
					
					$status = "D";  
					
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					} 
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($ID_PLANT)->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA]['TANGGAL_DATA'] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA]['TANGGAL_ENTRI'] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA]['STATUS'] 		= $status; 
					
					$i++;
					
				endforeach;
			endforeach;
		endforeach; #
		$this->list_data = $d;
		$this->template->nostyle("v_dashboard_daily");
	}
	
	
	public function access_log(){
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_access_log");
		$this->list_company = $this->m_company->list_company();
		
		//print_r($this->list_company);
		//exit();
		
		//$this->topGroupmenu = $this->m_access_log->topGroupmenu(date("m-Y"));
		//$this->topMenu 			= $this->m_access_log->topMenu(date("m-Y"));
		$this->topGroupmenu = $this->m_access_log->topUserQA();
		$this->topMenu 			= $this->m_access_log->topUserQC();
		//$this->topUserList  = $this->m_access_log->topUser();
		$this->topUserList  = $this->m_access_log->topUserNonQAQC();

		$this->template->adminlte("v_access_log_dashboard");
	}
	
	// -------------------------- start izza 17.05.21 --------------------------
	public function sapconfig(){
		// $config = $this->sap_config(); // prod
		$config = $this->sap_config('DEV'); // dev
		$tgl = ['20170330','20200130','20201016'];
    try {
			$sapi = new SapConnection($config);
					// $sapi = new SapConnection($config_prod);
			
					$rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
		
					if ($rfc == TRUE) {
						$date_start = '12/30/2020';
						$date_now = date('Ymd', strtotime($date_start));
						// $date_now = date('Ymd');
										$code_mat = '112-100-0009';
						$options = [
							'rtrim' => true
						];
						$rfc->setParameterActive('R_MATNR', true);
						$rfc->setParameterActive('R_PERIOD', true);
						$R_PERIOD = array();
						foreach ($tgl as $key) {
							$temp = ['SIGN' 	=> 'I', 'OPTION'=> 'EQ', 'HIGH' 	=> $key, 'LOW' 	=> $key];
							$R_PERIOD[] = $temp;
						}
						$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' => $R_PERIOD
						];
						$result = $rfc->invoke($parms, $options);
						$fromsap = count($result['R_RESULT']);
										
						// $action = $this->qbb->data_from_sap($result['R_RESULT']);
						// $existing = $fromsap-$action;
						// echo $fromsap;
						echo json_encode($result);		
						// echo "Syny date: ".date('d-m-Y')." => Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing;
				}
					//$rfc->Close();
					//$sapi->Close();
        } catch(SapException $ex) {
            echo 'Exception: ' . $ex->getMessage() . PHP_EOL;
        }
  } 
	public function sap_config($server = 'PROD'){
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_prod = [
			'ashost' => '10.15.4.200', // update by izza 17 mei 2021
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		if($server == 'PROD'){$config = $config_prod;}
		else{ $config = $config_dev;}
		return $config;
  } 
	// -------------------------- end izza 17.05.21 --------------------------
	// hit baru di dashboard
	
	public function new_hit_opco(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
			$opco  = array();
			$hasil = $this->m_access_log->aksesqmoDate(date("m"), date("Y"), date("m/Y"));		
			//echo $this->db->last_query();
			print_r($hasil);
			exit();
			//Group by company
			foreach ($hasil as $cp) {
				$id_com = $cp->ID_COMPANY;
				if(isset($opco[$id_com])){
					$opco[$id_com][] = (array) $cp;
				}else{
					$opco[$id_com] = array((array) $cp);
				}
			}


			usort($opco, function($a, $b) {
				return strtotime($a['TANGGAL']) - strtotime($b['TANGGAL']);
			});

			//Generate plotly data
			foreach ($opco as $key => $val) {
				foreach ($val as $dt) {
					$color = company_color_rgb2($dt['KD_COMPANY']);
					$res[$key]['date'][] = $dt['TANGGAL'];
					$res[$key]['data'][] = $dt['JML_AKSES'];
					$res[$key]['name'] = $dt['COMPANY'];
					$res[$key]['color'] = "rgba($color,2)";
					$sort[$key] = strtotime($dt['TANGGAL']);
				}
			}
			$cek = count($opco);
			if ($cek > 0) {
				array_multisort($sort, SORT_DESC, $opco);
			}
			
			$data['result'] 	= ($cek > 0) ? array_values($res): $res;
			to_json($data);
	}
	
	public function opco_hit($tipe=NULL){
		if (empty($tipe)) {
			to_json($this->m_access_log->perCompanyMonth(date("m/Y")));
		}else {
			$opco  = array();
			$hasil = $this->m_access_log->perCompanyDate(date("m/Y"));		//date("m/Y")
			// echo $this->db->last_query();
			//Group by company
			foreach ($hasil as $cp) {
				$id_com = $cp->ID_COMPANY;
				if(isset($opco[$id_com])){
					$opco[$id_com][] = (array) $cp;
				}else{
					$opco[$id_com] = array((array) $cp);
				}
			}


			usort($opco, function($a, $b) {
				return strtotime($a['TANGGAL']) - strtotime($b['TANGGAL']);
			});

			// print_r($opco);
			// exit();
			
			//Generate plotly data
			foreach ($opco as $key => $val) {
				foreach ($val as $dt) {
					$color = company_color_rgb2($dt['KD_COMPANY']);
					$res[$key]['y'][] = $dt['JML_AKSES'];
					$res[$key]['x'][] = $dt['TANGGAL'];
					$res[$key]['type'] = 'scatter';
					$res[$key]['name'] = $dt['COMPANY'];
					$res[$key]['fill'] = "";
					$res[$key]['line'] = array('dash' => 'solid','width' => "3", "color" => "rgba($color,2)");
					$sort[$key] = strtotime($dt['TANGGAL']);
				}
			}
			$cek = count($opco);
			if ($cek > 0) {
				array_multisort($sort, SORT_DESC, $opco);
			}
			//var_dump($opco); die();
			$lay['autosize'] = false;
			$lay['yaxis'] 	 = array(
								"tickfont" => array("color" => "rgb(107, 107, 107)", "size" => 11),
								"ticks" => "outside",
								"tickwidth" => 1,
								"title" => "HIT",
								"ticklen" => 5,
								"autorange" => true,
								"showticklabels" => true,
								"titlefont" => array("color" => "rgb(107, 107, 107)", "size" => 12),
								"mirror" => true,
								"zeroline" => true,
								"showline" => true,
							);
			$lay["title"] = "ACCESS LOG ON ".strtoupper(date("F Y"));
			$lay["height"] = 400;
			$lay["width"] = 380;
			$lay["bargap"] = 0.25;
			$lay['xaxis'] 	 = array(
								"tickfont" => array("color" => "", "size" => 8),
								"ticks" => "outside",
								"tickwidth" => 1,
								"tickangle" => 40,
								"ticklen" => 5,
								"showticklabels" => true,
								"mirror" => true,
								"showline" => true
							);
			$lay['barmode'] = "group";
			$lay['margin'] = array("r" => 60, "b" => 60, "l" => 60, "t" => 80);
			$lay['legend'] = array("orientation" => 'h'); 
			$data['data'] 	= ($cek > 0) ? array_values($res): $res;
			$data['layout'] = $lay;
			to_json($data);
		}
	}
	
	public function opco_hit_user($tipe=NULL){
		// echo 'aaaa'.$company;
		// var_dump($tipe);
		$ID_COMPANY = $this->input->post('company');
		// echo '<br>'. $this->input->post('company');
			// echo $value['ID_COMPANY'].'<br>';
	 
			$opco  = array();
			$hasil = $this->m_access_log->perCompanyDateUser($ID_COMPANY);
			// echo $this->db->last_query();die;
			//Group by company
			// var_dump($hasil);die;
			foreach ($hasil as $cp) {
				$id_com = $cp->ID_COMPANY;
				if(isset($opco[$id_com])){
					$opco[$id_com][] = (array) $cp;
				}else{
					$opco[$id_com] = array((array) $cp);
				}
			}


			usort($opco, function($a, $b) {
				return strtotime($a['TANGGAL']) - strtotime($b['TANGGAL']);
			});


			//Generate plotly data
			foreach ($opco as $key => $val) {
				// var_dump($val);
				foreach ($val as $dt) {
					$color = company_color_rgb2($dt['KD_COMPANY']);
					$res[$key]['y'][] = $dt['JML_AKSES'];
					$res[$key]['x'][] = $dt['FULLNAME'];
					$res[$key]['marker'] = array("color" => "rgb($color)", 'opacity' => "0");
					$res[$key]['type'] = 'bar';

					$res[$key]['textposition']  = 'auto';
					$res[$key]['textcolor']  = 'white';
					$res[$key]['hoverinfo']  = 'none';
					$res[$key]['text'][] = $dt['JML_AKSES'];

					$res[$key]['name'] = $dt['NM_COMPANY'];
					$res[$key]['fill'] = "";
					//$res[$key]['line'] = array('width' => "2", "color" => "rgba($color,2)");
					$sort[$key] = strtotime($dt['TANGGAL']);
				}
			}
			$cek = count($opco);
			if ($cek > 0) {
				array_multisort($sort, SORT_DESC, $opco);
			}
			//var_dump($opco); die();
			$lay['autosize'] = false;
			$lay['yaxis'] 	 = array(
								"tickfont" => array("color" => "rgb(107, 107, 107)", "size" => 11),
								"ticks" => "outside",
								"tickwidth" => 1,
								"title" => "HIT",
								"ticklen" => 5,
								"autorange" => true,
								"showticklabels" => true,
								"titlefont" => array("color" => "rgb(107, 107, 107)", "size" => 12),
								"mirror" => true,
								"zeroline" => true,
								"showline" => true,
							);
			$lay["title"] = "ACCESS LOG TOP USER IN ".$res[$key]['name'];
			$lay["height"] = 300;
			$lay["width"] = 530;
			$lay["bargap"] = 0.25;
			$lay['xaxis'] 	 = array(
								"tickfont" => array("color" => "", "size" => 8),
								// "ticks" => "outside",
								"tickwidth" => 2,
								"showarrow" => False,
								// "tickangle" => 40,
								// "ticklen" => 5, 
								"mirror" => true,
								"showline" => true
							);
			$lay['barmode'] = "group"; 
			$lay['margin'] = array("r" => 60, "b" => 60, "l" => 60, "t" => 80);

			$data['data'] 	= ($cek > 0) ? array_values($res): $res;
			$data['layout'] = $lay;
			to_json($data); 
	}
	
	
	
	public function dashboard_waktu(){   
		
		$header =  array();
		$getGroupPlant = $this->m_ketepatan->getGroupPlant(); 
		$arrayGroupArea = array('1', '81', '2', '6'); 
		
			$array_nilai_produk = array() ; 
			$array_nilai_bahan= array() ; 
		foreach($getGroupPlant as $gp){  
		
			array_push($header, $gp['NM_GROUP_PLANT']); 
			$getPlant = $this->m_ketepatan->getPlant($gp['ID_GROUP_PLANT']);  
			$nomer_produk = 0;
			$s_selisih_produk = 0;
			$nomer_bahan = 0;
			$s_selisih_bahan = 0;
			$hitung_nilai_produk = 0;
			$hitung_nilai_bahan= 0;
			foreach($arrayGroupArea as $ga){
				if($ga=='1' ||$ga=='81' ){
					$tipe_data = 'PRODUK' ;
				}else{
					$tipe_data = 'BAHAN' ;
				}
				$ID_GROUPAREA = $ga;
				foreach($getPlant as $pl){
					$r = @$this->treport->dashboard_daily("01/08/2020","31/08/2020",$ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT']);	 
					$nm_company	= $this->m_company->get_data_by_id($pl['ID_COMPANY'])->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT'])->JML_AREA_PRODUKSI;

					$status = "D";
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					}

					$tgl1 = new DateTime($r->TANGGAL_DATA);
					$tgl2 = new DateTime($r->TANGGAL_ENTRI);
					$selisih = $tgl2->diff($tgl1)->days; 
					
					if($status=='D' && $tipe_data == 'PRODUK'){ 
						$nomer_produk += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_produk += $selisih_fix;
					}	
					
					if($status=='D' && $tipe_data == 'BAHAN'){
						$nomer_bahan += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_bahan +=  $selisih_fix;
					}	
					
					
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($pl['ID_PLANT'])->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
				}
			}
			
			// if($s_selisih_produk>0 && $nomer_produk>0){ 
				// $hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
				// $hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan;  
				// array_push($array_nilai_produk, $hitung_nilai_produk);
				// array_push($array_nilai_bahan, $hitung_nilai_bahan);
				
			// }else{
				// array_push($array_nilai_produk, 1);
				// array_push($array_nilai_bahan, 1);
			// }
			
			
				if($s_selisih_produk>0 &&  $nomer_produk>0 ){
					// echo "produk   ". $s_selisih_produk . " ---- " . $nomer_produk."<br>"; 
					// array_push($array_nilai_produk,  number_format($hitung_nilai_produk, 2, '.', ''));
					$hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
					array_push($array_nilai_produk, $hitung_nilai_produk);
					
				}else{
					array_push($array_nilai_produk, 1);
				}

				if( $s_selisih_bahan>0 &&  $nomer_bahan>0 ){
					
					$hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan; 
					array_push($array_nilai_bahan, $hitung_nilai_bahan);
					
					// echo "bahan    ". $s_selisih_bahan . " ---- " . $nomer_bahan."<br>";
					// array_push($array_nilai_bahan,  number_format($hitung_nilai_bahan, 2, '.', ''));  
				}else{
					array_push($array_nilai_bahan, 1);
				}
			
		}
		
			
			$data = array(
							array(
								"name" => 'Pruduk',
								"data" => $array_nilai_produk,
							),
							array(
								"name" => 'Bahan',
								"data" => $array_nilai_bahan
							) 
						); 
						$arr_bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
						$bulan_angka = intval(date("m")) - 1;
						$bulan_fix = $arr_bulan[$bulan_angka]; 
						$bulan_tahun = $bulan_fix." ". date('Y');
		echo json_encode(array(
			'header' => $header ,
			'data' => $data,
			'bulan_tahun' => $bulan_tahun,
			'cek_data' => $d
		));
	}
	 
	public function dashboard_waktu_detail(){   
		$nm_group_plant = $_POST['nm_group_plant'];
		$header =  array();
		$getGroupPlant = $this->m_ketepatan->getGroupPlantNama($nm_group_plant); 
		$arrayGroupArea = array('1', '81', '2', '6'); 
		
			$array_nilai_produk = array() ; 
			$array_nilai_bahan= array() ; 
		foreach($getGroupPlant as $gp){   
			$getPlant = $this->m_ketepatan->getPlant($gp['ID_GROUP_PLANT']);   
			foreach($getPlant as $pl){
				
			array_push($header, $pl['NM_PLANT']); 
			
			$nomer_produk = 0;
			$s_selisih_produk = 0;
			$nomer_bahan = 0;
			$s_selisih_bahan = 0;
			$hitung_nilai_produk = 0;
			$hitung_nilai_bahan= 0;
			
				foreach($arrayGroupArea as $ga){
					
					if($ga=='1' ||$ga=='81' ){
						$tipe_data = 'PRODUK' ;
					}else{
						$tipe_data = 'BAHAN' ;
					}
					$ID_GROUPAREA = $ga;
					
					$r = @$this->treport->dashboard_daily("01/08/2020","31/08/2020",$ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT']);	 
					$nm_company	= $this->m_company->get_data_by_id($pl['ID_COMPANY'])->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT'])->JML_AREA_PRODUKSI;

					$status = "D";
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					}

					$tgl1 = new DateTime($r->TANGGAL_DATA);
					$tgl2 = new DateTime($r->TANGGAL_ENTRI);
					$selisih = $tgl2->diff($tgl1)->days; 
					
					if($status=='D' && $tipe_data == 'PRODUK'){ 
						$nomer_produk += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_produk += $selisih_fix;
					}	
					
					if($status=='D' && $tipe_data == 'BAHAN'){
						$nomer_bahan += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_bahan +=  $selisih_fix;
					}	
					
					
					
					// $NM_PLANT 									= $this->m_plant->get_data_by_id($pl['ID_PLANT'])->NM_PLANT;
					// $d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					// $d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					// $d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
				}
				
				
				
			
				if($s_selisih_produk>0 &&  $nomer_produk>0 ){
					// echo "produk   ". $s_selisih_produk . " ---- " . $nomer_produk."<br>"; 
					// array_push($array_nilai_produk,  number_format($hitung_nilai_produk, 2, '.', ''));
					$hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
					array_push($array_nilai_produk, $hitung_nilai_produk);
					
				}else{
					array_push($array_nilai_produk, 0);
				}

				if( $s_selisih_bahan>0 &&  $nomer_bahan>0 ){
					
					$hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan; 
					array_push($array_nilai_bahan, $hitung_nilai_bahan);
					
					// echo "bahan    ". $s_selisih_bahan . " ---- " . $nomer_bahan."<br>";
					// array_push($array_nilai_bahan,  number_format($hitung_nilai_bahan, 2, '.', ''));  
				}else{
					array_push($array_nilai_bahan, 0);
				}
				
			}
			
		}
		
			
			$data = array(
							array(
								"name" => 'Pruduk',
								"data" => $array_nilai_produk,
							),
							array(
								"name" => 'Bahan',
								"data" => $array_nilai_bahan
							) 
						); 
		echo json_encode(array(
			'header' => $header ,
			'data' => $data,
			// 'cek_data' => $d
		));
	}
	 
	
	
	
}
