<?php

class Opco extends QMUser {
	
	public $list_data = array();
	public $list_jabatan = array();
	public $data_opco;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_opco");
		$this->load->model("m_company");
		$this->load->model("m_jabatan");
		$this->load->model("employee");
		$this->load->model("m_user");
		$this->load->model("m_roles");
		$this->load->library('email');
	}
	
	public function listHris(){
		exit;
		$a = $this->employee->get_list();
		// echo "<pre>";
		// print_r($a);
		// echo "</pre>";

		foreach ($a as $key => $value) {
			if(strlen($value->MK_NOPEG) == 1){
				$value->MK_NOPEG = '0000000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 2){
				$value->MK_NOPEG = '000000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 3){
				$value->MK_NOPEG = '00000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 4){
				$value->MK_NOPEG = '0000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 5){
				$value->MK_NOPEG = '000'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 6){
				$value->MK_NOPEG = '00'.$value->MK_NOPEG;
			}elseif(strlen($value->MK_NOPEG) == 7){
				$value->MK_NOPEG = '0'.$value->MK_NOPEG;
			}

			$this->db->set('EMAIL', "'".$value->mk_email."'", FALSE);
			// $this->db->set('NOPEG', "'".$value->MK_NOPEG."'", FALSE);
			$this->db->where('USERNAME', $value->USERNAME);
			$this->db->update('M_USERS'); // gives UPDATE mytable SET field = field+1 WHERE id = 2
			// echo $this->db->last_query().'<br>';
		}

	}

	public function listAccess(){
		// echo "<pre>";
		// print_r($a);
		// echo "</pre>";

		exit;

		$a = $this->m_user->getData();
		foreach ($a as $i => $v) {

			$roles = array();
			$roles[0]['ID_USER'] = $v['ID_USER'];
			$roles[0]['ID_USERGROUP'] = $v['ID_USERGROUP'];
			$this->m_roles->insertBatch($roles);

		}

	}
	
	public function index(){
		$this->libExternal('dataTables');
		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		// $this->list_opco = $this->m_opco->datalist()->result();

		$this->template->adminlte("v_opco2");
	}

	public function getDataList(){
		$notif = $this->input->post("notif");
		$company = $this->input->post("company");
		$plant = $this->input->post("plant");
		$grouparea = $this->input->post("grouparea");
		$area = $this->input->post("area");
		// exit;
		$list = $this->m_opco->datalist($notif, $company, $plant, $grouparea, $area)->result_array();
		// echo $this->db->last_query();
		foreach ($list as $k => $v) {
			$list[$k]['NO'] = ($k +1);
		}

		$listData = array('data' => $list);
		echo json_encode($listData);
	}

	public function add(){
		$this->libExternal('dataTables');

		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_add");
	}
	
	public function create(){
		$check_exist = $this->m_opco->get_exist_notify_reciever($this->input->post());
		if(count($check_exist) > 0){
			$status = 3;
		} else {
			$this->m_opco->insert($this->input->post());
			
			$status = true;
			if($this->m_opco->error()){
				$status = false;	
			}
		}

		echo $status;
	}
	
	public function edit($ID_OPCO){
		$this->libExternal('dataTables');
		
		$this->data_opco = $this->m_opco->get_data_by_id($ID_OPCO);
		$this->list_company = $this->m_company->datalist();
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_opco_edit");
	}
	
	public function update($ID_OPCO){
		$check_exist = $this->m_opco->get_exist_notify_reciever($this->input->post());

		if(count($check_exist) > 0){
			$status = 3;
		} else {
			$this->m_opco->update($this->input->post(),$ID_OPCO);
			if($this->m_opco->error()){
				$status = false;
				// $this->notice->error($this->m_opco->error());
			} else{
				$status = true;
				// $this->notice->success("Opco Data Updated.");
			}
		}
		
		echo $status;
	}
	
	public function delete($ID_OPCO){
		$this->m_opco->delete($ID_OPCO);
		if($this->m_opco->error()){
			$this->notice->error($this->m_opco->error());
		}
		else{
			$this->notice->success("Opco Data Removed.");
		}
		redirect("opco");
	}
	
	public function notification_member($ID_AREA=NULL,$ID_JABATAN=NULL){
		echo json_encode($this->m_opco->notification_member($ID_AREA,$ID_JABATAN));
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}

	public function get_username_registered(){
		$username = $this->input->post('username');
		$get = $this->m_user->get_username_registered(strtoupper($username));
		to_json($get);
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
		to_json($area);
	}
	
}	

?>
