<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	use SAPNWRFC\Connection as SapConnection;
	use SAPNWRFC\Exception as SapException;
class Qbb_data extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_qbb_data","qbb");
	}
	
	public function index(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		
		if(isset($_POST["start_load"])){
			$a = $_POST["start_load"];
			$b = $_POST["end_load"];
			
			$start  = date('Y-m-d', strtotime($a));
			$end	= date('Y-m-d', strtotime($b));
			
			$this->start_data = $start;
			$this->end_data = $end;
		} else { 
			$today = date("Y-m-d");  
			$start 	= date('Y-m-d', strtotime('-1 month')); //date('Y')."-".."-".date("d");
			$end	= date("Y-m-d");	
			$this->start_data = $start;
			$this->end_data = $end;
		}
		// echo $start. " - ";
		// echo $end;
		// exit();
		$this->list_data = $this->qbb->get_data_periode($start, $end);
		
		$this->template->adminlte("v_qbb_data");
    }
	
	public function data_sap_qbb(){
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		$start  = $_POST["start_sync"];
		$end	= $_POST["end_sync"];
		$date_start 	= date('Ymd', strtotime($start));
		$date_end 	= date('Ymd', strtotime($end));
		
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_prod = [
			'ashost' => '10.15.5.13',
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];

        try {
            $sapi = new SapConnection($config_prod);
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
            if ($rfc == TRUE) {
                $code_mat = '112-100-0009';
				$options = [
					'rtrim' => true
				];
				$rfc->setParameterActive('R_MATNR', true);
				$rfc->setParameterActive('R_PERIOD', true);
				$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' 	=> [
								[
									'SIGN' 	=> 'I',
									'OPTION'=> 'BT',
									'LOW' 	=> $date_start,
									'HIGH' 	=> $date_end
								]
							]
						];
                $result = $rfc->invoke($parms, $options);

				$fromsap = count($result['R_RESULT']);
				$action = $this->qbb->data_from_sap($result['R_RESULT']);
				$existing = $fromsap-$action;
				
				$this->notice->success("Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing);
            }
        } catch(SapException $ex) {
            $error = 'Exception: ' . $ex->getMessage() . PHP_EOL;
			$this->notice->error($error);
        }
		redirect("qbb_data");
    }
	
	public function testing(){
		// require_once(APPPATH.'controllers/dashboard.php'); 
		$config = $this->sap_config('DEV'); // dev
		echo json_encode($config);
		die();
		$data = array();
		//error_reporting(-1);
		//ini_set('display_errors', 1);
		// $start  = $_POST["start_sync"];
		// $end	= $_POST["end_sync"];
		$start = '04/01/2021';
		$end = '04/30/2021';
		$date_start 	= date('Ymd', strtotime($start));
		$date_end 	= date('Ymd', strtotime($end));
		// $date['start'] = $date_start;
		// $date['end'] = $date_end;
		// echo json_encode($date);
		// die();
		$config_prod = [
			'ashost' => '10.15.4.200', // update by izza 17 mei 2021
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];

        try {
            $sapi = new SapConnection($config_prod);
            $rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
            if ($rfc == TRUE) {
                $code_mat = '112-100-0009';
								$options = [
									'rtrim' => true
								];
								$rfc->setParameterActive('R_MATNR', true);
								$rfc->setParameterActive('R_PERIOD', true);
								$parms = [
									'R_MATNR' 	=> $code_mat,
									'R_PERIOD' 	=> [
										[
											'SIGN' 	=> 'I',
											'OPTION'=> 'BT',
											'LOW' 	=> $date_start,
											'HIGH' 	=> $date_end
										]
									]
								];
								$result = $rfc->invoke($parms, $options);

								$fromsap = count($result['R_RESULT']);
								// $action = $this->qbb->data_from_sap($result['R_RESULT']);
								// $existing = $fromsap-$action;
								$data = $result;
								// $this->notice->success("Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing);
            }
        } catch(SapException $ex) {
            $error = 'Exception: ' . $ex->getMessage() . PHP_EOL;
						$data['error'] = $error;
						$this->notice->error($error);
        }
				echo json_encode($data);
		// redirect("qbb_data");
	}
	
	// -------------------------- start izza 17.05.21 --------------------------
	public function testing2(){
		// require_once(APPPATH.'controllers/dashboard.php'); 
		$config = $this->sap_config('DEV'); // dev
		$start = '2020-12-31';
		$end = '2020-12-31';
		// $first 	= date('Y-m-d', strtotime($start));
		// $last 	= date('Y-m-d', strtotime($end));
		// $coba = $this->date_range("2014-01-01", "2014-01-20", "+1 day", "Ymd");
		$current = strtotime($start);
    $last = strtotime($end);
		// $R_PERIOD = array(); 

    while( $current <= $last ) {
			$temp = [
				'SIGN' 	=> 'I', 
				'OPTION'=> 'EQ', 
				'LOW' 	=> date('Ymd', $current),
				'HIGH' 	=> date('Ymd', $current) 
			];
			$R_PERIOD[] = $temp;
			$current = strtotime('+1 day', $current);
    }
		try {
				$sapi = new SapConnection($config);
				$rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');
				if ($rfc == TRUE) {
						$code_mat = '112-100-0009';
						$options = [
							'rtrim' => true
						];
						$rfc->setParameterActive('R_MATNR', true);
						$rfc->setParameterActive('R_PERIOD', true);
						$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' 	=> $R_PERIOD
						];
						$result = $rfc->invoke($parms, $options);

						$fromsap = count($result['R_RESULT']);
						// $action = $this->qbb->data_from_sap($result['R_RESULT']);
						// $existing = $fromsap-$action;
						$data = $result;
						// $this->notice->success("Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing);
				}
		} catch(SapException $ex) {
				$error = 'Exception: ' . $ex->getMessage() . PHP_EOL;
				$data['error'] = $error;
				$this->notice->error($error);
		}
		echo json_encode($data);
		// redirect("qbb_data");
	}
	public function sapconfig(){
		// $config = $this->sap_config(); // prod
		$config = $this->sap_config('DEV'); // dev
		$tgl = ['20170330','20200130','20201016'];
    try {
			$sapi = new SapConnection($config);
					// $sapi = new SapConnection($config_prod);
			
					$rfc = $sapi->getFunction('ZCQM_GET_QUALITAS');

					if ($rfc == TRUE) {
						$date_start = '12/30/2020';
						$date_now = date('Ymd', strtotime($date_start));
						// $date_now = date('Ymd');
						$code_mat = '112-100-0009';
						$options = [
							'rtrim' => true
						];
						$rfc->setParameterActive('R_MATNR', true);
						$rfc->setParameterActive('R_PERIOD', true);
						$R_PERIOD = array();
						foreach ($tgl as $key) {
							$temp = ['SIGN' 	=> 'I', 'OPTION'=> 'EQ', 'HIGH' 	=> $key, 'LOW' 	=> $key];
							$R_PERIOD[] = $temp;
						}
						$parms = [
							'R_MATNR' 	=> $code_mat,
							'R_PERIOD' => $R_PERIOD
						];
						$result = $rfc->invoke($parms, $options);
						$fromsap = count($result['R_RESULT']);
										
						// $action = $this->qbb->data_from_sap($result['R_RESULT']);
						// $existing = $fromsap-$action;
						// echo $fromsap;
						echo json_encode($result);		
						// echo "Syny date: ".date('d-m-Y')." => Success: SAP Data ".$fromsap.". Total Data Sync ".$action.". Data similar ".$existing;
				}
					//$rfc->Close();
					//$sapi->Close();
        } catch(SapException $ex) {
            echo 'Exception: ' . $ex->getMessage() . PHP_EOL;
        }
  } 
	public function sap_config($server = 'PROD'){
		$config_dev = [
			'ashost' => '10.15.5.25',
			'sysnr'  => '00',
			'client' => '030',
			'user'   => 'sutrisno',
			'passwd' => 'semensg01',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		$config_prod = [
			'ashost' => '10.15.4.200', // update by izza 17 mei 2021
			'sysnr'  => '20',
			'client' => '210',
			'user'   => 'RFCPM',
			'passwd' => 'sgmerdeka99',
			'trace'  => SapConnection::TRACE_LEVEL_OFF,
		];
		if($server == 'PROD'){$config = $config_prod;}
		else{ $config = $config_dev;}
		return $config;
  } 
	// -------------------------- end izza 17.05.21 --------------------------
	

	public function info_server(){
		phpinfo();
	}
	
}

?>