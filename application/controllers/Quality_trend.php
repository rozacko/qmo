<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality_trend extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
		$this->load->model("c_product");
		$this->load->model("c_parameter");
		$this->load->model("m_product");
		$this->load->model("m_component");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("c_qaf_component");
		$this->load->model("c_range_qaf");
	}

	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist_auth($this->USER->ID_GROUPAREA);
		$this->list_product   = $this->m_product->datalist();
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_quality_trend", $data);
	}

	public function ajax_get_area_by_company(){
		$opt_c = (NULL==$this->input->post('opt_c'))? 0:$this->input->post('opt_c');
		$opt_g = (NULL==$this->input->post('opt_g'))? 0:$this->input->post('opt_g');
		$area = $this->m_area->or_where($opt_c, $opt_g);
		to_json($area);
	}

	public function ajax_get_product_by_area(){
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');

		foreach ($opt as $a) {
			if($a=='semua'){
			   unset($opt[0]);
		   }
		}

		$c_product = $this->c_product->or_where($opt);
		
		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
		}

		$tmp = array_count_values($tmp);

		foreach ($tmp as $key => $value) {
			if (count($opt) > 1) {
				if($value < 2) {
					unset($tmp[$key]);
				}
			}
		}

		$tmp = array_keys($tmp);

		foreach ($c_product as $key => $prod) {
			if (!in_array($prod->ID_PRODUCT, $tmp)) {
				unset($c_product[$key]);
			}
		}

		to_json($c_product);
	}
	// start create izza jan 2021
	public function ajax_get_product_by_area_read(){
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');

		foreach ($opt as $a) {
			if($a=='semua'){
				unset($opt[0]);
			}
		}

		$c_product = $this->c_product->or_where_read($opt); // izza
		
		// $tmp = array_count_values($tmp);

		// foreach ($tmp as $key => $value) {
		// 	if (count($opt) > 1) {
		// 		if($value < 2) {
		// 			unset($tmp[$key]);
		// 		}
		// 	}
		// }

		// $tmp = array_keys($tmp);

		// foreach ($c_product as $key => $prod) {
		// 	if (!in_array($prod->ID_PRODUCT, $tmp)) {
		// 		unset($c_product[$key]);
		// 	}
		// }

		// to_json($c_product);
		
		// izza 29.07.21
		$id_tmp = []; $id=0;
		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
			$id_tmp[] = $id;
			$id++;
		}
		$c_product_fix = [];
		$tmp2 = [];
		$id=0;
		foreach ($tmp as $key) {
			if(!in_array($key, $tmp2)){
				$tmp2[] = $tmp[$id];
				$c_product_fix[] = $c_product[$id];
			}
			$id++;
		}
		to_json($c_product_fix);
	}
	// end create izza jan 2021

	public function ajax_get_component(){
		$area = (null !== $this->input->post('area')) ? $this->input->post('area'):array(0);
		$group = $this->input->post('group');
		$dh_ly = ($this->input->post('dh_ly')=="D" || $this->input->post('dh_ly')=="H") ? "H":"D";
		foreach ($area as $val) {
			if($val!='semua'){
				$plant[] = $this->m_area->get_data_by_id($val)->ID_PLANT;
			}
			
		}

		if (in_array($group[0], array("1","4"))) {
			$c_parameter = $this->c_qaf_component->or_where($plant, $group, $dh_ly);
		}else{
			$c_parameter = $this->c_parameter->or_where($plant, $group, $dh_ly);
		}
		#echo $this->db->last_query();
		to_json($c_parameter);
	}

	public function generate_boxplot_ori(){
		$start 		= $this->input->post('h_start');
		$start 		= explode(":", $start);
		$start 		= $start[0];
		$end 			= $this->input->post('h_end');
		$end 			= explode(":", $end);
		$end 			= $end[0];
		$tgl1			= $this->input->post('d_start');
		$tgl2			= $this->input->post('d_end');
		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$dh_ly	 	= $this->input->post('dh_ly');
		$temp = [];
		switch ($dh_ly) {
			case 'Y':
				$date = $tahun; // izza 26.07.2021
				$tPer = $tahun;
				break;
			case 'M':
				
				if(strlen($bulan)==1) $bulan = '0'.$bulan;
				$date = $bulan.'/'.$tahun; // izza 26.07.2021
				$tPer = strtoupper(date("F", mktime(0, 0, 0, $bulan, 10))) . " " . $tahun;
				break;
			case 'D':
				$date = array($tgl1, $tgl2);
				$tPer = $tgl1 . ' to ' . $tgl2;
				break;
			case 'H':
				$tgl_start = explode(' ', $start);
				$temp_start = $tgl_start[1];
				$tgl_start = $tgl_start[0];
				if(strlen($temp_start)==1){$jam_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$jam_start = $tgl_start.' '.$temp_start.':00';}

				$temp_start++;
				if(strlen($temp_start)==1){$judul_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$judul_start = $tgl_start.' '.$temp_start.':00';}
				$start_fix['judul'] = $judul_start;
				$start_fix['tgl'] = $tgl_start;
				$start_fix['jam'] = $jam_start;

				$tgl_end = explode(' ', $end);
				$temp_end = $tgl_end[1];
				$tgl_end = $tgl_end[0];
				if(strlen($temp_end)==1){$jam_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$jam_end = $tgl_end.' '.$temp_end.':00';}

				$temp_end++;
				if(strlen($temp_end)==1){$judul_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$judul_end = $tgl_end.' '.$temp_end.':00';}
				$end_fix['judul'] = $judul_end;
				$end_fix['tgl'] = $tgl_end;
				$end_fix['jam'] = $jam_end;

				$date = array($start_fix, $end_fix);
				// $date = array($tgl1, $tgl2);
				$tPer = $start_fix['judul']. ' to ' . $end_fix['judul'];
				break;
			default:
				$date = '01/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				break;
		}
		#var_dump($date);
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= (null !== $this->input->post('param')) ? $this->input->post('param'):array(0);
		$component 	= array_filter($component, 'is_numeric');
		// $mod   		= (in_array($grouparea[0], array("1","4"))) ?  (($dh_ly=="D" || $dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="D" || $dh_ly=="H") ? "t_production_hourly":"t_production_daily");
		$mod   		= (in_array($grouparea[0], array("1","4","81"))) ?  (($dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="H") ? "t_production_hourly":"t_production_daily"); // izza 26.07.2021
		$parameter	= (in_array($grouparea[0], array("1","4","81"))) ?  array($product[0],$component):$component;

		$cmp = array();
		$plotdata = array();
		$komponen = "";
		foreach ($area as $a) {
			if($a=='semua'){
				unset($area[0]);
			}
		}

		// echo $mod;exit();
		// $production = $this->{$mod}->get_qtrend($date, $area, $parameter, $dh_ly, $grouparea[0]);
		$production = $this->{$mod}->get_qtrend2($date, $area, $parameter, $dh_ly, $grouparea[0]); // izza 26.07.2021
		#$temp_prod  = new stdClass();
		#$tmp = array();
		$query = $this->db->last_query();
		// echo $this->db->last_query();
		// die();
		$temp = [];
		if($production){

			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_AREA);
				$dash  = getDash($prod->ID_AREA);

				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1", "color" => "rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1"); // izza 25.07.21
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "lines";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines+markers";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("symbol" => "square", "size" => 8);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						
						#coding ini untuk menampilkan batas minimal dan maksimal pada grafik qaf quality trend
						$minmax = $this->c_range_qaf->configuration_component($prod->ID_COMPANY,$grouparea[0],$product[0],$prod->ID_COMPONENT);
						$query = $this->db->last_query();
						if($minmax['FLAG'] != null){
							/* Max */
							#if ($prod->V_MAX <= 900) {
							if($minmax['V_MAX'] != '9999' || $minmax['V_MAX'] != null){
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MAX'];
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "red");
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MAX " . trim($prod->KD_COMPONENT);
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
							}

							/* Min */
							#if ($prod->V_MIN > 0) {
							if($minmax['V_MIN'] != null){
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MIN'];
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "blue");
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MIN " . trim($prod->KD_COMPONENT);
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
							}
						}

					}
				}
			}

			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}

			
			if (isset($max)) {
				$k = 0;
				foreach ($max as $key => $value) {
					$ck = $max[$key];
					foreach ($ck as $ky => $v) {
						$pk[$ky] = count($ck[$ky]['y']);
					}
					$maks = array_keys($pk, max($pk));
					$ab[$k]['data'][] = $max[$key][$maks[0]];
					$k++;
				}
			}
			
			if (isset($min)) {
				$k = 0;
				foreach ($min as $key => $value) {
					$cl = $min[$key];
					foreach ($cl as $ky => $v) {
						$pl[$ky] = count($cl[$ky]['y']);
					}
					$mins = array_keys($pl, max($pl));
					$ab[$k]['data'][] = $min[$key][$mins[0]];
					$k++;
				}
			}

			//Generate Layout
			foreach ($ab as $key => $value) {
				$lay[$key]['layout'] = array(
					"title" => "Quality Trend - $tPer<BR>" . trim($value['data'][0]['parameter']),
					"xaxis" => array(
						"title" => "",
						"titlefont" => array('size' => ($dh_ly=='H') ? 18:14),
						"tickfont" => array('size' => ($dh_ly=='H') ? 5:10),
						"autorange" => TRUE
					),
					"yaxis" => array("title" => 'NILAI'),
					"legend"=> array(
						"font" => array("size" => 10)
					),
					"showlegend" => TRUE
				);
			}
			$plotdata['msg'] = "200";
		}else{
		$plotdata['date'] = $date;
			$plotdata['msg'] = "404";
		}
		$plotdata['ab'] = $ab;
		$plotdata['query'] = $query;
		$plotdata['date'] = $date;
		$plotdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$plotdata['layout'] = array_values((empty($lay)) ? array(0): $lay);
		to_json($plotdata);
	}
	// izza 28.07.21
	public function generate_boxplot2(){
		$start 		= $this->input->post('h_start');
		$start 		= explode(":", $start);
		$start 		= $start[0];
		$end 			= $this->input->post('h_end');
		$end 			= explode(":", $end);
		$end 			= $end[0];
		$tgl1			= $this->input->post('d_start');
		$tgl2			= $this->input->post('d_end');
		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$dh_ly	 	= $this->input->post('dh_ly');
		$temp = [];
		switch ($dh_ly) {
			case 'Y':
				$date = $tahun; // izza 26.07.2021
				$tPer = $tahun;
				break;
			case 'M':
				
				if(strlen($bulan)==1) $bulan = '0'.$bulan;
				$date = $bulan.'/'.$tahun; // izza 26.07.2021
				$tPer = strtoupper(date("F", mktime(0, 0, 0, $bulan, 10))) . " " . $tahun;
				break;
			case 'D':
				$date = array($tgl1, $tgl2);
				$tPer = $tgl1 . ' to ' . $tgl2;
				break;
			case 'H':
				$tgl_start = explode(' ', $start);
				$temp_start = $tgl_start[1];
				$tgl_start = $tgl_start[0];
				if(strlen($temp_start)==1){$jam_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$jam_start = $tgl_start.' '.$temp_start.':00';}

				$temp_start++;
				if(strlen($temp_start)==1){$judul_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$judul_start = $tgl_start.' '.$temp_start.':00';}
				$start_fix['judul'] = $judul_start;
				$start_fix['tgl'] = $tgl_start;
				$start_fix['jam'] = $jam_start;

				$tgl_end = explode(' ', $end);
				$temp_end = $tgl_end[1];
				$tgl_end = $tgl_end[0];
				if(strlen($temp_end)==1){$jam_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$jam_end = $tgl_end.' '.$temp_end.':00';}

				$temp_end++;
				if(strlen($temp_end)==1){$judul_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$judul_end = $tgl_end.' '.$temp_end.':00';}
				$end_fix['judul'] = $judul_end;
				$end_fix['tgl'] = $tgl_end;
				$end_fix['jam'] = $jam_end;

				$date = array($start_fix, $end_fix);
				// $date = array($tgl1, $tgl2);
				$tPer = $start_fix['judul']. ' to ' . $end_fix['judul'];
				break;
			default:
				$date = '01/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				break;
		}
		#var_dump($date);
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= (null !== $this->input->post('param')) ? $this->input->post('param'):array(0);
		$component 	= array_filter($component, 'is_numeric');
		// $mod   		= (in_array($grouparea[0], array("1","4"))) ?  (($dh_ly=="D" || $dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="D" || $dh_ly=="H") ? "t_production_hourly":"t_production_daily");
		$mod   		= (in_array($grouparea[0], array("1","4","81"))) ?  (($dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="H") ? "t_production_hourly":"t_production_daily"); // izza 26.07.2021
		$parameter	= (in_array($grouparea[0], array("1","4","81"))) ?  array($product[0],$component):$component;

		$cmp = array();
		$plotdata = array();
		$komponen = "";
		foreach ($area as $a) {
			if($a=='semua'){
				unset($area[0]);
			}
		}

		// echo $mod;exit();
		// $production = $this->{$mod}->get_qtrend($date, $area, $parameter, $dh_ly, $grouparea[0]);
		$production = $this->{$mod}->get_qtrend2($date, $area, $parameter, $dh_ly, $grouparea[0]); // izza 26.07.2021
		#$temp_prod  = new stdClass();
		#$tmp = array();
		// $query = $this->db->last_query();
		// echo $this->db->last_query();
		// die();
		// $temp = [];
		$temp_period = [];
		if($production){

			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_AREA);
				$dash  = getDash($prod->ID_AREA);

				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$period_x = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
						$temp_period[] = $period_x;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1", "color" => "rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1"); // izza 25.07.21
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "lines";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines+markers";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("symbol" => "square", "size" => 8);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("size" => 4);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						
						#coding ini untuk menampilkan batas minimal dan maksimal pada grafik qaf quality trend
						$minmax = $this->c_range_qaf->configuration_component($prod->ID_COMPANY,$grouparea[0],$product[0],$prod->ID_COMPONENT);
						// $temp[] = $minmax;
						$temp = $this->db->last_query();
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "red");
						// $max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines";
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MAX " . trim($prod->KD_COMPONENT) . " opco:" . $prod->ID_COMPANY;
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);

						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "blue");
						// $min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines";
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MIN " . trim($prod->KD_COMPONENT) . " opco:" . $prod->ID_COMPANY;
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						
						
						if($minmax['FLAG'] != null){
							/* Max */
							#if ($prod->V_MAX <= 900) {
							
							if($minmax['V_MAX'] != "9999"){
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MAX'];
								
							}else{
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							}
							/* Min */
							#if ($prod->V_MIN > 0) {
							if($minmax['V_MIN'] != null){
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MIN'];
							}else{
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							}
							
						} else{
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
						}

					}
				}
			}

			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}

			
			if (isset($max)) {
				$k = 0;
				foreach ($max as $key => $value) {
					$ck = $max[$key];
					foreach ($ck as $ky => $v) {
						$pk[$ky] = count($ck[$ky]['y']);
					}
					$maks = array_keys($pk, max($pk));
					$ab[$k]['data'][] = $max[$key][$maks[0]];
					$k++;
				}
			}
			
			if (isset($min)) {
				$k = 0;
				foreach ($min as $key => $value) {
					$cl = $min[$key];
					foreach ($cl as $ky => $v) {
						$pl[$ky] = count($cl[$ky]['y']);
					}
					$mins = array_keys($pl, max($pl));
					$ab[$k]['data'][] = $min[$key][$mins[0]];
					$k++;
				}
			}

			// $tmp_max_lagi = [];
			// for ($i=0; $i < count($period_x); $i++) { 
			// 	$tmp_max_lagi[] = '5'; // dummy
			// }
			// $tmp_max_param = [];
			// if (isset($max)) {
			// 	$k = 0;
			// 	$satu = 0;
			// 	foreach ($max as $key => $value) {
			// 		// $keys[] = $key;
			// 		// $values[] = $value;
			// 		// $templagi['y']  = $tmp_max_lagi;
			// 		// $templagi['x']  = $temp_period;
			// 		// $templagi['line'] = array('dash' => 'dashdot','width' => "1", "color" => "blue");
			// 		// // $templagi['type'] = "scatter";
			// 		// $templagi['mode'] = "lines";
			// 		// $templagi['name'] = "MIN " ;
			// 		// // $templagi['parameter']  = trim($prod->KD_COMPONENT);
			// 		// if($satu<2){
			// 		// 	$ab[$k]['data'][] = $templagi;
			// 		// }
			// 		$k++;
			// 		$satu++;
			// 	}
			// }
			
			//Generate Layout
			foreach ($ab as $key => $value) {
				$lay[$key]['layout'] = array(
					"title" => "Quality Trend - $tPer<BR>" . trim($value['data'][0]['parameter']),
					"xaxis" => array(
						"title" => "",
						"titlefont" => array('size' => ($dh_ly=='H') ? 18:14),
						"tickfont" => array('size' => ($dh_ly=='H') ? 5:10),
						"autorange" => TRUE
					),
					"yaxis" => array("title" => 'NILAI'),
					"legend"=> array(
						"font" => array("size" => 10)
					),
					"showlegend" => TRUE
				);
			}
			$plotdata['msg'] = "200";
		}else{
			$plotdata['msg'] = "404";
		}
		$plotdata['query'] = $temp;
		// $plotdata['max'] = $max;
		// $plotdata['keys'] = $keys;
		// $plotdata['values'] = $values;
		$plotdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$plotdata['layout'] = array_values((empty($lay)) ? array(0): $lay);
		to_json($plotdata);
	}

	// izza 31.07.21
	public function generate_boxplot(){
		$start 		= $this->input->post('h_start');
		$start 		= explode(":", $start);
		$start 		= $start[0];
		$end 			= $this->input->post('h_end');
		$end 			= explode(":", $end);
		$end 			= $end[0];
		$tgl1			= $this->input->post('d_start');
		$tgl2			= $this->input->post('d_end');
		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$dh_ly	 	= $this->input->post('dh_ly');
		$temp = [];
		switch ($dh_ly) {
			case 'Y':
				$date = $tahun; // izza 26.07.2021
				$tPer = $tahun;
				break;
			case 'M':
				
				if(strlen($bulan)==1) $bulan = '0'.$bulan;
				$date = $bulan.'/'.$tahun; // izza 26.07.2021
				$tPer = strtoupper(date("F", mktime(0, 0, 0, $bulan, 10))) . " " . $tahun;
				break;
			case 'D':
				$date = array($tgl1, $tgl2);
				$tPer = $tgl1 . ' to ' . $tgl2;
				break;
			case 'H':
				$tgl_start = explode(' ', $start);
				$temp_start = $tgl_start[1];
				$tgl_start = $tgl_start[0];
				if(strlen($temp_start)==1){$jam_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$jam_start = $tgl_start.' '.$temp_start.':00';}

				$temp_start++;
				if(strlen($temp_start)==1){$judul_start = $tgl_start.' 0' . $temp_start.':00';}
				else{$judul_start = $tgl_start.' '.$temp_start.':00';}
				$start_fix['judul'] = $judul_start;
				$start_fix['tgl'] = $tgl_start;
				$start_fix['jam'] = $jam_start;

				$tgl_end = explode(' ', $end);
				$temp_end = $tgl_end[1];
				$tgl_end = $tgl_end[0];
				if(strlen($temp_end)==1){$jam_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$jam_end = $tgl_end.' '.$temp_end.':00';}

				$temp_end++;
				if(strlen($temp_end)==1){$judul_end = $tgl_end.' 0' . $temp_end.':00';}
				else{$judul_end = $tgl_end.' '.$temp_end.':00';}
				$end_fix['judul'] = $judul_end;
				$end_fix['tgl'] = $tgl_end;
				$end_fix['jam'] = $jam_end;

				$date = array($start_fix, $end_fix);
				// $date = array($tgl1, $tgl2);
				$tPer = $start_fix['judul']. ' to ' . $end_fix['judul'];
				break;
			default:
				$date = '01/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				break;
		}
		#var_dump($date);
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= (null !== $this->input->post('param')) ? $this->input->post('param'):array(0);
		$component 	= array_filter($component, 'is_numeric');
		// $mod   		= (in_array($grouparea[0], array("1","4"))) ?  (($dh_ly=="D" || $dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="D" || $dh_ly=="H") ? "t_production_hourly":"t_production_daily");
		$mod   		= (in_array($grouparea[0], array("1","4","81"))) ?  (($dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="H") ? "t_production_hourly":"t_production_daily"); // izza 26.07.2021
		$parameter	= (in_array($grouparea[0], array("1","4","81"))) ?  array($product[0],$component):$component;

		$cmp = array();
		$plotdata = array();
		$komponen = "";
		foreach ($area as $a) {
			if($a=='semua'){
				unset($area[0]);
			}
		}

		// echo $mod;exit();
		// $production = $this->{$mod}->get_qtrend($date, $area, $parameter, $dh_ly, $grouparea[0]);
		$production = $this->{$mod}->get_qtrend2($date, $area, $parameter, $dh_ly, $grouparea[0]); // izza 26.07.2021
		#$temp_prod  = new stdClass();
		#$tmp = array();
		// $query = $this->db->last_query();
		// echo $this->db->last_query();
		// die();
		// $temp = [];
		$temp_period = [];
		if($production){

			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_AREA);
				$dash  = getDash($prod->ID_AREA);

				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$period_x = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
						$temp_period[] = $period_x;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1", "color" => "rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "1"); // izza 25.07.21
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "lines";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines+markers";
						// $cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("symbol" => "square", "size" => 8);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("size" => 4);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						
						#coding ini untuk menampilkan batas minimal dan maksimal pada grafik qaf quality trend
						$minmax = $this->c_range_qaf->configuration_component($prod->ID_COMPANY,$grouparea[0],$product[0],$prod->ID_COMPONENT);
						// $temp[] = $minmax;
						$temp = $this->db->last_query();
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "red");
						// $max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines";
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MAX " . trim($prod->KD_COMPONENT);
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['opco']  = $prod->ID_COMPANY; // izza 31.07.21
						$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['idcom']  = $prod->ID_COMPONENT; // izza 31.07.21

						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = $period_x;
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "1", "color" => "blue");
						// $min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines";
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MIN " . trim($prod->KD_COMPONENT);
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['opco']  = $prod->ID_COMPANY; // izza 31.07.21
						$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['idcom']  = $prod->ID_COMPONENT; // izza 31.07.21
						
						
						if($minmax['FLAG'] != null){
							/* Max */
							#if ($prod->V_MAX <= 900) {
							
							if($minmax['V_MAX'] != "9999"){
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MAX'];
								
							}else{
								$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							}
							/* Min */
							#if ($prod->V_MIN > 0) {
							if($minmax['V_MIN'] != null){
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $minmax['V_MIN'];
							}else{
								$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							}
							
						} else{
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = null;
						}

					}
				}
			}

			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}

			
			// if (isset($max)) {
			// 	$k = 0;
			// 	foreach ($max as $key => $value) {
			// 		$ck = $max[$key];
			// 		foreach ($ck as $ky => $v) {
			// 			$pk[$ky] = count($ck[$ky]['y']);
			// 		}
			// 		$maks = array_keys($pk, max($pk));
			// 		$ab[$k]['data'][] = $max[$key][$maks[0]];
			// 		$k++;
			// 	}
			// }
			
			// if (isset($min)) {
			// 	$k = 0;
			// 	foreach ($min as $key => $value) {
			// 		$cl = $min[$key];
			// 		foreach ($cl as $ky => $v) {
			// 			$pl[$ky] = count($cl[$ky]['y']);
			// 		}
			// 		$mins = array_keys($pl, max($pl));
			// 		$ab[$k]['data'][] = $min[$key][$mins[0]];
			// 		$k++;
			// 	}
			// }
			$tmp_max = [];
			if (isset($max)) {
				$k = 0;
				foreach ($max as $key => $value) {
					$ck = $max[$key];
					$nilai_y;
					$double = 0;
					foreach ($ck as $ky => $v) {
						if(!in_array($v['y'][0], $tmp_max[$v['idcom']])){
							$tmp_max[$v['idcom']][] = $v['y'][0];
						}
						if(count($tmp_max[$v['idcom']])>1){
							$nilai_y = null;
						} else{
							$nilai_y = $v['y'][0];
						}
					}

					$data_lagi = $value[0];
					$data_lagi['y'] = array_fill(0, count($data_lagi['y']), $nilai_y);
					if($nilai_y != null){
						$ab[$k]['data'][] = $data_lagi;
					}
					$k++;
				}
			}
			$tmp_min = [];
			if (isset($min)) {
				$k = 0;
				foreach ($min as $key => $value) {
					$ck = $min[$key];
					$nilai_y;
					$double = 0;
					foreach ($ck as $ky => $v) {
						if(!in_array($v['y'][0], $tmp_min[$v['idcom']])){
							$tmp_min[$v['idcom']][] = $v['y'][0];
						}
						if(count($tmp_min[$v['idcom']])>1){
							$nilai_y = null;
						} else{
							$nilai_y = $v['y'][0];
						}
					}

					$data_lagi = $value[0];
					$data_lagi['y'] = array_fill(0, count($data_lagi['y']), $nilai_y);
					if($nilai_y != null){
						$ab[$k]['data'][] = $data_lagi;
					}
					$k++;
				}
			}
			
			//Generate Layout
			foreach ($ab as $key => $value) {
				$lay[$key]['layout'] = array(
					"title" => "Quality Trend - $tPer<BR>" . trim($value['data'][0]['parameter']),
					"xaxis" => array(
						"title" => "",
						"titlefont" => array('size' => ($dh_ly=='H') ? 18:14),
						"tickfont" => array('size' => ($dh_ly=='H') ? 5:10),
						"autorange" => TRUE
					),
					"yaxis" => array("title" => 'NILAI'),
					"legend"=> array(
						"font" => array("size" => 10)
					),
					"showlegend" => TRUE
				);
			}
			$plotdata['msg'] = "200";
		}else{
			$plotdata['msg'] = "404";
		}
		// $plotdata['query'] = $temp;
		$plotdata['max'] = $max;
		$plotdata['entah'] = $entah;
		$plotdata['tmp_max'] = $tmp_max;
		$plotdata['tmp_max2'] = $tmp_max2;
		// $plotdata['keys'] = $keys;
		// $plotdata['values'] = $values;
		$plotdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$plotdata['layout'] = array_values((empty($lay)) ? array(0): $lay);
		to_json($plotdata);
	}
	
	private function getColor($num) {
        $hash = md5('warnawarni' . $num);
        return array(hexdec(substr($hash, 0, 2)), hexdec(substr($hash, 2, 2)), hexdec(substr($hash, 4, 2)));
	}

}

/* End of file Quality_trend.php */
/* Location: ./application/controllers/Quality_trend.php */
?>
