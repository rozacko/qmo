<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hebat_graph extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_product");
		$this->load->model("M_hebat_report");
	}
	
	public function index(){
		$this->libExternal('select2,highcharts');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_product = $this->m_product->datalist();
		$this->template->adminlte("v_hebat_graph_cement_progress2");
	}

	public function cement_progress(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_cement_progress");
	}

	public function clinker_progress(){
		$this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_clinker_progress");
	}

	public function qaf_st(){
		$this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_qaf_st");
	}

	public function qaf_cs(){
		$this->libExternal('select2');
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_qaf_cs");
	}

	public function score_input_cement(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_input_cement");
	}

	public function score_input_production(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_input_production");
	}

	public function score_ncqr(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_ncqr");
	}

	public function generate_json($tipe){
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');
		$id_plant  = $this->input->post('id_plant');
		$id_product  = $this->input->post('id_product');

		if(!$_POST['opt_company']){
			echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
			exit;
		}
		if(!$_POST['tahun']){
			echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
			exit;
		}
		$param = array();
		$param_in = array();

		$param['TAHUN'] = $tahun;
		$param['ID_COMPANY'] = $opco;

		if($id_plant){
			$param_in['ID_PLANT'] = $id_plant;
		}
		if($id_product){
			$param_in['ID_PRODUCT'] = $id_product;
		}

		switch (strtolower($tipe)) {
			case 'cement':
				$query = "qaf_cement_progress2";
				$title = "QAF CEMENT PROGRESS";
				break;

			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			case 'score_input_cement':
				$query = "score_input_cement";
				$title = "SCORE INPUT CEMENT";
				break;
			
			case 'score_input_production':
				$query = "score_input_production";
				$title = "SCORE INPUT PRODUCTION";
				break;
			
			case 'score_ncqr':
				$query = "score_ncqr";
				$title = "SCORE NCQR";
				break;
			
			default:
				$query = "qaf_cement_progress";
				$title = "QAF CEMENT PROGRESS";
				break;
		}
		if($query== 'qaf_cement_progress2'){
			$result = $this->M_hebat_report->{$query}($param, $param_in);
		}else{
			$result = $this->M_hebat_report->{$query}($opco, $tahun);
		}

		// var_dump($result);
		// exit;

		// echo $this->db->last_query();die();
		$temp   = array();
		$anno 	= array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $key => $res) {
				$color = monthColor($res->BULAN);
				$temp[$key]['x'][]    = $res->NILAI;
				$temp[$key]['type']   = 'bar';
				$temp[$key]['marker'] = array('color' => "rgb($color, 1.0)");
				$temp[$key]['name']   = strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10))) . ', ' . $res->NILAI;
			}

			$lastMonth   = $temp[count($temp)-1];
			$lastMonth_2 = $temp[count($temp)-2];
			$arrow_value = $lastMonth['x'][0]-$lastMonth_2['x'][0];
			$arrow_text	 = ($arrow_value < 0) ? $arrow_value:'+'.$arrow_value; 
			$arrow_color = ($arrow_value < 0) ? "rgb(214, 39, 40)":"rgb(44, 160, 44)"; 
			$arrow_sign  = ($arrow_value < 0) ? -30:30; 

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => false,
								"title" => "",
								//"range" => array(0,1),
								"showline" => true,
								"type" => '-',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['showlegend'] 	= true;
			$lay['barmode'] = 'group';
			$lay['xaxis'] 	= array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => "TOTAL QAF",
								//"range" => array(-0.5,0.5),
								"showline" => true,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.25
								)
							);
			$lay['title'] 		= $title;
			$lay['hovermode'] 	= 'closest';
			$lay['margin'] 		= array("l" => 200);

			if (count($temp)>1) {
				$lay['annotations'] = array(
					array(
						"xref" => "paper",
				        "yref" => "paper",
				        "text" => "<b>$arrow_text%</b>",
				        "arrowwidth" => 4,
				        "arrowsize" => 1,
				        "ay" => $arrow_sign,
				        "ax" => 0,
				        "y" => 0.5,
				        "x" => 1.1,
				        "font" => array("color" => $arrow_color),
				        "arrowcolor" => $arrow_color,
						"showarrow" => true
					)
				);
			}


			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= $temp;
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}

	public function json_cement_progress(){
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');

		$result = $this->M_hebat_report->qaf_cement_progress($bulan, $tahun, $opco);
		$temp   = array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $res) {
				$color 			= monthColor($res->BULAN);
				$temp['x'][]  	= $res->TOTAL_QAF;
				$temp['type'] 	= 'bar';
				$temp['marker'] = array('color' => "rgb($color, 1.0)");
				$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
			}

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => false,
								"title" => "",
								//"range" => array(0,1),
								"showline" => true,
								"type" => '-',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['showlegend'] 	= true;
			$lay['barmode'] = 'group';
			$lay['xaxis'] 	= array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => "TOTAL QAF",
								//"range" => array(-0.5,0.5),
								"showline" => true,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.23
								)
							);
			$lay['title'] 		= 'QAF CEMENT PROGRESS';
			$lay['hovermode'] 	= 'closest';
			$lay['margin'] 		= array("l" => 200);
			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= array($temp);
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);

	}

	public function json_line($tipe=''){
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');
		if(!$_POST['opt_company']){
			echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
			exit;
		}
		if(!$_POST['tahun']){
			echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
			exit;
		}

		switch ($tipe) {
			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			default:
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;
		}

		if($query == 'qaf_clinker_progress'){
			$id_plant  = $this->input->post('id_plant');

			$param = array();
			$param_in = array();

			$param['TAHUN'] = $tahun;
			$param['ID_COMPANY'] = $opco;

			if($id_plant){
				$param_in['ID_PLANT'] = $id_plant;
			}

			$result = $this->M_hebat_report->qaf_clinker_progress2($param, $param_in);
		}else{
			$result = $this->M_hebat_report->{$query}($opco, $tahun);
		}
		
		// $result = $this->M_hebat_report->{$query}($opco, $tahun);

		// print_r($result);
		// exit;
		#echo $this->db->last_query();die();

		$temp   = array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $res) {
				$color 			= monthColor($res->BULAN);
				$temp['x'][]  	= $res->BULAN;
				$temp['y'][]  	= $res->NILAI;
				$temp['type'] 	= 'scatter';
				$temp['line'] 	= array('dash' => 'dashdot');
				$temp['marker'] = array('symbol' => "square", "size" => 8);
				$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
				$temp['hoverinfo'] 	= 'x+y';
			}

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => 'TOTAL QAF',
								"showline" => true,
								"showgrid" => true,
								"gridwidth" => 4,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['xaxis'] 	= array(
								"title" => "MONTH",
								"showgrid" => false,
								"showline" => true,
								"type" => 'linear',
								"autorange" => false,
								"range" => array(0,12),
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.23
								)
							);
			$lay['title'] 		= $title;
			$lay['showlegend'] 	= false;
			$lay['hovermode'] 	= 'closest';
			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= array($temp);
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}
	

	public function generate_json3dchart($tipe){
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');
		$id_plant  = $this->input->post('id_plant');
		$id_product  = $this->input->post('id_product');

		if(!$_POST['opt_company']){
			echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
			exit;
		}
		if(!$_POST['tahun']){
			echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
			exit;
		}
		$param = array();
		$param_in = array();

		$param['TAHUN'] = $tahun;
		$param['ID_COMPANY'] = $opco;

		if($id_plant){
			$param_in['ID_PLANT'] = $id_plant;
		}
		if($id_product){
			$param_in['ID_PRODUCT'] = $id_product;
		}

		switch (strtolower($tipe)) {
			case 'cement':
				$query = "qaf_cement_progress2";
				$title = "QAF CEMENT PROGRESS";
				break;

			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			case 'score_input_cement':
				$query = "score_input_cement";
				$title = "SCORE INPUT CEMENT";
				break;
			
			case 'score_input_production':
				$query = "score_input_production";
				$title = "SCORE INPUT PRODUCTION";
				break;
			
			case 'score_ncqr':
				$query = "score_ncqr";
				$title = "SCORE NCQR";
				break;
			
			default:
				$query = "qaf_cement_progress";
				$title = "QAF CEMENT PROGRESS";
				break;
		}
		if($query== 'qaf_cement_progress2'){
			$result = $this->M_hebat_report->{$query}($param, $param_in);
		}else{
			$result = $this->M_hebat_report->{$query}($opco, $tahun);
		}
 		
 		$colors = array('#594a2d','#660057','#6e8cff','#3d0c4e','#9ce6ae','#009e78','#001f8f','#ff7852','#ebff26','#0073e6','#f05eff','#00decc','#594a2d'); 

		$series    = array();
 		if($result){
			for($i=1; $i<=12; $i++)
			{
			    $flag = 0;
			    foreach($result as $list){
			        if($list->BULAN == $i){
						$row['y'] = (int)$list->NILAI;
						$row['color'] = $colors[$i];

			            $series[] = $row;
			            $flag = 1;
			        }
			    }
			    if($flag == 0)
			    {
			    	$row['y'] = 0;
					$row['color'] = $colors[$i];
			        $series[] = $row;
			    }
			}
			$code = 'success';
 		}else{
			$code = 'error';
 		}

 		$data['status'] = $code; 
 		$data['series'] = $series;
 		$data['image'] = "https://dms.sisi.id/assets/images/logoo/" . $opco . ".jpeg";
		to_json($data);
	}

	public function calculate_qaf_cement(){
		// $this->M_hebat_report->calculate_qaf_cement($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "dones";
	}
	
	// public function calculate_qaf_cement(){
	// 	$param = $this->input->post();
	// 	if(!$_POST['ID_COMPANY']){
	// 		echo json_encode(array('status' => 'error', 'message' => 'Company must be selected!'));
	// 		exit;
	// 	}
	// 	if(!$_POST['YEAR']){
	// 		echo json_encode(array('status' => 'error', 'message' => 'Year must be selected!'));
	// 		exit;
	// 	}

	//     $param['ID_PLANT'] = implode(',', $param['ID_PLANT']);
	//     $param['ID_PRODUCT'] = implode(',', $param['ID_PRODUCT']);

	// 	// var_dump($param);
	// 	// exit;
	// 	// $this->M_hebat_report->hitung_qaf_daily($param);
	// 	echo "done";
	// }


	public function calculate_qaf_cs(){
		// $this->M_hebat_report->calculate_qaf_cs($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}
	
	public function calculate_qaf_st(){
		// $this->M_hebat_report->calculate_qaf_st($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}
	
	public function async_list_product_qaf(){
		$this->load->model("c_range_qaf");
	    $ID_PLANT = null;
	    $data = array();
		if($_POST['id_plant']){
			$ID_PLANT = $this->input->post('id_plant');
		    $ID_PLANT = implode(',', $ID_PLANT);
		}

		if($ID_PLANT){
			$data = $this->c_range_qaf->list_configured_product_qaf_wherein($ID_PLANT); # echo $this->c_range_qaf->get_sql();
		}
		echo json_encode($data);
	}

}

/* End of file Hebat_graph.php */
/* Location: ./application/controllers/Hebat_graph.php */
?>
