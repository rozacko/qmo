<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trend_graph extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
		$this->load->model("c_product");
		$this->load->model("c_parameter");
		$this->load->model("m_product");
		$this->load->model("m_component");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("c_qaf_component");
		$this->load->model("c_range_qaf");
	}
	
	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist_auth($this->USER->ID_GROUPAREA);
		$this->list_product   = $this->m_product->datalist();
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_trend_graph");
	}

	public function ajax_get_area_by_company(){
		$opt_c = (NULL==$this->input->post('opt_c'))? 0:$this->input->post('opt_c');
		$opt_g = (NULL==$this->input->post('opt_g'))? 0:$this->input->post('opt_g');
		$area = $this->m_area->or_where($opt_c, $opt_g);
		to_json($area);
	}

	public function ajax_get_product_by_area(){
		$tmp = array();
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');
        foreach ($opt as $a) {
            if($a=='semua'){
                unset($opt[0]);
            }
		}  
		$c_product = $this->c_product->or_where($opt);

		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
		}

		$tmp = array_count_values($tmp);

		foreach ($tmp as $key => $value) {
			if (count($opt) > 1) {
				if($value < 2) {
					unset($tmp[$key]);
				}
			}
		}
		
		$tmp = array_keys($tmp);

		foreach ($c_product as $key => $prod) {
			if (!in_array($prod->ID_PRODUCT, $tmp)) {
				unset($c_product[$key]);
			}
		}
		
		to_json($c_product);
	}
    // start create izza jan 2021
    public function ajax_get_product_by_area_read(){ // izza jan 2021
		$tmp = array();
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');
		// $opt = ['434'];
        foreach ($opt as $a) {
            if($a=='semua'){
                unset($opt[0]);
            }
		}  
		// $c_product = $this->c_product->or_where($opt);
		$c_product = $this->c_product->or_where_read($opt); // izza
        // $tmp = array_count_values($tmp);

		// foreach ($tmp as $key => $value) {
		// 	if (count($opt) > 1) {
		// 		if($value < 2) {
		// 			unset($tmp[$key]);
		// 		}
		// 	}
		// }

		// $tmp = array_keys($tmp);

		// foreach ($c_product as $key => $prod) {
		// 	if (!in_array($prod->ID_PRODUCT, $tmp)) {
		// 		unset($c_product[$key]);
		// 	}
		// }

		// to_json($c_product);
		
		// izza 29.07.21
		$id_tmp = []; $id=0;
		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
			$id_tmp[] = $id;
			$id++;
		}
		$c_product_fix = [];
		$tmp2 = [];
		$id=0;
		foreach ($tmp as $key) {
			if(!in_array($key, $tmp2)){
				$tmp2[] = $tmp[$id];
				$c_product_fix[] = $c_product[$id];
			}
			$id++;
		}
		to_json($c_product_fix);
	}
    // end create izza jan 2021
	public function ajax_get_component(){
		$area = (null !== $this->input->post('area')) ? $this->input->post('area'):array(0);
		$group = $this->input->post('group');
		$dh_ly = $this->input->post('dh_ly');
		foreach ($area as $val) {
            if($val!='semua'){
                $plant[] = $this->m_area->get_data_by_id($val)->ID_PLANT;
            }
		}
		
		$c_parameter = $this->c_parameter->or_where($plant, $group, $dh_ly);
		// echo $this->db->last_query();die();
		to_json($c_parameter);
	}

	public function test_boxplot(){
		$this->template->adminlte("testing_boxplot");
	}
    
    // function stringDivider($str, $width, $spaceReplacer) {
        // if (strlen($str)>$width) {
            // $p=$width;
            // for ($p>0 && $str[$p]!=' ';$p--) {
                // if ($p>0) {
                    // $left = substr($str, 0, $p);
                    // $right = substr($str, $p+1);
                    // return $left + $spaceReplacer + stringDivider($right, $width, $spaceReplacer);
                // }
            // }
        // }
        // return $str;
    // }
    
	public function generate_boxplot(){
		$date 		= array($this->input->post('start'), $this->input->post('end'));
		$dh_ly	 	= $this->input->post('dh_ly');
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= $this->input->post('param');
		$component 	= array_filter($component, 'is_numeric');
		$mod   		= (in_array($grouparea, array("1","4", "81"))) ?  (($dh_ly=="D") ? "t_cement_daily":"t_cement_hourly"):(($dh_ly=="D") ? "t_production_daily":"t_production_hourly");
        $parameter	= array($product, $component);
		$boxdata 	= array();
        foreach ($area as $a) {
            if($a=='semua'){
                unset($area[0]);
            }
        }
        // echo $mod;
		$production = $this->{$mod}->get_nilai($date, $area, $parameter, 1);
        // echo json_encode($production);
        // exit();
		#var_dump($component);
		#echo $this->db->last_query();die();
        // $cuu = stringDivider("Very Long Text Label That Doesn't Fit Properly", 20, "<br>");
		// echo $cuu;

        $color_arr = array( 0 => "176,196,222", #light steel blue
            1 => "176,0,186",   #purple
            2 => "0,255,0",     #lime
            3 => "0,0,255",     #blue
            4 => "255,255,0",   #yellow
            5 => "255,0,255",   #magenta
            6 => "0,255,255",   #cyan/aqua
            7 => "0,255,127",   #spring green
            8 => "138,43,226",  #blue violet
            9 => "255,228,225",  #misty rose
            10 => "255,243,0",
            11 => "173,179,54",
            12 => "181,204,41",
            13 => "180,230,23",
            14 => "170,255,0",
            15 => "125,179,54",
            16 => "119,204,41",
            17 => "102,230,23",
            18 => "73,255,0",
            19 => "77,179,54",
            20 => "56,204,41",
            21 => "23,230,23",
            22 => "0,255,24",
            23 => "54,179,77",
            24 => "41,204,87",
            25 => "23,230,102",
            26 => "0,255,121",
            27 => "54,179,125",
            28 => "41,204,150",
            29 => "23,230,180",
            30 => "0,255,219",
            31 => "54,179,173",
            32 => "41,196,204",
            33 => "23,200,230",
            34 => "0,194,255",
            35 => "54,137,179",
            36 => "41,134,204",
            37 => "23,121,230",
            38 => "0,97,255",
            39 => "54,89,179",
            40 => "41,72,204",
            41 => "23,43,230",
            42 => "0,0,255",
            43 => "65,54,179",
            44 => "72,41,204",
            45 => "82,23,230",
            46 => "97,0,255",
            47 => "113,54,179",
            48 => "134,41,204",
            49 => "161,23,230",
            50 => "194,0,255",
            51 => "161,54,179",
            52 => "196,41,204",
            53 => "230,23,220",
            54 => "255,0,219",
            55 => "179,54,149",
            56 => "204,41,150",
            57 => "230,23,141",
            58 => "255,0,121",
            59 => "179,54,101",
            60 => "204,41,87",
            61 => "230,23,62",
            62 => "255,0,24",
            63 => "179,54,179"
        );
		if($production){
			$x = 0;
			foreach ($production as $prod) {
				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$num = rand(0,63);
						$color = $color_arr[$num];
						unset($num);
						$x+=1;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['company'] 	= $prod->ID_COMPANY;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['idc'] 	    = trim($prod->ID_COMPONENT);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter'] 	= trim($prod->KD_COMPONENT);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] 		= trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] 		= array('color'=>"rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['boxmean']		= TRUE;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][] 		= $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] 		= array('width' => "1.5");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] 		= "box";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['boxpoints'] 	= false;
					}
				}
			}
			
			//var_dump ($cmp);
			//die();
			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}

            $max_min = array();
            $u=0;

            foreach ($ab as $key => $value) {
                $max_min['param'] = $value['data'][0]['parameter'];
                $max_min['max'] = [];
                $max_min['min'] = [];
                foreach ($value['data'] as $keys => $values) {
                    $max = max($values['y']);
                    $min = min($values['y']) == null ? '' : min($values['y']);
                    array_push($max_min['max'], $max);
                    array_push($max_min['min'], $min);
                    $u++;
                }
                $val[] = $max_min;
            }
            
			//Generate Layout
            $l=0;
			foreach ($ab as $key => $value) {
                // echo "<pre>";
                // print_r($value['data'][0]);
                //Get Max Min 
                $maxi = max($val[$l]['max']);
                $mini= min($val[$l]['min']);
                $l++;
                // $minmax = $this->c_range_qaf->configuration_component('10',$grouparea,$product,$value['data'][0]['idc']);
                $minmax = $this->c_range_qaf->configuration_component($value['data'][0]['company'],$grouparea,$product,$value['data'][0]['idc']); // izza 28.07.21
                
                $min_width = -0.3; // izza 25.06.2021
                // $max_width = count($value['data'])-1;
                $max_width = count($value['data'])-1 + 0.3; // izza 25.06.2021
                
                $line_max = array();
                $line_min = array();
                if($minmax['V_MAX'] == '9999' || $minmax['V_MAX'] == null){
                    // $line_max = array(
                    //     "type" => 'line',
                    //     "x0" => 0,
                    //     "y0" => $maxi,
                    //     "x1" => $max_width,
                    //     "y1" => $maxi,
                    //     "line" => array(
                    //         "color"=> 'rgb(50, 171, 96)',
                    //         "width"=> 2,
                    //         "dash"=> 'dashdot'
                    //     )
                    // );
                } else{
                    $line_max = array(
                        "type" => 'line',
                        "x0" => $min_width, // izza 25.06.2021
                        "y0" => $minmax['V_MAX'] == null ? $maxi : $minmax['V_MAX'],
                        "x1" => $max_width,
                        "y1" => $minmax['V_MAX'] == null ? $maxi : $minmax['V_MAX'],
                        "line" => array(
                            "color"=> 'rgb(50, 171, 96)',
                            "width"=> 2,
                            "dash"=> 'dashdot'
                        )
                    );
                }

                // if($minmax['V_MIN'] > 0){
                if($minmax['V_MIN'] == null){ // izza 23.07.2021
                //     $line_min = array(
                //         "type" => 'line',
                //         "x0" => $min_width, // izza 25.06.2021
                //         "y0" => $minmax['V_MIN'],
                //         "x1" => $max_width,
                //         "y1" => $minmax['V_MIN'],
                //         "line" => array(
                //             "color"=> '#dd4b39',
                //             "width"=> 2,
                //             "dash"=> 'dashdot'
                //         )
                //     );
                } else {
                    $line_min = array(
                        "type" => 'line',
                        "x0" => $min_width, // izza 25.06.2021
                        // "y0" => $mini,
                        "y0" => $minmax['V_MIN'] == null ? $maxi : $minmax['V_MIN'], // izza 28.07.2021
                        "x1" => $max_width,
                        // "y1" => $mini,
                        "y1" => $minmax['V_MIN'] == null ? $maxi : $minmax['V_MIN'], // izza 28.07.2021
                        "line" => array(
                            "color"=> '#dd4b39',
                            "width"=> 2,
                            "dash"=> 'dashdot'
                        )
                    );
                }
                
                // $maxi = is_null($minmax['V_MAX'])? 0 : $minmax['V_MAX'];
                // $mini = is_null($minmax['V_MIN'])? 0 : $minmax['V_MIN'];
                $lay[$key]['layout'] = array(
                    "title" => "Box Plot - " . trim($value['data'][0]['parameter']),
                    "paper_bgcolor" => "#F5F6F9",
                    "plot_bgcolor" => "#F5F6F9",
                    "height" => '700',
                    "xaxis" => array(
                        "tickfont" => array(
                            "color" => "#4D5663",
                            "size" => 9
                        ),
                        "gridcolor" => "#E1E5ED",
                        "titlefont" => array(
                            "color" => "#4D5663"
                        ),
                        "zerolinecolor" => "#E1E5ED",
                        "margin"=> array(
                            "l"=> 250,
                            "r"=> 50,
                            ),
                        // "title" => "PLANT - AREA"
                    ),
                    "legend" => array(
                        "bgcolor" => "#F5F6F9",
                        "font" => array(
                            "color" => "#4D5663",
                            "size" => 8
                        ),
                        "orientation" => "h",
                        "margin"=> array(
                            "r"=> 10,
                            "t"=> 50,
                            "b"=> 40,
                            "l"=> 60
                            ),
                    ),
                );
                if($maxi || $mini){
                    $lay[$key]['layout']['shapes'] = [
                            $line_max,
                            $line_min,
                    ];
                }
			}
			$boxdata['msg'] = "200";
		}else{
			$boxdata['msg'] = "404";
		}
		$boxdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$boxdata['layout'] 	= array_values((empty($lay)) ? array(0): $lay);
		to_json($boxdata);
	}
    

}

/* End of file Trend_graph.php */
/* Location: ./application/controllers/Trend_graph.php */
?>