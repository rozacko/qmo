<section class="content-header">
<h1> 
  Notify Reciever
  <small></small>
</h1>
</section>

<div class="col-xs-12">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">List data</h3>
      <a href="opco/add"><button class="pull-right btn btn-sm btn-primary">Create New</button></a>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table  id="tbList" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
          <tr>
              <th>No</th>
              <th>Name</th>
              <th>Email</th>
              <th>Company</th>
              <th>Plant</th>
              <th>Area</th>
              <th width="30%"></th>
          </tr>
          </thead>
        </table>
      </div>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</div><!-- /.col -->
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>

 function dtTbl(){
    $("#tbList").DataTable();
    $("#tbList").DataTable().destroy();

    $('#tbList').DataTable({
      "processing": true,
      "ajax": 'http://10.15.2.130/DEV/qmonline/opco/getDataList',
      "columns": [
        { "className": "text-center", "data": "NO" },
        { "data": "FULLNAME" },
        { "data": "EMAIL" },
        { "data": "NM_COMPANY" },
        { "data": "NM_PLANT" },
        { "data": "NM_AREA" },
        {
          "sortable": false,
          "className": "text-center",
          "render": function ( data, type, row, meta ) {
             var buttonList = '<div class="btn-group">'+
                              '<a style="color: #fff" href="opco/edit/'+row.ID_OPCO+'"><button type="button" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>'+
                              '<button type="button" id_opco="'+row.ID_OPCO+'" class="btn btn-danger btn-xs delete" data-title="Remove Group Employee" data-text="This employee group will be removed. Are you sure?"><i class="fa fa-times"></i></button>'+
                              '</div>';
             return buttonList;
           }
        },
      ],
      "initComplete":function( settings, json){
          $(document).on('click',".delete",function () {
            var id_opco = $(this).attr('id_opco');

            $.confirm({
                title: 'Delete',
                text: 'Are you sure?',
                confirmButton: 'Delete',
                confirmButtonClass: '',
                cancelButton: "Cancel",
                confirm: function() {
                  window.location.href = '<?php echo site_url("opco/delete/") ?>'+id_opco;
                },
                cancel: function() {
                    // nothing to do
                }
            });
          });
        }
    });
  }

  $(function() {
    dtTbl();
  });

</script>
