<?php

class Acclab_masterFormObservasi extends QMUser {
	
	public $list_data = array();
	public $list_panel = array();
	public $jsonData;
	public $key;
	public $data_groupmenu;
	 
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_acclab_temuanObservasi", "observasi");
	}
	
	public function index(){
		$this->list_data = $this->observasi->get_masterForm();
		$this->list_panel = $this->observasi->get_allTag();
		$this->template->adminlte("v_acclab_masterFormTemuanObservasi");
	}
	
	public function detail($id) {
		$this->key = $id;
		$this->list_data = $this->observasi->get_detailForm($id);
		$this->template->adminlte("v_acclab_detailFormObservasi");
	}

	public function addField() {
		$save = $this->observasi->saveField();
		$this->key = $this->input->post("fk");
		if($save){
			$this->notice->success("Data Created.");
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		}
	}
	
	public function updateField() {
		$update = $this->observasi->updateField();
		$this->key = $this->input->post("fk");
		if($update){
			$this->notice->success("Data Updated.");
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		}
	}

	public function deleteField($param) {
		$arr = explode("_", $param);
		$id = $arr[0];
		$this->key = $arr[1];
		$delete = $this->observasi->deleteField($id);
		if($delete){
			$this->notice->success("Data Deleted.");
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi/detail/" . $this->key);
		}
	}

	public function addForm() {
		$save = $this->observasi->saveForm();
		if($save){
			$this->notice->success("Data Created.");
			redirect("acclab_masterFormObservasi");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi");
		}
	}

	public function updateForm() {
		$update = $this->observasi->updateForm();
		if($update){
			$this->notice->success("Data Updated.");
			redirect("acclab_masterFormObservasi");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi");
		}
	}

	public function deleteForm($id) {
		$delete = $this->observasi->deleteForm($id);
		if($delete){
			$this->notice->success("Data Deleted.");
			redirect("acclab_masterFormObservasi");
		} else {
			$this->notice->error($this->acclab_periode->error());
			redirect("acclab_masterFormObservasi");
		}
	}


}	

?>
