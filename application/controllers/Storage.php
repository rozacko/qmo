<?php

class Storage extends QMUser {
	
	public $list_storage = array();
	public $list_company = array();
	
	public $ID_COMPANY;
	public $data_storage;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_storage");
		$this->load->model("m_company");
		$this->ID_COMPANY = ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$this->ID_COMPANY;
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->list_storage = $this->m_storage->datalist($this->ID_COMPANY); #echo $this->m_storage->get_sql();
		$this->template->adminlte("v_storage");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$ID_COMPANY = ($this->USER->ID_COMPANY)?$this->USER->ID_COMPANY:$ID_COMPANY;
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->list_company = $this->m_company->datalist($this->ID_COMPANY);
		$this->template->adminlte("v_storage_add");
	}
	
	public function create($ID_COMPANY=NULL){
		$this->m_storage->insert($this->input->post());
		if($this->m_storage->error()){
			$this->notice->error($this->m_storage->error());
			redirect("storage/add/".$this->input->post("ID_COMPANY"));
		}
		else{
			$this->notice->success("Storage Data Saved.");
			redirect("storage/by_company/".$this->input->post("ID_COMPANY"));
		}
	}
	
	public function edit($storage){

		$this->list_company = $this->m_company->datalist($this->ID_COMPANY);
		$this->data_storage = $this->m_storage->get_data_by_id($storage);
		$this->template->adminlte("v_storage_edit");
	}
	
	public function update($ID_STORAGE){
		$this->m_storage->update($this->input->post(),$ID_STORAGE);
		if($this->m_storage->error()){
			$this->notice->error($this->m_storage->error());
			redirect("storage/edit/".$ID_STORAGE);
		}
		else{
			$this->notice->success("Storage Data Updated.");
			redirect("storage/by_company/".$this->input->post("ID_COMPANY"));
		}
	}
	
	public function delete($ID_STORAGE,$ID_COMPANY=NULL){
		$this->m_storage->delete($ID_STORAGE);
		if($this->m_storage->error()){
			$this->notice->error($this->m_storage->error());
		}
		else{
			$this->notice->success("Storage Data Removed.");
		}
		redirect("storage/by_company/".$ID_COMPANY);
	}
	
	
	/* ajax */
	/* ajax */
	public function json_plant_list($ID_COMPANY){
		$data = $this->list_storage = $this->m_storage->datalist($ID_COMPANY);
		echo json_encode($data);
	}

	public function get_list(){
		$list = $this->m_storage->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_STORAGE;
			$row[] = $no;
			$row[] = $column->NM_COMPANY;
			$row[] = $column->NM_STORAGE;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_storage->count_all(),
            "recordsFiltered" => $this->m_storage->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
	
}	



?>
