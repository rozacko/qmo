<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Component_order extends QMUSER {

	public $list_grouparea;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_grouparea");
		$this->load->model("c_parameter_order");
		$this->load->model("m_component");
	}
	
	public function index(){
		$this->list_grouparea = $this->m_grouparea->datalist();
		$this->template->adminlte("v_component_order");
	}

	public function ajax_get_component($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=''){
		#$DISPLAY = ($ID_GROUPAREA=='1') ? $DISPLAY:NULL;
		$param = $this->c_parameter_order->get_component($ID_GROUPAREA,$DISPLAY);
		
		foreach ($param as $val):
			$rows[]  = array($val->KD_COMPONENT, $val->URUTAN);
			$id_comp[] = $val->ID_COMPONENT;
		endforeach;

		$header[]['title'] = 'COMPONENT';
		$header[]['title'] = 'COLUMN ORDER';
		$data['colHeader'] = $header;
		$data['rows'] 	   = count($rows);
		$data['product']   = $rows;
		$data['id_param']  = $id_comp;

		to_json($data);
	}

	public function save_table(){
		$post  = $this->input->post();
		$form  = $post['formData'];
		$data  = $post['data'];
		$id_comp = $post['id_comp'];
		$count = count($data);

		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		for ($i=0; $i < count($id_comp); $i++) { 
			$ins['ID_GROUPAREA'] = $form->ID_GROUPAREA;
			$ins['ID_COMPONENT'] = $this->m_component->get_data_by_id($id_comp[$i])->ID_COMPONENT;
			$ins['DISPLAY'] = ($form->ID_GROUPAREA=='1') ? $form->DISPLAY:NULL;
			$ins['URUTAN']  = $data[$i][1];

			$w = $this->c_parameter_order->check_exists(array("ID_GROUPAREA" => $form->ID_GROUPAREA, "ID_COMPONENT" => $ins['ID_COMPONENT'], "DISPLAY" => $ins['DISPLAY']));
			#echo $this->db->last_query();
			if (count($w)>0) {
				#echo "update";
				#var_dump($ins);
				$this->c_parameter_order->update_urutan($ins);
				#echo $this->db->last_query() . '<br>';
			}else{
				#echo "insert";
				#var_dump($ins);
				$this->c_parameter_order->insert($ins);
				#echo $this->db->last_query() . '<br>';
			}
		}

		to_json(array("result" => "ok"));

	}
}

/* End of file Component_order.php */
/* Location: ./application/controllers/Component_order.php */
?>
