<?php

class Telegram extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('TelegramNotification');
        $this->config->load('telegram', TRUE);
    }

    /**
     * Handle request from telegram for every single message
     */
    public function webhook() {
        $token = $this->input->get('token');
        $this->load->model('TelegramNotification');

        // A non-command message handle here
        $callable = function($update, $telegram, &$reason) {
            log_message('debug', json_encode($update));

            return true;
        };

        $this->TelegramNotification->handle_webhook_with_token($token, $callable);
    }

    /* 
     * WARN
     * This method is not intended to public access and should be used 
     * inside authorized page only.
     */
    public function set_webhook() {
        $telegram_config = @$this->config->item('default', 'telegram');

        $webhook_url = $this->input->get('url');

        if ( "" == $webhook_url ) {
            $webhook_url = $telegram_config['webhook_url'];
        }

        if ( "" == $webhook_url ) {
            $text = "Please provide webhook URL";

            print($text); 
            log_message('error', $text);

            return;
        }

        if ( $this->TelegramNotification->set_webhook($webhook_url) ) {
            $text = 'Telegram webhook set to ' . $webhook_url;
            log_message('debug', $text);

            echo $text;
        }
        else {
            $text = 'Error set webhook to ' . $url;
            log_message('error', $text);

            echo $text;
        }
    }
}
