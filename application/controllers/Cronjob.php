<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'controllers/Incident.php'); 
use SAPNWRFC\Connection as SapConnection;
use SAPNWRFC\Exception as SapException;

class Cronjob extends Home {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("C_range_component");
        $this->load->model("m_incident");
		$this->load->model("m_t_notifikasi");
        // $this->load->model("m_pis");
        $this->load->model("m_cronjob");
        $this->load->model("t_cement_daily");
        $this->load->model("t_production_daily");
		
	}
        
	public function pis_clinker(){
            // Create Table
            // $today =  date("Ymd", strtotime("+1 day"));
            $today =  date("Ymd");
            $this->m_pis->create_table($today);            
        
            $data =  file_get_contents("http://par4digma.semenindonesia.com/api/index.php/plant_rembang/qm_clinker");
            
            $data1 = str_replace("<title>Json</title>","",$data);
            $data2 =  str_replace("(", "[",$data1); 
            $data3 =str_replace(");", "]",$data2);
            $decode = json_decode($data3, TRUE);
            foreach($decode  as $val){
                foreach($val['tags']  as $value){
                    if($value['name']=="QCX_Clinker_C3S"){
                        $nc3s = $value['name'];
                        $c3s = $value['props'][0]['val'];
                    }else if($value['name']=="QCX_Clinker_FCao"){
                        $nfcao = $value['name'];
                        $fcao = $value['props'][0]['val'];
                    }else if($value['name']=="QCX_Clinker_LSF"){
                        $nlsf = $value['name'];
                        $lsf = $value['props'][0]['val'];
                    }else if($value['name']=="QCX_Clinker_Temp"){
                        $ntemp = $value['name'];
                        $temp = $value['props'][0]['val'];
                    }else{
                        $ntanggal = $value['name'];
                        $tanggal = $value['props'][0]['val'];
                        $myDateTime = DateTime::createFromFormat('d/m/Y H.i.s', $tanggal);
                        $tanggal_c = $myDateTime->format('Y-m-d H:i:s');
                    }
                }
            }
            if($tanggal_c!=''){
             $data1=array(
                  'date_log'=> $tanggal_c, 
                  'company'=> "5000",
                  'machine_id' => $nc3s,
                  'value' =>$c3s
              );
             $data2=array(
                  'date_log'=> $tanggal_c, 
                  'company'=> "5000",
                  'machine_id' => $nfcao,
                  'value' =>$fcao
              );
             $data3=array(
                  'date_log'=> $tanggal_c, 
                  'company'=> "5000",
                  'machine_id' => $nlsf,
                  'value' =>$lsf
              );
             $data4=array(
                  'date_log'=> $tanggal_c, 
                  'company'=> "5000",
                  'machine_id' => $ntemp,
                  'value' =>$temp
              );
              $query=$this->m_pis->save('qmo_clinker_'.$today, $data1);
              $query=$this->m_pis->save('qmo_clinker_'.$today, $data2);
              $query=$this->m_pis->save('qmo_clinker_'.$today, $data3);
              $query=$this->m_pis->save('qmo_clinker_'.$today, $data4);
            
              echo 'qmo_clinker_'.$today.' '. $tanggal_c;
            }
    }

    public function index(){
        // $tanggal = $this->input->post('tanggal');
        // $myDateTime = DateTime::createFromFormat('d/m/Y', $tanggal);
        // $tanggal_c = $myDateTime->format('Ymd');
        // $tanggal_c = '20180810';
        // print_r($decode);exit;
        // $id_area = $this->input->post('area');
        $id_area = "88"; // Area rembang
        $id_product = '';
        // $plant = $this->input->post('plant');
        $plant = "18"; // Plant clinker (Rembang)
        // $grouparea = $this->input->post('grouparea');
        $grouparea = "4"; // Kiln
        $component = $this->c_parameter->configuration($plant, $grouparea,'H');
        
        // if($tanggal==date('d/m/Y')){
            // $now = date('H');
        // }else{
            $now = 24;
        // }
        $cek_insert = 0;
        $cek_update = 0;
        
        // Perulangan Dua Hari
        for($x=1;$x<=2;$x++){
            if($x==1){
                $tanggal = date('d/m/Y');
                $tanggal_c = date("Ymd");   
            }else{
                $tanggal =  date("d/m/Y", strtotime("-1 day"));
                $tanggal_c =  date("Ymd", strtotime("-1 day"));
            }
            
            /// Get data
            $data =  file_get_contents("http://10.15.5.150/dev/par4digma/api/index.php/plant_rembang/dump_qmtbl_clinker?ymd=".$tanggal_c."");
            $decode = json_decode($data, TRUE);
            
            for($i=1;$i<=$now;$i++){
                $ada = '0';
                $ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($id_area,$id_product,$tanggal,$i);
                $tdata['ID_AREA']			= $id_area;
                $tdata['ID_PRODUCT']		= $id_product;
                $tdata['ID_MESIN_STATUS']	= '';
                $tdata['LOCATION']		    = '';
                $tdata['MESIN_REMARK']		= '';
                $tdata['DATE_DATA']			= $tanggal; # dd/mm/yyyy
                $tdata['JAM_DATA']			= $i;
                $status_mesin 				= '';

                $listID= array();
                foreach($decode  as $val){
                    // $jam_data=intval($val['hourmin'])+1;
                    $jam_data= intval(date("H", strtotime($val['hourmin'])))+1;
                    
                    if($jam_data==$i){
                        // echo $jam_data."-".$i."<br>";
                        $ada = '1';
                        // PROSES gg
                        if(!$ID_CEMENT_HOURLY){
                            $tdata['DATE_ENTRY'] = date("d/m/Y");
                            $tdata['JAM_ENTRY']  = date("H");
                            $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                            $cek_insert += 1;
                            $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                        }
                        else{
                            $tdata['DATE_ENTRY'] = date("d/m/Y");
                            $tdata['JAM_ENTRY']  = date("H");
                            $tdata['USER_UPDATE'] = $this->USER->ID_USER;
                            $cek_update += 1;
                            $this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
                        }
                        // NIlai COMPONENT
                        
                        
                        $no_field=0;
                        foreach ($component as $col) {
                            $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                            $nm = trim($cmp->KD_COMPONENT," ");
                            if($nm=='C3S'){
                                $nilai = $val['cl_c3s'];
                            }else if($nm=='FCaO'){
                                $nilai =$val['cl_fcao'];
                                // $nilai =$val['cl_fcao'];
                            }else if($nm=='LSF'){
                                $nilai =$val['cl_lsf'];
                            }else if($nm=='TEMP'){
                                $nilai =$val['cl_temp'];
                            }else{
                                $nilai ='';
                            }
                            
                            $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                            $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                            $ddata['NILAI']				= $this->str_clean($nilai);
                            $ddata['NO_FIELD']			= "$no_field";
                            
                            //param ncqr set
                            $paramsNcqr['ID_PLANT'] = $plant; 
                            $paramsNcqr['ID_AREA'] = $id_area; 
                            $paramsNcqr['ID_COMPONENT'] = $col->ID_COMPONENT;
                            $paramsNcqr['NILAI'] =  $this->str_clean($nilai);
                            $paramsNcqr['TANGGAL'] = "'".date('d-M-y', strtotime($tanggal))."'"; //d-M-y
                            $paramsNcqr['JAM_DATA'] = $i; 
                            $paramsNcqr['ID_CEMENT_HOURLY'] = $ID_CEMENT_HOURLY;
                            $paramsNcqr['ID_PRODUCT'] = $id_product;
                            $paramsNcqr['ID_MESIN_STATUS'] = $tdata['ID_MESIN_STATUS'];


                            if(!$this->t_cement_hourly->d_exists($ddata)){
                                $this->t_cement_hourly->d_insert($ddata);
                                    echo 2;
                            }
                            else{
                                if($nm=='C3S'||$nm=='FCaO'||$nm=='LSF'||$nm=='TEMP'){
                                    echo 1;
                                    $this->t_cement_hourly->d_update($ddata);
                                }
                            }
                            
                            //Set NCQR -------------------
                            $this->m_incident->chek_and_setNCQR($paramsNcqr);

                            //Check NCQR ---------------------
                            $paramIncident['ID_AREA'] = $id_area;
                            $paramIncident['ID_NCQR_STATUS'] = 1;
                            $paramIncident['SEND_NOTIF'] = 'N';

                            if($grouparea == 1){
                                $paramIncident['ID_COMPONENT'] = '15,13,7';
                                $paramIncident['ID_PRODUCT'] = $id_product;
                            }else{
                                $paramIncident['ID_COMPONENT'] = '12';
                            }

                            $incident = $this->m_incident->ncqr_incident($paramIncident);

                            if(count($incident) > 0){ 
                                $listID = $this->getIncident($paramIncident);
                            }

                            $no_field +=1;
                        }
                    }
                }
                    if($ada == '0'){
                    // Hanya insert
                        if(!$ID_CEMENT_HOURLY){
                            
                            $cek_insert += 1;
                            $tdata['DATE_ENTRY'] = date("d/m/Y");
                            $tdata['JAM_ENTRY']  = date("H");
                            $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                                
                             $cek +=1;
                            // echo '1';
                            $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                            
                            $cek +=1;
                            $no_field=0;
                            foreach ($component as $col) {
                                $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                                $nm = trim($cmp->KD_COMPONENT," ");
                                $nilai ='';
                            
                                $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                                $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                                $ddata['NILAI']				= $this->str_clean($nilai);
                                $ddata['NO_FIELD']			= "$no_field";
                                
                                // //param ncqr set
                                // $paramsNcqr2['ID_PLANT'] = $plant; 
                                // $paramsNcqr2['ID_AREA'] = $id_area; 
                                // $paramsNcqr2['ID_COMPONENT'] = $col->ID_COMPONENT;
                                // $paramsNcqr2['NILAI'] =  $this->str_clean($nilai);
                                // $paramsNcqr2['TANGGAL'] = "'".date('d-M-y', strtotime($tanggal))."'"; //d-M-y
                                // $paramsNcqr2['JAM_DATA'] = $i; 
                                // $paramsNcqr2['ID_CEMENT_HOURLY'] = $ID_CEMENT_HOURLY;
                                // $paramsNcqr2['ID_PRODUCT'] = $id_product;
                                // $paramsNcqr2['ID_MESIN_STATUS'] = $tdata['ID_MESIN_STATUS'];


                                $this->t_cement_hourly->d_insert($ddata);
                                
                                // //Set NCQR -------------------
                                // $this->m_incident->chek_and_setNCQR($paramsNcqr2);

                                // //Check NCQR ---------------------
                                // $paramIncident2['ID_AREA'] = $id_area;
                                // $paramIncident2['ID_NCQR_STATUS'] = 1;
                                // $paramIncident2['SEND_NOTIF'] = 'N';

                                // if($grouparea == 1){
                                //     $paramIncident2['ID_COMPONENT'] = '15,13,7';
                                //     $paramIncident2['ID_PRODUCT'] = $id_product;
                                // }else{
                                //     $paramIncident2['ID_COMPONENT'] = '12';
                                // }

                                // $incident2 = $this->m_incident->ncqr_incident($paramIncident2);

                                // if(count($incident2) > 0){ 
                                //     $listID = $this->getIncident($paramIncident2);
                                // }

                                $no_field +=1;
                            }
                        }
                    }
            }
        }
		$paramIncident['ID_AREA'] = $id_area;
        $paramIncident['ID_NCQR_STATUS'] = 1;

		if($grouparea == 1){
    		$paramIncident['ID_COMPONENT'] = '15,13,7';
            $paramIncident['ID_PRODUCT'] = $id_product;
    	}else{
    		$paramIncident['ID_COMPONENT'] = '12';
    	}

		$incident = $this->m_incident->ncqr_incident($paramIncident);

		$statusInc = 'ok';

		$param = array(
			'ID_AREA' => $id_area,
			'ID_GROUPAREA' => $grouparea,
            'ID_PRODUCT' => $id_product
        );

		if(count($incident) > 0){
			$this->getIncident($param);
		}
        echo "berhasil";
    	// to_json(array("insert" => $cek_insert,"update" => $cek_update,"result" => 'ok'));      

	}


	private function get_component($id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'H');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->KD_COMPONENT);
		}

		//Tambahkan Remark
		if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';
		$header[]['title'] = 'KODE';
		$header[]['title'] = 'LOKASI';
		$header[]['title'] = 'TYPE';

		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';
		$id_comp[] = '_kode';
		$id_comp[] = '_lokasi';
		$id_comp[] = '_type';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	//FASE 2-----------------------------------

	public function insert_from_qcx(){
        // $tanggal = $this->input->post('tanggal');
        // $myDateTime = DateTime::createFromFormat('d/m/Y', $tanggal);
        // $tanggal_c = $myDateTime->format('Ymd');
        // $tanggal_c = '20180810';
        $tanggal = date('d/m/Y');
        $tanggal_c = date("Ymd");
		$data =  file_get_contents("http://10.15.5.150/dev/par4digma/api/index.php/plant_rembang/dump_qmtbl_clinker?ymd=".$tanggal_c."");
        $decode = json_decode($data, TRUE);
        // print_r($decode);exit;
        // $id_area = $this->input->post('area');
        $id_area = "88"; // Area rembang
        $id_product = '';
        // $plant = $this->input->post('plant');
        $plant = "18"; // Plant clinker
        // $grouparea = $this->input->post('grouparea');
        $grouparea = "4"; // Kiln
        $component = $this->c_parameter->configuration($plant, $grouparea,'H');
        
        if($tanggal==date('d/m/Y')){
            $now = date('H');
        }else{
            $now = 24;
        }
        $cek_insert = 0;
        $cek_update = 0;
        for($i=1;$i<=$now;$i++){
            $ada = '0';
            $ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($id_area,$id_product,$tanggal,$i);
            $tdata['ID_AREA']			= $id_area;
            $tdata['ID_PRODUCT']		= $id_product;
            $tdata['ID_MESIN_STATUS']	= '';
            $tdata['LOCATION']		    = '';
            $tdata['MESIN_REMARK']		= '';
            $tdata['DATE_DATA']			= $tanggal; # dd/mm/yyyy
            $tdata['JAM_DATA']			= $i;
            $status_mesin 				= '';
            
            foreach($decode  as $val){
                $jam_data=intval($val['hourmin'])+1;
                if($jam_data==$i){
                    $ada = '1';
                    // PROSES gg
                    if(!$ID_CEMENT_HOURLY){
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                        $cek_insert += 1;
                        $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                    }
                    else{
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_UPDATE'] = $this->USER->ID_USER;
                        $cek_update += 1;
                        $this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
                    }
                    // NIlai COMPONENT
                    
                    
                    $no_field=0;
                    foreach ($component as $col) {
                        $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                        $nm = trim($cmp->KD_COMPONENT," ");
                        if($nm=='C3S'){
                            $nilai = $val['cl_c3s'];
                        }else if($nm=='FCaO'){
                            $nilai =$val['cl_fcao'];
                            // $nilai =$val['cl_fcao'];
                        }else if($nm=='LSF'){
                            $nilai =$val['cl_lsf'];
                        }else if($nm=='TEMP'){
                            $nilai =$val['cl_temp'];
                        }else{
                            $nilai ='';
                        }
                        
                            $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                            $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                            $ddata['NILAI']				= $this->str_clean($nilai);
                            $ddata['NO_FIELD']			= "$no_field";
                            
                            if(!$this->t_cement_hourly->d_exists($ddata)){
                                $this->t_cement_hourly->d_insert($ddata);
                            }
                            else{
                                if($nm=='C3S'||$nm=='FCaO'||$nm=='LSF'||$nm=='TEMP'){
                                    $this->t_cement_hourly->d_update($ddata);
                                }
                            }
                            
                            $no_field +=1;
                    }
                }
            }
                if($ada == '0'){
                // Hanya insert
                    if(!$ID_CEMENT_HOURLY){
                        
                        $cek_insert += 1;
                        $tdata['DATE_ENTRY'] = date("d/m/Y");
                        $tdata['JAM_ENTRY']  = date("H");
                        $tdata['USER_ENTRY'] = $this->USER->ID_USER;
                            
                         $cek +=1;
                        // echo '1';
                        $ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
                        
                        $cek +=1;
                        $no_field=0;
                        foreach ($component as $col) {
                            $cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
                            $nm = trim($cmp->KD_COMPONENT," ");
                            $nilai ='';
                        
                            $ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
                            $ddata['ID_COMPONENT']		= $col->ID_COMPONENT;
                            $ddata['NILAI']				= $this->str_clean($nilai);
                            $ddata['NO_FIELD']			= "$no_field";
                            
                        
                            $this->t_cement_hourly->d_insert($ddata);
                            
                            $no_field +=1;
                        }
                    }
                }
        }

		$paramIncident['ID_AREA'] = $id_area;
		if($id_area == 1){
    		$paramIncident['ID_COMPONENT'] = '15,13,7';
    	}else{
    		$paramIncident['ID_COMPONENT'] = '12';
    	}

		$incident = $this->m_incident->ncqr_incident($paramIncident);

		$statusInc = 'ok';

		$param = array(
			'ID_AREA' => $id_area,
			'ID_GROUPAREA' => $paramIncident['ID_COMPONENT']
		);

		if(count($incident) > 0){
			$this->getIncident($param);
		}
        echo "berhasil";
    	// to_json(array("insert" => $cek_insert,"update" => $cek_update,"result" => 'ok'));      

	}
    

	private function str_clean($chr=''){
		$str ='([^.0-9-]+)';
		return preg_replace($str, '', $chr);
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color = getFixColor($i);
			$nilai = array_filter($trace[$i], 'is_numeric');

			if(empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= $comp[$i]->title;
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;

			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}


    public function getIncident($post = array()){
        $this->load->library('libmail');

        $param['ID_AREA'] = $post['ID_AREA'];
        $param['SEND_NOTIF'] = 'N';

        if($post['ID_GROUPAREA'] == 1){
            $param['ID_COMPONENT'] = '15,13,7';
        }else{
            $param['ID_COMPONENT'] = '12';
            unset($param['ID_PRODUCT']);
        }

        $param['ID_NCQR_STATUS'] = 1;
        
        $incident = $this->m_incident->ncqr_incident($param);
        $listID = array();
        foreach ($incident as $i => $iv) {
            $listID[] = $iv['ID_INCIDENT'];

            $detail = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']);

            // $incident[$i]['DETAIL'] = $this->m_incident->get_data_by_id($iv['ID_INCIDENT']); # echo $this->m_incident->get_sql();
            $subject = $iv['NM_COMPANY'].' ('.$iv['NM_PLANT'].' - '.$iv['NM_AREA'].')';
        
            switch ($iv['ID_INCIDENT_TYPE']) {
                case '1':
                    $listJabatan = '1';
                    break;
                case '2':
                    $listJabatan = '1,2';
                    break; 
                case '3':
                    $listJabatan = '1,2,3';
                    break;
                default:
                    $listJabatan = '1,2,3,21';
            }
            $paramMail['b.ID_AREA'] = $param['ID_AREA'];
            $paramMail['ID_JABATAN'] = $listJabatan;
            $listMail = $this->m_incident->mail_incident($paramMail);
            // echo $this->db->last_query();
            
            $to = array();
            $cc = array();
            if($this->serverHost() == 'DEV'){
                $to[] = 'm.r.sucahyo2@gmail.com';
                $cc = array('95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');
            }else{
                foreach ($listMail as $j => $jv) {
                    if($iv['TEMBUSAN'] == 1){
                        $cc[] = $jv['EMAIL'];
                    }else{
                        $to[] = $jv['EMAIL'];
                    }
                }
            }

            $output = array(
                'URL'       => base_url('incident/solve/'.$iv['ID_INCIDENT']),
                'INCIDENT'  => $iv,
                'DETAIL'    => $detail,
                'MAIL'      => array('to'=>$to,'cc'=>$cc),
                'TO'        => $to,
                'CC'        => $cc,
                'SUBJECT'   => $subject
            );

            $msg = $this->libmail->ncqr($output);

            $this->load->library('email');
            $this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');

            $this->email->to($to); 
            $this->email->cc($cc);
            
            $this->email->subject($subject);
            $this->email->message($msg);

            if($this->email->send()){
                $status = 'OK';
                foreach ($listMail as $j => $jv) {
                    $this->m_t_notifikasi->insert($jv['ID_OPCO'], $iv['ID_INCIDENT'], $jv['ID_JABATAN'], $iv['ID_INCIDENT_TYPE']);   
                }

            }else{
                $status = 'FAIL';
            }
        }
        $this->m_incident->set_SendNotif($listID, 'Y');
        return $listID;

    }
	
	
	public function manual_cronjob_hourly(){
        $this->libExternal('datepicker');
        $this->libExternal('select2'); 
		$this->template->adminlte("v_manual_cronjob_hourly");
	} 
	
	public function insert_from_daily(){    
		 $tanggal = ISSET($_POST['TANGGAL']) ? $_POST['TANGGAL'] :  date('d/m/Y'); 
		 $get_area = $this->m_cronjob->get_data_area($tanggal); 
		$arrInputData = array();
		$arrInputHeader = array();
		$arrInputDataUpdate = array();
		$arrInputHeaderUpdate = array();
		foreach($get_area as $val){
			// if($val['ID_AREA']=="88"){  untuk testing clinker rembang 
				$get_data_hourly =  $this->m_cronjob->get_data_daily($val['ID_AREA'], $tanggal);
				$cek_ada_header = 1; // Hanya Data awal perulangan
				$no_field = 0; // DI CEMENT DAILY MULAI DARI 0
				foreach($get_data_hourly as $nil){
					if($cek_ada_header==1){ 
						$tdata = array();
						// PROSES INSERT HEADER T_CEMENT_DAILY
						$tdata['ID_AREA'] = $val['ID_AREA'];
						// $tdata['ID_PRODUCT'] = $nil['ID_PRODUCT'];
						$tdata['ID_PRODUCT'] = ""; // KILN DATA PRODUCT NUll
						$tdata['ID_MESIN_STATUS'] = "";
						$tdata['MESIN_REMARK'] =  "";
						$tdata['DATE_DATA'] =  $tanggal;  
						$ID_CEMENT_DAILY = $this->t_cement_daily->get_id_daily($val['ID_AREA'],"",$tanggal); 
						if(!$ID_CEMENT_DAILY){  
							$tdata['DATE_ENTRY'] = date("d/m/Y");
							$tdata['USER_ENTRY'] = "1234567890";
							$tdata['FROM_HOURLY'] = "Y";
							$ID_CEMENT_DAILY = $this->t_cement_daily->insert($tdata);
							array_push($arrInputHeader, $tdata);
						}
						else{
							$tdata['DATE_ENTRY'] = date("d/m/Y");
							 $tdata['USER_UPDATE'] = "1234567890";
							$this->t_cement_daily->update($tdata,$ID_CEMENT_DAILY);
							array_push($arrInputHeaderUpdate, $tdata);

						}
					}
					$cek_ada_header +=1;
					
					 if($ID_CEMENT_DAILY!=""){
						// PROSES INSERT DETAIL D_CEMENT_DAILY
						$ddata = array();
						// $ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
						$ddata['ID_CEMENT_DAILY'] 	= $ID_CEMENT_DAILY;
						$ddata['ID_COMPONENT']		= $nil['ID_COMPONENT'];
						$ddata['NILAI']				= $nil['NILAI_AVG'];
						$ddata['NO_FIELD']			= $no_field;
						$no_field  += 1 ;
						if(!$this->t_cement_daily->d_exists($ddata)){ // CEK DETAIL ADA TIDAK
							array_push($arrInputData, $ddata); // PUSH ARRAY JIKA ADA, 
						}
						else{
							 $this->t_cement_daily->d_update($ddata); // JIKA DATA ADA
							array_push($arrInputDataUpdate, $ddata);  
						}
						 
					 }
				}
			// }
		}
		
		
		if(count($arrInputData)>0){ 
			$this->t_cement_daily->insert_dcement($arrInputData); // SIMPAN ARRAY HASIL PUSH DIATAS
			echo json_encode("Sukses Tambah Data");
			// echo json_encode(implode(" ",$arrInputData));
		}else{
			echo json_encode("Sukses Update Data");
			// echo json_encode(implode(" ",$arrInputDataUpdate));
		}
		
		
		// echo "-------------- START DATA HEADER INSERT --------------";
		// echo json_encode($arrInputHeader); 
		// echo "-------------- END DATA HEADER INSERT --------------";
		// echo "<br>";
		// echo "-------------- START DATA HEADER UPDATE --------------";
		// echo json_encode($arrInputHeaderUpdate); 
		// echo "-------------- END DATA HEADER UPDATE --------------";
		// echo "<br>";
		
		
		// echo "-------------- START DATA DETAIL INSERT --------------";
		// echo json_encode($arrInputData); 
		// echo "-------------- END DATA DETAIL INSERT --------------";
		// echo "<br>";
		
		// echo "-------------- START DATA DETAIL UPDATE --------------";
		// echo json_encode($arrInputDataUpdate); 
		// echo "-------------- END DATA HEADER UPDATE --------------";
		// echo "<br>";
		 
		
	}
	
	public function insert_from_production(){   
		$tanggal_cari = date('d-m-Y');
		$tanggal_cari = "01-08-2020";
		 $tanggal = "01/08/2020";
		 $get_area = $this->m_cronjob->get_data_area_production($tanggal); 
		$arrInputData = array();
		$arrInputHeader = array();
		foreach($get_area as $val){
			if($val['ID_AREA']=="105"){  
				$get_data_hourly =  $this->m_cronjob->get_data_daily_production($val['ID_AREA'], $tanggal); 
				$cek_ada_header = 1; // Hanya Data awal perulangan
				$no_field = 0; // DI CEMENT DAILY MULAI DARI 0
				foreach($get_data_hourly as $nil){
					if($cek_ada_header==1){ 
						$tdata = array();
						// PROSES INSERT HEADER T_PRODUCTION_DAILY
						$tdata['ID_AREA'] = $val['ID_AREA']; 
						$tdata['ID_MESIN_STATUS'] = "";
						$tdata['MESIN_REMARK'] =  "";
						$tdata['DATE_DATA'] =  $tanggal;  
						$ID_PRODUCTION_DAILY = $this->t_production_daily->get_id_daily($val['ID_AREA'],$tanggal); 
						if(!$ID_PRODUCTION_DAILY){ 
						echo "ISOOkk";
							$tdata['DATE_ENTRY'] = date("d/m/Y");
							$tdata['USER_ENTRY'] = "1234567890";
							$tdata['FROM_HOURLY'] = "Y";
							$ID_PRODUCTION_DAILY = $this->t_production_daily->insert($tdata);
							array_push($arrInputHeader, $tdata);
						}
						else{
							$tdata['DATE_ENTRY'] = date("d/m/Y");
							 $tdata['USER_UPDATE'] = "1234567890";
							$this->t_production_daily->update($tdata,$ID_PRODUCTION_DAILY);

						}
					}
					$cek_ada_header +=1;
					
					 if($ID_PRODUCTION_DAILY!=""){
						// PROSES INSERT DETAIL D_CEMENT_DAILY
						$ddata = array();
						// $ddata['ID_PRODUCTION_DAILY'] 	= $ID_PRODUCTION_DAILY;
						$ddata['ID_PRODUCTION_DAILY'] 	= $ID_PRODUCTION_DAILY;
						$ddata['ID_COMPONENT']		= $nil['ID_COMPONENT'];
						$ddata['NILAI']				= $nil['NILAI_AVG'];
						$ddata['NO_FIELD']			= $no_field;
						$no_field  += 1 ;
						if(!$this->t_production_daily->d_exists($ddata)){ // CEK DETAIL ADA TIDAK
							array_push($arrInputData, $ddata); // PUSH ARRAY JIKA TIDAK ADA, 
						}
						else{
							  $this->t_production_daily->d_update($ddata); // JIKA DATA ADA
						}
						 
					 }
				}
			}
		}
		
		 echo json_encode($arrInputHeader);
		echo json_encode($arrInputData);
		print_r(count($arrInputHeader));
		if(count($arrInputData)>0){
			 
			$this->t_production_daily->insert_production($arrInputData); // SIMPAN ARRAY HASIL PUSH DIATAS
		}
		 

	}

}

/* End of file Input_hourly.php */
/* Location: ./application/controllers/Input_hourly.php */
?>
