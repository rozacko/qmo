<?php

class Roles extends QMUser {
	
	public $list_data = array();
	public $data_groupmenu;
	public $comp = '';

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_roles");
		$this->load->model("m_user");
		$this->load->model("m_usergroup");
 
	}
	
	public function index(){
		$this->libExternal('dataTables,select2');
		$this->list_roles = $this->m_roles->datalist();
		$this->template->adminlte("v_roles");
	}

	public function add(){
		$this->template->adminlte("v_usergroup_add");
	} 
	
	public function create(){
		$this->m_usergroup->insert($this->input->post());
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
			redirect("roles/add");
		}
		else{
			$this->notice->success("Roles Data Saved.");
			redirect("roles");
		}
	}
	
	public function edit($ID_USERGROUP){
		$this->data_usergroup = $this->m_usergroup->get_data_by_id($ID_USERGROUP);
		$this->template->adminlte("v_usergroup_edit");	
	}
	
	public function update($ID_USERGROUP){
		$data = $this->input->post();
		$data['PERM_WRITE'] = $this->input->post('PERM_WRITE');
		$data['PERM_READ'] = $this->input->post('PERM_READ');
		$this->m_usergroup->update($data,$ID_USERGROUP);
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
			redirect("roles/edit/".$ID_USERGROUP);
		}
		else{
			$this->notice->success("Roles Data Updated.");
			redirect("roles");
		}
	}
	
	public function delete($ID_USERGROUP){
		$this->m_usergroup->delete($ID_USERGROUP);
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
		}
		else{
			$this->notice->success("Roles Data Removed.");
		}
		redirect("roles");
	}


	public function get_list(){
		$list = $this->m_roles->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USERGROUP;
			$row[] = $no;
			$row[] = $column->NM_USERGROUP;
			$row[] = $column->KD_USERGROUP;
			$row[] = $column->PERM_READ;
			$row[] = $column->PERM_WRITE;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_roles->count_all(),
            "recordsFiltered" => $this->m_roles->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
 

	public function get_detlist($id_usergroup){
		$list = $this->m_roles->get_detlist($id_usergroup);
		// echo $this->db->last_query();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USER;
			$row[] = $no;
			$row[] = $column->FULLNAME;
			$row[] = $column->NM_COMPANY ? $column->NM_COMPANY.' ('.$column->KD_COMPANY.')' : '-';
			$row[] = $column->NM_PLANT ? $column->NM_PLANT : '-';
			$row[] = $column->NM_AREA ? $column->NM_AREA : '-';
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_roles->detcount_all($id_usergroup),
            "recordsFiltered" => $this->m_roles->detcount_filtered($id_usergroup),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
