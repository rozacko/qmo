<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktivitas_pelaksanaan extends QMUser {

	public $list_data = array();
	public $data_setup;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("M_aktivitas_pelaksanaan","act_pelaksanaan");
		$this->load->model("M_uji_proficiency_setup","setup");
	}
	
	public function index(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$role_in = $sesi_user['ROLE'];
		
		$akses = "";
		foreach($role_in as $dt_role){
			if($dt_role['NM_USERGROUP'] == "ADMINISTRATOR"){
				$akses = "admin";
			}
		}
		
		if($akses == "admin" or $user_in->ID_USERGROUP == 1){
			$this->list_data = $this->act_pelaksanaan->get_list();
		} else {
			$this->list_data = $this->act_pelaksanaan->get_list_non_admin($user_in->ID_USER);
		}		
		
		// print_r($user_in);
		// print_r($role_in);
		// print_r($akses);
		// exit();
		
		$this->template->adminlte("v_aktivitas_pelaksanaan");
    }
	
	public function realisasi($id_proficiency){
		$this->data_detil = $this->act_pelaksanaan->get_data_by_id($id_proficiency);
		$this->data_activity = $this->act_pelaksanaan->get_data_activity($id_proficiency);
		// print_r($this->data_detil);
		// print_r("<br><br>");
		// print_r($this->data_activity);
		// exit();
		 
		$this->template->adminlte("v_realisasi_aktivitas");
	}
	
	public function do_edit_activity(){
		$id_proficiency = $this->input->post('ID_PRO');
		$update_act 	= $this->act_pelaksanaan->update_activity();
		
		if($update_act){
			$this->notice->success("Data Edited.");
			redirect("aktivitas_pelaksanaan/realisasi/".$id_proficiency);
		} else {
			$this->notice->error($this->act_pelaksanaan->error());
			redirect("aktivitas_pelaksanaan/realisasi/".$id_proficiency);
		} 
	}
	
	public function timeline_act($id_proficiency){
		$list = $this->setup->get_data_activity($id_proficiency);
		//print_r($list);
		//exit();
		
		$data = array();
		
		foreach($list as $dt){
			
			//plan
			$date1 = $dt->PLAN_DATE_START;
			$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
			$out1 = $dateObj1->format('Y/m/d');
			$start = (strtotime("{$out1}") * 1000);
			
			$date2 = $dt->PLAN_DATE_END;
			$dateObj2 = DateTime::createFromFormat('d-M-y', $date2);
			$out2 = $dateObj2->format('Y/m/d');
			$end = (strtotime("{$out2}") * 1000);
			
			$values[] = array ("from" => $start, "to" => $end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttBlue");
			$hasil = array ("name"	 => $dt->ACTIVITY_NAME, "desc"	 => "Plan", "values" => $values);
			
			unset($values);
			array_push($data, $hasil);
			
			
			//realisasi
			if($dt->ACT_DATE_START != null and $dt->ACT_DATE_END != null){
				$date3 = $dt->ACT_DATE_START;
				$dateObj3 = DateTime::createFromFormat('d-M-y', $date3);
				$out3 = $dateObj3->format('Y/m/d');
				$real_start = (strtotime("{$out3}") * 1000);
				
				$date4 = $dt->ACT_DATE_END;
				$dateObj4 = DateTime::createFromFormat('d-M-y', $date4);
				$out4 = $dateObj4->format('Y/m/d');
				$real_end = (strtotime("{$out4}") * 1000);
				
				$values2[] = array ("from" => $real_start, "to" => $real_end, "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil2 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values2);
				array_push($data, $hasil2);
			} else {
				$values3[] = array ("from" => '', "to" => '', "label" => "{$dt->ACTIVITY_NAME}", "customClass" => "ganttOrange");
				$hasil3 = array ("desc"	 => "Real", "values" => $values2);
				
				unset($values3);
				array_push($data, $hasil3);
			}
			
			
		}
		
		//return json_encode($data);
		
		echo (json_encode($data));
	}
	
}

?>