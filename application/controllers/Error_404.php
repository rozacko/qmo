<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_404 extends QMUser {
	
	public $data;
	public $NM_AREA;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->template->error("error_404", $data);
	}
}
