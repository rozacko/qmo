<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uji_proficiency_penerimaan extends QMUser {

    public $list_proficiency = array();

	public function __construct(){
		parent::__construct();
        $this->load->helper("string");
        $this->load->model("M_penerimaan");
        $this->load->model("M_sampling");
    }

    public function index(){
        $this->template->adminlte("v_uji_proficiency_penerimaan");
    }

    public function add(){
        $this->libExternal('datepicker');
        $this->list_proficiency = $this->M_penerimaan->get_proficiency();
        $this->template->adminlte("v_uji_proficiency_penerimaan_add");
    }

    public function add_tgl_uji(){
        $this->libExternal('datepicker');
        $proficiency = $this->input->get("proficiency");
        $id = $this->input->get("id");
        $this->list_proficiency = $this->M_penerimaan->get_proficiency($proficiency, $id);
        $this->template->adminlte("v_uji_proficiency_penerimaan_tgl_uji");
    }

    public function penerima($id){
        $data = $this->M_penerimaan->get_penerima($id);
        to_json($data);
    }

    public function get_list(){
        $list = $this->M_penerimaan->get_list();
        $data = array();
        $no   = $this->input->post('start');
        
        foreach ($list as $column) {
			$no++;
            $row = array();
            $row["NO"] = $no;
			$row["ID_PROFICIENCY"] = $column->ID_PROFICIENCY;
			$row["TITLE_PP"] = $column->TITLE_PP;
			$row["YEAR_PP"] = $column->YEAR_PP;
			$row["ID_KOMODITI"] = $column->ID_KOMODITI;
			$row["NAMA_SAMPLE"] = $column->NAMA_SAMPLE;
			$row["ID_PENERIMA"] = $column->ID_PENERIMA;
			$row["TGL_PENERIMAAN"] = $column->TGL_PENERIMAAN;
			$row["NAMA_PENERIMA"] = $column->NAMA_PENERIMA;
			$row["TGL_UJI_START"] = $column->START_TGL_UJI; 
			$row["TGL_UJI_END"] = $column->END_TGL_UJI; 
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_penerimaan->count_all(),
            "recordsFiltered" => $this->M_penerimaan->count_filtered(),
            "data" => $data,
        );

		to_json($output);
    }

    public function action_input(){
        $data = array();
        $data["ID_PENERIMA"] = $this->input->post("ID_PENERIMA");
        $data["TGL_PENERIMAAN"] = $this->input->post("TGL_PENERIMAAN");
        $data["KONDISI_SAMPLE"] = $this->input->post("KONDISI_SAMPLE");
        $data["NOTES"] = $this->input->post("CATATAN");

        if($data["KONDISI_SAMPLE"] == "rusak"){
            $penerima = $this->M_penerimaan->get_penerima("", $data["ID_PENERIMA"]);
            $title = $penerima[0]->TITLE_PP." ".$penerima[0]->YEAR_PP;
            $to = $penerima[0]->EMAIL;
            $name = $penerima[0]->FULLNAME;
            $penerima = $penerima[0]->NAMA_PENERIMA;
            $notes = $penerima[0]->NOTES;
            $this->send_email($title, $to, $name, $penerima, $notes);
        }

        $config['upload_path']= './assets/uploads/proficiency/';
        $config['allowed_types'] = 'jpg|jpeg|png|xls|xlsx|csv';
        $config['max_size'] = 2048;

        if(isset($_FILES['FILE_PENERIMAAN_SAMPLE'])){
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('FILE_PENERIMAAN_SAMPLE')){
                $FILE_PENERIMAAN_SAMPLE = $this->upload->data();
                $data["FILE_PENERIMAAN_SAMPLE"] = $FILE_PENERIMAAN_SAMPLE["file_name"];
            }
        }

        $this->M_penerimaan->update($data, $data["ID_PENERIMA"]);
        if($this->M_penerimaan->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
    }

    public function action_add_tgl_uji($id){
        $data = array();
        $data["START_TGL_UJI"] = $this->input->post("START_TGL_UJI");
        // $data["END_TGL_UJI"] = $this->input->post("END_TGL_UJI");

        $this->M_penerimaan->update($data, $id);
        if($this->M_penerimaan->error()){
            $status = false;
		}
		else{
            $status = true;
		}
		echo $status;
    }

    public function send_email($title, $to, $name, $penerima, $notes){
        $from = "qmo@sig.id";
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        if($uri['1'] == 'DEV'){
            $to = "muh.rudi.hariyanto@gmail.com";
        }
        $subject = "Konfirmasi Penerimaan Sample ".$title;
        $message = "Kepada Yth.
        ".$name."
        
        Dengan hormat,
        Sample atas ".$title." sudah kami terima tetapi terdapat kerusakan pada sample yang diterima yakni ".$notes.",
        Oleh karena itu kami mohon untuk dikirimkan sample ulang sehingga proses pengujian bisa segera dilakukan.
        Demikian terima kasih
        
        Regards,
        ".$penerima;
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);
	}
}