<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_ketepatan extends Home {
	
	var $list_company = array();
	var $list_grouparea = array();
	var $list_area = array();
	var $list_data = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_access_log");
		$this->load->model("m_company");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("treport");
		$this->load->model("m_ketepatan");
		
	}

	public function index(){
		$this->libExternal('select2,datepicker');
		$this->list_company 	= $this->m_company->list_company();
		$this->list_grouparea 	= $this->m_grouparea->datalist();
		$this->list_area 		= $this->m_area->datalist();
		$this->template->adminlte("v_dashboard_ketepatan");
		#$this->access_log();
	}
	
	
	
	public function dashboard_waktu(){   
		
		$header =  array();
		$getGroupPlant = $this->m_ketepatan->getGroupPlant(); 
		$arrayGroupArea = array('1', '81', '2', '6'); 
		
			$array_nilai_produk = array() ; 
			$array_nilai_bahan= array() ; 
			$tanggal_awal =  date('01/m/Y');
			$tanggal_akhir =  date('d/m/Y');
		foreach($getGroupPlant as $gp){  
		
			array_push($header, $gp['NM_GROUP_PLANT']); 
			$getPlant = $this->m_ketepatan->getPlant($gp['ID_GROUP_PLANT']);  
			$nomer_produk = 0;
			$s_selisih_produk = 0;
			$nomer_bahan = 0;
			$s_selisih_bahan = 0;
			$hitung_nilai_produk = 0;
			$hitung_nilai_bahan= 0;
			foreach($arrayGroupArea as $ga){
				if($ga=='1' ||$ga=='81' ){
					$tipe_data = 'PRODUK' ;
				}else{
					$tipe_data = 'BAHAN' ;
				}
				$ID_GROUPAREA = $ga;
				foreach($getPlant as $pl){
					$r = @$this->treport->dashboard_daily($tanggal_awal,$tanggal_akhir,$ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT']);	 
					$nm_company	= $this->m_company->get_data_by_id($pl['ID_COMPANY'])->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT'])->JML_AREA_PRODUKSI;

					$status = "D";
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					}

					$tgl1 = new DateTime($r->TANGGAL_DATA);
					$tgl2 = new DateTime($r->TANGGAL_ENTRI);
					$selisih = $tgl2->diff($tgl1)->days; 
					
					if($status=='D' && $tipe_data == 'PRODUK'){ 
						$nomer_produk += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_produk += $selisih_fix;
					}	
					
					if($status=='D' && $tipe_data == 'BAHAN'){
						$nomer_bahan += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_bahan +=  $selisih_fix;
					}	
					
					
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($pl['ID_PLANT'])->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
				}
			}
			
			// if($s_selisih_produk>0 && $nomer_produk>0){ 
				// $hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
				// $hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan;  
				// array_push($array_nilai_produk, $hitung_nilai_produk);
				// array_push($array_nilai_bahan, $hitung_nilai_bahan);
				
			// }else{
				// array_push($array_nilai_produk, 1);
				// array_push($array_nilai_bahan, 1);
			// }
			
			
				if($s_selisih_produk>0 &&  $nomer_produk>0 ){
					// echo "produk   ". $s_selisih_produk . " ---- " . $nomer_produk."<br>"; 
					// array_push($array_nilai_produk,  number_format($hitung_nilai_produk, 2, '.', ''));
					$hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
					array_push($array_nilai_produk, $hitung_nilai_produk);
					
				}else{
					array_push($array_nilai_produk, 1);
				}

				if( $s_selisih_bahan>0 &&  $nomer_bahan>0 ){
					
					$hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan; 
					array_push($array_nilai_bahan, $hitung_nilai_bahan);
					
					// echo "bahan    ". $s_selisih_bahan . " ---- " . $nomer_bahan."<br>";
					// array_push($array_nilai_bahan,  number_format($hitung_nilai_bahan, 2, '.', ''));  
				}else{
					array_push($array_nilai_bahan, 1);
				}
			
		}
		
			
			$data = array(
							array(
								"name" => 'Pruduk',
								"data" => $array_nilai_produk,
							),
							array(
								"name" => 'Bahan',
								"data" => $array_nilai_bahan
							) 
						); 
						$bulan_tahun = 'M-Y';
						// $bulan_tahun = 'Agustus 2020';
		echo json_encode(array(
			'header' => $header ,
			'data' => $data,
			'bulan_tahun' => $bulan_tahun,
			'cek_data' => $d
		));
	}
	 
	public function dashboard_waktu_detail(){   
		$nm_group_plant = $_POST['nm_group_plant'];
		$header =  array();
		$getGroupPlant = $this->m_ketepatan->getGroupPlantNama($nm_group_plant); 
		$arrayGroupArea = array('1', '81', '2', '6'); 
		
			$array_nilai_produk = array() ; 
			$array_nilai_bahan= array() ; 
		foreach($getGroupPlant as $gp){   
			$getPlant = $this->m_ketepatan->getPlant($gp['ID_GROUP_PLANT']);   
			foreach($getPlant as $pl){
				
			array_push($header, $pl['NM_PLANT']); 
			
			$nomer_produk = 0;
			$s_selisih_produk = 0;
			$nomer_bahan = 0;
			$s_selisih_bahan = 0;
			$hitung_nilai_produk = 0;
			$hitung_nilai_bahan= 0;
			
				foreach($arrayGroupArea as $ga){
					
					if($ga=='1' ||$ga=='81' ){
						$tipe_data = 'PRODUK' ;
					}else{
						$tipe_data = 'BAHAN' ;
					}
					$ID_GROUPAREA = $ga;
					
					$r = @$this->treport->dashboard_daily($tanggal_awal,$tanggal_akhir,$ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT']);	 
					$nm_company	= $this->m_company->get_data_by_id($pl['ID_COMPANY'])->NM_COMPANY;
					$chek = $this->treport->dashboard_hourly_checkProduction($ID_GROUPAREA,$pl['ID_COMPANY'],$pl['ID_PLANT'])->JML_AREA_PRODUKSI;

					$status = "D";
					if((int)$chek == 0) {
						$status = "NP";
					}else{
						if($r->JML_DATA == 0){
							$status = "NC";
						}else{
							if($r->JML_DATA < $r->JML_ROW) {
								$status = "NC";
							}
						}
					}

					$tgl1 = new DateTime($r->TANGGAL_DATA);
					$tgl2 = new DateTime($r->TANGGAL_ENTRI);
					$selisih = $tgl2->diff($tgl1)->days; 
					
					if($status=='D' && $tipe_data == 'PRODUK'){ 
						$nomer_produk += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_produk += $selisih_fix;
					}	
					
					if($status=='D' && $tipe_data == 'BAHAN'){
						$nomer_bahan += 1;
						$a_selisih = floatval($selisih);
						if($selisih<= 2){
							$selisih_fix = 5;
						}else if($selisih>= 3 && $selisih <=4 ){
							$selisih_fix = 4;
						}else if($selisih>= 5 && $selisih <=6){
							$selisih_fix = 3;
						}else if($selisih >=7){
							$selisih_fix = 2;
						}	
						$s_selisih_bahan +=  $selisih_fix;
					}	
					
					
					
					// $NM_PLANT 									= $this->m_plant->get_data_by_id($pl['ID_PLANT'])->NM_PLANT;
					// $d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					// $d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					// $d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
				}
				
				
				
			
				if($s_selisih_produk>0 &&  $nomer_produk>0 ){
					// echo "produk   ". $s_selisih_produk . " ---- " . $nomer_produk."<br>"; 
					// array_push($array_nilai_produk,  number_format($hitung_nilai_produk, 2, '.', ''));
					$hitung_nilai_produk = $s_selisih_produk / $nomer_produk;
					array_push($array_nilai_produk, $hitung_nilai_produk);
					
				}else{
					array_push($array_nilai_produk, 0);
				}

				if( $s_selisih_bahan>0 &&  $nomer_bahan>0 ){
					
					$hitung_nilai_bahan = $s_selisih_bahan / $nomer_bahan; 
					array_push($array_nilai_bahan, $hitung_nilai_bahan);
					
					// echo "bahan    ". $s_selisih_bahan . " ---- " . $nomer_bahan."<br>";
					// array_push($array_nilai_bahan,  number_format($hitung_nilai_bahan, 2, '.', ''));  
				}else{
					array_push($array_nilai_bahan, 0);
				}
				
			}
			
		}
		
			
			$data = array(
							array(
								"name" => 'Pruduk',
								"data" => $array_nilai_produk,
							),
							array(
								"name" => 'Bahan',
								"data" => $array_nilai_bahan
							) 
						); 
		echo json_encode(array(
			'header' => $header ,
			'data' => $data,
			// 'cek_data' => $d
		));
	}
	 
	
	
	
	
}
