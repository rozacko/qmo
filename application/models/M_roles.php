<?php

class M_roles Extends DB_QM {
	private $post  = array();
	private $table = "M_USERGROUP";

	private $pKey  = "ID_USERGROUP";
	private $column_order = array('KD_USERGROUP'); //set column for datatable order
    private $column_search = array('NM_USERGROUP'); //set column for datatable search 
    private $order = array("KD_USERGROUP" => 'ASC'); //default order
	
	private $detpKey  = "a.ID_USER";
	private $detcolumn_order = array('a.ID_USER'); //set column for datatable order
    private $detcolumn_search = array('a.FULLNAME'); //set column for datatable search 
    private $detorder = array("a.ID_USER" => 'DESC'); //default order


	public function __construct(){
		$this->post = $this->input->post();
	}
 
	public function insertBatch($data){
		$q = true;
		$listData = array();
		$id =  '0';
		foreach ($data as $key => $value) {
			$id = $value['ID_USER'];
			$list = array();
				$list['ID_USER'] = $value['ID_USER'];
				$list['ID_USERGROUP'] = $value['ID_USERGROUP'];
				$listData[] = $list;
		} 
		if($listData){
			$del = $this->db->delete('M_ROLES', array('ID_USER' => $id));
			$q = $this->db->insert_batch('M_ROLES', $listData);
		} 
		// echo $this->db->last_query();
		return $q;
	}

	public function get_data_role_by_iduser($ID_USER){
		// $this->db_qm = $this->load->database('qm_dev', TRUE);
		// // $this->db_qm = $this->load->database('ujian_online', TRUE);

		// $this->db->select("aM_ROLES.*, M_USGROUP.NM_USERGROUP");
		// $this->db->join("M_USERGROUP M_USGROUP","aM_ROLES.ID_USERGROUP=M_USGROUP.ID_USERGROUP","LEFT");
		// $this->db->where("aM_ROLES.ID_USER",$ID_USER);
		// return $this->db->get("M_ROLES aM_ROLES")->result();
		// $this->db->select("M_ROLES.*, M_USERGROUP.NM_USERGROUP");
		// $this->db->from('M_ROLES');
		// $this->db->join("M_USERGROUP","M_ROLES.ID_USERGROUP=M_USERGROUP.ID_USERGROUP","LEFT");
		// $this->db->where("M_ROLES.ID_USER",$ID_USER);
		// $a = $this->db->get()->result();
 
		// return $a; 
		return $this->db->query("SELECT * FROM M_ROLES  A
			LEFT JOIN M_USERGROUP B ON A.ID_USERGROUP = B.ID_USERGROUP
			WHERE A.ID_USER = '{$ID_USER}'")->result();
	}


//-------------------------------


	public function datalist(){
		$this->db->order_by("a.KD_USERGROUP");
		return $this->db->get("M_USERGROUP a")->result();
	}
	
	public function search(&$keyword){
		$this->db->like("NM_USERGROUP",$keyword);
		return $this->db->get("M_USERGROUP")->result();
	}
	
	public function data($where){
		$this->db->where($where);
		return  $this->db->get("M_USERGROUP")->row();
	}

	public function get_query(){
		$this->db->select("*");
		$this->db->from($this->table .' a');
		#echo $this->db->get_compiled_select();exit();
	}

	public function get_list() {
		$this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
			#echo $this->db->get_compiled_select();exit();
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}		

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

	public function get_detquery($id_usergroup){
		$this->db->select("a.*, c.NM_COMPANY, c.KD_COMPANY, d.NM_PLANT, e.NM_AREA");
		$this->db->from('M_USERS a');
		$this->db->where("b.ID_USERGROUP", $id_usergroup);
		$this->db->where("a.ISACTIVE", 'Y');
		$this->db->join('M_ROLES b', 'a.ID_USER = b.ID_USER', 'LEFT');
		$this->db->join('M_COMPANY c', 'a.ID_COMPANY = c.ID_COMPANY', 'LEFT');
		$this->db->join('M_PLANT d', 'a.ID_PLANT = d.ID_PLANT', 'LEFT');
		$this->db->join('M_AREA e', 'a.ID_AREA = e.ID_AREA', 'LEFT');

		#echo $this->db->get_compiled_select();exit();
	}


	//-----------------_DETAIL-------------------
	public function get_detlist($id_usergroup) {
		$this->get_detquery($id_usergroup);
		$i = 0;

		//Loop column search
		foreach ($this->detcolumn_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->detcolumn_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->detcolumn_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
			#echo $this->db->get_compiled_select();exit();
		}elseif (isset($this->detorder)) {
			$this->db->order_by(key($this->detorder), $this->detorder[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}		

	/** Count query result after filtered **/
	public function detcount_filtered($id_usergroup){
		$this->get_detquery($id_usergroup);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function detcount_all($id_usergroup){
		$this->get_detquery($id_usergroup);
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

}
