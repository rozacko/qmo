<?php

class M_tis_excel_config Extends DB_QM {

    function _query(){
        $this->db->select("*");
        $this->db->from("TIS_EXCEL_FILE");
        $this->db->join("M_PLANT", "TIS_EXCEL_FILE.ID_PLANT = M_PLANT.ID_PLANT");
        $this->db->join("M_GROUPAREA", "TIS_EXCEL_FILE.ID_GROUPAREA = M_GROUPAREA.ID_GROUPAREA");
        $this->db->order_by("TIS_EXCEL_FILE.ID_TIS_EXCEL_FILE", "DESC");
    }

    public function datalist(){
        $this->_query();
        $data = $this->db->get();
        return $data->result_array();
    }

    public function detail($id){
        $this->_query();
        $this->db->where('TIS_EXCEL_FILE.ID_TIS_EXCEL_FILE', $id);
        $data = $this->db->get();
        return $data->row();
    }

    public function insert($data){
        $this->db->set($data);
        $this->db->set("ID_TIS_EXCEL_FILE","TIS_EXCEL_FILE_SEQ.NEXTVAL",FALSE);
        $this->db->insert("TIS_EXCEL_FILE");
        return true;
    }

    public function update($data, $id){
        $this->db->set($data);
        $this->db->where('ID_TIS_EXCEL_FILE', $id)->update("TIS_EXCEL_FILE");
        return true;
    }
    
    public function delete($id){
        $this->db->where('ID_TIS_EXCEL_FILE', $id)->delete("TIS_EXCEL_FILE");
        return true;
    }
}