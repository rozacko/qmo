<?php

class M_clinker_factor Extends DB_QM {
    
	public function company(){
		return $this->db->query("SELECT * FROM M_COMPANY ORDER BY KD_COMPANY")->result_array();
	}
	
	public function area($company){
		return $this->db->query("SELECT ID_AREA from M_COMPANY a
        LEFT JOIN M_PLANT b ON a.ID_COMPANY=b.ID_COMPANY
        LEFT JOIN M_AREA c ON b.ID_PLANT=c.ID_PLANT
        WHERE ID_GROUPAREA='1' and KD_COMPANY='{$company}'
        ORDER BY KD_COMPANY, b.ID_PLANT, c.ID_AREA")->result_array();
	}
	
	public function data($where){
		$this->db->where($where);
		return  $this->db->get("M_AREA")->row();
	}
	
	public function get_data_by_id($ID_AREA){
		$this->db->where("ID_AREA",$ID_AREA);
		return $this->db->get("M_AREA")->row();
	}
	
	public function data_except_id($where,$skip_id){
		$this->db->where("ID_AREA !=",$skip_id);
		$this->db->where($where);
		return $this->db->get("M_AREA")->row();
	}
    
    ///     INDEKS CLINKER     |||\\\\
    
	public function plant($where){
		$this->db->where($where);
		return  $this->db->get("M_PLANT")->result_array();
	}
    
	
	

}
