<?php

class M_pairing_trend Extends DB_QM {

    public function __construct(){
        $this->db = $this->load->database('mso_prod', TRUE);
    }

    public function get_company($companys, $use_plant = null){
        $this->db->select("*");
        $this->db->from("M_COMPANY");
        $this->db->where_in("ID_COMPANY", $companys);
        if(isset($use_plant)){
            $this->db->where("USE_DETAIL_PLANT", "Y");
        }
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_area($companys){
        $this->db->select("*");
        $this->db->from("M_AREA");
        $this->db->join("M_GROUPAREA", "M_AREA.ID_GROUPAREA = M_GROUPAREA.ID_GROUPAREA");
        $this->db->join("M_PLANT", "M_AREA.ID_PLANT = M_PLANT.ID_PLANT");
        $this->db->where("M_PLANT.ID_COMPANY", $companys);
        $this->db->where("(M_GROUPAREA.NM_GROUPAREA like '%OUTSILO%' OR M_GROUPAREA.NM_GROUPAREA  like '%OUT SILO%')");
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_component($components){
        $this->db->select("*");
        $this->db->from("M_COMPONENT");
        $this->db->where_in("ID_COMPONENT", $components);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_product($products){
        $this->db->select("*");
        $this->db->from("M_PRODUCT");
        $this->db->where_in("ID_PRODUCT", $products);
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return array();
        }
    }

    public function get_data_pairing($company, $product, $component, $date_start, $date_end, $by_plant = null){
        // ----------------------------------------------- start 16.03.2021 izza -----------------------------------------------
        // $arr_company = array();
        // foreach($company as $comp){
        //     array_push($arr_company, $comp);
        // }

        // $arr_product = array();
        // foreach($product as $p){
        //     array_push($arr_product, $p);
        // }

        // $arr_component = array();
        // foreach($component as $c){
        //     array_push($arr_component, $c);
        // }
        $arr_product = implode(',', $product);
        $arr_company = implode(',', $company);
        $arr_component = implode(',', $component);
        // ----------------------------------------------- end 16.03.2021 izza -----------------------------------------------
        if(! isset($by_plant)){
            $select = "PL.ID_COMPANY, CO.NM_COMPANY,";
            $group = "PL.ID_COMPANY, CO.NM_COMPANY,";
        } else {
            $select = "PL.ID_PLANT, PL.NM_PLANT,";
            $group = "PL.ID_PLANT, PL.NM_PLANT,";
        }
        
        $sql = 
        " SELECT $select
                T.ID_PRODUCT,
                P.NM_PRODUCT,
                D.ID_COMPONENT,
                C.NM_COMPONENT,
                AVG( D.NILAI ) AS AVG_NILAI,
                STDDEV( D.NILAI ) AS STDDEV_NILAI, 
                TO_CHAR(T.DATE_DATA, 'MM-YYYY' ) AS PERIODE,
                TO_CHAR(T.DATE_DATA, 'YYYYMM' ) AS PERIODE2 -- izza 16.03.2021
            FROM T_CEMENT_DAILY T
                LEFT JOIN D_CEMENT_DAILY D ON T.ID_CEMENT_DAILY = D.ID_CEMENT_DAILY 
                LEFT JOIN M_AREA A ON T.ID_AREA = A.ID_AREA AND (A.NM_AREA LIKE '%OUTSILO%' OR A.NM_AREA LIKE '%OUT SILO%')
                LEFT JOIN M_PLANT PL ON A.ID_PLANT = PL.ID_PLANT
                LEFT JOIN M_COMPANY CO ON PL.ID_COMPANY = CO.ID_COMPANY
                LEFT JOIN M_GROUPAREA GA ON A.ID_GROUPAREA = GA.ID_GROUPAREA
                LEFT JOIN M_PRODUCT P ON T.ID_PRODUCT = P.ID_PRODUCT
                LEFT JOIN M_COMPONENT C ON D.ID_COMPONENT = C.ID_COMPONENT 
            WHERE T.ID_PRODUCT IN ($arr_product) 
                AND T.DATE_DATA BETWEEN TO_DATE('".$date_start."', 'DD/MM/YYYY') AND TO_DATE('".$date_end."', 'DD/MM/YYYY')
                AND D.ID_COMPONENT IN ($arr_component) 
                AND PL.ID_COMPANY IN ($arr_company)
                AND D.NILAI IS NOT NULL 
            GROUP BY
                TO_CHAR( T.DATE_DATA, 'YYYYMM' ), -- izza 16.03.2021
                $group
                T.ID_PRODUCT,
                P.NM_PRODUCT,
                D.ID_COMPONENT,
                C.NM_COMPONENT,
                TO_CHAR( T.DATE_DATA, 'MM-YYYY' )
            ORDER BY
                TO_CHAR( T.DATE_DATA, 'YYYYMM' ) -- izza 16.03.2021
        ";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    public function get_boxplot_pairing($company, $product, $component, $date_start, $date_end){
        // ----------------------------------------------- start 16.03.2021 izza -----------------------------------------------
        // $arr_company = array();
        // foreach($company as $comp){
        //     array_push($arr_company, $comp);
        // }

        // $arr_product = array();
        // foreach($product as $p){
        //     array_push($arr_product, $p);
        // }

        // $arr_component = array();
        // foreach($component as $c){
        //     array_push($arr_component, $c);
        // }
        $arr_product = implode(',', $product);
        $arr_company = implode(',', $company);
        $arr_component = implode(',', $component);
        // ----------------------------------------------- end 16.03.2021 izza -----------------------------------------------

        $sql = "SELECT
                PL.ID_COMPANY,
                CO.NM_COMPANY,
                T.ID_PRODUCT,
                P.NM_PRODUCT,
                D.ID_COMPONENT,
                C.NM_COMPONENT,
                to_char( t.DATE_DATA, 'MM-YYYY' ) AS PERIODE,
                TO_CHAR(T.DATE_DATA, 'YYYYMM' ) AS PERIODE2, -- izza 16.03.2021
                LISTAGG ( D.NILAI, ',' ) WITHIN GROUP ( ORDER BY PL.ID_COMPANY ) AS NILAI
            FROM
                T_CEMENT_DAILY T
                JOIN D_CEMENT_DAILY D ON T.ID_CEMENT_DAILY = D.ID_CEMENT_DAILY
                JOIN M_AREA A ON T.ID_AREA = A.ID_AREA AND (A.NM_AREA LIKE '%OUTSILO%' OR A.NM_AREA LIKE '%OUT SILO%')
                JOIN M_PLANT PL ON A.ID_PLANT = PL.ID_PLANT
                JOIN M_COMPANY CO ON PL.ID_COMPANY = CO.ID_COMPANY
                JOIN M_GROUPAREA GA ON A.ID_GROUPAREA = GA.ID_GROUPAREA
                JOIN M_PRODUCT P ON T.ID_PRODUCT = P.ID_PRODUCT
                JOIN M_COMPONENT C ON D.ID_COMPONENT = C.ID_COMPONENT 
            WHERE
                T.ID_PRODUCT IN ($arr_product)
                AND D.ID_COMPONENT IN ($arr_component)
                AND PL.ID_COMPANY IN ($arr_company)
                AND T.DATE_DATA BETWEEN TO_DATE('".$date_start."', 'DD/MM/YYYY') AND TO_DATE('".$date_end."', 'DD/MM/YYYY')
                AND D.NILAI IS NOT NULL AND D.NILAI != 0
            GROUP BY
                TO_CHAR( T.DATE_DATA, 'YYYYMM' ), -- izza 16.03.2021
                PL.ID_COMPANY,
                CO.NM_COMPANY,
                T.ID_PRODUCT,
                P.NM_PRODUCT,
                D.ID_COMPONENT,
                C.NM_COMPONENT,
                to_char( t.DATE_DATA, 'MM-YYYY' ) 
            ORDER BY
                to_char( t.DATE_DATA, 'YYYYMM' ) -- izza 16.03.2021
        "; 

        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }

    public function get_company_product($product){
        $sql = "SELECT DISTINCT
        MC.ID_COMPANY,
        MC.NM_COMPANY 
    FROM
        C_PRODUCT CP
        JOIN M_AREA MA ON CP.ID_AREA = MA.ID_AREA
        JOIN M_PLANT MP ON MA.ID_PLANT = MP.ID_PLANT
        JOIN M_GROUPPLANT MGP ON MP.ID_GROUP_PLANT = MGP.ID_GROUP_PLANT
        JOIN M_COMPANY MC ON MP.ID_COMPANY = MC.ID_COMPANY 
    WHERE
        CP.ID_PRODUCT = '".$product."'";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function data_product_company($ID_COMPANY=NULL){		
        $this->db->select("a.ID_PRODUCT, c.KD_PRODUCT, c.NM_PRODUCT");
		$this->db->from("C_PRODUCT a");
		$this->db->join("M_AREA b","b.ID_AREA = a.ID_AREA","LEFT");
		$this->db->join("M_PRODUCT c","c.ID_PRODUCT=a.ID_PRODUCT","LEFT");
        $this->db->join("M_PLANT d","b.ID_PLANT=d.ID_PLANT","LEFT");
		if($ID_COMPANY) $this->db->where_in("d.ID_COMPANY", $ID_COMPANY);
        $this->db->group_by('a.ID_PRODUCT'); 
        $this->db->group_by('c.KD_PRODUCT'); 
        $this->db->group_by('c.NM_PRODUCT'); 
		$data= $this->db->get();
		return $data->result();
    }

    // start izza jan 2021
    public function data_product_company_read($ID_COMPANY=NULL){		
        $this->db->select("a.ID_PRODUCT, c.KD_PRODUCT, c.NM_PRODUCT");
		$this->db->from("C_PRODUCT a");
		$this->db->join("M_AREA b","b.ID_AREA = a.ID_AREA","LEFT");
		$this->db->join("M_PRODUCT c","c.ID_PRODUCT=a.ID_PRODUCT","LEFT");
        $this->db->join("M_PLANT d","b.ID_PLANT=d.ID_PLANT","LEFT");
		$this->db->where("a.READ", '1'); // izza jan 2021
		if($ID_COMPANY) $this->db->where_in("d.ID_COMPANY", $ID_COMPANY);
        $this->db->group_by('a.ID_PRODUCT'); 
        $this->db->group_by('c.KD_PRODUCT'); 
        $this->db->group_by('c.NM_PRODUCT'); 
		$data= $this->db->get();
		return $data->result();
    }
    // end izza jan 2021
    
    public function data_component_company($ID_COMPANY=NULL){		
        $this->db->distinct();
    	$this->db->select("MC.ID_COMPONENT, MC.KD_COMPONENT, MC.NM_COMPONENT");
		$this->db->from("C_PARAMETER CP");
		$this->db->join("M_COMPONENT MC","CP.ID_COMPONENT = MC.ID_COMPONENT");
        $this->db->join("M_PLANT MP","CP.ID_PLANT = MP.ID_PLANT AND (MP.NM_PLANT LIKE '%OUTSILO%' OR MP.NM_PLANT LIKE '%OUT SILO%')");
        $this->db->order_by("MC.ID_COMPONENT");
		if($ID_COMPANY) $this->db->where_in("MP.ID_COMPANY", $ID_COMPANY);
		$data= $this->db->get();
		return $data->result();
	}

    public function get_min_max_value_pairing($company, $product, $component, $date_start, $date_end){
        $arr_company = array();
        foreach($company as $comp){
            array_push($arr_company, $comp);
        }

        $arr_product = array();
        foreach($product as $p){
            array_push($arr_product, $p);
        }

        $arr_component = array();
        foreach($component as $c){
            array_push($arr_component, $c);
        }

        $sql = "SELECT
        ID_COMPANY,
        NM_COMPANY,
        ID_PRODUCT,
        NM_PRODUCT,
        ID_COMPONENT,
        NM_COMPONENT,
        MIN( AVG_NILAI ) AS MIN_AVG,
        MAX( AVG_NILAI ) AS MAX_AVG 
    FROM
        (
    SELECT
        PL.ID_COMPANY,
        CO.NM_COMPANY,
        T.ID_PRODUCT,
        P.NM_PRODUCT,
        D.ID_COMPONENT,
        C.NM_COMPONENT,
        to_char( t.DATE_DATA, 'MM-YYYY' ) AS PERIODE,
        AVG( D.NILAI ) AS AVG_NILAI 
    FROM
        T_CEMENT_DAILY T
        JOIN D_CEMENT_DAILY D ON T.ID_CEMENT_DAILY = D.ID_CEMENT_DAILY
        JOIN M_AREA A ON T.ID_AREA = A.ID_AREA
        JOIN M_PLANT PL ON A.ID_PLANT = PL.ID_PLANT
        JOIN M_COMPANY CO ON PL.ID_COMPANY = CO.ID_COMPANY
        JOIN M_GROUPAREA GA ON A.ID_GROUPAREA = GA.ID_GROUPAREA
        JOIN M_PRODUCT P ON T.ID_PRODUCT = P.ID_PRODUCT
        JOIN M_COMPONENT C ON D.ID_COMPONENT = C.ID_COMPONENT 
    WHERE
        T.ID_PRODUCT IN (".implode(',', $arr_product).")
        AND D.ID_COMPONENT IN (".implode(',', $arr_component).")
        AND PL.ID_COMPANY IN (".implode(',', $arr_company).")
        AND T.DATE_DATA BETWEEN TO_DATE('".$date_start."', 'DD/MM/YYYY') AND TO_DATE('".$date_end."', 'DD/MM/YYYY')
        AND D.NILAI IS NOT NULL  
    GROUP BY
        PL.ID_COMPANY,
        CO.NM_COMPANY,
        T.ID_PRODUCT,
        P.NM_PRODUCT,
        D.ID_COMPONENT,
        C.NM_COMPONENT,
        to_char( t.DATE_DATA, 'MM-YYYY' ) 
    ORDER BY
        to_char( t.DATE_DATA, 'MM-YYYY' )) 
    GROUP BY
        ID_COMPANY,
        NM_COMPANY,
        ID_PRODUCT,
        NM_PRODUCT,
        ID_COMPONENT,
        NM_COMPONENT";

        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->result_array();
        } else {
            return false;
        }
    }
}