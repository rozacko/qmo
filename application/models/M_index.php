<?php

class M_index Extends DB_QM {
	
    public function __construct(){
      parent::__construct();
      $this->load->database();
      $this->bcs = $this->load->database('bcs', TRUE);
    }
    
	public function get_plant($company, $produk, $date){
		$data= $this->bcs->query("SELECT 
            PLANT, DESCRIPTION
        FROM(
            SELECT 
                PLANT, DESCRIPTION
            FROM(
            SELECT
                P.PLANT, X.DESCRIPTION
            FROM
                PRODUCTION P
                LEFT JOIN M_MATERIAL M ON P.MATERIAL_PRODUCT = M.MATERIAL
                JOIN M_WORKCENTER W ON P.WORKCENTER = W.WORKCENTER
                JOIN M_PLANT X ON X.PLANT = P.PLANT
            WHERE
                FISCAL_YEAR_PERIOD = '{$date}'
            AND DOC_DATE IS NOT NULL
            AND COMPANY = '{$company}'
            AND P.MATERIAL_PRODUCT IS NOT NULL
            AND MOVEMENT_TYPE IN ('261', '262')
            AND M.MATERIAL IN ({$produk})
            )UNION ALL
            SELECT 
                PLANT, DESCRIPTION
            FROM(
            SELECT
                P.PLANT, X.DESCRIPTION
            FROM
                PRODUCTION P
                LEFT JOIN M_MATERIAL M ON P.MATERIAL_PRODUCT = M.MATERIAL
                JOIN M_WORKCENTER W ON P.WORKCENTER = W.WORKCENTER
                JOIN M_PLANT X ON X.PLANT = P.PLANT
            WHERE
                FISCAL_YEAR_PERIOD = '{$date}'
            AND DOC_DATE IS NOT NULL
            AND COMPANY = '{$company}'
            AND P.MATERIAL_PRODUCT IS NOT NULL
            AND MOVEMENT_TYPE IN ('101', '102')
            AND M.MATERIAL IN ({$produk})
            )
        )
        GROUP BY 
            PLANT, DESCRIPTION
        ORDER BY PLANT");
        // echo $this->bcs->last_query();
        return $data->result_array();
	}
    
    public function get_value($company, $produk, $date, $plant){
		$data= $this->bcs->query("SELECT 
                COMPANY,
                WORKCENTER,
                DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                DESC_MATERIAL_PRODUCT,
                SUM(AMOUNT) AS V1,
                (CASE WHEN SUM(QQ1) = 0 THEN 0 ELSE ROUND(SUM(Q1)/SUM(QQ1),2) END) AS INDEX1,
                PLANT,
                PLANT_TO
            FROM(
            SELECT 
                COMPANY,
                WORKCENTER,
                DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                DESC_MATERIAL_PRODUCT,
                AMOUNT,
                Q1,
                0 AS QQ1,
                PLANT,
                PLANT_TO
            FROM(
            SELECT
                COMPANY,
                P.WORKCENTER,
                W.DESCRIPTION AS DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                M.DESCRIPTION AS DESC_MATERIAL_PRODUCT,
                AMOUNT,
                QTY AS Q1,
                PLANT,
                PLANT_TO
            FROM
                PRODUCTION P
                LEFT JOIN M_MATERIAL M ON P.MATERIAL_PRODUCT = M.MATERIAL
                JOIN M_WORKCENTER W ON P.WORKCENTER = W.WORKCENTER
            WHERE
                FISCAL_YEAR_PERIOD = '{$date}'
            AND DOC_DATE IS NOT NULL
            AND COMPANY = '{$company}'
            AND P.MATERIAL_PRODUCT IS NOT NULL
            AND MOVEMENT_TYPE IN ('261', '262')
            AND M.MATERIAL IN ({$produk})
            AND PLANT IN ({$plant})
            )UNION ALL
            SELECT 
                COMPANY,
                WORKCENTER,
                DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                DESC_MATERIAL_PRODUCT,
                0 AS AMOUNT,
                0 AS Q1,
                QQ1,
                PLANT,
                PLANT_TO
            FROM(
            SELECT
                COMPANY,
                P.WORKCENTER,
                W.DESCRIPTION AS DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                M.DESCRIPTION AS DESC_MATERIAL_PRODUCT,
                QTY AS QQ1,
                PLANT,
                PLANT_TO
            FROM
                PRODUCTION P
                LEFT JOIN M_MATERIAL M ON P.MATERIAL_PRODUCT = M.MATERIAL
                JOIN M_WORKCENTER W ON P.WORKCENTER = W.WORKCENTER
            WHERE
                FISCAL_YEAR_PERIOD = '{$date}'
            AND DOC_DATE IS NOT NULL
            AND COMPANY = '{$company}'
            AND P.MATERIAL_PRODUCT IS NOT NULL
            AND MOVEMENT_TYPE IN ('101', '102')
            AND M.MATERIAL IN ({$produk})
            AND PLANT IN ({$plant})
            )
            )
            GROUP BY 
                COMPANY,
                WORKCENTER,
                DESC_WORKCENTER,
                MATERIAL_PRODUCT,
                DESC_MATERIAL_PRODUCT,
                PLANT,
                PLANT_TO");
                // if($company==='7000'){
                    // echo $this->bcs->last_query();
                    
                // }
        return $data->result_array();
    }
    
     public function get_plantin($company){
		$data= $this->db->query("SELECT * from M_PLANT
        join M_COMPANY on M_COMPANY.ID_COMPANY = M_PLANT.ID_COMPANY
        WHERE KD_COMPANY = '{$company}'
        ORDER BY KD_PLANT");
        return $data->result_array();
     }
}
