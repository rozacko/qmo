<?php

class M_sampling Extends DB_QM {
    
	private $post  = array();
	private $table = "T_PROFICIENCY";
	private $pKey  = "ID_PROFICIENCY";
	private $column_order = array(NULL, "ID_PROFICIENCY"); //set column for datatable order
    private $column_search = array("ID_KOMODITI", "ID_LAB", "ID_PIC"); //set column for datatable search 
    private $order = array("ID_PROFICIENCY" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
    }

    public function detail($id){
        $this->get_query();
        $this->db->where($this->table.".ID_PROFICIENCY", $id);
        $query = $this->db->get();
		return $query->row();
    }

    public function get_query(){
		$this->db->select("*");
        $this->db->from($this->table);
        $this->db->join("T_PERIODE_PROFICIENCY", $this->table.".ID_PRIODE = T_PERIODE_PROFICIENCY.ID_PP");
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", $this->table.".ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->join("M_LABORATORIUM", $this->table.".ID_LAB = M_LABORATORIUM.ID_LAB");
        $this->db->join("M_USERS", $this->table.".ID_PIC = M_USERS.ID_USER");
		$this->db->where("T_PERIODE_PROFICIENCY.DELETE_AT IS NULL");
		$this->db->where($this->table.".DELETED_AT IS NULL");
		$this->db->where("T_PERIODE_PROFICIENCY.GROUP_PP", "Internal");
		$this->db->order_by("T_PERIODE_PROFICIENCY.YEAR_PP", "DESC");
	}

	public function data_pic($id_user = ""){
		$wher = "";
		if($id_user != ""){
			$where = "and mu.id_user = $id_user";
		}
		$sql = "
			select mu.*, ma.nm_area from m_users mu
			left join m_area ma on mu.id_area = ma.id_area
			where 
				mu.deleted = 0 and
				mu.isactive = 'Y' and
				mu.email is not null and
				mu.fullname is not null $where
			order by
				mu.fullname
		";
		$q = $this->db->query($sql);
		return $q->result();
	}

    public function get_list($akses = "", $id_user = "") {
        $this->get_query();
        if($akses == "sample_provider"){
        	$this->db->where("T_PROFICIENCY.ID_PIC", $id_user);
        }
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
    }

    public function update($data, $id){
        $this->db->set($data);
        $this->db->set("UPDATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);

        if(isset($data['FILE_SAMPLE_AT'])){
            $this->db->set("FILE_SAMPLE_AT", "TO_DATE('".$data['FILE_SAMPLE_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
        }

        if(isset($data['FILE_HOMOGEN_STABIL_1_AT'])){
            $this->db->set("FILE_HOMOGEN_STABIL_1_AT", "TO_DATE('".$data['FILE_HOMOGEN_STABIL_1_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
        }

        if(isset($data['FILE_HOMOGEN_STABIL_2_AT'])){
            $this->db->set("FILE_HOMOGEN_STABIL_2_AT", "TO_DATE('".$data['FILE_HOMOGEN_STABIL_2_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
        }

        if(isset($data['FILE_PENGEMASAN_AT'])){
            $this->db->set("FILE_PENGEMASAN_AT", "TO_DATE('".$data['FILE_PENGEMASAN_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
		}

		if(isset($data['FILE_PETUNJUK_TEKNIS_AT'])){
            $this->db->set("FILE_PETUNJUK_TEKNIS_AT", "TO_DATE('".$data['FILE_PETUNJUK_TEKNIS_AT']."', 'yyyy/mm/dd hh24:mi:ss')", false);
		}
        
        $this->db->where("ID_PROFICIENCY", $id);
		$this->db->update($this->table);
	}

	public function list_penerima($id, $akses = "", $user_id = ""){
		$this->db->select("*");
		$this->db->from("T_PENERIMA_PROFICIENCY");
		$this->db->where("ID_PROFICIENCY", $id);
		$this->db->where("DELETED_AT IS NULL");
		$this->db->order_by("ID_PENERIMA", "asc");
		if($akses == "peserta"){
			if($user_id != ""){
				$this->db->where("ID_USER_PENERIMA", $user_id);
			}
		}
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function add_penerima($data){
		$this->db->set("CREATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
		if(isset($data["TGL_PENGIRIMAN"])){
			$this->db->set("TGL_PENGIRIMAN", "TO_DATE('".$data['TGL_PENGIRIMAN']."', 'dd/mm/yyyy')", false);
		}
		unset($data['TGL_PENGIRIMAN']);
		$this->db->insert("T_PENERIMA_PROFICIENCY", $data);
	}

	public function edit_penerima($DATA, $ID_PENERIMA){
		$this->db->set("UPDATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
		$this->db->set("TGL_PENGIRIMAN", "TO_DATE('".$DATA['TGL_PENGIRIMAN']."', 'dd/mm/yyyy')", false);
		unset($DATA['TGL_PENGIRIMAN']);
		$this->db->where("ID_PENERIMA", $ID_PENERIMA);
		$this->db->update("T_PENERIMA_PROFICIENCY", $DATA);
	}

	public function get_detail_penerima($ID_PENERIMA){
		$this->db->select("*");
		$this->db->from("T_PENERIMA_PROFICIENCY");
		$this->db->where("ID_PENERIMA", $ID_PENERIMA);
		$this->db->where("DELETED_AT IS NULL");
		$query = $this->db->get();

		return $query->row();
	}

	public function delete_penerima($ID_PENERIMA){
        $this->db->set("DELETED_AT", "CURRENT_DATE", false);
		$this->db->where("ID_PENERIMA",$ID_PENERIMA);
		$this->db->update("T_PENERIMA_PROFICIENCY");
	}
}