<?php

class M_commodity Extends DB_QM {

    public function data_comodity(){
        $this->db->where("DELETED IS NULL");
		$this->db->order_by("NAMA_SAMPLE");
		return $this->db->get("M_SAMPLE_UJI_PROFICIENCY")->result();
    }
	
	public function save_commodity(){
		$kode 	 = $this->input->post('kode_sample');
		$nama	 = $this->input->post('nama_sample');
		
		$this->db->set("KODE_SAMPLE", $kode);
		$this->db->set("NAMA_SAMPLE", $nama);
		$q = $this->db->insert("M_SAMPLE_UJI_PROFICIENCY");

		return $q;
	}
	
	public function edit_commodity(){
		$id 	 = $this->input->post('id_sample');
		$kode 	 = $this->input->post('kode_sample');
		$nama	 = $this->input->post('nama_sample');
		
		$this->db->set("KODE_SAMPLE", $kode);
		$this->db->set("NAMA_SAMPLE", $nama);
		$this->db->where("ID_SAMPLE", $id);
		$q = $this->db->update("M_SAMPLE_UJI_PROFICIENCY");

		return $q;
	}
	
	public function delete_commodity($id){
		
		$this->db->set("DELETED", 1);
		$this->db->where("ID_SAMPLE", $id);
		$q = $this->db->update("M_SAMPLE_UJI_PROFICIENCY");

		return $q;
	}
    
}

?>