<?php
	class M_detail_plant Extends DB_QM {
		private $post  = array();
		private $table = "M_DETAIL_PLANT";
		private $pKey  = "ID_DETAIL_PLANT";
		private $column_order = array('NM_COMPANY', 'NM_PLANT', 'KD_DETAIL_PLANT', 'NM_DETAIL_PLANT'); //set column for datatable order
		private $column_search = array('NM_COMPANY', 'NM_PLANT', 'KD_DETAIL_PLANT', 'NM_DETAIL_PLANT'); //set column for datatable search 
		private $order = array("ID_DETAIL_PLANT" => 'DESC'); //default order

		public function __construct(){
			$this->post = $this->input->post();
		}

		public function get_query(){
			$this->db->select('*');
			$this->db->from($this->table);
			$this->db->join('M_PLANT', $this->table.".ID_PLANT = M_PLANT.ID_PLANT");
			$this->db->join('M_COMPANY', 'M_PLANT.ID_COMPANY = M_COMPANY.ID_COMPANY');
		}

		public function get_list() {
			$this->get_query();
			$i = 0;

			//Loop column search
			foreach ($this->column_search as $item) {
				if($this->post['search']['value']){
					if($i===0){ //first loop
						$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
						$this->db->like($item, strtoupper($this->post['search']['value']));
					}else{
						$this->db->or_like($item, strtoupper($this->post['search']['value']));
					}

					if(count($this->column_search) - 1 == $i){ //last loop
						$this->db->group_end(); //close bracket
					}
				}
				$i++;
			}

			if(isset($this->post['order'])){ //order datatable
				$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
			}elseif (isset($this->order)) {
				$this->db->order_by(key($this->order), $this->order[key($this->order)]);
			}

			if($this->post['length'] != -1){
				$this->db->limit($this->post['length'],$this->post['start']);
				$query = $this->db->get();
			}else{
				$query = $this->db->get();
			}

			return $query->result();
		}

		public function count_filtered(){
			$this->get_query();
			$query = $this->db->get();
			return (int) $query->num_rows();
		}

		public function count_all(){
			$this->get_query();
			$query = $this->db->get();
			return (int) $query->num_rows();
		}

		public function get_plant_company_use_detail($ID_COMPANY){
			$this->db->select('*');
			$this->db->from('M_PLANT');
			$this->db->where('ID_COMPANY', $ID_COMPANY);
			$query = $this->db->get();
			return $query->result();
		}

		public function get_data_by_id($ID_DETAIL_PLANT){
			$this->get_query();
			$this->db->where('ID_DETAIL_PLANT', $ID_DETAIL_PLANT);
			$query = $this->db->get();
			return $query->row();
		}

		public function insert($data){
			$this->db->set($data);
			$this->db->set("ID_DETAIL_PLANT","M_DETAIL_PLANT_SEQ.NEXTVAL",FALSE);
			$this->db->insert($this->table);
		}

		public function update($data,$ID_DETAIL_PLANT){
			$this->db->set($data);
			$this->db->where("ID_DETAIL_PLANT",$ID_DETAIL_PLANT);
			$this->db->update($this->table);
		}

		public function delete($ID_DETAIL_PLANT){
			$this->db->where("ID_DETAIL_PLANT",$ID_DETAIL_PLANT);
			$this->db->delete($this->table);
		}
	}