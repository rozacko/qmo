<?php

class M_periode_proficiency Extends DB_QM {
	
	public function get_list(){
        $this->db->where("DELETE_BY IS NULL");
		$this->db->where("GROUP_PP","Internal");
		$this->db->order_by("YEAR_PP","DESC");
		return $this->db->get("T_PERIODE_PROFICIENCY")->result();
    }
	
	public function get_gp(){
		$this->db->where("KET","grp-pp");
		return $this->db->get("M_GROUP_PROFICIENCY")->result();
    }
	
	public function save(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$group_pp 	 = $this->input->post('group_pp');
		$nama_pp	 = $this->input->post('nama_pp');
		$tahun_pp	 = $this->input->post('tahun_pp');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			INSERT INTO T_PERIODE_PROFICIENCY (GROUP_PP, TITLE_PP, YEAR_PP, CREATE_AT, CREATE_BY, STATUS) VALUES ('{$group_pp}', '{$nama_pp}', {$tahun_pp}, TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), '{$user_in->FULLNAME}', 'Open')
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function edit(){
		$id_pp		 = $this->input->post('id_pp');
		$group_pp 	 = $this->input->post('group_pp');
		$nama_pp	 = $this->input->post('nama_pp');
		$tahun_pp	 = $this->input->post('tahun_pp');
		
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
		
		$sql = "
			UPDATE T_PERIODE_PROFICIENCY SET GROUP_PP = '{$group_pp}', TITLE_PP = '{$nama_pp}', YEAR_PP = {$tahun_pp} WHERE ID_PP = {$id_pp}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function edit_status(){
		$id_pp		 = $this->input->post('id_pp');
		$status_pp 	 = $this->input->post('status_pp');
		
		$sql = "
			UPDATE T_PERIODE_PROFICIENCY SET STATUS = '{$status_pp}' WHERE ID_PP = {$id_pp}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
	public function deleted($id){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now));
		
		$sql = "
			UPDATE T_PERIODE_PROFICIENCY SET DELETE_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), DELETE_BY = '{$user_in->FULLNAME}' WHERE ID_PP = {$id}
		";
		$q = $this->db->query($sql);
		return $q;
	}
	
}

?>