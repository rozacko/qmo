<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_cronjob Extends DB_QM {

	public function __construct(){
			parent::__construct();
			// $this->db = $this->load->database('mso_prod', TRUE);
	}

	// khusus kiln (grouparea 4)
	public function get_data_daily($area, $tanggal, $ID_GROUPAREA = '4'){
		 // TES QUERY
		// SELECT A.ID_CEMENT_HOURLY, A.ID_AREA, A.ID_PRODUCT, A.DATE_DATA, A.JAM_DATA, B.*  FROM T_CEMENT_HOURLY A
		// JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
		// WHERE to_char(A.DATE_DATA,'DD-MM-YYYY') = '01-08-2020'
		// AND A.ID_AREA = 21 AND NILAI IS NOT NULL
		// ORDER BY A.JAM_DATA ASC, NO_FIELD ASC


		// SELECT  AVG(B.NILAI) AS NILAI_AVG, B.ID_COMPONENT  FROM T_CEMENT_HOURLY A
		// JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
		// WHERE to_char(A.DATE_DATA,'DD-MM-YYYY') = '01-08-2020'
		// AND A.ID_AREA = 21 AND NILAI IS NOT NULL
		// GROUP BY B.ID_COMPONENT 
		// ORDER BY A.JAM_DATA ASC, NO_FIELD ASC

		$sql = "SELECT  ROUND(AVG(B.NILAI), 2) AS NILAI_AVG, B.ID_COMPONENT, E.KD_COMPONENT FROM T_CEMENT_HOURLY A
						JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
						JOIN M_AREA C ON A.ID_AREA = C.ID_AREA
						JOIN M_GROUPAREA D ON C.ID_GROUPAREA = D.ID_GROUPAREA
						JOIN M_COMPONENT E ON B.ID_COMPONENT = E.ID_COMPONENT
						WHERE to_char(A.DATE_DATA,'DD/MM/YYYY') = '{$tanggal}'
							-- AND A.ID_AREA = {$area}  AND D.ID_GROUPAREA = '4'
							AND A.ID_AREA = {$area}  AND D.ID_GROUPAREA = $ID_GROUPAREA -- izza
						GROUP BY A.ID_AREA, B.ID_COMPONENT, E.KD_COMPONENT
						ORDER BY  A.ID_AREA";
		return $this->db->query($sql)->result_array();
	}
	
	// khusus kiln (grouparea 4)
	public function get_data_area($tanggal, $ID_GROUPAREA = '4'){ 
		$sql = "SELECT   A.ID_AREA  FROM T_CEMENT_HOURLY A
						JOIN D_CEMENT_HOURLY B ON A.ID_CEMENT_HOURLY = B.ID_CEMENT_HOURLY
						JOIN M_AREA C ON A.ID_AREA = C.ID_AREA
						JOIN M_GROUPAREA D ON C.ID_GROUPAREA = D.ID_GROUPAREA
						WHERE to_char(A.DATE_DATA,'DD/MM/YYYY') = '{$tanggal}'
							-- AND   NILAI IS NOT NULL  AND D.ID_GROUPAREA = '4'
							AND   NILAI IS NOT NULL  AND D.ID_GROUPAREA = $ID_GROUPAREA -- izza 
						GROUP BY A.ID_AREA 
						ORDER BY  A.ID_AREA ASC";
		return $this->db->query($sql)->result_array();
	}

	public function get_data_daily_production($area, $tanggal){ 
		return $this->db->query("SELECT  ROUND(AVG(B.NILAI), 2) AS NILAI_AVG, B.ID_COMPONENT   FROM T_PRODUCTION_HOURLY A
		JOIN D_PRODUCTION_HOURLY B ON A.ID_PRODUCTION_HOURLY = B.ID_PRODUCTION_HOURLY
		 WHERE to_char(A.DATE_DATA,'DD/MM/YYYY') = '{$tanggal}'
		 AND A.ID_AREA = '{$area}' 
		GROUP BY A.ID_AREA, B.ID_COMPONENT  
		ORDER BY  A.ID_AREA   ")->result_array();
	}
	
	
	public function get_data_area_production($tanggal){ 
		return $this->db->query("SELECT
				A.ID_AREA 
			FROM
				T_PRODUCTION_HOURLY A
				JOIN D_PRODUCTION_HOURLY B ON A.ID_PRODUCTION_HOURLY = B.ID_PRODUCTION_HOURLY 
			WHERE
				to_char( A.DATE_DATA, 'DD/MM/YYYY' ) = '{$tanggal}'
				AND NILAI IS NOT NULL  
			GROUP BY
				A.ID_AREA 
			ORDER BY
				A.ID_AREA ASC")->result_array();
	}
	
	
	public function get_data() {
		return $this->db->query("SELECT
							* 
						FROM
							M_JADWAL
							LEFT JOIN M_USERS ON M_JADWAL.CREATE_BY = M_USERS.ID_USER 
						WHERE
							DELETE_MARK = 0
						ORDER BY M_JADWAL.STATUS ASC, ID_JADWAL DESC
                         
                ")->result_array();
	}
	
	public function get_data_terjadwal(){ // izza 30.03.2021
		$this->db->where('DELETE_MARK', '0'); // data masih ada
		$this->db->where('STATUS', '0'); // data blm pernah di sinkron
		$this->db->order_by("TGL_JADWAL");
		$result = $this->db->get("M_JADWAL")->result_array();
		return $result;
	}
	
    public function cek_data($tanggal, $jam ) {
		$jam = intval($jam);
		if($jam==''){
			$wjam = " AND to_char( TGL_DATA, 'DD/MM/YYYY' ) = '{$tanggal}'";
		}else{
			$wjam = " AND to_char( TGL_JADWAL, 'DD/MM/YYYY' ) = '{$tanggal}' AND JAM_JADWAL = '{$jam}'";
		}
		return $this->db->query("SELECT TGL_DATA_START, TGL_DATA_END FROM M_JADWAL
			LEFT JOIN M_USERS ON M_JADWAL.CREATE_BY = M_USERS.ID_USER
			WHERE DELETE_MARK = 0  AND STATUS = '0'  {$wjam}
                         
                ")->result_array();
	}
	
	public function simpan($column, $data) {
		return $this->db->query("INSERT INTO M_JADWAL (".implode(",", $column).") VALUES (".implode(",", $data).")");
	}
	
	public function hapus($id) {
		return $this->db->query("UPDATE  M_JADWAL SET DELETE_MARK = 1 WHERE ID_JADWAL={$id}");
	}
	public function update_jadwal($tanggal, $end, $tot_insert, $tot_update) {
		return $this->db->query("UPDATE  M_JADWAL SET STATUS = 1, INSERT_HEADER = '{$tot_insert}', UPDATE_HEADER = '{$tot_update}' WHERE to_char( TGL_DATA_START, 'DD/MM/YYYY' ) = '{$tanggal}' AND to_char( TGL_DATA_END, 'DD/MM/YYYY' ) = '{$end}'");
	}

	public function insert_production($data){ 
      $this->db->insert_batch('D_PRODUCTION_DAILY', $data);
    }			

}
