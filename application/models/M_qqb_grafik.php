<?php
	class M_qqb_grafik Extends DB_QM {

		public function get_data_qbb_chart($plant, $date_start, $date_end){
			$arr_plant = array();
			foreach($plant as $pl){
				array_push($arr_plant, $pl);
			}

			$sql = "SELECT
						TKM.KD_PLANT,
						TKM.NAMA_PLANT,
						to_char( TKM.CREATE_AT, 'MM-YYYY' ) AS PERIODE,
						AVG( TKM.TOTAL_MOISTURE ) AS TOTAL_MOISTURE,
						AVG( TKM.INHERENT_MOISTURE ) AS INHERENT_MOISTURE,
						AVG( TKM.ASH_CONTENT ) AS ASH_CONTENT,
						AVG( TKM.CALORI_AR ) AS CALORI_AR,
						AVG( TKM.CALORI_ADB ) AS CALORI_ADB 
					FROM
						T_KUALITAS_MATERIAL TKM 
					WHERE
						TKM.KD_PLANT IN ( ".implode(',', $arr_plant)." ) 
						AND TKM.CREATE_AT BETWEEN TO_DATE( '".$date_start."', 'DD/MM/YYYY' ) 
						AND TO_DATE( '".$date_end."', 'DD/MM/YYYY' ) 
					GROUP BY
						TKM.KD_PLANT,
						TKM.NAMA_PLANT,
						to_char( TKM.CREATE_AT, 'MM-YYYY' ) 
					ORDER BY
						to_char(
						TKM.CREATE_AT,
						'MM-YYYY' 
						)";
			$query = $this->db->query($sql);
			if($query->num_rows() > 0){
				return $query->result_array();
			} else {
				return false;
			}
		}

		public function data_plant_company($company = null, $plant = null){
			if(isset($company)){
				$this->db->where_in("ID_COMPANY", $company);
			}

			if(isset($plant)){
				$this->db->where_in("KD_PLANT", $plant);
			}
			$this->db->distinct();
			$this->db->select("KD_PLANT, NAMA_PLANT");
			$this->db->from("T_KUALITAS_MATERIAL");
			

			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result_array();
			} else {
				return array();
			}
		}


	}