<?php

class M_penerimaan Extends DB_QM {

    public function get_query(){
		$this->db->select("*");
        $this->db->from("T_PERIODE_PROFICIENCY");
        $this->db->join("T_PROFICIENCY", "T_PERIODE_PROFICIENCY.ID_PP = T_PROFICIENCY.ID_PRIODE");
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", "T_PROFICIENCY.ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->join ("T_PENERIMA_PROFICIENCY", "T_PROFICIENCY.ID_PROFICIENCY = T_PENERIMA_PROFICIENCY.ID_PROFICIENCY");
        $this->db->where("T_PERIODE_PROFICIENCY.DELETE_AT IS NULL");
        $this->db->where("T_PROFICIENCY.DELETED_AT IS NULL");
        $this->db->where("T_PENERIMA_PROFICIENCY.DELETED_AT IS NULL");
		$this->db->where("T_PENERIMA_PROFICIENCY.TGL_PENERIMAAN IS NOT NULL");
		$this->db->where("T_PERIODE_PROFICIENCY.GROUP_PP", "Internal");
	}

    public function get_list() {
        $this->get_query();
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
    }

    public function get_proficiency($ID_PROFICIENCY = ""){
		$this->db->select("*");
		$this->db->from("T_PERIODE_PROFICIENCY");
		$this->db->join("T_PROFICIENCY", "T_PERIODE_PROFICIENCY.ID_PP = T_PROFICIENCY.ID_PRIODE");
        $this->db->join("M_SAMPLE_UJI_PROFICIENCY", "T_PROFICIENCY.ID_KOMODITI = M_SAMPLE_UJI_PROFICIENCY.ID_SAMPLE");
        $this->db->join("M_LABORATORIUM", "T_PROFICIENCY.ID_LAB = M_LABORATORIUM.ID_LAB");
        $this->db->join("M_USERS", "T_PROFICIENCY.ID_PIC = M_USERS.ID_USER");
        $this->db->where("T_PERIODE_PROFICIENCY.DELETE_AT IS NULL");
        $this->db->where("T_PROFICIENCY.DELETED_AT IS NULL");
        $this->db->where("T_PROFICIENCY.FILE_SAMPLE IS NOT NULL");
        $this->db->where("T_PROFICIENCY.FILE_HOMOGEN_STABIL_1 IS NOT NULL");
        $this->db->where("T_PROFICIENCY.FILE_HOMOGEN_STABIL_2 IS NOT NULL");
        $this->db->where("T_PROFICIENCY.FILE_PENGEMASAN IS NOT NULL");
		$this->db->where("T_PROFICIENCY.ID_PROFICIENCY IN (SELECT DISTINCT ID_PROFICIENCY FROM T_PENERIMA_PROFICIENCY WHERE DELETED_AT IS NULL)");
		$this->db->where("T_PERIODE_PROFICIENCY.GROUP_PP", "Internal");
		if($ID_PROFICIENCY != ""){
			$this->db->where("T_PROFICIENCY.ID_PROFICIENCY", $ID_PROFICIENCY);
		}

		$query = $this->db->get();
		return $query->result();
    }

    public function get_penerima($id = "", $id_penerima = ""){
        $this->db->select("*");
		$this->db->from("T_PENERIMA_PROFICIENCY");
		if($id != ""){
			$this->db->where("T_PENERIMA_PROFICIENCY.ID_PROFICIENCY", $id);
		}

		if($id_penerima != ""){
			$this->db->where("T_PENERIMA_PROFICIENCY.ID_PENERIMA", $id_penerima);
		}
		$this->db->join("T_PROFICIENCY", "T_PENERIMA_PROFICIENCY.ID_PROFICIENCY = T_PROFICIENCY.ID_PROFICIENCY");
		$this->db->join("T_PERIODE_PROFICIENCY", "T_PROFICIENCY.ID_PRIODE = T_PERIODE_PROFICIENCY.ID_PP");
		$this->db->join("M_USERS", "T_PROFICIENCY.ID_PIC = M_USERS.ID_USER");
        $this->db->where("T_PENERIMA_PROFICIENCY.TGL_PENERIMAAN IS NULL");
        $this->db->where("T_PENERIMA_PROFICIENCY.DELETED_AT IS NULL");
		$this->db->order_by("T_PENERIMA_PROFICIENCY.ID_PENERIMA", "asc");
        $query = $this->db->get();
		return $query->result();
    }

    public function update($data, $ID_PENERIMA){
        $sesi_user = $this->session->userdata();
        $user = $sesi_user['USER'];
		
		if(isset($data['TGL_PENERIMAAN'])){
			$this->db->set("TGL_PENERIMAAN", "TO_DATE('".$data['TGL_PENERIMAAN']."', 'dd/mm/yyyy')", false);
        	unset($data["TGL_PENERIMAAN"]);
		}

		if(isset($data['START_TGL_UJI'])){
			$this->db->set("START_TGL_UJI", "TO_DATE('".$data['START_TGL_UJI']."', 'dd/mm/yyyy')", false);
        	unset($data["START_TGL_UJI"]);
		}

		if(isset($data['END_TGL_UJI'])){
			$this->db->set("END_TGL_UJI", "TO_DATE('".$data['END_TGL_UJI']."', 'dd/mm/yyyy')", false);
        	unset($data["END_TGL_UJI"]);
		}
        
        $this->db->set($data);
        $this->db->set("UPDATED_AT", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
        $this->db->set("UPDATED_BY", $user_in->FULLNAME);
        $this->db->where("ID_PENERIMA",$ID_PENERIMA);
		$this->db->update("T_PENERIMA_PROFICIENCY");
    }
    
    
}