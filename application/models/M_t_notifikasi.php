<?php

class M_t_notifikasi Extends DB_QM {

	public function insert($id_opco, $id_incident, $id_jabatan, $id_incident_type){

		$sql = "
			insert into T_NOTIFIKASI 
			(ID_NOTIFIKASI, ID_OPCO, ID_INCIDENT, ID_JABATAN, TANGGAL, ID_INCIDENT_TYPE ) 
		   	values
		   	(SEQ_ID_NOTIFIKASI.NEXTVAL, '".$id_opco."', '".$id_incident."', '".$id_jabatan."', SYSDATE, '".$id_incident_type."')
		";

		$this->db->query($sql);

	}

	public function getData($where){
		$this->db->where($where);
		return $this->db->get('T_NOTIFIKASI')->result_array();
	}
}
