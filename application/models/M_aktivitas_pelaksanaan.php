<?php

class M_aktivitas_pelaksanaan Extends DB_QM {
	
	public function get_list($id_pp = false){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, ML.NAMA_LAB, MU.FULLNAME, TPP.*");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.ID_LAB","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.GROUP_PP","Internal");
		$this->db->where("TPP.DELETE_BY IS NULL");
		if($id_pp != null){
			$this->db->where("TP.ID_PRIODE", $id_pp);
		} 
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
	}
	
	public function get_list_non_admin($id_pic){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, ML.NAMA_LAB, MU.FULLNAME, TPP.*");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.ID_LAB","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TPP.GROUP_PP","Internal");
		$this->db->where("TPP.STATUS","On-progress");
		$this->db->where("TPP.DELETE_BY IS NULL");
		$this->db->where("TP.ID_PIC", $id_pic);
		$this->db->order_by("TPP.YEAR_PP","DESC");
		$this->db->order_by("TP.CREATED_AT","DESC");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->result();
	}
	
	public function get_data_by_id($id){
		$this->db->select("TP.*,  SUP.NAMA_SAMPLE, ML.NAMA_LAB, MU.FULLNAME, TPP.*");
		$this->db->join("M_USERS MU","MU.ID_USER = TP.ID_PIC","LEFT");
		$this->db->join("M_SAMPLE_UJI_PROFICIENCY SUP","SUP.ID_SAMPLE = TP.ID_KOMODITI","LEFT");
		$this->db->join("M_LABORATORIUM ML","ML.ID_LAB = TP.ID_LAB","LEFT");
		$this->db->join("T_PERIODE_PROFICIENCY TPP","TPP.ID_PP = TP.ID_PRIODE","LEFT");
		$this->db->where("TP.DELETED_BY IS NULL");
		$this->db->where("TP.ID_PROFICIENCY", $id);
		$this->db->order_by("TP.CREATED_AT");
		$query = $this->db->get('T_PROFICIENCY TP');
		return $query->row();
	}
	
	public function get_data_activity($id){
		$this->db->where("ID_PROFICIENCY", $id);
		$this->db->where("DELETED_BY IS NULL");
		$this->db->order_by("PLAN_DATE_START");
		$query = $this->db->get('T_ACTIVITY');
		return $query->result();
	}
	
	public function update_activity(){
		$sesi_user = $this->session->userdata();
		$user_in = $sesi_user['USER'];
		
		$id_activity = $this->input->post('ID_ACTIVITY');
		$start 	  = $this->input->post('start_date');
		$end	  = $this->input->post('end_date');
		
		$start_in 	= date("Y-m-d", strtotime($start));
		$end_in 	= date("Y-m-d", strtotime($end)); 
		$date_now 	= date("Y-m-d");
		$date_in 	= date("Y-m-d", strtotime($date_now)); 
				
		$sql = "
			UPDATE T_ACTIVITY SET ACT_DATE_START = TO_DATE('{$start_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), ACT_DATE_END = TO_DATE('{$end_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_AT = TO_DATE('{$date_in} 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), UPDATED_BY = '{$user_in->FULLNAME}' WHERE ID_ACTIVITY = {$id_activity}
		";
		$q = $this->db->query($sql);
		return $q;
	}
}

?>