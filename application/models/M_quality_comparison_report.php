<?php

class M_quality_comparison_report Extends DB_QM {
	private $post  = array();
	private $table = "M_GROUPAREA";
	private $pKey  = "ID_GROUPAREA";
	private $column_order = array(NULL, 'ID_GROUPAREA'); //set column for datatable order
	private $column_search = array('NM_GROUPAREA', 'KD_GROUPAREA'); //set column for datatable search
	private $order = array("ID_GROUPAREA" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	public function get_area_sample_list_scm($data){
		$this->scmdb->where("ID_M_KOTA", $data['ID_SAMPLE_AREA']);
		return $this->scmdb->get("ZREPORT_M_KOTA")->row_array();
	}

	public function get_competitor_detail_scm($key){
		$this->scmdb->where("KODE_PERUSAHAAN", $key);
		return $this->scmdb->get("ZREPORT_MS_PERUSAHAAN")->row_array();
	}

	public function get_prov_sample_list_scm($data){
		$this->scmdb->where("KD_PROV", $data);
		return $this->scmdb->get("ZREPORT_M_PROVINSI")->row_array();
	}

	public function get_cluster_national($months, $id_typeproduct){
		$this->scmdb->select("A.KODE_PERUSAHAAN, A.NAMA_PERUSAHAAN, B.INISIAL, B.PRODUK, B.KELOMPOK, SUM(A.QTY_REAL) AS REALISASI_MS");
		$this->scmdb->join("ZREPORT_MS_PERUSAHAAN B", "B.KODE_PERUSAHAAN = A.KODE_PERUSAHAAN", "RIGHT");
		if ( (int) $id_typeproduct == 0 ) {
			# code...
			$this->scmdb->where("TIPE", '121-301');
		} else {
			# code...
			$this->scmdb->where("TIPE", '121-302');
		}
		$between = "TO_DATE(CONCAT(TAHUN, BULAN), 'YYYY-MM') BETWEEN TO_DATE('".$months['start']."', 'YYYY-MM') AND TO_DATE('".$months['end']."', 'YYYY-MM')";
		$this->scmdb->where($between);
		$this->scmdb->order_by("A.KODE_PERUSAHAAN, A.NAMA_PERUSAHAAN, B.INISIAL, B.PRODUK, B.KELOMPOK");
		$this->scmdb->group_by("A.KODE_PERUSAHAAN, A.NAMA_PERUSAHAAN, B.INISIAL, B.PRODUK, B.KELOMPOK");
		return $this->scmdb->get("ZREPORT_MS_TRANS1 A")->result_array();
	}

	public function get_total_ms_scm($months, $id_typeproduct){
		$this->scmdb->select("SUM(A.QTY_REAL) AS REALISASI_MS");
		if ( (int) $id_typeproduct == 0 ) {
			# code...
			$this->scmdb->where("TIPE", '121-301');
		} else {
			# code...
			$this->scmdb->where("TIPE", '121-302');
		}
		$between = "TO_DATE(CONCAT(TAHUN, BULAN), 'YYYY-MM') BETWEEN TO_DATE('".$months['start']."', 'YYYY-MM') AND TO_DATE('".$months['end']."', 'YYYY-MM')";
		$this->scmdb->where($between);
		return $this->scmdb->get("ZREPORT_MS_TRANS1 A")->row_array();
	}

	public function get_component_value_national($id_company, $tanggal, $id_typeproduct, $id_component){
		$this->db->select("COALESCE (AVG(A .COMPONENT_VALUE), 0) AS RERATA");
		$this->db->join("M_COMPONENT B", "B.ID_COMPONENT = A.ID_COMPONENT");
		$this->db->join("M_PRODUCT E", "E.ID_PRODUCT = A.ID_TYPE_PRODUCT");
		if ( (int) $id_typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(E.KD_PRODUCT) = 'ppc' OR LOWER(E.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(E.KD_PRODUCT) = 'opc'");
		}
		$between = "A.DATE_COLLECT BETWEEN TO_DATE ('".$tanggal['start']."', 'YYYY-MM-DD') AND TO_DATE ('".$tanggal['end']."', 'YYYY-MM-DD')";
		$this->db->where($between);
		$this->db->where("A.ID_COMPETITOR", $id_company);
		$this->db->where("A.ID_COMPONENT", $id_component);
		return $this->db->get("O_SAMPLE_QUALITY A")->row_array();
	}

	public function get_marketshare_value_scm($provinsi, $months, $competitor, $id_typeproduct){

		$this->scmdb->select("SUM(QTY_REAL) AS MS_QTY");
		if ( (int) $id_typeproduct == 0 ) {
			# code...
			$this->scmdb->where("TIPE", '121-301');
		} else {
			# code...
			$this->scmdb->where("TIPE", '121-302');
		}
		$this->scmdb->where("PROPINSI", $provinsi);
		if ($competitor) {
			# code...
			$this->scmdb->where("KODE_PERUSAHAAN", $competitor);
		}

		$between = "TO_DATE(CONCAT(TAHUN, BULAN), 'YYYY-MM') BETWEEN TO_DATE('".$months['start']."', 'YYYY-MM') AND TO_DATE('".$months['end']."', 'YYYY-MM')";
		$this->scmdb->where($between);
		$data = $this->scmdb->get("ZREPORT_MS_TRANS1")->row_array();
		return $data;
	}

	public function get_cluster($id_company, $tanggal, $id_typeproduct, $areagroup){
		if ($areagroup == 'Province') {
			# code...	
		} else if ($areagroup == 'Island') {
			# code...
		} else if ($areagroup == 'Region') {
			# code...
		} else if ($areagroup == 'National') {
			# code...
		}

		$this->db->select("DISTINCT TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') AS PERIODE, A.ID_COMPANY, A.ID_SAMPLE_AREA, A.ID_TYPE_PRODUCT");
		$this->db->join("M_PRODUCT D", "A.ID_TYPE_PRODUCT = D.ID_PRODUCT");

		if ( (int) $id_typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(D.KD_PRODUCT) = 'ppc' OR LOWER(D.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(D.KD_PRODUCT) = 'opc'");
		}

		$this->db->where("A.ID_COMPANY", $id_company);

		$startdate = $tanggal['start'];
		$enddate = $tanggal['end'];

		$between = "A.DATE_COLLECT BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);

		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}

	public function get_cluster_detail($id_company, $tanggal, $id_typeproduct, $areagroup, $listarea){
		if ($areagroup == 'Province') {
			# code...		
		} else if ($areagroup == 'Island') {
			# code...
		} else if ($areagroup == 'Region') {
			# code...
		} else if ($areagroup == 'National') {
			# code...
		}

		$this->db->select("COALESCE (AVG(A .COMPONENT_VALUE), 0) AS RERATA, A .ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A .ID_COMPETITOR, A .ID_COMPANY --, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY");
		$this->db->order_by(" A.ID_COMPONENT--, C.MERK_PRODUCT, A.ID_COMPONENT");
		$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, A .ID_COMPANY--, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY");

		$this->db->join("M_COMPONENT B", "B.ID_COMPONENT = A.ID_COMPONENT");
		$this->db->join("M_PRODUCT E", "E.ID_PRODUCT = A.ID_TYPE_PRODUCT");

		$this->db->where_in('A.ID_SAMPLE_AREA', $listarea);

		if ( (int) $id_typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(E.KD_PRODUCT) = 'ppc' OR LOWER(E.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(E.KD_PRODUCT) = 'opc'");
		}

		$startdate = $tanggal['start'];
		$enddate = $tanggal['end'];

		$between = "A.DATE_COLLECT BETWEEN TO_DATE ('$startdate', 'YYYY-MM-DD') AND TO_DATE ('$enddate', 'YYYY-MM-DD')";
		$this->db->where($between);

		$this->db->where("A.ID_COMPANY", $id_company);

		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}

	public function get_cluster_detaile($id_company, $tanggal, $id_typeproduct, $areagroup, $listarea){
		if ($areagroup == 'Province') {
			# code...		
		} else if ($areagroup == 'Island') {
			# code...
		} else if ($areagroup == 'Region') {
			# code...
		} else if ($areagroup == 'National') {
			# code...
		}

		$this->db->select("COALESCE (AVG(A .COMPONENT_VALUE), 0) AS RERATA, A .ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A .ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY");
		$this->db->order_by("C.MERK_PRODUCT, A.ID_COMPONENT");
		$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY");

		$this->db->join("M_COMPONENT B", "B.ID_COMPONENT = A.ID_COMPONENT");
		$this->db->join("O_COMPETITOR C", "C.ID_COMPETITOR = A.ID_COMPETITOR");
		$this->db->join("M_PRODUCT E", "E.ID_PRODUCT = C.TYPE_PRODUCT");

		$this->db->where("TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') = ", $tanggal);

		$this->db->where_in('A.ID_SAMPLE_AREA', $listarea);

		if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(E.KD_PRODUCT) = 'ppc' OR LOWER(E.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(E.KD_PRODUCT) = 'opc'");
		}

		$this->db->where("A.ID_COMPANY", $id_company);

		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}


	public function get_cluster_backup($id_company, $tanggal, $id_typeproduct, $areagroup){
		if ($areagroup == 'Province') {
			# code...
			$this->db->select("DISTINCT TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') AS PERIODE, A.ID_COMPANY, B.NAME_PROVINCE, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, D.KD_PRODUCT");
			$this->db->order_by("B.NAME_PROVINCE");
		
		} else if ($areagroup == 'Island') {
			# code...
			$this->db->select("DISTINCT TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') AS PERIODE, A.ID_COMPANY, B.NAME_ISLAND, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, D.KD_PRODUCT");
			$this->db->order_by("B.NAME_ISLAND");
		} else if ($areagroup == 'Region') {
			# code...
			$this->db->select("DISTINCT TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') AS PERIODE, A.ID_COMPANY, B.NAME_REGION, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, D.KD_PRODUCT");
			$this->db->order_by("B.NAME_REGION");
		} else if ($areagroup == 'National') {
			# code...
			$this->db->select("DISTINCT TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') AS PERIODE, A.ID_COMPANY, C.TYPE_PRODUCT, D.KD_PRODUCT");
		}

		$this->db->join("O_SAMPLE_AREA B", "A.ID_SAMPLE_AREA = B.ID_SAMPLE_AREA");
		$this->db->join("O_COMPETITOR C", "A.ID_COMPETITOR = C.ID_COMPETITOR");
		$this->db->join("M_PRODUCT D", "C.TYPE_PRODUCT = D.ID_PRODUCT");

		$this->db->where("TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') = ", $tanggal);

		if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(D.KD_PRODUCT) = 'ppc' OR LOWER(D.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(D.KD_PRODUCT) = 'opc'");
		}

		$this->db->where("A.ID_COMPANY", $id_company);
		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}

	public function get_cluster_detail_backup($id_company, $tanggal, $id_typeproduct, $areagroup){
		if ($areagroup == 'Province') {
			# code...
			$this->db->select("COALESCE (AVG(A.COMPONENT_VALUE), 0) AS RERATA, A.ID_COMPONENT,	B.KD_COMPONENT, B.NM_COMPONENT,	A.ID_COMPETITOR, C.NAME_COMPETITOR,	C.MERK_PRODUCT, C.TYPE_PRODUCT,	C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_PROVINCE AS GROUP_AREA");
			$this->db->order_by("D.NAME_PROVINCE, C.MERK_PRODUCT, A.ID_COMPONENT");
			$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_PROVINCE");
		
		} else if ($areagroup == 'Island') {
			# code...
			$this->db->select("COALESCE (AVG(A.COMPONENT_VALUE), 0) AS RERATA, A.ID_COMPONENT,	B.KD_COMPONENT, B.NM_COMPONENT,	A.ID_COMPETITOR, C.NAME_COMPETITOR,	C.MERK_PRODUCT, C.TYPE_PRODUCT,	C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_ISLAND AS GROUP_AREA");
			$this->db->order_by("D.NAME_ISLAND, C.MERK_PRODUCT, A.ID_COMPONENT");
			$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_ISLAND");

		} else if ($areagroup == 'Region') {
			# code...
			$this->db->select("COALESCE (AVG(A.COMPONENT_VALUE), 0) AS RERATA, A.ID_COMPONENT,	B.KD_COMPONENT, B.NM_COMPONENT,	A.ID_COMPETITOR, C.NAME_COMPETITOR,	C.MERK_PRODUCT, C.TYPE_PRODUCT,	C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_REGION AS GROUP_AREA");
			$this->db->order_by("D.NAME_REGION, C.MERK_PRODUCT, A.ID_COMPONENT");
			$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT, D.NAME_REGION");

		} else if ($areagroup == 'National') {
			# code...
			$this->db->select("COALESCE (AVG(A.COMPONENT_VALUE), 0) AS RERATA, A.ID_COMPONENT,	B.KD_COMPONENT, B.NM_COMPONENT,	A.ID_COMPETITOR, C.NAME_COMPETITOR,	C.MERK_PRODUCT, C.TYPE_PRODUCT,	C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT");
			$this->db->order_by("C.MERK_PRODUCT, A.ID_COMPONENT");
			$this->db->group_by("A.ID_COMPONENT, B.KD_COMPONENT, B.NM_COMPONENT, A.ID_COMPETITOR, C.NAME_COMPETITOR, C.MERK_PRODUCT, C.TYPE_PRODUCT, C.IS_SMIG, C.ID_COMPANY, E.KD_PRODUCT");
		}

		$this->db->join("M_COMPONENT B", "B.ID_COMPONENT = A.ID_COMPONENT");
		$this->db->join("O_COMPETITOR C", "C.ID_COMPETITOR = A.ID_COMPETITOR");
		$this->db->join("O_SAMPLE_AREA D", "D.ID_SAMPLE_AREA = A.ID_SAMPLE_AREA");
		$this->db->join("M_PRODUCT E", "E.ID_PRODUCT = C.TYPE_PRODUCT");

		$this->db->where("TO_CHAR(A.DATE_COLLECT, 'MM-YYYY') = ", $tanggal);

		if ( (int) $typeproduct == 0 ) {
			# code...
	    	$this->db->where("(LOWER(E.KD_PRODUCT) = 'ppc' OR LOWER(E.KD_PRODUCT) = 'pcc')");
		} else {
			# code...
	    	$this->db->where("LOWER(E.KD_PRODUCT) = 'opc'");
		}

		$this->db->where("A.ID_COMPANY", $id_company);

		return $this->db->get("O_SAMPLE_QUALITY A")->result_array();
	}

}
