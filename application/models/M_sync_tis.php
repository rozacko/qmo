<?php
	class M_sync_tis Extends DB_QM {

        public function __construct(){
            parent::__construct();
            // $this->db = $this->load->database('mso_prod', TRUE);
        }

		public function get_company(){
			$this->db->select("ID_COMPANY, KD_COMPANY, TRIM(NM_COMPANY) as NM_COMPANY");
			$this->db->from("M_COMPANY");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_plant(){
			$this->db->select("ID_PLANT, TRIM(KD_PLANT) AS KD_PLANT, NM_PLANT, ID_COMPANY");
			$this->db->from("M_PLANT");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_group_area(){
			$this->db->select("ID_GROUPAREA, TRIM(KD_GROUPAREA) AS KD_GROUPAREA, NM_GROUPAREA");
			$this->db->from("M_GROUPAREA");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_area(){
			$this->db->select("ID_AREA, TRIM(KD_AREA) AS KD_AREA, NM_AREA");
			$this->db->from("M_AREA");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_product(){
			$this->db->select("ID_PRODUCT, TRIM(UPPER(KD_PRODUCT)) AS KD_PRODUCT, NM_PRODUCT");
			$this->db->from("M_PRODUCT");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_component(){
			$this->db->select("ID_COMPONENT, TRIM(UPPER(KD_COMPONENT)) AS KD_COMPONENT, NM_COMPONENT");
			$this->db->from("M_COMPONENT");
			$query = $this->db->get();
			return $query->result_array();
		}

		public function get_data_t_prod_tis($data = array(), $date_data = null){
			$this->db->select("*");
			$this->db->from("T_PRODUCTION_TIS");
			if(isset($data)){
				$this->db->where($data);
			}

			if(isset($date_data)){
				$this->db->where("DATE_DATA = TO_DATE('".date("Y-m-d", strtotime(str_replace(".", "-", $date_data)))."', 'yyyy/mm/dd')");
			}

			$query = $this->db->get();
			if($query->num_rows() > 0){
				return $query->result_array();
			} else {
				return false;
			}
			
		}

		public function add_data_t_prod_tis($data, $date_data){
			$this->db->set("DATE_DATA", "TO_DATE('".date("Y-m-d", strtotime(str_replace(".", "-", $date_data)))."', 'yyyy/mm/dd')", false);
			$this->db->set("DATE_ENTRY", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
			$this->db->insert("T_PRODUCTION_TIS", $data);

			$this->db->select("MAX(ID_PRODUCTION_TIS) AS ID_PRODUCTION_TIS");
			$this->db->from("T_PRODUCTION_TIS");

			$query = $this->db->get();
			return $query->row();
		}

		public function update_data_t_prod_tis($data, $date_data, $id){
			$this->db->set("DATE_DATA", "TO_DATE('".date("Y-m-d", strtotime(str_replace(".", "-", $date_data)))."', 'yyyy/mm/dd')", false);
			$this->db->set("DATE_ENTRY", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
			$this->db->where("ID_PRODUCTION_TIS", $id)->update("T_PRODUCTION_TIS", $data);

			return $id;
		}

		public function add_data_d_prod_tis($data){
			$this->db->insert_batch("D_PRODUCTION_TIS", $data);
			return true;
		}

		public function delete_data_d_prod_tis($id){
			$this->db->where("ID_PRODUCTION_TIS", $id)->delete("D_PRODUCTION_TIS");
			return TRUE;
		}

		public function add_data_sync_tis($data){
			$this->db->select("T_SYNC_TIS_SEQ.NEXTVAL as DATAID",FALSE);
			$ID_SYNC_TIS = $this->db->get("DUAL")->row()->DATAID;

			$this->db->set("ID_SYNC_TIS",$ID_SYNC_TIS);
			$this->db->set("TYPE",$data['TYPE']);
			$this->db->set("USER_SYNC",$data['USER_SYNC']);
			$this->db->set("DATE_SYNC", "TO_DATE('".date("Y-m-d H:i:s")."', 'yyyy/mm/dd hh24:mi:ss')", false);
			$this->db->set("TOTAL_ADD_DATA",$data['TOTAL_ADD_DATA']);
			$this->db->set("TOTAL_UPDATE_DATA",$data['TOTAL_UPDATE_DATA']);

			$this->db->insert("T_SYNC_TIS");
			return $ID_SYNC_TIS;
		}
	}