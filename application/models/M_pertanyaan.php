<?php

class M_pertanyaan Extends DB_QM {
    
	private $post  = array();
	private $table = "M_DTL_FORM_PENGUJIAN";
	private $pKey  = "ID_QUESTION";
	private $column_order = array(NULL, "ID_QUESTION"); //set column for datatable order
    private $column_search = array("QUEST_TEXT"); //set column for datatable search 
    private $order = array("ID_QUESTION" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
    }

    public function get_query(){
		$this->db->select("*");
        $this->db->from($this->table);
        $this->db->where($this->table.".DELETED_AT IS NULL");
	}

	public function detail($ID_QUESTION){
		$this->get_query();
		$this->db->where("ID_QUESTION", $ID_QUESTION);
		$query = $this->db->get();
		return $query->row();
	}

    public function get_list($id_form = "", $tipe = "") {
		$this->get_query();
		if($id_form != ""){
			$this->db->where("ID_FORM", $id_form);
		}

		if($tipe != ""){
			$this->db->where("TIPE", $tipe);
		}
        
		$i = 0;

		//Loop column search
		foreach ($this->column_search as $item) {
			if($this->post['search']['value']){
				if($i===0){ //first loop
					$this->db->group_start(); //open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, strtoupper($this->post['search']['value']));
				}else{
					$this->db->or_like($item, strtoupper($this->post['search']['value']));
				}

				if(count($this->column_search) - 1 == $i){ //last loop
                    $this->db->group_end(); //close bracket
				}
			}
			$i++;
		}

		if(isset($this->post['order'])){ //order datatable
			$this->db->order_by($this->column_order[$this->post['order']['0']['column']], $this->post['order']['0']['dir']);
		}elseif (isset($this->order)) {
			$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		}

		if($this->post['length'] != -1){
			$this->db->limit($this->post['length'],$this->post['start']);
			$query = $this->db->get();
		}else{
			$query = $this->db->get();
		}

		return $query->result();
	}

	public function count_filtered($id_form = "", $tipe = ""){
		$this->get_query();
		if($id_form != ""){
			$this->db->where("ID_FORM", $id_form);
		}

		if($tipe != ""){
			$this->db->where("TIPE", $tipe);
		}
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	public function count_all($id_form = "", $tipe = ""){
		$this->get_query();
		if($id_form != ""){
			$this->db->where("ID_FORM", $id_form);
		}

		if($tipe != ""){
			$this->db->where("TIPE", $tipe);
		}
		$query = $this->db->get();
		return (int) $query->num_rows();
	}
	
	public function insert($data){
        $this->db->set($data);
        $this->db->set("CREATED_AT", "CURRENT_DATE", false);
		$this->db->insert($this->table);
    }
    
    public function update($data, $ID_QUESTION){
        $this->db->set($data);
        $this->db->set("UPDATED_AT", "CURRENT_DATE", false);
        $this->db->where("ID_QUESTION",$ID_QUESTION);
		$this->db->update($this->table);
    }
    
    public function delete($ID_QUESTION){
        $this->db->set("DELETED_AT", "CURRENT_DATE", false);
		$this->db->where("ID_QUESTION",$ID_QUESTION);
		$this->db->update($this->table);
    }
    
    public function get_detail_keyword($ID_KOMODITI, $KEYWORD){
        $this->db->select("*");
        $this->db->from('T_PARAMETER_FORM_PENGUJIAN');
        $this->db->where('ID_KOMODITI', $ID_KOMODITI);
        $this->db->where('PARAMETER', $KEYWORD);
        $data = $this->db->get();
        if($data->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function insert_keyword($data){
        $this->db->set($data);
		$this->db->insert('T_PARAMETER_FORM_PENGUJIAN');
    }

    public function get_detail_form($ID_FORM){
        $this->db->select("*");
        $this->db->from('M_FORM_PENGUJIAN');
        $this->db->where('ID_FORM', $ID_FORM);
        $data = $this->db->get();
        if($data->num_rows() > 0){
            return $data->row();
        } else {
            return false;
        }
    }
}