<?php

class T_production_hourly Extends DB_QM {

	public function datalist(){
		$this->db->order_by("a.ID_PRODUCTION_HOURLY");
		return $this->db->get("T_PRODUCTION_HOURLY a")->result();
	}

	public function search(&$keyword){
		$this->db->like("NM_PRODUCTION_HOURLY",$keyword);
		return $this->db->get("T_PRODUCTION_HOURLY")->result();
	}

	public function data($where){
		$this->db->where($where);
		return  $this->db->get("T_PRODUCTION_HOURLY")->row();
	}


	public function data_where($where){
		$this->db->select("a.*, to_char(DATE_DATA,'DD') AS TANGGAL, x.NM_AREA, y.NM_PLANT");
		$this->db->where($where);
		$this->db->join('M_AREA x', 'x.ID_AREA = a.ID_AREA');
		$this->db->join('M_PLANT y', 'y.ID_PLANT = x.ID_PLANT');
		// $this->db->order_by('a.ID_PRODUCTION_HOURLY', 'asc');
		$this->db->order_by('a.DATE_DATA', 'asc'); // izza
		$this->db->order_by('a.JAM_DATA', 'asc');
		return  $this->db->get('T_PRODUCTION_HOURLY a')->result();
	}

	public function get_id($ID_AREA,$DATE_DATA,$JAM_DATA){
		return $this->exists($ID_AREA,$DATE_DATA,$JAM_DATA)->ID_PRODUCTION_HOURLY;
	}

	public function get_nilai($arr_dt, $arr_ida='', $arr_idc='', $group=null){
		$this->db->select('a.ID_PRODUCTION_DAILY, a.ID_AREA, b.ID_COMPONENT, e.KD_COMPONENT, e.NM_COMPONENT, b.NILAI, a.DATE_DATA');
		// $this->db->select("CONCAT(d.NM_PLANT,CONCAT(' - ',c.KD_AREA)) AREA");
		$this->db->select("CONCAT(d.INISIAL,CONCAT(' <br> ',c.KD_AREA)) AREA, f.ID_COMPANY");
		$this->db->from('T_PRODUCTION_DAILY a');
		$this->db->join('D_PRODUCTION_DAILY b', 'b.ID_PRODUCTION_DAILY=a.ID_PRODUCTION_DAILY', 'left');
		$this->db->join('M_AREA c', 'a.ID_AREA=c.ID_AREA', 'left');
		$this->db->join('M_PLANT d', 'c.ID_PLANT=d.ID_PLANT', 'left');
		$this->db->join('M_COMPANY f', 'f.ID_COMPANY=d.ID_COMPANY', 'left');
		$this->db->join('M_COMPONENT e', 'b.ID_COMPONENT=e.ID_COMPONENT', 'left');
		$this->db->where("to_char(DATE_DATA, 'DD/MM/YYYY') >=", $arr_dt[0]);
		$this->db->where("to_char(DATE_DATA, 'DD/MM/YYYY') <=", $arr_dt[1]);
		$this->db->where_in('a.ID_AREA', $arr_ida);
		$this->db->where_in('b.ID_COMPONENT', ($group==1) ? $arr_idc[1]:$arr_idc);
		// $this->db->order_by('a.ID_PRODUCTION_DAILY', 'asc');
		$this->db->order_by('f.ID_COMPANY', 'asc');
		$this->db->order_by('d.INISIAL, c.KD_AREA', 'asc');
		$data= $this->db->get();
		return $data->result();
		// $this->db->get();
		// echo $this->db->last_query();
	}

	public function get_qtrend($arr_dt, $arr_ida='', $arr_idc='', $periode=''){
		if ($periode=='D') {
			$this->db->select("a.ID_AREA, b.ID_COMPONENT, e.KD_COMPONENT, e.NM_COMPONENT, f.V_MIN, f.V_MAX");
			$this->db->select("ROUND(AVG(b.NILAI)) AS NILAI");
			$this->db->select("CONCAT(d.NM_PLANT, CONCAT(' - ', c.KD_AREA)) AS AREA");
			$this->db->select("a.DATE_DATA AS PERIODE");
			$this->db->from("T_PRODUCTION_HOURLY a");
		  $this->db->join('D_PRODUCTION_HOURLY b', 'b.ID_PRODUCTION_HOURLY=a.ID_PRODUCTION_HOURLY', 'left');
		  $this->db->join('M_AREA c', 'a.ID_AREA=c.ID_AREA', 'left');
		  $this->db->join('M_PLANT d', 'c.ID_PLANT=d.ID_PLANT', 'left');
		  $this->db->join('M_COMPONENT e', 'b.ID_COMPONENT=e.ID_COMPONENT', 'left');
			$this->db->join('C_COMPONENT_RANGE f', 'b.ID_COMPONENT=f.ID_COMPONENT AND f.V_MAX <= 999', 'left');
			$this->db->where("DATE_DATA >=", "TO_DATE('".$arr_dt[0]."','DD/MM/YYYY')", FALSE);
			$this->db->where("DATE_DATA <=", "TO_DATE('".$arr_dt[1]."','DD/MM/YYYY')", FALSE);
			$this->db->where_in('a.ID_AREA', $arr_ida);
			$this->db->where_in('b.ID_COMPONENT', $arr_idc[1]);
			if($group==1) $this->db->where("a.ID_PRODUCT", $arr_idc[0]);
			$this->db->group_by("a.ID_AREA");
			$this->db->group_by("b.ID_COMPONENT");
			$this->db->group_by("e.KD_COMPONENT");
			$this->db->group_by("e.NM_COMPONENT");
			$this->db->group_by("a.DATE_DATA");
			$this->db->group_by("CONCAT( d.NM_PLANT, CONCAT( ' - ', c.KD_AREA ))");
			$this->db->group_by("f.V_MIN");
			$this->db->group_by("f.V_MAX");
			$this->db->order_by("AREA", 'ASC');
			$this->db->order_by("DATE_DATA", 'ASC');

		}elseif ($periode=='H') {
			$dt_start = explode(" ", $arr_dt[0]);
			$d_start  = $dt_start[0];
			$h_start  = ($dt_start[1]=='00') ? '24':$dt_start[1];

			$dt_end  = explode(" ", $arr_dt[1]);
			$d_end 	 = $dt_end[0];
			$h_end 	 = ($dt_end[1]=='00') ? '24':$dt_end[1];

			$this->db->select("a.ID_AREA, b.ID_COMPONENT, e.KD_COMPONENT, e.NM_COMPONENT, f.V_MIN, f.V_MAX");
			$this->db->select("ROUND(AVG(b.NILAI)) AS NILAI");
			$this->db->select("CONCAT(d.NM_PLANT, CONCAT(' - ', c.KD_AREA)) AS AREA");
			$this->db->select("CONCAT(a.JAM_DATA, CONCAT(' - ', TO_CHAR(a.DATE_DATA,'dd/mm/yyyy'))) AS PERIODE");
			$this->db->select("a.JAM_DATA, a.DATE_DATA");
			$this->db->from("T_PRODUCTION_HOURLY a");
		  $this->db->join('D_PRODUCTION_HOURLY b', 'b.ID_PRODUCTION_HOURLY=a.ID_PRODUCTION_HOURLY', 'left');
		  $this->db->join('M_AREA c', 'a.ID_AREA=c.ID_AREA', 'left');
		  $this->db->join('M_PLANT d', 'c.ID_PLANT=d.ID_PLANT', 'left');
		  $this->db->join('M_COMPONENT e', 'b.ID_COMPONENT=e.ID_COMPONENT', 'left');
			$this->db->join('C_COMPONENT_RANGE f', 'b.ID_COMPONENT=f.ID_COMPONENT AND f.V_MAX <= 999', 'left');
			$this->db->where("DATE_DATA >=", "TO_DATE('".$d_start."','DD/MM/YYYY')", FALSE);
			$this->db->where("JAM_DATA >=", $h_start, FALSE);
			$this->db->where("DATE_DATA <=", "TO_DATE('".$d_end."','DD/MM/YYYY')", FALSE);
			$this->db->where("JAM_DATA <=", $h_end, FALSE);
			$this->db->where_in('a.ID_AREA', $arr_ida);
			$this->db->where_in('b.ID_COMPONENT', $arr_idc[1]);
			if($group==1) $this->db->where("a.ID_PRODUCT", $arr_idc[0]);
			$this->db->group_by("a.ID_AREA");
			$this->db->group_by("b.ID_COMPONENT");
			$this->db->group_by("e.KD_COMPONENT");
			$this->db->group_by("e.NM_COMPONENT");
			$this->db->group_by("a.JAM_DATA, a.DATE_DATA");
			$this->db->group_by("CONCAT( d.NM_PLANT, CONCAT( ' - ', c.KD_AREA ))");
			$this->db->group_by("CONCAT(a.JAM_DATA, CONCAT(' - ', a.DATE_DATA))");
			$this->db->group_by("f.V_MIN");
			$this->db->group_by("f.V_MAX");
			$this->db->order_by("AREA", 'ASC');
			$this->db->order_by("a.DATE_DATA", 'ASC');
			$this->db->order_by("a.JAM_DATA", 'ASC');
		}
		else{
			$this->db->select("a.ID_AREA, b.ID_COMPONENT, e.KD_COMPONENT, e.NM_COMPONENT, f.V_MIN, f.V_MAX");
			$this->db->select("ROUND(AVG(b.NILAI)) AS NILAI");
			$this->db->select("a.JAM_DATA AS PERIODE");
			$this->db->select("CONCAT(d.NM_PLANT, CONCAT(' - ', c.KD_AREA)) AS AREA");
			$this->db->from("T_PRODUCTION_HOURLY a");
		  $this->db->join('D_PRODUCTION_HOURLY b', 'b.ID_PRODUCTION_HOURLY=a.ID_PRODUCTION_HOURLY', 'left');
		  $this->db->join('M_AREA c', 'a.ID_AREA=c.ID_AREA', 'left');
		  $this->db->join('M_PLANT d', 'c.ID_PLANT=d.ID_PLANT', 'left');
		  $this->db->join('M_COMPONENT e', 'b.ID_COMPONENT=e.ID_COMPONENT', 'left');
			$this->db->join('C_COMPONENT_RANGE f', 'b.ID_COMPONENT=f.ID_COMPONENT AND f.V_MAX <= 999', 'left');
			$this->db->where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')=", $arr_dt);
			$this->db->where_in('a.ID_AREA', $arr_ida);
			$this->db->where_in('b.ID_COMPONENT', $arr_idc[1]);
			if($group==1) $this->db->where("a.ID_PRODUCT", $arr_idc[0]);
			$this->db->group_by("a.ID_AREA");
			$this->db->group_by("b.ID_COMPONENT");
			$this->db->group_by("e.KD_COMPONENT");
			$this->db->group_by("e.NM_COMPONENT");
			$this->db->group_by("a.JAM_DATA");
			$this->db->group_by("CONCAT( d.NM_PLANT, CONCAT( ' - ', c.KD_AREA ))");
			$this->db->group_by("f.V_MIN");
			$this->db->group_by("f.V_MAX");
			$this->db->order_by("JAM_DATA", 'ASC');
		}

		//echo $this->db->get_compiled_select();exit();
		return $this->db->get()->result();
	}
	public function get_qtrend2($arr_dt, $arr_ida='', $arr_idc='', $periode, $group=null){ // izza 27.07.21
		$tgl_start = $arr_dt[0]['tgl'];
		$jam_start = $arr_dt[0]['jam'];
		$tgl_end = $arr_dt[1]['tgl'];
		$jam_end = $arr_dt[1]['jam'];
		$this->db->select("a.ID_AREA, b.ID_COMPONENT, d.ID_COMPANY, e.KD_COMPONENT, e.NM_COMPONENT");

		$this->db->select("ROUND(b.NILAI,2) AS NILAI");
		// $this->db->select("CONCAT(TO_CHAR(a.DATE_DATA, 'YYYY-MM-DD'),CONCAT(' Jam ',CONCAT(a.JAM_DATA)) AS PERIODE");
		$this->db->select("CONCAT(TO_CHAR(a.DATE_DATA, 'YYYY-MM-DD'),CONCAT(' ',a.JAM_DATA)) AS PERIODE");
		$this->db->select("CASE WHEN LENGTH(a.JAM_DATA)=1 THEN CONCAT(TO_CHAR(a.DATE_DATA, 'YYYY/MM/DD'), CONCAT(' 0',a.JAM_DATA)) ELSE CONCAT(TO_CHAR(a.DATE_DATA, 'YYYY/MM/DD'), CONCAT(' ',a.JAM_DATA))  END AS URUT");
		$this->db->select("TO_DATE( CONCAT(TO_CHAR(a.DATE_DATA, 'YYYY-MM-DD'),CONCAT(' ',CONCAT(a.JAM_DATA-1,CONCAT(':00',':00')))), 'YYYY-MM-DD HH24:MI:SS') AS DATE_TIME_FIX");

		$this->db->select("CONCAT(d.NM_PLANT, CONCAT(' - ', c.KD_AREA)) AS AREA");
		$this->db->from("T_PRODUCTION_HOURLY a");
		$this->db->join('D_PRODUCTION_HOURLY b', 'b.ID_PRODUCTION_HOURLY=a.ID_PRODUCTION_HOURLY', 'left');

		$this->db->join('M_AREA c', 'a.ID_AREA=c.ID_AREA', 'left');
		$this->db->join('M_PLANT d', 'c.ID_PLANT=d.ID_PLANT', 'left');
		$this->db->join('M_COMPONENT e', 'b.ID_COMPONENT=e.ID_COMPONENT', 'left');
		// $this->db->join('C_RANGE_QAF f', 'e.ID_COMPONENT=f.ID_COMPONENT AND c.ID_GROUPAREA=f.ID_GROUPAREA AND d.ID_COMPANY = f.ID_COMPANY AND a.ID_PRODUCT = f.ID_PRODUCT AND f.V_MAX <= 999', 'left');
		
		// $this->db->where("a.DATE_DATA BETWEEN TO_DATE('{$arr_dt[0]}','DD/MM/YYYY') AND TO_DATE('{$arr_dt[1]}','DD/MM/YYYY')");
		$this->db->where("a.DATE_DATA BETWEEN TO_DATE('{$tgl_start}','DD/MM/YYYY') AND TO_DATE('{$tgl_end}','DD/MM/YYYY')");
		// $this->db->where("a.DATE_DATA TO_DATE('{$tgl_start}','DD/MM/YYYY') AND TO_DATE('{$tgl_end}','DD/MM/YYYY')");
		// $this->db->where("a.DATE_DATA", );

		$this->db->where_in('a.ID_AREA', $arr_ida);
		$this->db->where_in('e.ID_COMPONENT', $arr_idc[1]);
		#$this->db->where('d.ID_COMPANY=f.ID_COMPANY');
		#$this->db->where('a.ID_PRODUCT=f.ID_PRODUCT');
		if($group==1) $this->db->where("a.ID_PRODUCT", $arr_idc[0]);
		
		$this->db->order_by("TRIM(AREA), URUT, JAM_DATA", 'ASC');
		//echo $this->db->get_compiled_select();exit();
		// return $this->db->get()->result();
		$this->db->get();
		$query_awal = $this->db->last_query();
		$query_awal = str_replace("\n"," ",$query_awal);

		$query_awal2 = 
			"	SELECT COBA.* 
				FROM ({$query_awal}) COBA 
				WHERE COBA.DATE_TIME_FIX BETWEEN TO_DATE('{$jam_start}','DD/MM/YYYY HH24:MI') AND TO_DATE('{$jam_end}','DD/MM/YYYY HH24:MI')
			";
		return $this->db->query($query_awal2)->result();
	}

	public function exists($ID_AREA,$DATE_DATA,$JAM_DATA){
		$this->db->where("ID_AREA",$ID_AREA);
		$this->db->where("JAM_DATA",$JAM_DATA);
		$this->db->where("TO_CHAR(DATE_DATA,'DD/MM/YYYY')", "'$DATE_DATA'", FALSE);
		return $this->db->get("T_PRODUCTION_HOURLY")->row();
	}

	public function get_data_by_id($ID_PRODUCTION_HOURLY){
		$this->db->where("ID_PRODUCTION_HOURLY",$ID_PRODUCTION_HOURLY);
		return $this->db->get("T_PRODUCTION_HOURLY")->row();
	}

	public function data_except_id($where,$skip_id){
		$this->db->where("ID_PRODUCTION_HOURLY !=",$skip_id);
		$this->db->where($where);
		return $this->db->get("T_PRODUCTION_HOURLY")->row();
	}

	public function insert($data){
		$this->db->select("SEQ_ID_PRODUCTION_HOURLY.NEXTVAL as DATAID",FALSE);
		$ID_PRODUCTION_HOURLY = $this->db->get("DUAL")->row()->DATAID;

		$this->db->set("ID_PRODUCTION_HOURLY",$ID_PRODUCTION_HOURLY);
		$this->db->set("ID_AREA",$data['ID_AREA']);
		$this->db->set("ID_MESIN_STATUS",$data['ID_MESIN_STATUS']);
		$this->db->set("MESIN_REMARK",$data['MESIN_REMARK']);
		$this->db->set("DATE_DATA","to_date('".$data['DATE_DATA']."','dd/mm/yyyy')",FALSE);
		$this->db->set("JAM_DATA",$data['JAM_DATA']);
		$this->db->set("DATE_ENTRY","to_date('".$data['DATE_ENTRY']."','dd/mm/yyyy')",FALSE);
		$this->db->set("JAM_ENTRY",$data['JAM_ENTRY']);
		$this->db->set("USER_ENTRY",$data['USER_ENTRY']);
		$this->db->set("LOCATION",$data['LOCATION']);

		$this->db->insert("T_PRODUCTION_HOURLY");
		return $ID_PRODUCTION_HOURLY;
	}

	public function update($data,$ID_PRODUCTION_HOURLY){
		$this->db->set("ID_MESIN_STATUS",$data['ID_MESIN_STATUS']);
		$this->db->set("MESIN_REMARK",$data['MESIN_REMARK']);
		$this->db->set("DATE_ENTRY","to_date('".$data['DATE_ENTRY']."','dd/mm/yyyy')",FALSE);
		$this->db->set("JAM_ENTRY",$data['JAM_ENTRY']);
		$this->db->set("USER_UPDATE",$data['USER_UPDATE']);

		$this->db->where("ID_PRODUCTION_HOURLY",$ID_PRODUCTION_HOURLY);
		$this->db->update("T_PRODUCTION_HOURLY");
	}

	public function delete($ID_PRODUCTION_HOURLY){
		$this->db->where("ID_PRODUCTION_HOURLY",$ID_PRODUCTION_HOURLY);
		$this->db->delete("T_PRODUCTION_HOURLY");
	}

	public function d_exists($data){
		$this->db->where("ID_PRODUCTION_HOURLY",$data['ID_PRODUCTION_HOURLY']);
		$this->db->where("ID_COMPONENT",$data['ID_COMPONENT']);
		return $this->db->get("D_PRODUCTION_HOURLY")->num_rows();
	}

	public function d_insert($data){
		$this->db->set($data);
		$this->db->insert("D_PRODUCTION_HOURLY");
	}

	public function d_update($data){
		$this->db->set("NILAI",$data['NILAI']);
		$this->db->set("NO_FIELD",$data['NO_FIELD']);
		$this->db->where("ID_PRODUCTION_HOURLY",$data['ID_PRODUCTION_HOURLY']);
		$this->db->where("ID_COMPONENT",$data['ID_COMPONENT']);
		$this->db->update("D_PRODUCTION_HOURLY");
	}

	public function table($TANGGAL,$ID_AREA){
		$this->db->select("to_char(a.DATE_DATA,'FXFMDD') AS DATE_DATA, a.JAM_DATA, b.ID_COMPONENT, b.NILAI, c.NM_MESIN_STATUS, a.MESIN_REMARK");
		$this->db->join("D_PRODUCTION_HOURLY b","a.ID_PRODUCTION_HOURLY=b.ID_PRODUCTION_HOURLY","LEFT");
		$this->db->join("M_MESIN_STATUS c","a.ID_MESIN_STATUS=c.ID_MESIN_STATUS","LEFT");
		$this->db->where("a.ID_AREA",$ID_AREA);
		$this->db->where("to_char(a.DATE_DATA,'DD/MM/YYYY')",$TANGGAL,FALSE);
		$this->db->order_by("a.ID_PRODUCTION_HOURLY","ASC",FALSE);
		$this->db->order_by("b.NO_FIELD","ASC");
		$q = $this->db->get("T_PRODUCTION_HOURLY a");# echo $this->db->last_query();
		return $q->result();
	}
    
    

	public function insert_production($data){ 
      $this->db->insert_batch('D_PRODUCTION_HOURLY', $data);
    }			
}
