<?php

class M_hebat_report extends DB_QM {
	
	public function calculate_qaf_cement($year,$id_company){
		$this->db->query("call QAF_CEMENT_YEARLY(?,?)",array($year,$id_company));
		// echo $this->db->last_query();
		// exit;
	} 

	public function hitung_qaf_daily($param){
		$year = $param['YEAR'];
		$id_company = $param['ID_COMPANY'];

		$id_plant = $param['ID_PLANT'];
		$id_product = $param['ID_PRODUCT'];
		$q = "";
		if($id_plant){
			$q .= " AND b.ID_PLANT IN ({$id_plant}) ";
		}
		if($id_product){ 
			$q .= " AND d.ID_PRODUCT IN ({$id_product}) ";
		}

		$sql = "
			SELECT
				e.NO_MOUNTH AS BULAN,
				{$year} AS TAHUN,
				c.ID_AREA,
				d.ID_GROUPAREA,
				d.ID_PRODUCT,
				b.ID_PLANT
			FROM
				m_company a,
				m_plant b,
				m_area c,
				c_qaf_product d,
				T_MOUNTH e
			WHERE
				a.ID_COMPANY = b.ID_COMPANY
				AND b.ID_PLANT = c.ID_PLANT
				AND d.id_plant = b.id_plant
				AND d.id_grouparea = c.id_grouparea
				AND a.ID_COMPANY = {$id_company} 
				{$q} 
			ORDER BY
				a.ID_COMPANY,
				b.ID_PLANT,
				c.ID_AREA,
				d.ID_PRODUCT,
				e.NO_MOUNTH
		";
		$res = $this->db->query($sql)->result_array();

		// ( r.BULAN,r.TAHUN,r.ID_PRODUCT,r.ID_AREA )
		foreach ($res as $i => $v) {
			$this->db->query("call HITUNG_QAF_DAILY(?,?,?,?)",array($v['BULAN'],$v['TAHUN'],$v['ID_PRODUCT'],$v['ID_PLANT']));
		}
	}
	
	public function calculate_qaf_cs($year,$id_company){
		$this->db->query("call QAF_CS_YEARLY(?,?)",array($year,$id_company));
	}
	
	public function calculate_qaf_st($year,$id_company){
		$this->db->query("call QAF_ST_YEARLY(?,?)",array($year,$id_company));
	}
	
	public function qaf_cement_progress($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, TOTAL_QAF AS NILAI');
		$this->db->from('V_QAF_CEMENT_PROGRESS');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function qaf_cement_progress2($param, $param_in){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, ROUND(AVG(TOTAL_QAF), 2) AS NILAI');
		$this->db->from('V_QAF_CEMENT_PROGRESS2');
		$this->db->where($param);

		foreach ($param_in as $i => $v) {
			$this->db->where_in($i, $v);
		}

		$this->db->order_by('BULAN');
		$this->db->group_by('ID_COMPANY'); 
		$this->db->group_by('TAHUN'); 
		$this->db->group_by('BULAN'); 
		$query = $this->db->get();
		// echo $this->db->last_query();
		return $query->result();
	}


	public function qaf_clinker_progress($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, TOTAL_QAF AS NILAI');
		$this->db->from('V_QAF_CLINKER_PROGRESS');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function qaf_clinker_progress2($param, $param_in){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, ROUND( avg(TOTAL_QAF), 2 ) AS NILAI');
		$this->db->from('V_QAF_CLINKER_PROGRESS2');

		$this->db->where($param);

		foreach ($param_in as $i => $v) {
			$this->db->where_in($i, $v);
		}
		$this->db->order_by('BULAN'); 
		$this->db->group_by('ID_COMPANY');
		$this->db->group_by('BULAN');
		$this->db->group_by('TAHUN');
		$query = $this->db->get();

		// echo $this->db->last_query();exit();
		return $query->result();
	}

	public function qaf_st($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, TOTAL_QAF AS NILAI');
		$this->db->from('V_QAF_ST');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function qaf_cs($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, TOTAL_QAF AS NILAI');
		$this->db->from('V_QAF_CS');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();

		// echo $this->db->last_query();
		// exit;
		return $query->result();
	}

	public function score_input_cement($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, SCORE AS NILAI');
		$this->db->from('V_SCORE_INPUT_CEMENT');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function score_input_production($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, SCORE AS NILAI');
		$this->db->from('V_SCORE_INPUT_PRODUCTION');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function score_ncqr($company, $tahun){
		$this->db->select('ID_COMPANY, TAHUN, BULAN, SCORE AS NILAI');
		$this->db->from('V_SCORE_NCQR');
		$this->db->where('ID_COMPANY', $company);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get();
		return $query->result();
	}

	public function v_skoring_hebat($aspek='ALL', $bulan=6, $tahun=2017){
		if($aspek != 'ALL') $this->db->where('ID_ASPEK', $aspek);
		$this->db->where('BULAN', $bulan);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get('V_SKORING_HEBAT');

		// echo $this->db->last_query();
		return $query->result();
	}
	public function v_skoring_hebat2($aspek='ALL', $bulan=6, $tahun=2017){
		if($aspek != 'ALL') $this->db->where('ID_ASPEK', $aspek);
		$this->db->where('BULAN', $bulan);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get('V_SKORING_HEBAT2');

		// echo $this->db->last_query();
		return $query->result();
	}
	public function v_skoring_hebat1($aspek='ALL', $bulan=6, $tahun=2017){
		if($aspek != 'ALL') $this->db->where('ID_ASPEK', $aspek);
		$this->db->where('BULAN', $bulan);
		$this->db->where('TAHUN', $tahun);
		$this->db->order_by('BULAN');
		$query = $this->db->get('V_SKORING_HEBAT');
		return $query->result();
	}
	public function getDataProduct(){
		$this->db->order_by('NM_PRODUCT');
		$query = $this->db->get('M_PRODUCT');
		return $query->result_array();
	}

	// FASE 2 ------------------------
	
	public function getDataTcementHourly($id_company, $year, $month){

		$sql = "
			SELECT
				COUNT(*) as JML_DATA
			FROM
				T_CEMENT_HOURLY INch
				LEFT JOIN M_AREA INar ON INch.ID_AREA = INar.ID_AREA
				LEFT JOIN M_PLANT INpl ON INar.ID_PLANT = INpl.ID_PLANT
			WHERE
				INpl.ID_COMPANY = {$id_company}
				AND TO_NUMBER( TO_CHAR( INch.DATE_DATA, 'YYYY' )) = {$year}
				AND TO_NUMBER( TO_CHAR( INch.DATE_DATA, 'MM' )) = {$month}
		";
		$query = $this->db->query($sql);
		return $query->row()->JML_DATA;
	}

	public function get_cek_input($id_com, $bulan, $tahun){

		$sql = "
			SELECT
				COUNT(*) as JML_DATA
			FROM
				D_CEMENT_HOURLY
			JOIN T_CEMENT_HOURLY v ON D_CEMENT_HOURLY.ID_CEMENT_HOURLY = v.ID_CEMENT_HOURLY
			JOIN M_AREA x ON x.ID_AREA = v.ID_AREA
			JOIN M_PLANT y ON y.ID_PLANT = x.ID_PLANT
			WHERE
				TO_CHAR (DATE_DATA, 'MM') = '".$bulan."'
				AND TO_CHAR (DATE_DATA, 'YYYY') = '".$tahun."'
				AND y.ID_COMPANY = '".$id_com."'
				AND D_CEMENT_HOURLY.NILAI is NOT NULL
		";
		$query = $this->db->query($sql);
		return $query->row()->JML_DATA;
	}

}

