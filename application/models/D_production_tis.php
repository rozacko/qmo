<?php defined('BASEPATH') OR exit('No direct script access allowed');

class D_production_tis extends DB_QM {

	private $post  = array();
	private $table = "D_PRODUCTION_TIS";
	private $pKey  = "ID_PRODUCTION_TIS";
	private $column_order = array(NULL, 'ID_PRODUCTION_TIS','ID_COMPONENT','NILAI', 'NO_FIELD'); //set column for datatable order
    private $column_search = array('ID_COMPONENT','NILAI', 'ID_PRODUCTION_TIS'); //set column for datatable search 
    private $order = array("NO_FIELD" => 'ASC'); //default order

	public function get_by_id($key){
        $this->db->join("M_COMPONENT", $this->table.".ID_COMPONENT = M_COMPONENT.ID_COMPONENT");
		$this->db->where($this->pKey, $key);
		$this->db->order_by(key($this->order), $this->order[key($this->order)]);
		$query = $this->db->get($this->table);
		return $query->result();
	}

}