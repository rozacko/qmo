<?php

class M_Samplearea Extends DB_QM {
	private $post  = array();
	private $table = "O_SAMPLE_AREA";
	private $pKey  = "ID_SAMPLE_AREA";
	private $column_order = array(NULL, 'URUTAN'); //set column for datatable order
    private $column_search = array('NAME_AREA'); //set column for datatable search 
    private $order = array("ID_SAMPLE_AREA" => 'ASC'); //default order

	public function __construct(){
		$this->post = $this->input->post();
		$this->scmdb = $this->load->database('scm', TRUE);
	}

	var $column = array(
		'ID_SAMPLE_AREA', 'NAME_AREA', 'NAME_PROVINCE', 'NAME_ISLAND', 'NAME_REGION', 'ID_SAMPLE_AREA'
	);

	var $columnscm = array(
		'A.ID_M_KOTA', 'A.KD_KOTA', 'A.NM_KOTA', 'B.KD_PROV', 'B.NM_PROV', 'B.KD_PETA', 'C.KD_PULAU', 'C.NM_PULAU'
	);

	public function is_sample_exists($data){
		if (isset($data['ID_SAMPLE_AREA'])) {
			# code...
		}
		$this->db->where("LOWER(NAME_AREA)", strtolower($data['NAME_AREA']));
		$this->db->where("LOWER(NAME_PROVINCE)", strtolower($data['NAME_PROVINCE']));
		$this->db->where("LOWER(NAME_ISLAND)", strtolower($data['NAME_ISLAND']));
		$this->db->where("LOWER(NAME_REGION)", strtolower($data['NAME_REGION']));
		return $this->db->get("O_SAMPLE_AREA")->num_rows();
	}

	public function sample_insert($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set($data);
		$this->db->set("CREATE_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$query = $this->db->insert("O_SAMPLE_AREA");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_update($data){
		$dtnow = date("Y-m-d H:i:s");
		$this->db->set("NAME_AREA", $data['NAME_AREA']);
		$this->db->set("NAME_PROVINCE", $data['NAME_PROVINCE']);
		$this->db->set("NAME_ISLAND", $data['NAME_ISLAND']);
		$this->db->set("NAME_REGION", $data['NAME_REGION']);
		$this->db->set("MODIFIED_DATE", "to_date('".$dtnow."','YYYY-MM-DD HH24:MI:SS')", FALSE);
		$this->db->set("MODIFIED_BY", $data['MODIFIED_BY']);
		$this->db->where("ID_SAMPLE_AREA", $data['ID_SAMPLE_AREA']);
		$query = $this->db->update("O_SAMPLE_AREA");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	public function sample_delete($data){
		$this->db->where("ID_SAMPLE_AREA", $data['ID_SAMPLE_AREA']);
		$query = $this->db->delete("O_SAMPLE_AREA");
		if($query == true){
			return true;
		}else{
			return false;
		}
	}

	function _qry($key){
		$this->db->from('O_SAMPLE_AREA');
		if($key['search']!==''){
			$this->db->or_like('LOWER(NAME_AREA)', strtolower($key['search']));
			$this->db->or_like('LOWER(NAME_REGION)', strtolower($key['search']));
			$this->db->or_like('LOWER(NAME_ISLAND)', strtolower($key['search']));
			$this->db->or_like('LOWER(NAME_PROVINCE)', strtolower($key['search']));
		}
		$this->db->where('DELETE_FLAG', 0);
		$order = $this->column[$key['ordCol']];
		$this->db->order_by($order, $key['ordDir']);

	}

	function _qry_scm($key){
		$this->scmdb->select('A .ID_M_KOTA, A .KD_AREA, A .KD_KOTA, A .NM_KOTA, B.*, C.KD_PULAU, C.NM_PULAU');
		$this->scmdb->from('ZREPORT_M_KOTA A');
		$this->scmdb->join("ZREPORT_M_PROVINSI B", "B.KD_PROV = A .KD_PROP");
		$this->scmdb->join("M_PULAU C", "C.ID_PULAU = B.ID_PULAU");
		if($key['search']!==''){
			$this->scmdb->or_like('LOWER(A.KD_KOTA)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(A.NM_KOTA)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(B.KD_PROV)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(B.NM_PROV)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(B.KD_PETA)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(C.KD_PULAU)', strtolower($key['search']));
			$this->scmdb->or_like('LOWER(C.NM_PULAU)', strtolower($key['search']));
		}
		$order = $this->columnscm[$key['ordCol']];
		$this->scmdb->order_by($order, $key['ordDir']);

	}

	function get($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();
		return $data;
	}

	function get_data($key){
		$this->_qry($key);
		$this->db->limit($key['length'], $key['start']);
		$query		= $this->db->get();
		$data			= $query->result();

		return $data;
	}

	function recFil($key){
		$this->_qry($key);
		$query			= $this->db->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot(){		
		$query			= $this->db->get('O_SAMPLE_AREA');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function get_data_scm($key){
		$this->_qry_scm($key);
		$this->scmdb->limit($key['length'], $key['start']);
		$query		= $this->scmdb->get();
		$data			= $query->result();
		return $data;
	}

	function recFil_scm($key){
		$this->_qry_scm($key);
		$query			= $this->scmdb->get();
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	function recTot_scm(){		
		$query			= $this->scmdb->get('ZREPORT_M_KOTA');
		$num_rows		= $query->num_rows();
		return $num_rows;
	}

	public function datalist(){
		$this->db->order_by("a.ID_SAMPLE_AREA");
		return $this->db->get("O_SAMPLE_AREA a")->result();
	}

	/** Count query result after filtered **/
	public function count_filtered(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}

	/** Count all result **/
	public function count_all(){
		$this->get_query();
		$query = $this->db->get();
		return (int) $query->num_rows();
	}	

}
