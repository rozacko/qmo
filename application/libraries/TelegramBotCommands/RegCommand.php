<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * Registration command
 */
class RegCommand extends UserCommand
{
    protected $name = 'reg';                      // Your command's name
    protected $description = 'Use to subscribe notification'; // Your command description
    protected $usage = '/reg';                    // Usage of your command
    protected $version = '1.0.0-dev';                  // Version of your command
    protected $private_only = true;

    public function execute() {
        $message = $this->getMessage() ?: $this->getEditedMessage();
        $chat_id = $message->getChat()->getId();
        $identity = trim(substr($message->getText(), strlen($this->usage)));

        $CI =& get_instance();
        $CI->load->model('TelegramNotification');

        // TODO send verification link (with unique token) to email 
        $result = $CI->TelegramNotification->subscribe_by_email($identity, $message);

        $text = $this->handleReturnMessage($result);

        $data = [
            'chat_id'   => $chat_id,
            'text'      => $text
        ];

        return Request::sendMessage($data);
    }

    private function handleReturnMessage($return) {
        if ($return === true) {
            return  <<<TXT
Success

Starting from now you will receive notification message

Thank you.
TXT;
        }

        if (500 === $return) {
            return <<<TXT
Error

Something error happened when subscribing
Please contact administrator.

Thank you.
TXT;
        }

        if (404 === $return) {
            return <<<TXT
Error

Email not found in system.
Please recheck or contact administrator.

Thank you.
TXT;
        }

        return <<<TXT
Error

Unknown error.
TXT;
    }
}
