<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * Registration command
 */
class UnregCommand extends UserCommand
{
    protected $name = 'unreg';                      // Your command's name
    protected $description = 'Use to unsubscribe notification'; // Your command description
    protected $usage = '/unreg';                    // Usage of your command
    protected $version = '1.0.0-dev';                  // Version of your command
    protected $private_only = true;

    public function execute() {
        $message = $this->getMessage() ?: $this->getEditedMessage();
        $chat_id = $message->getChat()->getId();

        $CI =& get_instance();
        $CI->load->model('TelegramNotification');

        // TODO send verification link (with unique token) to email 
        $result = $CI->TelegramNotification->unsubscribe_from_chat($message);

        $text = $this->handleReturnMessage($result);

        $data = [
            'chat_id'   => $chat_id,
            'text'      => $text
        ];

        return Request::sendMessage($data);
    }

    private function handleReturnMessage($return) {
        if ($return === true || 404 === $return) {
            return  <<<TXT
Success

You already unsubscribed from receiving notification message

Thank you.
TXT;
        }

        if (500 === $return) {
            return <<<TXT
Error

Something error happened when unsubscribing
Please contact administrator.

Thank you.
TXT;
        }

        return <<<TXT
Error

Unknown error.
TXT;
    }
}
