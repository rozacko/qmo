<?php
namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Exception\TelegramException;

/**
 * Start command
 */
class StartCommand extends UserCommand
{
    protected $name = 'start';                      // Your command's name
    protected $description = 'A command to say welcome'; // Your command description
    protected $usage = '/start';                    // Usage of your command
    protected $version = '1.0.0-dev';                  // Version of your command
    protected $private_only = true;

    public function execute() {
        $message = $this->getMessage();

        $chat_id = $message->getChat()->getId();

        $data = [
            'chat_id' => $chat_id,
            'text' => sprintf(
                "Welcome\n" .
                "Your Chat ID is %s\n" .
                "Use this to identify this chat, if needed.\n" .
                "Thank you.",
                $chat_id)
        ];


        return Request::sendMessage($data);
    }
}
