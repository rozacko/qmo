<?php defined('BASEPATH') OR exit('No direct script access allowed.');

use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Request as TelegramRequest;
use Longman\TelegramBot\Entities\Update as EntitiesUpdate;
use Longman\TelegramBot\Exception\TelegramException;

class TelegramLib {
    private $apiKey;
    private $username;
    private $webhookToken;
    private $webhookUrl;
    private $telegram;
    private $default_parse_mode = 'Markdown';
    

    public function __construct($options = []) {
        if ($options) {
            $this->setup($options);
        }
    }

    public function setup($options = []) {
        try {
            $this->telegram = new Telegram(
                $options['api_key'],
                $options['username']
            );

            if (isset($options['commands_paths'])) {
                if (! is_array($options['commands_paths'])) {
                    $options['commands_paths'] = (array) $options['commands_paths'];
                }

                $this->telegram->addCommandsPaths($options['commands_paths']);
            }

            $this->apiKey = $options['api_key'];
            $this->webhookUrl = $options['webhook_url'];
            $this->webhookToken = $options['webhook_token'];
            $this->commandPaths = $options['commands_paths'];

            TelegramRequest::initialize($this->telegram); 
        } catch(TelegramException $e) {
            log_message('error', $e->getMessage());
        }
    }

    public function sendMessage($chat_id, $message, $options = []) {
        $options = array_merge(
            [
                'parse_mode' => $this->default_parse_mode,
            ],
            $options
        );

        $params = array_merge(
            [
                'chat_id' => $chat_id,
                'text'    => $message,
            ],
            $options
        );

        try {
            return TelegramRequest::sendMessage($params);

        } catch (TelegramException $e) {
            log_message('error', $e->getMessage());

            return false;
        }
    }

    public function sendPhoto($chat_id, $photo_path, $caption, $options = []) {
        $params = array_merge(
            [
                'chat_id'   => $chat_id,
                'photo'     => TelegramRequest::encodeFile($photo_path),
                'caption'   => $caption ? $caption : NULL,
            ],
            $options
        );

        try {
            return TelegramRequest::sendPhoto($params);

        } catch (TelegramException $e) {
            log_message('error', $e->getMessage());

            return false;
        }
    }

    /**
     * Handle webhook from telegram server.
     * Use setUpdateFilter() instead if you use version >=0.64
     *
     * @param callable|null $callback
     */
    public function handleWebhook($callback = NULL) {
        try {
            if (! $this->telegram->handle()) {
                throw new TelegramException('Something error on processing update');
            }

            if (! empty($callback) && is_callable($callback)) {
                // After handle above, the respons should be OK
                $input = TelegramRequest::getInput();
                $post = json_decode($input, true);

                $callback(
                    new EntitiesUpdate(
                        $post,
                        $this->telegram->getBotUsername()
                    ),
                    $this
                );
            }

        } catch (TelegramException $e) {
            log_message('error', $e->getMessage());

            return false;
        }
    }

    public function getTelegram() {
        return $this->telegram;
    }

    public function setToken($token) {
        $this->webhookToken = $token;
    }

    public function getToken($token) {
        return $this->webhookToken;
    }

    public function setWebhook($url) {
        try {
            $result = $this->telegram->setWebhook($url);

            if ( $result->isOk() ) {
                return true;
            }
        } catch (TelegramException $e) {
            log_message('error', $e->getMessage());
        }

        return false;
    }

    public function set_parse_mode($parse_mode) {
        $this->default_parse_mode = $parse_mode;
    }
}
