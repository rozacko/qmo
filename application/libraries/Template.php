<?php

class Template {

	private $CI;
	private $domain		= "qm.semenindonesia.co.id";
	private $site_descriptin = "";
	public $web_title 	= "";
	public $cat_alias 	= "all";
	public $admin_tool	= false;
	public $kepo_tool	= false;

	public $cat_list;
	public $recent_update_post;



	private $header		= array(); //
	public $meta;
	private $menu;
	private $path = "adminlte/";
	private $LANG;

	public $MENU_LIST;

	public function __construct(){
		$this->menu = new stdClass();
		$this->meta = new stdClass();
		$this->CI =& get_instance();
	}

	public function adminlte($view="index",$data=false){
		if(!$this->web_title){
			$this->web_title = $this->domain." ".$this->site_descriptin;
		}

		$file_header = __FUNCTION__."/".$this->CI->LANGUAGE."/header";
		$file_footer = __FUNCTION__."/".$this->CI->LANGUAGE."/footer";
		$file_view   = __FUNCTION__."/".$this->CI->LANGUAGE."/".$view;

		if(!@file_exists(APPPATH.'views/'.$file_header.'.php')){
		    $file_header = __FUNCTION__."/header";
		}

		if(!@file_exists(APPPATH.'views/'.$file_footer.'.php')){
		    $file_footer = __FUNCTION__."/footer";
		}

		if(!@file_exists(APPPATH.'views/'.$file_view.'.php')){
		    $file_view = __FUNCTION__."/".$view;
		}

		$data['notice'] = $this->CI->notice->get(); #var_dump($data['notice']);
		
		$data['menulist'] = $this->menu();
		
		
		$this->CI->load->view($file_header,$data);
		$this->CI->load->view($file_view,$data);
		$this->CI->load->view($file_footer);

	}

	public function login($view="login"){
		$data['notice'] = $this->CI->notice->get();
	//	$this->CI->load->view($this->path."header-login",$data);
		$this->CI->load->view($this->path.$view,$data);
	//	$this->CI->load->view($this->path."footer");
	}

	public function v_full_behavior($view="v_full_behavior"){
		$data['notice'] = $this->CI->notice->get();
	//	$this->CI->load->view($this->path."header-login",$data);
		$this->CI->load->view($this->path.$view,$data);
	//	$this->CI->load->view($this->path."footer");
	}

	public function nostyle($view,$data=false){
		$this->path = "nostyle/";
		$data['notice'] = $this->CI->notice->get();
		$this->CI->load->view($this->path.$view,$data);
	}

	public function cetak($view,$data=false){
		$this->path = "cetak/";
		$this->CI->load->view($this->path."header-cetak",$data);
		$this->CI->load->view($this->path.$view,$data);
	}

	public function ajax_response($view,$data=false){
		$this->path = "ajax_response/";
		$data['notice'] = $this->CI->notice->get();
		$this->CI->load->view($this->path.$view,$data);
	}

	public function set_meta($name,$content){
		$this->meta->$name = $content;
	}

	public function set_menu($name){
		$this->menu->$name = true;
	}
	
	private function menu(){ 
		//sementara menunggu AD
		$this->CI->load->model("m_menu");
		$r = $this->CI->m_menu->list_active($this->CI->USER->ID_USER); #echo $this->CI->m_menu->get_sql();
		// echo $this->CI->db->last_query();

		$menu = array();
		$paths = array();

		$listmenu = array();
		$no = 0;
		foreach($r as $i => $m){
			if(!$paths[$m->URL_MENU]){

				$paths[$m->URL_MENU] = 1;
				@$menu[$m->NM_GROUPMENU]['ICON'] = $m->ICON;
				@$menu[$m->NM_GROUPMENU]['MENU'][$i]->PATH = $m->URL_MENU;
				// @$menu[$m->NM_GROUPMENU][$i]->ICON = $m->ICON;
				@$menu[$m->NM_GROUPMENU]['MENU'][$i]->NM_MENU = $m->NM_MENU;
				$no += 1;

			} 
		} 

		$listmenu = $this->CI->m_menu->getParent($this->CI->USER->ID_USER);

		$actual_link = str_replace('/DEV/qmonline','', "$_SERVER[REQUEST_URI]");
		$actual_link = str_replace('/PROD/qmonline','', $actual_link);
		foreach($listmenu as $i => $v){
			$subone = $this->CI->m_menu->getSubMenu($this->CI->USER->ID_USER, $v['ID_GROUPMENU']);
			$listmenu[$i]['ACTIVATED'] = 'N';
			$no = 0;
			foreach($subone as $j => $jv){
				if($jv['IS_SUBMENU'] == 'N'){
					$linkMenu = '/'.$jv['URL_MENU'];
					if ($actual_link == $linkMenu) {
						// $this->CI->PERM_READ = $jv['READ'];
						// $this->CI->PERM_WRITE = $jv['WRITE'];

						$status = 'Y';
						$listmenu[$i]['ACTIVATED'] = 'Y'; 
					}else{
						$status = 'N';
					}

					$listmenu[$i]['SUBMENU'][$no]['ID_MENU'] = $jv['ID_MENU']; 
					$listmenu[$i]['SUBMENU'][$no]['PATH'] = base_url($jv['URL_MENU']); 
					$listmenu[$i]['SUBMENU'][$no]['NM_MENU'] = $jv['NM_MENU']; 
					$listmenu[$i]['SUBMENU'][$no]['ICON'] = $jv['ICON']; 
					$listmenu[$i]['SUBMENU'][$no]['ACTIVATED'] = $status; 
					$subtwo = $this->CI->m_menu->getSubMenu($this->CI->USER->ID_USER, $v['ID_GROUPMENU'], $jv['ID_MENU']);
					$no2 = 0;

					if(count($subtwo) > 0){
						foreach($subtwo as $k => $kv){
							if($kv['IS_SUBMENU'] == 'Y'){

								$linkMenu = '/'.$kv['URL_MENU'];
								// if (strpos($actual_link, $linkMenu) !== false) {
								if ($actual_link == $linkMenu) {
									// $this->CI->PERM_READ = $kv['READ'];
									// $this->CI->PERM_WRITE = $kv['WRITE'];
			
									$status = 'Y';
									$listmenu[$i]['ACTIVATED'] = 'Y'; 
									$listmenu[$i]['SUBMENU'][$no]['ACTIVATED'] = 'Y'; 
								}else{
									$status = 'N';
								}
 
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['ID_MENU'] = $kv['ID_MENU'];
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['PARENT'] = $kv['PARENT'];
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['PATH'] = base_url($kv['URL_MENU']);
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['NM_MENU'] = $kv['NM_MENU'];
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['ICON'] = $kv['ICON'];
								$listmenu[$i]['SUBMENU'][$no]['SUBMENU'][$no2]['ACTIVATED'] = $status;
								$no2 += 1;
							}
						}
					}
					$no += 1; 
				}
			}
		}
		// if(!$this->CI->PERM_WRITE){
		// 	//	unset($_POST);
		// 	//	$_POST = NULL;
		// 		if(in_array($this->uri->segment(2),array("add","new","create","auth","deauth","edit","update","del","delete","remove","hapus"))){
		// 			redirect($this->uri->segment(1));
		// 		}
		// 	}

		$paths = null;
		$r =null;
		return $listmenu;
		// return $menu;
	}


}

?>
