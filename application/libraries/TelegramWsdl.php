<?php

class TelegramWsdl {
    private $service_url;
    private $send_action = 'Telegram.sending_massage';
    private $client;
    private $last_error;
    private $last_error_message;

    private $result;

    public function __construct($options = []) {
        if (!class_exists('nusoap_client')) {
            throw new Exception('NuSOAP library (nusoap_client class), i.e. econea/nusoap, is required');
        }

        if ($options) {
            $this->setup($options);
        }
    }

    public function setup($options = []) {
        foreach(['service_url', 'service_type'] as $key) {
            if ( isset($options[$key]) ) {
                $this->$key = $options[$key];
            }
        }

        $this->client = new \nusoap_client($this->service_url, $this->service_type ?: 'wsdl');
    }

    public function sendMessage($phone_number, $message, $options = []) {
        $data = [
            'To' => $phone_number,
            'Message' => rawurlencode($message),
        ];

        try {
            return $this->send($data);
        } catch(Exception $e) {
           log_message('error', $e->getMessage());

           $this->last_error = $e->getCode();
           $this->last_error_message = $e->getMessage();

           return false;
        }
    }

    protected function send($data) {
        $this->reset_error();

        $response = $this->client->call($this->send_action, $data);

        if ( $this->client->fault ) {
            throw new Exception('Fault sending telegram with result ' . json_encode($result));
        }

        if ( $err = $this->client->getError() ) {
            throw new Exception('Error sending telegram with error message ' . $err);
        }

        $result = $this->parse_response($response);

        // if ( $result->ok !== true ) {
        //     throw new Exception('Error in response ' . $result->message);
        // }
        
        return $result;
    }

    private function parse_response($response) {
        $status = false; $message = '';

        // Try decode to JSON. success result returned in JSON format
        $json = json_decode($response);

        $json_error = json_last_error();

        // have success return
        if ( !$json_error && $json ) {
            $status = $json->return;
            $message = $json->msg;
        }
        // example response: 'You'r not Authorized'
        elseif (is_string($response)) {
            $status = false;
            $message = $response;
        }
        else {
            $status = false;
            $message = 'Unknown error';
        }

        $return = new stdClass;
        $return->status = $status;
        $return->message = $message;
        $return->original_response = $response;
        $return->ok = $status; // to be compatible with TelegramLib

        return $return;
    }

    private function reset_error() {
        $this->last_error = '';
        $this->last_error_message = '';
    }

}
