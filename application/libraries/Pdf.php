<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH.'third_party/TCPDF/tcpdf.php';
class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }

    public function Header()
    {
        $image_file = K_PATH_IMAGES.'logo_example.jpg';
        // $this->Image($image_file, 10, 10, 15, '', 'JPG');
        $html = '<strong>FP/SMI/SYM/003</strong><br/>
                 Revisi 0 ';
        $this->writeHTMLCell(
            $w=0,
            $h=0,
            $x=0,
            $y=10,
            $html,
            $border=0,
            $ln=0,
            $fill=false,
            $reseth=true,
            $align='R'
        );
    }
}