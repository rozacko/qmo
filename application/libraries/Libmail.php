<?php

class Libmail {

	public function __construct(){
	}

	public function index(){
		echo "this is mail library";
	}

	public function sendMail($param = array()){
		$to = array();
		$cc = array();

		$to[] = 'm.r.sucahyo2@gmail.com';
		$cc = array('indra.nofiandi@semenindonesia.com', '95irhasmadani95@gmail.com', 'bagushide@gmail.com', 'putri.hardiyanti@sisi.id');

		$this->load->library('email');
		$this->email->from('qmo-noreply@semenindonesia.com', 'QM Online');

		$this->email->to($to);
		$this->email->cc($cc);
		
		$this->email->subject('QMO Approval');
		$this->email->message('Hy');

		if($this->email->send()){
			$result = true;
		}else{
			$result = false;
		}

		return $result;
	}


	#TEMPLATE EMAIL
	//NCQR----------------------------------
	public function ncqr($data){
		$isiemail = "";

		$border 		= 'style="border: 1px solid #000"';
		$alert			= "font-size: 16px; color: #fff; font-weight: bold; padding: 20px; text-align: center; border-radius: 3px 3px 0 0;";
		$body_wrap 		= "background-color: #f6f6f6; width: 100%;";
		$success		= "background: #1ab394;";
		$warning	= "background: #ff5500;";
		$danger		= "background: #b31a1a;";
		$container 		= "display: block !important; max-width: 600px !important; margin: 0 auto !important; /* makes it centered */ clear: both !important;";
		$content 		= "max-width: 600px; margin: 0 auto; display: block; padding: 20px;";
		$content_wrap	= "padding: 20px;";
		$btn_primary 	= "text-decoration: none; color: #FFF; background-color: #1ab394; border: solid #1ab394; border-width: 5px 10px; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize;";
		$label			= "color: #fff; border-radius: 3px; padding: 5px";
		$content_block	= "padding: 0 0 20px;";
		$main 			= "background: #fff; border: 1px solid #e9e9e9; border-radius: 3px;";
		$footer			= "width: 100%; clear: both; color: #999; padding: 20px;";
		
		$message = 'Sorry, your request was not accepted.';
		$alert_color = $warning;


		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		$isiemail .= '
			<table style="'.$body_wrap.'">
				<tr>
					<td></td>
					<td style="'.$container.'" width="600">
						<div style="'.$content.'">
							<table style="'.$main.'" width="100%" cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td style="'.$alert . $alert_color.'">
										Notif: Non Confirmity Quality Report.
									</td>
								</tr>
								<tr>
									<td style="'.$content_wrap.'">';
										$isiemail .= '<table>';
										if($i == 0){

											$isiemail .='<tr>
												<td width="100">Company</td>
												<td>:</td>
												<td width="300">'.$data["INCIDENT"]["NM_COMPANY"].'</td>
											</tr>
											<tr>
												<td>Plant</td>
												<td>:</td>
												<td>'.$data["INCIDENT"]["NM_PLANT"].'</td>
											</tr>
											<tr>
												<td>Area</td>
												<td>:</td>
												<td>'.$data["INCIDENT"]['NM_AREA'].'</td>
											</tr>
											'.($data["INCIDENT"]["ID_PRODUCT"] ? '<tr> <td>Product</td> <td>:</td> <td>'.$data["INCIDENT"]["NM_PRODUCT"].'</td> </tr>' : '');
										}

										$det_incident = "";
										foreach ($data['DETAIL'] as $key => $value) {
											$det_incident .= '
												<tr>
													<td align="center">'.($key+1).'.</td>
													<td align="center">'.$value->NM_COMPONENT.' - ('.$value->KD_COMPONENT.')</td>
													<td align="center">'.$value->JAM_ANALISA.'</td>
													<td align="center">'.$value->ANALISA.'</td>
													<td align="center">'.$value->NILAI_STANDARD_MIN.' - '.$value->NILAI_STANDARD_MAX.'</td>
												</tr>
											';
										}
						$buttonAction = '<a href="'.$data["URL"].'" style="'.$btn_primary.' cursor: pointer">Solve this NCQR.</a>';


										$isiemail .= '
											<tr>
												<td colspan="3" class="'.$content_block.'">
												<br><br>
												</td>
											</tr>
											<tr>
												<td colspan="3" class="'.$content_block.'">
													'.$data['INCIDENT']["SUBJECT"].' - <span style="'.$danger . $label.'">'.$data['INCIDENT']["NM_INCIDENT_TYPE"].'</span><br>
												</td>
											</tr>
										</table><br>
										<table style="width:100%; border-color: #f0f0f0;" border="1" cellpadding="0" cellspacing="0">
											<thead>
											<tr>
												<th>NO</th>
												<th>COMPONENT</th>
												<th>TIME</th>
												<th>ANALIZE</th>
												<th>STANDARD</th>
											</tr>
											</thead>
											<tbody>'.$det_incident.'</tbody>
										</table>
										<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td style="'.$content_block.'"><br>
												</td>
											</tr>
											<tr>
												<td style="'.$content_block.'" align="center">
													'.$buttonAction.'
												</td>
											</tr>
										</table><hr>';

										$isiemail .= '<table width="100%" cellpadding="0" cellspacing="0">
											<tr>
												<td style="'.$content_block.'">
													Please do not reply this email.<br>
													

												</td>
											</tr>
										</table>
										
									</td>
								</tr>
							</table>
							<div style="'.$footer.'">
								<table width="100%">
									<tr>
										<td class="'.$content_block.'"><a href="http://qmo.semenindonesia.com">QMO</a> &copy; Quality Management Online.</td>
									</tr>
								</table>
							</div></div>
					</td>
					<td></td>
				</tr>
			</table>

		';
		
		return $isiemail;
	}

	public function sendMailNative($data){ // izza 24.03.2021
		// $TO         = "izzatulmasrurog@gmail.com";
		$TO 				= implode(", ",$data['TO']);
		$CC 				= implode(", ",$data['CC']);
		$subject    = $data['SUBJECT'];
		$message 		= $this->ncqr($data);

		$separator  = md5(time());
		$eol        = "\r\n";

		//header email
		$headers  = "MIME-Version: 1.0" . $eol;
		$headers .= "Content-type:multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
		$headers .= 'From: QM Online <qmo-noreply@semenindonesia.com>'. $eol;
		$headers .= 'CC: '. $CC . $eol;
		$headers .= "Content-Transfer-Encoding: 7bit" . $eol;
		$headers .= "This is a MIME encoded message." . $eol;

		//body email
		$body  = "--" . $separator . $eol;
		$body .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
		$body .= "Content-Transfer-Encoding: 8bit" . $eol;
		$body .= $message . $eol;

		$send = mail($TO, $subject, $body, $headers);

		return $send;
	}
}
// TO =<br> '.join($data['TO'], "<br> ").'<hr>
// CC =<br> '.join($data['CC'], "<br> ").'

?>

