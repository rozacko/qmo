<?php

/*
1 Administrator
2 User 
3 -
*/

class QMUser extends CI_Controller { 
	public  $USER;
	public $PERM_READ = NULL;
	public $PERM_WRITE = NULL; 
	public $MENU_LIST;
	public $APP = "QM";
	public $AKTIF_ID_GROUPMENU = 9; //dashboard
	public $AKTIF_ID_MENU = 87; //dashboard qm
	public $AKTIF_GROUPMENU = "DASHBOARD"; 
	public $AKTIF_MENU = "DASHBOARD QM"; 
	public $AKTIF_ACTION = 'view'; //default view (datalist
	public $cssExt = array();
	public $jsExt = array();
	
	public function __construct() {
		parent::__construct();
		// echo '<pre>';
		// echo '1231231312';
		// echo '</pre>';
		// exit;
		
		if($this->session->has_userdata("APP")){
			$this->APP = $this->session->has_userdata("APP");
		}		
		$currentUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$currentUrl = explode('/', $currentUrl);
		$getActivation = array_search('activation', $currentUrl);
		$getUpdateActivation = array_search('updateActivation', $currentUrl);
		$getUpdateMenu = array_search('menu', $currentUrl);
		$getUpdateProfile = array_search('profile', $currentUrl);
		$save_table_cement_process_cli = array_search('save_table_cement_process_cli', $currentUrl);
		$save_table_process_cli = array_search('save_table_process_cli', $currentUrl);
		if($getActivation > 0 || $getUpdateActivation > 0 || $getUpdateMenu > 0 || $getUpdateProfile > 0 || $save_table_cement_process_cli > 0 || $save_table_process_cli > 0){

		}else{

			if(!$this->session->has_userdata("USER")){
				?>
				<script type="text/javascript">
					currentLocation = window.location + '';
					currentLocation = currentLocation.split("/");
					getApprove = currentLocation.indexOf("approve");
					if(getApprove > 0){
						localStorage.setItem("last_url", window.location);
					}
					window.location.href = "<?=base_url('login')?>";
				</script>
				<?php
				// redirect("login");
				die();
			}

			$data_user = $this->session->userdata("USER"); 
			unset($data_user->USERPASS);
			$this->USER = $data_user;
			unset($data_user);	
			// echo "string";
			//user authorized
			if($this->uri->segment(1) and $this->uri->segment(1) != "login"){
				$this->load->model("m_authority");
				$this->load->model("m_menu");
				// echo $this->uri->segment(1);
				$permition = $this->m_authority->permition($this->uri->segment(1),$this->USER->ID_USER); #echo $this->m_authority->get_sql();ec
				if($permition){ 
					// $this->PERM_READ = $permition->PERM_READ;
					// $this->PERM_WRITE = $permition->PERM_WRITE;
					$this->AKTIF_GROUPMENU = $permition->NM_GROUPMENU;
					$this->AKTIF_MENU = $permition->NM_MENU;
					$this->AKTIF_ID_GROUPMENU = $permition->ID_GROUPMENU;
					$this->AKTIF_ID_MENU = $permition->ID_MENU;
					$this->AKTIF_ACTION = $this->uri->segment(2);

					
					$listmenu = $this->m_menu->getParent($this->USER->ID_USER);

					$actual_link = str_replace('/DEV/qmonline','', "$_SERVER[REQUEST_URI]");
					$actual_link = str_replace('/PROD/qmonline','', $actual_link);
					foreach($listmenu as $i => $v){
						$subone = $this->m_menu->getSubMenu($this->USER->ID_USER, $v['ID_GROUPMENU']);
						$no = 0;
						foreach($subone as $j => $jv){
							if($jv['IS_SUBMENU'] == 'N'){
								$linkMenu = '/'.$jv['URL_MENU'];
								// if ($actual_link == $linkMenu) {
								if (strpos($actual_link, $linkMenu) !== false) {
									$this->PERM_READ = $jv['READ'];
									$this->PERM_WRITE = $jv['WRITE'];
								}

								$subtwo = $this->m_menu->getSubMenu($this->USER->ID_USER, $v['ID_GROUPMENU'], $jv['ID_MENU']);
								$no2 = 0;
								if(count($subtwo) > 0){
									foreach($subtwo as $k => $kv){
										if($kv['IS_SUBMENU'] == 'Y'){
											$linkMenu = '/'.$kv['URL_MENU'];
											// if ($actual_link == $linkMenu) {
											if (strpos($actual_link, $linkMenu) !== false) {
												$this->PERM_READ = $kv['READ'];
												$this->PERM_WRITE = $kv['WRITE'];
											}
											$no2 += 1;
										}
									}
								}
								$no += 1;
							}
						}
					}
				}
				else{
					redirect();
				}
				// echo $this->PERM_WRITE;
				if(!$this->PERM_WRITE){
					if(in_array($this->uri->segment(2),array("write","dewrite","add","new","create","auth","deauth","edit","update","del","delete","remove","hapus"))){
						redirect($this->uri->segment(1));
					}
				}
			}
			$this->libExternal();
		}

    }

    public function __destruct(){
		foreach($this->db->queries as $q){
			$this->load->helper("file");
			#write_file(APPPATH  . "/logs/sql.log", $q."\n=================================================\n\n", 'a+');
		}
		
		//akses log
		if($this->USER->ID_USER){
			$this->load->model("m_access_log");
			$data[ID_USER] 		= $this->USER->ID_USER;
			$data[ID_GROUPMENU] = $this->AKTIF_ID_GROUPMENU;
			$data[ID_MENU] 		= $this->AKTIF_ID_MENU;
			$data[ACTION] 		= $this->ACTIVE_ACTION;
			$data[URL] 			= current_url();
			$data[IP_ADDRESS]	= $_SERVER['REMOTE_ADDR'];
			$this->m_access_log->insert($data);
			#var_dump($data);
		}
	}


	// Library external link -----------------------------
	public function libExternal($lib = null){
		$this->jsExt[] = 'js/support.js';
		$this->cssExt[] = 'plugins/pnotify/pnotify.custom.min.css';
		$this->jsExt[] = 'plugins/pnotify/pnotify.custom.min.js';

	    $filter = explode(',', $lib);
	    
	    foreach($filter as $v){
		
			if($v == 'dataTables'){
				$this->cssExt[] = 'plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css';
				$this->jsExt[] = 'plugins/datatables/datatables.net/js/jquery.dataTables.min.js';
				$this->jsExt[] = 'plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js';
			}
			if($v == 'datepicker'){
				$this->cssExt[] = 'plugins/datepicker/datepicker3.css';
				$this->jsExt[] = 'plugins/datepicker/bootstrap-datepicker.js';
			}
			if($v == 'select2'){
				$this->cssExt[] = 'plugins/select2/select2.min.css';
				$this->cssExt[] = 'plugins/select2/select2.full.min.css';
				$this->jsExt[] = 'plugins/select2/select2.min.js';
				$this->jsExt[] = 'plugins/select2/select2.full.min.js';
			}
			if($v == 'highcharts'){
				$this->cssExt[] = 'plugins/highcharts/modules/export_data.css';
				$this->jsExt[] = 'plugins/highcharts/highcharts.js';
				$this->jsExt[] = 'plugins/highcharts/highcharts-3d.js';
				$this->jsExt[] = 'plugins/highcharts/modules/exporting.js';
			}
			if($v == 'momentjs'){
				$this->jsExt[] = 'plugins/handsontable/moment/moment.js';
			}
			if($v == 'iconpicker'){
				$this->cssExt[] = 'css/iconpicker-3.0.0/fontawesome-iconpicker.css';
				$this->jsExt[] = 'js/iconpicker-3.0.0/fontawesome-iconpicker.js';
			}
		}
	}

	public function response($data = array()){
		echo json_encode($data);
	}
    public function getViewer(){
		$this->load->model("m_roles");
		$ses = $this->session->userdata('USER');
		$id_user = $ses->ID_USER;
		$roles = $this->m_roles->get_data_role_by_iduser($id_user);

		$result = false;
		foreach ($roles as $key => $value) {
			if($value->ID_USERGROUP == 4){
				$result = true;
			}
		}

		return $result;
    }
 
    public function getAccess($type = null){
		$this->load->model("m_roles");
		$ses = $this->session->userdata('USER');
		$id_user = $ses->ID_USER;
		$roles = $this->m_roles->get_data_role_by_iduser($id_user);

		$result = false;
		foreach ($roles as $key => $value) {
			switch ($type) {
			    case 'ADM':
			        if($value->ID_USERGROUP == 1){
						$result = true;
					}
			        break;
			    case 'viewer':
			        if($value->ID_USERGROUP == 4){
						$result = true;
					}
			        break;
			    case 'adminuser':
			        if($value->ID_USERGROUP == 162){
						$result = true;
					}
			        break;
			    case 'RECEIVER-ACT':
			        if($value->ID_USERGROUP == 5){
						$result = true;
					}
			        break;
			    case 'NCQR-TEKNISI':
			        if($value->ID_USERGROUP == 5){
						$result = true;
					}
			        break;
			    case 'QC-VERIF':
			        if($value->ID_USERGROUP == 164){
						$result = true;
					}
			        break;
			    default:
			        $return = false;
			}

		}

		return $result;
    } 

    public function serverHost(){
		// return 'PROD';
		return 'DEV';
    }
}


class Home extends CI_Controller {
	public $USER;
	public $LANGUAGE = "en";

	public function __construct() {
        parent::__construct();
		$this->libExternal();

    }
    
    public function __destruct(){
		//akses log	
		$this->load->model("m_access_log");
		$data['ID_USER'] 		= $this->USER['ID_USER'];
		$data['ID_GROUPMENU'] = 9;//$this->AKTIF_ID_GROUPMENU;
		$data['ID_MENU'] 		= 87;//$this->AKTIF_ID_MENU;
		$data['ACTION'] 		= (uri_string() == "login")?"login":$this->ACTIVE_ACTION;
		$data['URL'] 			= current_url();
		$data['IP_ADDRESS']	= $_SERVER['REMOTE_ADDR'];
		$this->m_access_log->insert($data);
		#var_dump($data);
	}
  
    public function getViewer(){
		$this->load->model("m_roles");
		$ses = $this->session->userdata('USER');
		$id_user = $ses->ID_USER;
		$roles = $this->m_roles->get_data_role_by_iduser($id_user);

		$result = false;
		foreach ($roles as $key => $value) {
			if($value->ID_USERGROUP == 4){
				$result = true;
			}
		}

		return $result;
    }

	public function response($data = array()){
		echo json_encode($data);
	}
	// Library external link -----------------------------
	public function libExternal($lib = null){
		$this->jsExt[] = 'js/support.js';
		$this->cssExt[] = 'plugins/pnotify/pnotify.custom.min.css';
		$this->jsExt[] = 'plugins/pnotify/pnotify.custom.min.js';

	    $filter = explode(',', $lib);
	    
	    foreach($filter as $v){
			if($v == 'select2'){
				$this->cssExt[] = 'plugins/select2/select2.min.css';
				$this->cssExt[] = 'plugins/select2/select2.full.min.css';
				$this->jsExt[] = 'plugins/select2/select2.min.js';
				$this->jsExt[] = 'plugins/select2/select2.full.min.js';
			}
		} 
	}
    public function serverHost(){
		// return 'PROD';
		return 'DEV';
    }


}

