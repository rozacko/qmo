<?php

class Groupmenu extends QMUser {
	
	public $list_data = array();
	public $data_groupmenu;
	 
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_groupmenu");
		$this->load->model("m_menu");
	}
	
	public function index(){
		$this->libExternal('iconpicker,select2');
		$this->list_groupmenu = $this->m_groupmenu->datalist();
		$this->template->adminlte("v_groupmenu");
	}
	
	public function add(){
		$this->template->adminlte("v_groupmenu_add");
	}
	
	public function create(){
		$this->m_groupmenu->insert($this->input->post());
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
			redirect("groupmenu/add");
		}
		else{
			$this->notice->success("Group Menu Data Saved.");
			redirect("groupmenu");
		}
	}
	public function create_v2(){
		$this->m_groupmenu->insert($this->input->post());
		if($this->m_groupmenu->error()){
			$status = false;
			$msg = $this->m_groupmenu->error();
		}
		else{
			$status = true;
			$msg = 'Group Menu Data Saved.';
		}

		$output = array(
			'status' => $status,
			'msg' => $msg
		);

		to_json($output);
	}
	
	public function edit($ID_GROUPMENU){
		$this->data_groupmenu = $this->m_groupmenu->get_data_by_id($ID_GROUPMENU);
		$this->template->adminlte("v_groupmenu_edit");
	}
	
	public function update($ID_GROUPMENU){
		$this->m_groupmenu->update($this->input->post(),$ID_GROUPMENU);
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
			redirect("groupmenu/edit/".$ID_GROUPMENU);
		}
		else{
			$this->notice->success("Group Menu Data Updated.");
			redirect("groupmenu");
		}
	}

	public function update_v2($ID_GROUPMENU){
		$this->m_groupmenu->update($this->input->post(),$ID_GROUPMENU);
		if($this->m_groupmenu->error()){
			$status = false;
			$msg = $this->m_groupmenu->error();
		}
		else{
			$status = true;
			$msg = 'Group Menu Data Saved.';
		}

		$output = array(
			'status' => $status, 
			'msg' => $msg
		);

		to_json($output);
	}

	
	public function delete($ID_GROUPMENU){
		$this->m_groupmenu->delete($ID_GROUPMENU); 
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
		}
		else{
			$this->notice->success("Group Menu Data Removed.");
		}
		redirect("groupmenu");
	}

	public function delete_v2($ID_GROUPMENU){
		$this->m_groupmenu->delete($ID_GROUPMENU); 
		if($this->m_groupmenu->error()){
			$status = false;
			$msg = $this->m_groupmenu->error();
		}
		else{
			$status = true;
			$msg = 'Group Menu Data Deleted.';
		}

		$output = array(
			'status' => $status,
			'msg' => $msg
		);

		to_json($output);
	}

	public function get_list(){
		$list = $this->m_groupmenu->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_GROUPMENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NO_ORDER;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_groupmenu->count_all(),
            "recordsFiltered" => $this->m_groupmenu->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}
	
	public function get_detlist($id_groupmenu){
		$list = $this->m_menu->get_list($id_groupmenu);
		// print_r($list);
		// echo $this->db->last_query();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_MENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NM_MENU;
			$row[] = $column->URL_MENU;
			$row[] = $column->ID_GROUPMENU;
			$data[] = $row;	
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_menu->count_all($id_groupmenu),
            "recordsFiltered" => $this->m_menu->count_filtered($id_groupmenu),
            "data" => $data,
		);
		
		// return $output;
		to_json($output);
	}

	public function get_detListParent($id_groupmenu){
		$list = $this->m_menu->get_list(null, $id_groupmenu);
		// echo $this->db->last_query();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_MENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NM_MENU;
			$row[] = $column->URL_MENU;
			$row[] = $column->ID_GROUPMENU;
			$data[] = $row;	
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_menu->count_all(null, $id_groupmenu),
            "recordsFiltered" => $this->m_menu->count_filtered(null, $id_groupmenu),
            "data" => $data,
        );

		// return $output;
		to_json($output);
	}

	public function getDetailMenu($id_groupmenu){
		// $this->listdata = $this->get_detlist($id_groupmenu);
		$this->id_groupmenu = $id_groupmenu;
		
		// var_dump($this->listdata);

		$this->template->ajax_response("v_submenu");
	}

}	

?>
