<?php
    $judul = $header["colHeader"];
    $id_comp = $header["id_comp"];

    // header("Content-type: application/vnd.ms-excel; name='excel'");
    // header("Content-Disposition: attachment; filename=\"".$this->NM_AREA." DAILY ".$this->input->post("MONTH")."-".$this->input->post("YEAR").".xls\"");
    // header("Pragma: no-cache");
    // header("Expires: 0");
    $tab = '<table border=1 id="table_export" style="border: none;">';
    $tab .= "<tr><th colspan='16'  style='border: none;'><h1>DAILY ".$tgl["START"]."-".$tgl["END"]."</h1></th></tr>";
    // --------------- ISI  
    

    // ini untuk judul
    $tab .= "<tr>";
    foreach ($judul as $element) {
      $tab .= "<th>".$element."</th>"; 
    }
    $tab .= "</tr>";



    // INI UNTUK DATA
    if(count($data) == 0){
      $tab .= "<tr><td colspan=".count($judul)."><i>(Empty Data)</i></td></tr>";
    } else{
      for($i=0; $i<count($data) ; $i++){
        $data_row = $data[$i];
        $tab .= "<tr>";
        foreach ($judul as $element) {
          $tab .= "<td>".$data_row[$element]."</td>"; 
        }
        $tab .= "</tr>";
      }
    }

    // INI UNTUK FOOTER
    for($i=0; $i<count($footer) ; $i++){
      $data_row = $footer[$i];
      $tab .= "<tr>";
      foreach ($judul as $element) {
        $tab .= "<td>".$data_row[$element]."</td>"; 
      }
      $tab .= "</tr>";
    }


    // ----------------
    $tab .= "</table>";
    echo $tab;
?>

<script src="<?php echo base_url("templates/adminlte/plugins/jQuery/jquery-2.2.3.min.js");?>"></script>


<script>

    $(document).ready(function(){
        window.onload = function(e){
            var  tableID = 'table_export';
            var filename= "<?php echo "DAILY ".$tgl["START"]."-".$tgl["END"] ?>";
            // var filename= "testing";
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
            
            filename = filename ? filename+'.xls':'excel_data.xls';
            downloadLink = document.createElement("a");
            document.body.appendChild(downloadLink);
            
            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            
                downloadLink.download = filename;
                
                downloadLink.click();
            }
            window.close();
        }
    });
</script>


