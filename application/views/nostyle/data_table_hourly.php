<?php
// header("Content-type: application/vnd.ms-excel; name='excel'");
// header("Content-Disposition: attachment; filename=\"".$this->NM_AREA." HOURLY ".$this->input->post("TANGGAL").".xls\"");
// header("Pragma: no-cache");
// header("Expires: 0");

$data = json_decode($this->data); 


$tab = '<table border=1 id="table_export" style="border: none;">';
$tab .= "<tr><th colspan='16'  style='border: none;'><h1>".$this->NM_AREA." HOURLY ".$this->input->post("TANGGAL")."</h1></th></tr>";
$tab .= '<tr>';
// $tab .= '<th>Date</th>';
// $tab .= '<th>Time</th>';
$jth = 1;

#var_dump($data->th); 

#var_dump($data->tr);

foreach($data->header->colHeader as $r){
	$tab .= '<th>'.$r->title.'</th>';
	$jth++;
}
$tab .= '</tr>';
$jth .= 2;

$ltr = 0;



foreach($data->data as $i => $r){
    $i+=1;
	// $tab .= "<tr><td>".$this->input->post("TANGGAL")."</td><td>".$i."</td>";
	
	foreach($r as $x => $y){
        
        foreach($data->header->colHeader as $u => $val){
             if($x==$u){
                $tab .= "<td class=". $val->title.">".$y."</td>";
            }
        }
	}

	$tab .= "</tr>";
	$ltr = 1;
	
}

if($ltr == 0){
	$tab .= "<tr><td colspan="+$jth+"><i>(Empty Data)</i></td></tr>";
}

// Min
$no=0;
$tab .= '<tr>';
$tab .= '<th>Min</th>';
$tab .= '<th>-</th>';
foreach($data->header->colHeader as $r){
    if($no!=0&&$no!=1)
    $tab .= '<th id="min'.$r->title.'"></th>';
    $no+=1;
}
$tab .= '</tr>';
// Max
$no=0;
$tab .= '<tr>';
$tab .= '<th>Max</th>';
$tab .= '<th>-</th>';
foreach($data->header->colHeader as $r){
    if($no!=0&&$no!=1)
    $tab .= '<th id="max'.$r->title.'"></th>';
    $no+=1;
}
$tab .= '</tr>';


// Average
$no=0;
$tab .= '<tr>';
$tab .= '<th>Average</th>';
$tab .= '<th>-</th>';
foreach($data->header->colHeader as $r){
    if($no!=0&&$no!=1)
    $tab .= '<th id="avg'.$r->title.'"></th>';
    $no+=1;
}
$tab .= '</tr>';

// Deviasi
$no=0;
$tab .= '<tr>';
$tab .= '<th>Deviasi</th>';
$tab .= '<th>-</th>';
foreach($data->header->colHeader as $r){
    if($no!=0&&$no!=1)
    $tab .= '<th id="dev'.$r->title.'"></th>';
    $no+=1;
}
$tab .= '</tr>';


$tab .= '</table>';

// echo "<h1>".$this->NM_AREA." HOURLY ".$this->input->post("TANGGAL")."</h1>";
echo $tab;
?>

<script src="<?php echo base_url("templates/adminlte/plugins/jQuery/jquery-2.2.3.min.js");?>"></script>


<script>

$(document).ready(function(){
var data = [<?php echo json_encode($data->header->colHeader);?>];

console.log(data[0]);

    		data[0].forEach(function(item,index){
                
                var sum = 0;
                var max = '';
                var min = 1000;
                var bagi = 0;
                var average = 0;
                var urut = 0;
                // if(index!=0){
                    
                    $("."+item.title).each(function() {
                        urut +=1;
                        //total
                        var value = parseFloat($(this).text());
                        console.log(value)
                        if(!isNaN(value) && value.length != 0 && value !='' && typeof value != "string") {
                            sum += value;
                            bagi +=1;
                        }
                         average = sum/bagi;
                         if(isNaN(average)){average=0}
                         // console.log(average)
                         
                         
                        
                        //max min
                       $this = parseFloat($(this).text());
                       if(!isNaN($this) && $this !='' && typeof $this != "string") {
                           if ($this > max) max = $this;
                           if ($this < min) min = $this;
                        }
                       
                        // console.log($this)
                        
                    });
                        if(min==1000){
                            min='';
                        }
                        if(average==0){
                            $("#avg"+item.title).html('')
                        }else{
                            $("#avg"+item.title).html(average.toFixed(2))
                        }
                        $("#max"+item.title).html(max)
                        $("#min"+item.title).html(min)
                // }
    		}); 
            
                ///DEVIASI
                 
    		data[0].forEach(function(item,index){
                var dev =0;
                var deviasi =0;
                var tot = 0;
                // if(index!=0){
                    var average = $("#avg"+item.title).text();
                $("."+item.title).each(function() {    
                    var value = parseFloat($(this).text());
                    if(!isNaN(value) && value.length != 0 && value !='' && typeof value != "string") {
                        tot += 1;
                        deviasi = value-average;
                        // console.log(deviasi)
                        deviasi = deviasi*deviasi;
                        dev += deviasi;
                    }
                    
                    
                    // $("#dev"+item.title).html(dev.toFixed(3))
                    
                });
                console.log(tot)
                if(dev==0){
                    $("#dev"+item.title).html('')
                }else{
                    var standart = Math.sqrt(dev/(tot-1));
                    // $("#dev"+item.title).html(dev.toPrecision(3))
                    $("#dev"+item.title).html(standart.toFixed(2))
                }
                // }
             });
             
             
             
             
            // var filename= "<?php echo $this->NM_AREA." HOURLY ".$this->input->post("TANGGAL") ?>";
    window.onload = function(e)
            {
            var  tableID = 'table_export';
            var filename= "<?php echo " HOURLY ".$this->input->post("TANGGAL") ?>";
            var downloadLink;
            var dataType = 'application/vnd.ms-excel';
            var tableSelect = document.getElementById(tableID);
            var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
            
            filename = filename?filename+'.xls':'excel_data.xls';
            
            downloadLink = document.createElement("a");
            
            document.body.appendChild(downloadLink);
            
            if(navigator.msSaveOrOpenBlob){
                var blob = new Blob(['\ufeff', tableHTML], {
                    type: dataType
                });
                navigator.msSaveOrOpenBlob( blob, filename);
            }else{
                downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
            
                downloadLink.download = filename;
                
                downloadLink.click();
            }
            window.close();
    }
});

</script>


