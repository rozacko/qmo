
<?php
// var_dump($this->list_grouparea);
?>
<div class="table-responsive">
	<table border=1 class="trep table">
	<tr>
		<th rowspan="3"  valign="center" style="text-align: center;top: 25px;position: sticky;" width="200">PLANT</th>
		<?php 
		$style = "top: 20px;position: sticky;";
		foreach($this->list_grouparea as $r): ?>
			<?php
			// $rowspan = 'rowspan="2"';
			if(trim($r->KD_GROUPAREA) == 'FC'){
				echo '<th colspan="6" STYLE="text-align: center"><b>COAL MILL</b></th>';
			}else if(trim($r->KD_GROUPAREA) == 'RC'){
			}else{
				echo '<th colspan="3" valign="center" rowspan="2" style="text-align: center;"><b>'.$r->NM_GROUPAREA.'</b></th>';
			}
			?>
		<?php endforeach; ?>	
	</tr>
	<tr>
		<?php 
		foreach($this->list_grouparea as $r): ?>
			<?php
			if(trim($r->KD_GROUPAREA) == 'FC' || trim($r->KD_GROUPAREA) == 'RC'){
				echo '<th colspan="3" style="text-align: center;position: sticky;"><b>'.$r->NM_GROUPAREA.'</b></th>';
			}
			?>
		<?php endforeach; ?>	
	</tr>

	<TR>
		<?php foreach($this->list_grouparea as $r): ?>	
		<th style="text-align: center" ><i class="fa fa-info-circle"></i></th>
		<th style="text-align: center" >DATA</th>
		<th style="text-align: center" >ENTRI</th>	
		<?php endforeach; ?>	
	</TR>

	<?php 

	// var_dump($this->list_data);

	foreach($this->list_data as $NM_PLANT => $r): ?>
	<TR>
		<td nowrap=""><?php echo $NM_PLANT?></td>
			<?php foreach($this->list_grouparea as $g): ?>
				<td STYLE="text-align: center;" class="<?php echo $r[$g->ID_GROUPAREA][STATUS] ?>"><?php echo $r[$g->ID_GROUPAREA][STATUS]; 
				// ($r[$g->ID_GROUPAREA][STATUS] == 'NC' ? 'N' : ($r[$g->ID_GROUPAREA][STATUS] == 'NY' ? 'Y' : 'D')) 
				?></td>
				<td STYLE="text-align: center"><?php echo ($r[$g->ID_GROUPAREA][TANGGAL_DATA] == '-' ? '-' : date('d-m-Y', strtotime($r[$g->ID_GROUPAREA][TANGGAL_DATA])))?></td>
				<td STYLE="text-align: center"><?php echo ($r[$g->ID_GROUPAREA][TANGGAL_ENTRI] == '-' ? '-' : date('d-m-Y', strtotime($r[$g->ID_GROUPAREA][TANGGAL_ENTRI])))?></td>
			<?php endforeach; ?>
	</TR>
	<?php endforeach; ?>

	</table>
</div>

<table border=0 class=tlegend>
<tr><td style="font-weight: bold; text-align: center; vertical-align: middle;" class="D">D</td><td>DONE</td></tr>
<tr><td style="font-weight: bold; text-align: center; vertical-align: middle;" class="NC">NC</td><td>NOT COMPLETED</td></tr>
<tr><td style="font-weight: bold; text-align: center; vertical-align: middle;" class="NP">NP</td><td>NOT PRODUCTION</td></tr>
</table>

<style>
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    border-color: antiquewhite;
}
.table {
    border-color: antiquewhite;

}
.NP {
	background: #f27070;
	color: #fff
}

.D {
	background: #96da88;
	/*color: #0035B0;*/
}

.NC {
	background: yellow;
}

.tlegend td {
	padding: 5px;
}

.tlegend {
/*	border-collapse: separate;
    border-spacing: 2px;
*/}
</style>

