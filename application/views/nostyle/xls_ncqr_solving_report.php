<?php
// header("Content-type: application/pdf; name='excel'");
// header("Content-Disposition: attachment; filename=\"ncqr_report.pdf\"");
// header("Pragma: no-cache");
// header("Expires: 0");

// $data = json_decode($this->data);
// echo "string";
// print_r($INCIDENT);
// print_r($M);
?>

<style type="text/css">
	div {
		border: 1px solid black;
	}
	.container {
		display: grid;
		grid-template-columns: repeat(4, 1fr)   ;
		grid-template-rows: 30px 100px 100px 100px 30px;
		grid-gap: 4px;
	}
	table {
		
	}

	/*tr td {
		border: 1px solid black;
	}*/
	label {
		font-weight: bold;
	}

</style>
<table id="main" border="1" cellpadding="5"> 
	<tr >
		<td colspan="2" align="center"><h3>TINDAKAN KOREKSI DAN PENCEGAHAN</h3></td>
	</tr>
	<tr> 
		<td height="20">No. TKP : <label><?= substr('000'.$DATA->ID_ACTION, -3)."/TKP/".$M[TXT_COMPANY]."/".date('m').".".date('Y') ?></label></td> 
		<td>Tanggal Ketidaksesuaian : <label><?= date('d F Y', strtotime($DATA->CREATE_DATE)) ?></label></td> 
	</tr> 
	<tr> 
		<td colspan="2">
			Kategori Audit
			<input type="checkbox" checked> INTERNAL
			<!-- <input type="checkbox" > EXTERNAL -->
		</td> 
	</tr> 
	<tr> 
		<td>Unit Kerja : <label><?= $DATA->UNIT_KERJA ?></label></td> 
		<td>Area Ketidaksesuaian : <label><?= $DATA->AREA ?></label></td> 
	</tr> 
	<tr> 
		<td>Jenis TKP : <label>QUALITY</label></td> 
		<td>Type TKP : <label><?= $M[NM_INCIDENT_TYPE] ?></label></td> 
	</tr> 
	<tr> 
		<td>Acuan : <label><?= $DATA->ACUAN ?></label></td> 
		<td>Klausul : <label><?= $DATA->KLAUSUL ?></label></td> 
	</tr> 
	<tr> 
		<td colspan="2">Rincian Ketidaksesuaian : 
			<p>
			<label>
				<table border="1" cellpadding="5">
				  <tr style="font-weight: bold; border-bottom: 1px solid black;">
                    <th>Component</th>
                    <th>Time</th>
                    <th>Analize</th>
                    <th>Standard</th>
                  </tr>
				  <?php foreach($INCIDENT as $key => $r): ?>
                  <tr style="font-weight: bold;">
                    <td><?php echo $r->NM_COMPONENT ."(".$r->KD_COMPONENT.")" ?></td>
                    <td><?php echo $r->JAM_ANALISA ?></td>
                    <td><?php echo $r->ANALISA ?></td>
                    <td><?php echo $r->NILAI_STANDARD_MIN." - ".$r->NILAI_STANDARD_MAX ?></td>
                  </tr>
                  <?php endforeach; ?>
                </table>
			</label>
			</p>
		</td> 
	</tr>
	<tr> 
		<td>Creator : <label><?= $DATA->CREATE_BY ?></label></td> 
		<td>Tanggal : <label><?= date('d F Y', strtotime($DATA->CREATE_DATE)) ?></label></td> 

	</tr>  
	<tr> 
		<td colspan="2">Analisa Penyebab : 
			<p> <label><?= $DATA->ANALISA ?></label></p>
		</td> 
	</tr> 
	<tr> 
		<td>Evaluator : <label><?= $DATA->CREATE_BY ?></label></td> 
		<td>Tanggal : <label><?= date('d F Y', strtotime($DATA->CREATE_DATE)) ?></label></td> 
	</tr> 
	<tr> 
		<td colspan="2">Tindakan Koreksi :
			<P><label><?= $DATA->TINDAKAN ?></label></P>
		</td> 
	</tr> 
	<tr rowspan="2"> 
		<td colspan="2">Tindakan Pencegahan :
			<p><label><?= $DATA->PENCEGAHAN ?></label></p>
		</td> 
	</tr> 
	<tr > 
		<td colspan="2">Tanggal : <label><?= date('d F Y', strtotime($DATA->CREATE_DATE)) ?></label></td> 
	</tr> 
	<tr > 
		<td colspan="2">Tertanda : <?= $DATA->CREATE_BY ?> </td> 
	</tr> 

	<tr> 
		<td>Perencana : <label><?= $DATA->CREATE_BY ?></label></td> 
		<td>Tanggal : <label><?= date('d F Y', strtotime($DATA->CREATE_DATE)) ?></label></td> 
	</tr> 
</table>

</div>

