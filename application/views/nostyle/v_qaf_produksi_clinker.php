<div class="col-sm-6 col-sm-6">
	<div class="box-body table-responsive no-padding">
		<table class="table" border=1 id=tbldata WIDTH=100%>
			<tr><th>CLINKER</th><th>PRODUCTION</th><th>QAF</th></tr>
			<?php 
			$TOTAL_QAF = 0;
			$TOTAL_PRODUKSI = 0;
			$QAFxPROD = 0;
			$TOTAL_QxP = 0;
			foreach($this->list_qaf as $r): 
				if($r->QAF_AREA > 0 ){
					$TOTAL_PRODUKSI += $r->PRODUKSI;
					$QAFxPROD = ($r->QAF_AREA * $r->PRODUKSI);
					//$QAFxPROD = ($r->QAF/100 * $r->PRODUKSI);
					$TOTAL_QxP += $QAFxPROD;
				}
			?>
			<TR>
				<TD><?php echo $r->NM_AREA ?></TD>
				<TD><?php echo ($r->PRODUKSI)?$r->PRODUKSI:0; ?></TD>
				<TD><?php echo (is_nan($r->QAF_AREA))?0:$r->QAF_AREA?></TD>
			</TR>
			<?PHP endforeach; 
			
			$total = @ROUND( ($TOTAL_QxP/$TOTAL_PRODUKSI),2);
			
			$total = (is_nan($total))?0:$total;
			?>
			<TR ID=trtotal><TD colspan=2>QAF TOTAL</TD><TD><?PHP echo $total  ?></TD></TR>
		</table>
	</div>
</div>


<div class="col-sm-6 col-sm-6">
	<div class="box-body table-responsive no-padding">
		<table class="table" border=1 id=tbldata WIDTH=100%>
			<tr>
				<th ROWSPAN="2" style="vertical-align: middle">CLINKER</th>
				<th ROWSPAN="2" style="vertical-align: middle">PRODUCTION</th>
				<th COLSPAN="<?PHP echo count($this->list_component) ?>">CLINKER</th>
				<th rowspan="2">QAF</th>
			</tr>

			<tr>
				<?php foreach($this->list_component as $c): ?>
					<th><?php echo $c->KD_COMPONENT ?></th>
				<?php endforeach; ?>
			</tr>
			<?php 
			$TOTAL_QAF = 0;
			$TOTAL_PRODUKSI = 0;
			$QAFxPROD = 0;
			$TOTAL_QxP = 0;
			$prod_total = 0;
			$totalSub = array();
			// var_dump($this->list_qaf2);
			foreach($this->list_qaf as $r): 
				if($r->QAF_AREA > 0 ){
					$TOTAL_PRODUKSI += $r->PRODUKSI;
					$QAFxPROD = ($r->QAF_AREA * $r->PRODUKSI);
					$TOTAL_QxP += $QAFxPROD;
					$prod_total += ($r->PRODUKSI)?$r->PRODUKSI:0;
					$prod_plant = ($r->PRODUKSI)?$r->PRODUKSI:0;
				}
			?>
			<TR>
				<TD><?php echo $r->NM_AREA ?></TD>
				<TD><?php echo ($r->PRODUKSI)?$r->PRODUKSI:0; ?></TD>
			<?php foreach($this->list_qaf2 as $c): ?>	
			<?php
			// var_dump($c);
				foreach($c as $d): 
				if($r->NM_AREA == $d->NM_AREA){
			?> <td> 
				<?php
					echo ((float)$d->PERSEN_QAF != 0)?(float)$d->PERSEN_QAF:''; 
					if(!isset($totalSub[$d->ID_COMPONENT])){
						$totalSub[$d->ID_COMPONENT] = 0;
					}
					$totalSub[$d->ID_COMPONENT] += $prod_plant * ((float)$d->PERSEN_QAF);
					$total += $d->PERSEN_QAF;
					$total1 += ($d->S_DATA != 0)?count($d->S_DATA):''; 
					#count($c->S_DATA); ########## <<<<----- TOTAL QAF ?></td>
				<?php 
					}
				endforeach; 
				endforeach; 
				?>
			<?php if(!count($this->list_qaf2)): ?>
				<?php foreach($this->list_component as $c): ?>
					<td>0</td>
				<?php endforeach; ?>
			<?php endif; ?>
				<TD><?php echo ($r->PRODUKSI)?$r->QAF_AREA:0; ?></TD>
			</TR>
			<?PHP endforeach; 
			
			$total = @ROUND( ($TOTAL_QxP/$TOTAL_PRODUKSI),2);

			$total = (is_nan($total))?0:$total;
			?>
			<TR ID=trtotal>
				<TD>QAF TOTAL</TD>
				<TD><?php echo $prod_total;?></TD>
				<?php 
					// var_dump($totalSub);
					foreach ($totalSub as $tu) {
				?>
				 <td> 
          <?php 
            $total_sub = @ROUND($tu/$prod_total,2);
            $total_sub = (is_nan($total_sub)) ? 0 : $total_sub;

            echo $total_sub;
          ?>
				 </td>
				 <?php
					}
				?>
				<TD><?PHP echo $total  ?></TD></TR>
		</table>
	</div>
</div>
