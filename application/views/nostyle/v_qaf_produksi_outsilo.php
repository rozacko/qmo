<div class="col-sm-6 col-sm-12">
	<div class="box-body table-responsive no-padding">
		<table class="table" border=1 id=tbldata WIDTH=100%>
			<tr><th>Product</th><th>CEMENT PRODUCTION</th><th>QAF</th></tr>
			<?php 
			$TOTAL_QAF = 0;
			$TOTAL_PRODUKSI = 0;
			$QAFxPROD = 0;
			$TOTAL_QxP = (float) 0;
			foreach($this->list_qaf as $r): 
				if ($r->QAF_PRODUCT > 0){
					$TOTAL_PRODUKSI += (float)$r->PRODUKSI;				
					$TOTAL_QxP += (float) $r->QAF_PRODUCT * (float) $r->PRODUKSI;	
				}		
			?>
			<TR>
				<TD><?php echo $r->KD_PRODUCT; ?></TD>
				<TD><?php echo (float)$r->PRODUKSI; ?></TD>
				<TD><?php echo (is_nan($r->QAF_PRODUCT)) ? 0: (float) $r->QAF_PRODUCT; ?></TD>
			</TR>
			<?PHP 
			endforeach; 
			
			$total = $TOTAL_QxP/$TOTAL_PRODUKSI;			
			$total = (is_nan($total))?0:$total;
			?>
			<TR ID=trtotal><TD colspan=2>QAF TOTAL</TD><TD><?PHP echo round($total ,2) ?></TD></TR>
		</table>
	</div>
</div>
