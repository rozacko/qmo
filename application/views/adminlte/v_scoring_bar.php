<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Scoring Bar Chart
  <small></small>
  </h1>
</section>
<style>
/*START CSS LOADING*/
  .spinner {
    margin: 100px auto 0;
    width: 70px;
    text-align: center;
  }

  .spinner>div {
    width: 18px;
    height: 18px;
    background-color: #333;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0)
    }
    40% {
      -webkit-transform: scale(1.0)
    }
  }

  @keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0);
      transform: scale(0);
    }
    40% {
      -webkit-transform: scale(1.0);
      transform: scale(1.0);
    }
  }
  /*END CSS LOADING*/
</style>
<!-- Main content -->

<section class="content" >
    <div class="row" >
      <div class="col-md-12">
          <div class="box">
          <div class="box-body">
            <div class="col-md-2">
                    <div class="form-group">
                      <label>YEAR</label>
                      <select id="tahun" class="form-control select2">
                        <?php
                          for($i=2016; $i<=date('Y'); $i++){
                            if(date('Y') == $i){
                              echo '<option selected value="'.$i.'">'.$i.'</option>';
                            }else{
                              echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                          }
                        ?>
                      </select> 
                    </div>
            </div>
            <div class="col-md-2">
                    <div class="form-group">
                      <label>PERIODE</label>
                      <select id="tipe" class="form-control select2">
                        <option value="1">1 Bulan</option>
                        <option value="3">3 Bulan</option>
                        <option value="6">6 Bulan</option>
                        <option value="12">12 Bulan</option>
                      </select>
                    </div>
            </div>
            <div class="col-md-8">
                    <div class="form-group">
                      <label>&nbsp;</label><br>
                      <button class="btn btn-sm btn-primary" id="btLoad"> <i class="fa fa-eye"></i> Load data</button>
                    </div>
            </div>
            <div class="col-md-12">
              <hr>
            </div>
                <div class="row" >
                    <div class="col-lg-12">
                        <div id="grafik_all" style="width:100%; height: 400px;"></div>
                    </div>
                </div>
            </div>
        </div>
      </div>
  </div>
</section>
<!-- /.content -->

<style type="text/css">
  th, td {
    padding: 9px;
    text-align: left;
  }

  div #tb_container {
    width: 250px;
    height:auto;
    min-height:200px;
    height:auto !important;  
    height:200px;
    text-align: center;
  }

  div #tb_label {
    border-radius: 100px;
    border: 3px solid #FFB432;
    width: 100px;
    height: 100px; 
    text-align: center;
    z-index: 1;position:relative;
  }

  #lb_text_opco {
    font-size: 30px;
    color: white;
  }

  #lb_text_nilai {
    float: left;
    font-size: 25px;
    color: white;
    width: 100%;
    margin-top: -8px;
  }

  span h3 { 
    font-weight: bold; 
    text-align: center;
  }

  div #tx_catatan {
    background: #CCDDFF;
    box-shadow: 12px 0 8px -4px rgba(31, 73, 125, 0.5), -12px 0 8px -4px rgba(31, 73, 125, 0.5);
    border-radius: 25px;
    width: 250px; 
    height:500px;
    margin-top: -15px;
    padding: 5px 5px 5px 0px;
    text-align: left;
  }

</style>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
    $(function(){
    $('.select2').select2();
  });

         $('#loading_css').hide();
          $(document).ready(function(){
            grafik_all()
                  $("#btLoad").click(function(){
                      grafik_all()
                  });
              
          });
          
          
          function grafik_all(){
              var tahun = $("#tahun").val();
              var tipe = $("#tipe").val();
              $('#grafik_all').hide();
              $('#loading_css').show('700');
              $.ajax({
                url: "<?php echo site_url("report_score/grafik_skor_bar");?>",
                type: "POST",
                data: {
                  'tahun' : tahun,
                  'tipe' : tipe,
                },
                dataType: "JSON",
                success: function (data) {
                     $('#grafik_all').show();
                    $('#loading_css').hide('500');                    
                  var chart2 = new Highcharts.chart('grafik_all', {
                 title: {
                      text: 'SI RAMAH SCORING '+tahun+' ('+tipe+' Bulanan)' 
                    },
                  credits: {
                    enabled: false
                  },
                xAxis: {
                categories: data.kategori,
                title: {
                      text: ''
                    }
                },
                  yAxis: {
                    title: {
                      text: 'skor'
                    },
                  },
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  },

                  plotOptions: {
                    series: {
                      label: {
                        connectorAllowed: false
                      }
                    }
                  },colors: [
                                    '#05354d',
                                    '#f7ca78',
                                    '#7bc8a4',
                                    '#93648d',
                                    '#4cc3d9',
                                    '#1f7f91',
                                    '#074b6d',
                                    '#d18700',
                                    '#ffdd9e',
                                ],

                          series: data.grafik,
                          plotOptions: {
                          column: {
                              depth: 25,
                              dataLabels: {
                                _useHTML: true,
                                enabled: true,
                                crop: false,
                                rotation: 270,
                                x: 0,
                                y: -14,
                                        style: {
                                      fontSize: 10, 
                                  overflow: 'none',
                                      textOutline: '1px contrast'
                                }
                            }
                          }
                      },

                          responsive: {
                            rules: [{
                              condition: {
                                maxWidth: 500
                              },
                              chartOptions: {
                                legend: {
                                  layout: 'horizontal',
                                  align: 'center',
                                  verticalAlign: 'bottom'
                                }
                              }
                            }]
                          }
                        },
                    );
                  } 
                  
                  
                });
              
          }
          
</script>
