   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelaksanaan Blind Test
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">    
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Pelaksanaan Blind Test</h3>
                <!-- <div class="input-group input-group-sm" style="width: 150px; float: right;">
                  <a id="AddNew" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a>
                </div> -->
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">No. </th>
                        <th width="*">PELAKSANAAN</th>
                        <th width="*">PLANT</th>
                        <th width="*">TAHUN</th>
                        <th width="*">START DATE</th>
                        <th width="*">END DATE</th>
                        <th width="*">PIC Observasi</th>
                        <th width="*">PIC PLANT</th>
                        <th width="*">TANGGAL KIRIM</th>
                        <th width="5%">ACTION</th>
                      </tr>
                    </thead>
                  </table>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script type="text/javascript">
  
  function sendsample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk mengirim sample sekarang ?");
    if (r == true) {
      // console.log("testing"); 
      // $("#ReloadData").click();
      $.ajax({
        url: "<?php echo site_url("acclab_kirimsampleblindtest/send_blindtest");?>",
        data: {"FK_ID_PERIODE": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }

$(document).ready(function(){

  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable;
  var example1 = document.getElementById('example1'), hot1;

  reloadtabel();  

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    // $('#saving').show();
    // $('#formArea').show();
    // $('#viewArea').hide();  

    // $('#UpdateArea').show();
    // $('#AddArea').hide(); 

    // var data = oTable.row($(this).parents('tr')).data();

    // $('#ID_MATERIAL').val(data['ID_MATERIAL']);
    // $('#KODE_MATERIAL').val(data['KODE_MATERIAL']);
    // $('#NAME_MATERIAL').val(data['NAME_MATERIAL']);
    // $('#H20_PERCENT').val(data['H2O_PERCENTAGE']);
    // $('#COMP_RELATION').val(data['ID_COMPONENT']);
    // $('#COMP_PERCENT').val(data['COMP_PERCENTAGE']);
    // $('#saving').hide();
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    // $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("acclab_kirimsampleblindtest/blindtest_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "RNUM", "width": 50},
        {"data": "NAMA"},
        {"data": "NAMA_LAB"},
        {"data": "TAHUN"},
        {"data": "SETUP_START"},
        {"data": "SETUP_END"},
        {"data": "NAMA_PIC_OBS"},
        {"data": "NAMA_PIC_PLANT"},
        {"data": "TANGGAL_KIRIM"},
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){
            // return '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';
            // console.log(row);
            // console.log(data);
            // console.log(index);
            // console.log(index['TANGGAL_KIRIM']);

            if (!index['TANGGAL_KIRIM']) {
              var btn = '';
              <?php if($this->PERM_WRITE): ?>
                btn =  '<button title="Kirim Sample" class="btSend btn btn-warning btn-xs" type="button" onclick="sendsample('+index['ID']+')"><i class="fa fa-send"></i> Kirim Blind Test</button>';
              <?PHP endif; ?>
              return btn;
            } else {
              return '';
            }

          }
        },
      ]
    });
    
    $('#ReloadData').show();
    // $('#AddNew').show();
  }

  function getContentData() {
      return [["TRASS", , , , ],["LIMESTONE", , , , ],["GYPSUM", , , , ]];
  }

  function getRowOptions() {
    var initial_coloptions = [
      {
        type: 'text',
        readOnly: true
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      }
    ];

    return initial_coloptions;
  }

  function getColHeader() {
    var columnlist = ['Third Material', 'H20', 'LOi', 'Insol', 'SO3'];
    return columnlist;
  }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
