<style>
  #dt_subtables_wrapper, #dt_submenutables_wrapper{
    display: none;
  }
</style>

<section class="content-header">
      <h1>
        Master Data Group Menu
        <small>information about data group menu.</small>
      </h1>

    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <ul class="timeline">
            <li id="layoutList" class="animated fadeIn">
              <i class="fa fa-list bg-blue"></i>

              <div class="timeline-item timeline-primaryhear">
                <span class="time">
                  <?php if ($this->PERM_WRITE): ?>
                      <div class="input-group input-group-sm">
                        <button id="btnCreate" create="groupmenu" type="button" class="btn btn-block btn-primary btn-sm"><i class="fa fa-pencil"></i> Create New</button>
                      </div>
                      <?PHP endif;?>
                 </span>
                <h3 class="timeline-header">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group has-feedback">
                        <select class="select2 id_groupmenu form-control" name="Q_ID_GROUPMENU" id="q_id_groupmenu">
                          <option value="">Choose Groupmenu...</option>
                          <?PHP
                          foreach ($this->list_groupmenu as $i => $v) {
                            echo '<option value="'.$v->ID_GROUPMENU.'">'.$v->NM_GROUPMENU.'</option>';
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group has-feedback">
                        <select class="select2 form-control" name="Q_ID_SUBMENU" id="q_id_submenu">
                          <option value="">Choose Submenu...</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </h3>

                <div class="timeline-body" id="dt_detailTable">
                  <table  id="dt_tables"
                        class="table table-striped table-bordered table-hover dt-responsive nowrap"
                        cellspacing="0"
                        width="100%">
                    <thead>
                      <tr>
                        <!-- <th width="3%">ID</th> -->
                        <th width="5%">No.</th>
                        <th>NAME</th>
                        <th width="3%">ORDER</th>
                        <th width="10%"></th>
                      </tr>
                    </thead>
                  </table>

                  <table  id="dt_subtables"
                        class="table table-striped table-bordered table-hover dt-responsive nowrap"
                        cellspacing="0"
                        width="100%" style="display: none">
                    <thead>
                      <tr>
                        <!-- <th width="3%">ID</th> -->
                        <th width="5%">No SUB.</th>
                        <th>NAME</th>
                        <th width="3%">ORDER</th>
                        <th width="10%"></th>
                      </tr>
                    </thead>
                  </table>

                  <table  id="dt_submenutables"
                        class="table table-striped table-bordered table-hover dt-responsive nowrap"
                        cellspacing="0"
                        width="100%" style="display: none">
                    <thead>
                      <tr>
                        <!-- <th width="3%">ID</th> -->
                        <th width="5%">No.</th>
                        <th>NAME</th>
                        <th width="3%">ORDER</th>
                        <th width="10%"></th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </li>
            <li id="layoutCreate" class="animated fadeIn" style="display: none">
              <i class="fa fa-pencil bg-yellow"></i>
              <div class="timeline-item timeline-primaryhear typeGroupmenu">
                <h3 class="timeline-header">Add Data Group Menu</h3>
                <div class="timeline-body">
                  <form class="formSubmit" typeSubmit="groupmenu" role="form" method="POST" action="<?php echo site_url("groupmenu/create_v2") ?>" >
                    <div class="box-body">
                      <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group">
                          <label>GROUPMENU NAME</label>
                          <input type="text" class="form-control" name="NM_GROUPMENU" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" placeholder="Order">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>


              <div class="timeline-item timeline-primaryhear typeSubmenu">
                <h3 class="timeline-header">Add Data Sub Menu</h3>
                <div class="timeline-body">
                  <form class="formSubmit" typeSubmit="submenu" role="form" method="POST" action="<?php echo site_url("menu/create_v2") ?>" >
                    <div class="box-body">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>SUB MENU NAME</label>
                            <input type="text" class="form-control" name="NM_MENU" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>URL MENU</label>
                            <input type="text" class="form-control" name="URL_MENU" placeholder="Url Menu" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" placeholder="Order">
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="checkbox" name="ACTIVE" value="1"> Activated
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>

              <div class="timeline-item timeline-primaryhear typeMenu">
                <h3 class="timeline-header">Add Data Menu</h3>
                <div class="timeline-body">
                  <form class="formSubmit" typeSubmit="menu" role="form" method="POST" action="<?php echo site_url("menu/create_v2") ?>" >
                    <div class="box-body">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>MENU NAME</label>
                            <input type="hidden" readonly class="form-control" name="IS_SUBMENU" value="Y" REQUIRED>
                            <input type="text" class="form-control" name="NM_MENU" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>URL MENU</label>
                            <input type="text" class="form-control" name="URL_MENU" placeholder="Url Menu" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" placeholder="Order" required>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="checkbox" name="ACTIVE" value="1"> Activated
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>

            </li>
            <li id="layoutUpdate" class="animated fadeIn" style="display: none;">
              <i class="fa fa-pencil bg-yellow"></i>
              <div class="timeline-item timeline-primaryhear typeGroupmenu">
                <h3 class="timeline-header">Update Data Group Menu</h3>
                <div class="timeline-body">
                  <form id="formUpdateGroupmenu" class="formSubmit" typeSubmit="groupmenu" typeForm="update" role="form" method="POST" action="" >
                    <input id="ID_GROUPMENU" type="hidden" class="form-control" name="ID_GROUPMENU" REQUIRED readonly>
                    <div class="box-body">
                      <div class="row">
                       <div class="col-sm-4">
                        <div class="form-group">
                          <label>GROUPMENU NAME</label>
                          <input id="NM_GROUPMENU" type="text" class="form-control" name="NM_GROUPMENU" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input id="NO_ORDER" name="NO_ORDER" type="number" class="form-control" name="NO_ORDER" placeholder="Order">
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                      <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>

              <div class="timeline-item timeline-primaryhear typeSubmenu">
                <h3 class="timeline-header">Add Data Sub Menu</h3>
                <div class="timeline-body">
                  <form id="formUpdateSubmenu" class="formSubmit" typeSubmit="submenu" typeForm="update" role="form" method="POST" action="" >
                    <div class="box-body">
                      <div class="row">
                      <div class="col-sm-4">
                          <label>GROUPMENU</label>
                          <select style="width: 100%" class="select2 form-control id_groupmenu" name="ID_GROUPMENU" id="ID_GROUPMENU_UP">
                            <option value="">Choose Groupmenu...</option>
                            <?PHP
                            foreach ($this->list_groupmenu as $i => $v) {
                              echo '<option value="'.$v->ID_GROUPMENU.'">'.$v->NM_GROUPMENU.'</option>';
                            } 
                            ?>
                          </select> 
                        </div>
                        <div class="col-sm-4">
                          <label>SUBMENU <small class="pull-right" style="color: #ccc">(Menu Jadi "Submenu Jika dikosongi")</small></label>
                          <select style="width: 100%" class="select2 form-control id_submenu" name="PARENT" id="ID_PARENT_UP_E">
                            <option value="">Choose Submenu...</option>
                          </select>
                        </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>SUB MENU NAME</label>
                            <input type="hidden" class="form-control" name="ID_MENU" id="ID_MENU_UP" REQUIRED>
                            <input type="text" class="form-control" name="NM_MENU" id="NM_MENU_UP" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>URL MENU</label>
                            <input type="text" class="form-control" name="URL_MENU" id="URL_MENU_UP" placeholder="Url Menu" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" id="NO_ORDER_UP" placeholder="Order">
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="checkbox" name="ACTIVE" id="ACTIVE_UP" value="1"> Activated
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>

              <div class="timeline-item timeline-primaryhear typeMenu">
                <h3 class="timeline-header">Update Data Menu</h3>
                <div class="timeline-body">
                  <form id="formUpdateMenu" class="formSubmit" typeSubmit="menu" typeForm="update" role="form" method="POST" action="" >
                    <div class="box-body">
                      <div class="row">
                        <div class="col-sm-4">
                          <label>GROUPMENU</label>
                          <select style="width: 100%" class="select2 form-control id_groupmenu" name="ID_GROUPMENU" id="ID_GROUPMENU_UP_" dataType="form">
                            <option value="">Choose Groupmenu...</option>
                            <?PHP
                            foreach ($this->list_groupmenu as $i => $v) {
                              echo '<option value="'.$v->ID_GROUPMENU.'">'.$v->NM_GROUPMENU.'</option>';
                            }
                            ?>
                          </select>
                        </div>
                        <div class="col-sm-4">
                          <label>SUBMENU</label>
                          <select style="width: 100%" class="select2 form-control id_submenu" name="PARENT" id="ID_PARENT_UP">
                            <option value="">Choose Submenu...</option>
                          </select>
                        </div>

                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>SUB MENU NAME</label>
                            <input type="hidden" class="form-control" name="ID_MENU" id="ID_MENU_UP_" REQUIRED>
                            <input type="text" class="form-control" name="NM_MENU" id="NM_MENU_UP_" placeholder="Group Menu Name" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="form-group">
                            <label>URL MENU</label>
                            <input type="text" class="form-control" name="URL_MENU" id="URL_MENU_UP_" placeholder="Url Menu" REQUIRED>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-group">
                            <label>ORDER</label>
                            <input type="number" class="form-control" name="NO_ORDER" id="NO_ORDER_UP_" placeholder="Order">
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <input type="checkbox" name="ACTIVE" id="ACTIVE_UP_" value="1"> Activated
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" class="btnCancel btn btn-danger">Cancel</button>
                    </div>
                  </form>
                </div>
              </div>

            </li>

            <li>
              <!-- <i class="fa fa-clock-o bg-gray"></i> -->
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

</section>


<div class="modal fade" id="modal_submenu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">LIST MENU <span class="title_submenu">-</span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="dt_detail">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


<!-- msg confirm -->
<?php if ($notice->error): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>

<?php endif;?>

<?php if ($notice->success): ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>
<?php endif;?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>

<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css"); ?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js"); ?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js"); ?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>

$(document).ready(function(){

  $('.select2').select2();

  $('#btnCreate').on('click', function(){
    $('#layoutList').hide();
    $('#layoutCreate').show();
    $('#layoutUpdate').hide();

    typeSubmit = $(this).attr('create');
    if(typeSubmit == 'groupmenu'){
      $('.typeGroupmenu').show();
      $('.typeSubmenu, .typeMenu').hide();

    }else if(typeSubmit == 'submenu'){
      $('.typeSubmenu').show();
      $('.typeGroupmenu, .typeMenu').hide();

    }else{
      $('.typeMenu').show();
      $('.typeGroupmenu, .typeSubmenu').hide();
    }
  });

  $('.btnCancel').on('click', function(){
    $('#layoutList').show();
    $('#layoutCreate').hide();
    $('#layoutUpdate').hide();
  });

	$(".delete").confirm({
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});

	$(".notice-error").confirm({
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});

	$(".notice-success").confirm({
		confirm: function(button) { /* Nothing */ },
		confirmButton: "OK",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-success"
	});

	<?php if ($notice->error): ?>
	$("#a-notice-error").click();
	<?php endif;?>

	<?php if ($notice->success): ?>
	$("#a-notice-success").click();
	<?php endif;?>

  /** DataTables Init **/
  var table = $("#dt_tables").DataTable({
      language: {
        searchPlaceholder: ""
      },
      "paging": true,
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
              "<'row'<'col-sm-12'rt>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": [
      {
        extend: "pageLength",
        className: "btn-sm bt-separ"
      },
      {
        text: "<i class='fa fa-refresh'></i> Reload",
        className: "btn-sm",
          action: function(){
            dtReload(table);
          }
        }
      ],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url" : '<?php echo site_url("groupmenu/get_list"); ?>',
      "type": 'post',
        "data": function(data){
          //data.groupmenu = $("#ID_COMPANY").val();
        }
      },
      "initComplete": function(settings, json) {
      },
      "columns" : [
        {data:'1', className: "text-center"},
        {data:'2'},
        {className: "text-center",
          "render": function ( data, type, full, meta ) {
            return '<span class="label label-warning">'+full['3']+'</span>';
          }
        },
        {
          className: "text-center",
          sortable: false,
          "render": function ( data, type, full, meta ) {
                     return '<?php if ($this->PERM_WRITE) {?> <div class="btn-group"><button title="Update data" class="btn btn-primary btn-sm btnUpdate" dataclick="update" typeSubmit="groupmenu"><i class="fa fa-pencil"></i></button><button title="Delete data" class="btn btn-sm btn-danger btnDelete" dataclick="delete"><i class="fa fa-trash-o"></i></button></div><?php }?>';
          }
        },
      ]
    });

  $('#dt_tables tbody').on('click', 'td button', function () {
      var data = table.row($(this).parents('tr')).data();
      dataclick= $(this).attr('dataclick');
      if(dataclick == 'update'){

        $('#ID_GROUPMENU').val(data[0]);
        $('#NM_GROUPMENU').val(data[2]);
        $('#NO_ORDER').val(data[3]);

        $('#formUpdateGroupmenu').attr('action', '<?php echo site_url("groupmenu/update_v2/") ?>'+data[0]);

        $('#layoutList').hide();
        $('#layoutCreate').hide();
        $('#layoutUpdate').show();
        
        $('.typeGroupmenu').show();
        $('.typeSubmenu, .typeMenu').hide();

      }else if(dataclick == 'delete'){
        var url = '<?php echo site_url("groupmenu/delete_v2/"); ?>' + data[0];

        $.confirm({ 
          title: "Remove Groupmenu",
          text: "This groupmenu will be removed. Are you sure?",
          confirmButton: "Remove",
          confirmButtonClass: "btn-danger",
          cancelButton: "Cancel",
          confirm: function() {
            $.get( url, function(data) {
              if(data['status'] == true){
                  notif(1, data['msg']);
                  table.ajax.reload();
                }else{
                  notif(3, data['msg']);
                }
            });
          },
          cancel: function() {
              // nothing to do
          }
        });
      }else{

      }
  });

  var subtable = $("#dt_subtables").DataTable({
      language: {
        searchPlaceholder: ""
      },
      "paging": true,
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
              "<'row'<'col-sm-12'rt>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": [
      {
        extend: "pageLength",
        className: "btn-sm bt-separ"
      },
      {
        text: "<i class='fa fa-refresh'></i> Reload",
        className: "btn-sm",
          action: function(){
            dtReload(subtable);
          }
        }
      ],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url" : '<?php echo site_url("menu/get_list"); ?>',
      "type": 'post',
        "data": function(data){
          data.groupmenu = $("#q_id_groupmenu").val();
        }
      },
      "initComplete": function(settings, json) {
      },
      "columns" : [
        {data:'1', className: "text-center"},
        {data:'3'},
        {className: "text-center",
          "render": function ( data, type, full, meta ) {
            return '<span class="label label-warning">'+full['5']+'</span>';
          }
        },
        {
          className: "text-center",
          sortable: false,
          "render": function ( data, type, full, meta ) {
                     return '<?php if ($this->PERM_WRITE) {?> <div class="btn-group"><button title="Update data" class="btn btn-primary btn-sm btnUpdate" dataclick="update"><i class="fa fa-pencil"></i></button><button title="Delete data" class="btn btn-sm btn-danger btnDelete" dataclick="delete"><i class="fa fa-trash-o"></i></button></div><?php }?>';
          }
        },
      ]
    });

  $('#dt_subtables tbody').on('click', 'td button', function () {
      var data = subtable.row($(this).parents('tr')).data();
// console.log(data);
      dataclick= $(this).attr('dataclick');
      if(dataclick == 'update'){
        $('#ID_GROUPMENU_UP').val(data[7]).trigger("change");

        $('#ID_MENU_UP').val(data[0]);
        $('#NM_MENU_UP').val(data[3]);
        $('#URL_MENU_UP').val(data[4]);
        $('#NO_ORDER_UP').val(data[5]);
        submenu_picker(data[7], "#ID_PARENT_UP_E", data[0]);

        if(data[6] == 1){
          $('#ACTIVE_UP').attr("checked", true);
        }else{
          $('#ACTIVE_UP').attr("checked", false);
        }

        $('#formUpdateSubmenu').attr('action', '<?php echo site_url("menu/update_v2/") ?>'+data[0]);

        $('#layoutList').hide();
        $('#layoutCreate').hide();
        $('#layoutUpdate').show();

        $('.typeSubmenu').show();
        $('.typeGroupmenu, .typeMenu').hide();

      }else if(dataclick == 'delete'){
        var url = '<?php echo site_url("menu/delete_v2/"); ?>' + data[0];

        $.confirm({ 
          title: "Remove Submenu",
          text: "This submenu will be removed. Are you sure?",
          confirmButton: "Remove",
          confirmButtonClass: "btn-danger",
          cancelButton: "Cancel",
          confirm: function() {
            $.get( url, function(data) {
              if(data['status'] == true){
                  notif(1, data['msg']);
                  $('#q_id_submenu').click();
                  subtable.ajax.reload();
              }else{
                  notif(3, data['msg']);
              }
            }).error(function() {
              notif(4, 'SQL Error [2292] [23000]: ORA-02292: integrity constraint (QMUSER2.FK_ID_MENU) violated - child record found');
            });
          },
          cancel: function() {
              // nothing to do
          }
        });
      }else{
      }
  });

  var submenutable = $("#dt_submenutables").DataTable({
      language: {
        searchPlaceholder: ""
      },
      "paging": true,
      "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
      "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
              "<'row'<'col-sm-12'rt>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": [
      {
        extend: "pageLength",
        className: "btn-sm bt-separ"
      },
      {
        text: "<i class='fa fa-refresh'></i> Reload",
        className: "btn-sm",
          action: function(){
            dtReload(submenutable);
          }
        }
      ],
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": {
      "url" : '<?php echo site_url("menu/get_list"); ?>',
      "type": 'post',
        "data": function(data){
          data.parent = $("#q_id_submenu").val();
        }
      },
      "initComplete": function(settings, json) {
      },
      "columns" : [
        {data:'1', className: "text-center"},
        {data:'3'},
        {className: "text-center",
          "render": function ( data, type, full, meta ) {
            return '<span class="label label-warning">'+full['5']+'</span>';
          }
        },
        {
          className: "text-center",
          sortable: false,
          "render": function ( data, type, full, meta ) {
                     return '<?php if ($this->PERM_WRITE) {?> <div class="btn-group"><button title="Update data" class="btn btn-primary btn-sm btnUpdate" dataclick="update"><i class="fa fa-pencil"></i></button><button title="Delete data" class="btn btn-sm btn-danger btnDelete" dataclick="delete"><i class="fa fa-trash-o"></i></button></div><?php }?>';
          }
        },
      ]
    });

  $('#dt_submenutables tbody').on('click', 'td button', function () {
      var data = submenutable.row($(this).parents('tr')).data();
      dataclick= $(this).attr('dataclick');
      if(dataclick == 'update'){
        
        $('#ID_GROUPMENU_UP_').val(data[7]).trigger("change");
        $('#ID_GROUPMENU_UP_').attr('parent', data[8]);
        // alert(data[8]+ ' - EDIT');
        $('#ID_MENU_UP_').val(data[0]);
        $('#NM_MENU_UP_').val(data[3]);
        $('#URL_MENU_UP_').val(data[4]);
        $('#NO_ORDER_UP_').val(data[5]);

        if(data[6] == 1){
          $('#ACTIVE_UP_').attr("checked", true);
        }else{
          $('#ACTIVE_UP_').attr("checked", false);
        }

        $('#formUpdateMenu').attr('action', '<?php echo site_url("menu/update_v2/") ?>'+data[0]);

        $('#layoutList').hide();
        $('#layoutCreate').hide();
        $('#layoutUpdate').show();

        $('.typeMenu').show();
        $('.typeGroupmenu, .typeSubmenu').hide();

      }else if(dataclick == 'delete'){
        var url = '<?php echo site_url("menu/delete_v2/"); ?>' + data[0];

        $.confirm({ 
          title: "Remove Menu",
          text: "This Menu will be removed. Are you sure?",
          confirmButton: "Remove",
          confirmButtonClass: "btn-danger",
          cancelButton: "Cancel",
          confirm: function() {
            $.get( url, function(data) {
              if(data['status'] == true){
                  notif(1, data['msg']);
                  submenutable.ajax.reload();
                }else{
                  notif(3, data['msg']);
                }
            });
          },
          cancel: function() {
              // nothing to do
          }
        });
      }else{
      }
  });



  

    /** DataTable Ajax Reload **/
    function dtReload(table,time) {
      var time = (isNaN(time)) ? 100:time;
      setTimeout(function(){ table.search('').draw(); }, time);
    }

    //DataTable search on enter
    $('#dt_tables_filter input').unbind();
    $('#dt_tables_filter input').bind('keyup', function(e) {
      var val = this.value;
      var len = val.length;

      if(len==0) table.search('').draw();
      if(e.keyCode == 13) {
        table.search(this.value).draw();
      }
    });

    $('#q_id_groupmenu').on('change', function(){
      id_groupmenu = $(this).val();

      if(id_groupmenu){

        $('#btnCreate').attr('create', 'submenu');

        $('#dt_tables, #dt_tables_wrapper').hide();
        $('#dt_subtables, #dt_subtables_wrapper').show();
        $('#dt_submenutables, #dt_submenutables_wrapper').hide();

        //SUB TABLE ======================================= START
        $('#q_id_submenu').html('<option value="">Loading...</option>');
        $.ajax({
          url : '<?=base_url('groupmenu/get_detlist/')?>'+id_groupmenu,
          type: 'GET',
        }).done(function(data){
          console.log(data);
          $('#q_id_submenu').html('<option value="">Choose Submenu...</option>');
          $.each(data.data, function(i, v) {
            $('#q_id_submenu').append('<option value="'+v[0]+'">'+v[3]+'</option>');
          });
        });
        subtable.ajax.reload();
        //SUB TABLE ======================================= END

      }else{
        $('#btnCreate').attr('create', 'groupmenu');
        $('#q_id_submenu').html('<option value="">Choose Submenu...</option>');

        $('#dt_tables, #dt_tables_wrapper').show();
        $('#dt_subtables, #dt_subtables_wrapper').hide();
        $('#dt_submenutables, #dt_submenutables_wrapper').hide();    
      }
    });

    $('#ID_GROUPMENU_UP_, #ID_GROUPMENU_UP').on('change', function(){
      id_groupmenu = $(this).val();
      submenu_picker(id_groupmenu, "#ID_PARENT_UP");
      // parent = $(this).attr('parent');
      // alert(parent + ' -  FORM');
      
      // $('#ID_GROUPMENU_UP_').change();
    });

    function submenu_picker(id_groupmenu ,id, val = null){
      if(id_groupmenu){
        $(id).html('<option value="">Loading...</option>');
        $.ajax({
          url : '<?=base_url('groupmenu/get_detlist/')?>'+id_groupmenu,
          type: 'GET',
        }).done(function(data){
          // console.log(data);
          $(id).html('<option value="">Choose Submenu...</option>');
          parent = $('#ID_GROUPMENU_UP_').attr('parent');

          $.each(data.data, function(i, v) {
            if(parent == v[0]){
              $(id).append('<option selected value="'+v[0]+'">'+v[3]+'</option>');
            }else{
              $(id).append('<option value="'+v[0]+'">'+v[3]+'</option>');
            }
          });

          if(val){
            $(id).val(val).change();
          }
        });
      }else{
        $(id).html('<option value="">Choose Submenu...</option>');
      }
    }

    $('#q_id_submenu').on('change', function(){
      parent = $(this).val();

      if(parent){
        $('#btnCreate').attr('create', 'subdetmenu');

        $('#dt_tables, #dt_tables_wrapper').hide();
        $('#dt_subtables, #dt_subtables_wrapper').hide();
        $('#dt_submenutables, #dt_submenutables_wrapper').show();

        submenutable.ajax.reload();

      }else{
        $('#btnCreate').attr('create', 'submenu');

        $('#dt_tables, #dt_tables_wrapper').hide();
        $('#dt_subtables, #dt_subtables_wrapper').show();
        $('#dt_submenutables, #dt_submenutables_wrapper').hide();
      }
    });

  $(".formSubmit").submit(function(e) {
    var form = $(this);
    var url = form.attr('action');
    var typeSubmit = form.attr('typeSubmit');
    var typeForm = form.attr('typeForm');

    if(typeForm == 'update'){

    }else{
      if(typeSubmit == 'submenu'){
        $input_groupmenu = $('<input type="hidden" name="ID_GROUPMENU"/>').val($('#q_id_groupmenu').val());
        $(this).append($input_groupmenu);
      }else if(typeSubmit == 'menu'){
        $input_groupmenu = $('<input type="hidden" name="ID_GROUPMENU"/>').val($('#q_id_groupmenu').val());
        $input_parent = $('<input type="hidden" name="PARENT"/>').val($('#q_id_submenu').val());
        $(this).append($input_groupmenu);
        $(this).append($input_parent);
      }
    }

    $.ajax({
      type: "POST",
      url: url,
      data: form.serialize(), // serializes the form's elements.
      success: function(data)
      {
        if(data['status'] == true){
          notif(1, data['msg']);
          $('.btnCancel').click();
          if(typeSubmit == 'submenu'){
            $('#q_id_groupmenu').change();
            subtable.ajax.reload();
          }else if(typeSubmit == 'menu'){
            submenutable.ajax.reload();
          }else{
            table.ajax.reload();
          }

        }else{
          notif(3, data['msg']);
        }
      },
      error: function (request, error) {
        notif(4, " Can't do because: " + error);
      },
    });

    e.preventDefault(); // avoid to execute the actual submit of the form.

  });
});

</script>
