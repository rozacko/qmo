   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        NCQR Incident
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <form id="receiverForm" role="form" method="POST" action="<?php echo site_url("incident/solving/".$this->data_incident[0]->ID_INCIDENT.'/'.$this->CURRENT.'/'.$this->ID_COMPANY) ?>" >
      <!-- <form id="receiverForm" role="form" method="POST" action="<?php echo site_url("incident/coba2/".$this->data_incident[0]->ID_INCIDENT.'/'.$this->CURRENT.'/'.$this->ID_COMPANY) ?>" > -->
      <div class="row">
        <div class="col-xs-12 col-md-12">

            <?php 
              if ($this->access && !isset($this->receiver->ID_ACTION)) {
                echo "<div class='box' id='RECEIVER'>";
              }else{
                echo "<div class='box' id='RECEIVER'>";
                echo "<input type='text' hidden='true' name='ID_ACTION'  value='".$this->receiver->ID_ACTION."'>";
                ?>
                <script type="text/javascript">
                  $(document).ready(function(){
                    $("#RECEIVER :input").attr("readonly", true);
                  });
                </script>
                <?php

              }
            ?>
         
            <div class="box-header">
              <div class="col-md-12" valign="center">
                
                  <label>CORRECTION AND PREVENTION ACTIONS</label>
              </div>
              
            </div>
            <div class="box-body">
                  <?php 
                    // print_r($this->incident);
                    // print_r($this->receiver);
                    // print_r($_SESSION);
                    // print_r($this->data_user);
                    // echo $incident;
                   ?>
                  <div class="col-sm-12 clearfix ">
                    <!--    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>NO. TKP </label>
                        <input type="text" class="form-control" name="NO">
                      </div>
                    </div> -->
                    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>DATE INCIDENT</label>
                        <input type="text" class="form-control" name="DATE_INCIDENT" readonly value="<?php echo $this->receiver->DATE_INCIDENT ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>UNIT KERJA </label>
                        <input type="text" class="form-control" name="UK_KODE" readonly style="display: none;" value="<?php echo $this->data_user->unit_kerja ?>">
                        <input type="text" class="form-control" name="UK_NAMA" value="<?php echo $this->receiver->UNIT_KERJA ?>">
                      </div>
                    </div>
                    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>INCIDENT AREA </label>
                        <input type="text" class="form-control" name="KD_AREA" readonly style="display: none;" value="<?= $this->incident['KD_AREA'] ?>">
                        <input type="text" class="form-control" name="AREA" readonly value="<?php echo $this->incident[NM_AREA] ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                  </div> 
                  <div class="col-sm-12 clearfix ">

                    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>REFERENCE </label>

                       <input type="text" class="form-control" name="REFERENCE" value="<?= (($this->receiver->ACUAN)?$this->receiver->ACUAN:'ISO/IEC 17025:205'); ?>">
                           

                      </div>
                    </div>
                    <div class="col-sm-6 ">
                      <div class="form-group">
                        <label>CLAUSE</label>
                        <input type="text" class="form-control" name="CLAUSE" value="<?= (($this->receiver->KLAUSUL)?$this->receiver->KLAUSUL:'5.9 : 4.11'); ?>">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                    <div class="col-sm-12 ">
                      <div class="form-group">
                        <label>INCIDENT DETAILS </label>
                          <!-- <div class="box-body table-responsive no-padding"> -->
                            <table class="table-fixed table-responsive no-padding">
                              <tr>
                                <th>Component</th>
                                <th>Time</th>
                                <th>Analize</th>
                                <th>Standard</th>
                              </tr>
                              <?php foreach($this->data_incident as $r): ?>
                              <tr>
                                <td><?php echo $r->NM_COMPONENT ."(".$r->KD_COMPONENT.")" ?></td>
                                <td><?php echo $r->JAM_ANALISA ?></td>
                                <td><?php echo $r->ANALISA ?></td>
                                <td><?php echo $r->NILAI_STANDARD_MIN." - ".$r->NILAI_STANDARD_MAX ?></td>
                              </tr>
                              <?php endforeach; ?>
                            </table>
                          <!-- </div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                    <div class="col-sm-12 ">
                      <div class="form-group">
                        <label>CAUSE ANALYSIS <i style="color: red;"> ( * Wajib di isi ) </i></label>
                        <textarea type="text" class="form-control" id="ANALISA" name="ANALISA"><?= $this->receiver->ANALISA; ?></textarea>
                        <span id="ANALISA1"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                    <div class="col-sm-12 ">
                      <div class="form-group">
                        <label>CORRECTION  <i style="color: red;"> ( * Wajib di isi ) </i></label>
                        <textarea type="text" class="form-control" name="TINDAKAN" id="TINDAKAN"><?= $this->receiver->TINDAKAN; ?></textarea>
                        <span id="TINDAKAN1"></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 clearfix ">
                    <div class="col-sm-12 ">
                      <div class="form-group">
                        <h5><label>PREVENTIVE MEASURE<label> <small>( tindakan pencegahan )</small> <i style="color: red;"> ( * Wajib di isi ) </i></h5>
                        <textarea type="text" class="form-control" name="PENCEGAHAN" id="PENCEGAHAN"><?= $this->receiver->PENCEGAHAN; ?></textarea>
                        <span id="PENCEGAHAN1"></span>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
            <?php 
            // echo 'aaa'. $this->accessQc;
              if ($this->accessQc) {
                echo "<div class='box' id='EVALUATOR'>";
              }else{
                echo "<div class='box' id='EVALUATOR' style='display:none;'>";

              }
            ?>

            <div class="box-header">
              <div class="col-md-12" valign="center">
                  <label>CORRECTION & PREVENTION ACTION VERIFICATION</label>
              </div>
            </div>
            <div class="box-body">
              <div class="col-sm-12 clearfix ">
                <div class="col-sm-12 ">
                  <div class="form-group">
                    <label>ACTION VERIFICATION  <i style="color: red;"> ( * Wajib di isi ) </i></label>
                    <textarea type="text" class="form-control" id="VERIFIKASI" name="VERIFIKASI"><?= $this->receiver->VERIFIKASI; ?></textarea>
                        <span id="VERIFIKASI1"></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
         <?php 
                echo "<input type='text' hidden='true' name='ID_ACTION'  value='".$this->receiver->ID_ACTION."'>";
              if ($this->accessQC) {
              }
            ?>
	
       <div class="row">
        <div class="col-xs-12">
          <!-- /.box-body -->
          <div class="box-footer">
            <?php
            // if (isset($this->receiver->ID_ACTION)){

            //   echo '<a href="'.site_url("incident/export/".$this->receiver->ID_INCIDENT).'" target="_blank" class="btn btn-warning">Print</a>';
            // } else { 
              echo '<button type="submit" class="btn btn-primary" id="tombol_registration">Approve</button>';
            // } ?>
            <a type="button" class="btn btn-default" href="<?php echo site_url("incident/index/").$this->CURRENT.'/'.$this->ID_COMPANY;?>" >Back</a>

            <button type="submit" class="btn btn-danger" name="tombol_reject" value="reject">Reject</button>
          </div>
          <!-- /.box -->
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
	
<script language="javascript" type="text/javascript" src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("js/jquery.maskedinput.js"); ?>" ></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("js/bootstrapValidator.js"); ?>" ></script>

<script>

$(document).ready(function(){

  console.log($("#receiverForm").serialize());

	$(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});
			
});

$(function() {

  $('#ANALISA').on('keyup', function() {
    var value = $(this).val();
    var count = 0;  
    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";  
    // 
    if ($('#ANALISA').val() == ''){  
      document.getElementById("tombol_registration").disabled = true;
      $('#ANALISA1').html('<span style="color:red;font-size:11.5px">Kolom tidak boleh kosong</span>');
      stop = true;
    } else {  
      document.getElementById("tombol_registration").disabled = false;  
      $('#ANALISA1').html('');
    }  
  });

  $('#VERIFIKASI').on('keyup', function() {
      var value = $(this).val();
      var count = 0;  
      var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";  
      for (var i = 0; i < value.length; i++){  
        if (iChars.indexOf(value.charAt(i)) != -1){  
          count++;  
        }  
      }  
      if ($(this).val() == ''){  
        document.getElementById("tombol_registration").disabled = true;
        $('#VERIFIKASI1').html('<span style="color:red;font-size:11.5px">Kolom tidak boleh kosong</span>');
        stop = true;
      } else {  
        document.getElementById("tombol_registration").disabled = false;  
        $('#VERIFIKASI1').html('');
      }  
    });


  $('#TINDAKAN').on('keyup', function() {
    var value = $(this).val();
    var count = 0;  
    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";  
    // 
    if ($('#TINDAKAN').val() == ''){  
      document.getElementById("tombol_registration").disabled = true;
      $('#TINDAKAN1').html('<span style="color:red;font-size:11.5px">Kolom tidak boleh kosong</span>');
      stop = true;
    } else {  
      document.getElementById("tombol_registration").disabled = false;  
      $('#TINDAKAN1').html('');
    }  
  });

  $('#PENCEGAHAN').on('keyup', function() {
    var value = $(this).val();
    var count = 0;  
    var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";  
    // 
    if ($('#PENCEGAHAN').val() == ''){  
      document.getElementById("tombol_registration").disabled = true;
      $('#PENCEGAHAN1').html('<span style="color:red;font-size:11.5px">Kolom tidak boleh kosong</span>');
      stop = true;
    } else {  
      document.getElementById("tombol_registration").disabled = false;  
      $('#PENCEGAHAN1').html('');
    }  
  });

  $('#receiverForm').bootstrapValidator({
    fields: {
      ANALISA: {
        validators: {
          notEmpty: {}
        }
      },

      VERIFIKASI: {
        validators: {
          notEmpty: {}
        }
      },
      
      PENCEGAHAN: {
        validators: {
          notEmpty: {}
        }
      },
      
      TINDAKAN: {
        validators: {
          notEmpty: {}
        }
      },

    }
  });
});
</script>
