
<section class="content-header">
  <h1>
     Set up pelaksanaan (Uji Proficiency SIG)
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">
			
            <div class="box-header">
              <form class="form-inline">
	          <?php if($this->PERM_WRITE): ?>
	            <div class="input-group input-group-sm" style="width: 150px; ">
	              <a href="<?php echo site_url("Uji_proficiency_setup/add");?>" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
	            </div>
	          <?PHP endif; ?>
	          </form>
          	  <hr/>
              
            </div>
           
            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap "
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th >No.</th>
					<th >Periode Proficiency</th>
					<th >Status </th>
	                <th >Commodity</th>
	                <th >Laboratorium</th>
	                <th >PIC</th>
                    <th >Aktivitas Terkini</th>
					<th >Create at</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
					<td><?= $dt->TITLE_PP;?> [<?= $dt->GROUP_PP;?> - <?= $dt->YEAR_PP;?>]</td>
					<td><?= $dt->STATUS;?></td>
                    <td><?= $dt->NAMA_SAMPLE;?> </td>
                    <td><?= $dt->NAMA_LAB;?> </td>
                    <td><?= $dt->FULLNAME;?> </td>
                    <td>
					<?php
						$fase = $this->setup->fase_proficiency($dt->ID_PROFICIENCY);
						echo $fase->ACTIVITY_NAME;
					?>
					</td>
					<td><?= $dt->CREATED_AT;?> </td>
                    <td> 
                    <?php if($this->PERM_WRITE): ?>
                      <a href="<?php echo site_url("Uji_proficiency_setup/edit/");?><?= $dt->ID_PROFICIENCY; ?>"><button title="Detail" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-arrows "></i> Detail</button></a>
                      <a href="<?php echo site_url("Uji_proficiency_setup/hapus/");?><?= $dt->ID_PROFICIENCY; ?>" onClick="return doconfirm();"><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button></a>
                    <?php endif; ?>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
	          </table>
            </div>
           
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>