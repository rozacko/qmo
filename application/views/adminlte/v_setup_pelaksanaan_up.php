
<section class="content-header">
  <h1>
     Set up pelaksanaan (Uji Proficiency Nasional / Internasional)
   <small></small>
  </h1>
</section>

 <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">

          <div class="box">
			
            <div class="box-header">
              <form class="form-inline">
	          <?php if($this->PERM_WRITE): ?>
	            <div class="input-group input-group-sm" style="width: 150px; ">
	              <a data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
	            </div>
	          <?PHP endif; ?>
	          </form>
          	  <hr/>
              
            </div>
           
            <div class="box-body">
              <table  id="dt_tables"
	            class="table table-striped table-bordered table-hover dt-responsive nowrap"
	            cellspacing="0"
	            width="100%">
	            <thead>
	              <tr>
	                <th >No.</th>
					<th >Periode Proficiency</th>
	                <th >Commodity</th>
					<th >PIC</th>
                    <th >Create at</th>
	                <th ></th>
	              </tr>
	            </thead>
                <tbody style="font-weight: normal;">
                <?php 
                  $count = 1;
                  foreach ($this->list_data as $dt) { ?>
                  <tr>
                    <td><?= $count++; ?></td>
					<td><?= $dt->TITLE_PP;?> [<?= $dt->GROUP_PP;?> - <?= $dt->YEAR_PP;?>]</td>
                    <td><?= $dt->NAMA_SAMPLE;?> </td>
					<td> <?= $dt->FULLNAME;?> </td>
                    <td><?= $dt->CREATED_AT;?> </td>
                    <td> 
                    <?php if($this->PERM_WRITE): ?>
						<a href="<?php echo site_url("setup_pelaksanaan_up/activity/") ?><?= $dt->ID_PROFICIENCY; ?>" ><button title="Act" class="btEdit btn btn-success btn-xs" type="button"><i class="fa fa-calendar"></i> Aktivitas Pelaksanaan</button></a>
                      <a data-toggle="modal" data-target="#editModal<?= $dt->ID_PROFICIENCY; ?>" ><button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button></a>
                      <a href="<?php echo site_url("setup_pelaksanaan_up/hapus/");?><?= $dt->ID_PROFICIENCY; ?>" onClick="return doconfirm();"><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button></a>
                    <?php endif; ?>
                    </td>
                  </tr>
				  
<!-- Modal edit  -->

<div id="editModal<?= $dt->ID_PROFICIENCY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("setup_pelaksanaan_up/do_edit") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Edit Set up pelaksanaan (Uji Proficiency Nasional / Internasional) :</b> </h4>
      </div>
      <div class="modal-body">
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-6 clearfix">
				  <input type="hidden" name="ID_PROFICIENCY" value="<?= $dt->ID_PROFICIENCY;?>" />
                  <label>Periode Proficiency</label>
					<select class="form-control select2" NAME="ID_PP"  placeholder="Select a Periode Proficiency ...">
						<option value="">Select a Periode Proficiency ...</option>
						<?php  foreach($this->list_periode as $dt_periode): ?>
						  <option value="<?php echo $dt_periode->ID_PP; ?>" <?php echo ($dt->ID_PRIODE == $dt_periode->ID_PP)?"SELECTED":""; ?> ><?= $dt_periode->TITLE_PP;?> [<?= $dt_periode->GROUP_PP;?> - <?= $dt_periode->YEAR_PP;?>]</option>
						<?php endforeach; ?>
					</select>
				  </div>
				  <div class="col-sm-6 ">
                    <label>Commodity</label>
					<select class="form-control select2" NAME="ID_KOMODITI" placeholder="Select a Commodity ...">
						<option value="">Select a Commodity ...</option>
						<?php  foreach($this->list_comodity as $dt_comodity): ?>
						  <option value="<?php echo $dt_comodity->ID_SAMPLE ?>" <?php echo ($dt->ID_KOMODITI == $dt_comodity->ID_SAMPLE)?"SELECTED":""; ?> >[<?=  $dt_comodity->KODE_SAMPLE ?>] <?php echo $dt_comodity->NAMA_SAMPLE ?></option>
						<?php endforeach; ?>
					</select> 
				  </div> 
				  <div class="col-sm-12 clearfix">
					<label>PIC</label>
					<select class="form-control select2" NAME="ID_PIC" id="ID_PIC" placeholder="Select a PIC ...">
					<option value="">Select a PIC ...</option>
					<?php  foreach($this->list_pic as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_USER ?>" <?php echo ($dt->ID_PIC == $dt_pic->ID_USER)?"SELECTED":""; ?> ><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
					<?php endforeach; ?>
					</select>     
					  
				  </div>   
				  
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>    
				  
                <?php } ?>
                </tbody>
	          </table>
            </div>
           
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->

<!-- Modal add  -->

<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("setup_pelaksanaan_up/create") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Set up pelaksanaan (Uji Proficiency Nasional / Internasional) :</b> </h4>
      </div>
      <div class="modal-body">
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-6 clearfix">
                  <label>Periode Proficiency</label>
					<select class="form-control select2" NAME="ID_PP"  placeholder="Select a Periode Proficiency ...">
						<option value="">Select a Periode Proficiency ...</option>
						<?php  foreach($this->list_periode as $dt_periode): ?>
						  <option value="<?php echo $dt_periode->ID_PP; ?>" <?php echo ($this->uri->segment(3) == $dt_periode->ID_PP)?"SELECTED":""; ?> ><?= $dt_periode->TITLE_PP;?> [<?= $dt_periode->GROUP_PP;?> - <?= $dt_periode->YEAR_PP;?>]</option>
						<?php endforeach; ?>
					</select>
				  </div>
				  <div class="col-sm-6 ">
                    <label>Commodity</label>
					<select class="form-control select2" NAME="ID_KOMODITI" placeholder="Select a Commodity ...">
						<option value="">Select a Commodity ...</option>
						<?php  foreach($this->list_comodity as $dt_comodity): ?>
						  <option value="<?php echo $dt_comodity->ID_SAMPLE ?>"  >[<?=  $dt_comodity->KODE_SAMPLE ?>] <?php echo $dt_comodity->NAMA_SAMPLE ?></option>
						<?php endforeach; ?>
					</select> 
				  </div> 
				  <div class="col-sm-12 clearfix">
				  <label>PIC</label>
					<select class="form-control select2" NAME="ID_PIC" id="ID_PIC" placeholder="Select a PIC ...">
					<option value="">Select a PIC ...</option>
					<?php  foreach($this->list_pic as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_USER ?>"  ><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
					<?php endforeach; ?>
					</select> 
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>    

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
				 
<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
$(document).ready(function(){
    $('select').selectize({
          sortField: 'text'
      });
	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>