<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quality Management Online | PT. SEMEN INDONESIA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/bootstrap/css/bootstrap.min.css"); ?>">
  <!-- Font Awesome -->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"-->
  <link href="<?php echo base_url("css/font-awesome.min.css"); ?>" rel="stylesheet">

  <!-- Ionicons -->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"-->
  <link href="<?php echo base_url("css/ionicons.min.css"); ?>" rel="stylesheet">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/AdminLTE.css"); ?>">

  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect.
  -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/skins/skin-red-light.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("css/animation.css"); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="<?php echo base_url('js/html5shiv/html5shiv.min.js'); ?>"></script>
  <script src="<?php echo base_url('js/respond/respond.min.js'); ?>"></script>
  <![endif]-->

  <script src="<?php echo base_url("templates/adminlte/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>

  <script src="<?php echo base_url("templates/adminlte/bootstrap/js/bootstrap.min.js"); ?>" ></script>

  <script src="<?php echo base_url("js/function.js"); ?>" ></script>

<!-- CSS External-->
<?php
foreach ($this->cssExt as $k => $v) {
    echo '<link href="' . base_url('assets/' . $v) . '" rel="stylesheet">';
}
?>

<style type="text/css">


  .modal_load {
    display:    none;
    position:   fixed;
    z-index:    +9999999999999999999999;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 )
          url('<?php echo base_url("images/ajax-loader-modal.gif"); ?>')
          50% 50%
        no-repeat;
  }
  body.loading {
      overflow: hidden;
  }
  body.loading .modal_load {
      display: block;
  }
  .pointer{
    cursor: pointer
  }
  .readonly{
    background-color: !important #fff;
  }

  /*SKIN LAYOUT---------------------------*/

  .skin-red-light .main-sidebar, .skin-red-light .left-side {
    background: linear-gradient( #2b2625, rgba(255, 0, 0, 0.45) ), url(https://demos.creative-tim.com/material-dashboard-pro-angular2/assets/img/sidebar-1.jpg);
    /* color: #fff; */
    background-size: cover;
  }
  .skin-red-light .sidebar-menu>li:hover>a, .skin-red-light .sidebar-menu>li.active>a {
    color: #fff;
    font-weight: bold;
    background: none;
  }
  .skin-red-light .treeview-menu>li.active>a, .skin-red-light .treeview-menu>li>a:hover {
    color: #fff;
    box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(60,72,88,.4);
    background: #fff;
    color: #9b2422;
    border-radius: 5px 0 0 5px;
    font-weight: bold;
  }
  .skin-red-light .sidebar-menu>li>.treeview-menu {
    background: none;
  }
  .skin-red-light .treeview-menu>li>a {
    color: #ecf0f5;
    font-weight: normal;
  }
  .skin-red-light .sidebar a {
    color: #fff;
  }
  .skin-red-light .main-header .navbar {
      background-color: #a12322;
  }
  .skin-red-light .main-header .logo {
    background-color: #861b1a;
  }

  /*BOX STYLE-------------------------------*/
  .box {
    box-shadow: 0 1px 4px 0 rgba(0,0,0,.14);
  }

  /*BOX TIMELINE----------------------------*/
  .timeline > li > .timeline-item {
    -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    border-radius: 3px;
    margin-top: 0;
    background: #fff;
    color: #444;
    margin-left: 60px;
    margin-right: 15px;
    padding: 0;
    position: relative;
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 20px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    font-weight: normal;
}

.timeline > li > .timeline-item > .timeline-header {
    line-height: 2.1;
}

.sidebar-menu ul li {
  padding: 5px 0 0 0;
}

.select2{
  width: 100%;
} 
</style>


</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-red-light sidebar-mini">
  <div class="modal_load"></div>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo btnMenu">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Quality Management</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu" style="margin: 0 15px 0 0;">
            <!-- <h4 style="color: #fff; padding-top: 3px; margin-right: 30px;">Welcome Back <span id="userNameLogin"><?php echo $this->USER->FULLNAME ?></span></h4> -->
        <ul class="nav navbar-nav">

              <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                  Welcome back, <span id="userNameLogin"><?php echo $this->USER->FULLNAME ?></span> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(60,72,88,.4);">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Setting</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?=site_url("logout")?>">Sign Out</a></li>
                </ul>
              </li>
        </ul>
      </div>
    </nav>



  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">


      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
       <!-- <li class="header">Quality Management</li> -->
        <!-- Optionally, you can add icons to the links -->
        <?php        
          foreach ($menulist as $i => $v) {

            $g_activated = '';
            if($v['ACTIVATED'] == 'Y'){
              $g_activated = 'active menu-open';
            }    

                echo '
                <li class="treeview '.$g_activated.'">
                <a href="#">
                  <i class="'.$v['ICON'].'"></i> <span>'.$v['NM_GROUPMENU'].'</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>';

                if(isset($v['SUBMENU'])){
                  echo '<ul class="treeview-menu">';
                  foreach($v['SUBMENU'] as $j => $jv){

                    $sg_activated = '';
                    if($jv['ACTIVATED'] == 'Y'){
                      $sg_activated = 'active menu-open';
                    }        
      
                      if(isset($jv['SUBMENU'])){
                        echo '<li class="treeview '.$sg_activated.'">
                        <a href="'.$jv['PATH'].'"><i class="'.$jv['ICON'].'"></i> '.$jv['NM_MENU'].'
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu">';
    
                          foreach($jv['SUBMENU'] as $k => $kv){
                            $m_activated = '';
                            if($kv['ACTIVATED'] == 'Y'){
                              $m_activated = 'active menu-open';
                            }    

                            echo '<li class="'.$m_activated.'"><a href="'.$kv['PATH'].'"><i class="'.$jv['ICON'].'"></i> '.$kv['NM_MENU'].'</a></li>';
                          }
                        echo '</ul></li>';
                      }else{
                        echo '<li class="'.$sg_activated.'"><a href="'.$jv['PATH'].'"><i class="'.$jv['ICON'].'"></i> '.$jv['NM_MENU'].'</a></li>';
                      }
                  }
                  echo '</ul>';

                }

              echo '</li>
                ';

          }
          ?>
        <!-- <li class="header">--</li> -->
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	<?PHP if ($this->AKTIF_MENU): ?>
	<ol class="breadcrumb">
        <li><i class="fa fa-directory"></i><a href="#"><?php echo $this->AKTIF_GROUPMENU ?></a></li>
        <li class="active"><a href="#"><?php echo $this->AKTIF_MENU ?></a></li>
        <?PHP if ($this->AKTIF_ACTION): ?>
        <li class="active"><a href="#"><?php echo $this->AKTIF_ACTION ?></a></li>
        <?php endif;?>
    </ol>
    <?php endif;?>
    <style>
		.breadcrumb {
			float: right;
		}

    </style>
