   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ACCESS LOG STATISTIC
        <small></small>
      </h1>

    </section>
<style type="text/css">
  .info-box-icon {
    border-top-left-radius: 2px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 2px;
    display: block;
    float: left;
    height: 70px;
    width: 70px;
    text-align: center;
    font-size: 20px;
    line-height: 70px;
    background: rgba(0, 0, 0, 0.2);
}
.info-box {
    min-height: 70px;
}
.progress-description, .info-box-text {
    display: block;
    font-size: 11px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.info-box small {
    font-size: 10px;
}
table, th, td {
    width: 100%;
            border: solid 1px #ddd;
            border-collapse: collapse;
            padding: 2px 3px;
            text-align: center;
        }
        td {
            font-weight:normal;
        }
        th {
            background-color: #fff;
        }
</style>
<style>

.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#DashboardKetepatan {
    height: 400px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <?php  foreach($this->list_company as $company): ?>
          <div class="col-md-4 col-sm-6 col-xs-12" style="width: 33.3%">
            <div class="info-box">
              <span class="info-box-icon" style="background-color:<?php echo company_color($company->KD_COMPANY);?>">
			  
			   <?php if($company->LOGO == null or $company->LOGO == ''){ ?>
					<img src="<?= base_url('assets/uploads/profile/company-img_default-logo.jpg');?>" width="70" height="66">
				<?php } else { ?> 
					<img src="<?= base_url($company->LOGO);?>" width="70" height="66">
				<?php } ?>
                
				
                <?php
                // echo $company->ID_COMPANY;
				/*
                switch ($company->KD_COMPANY) {
                  case 3000: //Padang
                      echo '<img src="'.base_url("images/padang.png").'" width="70" height="66">';
                      break;
                  case 4000: //Tonasa
                      echo '<img src="'.base_url("images/tonasa.png").'" width="70" height="66">';
                      break;
                  case 5000: //Semen Gresik
                      echo '<img src="'.base_url("images/gresik.png").'" width="70" height="66">';
                      break;
                  case 6000: // TLCCC
                      echo '<img src="'.base_url("images/tanglong.png").'" width="70" height="66">';
                      break;
                  case 7000: //SIBU
                      echo '<img src="'.base_url("images/Semenindonesia.png").'" width="70" height="66">';
                      break;
                   case 7001: //SBI
                      echo '<img src="'.base_url("images/sbi_logo.png").'" width="70" height="66">';
                      break;
					case 7002: //SBA
                      echo '<img src="'.base_url("images/sba_logo.png").'" width="70" height="66">';
                      break;
                   
                  default:
					  echo '<img src="'.base_url("images/Semenindonesia.png").'" width="70" height="66">';
                     
              }
*/
                ?>
                

              </span>

              <div class="info-box-content">
                <span class="info-box-text"><?php echo $company->NM_COMPANY;?></span>
                <span class="info-box-number"><span id="hit_<?php echo $company->ID_COMPANY;?>">0</span></span>
                <span class="info-box-text"><small>on <?php echo date('F');?></small></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        <?php endforeach; ?>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Monthly Report</h3>
            </div>
            <div class="box-body">
              <div >
                <div id="divTable" >
                  <div id="boxPlot_div" class="boxPlot_div"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
		
		<div class="col-md-4">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">The Most Fastest and Complete Input</h3>
            </div>
            <div class="box-body" style="height: 430px">
				<figure class="highcharts-figure">
				<div id="tmf_ci"></div>
				</figure>
			</div>
          </div>
        </div>
		
		 <div class="col-md-4">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Ketepatan Waktu Upload Dokumen</h3>
            </div>
            <div class="box-body" style="height: 430px">
			
				<figure class="highcharts-figure grafik_header">
					<div id="DashboardKetepatan"></div> 
				</figure>
				<div class='pull-right'><button class="btn-warning" name="kembali" onClick="kembali()" id="kembali">Back</button> &nbsp;  </div>
				<figure class="highcharts-figure grafik_detail">
					<div id="DashboardKetepatanDetail"></div> 
				</figure>
            </div>
          </div>
        </div>
		
		 <div class="col-md-4" style="display: none;">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">The Most Fastest and Complete Input</h3>
            </div>
            <div class="box-body" style="height: 430px">
              <p style="text-align: center;">On <?php echo date('F Y');?></p>
              <div id="showData">
               
              </div>
            </div>
          </div>
        </div>
		
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Top 5 User QA</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php foreach ($this->topGroupmenu as $key => $groupmenu): ?>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product">
                      <span class="label label-warning pull-right"><?php echo $groupmenu->JML_AKSES; ?></span>
                      <span class="product-description">
                        <?php echo $groupmenu->RNUM . ". " . $groupmenu->FULLNAME; ?>
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Top 5 User QC</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php foreach ($this->topMenu as $key => $groupmenu): ?>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product">
                      <span class="label label-warning pull-right"><?php echo $groupmenu->JML_AKSES; ?></span>
                      <span class="product-description">
                        <?php echo $groupmenu->RNUM . ". " . $groupmenu->FULLNAME; ?>
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Top 5 User General (Non QA & QC)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                <?php foreach ($this->topUserList as $key => $user): ?>
                  <!-- /.item -->
                  <li class="item">
                    <div class="product">
                      <span class="label label-warning pull-right"><?php echo $user->JML_AKSES; ?></span>
                      <span class="product-description">
                        <?php echo $user->RNUM . ". " . $user->FULLNAME; ?>
                      </span>
                    </div>
                  </li>
                  <!-- /.item -->
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>

        

     
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Report User</h3>
            </div>
            <div class="box-body">
              
                <div id="divTable" >
                  <?php 
                    foreach($this->list_company as $company) { 
                        if($company->LOGO == null or $company->LOGO == ''){ ?>
							<div class="col-sm-6">
							<img src="<?= base_url('assets/uploads/profile/company-img_default-logo.jpg');?>" width="70" height="66">
					<?php } else { ?> 
							<div class="col-sm-6">
							<img src="<?= base_url($company->LOGO);?>" width="70" height="66">
					<?php } ?>
					

				 <?php 
                      // echo $i;
                // echo $company->ID_COMPANY;
				/*
                        switch ($company->KD_COMPANY) {
                           case 3000: //Padang
                      echo '<img src="'.base_url("images/padang.png").'" width="70" height="66">';
                      break;
                  case 4000: //Tonasa
                      echo '<img src="'.base_url("images/tonasa.png").'" width="70" height="66">';
                      break;
                  case 5000: //Semen Gresik
                      echo '<img src="'.base_url("images/gresik.png").'" width="70" height="66">';
                      break;
                  case 6000: // TLCCC
                      echo '<img src="'.base_url("images/tanglong.png").'" width="70" height="66">';
                      break;
                  case 7000: //SIBU
                      echo '<img src="'.base_url("images/Semenindonesia.png").'" width="70" height="66">';
                      break;
                   case 7001: //SBI
                      echo '<img src="'.base_url("images/sbi_logo.png").'" width="70" height="66">';
                      break;
					case 7002: //SBA
                      echo '<img src="'.base_url("images/sba_logo.png").'" width="70" height="66">';
                      break;
                   
                  default:
					  echo '<img src="'.base_url("images/Semenindonesia.png").'" width="70" height="66">';
                      }
					 */
                     ?>
					 <div id="boxPlot_div_user<?=$company->ID_COMPANY;?>" class="boxPlot_div"></div><hr>
                     </div>
				 <?php 
                    }
					
                  ?>
                </div>
              
            </div>
          </div>
        </div>
        
        <?php if($this->USER->ID_USERGROUP == '1'){ ?>
        <div class="col-md-12">
          <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
              <ul class="products-list product-list-in-box">
                
                <a href="<?php echo site_url("access_log/reset_log") ?>"> Reset Statistic</a>

              </ul>
            </div>
          </div>
        </div>
      <?php } ?>
     
      </div>
    </section>
    <!-- /.content -->

<style type="text/css">
  .boxPlot_div { margin:auto;margin-bottom:10px; }
</style>

<!-- picker css | js -->
<link href="<?php echo base_url("plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>

<script src="<?php echo base_url("plugins/highcharts/highcharts.js");?>"/></script> 
<script src="<?php echo base_url("plugins/highcharts/exporting.js");?>"/></script> 
<script src="<?php echo base_url("plugins/highcharts/export-data.js");?>"/></script> 
<script src="<?php echo base_url("plugins/highcharts/highcharts.js");?>"/></script>

<script>

$(document).ready(function(){
	
	$(".grafik_detail, #kembali").hide()
	  
	  load_ketepatan()
	   
  qmodash();
    
  $("#pd_start").datetimepicker({format: 'dd/mm/yyyy hh:ii:ss', autoclose: true, minView: 0});
  $("#pd_end").datetimepicker({format: 'dd/mm/yyyy hh:ii:ss', autoclose: true, minView: 0});
  get_opco_hit();
  get_opco_hit_month();
  // for(var i = 0; i<=4; i++){
    // alert(i);
  <?php  foreach($this->list_company as $company){ ?>
    get_opco_hit_user('<?php echo $company->ID_COMPANY;?>');
  <?php } ?>
  
});

function qmodash(){
    $.ajax({
      type:"GET",
      url:"<?php echo site_url("h_report_score/qmodashboard") ?>",
      dataType: "json",   
      success:function(data_in){
          //console.log(data_in);
          CreateTableFromJSON(data_in);
		  makegrafqmo(data_in);
      },
      error: function (xhr, ajaxOptions, thrownError) { 
        lert(xhr.responseText);
      }
    });
}

function CreateTableFromJSON(myBooks) {
        // EXTRACT VALUE FOR HTML HEADER. 
        var col = [];
        for (var i = 0; i < myBooks.length; i++) {
            for (var key in myBooks[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }
        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.
        var tr = table.insertRow(-1);                   // TABLE ROW.
        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }
        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < myBooks.length; i++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = myBooks[i][col[j]];
            }
        }
        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
}

function get_opco_hit() {
  $.ajax({
    type: "POST",
    dataType: "json",
    data: {
      string: ''
    },
    url: '<?php echo site_url("dashboard/opco_hit");?>',
    success: function(res){
      $(res).each(function(idx, elm) {
        // console.log(elm);
        total = 0;
        if(elm.JML_AKSES){
          total = elm.JML_AKSES;
        }
        $("#hit_" + elm.ID_COMPANY).text(total);
      });

    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("XHR: " + JSON.stringify(jqXHR));
      console.log("Status: " + textStatus);
      console.log("Error: " + errorThrown);
    }
  });
}

function get_opco_hit_month() {
  $.ajax({
    type: "POST",
    dataType: "json",
    data: {
      string: ''
    },
    url: '<?php echo site_url("dashboard/opco_hit/month");?>',
    success: function(res){
      // console.log(res);
      BoxPlot = document.getElementById('boxPlot_div');
      Plotly.purge(BoxPlot);
      Plotly.plot(BoxPlot, {
        data: res.data,
        layout: res.layout
      });
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("XHR: " + JSON.stringify(jqXHR));
      console.log("Status: " + textStatus);
      console.log("Error: " + errorThrown);
    }
  });
}

function get_opco_hit_user(urut) {
  // alert('boxPlot_div_user'+urut);
  var url = 'opco_hit_user'+urut;
  $.ajax({
    type: "POST",
    dataType: "json",
    data: {
      string: '',
      company:urut
    },
    url: '<?php echo site_url("dashboard/opco_hit_user/month");?>',
    success: function(res){
      // console.log(res);
      BoxPlot = document.getElementById('boxPlot_div_user'+urut);
      Plotly.purge(BoxPlot);
      Plotly.plot(BoxPlot, {
        data: res.data,
        layout: res.layout
      });
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log("XHR: " + JSON.stringify(jqXHR));
      console.log("Status: " + textStatus);
      console.log("Error: " + errorThrown);
    }
  });
}



	  function load_ketepatan(){
		  
      //Set table columns | Update setting
      $.post('<?php echo site_url("dashboard/dashboard_waktu");?>', function (result) {
			Highcharts.setOptions({
				colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
			});
			Highcharts.chart('DashboardKetepatan', {
			chart: {
				 height: 400,
				type: 'column', 
					events: {
						load: function() {
							Highcharts.each(this.series[0].points, function(p) {
								console.log("ssss")
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							}),
							
							Highcharts.each(this.series[1].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							});
						}, 
					} 
			},
			title: {
				text: ' Ketepatan Waktu Upload Dokumen'
			},
			subtitle: {
				text: result.bulan_tahun
			},
			xAxis: {
				categories: result.header,
				crosshair: true
			},
			yAxis: {
				min: 0,
				max: 5,
				title: {
					text: 'Skor Si Hebat'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				},
				  series: {
						dataLabels: {
							enabled: true,
							format: '{point.y}'
						},
						cursor: 'pointer',
						point: {
							events: {
								click: function () {
									load_detail(this.category);
								}
							}
						}
					}
			},
			
			series: result.data,
			
			 
		});
		
      },"json");
	  }
	  
	  
	  function load_detail(nm_group_plant){
		  
      //Set table columns | Update setting 
      $.post('<?php echo site_url("dashboard/dashboard_waktu_detail");?>',{'nm_group_plant' : nm_group_plant}, function (result) {
			Highcharts.setOptions({
				colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
			});
			Highcharts.chart('DashboardKetepatanDetail', {
			chart: {
				 height: 400,
				type: 'column', 
					events: {
						load: function() {
							Highcharts.each(this.series[0].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							}),
							
							Highcharts.each(this.series[1].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							});
						}, 
					} 
			},
			title: {
				text: ' Ketepatan Waktu'
			},
			subtitle: {
				text: ' Upload Dokumen'
			},
			xAxis: {
				categories: result.header,
				crosshair: true
			},
			yAxis: {
				min: 0,
				max: 5,
				title: {
					text: 'Skor Si Hebat'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}, 
			},
			
			series: result.data,
			
			 
		});
		
      },"json");
	  
	  $(".grafik_header").hide()
	  $(".grafik_detail, #kembali").show()
	  }
	  
	  function kembali(){
		  
	  $(".grafik_header").show()
	  $(".grafik_detail, #kembali").hide()
	  }
</script>

<script>
function makegrafqmo(datanya){
	//console.log(datanya.COMPANY);
		var comp = [];
		var posisi = [];
		var skor = [];
        for (var i = 0; i < datanya.length; i++) {
            comp.push(datanya[i]['COMPANY']);
			posisi.push(datanya[i]['POSISI']);
			skor.push(datanya[i]['SKOR']);
        }
	console.log(comp);
	
	Highcharts.chart('tmf_ci', {
	  chart: {
		type: 'bar'
	  },
	  title: {
		text: 'The Most Fastest and Complete Input'
	  },
	  subtitle: {
		text: '<?= date('F Y');?>'
	  },
	  xAxis: {
		categories: comp,
		title: {
		  text: null
		}
	  },
	  yAxis: {
		min: 0,
		title: {
		  text: 'Position / Score',
		  align: 'high'
		},
		labels: {
		  overflow: 'justify'
		}
	  },
	  tooltip: {
		valueSuffix: ''
	  },
	  plotOptions: {
		bar: {
		  dataLabels: {
			enabled: true
		  }
		}
	  },
	  legend: {
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'bottom',
		x: 0,
		y: -50,
		floating: true,
		borderWidth: 1,
		backgroundColor:
		  Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
		shadow: true
	  },
	  credits: {
		enabled: false
	  },
	  series: [{
		color: '#686de0',
		name: 'Position',
		data: posisi
	  }, {
		color: '#30336b',
		name: 'Score',
		data: skor
	  }]
	});
}
</script>
