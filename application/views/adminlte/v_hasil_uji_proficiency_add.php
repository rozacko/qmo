<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style>
    #form_uji {
        margin-bottom:12px;
    }
</style>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
                    <center><b><h3>FORM PENGUJIAN <?= strtoupper($this->proficiency->TITLE_PP.' '.$this->proficiency->NAMA_SAMPLE); ?> <br/> TAHUN  <?= $this->proficiency->YEAR_PP; ?></h3></b></center>
                </div>
				<div class="box-body">
                <?php 
                    $analis = $this->input->get('analis');
                    $approver = $this->input->get('approver');

                    $readonly = ($approver == "Y" ? "disabled='disabled'" : "");
                ?>
                    <form action="<?= base_url('hasil_uji_proficiency/action_input/').$this->input->get('proficiency'); ?>" method="POST" encytpe="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12" id="form_uji">
                                <p>
                                    Mohon diisi dengan pembulatan yang sesuai pada tiap pengujian. Pengujian dilakukan DUPLO dan dilaporkan keduanya pada kolom uji ke 1 dan uji ke 2.
                                    <br/>
                                    <li>Silahkan Kosongkan pada Pengujian yang tidak ikut serta.</li>
                                    <li>Ketidakpastian tanpa penulisan plus-minus (+/-)</li>
                                    <li>Format penulisan koma menggunakan titik dan tanpa ditulis satuannya.<br/>Contoh : Nilai uji 0,25% ditulis 0.25</li>
                                    <br/>* Required (Harus diisi)
                                </p><br/>
                                <div class="col-md-1"></div>
                                <div class="col-md-4">1. Lab Penguji *</div>
                                <div class="col-md-3">
                                    <?php
                                        $role = '';
                                        foreach($this->session->userdata()['ROLE'] as $r){
                                            $role .= $r['NM_USERGROUP'].",";
                                        }
                                    ?>
                                    <input type="hidden" id="role" value="<?= $role ?>" />
                                    <select class="form-control" name="ID_LABORATORIUM" id="ID_LABORATORIUM" <?= $readonly ?> required>
                                        <option>Pilih Lab Penguji</option>
                                        <?php foreach($this->list_lab as $lb){ ?>
                                        <option value="<?= $lb->ID_LAB ?>" <?= ($analis != "" ? $lb->ID_LAB == $this->dtl_hasil_uji[0]['ID_LABORATORIUM'] ? "selected" : "" : "") ?>><?= $lb->NAMA_LAB ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" id="form_uji">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">2. Analis/Penguji *</div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="ANALIS" id="ANALIS" value="<?= ($analis != "" ? $this->dtl_hasil_uji[0]['ANALIS'] : "") ?>" <?= $readonly ?>  required />
                                    <?php if($this->input->get("analis") != ""){ ?>
                                        <input type="hidden" class="form-control" name="ID_HASIL_UJI" id="ID_HASIL_UJI" value="<?= $this->dtl_hasil_uji[0]['ID_HASIL_UJI']; ?>" required />
                                    <?php } ?>
                                </div>
                            </div>&nbsp;<hr/>&nbsp;
                            <?php
                               
                                if(count($this->list_pertanyaan) > 0){
                                    $i = 3; foreach($this->list_pertanyaan as $p){?>
                                <div class="col-md-12" id="form_uji">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-4"><?= $i.". ".$p->QUEST_TEXT.($p->IS_REQUIRED == "Y" ? "*" : "") ?></div>
                                    <div class="col-md-3">
                                        <input type="hidden" class="form-control" name="QUESTION[]" value="<?= $p->ID_QUESTION ?>" />
                                        <?php if($this->input->get("analis") != ""){ ?>
                                            <input type="hidden" class="form-control" name="ID_DTL_HASIL_UJI[]" value="<?= $p->ID_DTL_HASIL_UJI ?>" />
                                        <?php } ?>
                                        <input type="hidden" class="form-control" name="QUEST_TYPE[]" value="<?= $p->QUEST_TYPE ?>" />
                                        <?php if($p->QUEST_TYPE == "TEXT"){ ?>
                                        <input type="text" class="form-control" name="ANSWER[]" value="<?= ($analis != "" ? $p->ANSWER : "") ?>" <?= ($p->IS_REQUIRED == "Y" ? "required" : "")  ?> <?= $readonly ?> />
                                        <?php } else if($p->QUEST_TYPE == "FILE"){ ?>
                                        <input type="file" class="form-control" name="ANSWER[]" <?= ($p->IS_REQUIRED == "Y" ? "required" : "")  ?> <?= $readonly ?> />
                                        <?php if($p->ANSWER != ''){ ?>
                                            <a href="<?= base_url().'assets/uploads/proficiency/'.$p->ANSWER ?>" target="_BLANK"><?= $p->ANSWER ?></a>
                                        <?php } ?>
                                        <?php } else if($p->QUEST_TYPE == "OPTION"){ ?>
                                        <select class="form-control" name="ANSWER[]" <?= ($p->IS_REQUIRED == "Y" ? "required" : "")  ?> <?= $readonly ?>>
                                            <option>Pilih Opsi</option>
                                            <?php
                                                $option = explode(";", $p->QUEST_OPTIONAL);
                                                foreach($option as $opt){ ?>
                                                <option value="<?= $opt ?>"><?= $opt ?></option>
                                            <?php }?>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php 
                                $i++; }
                                } else {  ?>
                            <div class="col-md-12">
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_kimia" data-toggle="tab">Pengujian Kimia</a></li>
                                        <li><a href="#tab_fisika" data-toggle="tab">Pengujian Fisika</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_kimia">
                                            <div class="row">
                                                <?php $ki = 1; foreach($this->list_pertanyaan_kimia as $k){ ?>
                                                    <div class="col-md-12" id="form_uji">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-4"><?= $ki++.". ".$k->QUEST_TEXT.($k->IS_REQUIRED == "Y" ? "*" : "") ?></div>
                                                        <div class="col-md-3">
                                                            <input type="hidden" class="form-control" name="QUESTION[]" value="<?= $k->ID_QUESTION ?>" />
                                                            <input type="hidden" class="form-control" name="QUEST_TYPE[]" value="<?= $k->QUEST_TYPE ?>" />
                                                            <?php if($k->QUEST_TYPE == "TEXT"){ ?>
                                                            <input type="text" class="form-control" name="ANSWER[]" <?= ($k->IS_REQUIRED == "Y" ? "required" : "")  ?> />
                                                            <?php } else if($k->QUEST_TYPE == "FILE"){ ?>
                                                            <input type="file" class="form-control" name="ANSWER[]" <?= ($k->IS_REQUIRED == "Y" ? "required" : "")  ?> />
                                                            <?php } else if($k->QUEST_TYPE == "OPTION"){ ?>
                                                            <select class="form-control" name="ANSWER[]" <?= ($k->IS_REQUIRED == "Y" ? "required" : "")  ?>>
                                                                <option>Pilih Opsi</option>
                                                                <?php
                                                                    $option = explode(";", $k->QUEST_OPTIONAL);
                                                                    foreach($option as $opt){ ?>
                                                                    <option value="<?= $opt ?>"><?= $opt ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_fisika">
                                            <div class="row">
                                                <?php $fi = 1; foreach($this->list_pertanyaan_fisika as $f){ ?>
                                                    <div class="col-md-12" id="form_uji">
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-4"><?= $fi++.". ".$f->QUEST_TEXT.($f->IS_REQUIRED == "Y" ? "*" : "") ?></div>
                                                        <div class="col-md-3">
                                                            <input type="hidden" class="form-control" name="QUESTION[]" value="<?= $f->ID_QUESTION ?>" />
                                                            <input type="hidden" class="form-control" name="QUEST_TYPE[]" value="<?= $f->QUEST_TYPE ?>" />
                                                            <?php if($f->QUEST_TYPE == "TEXT"){ ?>
                                                            <input type="text" class="form-control" name="ANSWER[]" <?= ($f->IS_REQUIRED == "Y" ? "required" : "")  ?> />
                                                            <?php } else if($f->QUEST_TYPE == "FILE"){ ?>
                                                            <input type="file" class="form-control" name="ANSWER[]" <?= ($f->IS_REQUIRED == "Y" ? "required" : "")  ?> />
                                                            <?php } else if($f->QUEST_TYPE == "OPTION"){ ?>
                                                            <select class="form-control" name="ANSWER[]" <?= ($f->IS_REQUIRED == "Y" ? "required" : "")  ?>>
                                                                <option>Pilih Opsi</option>
                                                                <?php
                                                                    $option = explode(";", $f->QUEST_OPTIONAL);
                                                                    foreach($option as $opt){ ?>
                                                                    <option value="<?= $opt ?>"><?= $opt ?></option>
                                                                <?php }?>
                                                            </select>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="container">
                                <?php if($approver != "Y"){ ?>
                                <div class="col-md-6">
                                    <button type="button" id="btn-save" class="btn btn-block btn-sm btn-info">SUBMIT</button>
                                </div>
                                <div class="col-md-6">
                                    <button type="button" id="btn-save-draft" class="btn btn-block btn-sm btn-primary">SAVE AS DRAFT</button>
                                </div>
                                <?php } else { ?>
                                    <div class="col-md-6">
                                        <button type="button" id="btn-approver-y" class="btn btn-block btn-sm btn-primary">APPROVE</button>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" id="btn-approver-n" class="btn btn-block btn-sm btn-danger">REJECT</button>
                                    </div>
                                <?php } ?>
                            </div>
                            
                        </div>

                        <!-- MODAL SELECT APPROVAL -->
                        <div class="modal modal-default fade" id="modal-form">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">APPROVAL HASIL UJI</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" method="POST" >
                                            <div class="box-body" style="background-color:#c5d5ea;">
                                                <div class="form-group">
                                                    <div class="col-sm-12 clearfix">
                                                    <label>MANAGER APPROVAL *</label><br/>&nbsp;
                                                    <select class="form-control select2" NAME="APPROVER" id="APPROVER" placeholder="Select Approver ..." style="width:100%">
                                                        <option value="">Select a Approver ...</option>
                                                        <?php foreach($this->list_approver as $dt_appr): ?>
                                                        <option value="<?php echo $dt_appr->ID_USER ?>" data-fullname="<?= $dt_appr->FULLNAME ?>" data-email="<?= $dt_appr->EMAIL ?>" <?= $this->dtl_hasil_uji[0]['APPROVER'] == $dt_appr->ID_USER ? "selected" : ""  ?>><?php echo "<b>".$dt_appr->FULLNAME."</b> [".$dt_appr->EMAIL."]" ?></option>
                                                        <?php endforeach; ?>
                                                    </select> <br/>
                                                    <sub>Silahkan pilih Atasan/Manager terkait untuk proses approval dari input hasil uji proficiency</sub>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-sm btn-info">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODAL SELECT APPROVAL -->

                        <!-- MODAL ACTION APPROVAL -->
                        <div class="modal modal-default fade" id="modal-approver">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">NOTE APPROVER</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form role="form" method="POST" >
                                            <div class="box-body" style="background-color:#c5d5ea;">
                                                <div class="form-group">
                                                    <div class="col-sm-12 clearfix">
                                                    <input type="hidden" name="APPROVE_VALUE" id="APPROVE_VALUE" />
                                                    <textarea name="APPROVE_NOTE" id="APPROVE_NOTE" class="form-control" rows="5"></textarea>
                                                    <sub>Tuliskan catatan dari hasil validasi input hasil uji proficiency</sub>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="box-footer">
                                                <button type="button" class="btn btn-sm btn-warning" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-sm" id="btn-action-approver"></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END MODAL ACTION APPROVAL -->

                    </form>
				</div>
			</div>
		</div>
	</div>

</section>
<script>
    $(document).ready(function(){
        
    });

    $(document).on("click", "#btn-save-draft", function(e){
        e.preventDefault();
        var form_data = new FormData($("form")[0]);
        form_data.append("STATUS", "DRAFT");

        $.ajax({
            processData: false,
            contentType: false,
			url : "<?= base_url('hasil_uji_proficiency/action_input/').$this->input->get('proficiency').'/'.$this->input->get('penerima'); ?>",
			type: "POST",
			data : form_data
		}).done(function(response){
			if(response == 1){
                alert("Input Draft Hasil Uji Successfully");
                window.location = "<?= base_url('hasil_uji_proficiency/view').'?proficiency='.$this->input->get('proficiency').'&komoditi='.$this->input->get('komoditi') ?>"
			} else {
                alert("Input Hasil Uji Failed");
			}
		});
    });

    $(document).on("click", "#btn-save", function(e){
        e.preventDefault();
        var isFile = $('input[name="ANSWER[]"]:file').val();
        var roles = $("#role").val();
        if(roles.includes('ADMINISTRATOR')){
            var form_data = new FormData($('form')[0]);
            form_data.append("STATUS", "APPROVED");
            $.ajax({
                processData: false,
                contentType: false,
                url : "<?= base_url('hasil_uji_proficiency/action_input/').$this->input->get('proficiency').'/'.$this->input->get('penerima'); ?>",
                type: "POST",
                data : form_data
            }).done(function(response){
                if(response == 1){
                    alert("Input Hasil Uji Successfully");
                    window.location = "<?= base_url('hasil_uji_proficiency/view').'?proficiency='.$this->input->get('proficiency').'&komoditi='.$this->input->get('komoditi') ?>"
                } else {
                    alert("Input Hasil Uji Failed");
                }
            });
        } else {
            $("#modal-form").modal("show");
            $("#APPROVER").select2();
        }
    });

    $(document).on("click", "#btn-approver-y", function(e){
        e.preventDefault();
        $("#APPROVE_VALUE").val("Y");

        $("#btn-action-approver").html("Approve");
        $("#btn-action-approver").addClass("btn-primary");
        $("#btn-action-approver").removeClass("btn-danger");
        $("#modal-approver").modal("show");
    });

    $(document).on("click", "#btn-approver-n", function(e){
        e.preventDefault();
        $("#APPROVE_VALUE").val("N");
        
        $("#btn-action-approver").html("Reject");
        $("#btn-action-approver").addClass("btn-danger");
        $("#btn-action-approver").removeClass("btn-primary");
        $("#modal-approver").modal("show");
    });

    $("form").on("submit", function(e) {
		e.preventDefault();
        
		var request_method = $(this).attr("method");
        var form_data = new FormData(this);
        
        <?php if($this->input->get('approver') != ""){ ?>
            form_data.append("ID_LABORATORIUM", $("#ID_LABORATORIUM").val());
            form_data.append("ANALIS", $("#ANALIS").val());
            form_data.append("ID_HASIL_UJI", $("#ID_HASIL_UJI").val());
        <?php } else { ?>
            form_data.append("STATUS", "WAITING APPROVAL");
        <?php } ?>
		
		$.ajax({
            processData: false,
            contentType: false,
			url : "<?= base_url('hasil_uji_proficiency/action_input/').$this->input->get('proficiency').'/'.$this->input->get('penerima'); ?>",
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
                alert("Input Hasil Uji Successfully");
                window.location = "<?= base_url('hasil_uji_proficiency/view').'?proficiency='.$this->input->get('proficiency').'&komoditi='.$this->input->get('komoditi') ?>"
			} else {
                alert("Input Hasil Uji Failed");
			}
		});
	});
</script>