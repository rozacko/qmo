<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>TIS Excel Config</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-xs-12">    
            <div class="box">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">List Config Excel TIS</h3>
                    </div>
                    <div class="box-body">
                        <a href="<?= base_url('tis_excel_config/add') ?>" class="btn btn-sm btn-info">Tambah Config</a><br/>&nbsp;
                        <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">No.</th>
                                    <th width="*">PLANT</th>
                                    <th width="*">TYPE</th>
                                    <th width="*">URL</th>
                                    <th width="10%">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1; foreach($this->dataconfig as $c){ ?>
                                <tr>
                                    <td><?= $i++ ?></td>
                                    <td><?= $c['NM_PLANT'] ?></td>
                                    <td><?= $c['NM_GROUPAREA'] ?></td>
                                    <td><?= $c['URL'] ?></td>
                                    <td>
                                        <a href="<?= base_url('tis_excel_config/edit/').$c['ID_TIS_EXCEL_FILE'] ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Ubah</a>
                                        <a href="#" onclick="do_delete(<?= $c['ID_TIS_EXCEL_FILE'] ?>)" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
$(document).ready(function(){
    var table = $("#dt_tables").DataTable();   
});

$(".notice-error").confirm({ 
	confirm: function(button) { /* Nothing */ },
	confirmButton: "OK",
	cancelButton: "Cancel",
	confirmButtonClass: "btn-danger"
});
	
$(".notice-success").confirm({ 
	confirm: function(button) { /* Nothing */ },
	confirmButton: "OK",
	cancelButton: "Cancel",
	confirmButtonClass: "btn-success"
});


<?php if($notice->error): ?>
    $("#a-notice-error").click();
<?php endif; ?>
        
<?php if($notice->success): ?>
    $("#a-notice-success").click();
<?php endif; ?>

function do_delete(id){
    var r = confirm('Apakah Yakin Ingin Menghapus Data ?');
    if(r){
        window.location = "<?php echo base_url('tis_excel_config/action_delete/') ?>"+id;
    }
}

</script>