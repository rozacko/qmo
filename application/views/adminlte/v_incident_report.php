<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<style>
.modal {
    max-width: 800px;
}

.myFont{
    -webkit-box-shadow: 0;
      box-shadow: 0;
      background-color: #fff;
      border: 0;
      border-radius: 0;
      color: #555555;
      font-size: 12px;
      outline: 0;
      min-height: 48px;
      text-align: left;
      
}
#tbldata th, #tbldata td {
    border: 1px solid #ccc;
    text-align: left;
    padding: 2px;
}

.select2-selection__rendered {
  margin: 10px;
}

.select2-selection__arrow {
  margin: 10px;
}

</style>
<section class="content-header">
  <h1>
  NCQR Report
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>

      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>

      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" method="post" action="<?php echo site_url("incident/export_report") ?>" target="_blank">
            <div class="form-group row">

              <div class="form-group col-sm-12 col-sm-3">
                <label for="ID_COMPANY">COMPANY</label>
                <?php if($this->USER->ID_COMPANY): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_COMPANY; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_COMPANY" name="" VALUE="<?php echo $this->USER->ID_COMPANY; ?>" readonly class="form-control" />
                <?php else: ?>
                <select id="ID_COMPANY" class="form-control select2">
                  <?php  foreach($this->list_company as $company): ?>
                    <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>
                <?PHP endif; ?>
              </div>

              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_COMPANY">PLANT</label>
                <?php if($this->USER->ID_PLANT): ?>
                
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_PLANT; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_PLANT" name="" VALUE="<?php echo $this->USER->ID_PLANT; ?>" readonly class="form-control" />
                <?php else: ?>
                <input type="checkbox" id="checkbox_plant" title="Select All" class="pull-right">
                <select id="ID_PLANT" name="ID_PLANT[]" class="form-control select2" multiple="multiple">
                  <option disabled></option>
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_AREA">AREA</label>
                <?php if($this->USER->ID_AREA): ?>
                <INPUT TYPE="TEXT" VALUE="<?php echo $this->USER->NM_AREA; ?>" readonly class="form-control" />
                <INPUT TYPE="HIDDEN" ID="ID_AREA" name=ID_AREA VALUE="<?php echo $this->USER->ID_AREA; ?>" readonly class="form-control" />
                <?php else: ?>
                <input type="checkbox" id="checkbox_area" title="Select All" class="pull-right">
                <select id="ID_AREA" name="ID_AREA[]" class="form-control select2" multiple="multiple">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>

            </div>

            <div class="form-group row">

              <div class="form-group col-sm-12 col-sm-3">
                <label for="ID_COMPANY">START DATE</label>
                <INPUT TYPE="TEXT" id="STARTDATE" NAME="STARTDATE" VALUE="" readonly class="form-control" />

              </div>

              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_COMPANY">END DATE</label>
                <INPUT TYPE="TEXT" id="ENDDATE" name="ENDDATE" VALUE="" readonly class="form-control" />

              </div>
              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_AREA">TYPE</label>
                <input type="checkbox" id="checkbox_type" title="Select All" class="pull-right">

                <select id="ID_INCIDENT_TYPE" name="ID_INCIDENT_TYPE[]" class="form-control select2" multiple="multiple">
				  <!--<option value="">All</option>-->
                  <!-- <option disabled></option> -->
				  <?php foreach($this->list_type as $type): ?>
                  <option value="<?php echo $type->ID_INCIDENT_TYPE ?>"><?php echo $type->NM_INCIDENT_TYPE ?></option>
				  <?php endforeach; ?>
                </select>
              </div>

            </div>
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn-primary" name="load" id="btLoad">VIEW DATA</button> &nbsp;
                <button class="btn-success" id="btnExportXLS">EXPORT (XLS)</button>
              </div>
            </div>
            <hr/>


          </form>

          <div id="divTable" style="display:none;">
            <div class="form-group row">
              <div class="col-sm-12">
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
                <div id="handsonTable">

                </div>
              </div>
            </div>
          </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
</style>


<script src="https://rawgit.com/select2/select2/master/dist/js/select2.full.js" ></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery-ui.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<script>
  var bsModal = $.fn.modal.noConflict();
</script>
<script src="<?php echo base_url("js/jquery.modal.js") ?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery.modal.css") ?>" />
<link rel="stylesheet" type="text/css" href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" />

<script>
  $(document).ready(function(){
        $('#ID_PLANT').select2({
            placeholder: "Select Plant",
            closeOnSelect: false,
        });
        $('#ID_AREA').select2({
            placeholder: "Select Area",
            closeOnSelect: false,
        });
        $('#ID_INCIDENT_TYPE').select2({
            placeholder: "Select Type",
            closeOnSelect: false,
        });
        
        $("#checkbox_plant").click(function(){
            if($("#checkbox_plant").is(':checked') ){
                $("#ID_PLANT > option").prop("selected","selected");
                $("#ID_PLANT").trigger("change");
            }else{
                $("#ID_PLANT > option").removeAttr("selected");
                 $("#ID_PLANT").trigger("change");
                 $("#checkbox_area").attr('checked', false)
             }
        });
        $("#checkbox_area").click(function(){
            if($("#checkbox_area").is(':checked') ){
                $("#ID_AREA > option").prop("selected","selected");
                $("#ID_AREA").trigger("change");
            }else{
                $("#ID_AREA > option").removeAttr("selected");
                 $("#ID_AREA").trigger("change");
             }
        });
        $("#checkbox_type").click(function(){
            if($("#checkbox_type").is(':checked') ){
                $("#ID_INCIDENT_TYPE > option").prop("selected","selected");
                $("#ID_INCIDENT_TYPE").trigger("change");
            }else{
                $("#ID_INCIDENT_TYPE > option").removeAttr("selected");
                 $("#ID_INCIDENT_TYPE").trigger("change");
             }
        });
        
      // combobox()
	$("#STARTDATE, #ENDDATE").datepicker({
		 todayBtn: "linked",
        format: 'dd/mm/yyyy',
        // viewMode: 'months',
        // minViewMode: 'months',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
	});
    

	// $("").datepicker({
		// dateFormat: 'dd/mm/yy'
	// });

    $("#ID_COMPANY").change(function(){
      var company = $(this).val(), plant = $('#ID_PLANT');
      $.getJSON('<?php echo site_url("incident/ajax_get_plant/");?>' + company, function (result) {
        var values = result;

        plant.find('option').remove();
        if (values != undefined && values.length > 0) {
          plant.css("display","");
          $(values).each(function(index, element) {
            plant.append($("<option></option>").attr("value", element.ID_PLANT).text(element.NM_PLANT));
          });

        }else{
          plant.find('option').remove();
          plant.append($("<option></option>").attr("value", '00').text("NO PLANT"));
        }
        
                 $("#checkbox_area").attr('checked', false)
                 $("#checkbox_plant").attr('checked', false)
                 
      $("#ID_PLANT").change();
      });
      
    });

    $("#ID_PLANT").change(function(){
      var plant = $(this).val(), area = $('#ID_AREA');
      $.ajax({
      url: '<?php echo site_url("incident/ajax_get_area/");?>',
      type: "POST",
      data: {
            'id_company':$("#ID_COMPANY").val(),
            'plant':plant,
      },
      dataType: "JSON",
      success: function (result) {  
        var values = result;

        area.find('option').remove();
        if (values != undefined && values.length > 0) {
          area.css("display","");
          $(values).each(function(index, element) {
            area.append($("<option></option>").attr("value", element.ID_AREA).text(element.NM_AREA));
          });
        }else{
          area.find('option').remove();
          area.append($("<option></option>").attr("value", '00').text("NO AREA"));
        }
      }
    });
    });

    $("#btLoad").click(function(event){
      event.preventDefault();
      var formData = $("#formData").serializeArray();
	  var display = $("#DISPLAY").val();
      $("#saving").css('display','');
      $("#divTable").css("display","");

      //Set table columns | Update setting
      $.post('<?php echo site_url("incident/get_report");?>', formData, function (result) {
		//  console.log(result);
		$("#saving").css('display','none');
		var tab = '<div class="box-body table-responsive no-padding"><table class="table" border=1 id=tbldata>';
		tab += '<tr>';
		tab += '<th>No</th><TH>COMPANY</TH><TH>PLANT</TH><TH>AREA</TH><th>SUBJECT</th><th>CREATED BY</th><th>RESOLVED BY</th><th>CLOSED BY</th><th>TYPE</th><th>STATUS</th><th>DATE</th><TH>&nbsp;</TH>';
		tab += '</tr>';

		var ltr = 0
		//console.log(result);
		$.each(result, function(i,item){
			tab += "<tr>";
			tab += "<td>"+(i+1)+"</td>";
			tab += "<td>"+item.NM_COMPANY+"</td>";
			tab += "<td>"+item.NM_PLANT+"</td>";
			tab += "<td>"+item.NM_AREA+"</td>";
			tab += "<td>"+item.SUBJECT+"</td>";
      tab += "<td>"+(item.FULLNAME == null ? '<center>-</center>' : item.FULLNAME)+"</td>";
      tab += "<td>"+(item.RESOLVED_BY == null ? '<center>-</center>' : item.RESOLVED_BY)+"</td>";
			tab += "<td>"+(item.VERIFIKATOR == null ? '<center>-</center>' : item.VERIFIKATOR)+"</td>";
      tab += "<td>"+item.NM_INCIDENT_TYPE+"</td>";
			tab += "<td>"+item.NM_NCQR_STATUS+"</td>";
			tab += "<td>"+item.TANGGAL+"</td>";
			tab += "<td><a href='<?php echo site_url("incident/notif_sent") ?>/"+item.ID_INCIDENT+"' rel='modal:open' type='button' class='btn btn-info btn-xs'>Notification Sent</a></td>";
			tab += "</tr>";
			ltr++;
		});

		if(ltr == 0){
			tab += "<tr><td colspan='6'><i>(Empty Data)</i></td></tr>";
		}

		tab += '</table></div>';
		$("#handsonTable").html(tab);

      },"json");
    });


  $("#ID_COMPANY").change();
  
    
    
    // let branch_all_1 = [];
    
    // function formatResult_1(state) {
        // if (!state.id) {
            // var btn = $('<div class="text-right"><button id="all-branch_1" style="margin-right: 10px;" class="btn btn-default">Select All</button><button id="clear-branch_1" class="btn btn-default">Clear All</button></div>')
            // return btn;
        // }
        
        // branch_all_1.push(state.id);
        // var id = 'state' + state.id;
        // var checkbox = $('<div class="checkbox"><input id="'+id+'_1" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
        // return checkbox;   
    // }
    
    
    // let optionSelect2_1 = {
        // templateResult: formatResult_1,
        // closeOnSelect: false,
        // width: '100%'
    // };
    
    // let $select2_1 = $("#ID_PLANT").select2(optionSelect2_1);
    
    // var scrollTop;
    
    // $select2_1.on("select2:selecting", function( event ){
        // var $pr = $( '#'+event.params.args.data._resultId ).parent();
        // scrollTop = $pr.prop('scrollTop');
    // });
    
    // $select2_1.on("select2:select", function( event ){
        // $(window).scroll();
        
        // var $pr = $( '#'+event.params.data._resultId ).parent();
        // $pr.prop('scrollTop', scrollTop );
        
        // $(this).val().map(function(index) {
            // $("#state"+index).prop('checked', true);
        // });
    // });
    
    // $select2_1.on("select2:unselecting", function ( event ) {
        // var $pr = $( '#'+event.params.args.data._resultId ).parent();
        // scrollTop = $pr.prop('scrollTop');
    // });
    
    // $select2_1.on("select2:unselect", function ( event ) {
        // $(window).scroll();
        
        // var $pr = $( '#'+event.params.data._resultId ).parent();
        // $pr.prop('scrollTop', scrollTop );
        
        // var branch	=	$(this).val() ? $(this).val() : [];
        // var branch_diff = arr_diff(branch_all, branch);
        // branch_diff.map(function(index) {
            // $("#state"+index).prop('checked', false);
        // });
    // });
    
    // $(document).on("click", "#all-branch_1",function(){
        // $("#ID_PLANT > option").not(':first').prop("selected", true);// Select All Options
        // $("#ID_PLANT").trigger("change")
        // $(".select2-results__option").not(':first').attr("aria-selected", true);
        // $("[id^=state]").prop("checked", true);
        // $(window).scroll();
    // });
    
    // $(document).on("click", "#clear-branch_1", function(){
    	// $("#ID_PLANT > option").not(':first').prop("selected", false);
    	// $("#ID_PLANT").trigger("change");
        // $(".select2-results__option").not(':first').attr("aria-selected", false);
        // $("[id^=state]").prop("checked", false);
        // $(window).scroll();
    // });

});


function combobox(){
      
    let branch_all = [];
    
    function formatResult(state) {
        if (!state.id) {
        console.log(state.id)
            var btn = $('<div class="text-right"><button id="all-branch" style="margin-right: 10px;" class="btn btn-default">Select All</button><button id="clear-branch" class="btn btn-default">Clear All</button></div>')
            return btn;
        }
        
        branch_all.push(state.id);
        var id = 'state' + state.id;
        var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
        return checkbox;   
    }
    
    function arr_diff(a1, a2) {
        var a = [], diff = [];
        for (var i = 0; i < a1.length; i++) {
            a[a1[i]] = true;
        }
        for (var i = 0; i < a2.length; i++) {
            if (a[a2[i]]) {
                delete a[a2[i]];
            } else {
                a[a2[i]] = true;
            }
        }
        for (var k in a) {
            diff.push(k);
        }
        return diff;
    }
    
    
    let optionSelect2 = {
        templateResult: formatResult,
        closeOnSelect: false,
        width: '100%',
        // containerCssClass : "error",
        dropdownCssClass: 'myFont',
       
    };
    
    let $select2 = $("#ID_INCIDENT_TYPE").select2(optionSelect2);
    
    var scrollTop;
    
    $select2.on("select2:selecting", function( event ){
        var $pr = $( '#'+event.params.args.data._resultId ).parent();
        scrollTop = $pr.prop('scrollTop');
    });
    
    $select2.on("select2:select", function( event ){
        $(window).scroll();
        
        var $pr = $( '#'+event.params.data._resultId ).parent();
        $pr.prop('scrollTop', scrollTop );
        
        $(this).val().map(function(index) {
            $("#state"+index).prop('checked', true);
        });
    });
    
    $select2.on("select2:unselecting", function ( event ) {
        var $pr = $( '#'+event.params.args.data._resultId ).parent();
        scrollTop = $pr.prop('scrollTop');
    });
    
    $select2.on("select2:unselect", function ( event ) {
        $(window).scroll();
        
        var $pr = $( '#'+event.params.data._resultId ).parent();
        $pr.prop('scrollTop', scrollTop );
        
        var branch	=	$(this).val() ? $(this).val() : [];
        var branch_diff = arr_diff(branch_all, branch);
        branch_diff.map(function(index) {
            $("#state"+index).prop('checked', false);
        });
    });
    
    $(document).on("click", "#all-branch",function(){
        $("#ID_INCIDENT_TYPE > option").not(':first').prop("selected", true);// Select All Options
        $("#ID_INCIDENT_TYPE").trigger("change")
        $(".select2-results__option").not(':first').attr("aria-selected", true);
        $("[id^=state]").prop("checked", true);
        $(window).scroll();
    });
    
    $(document).on("click", "#clear-branch", function(){
    	$("#ID_INCIDENT_TYPE > option").not(':first').prop("selected", false);
    	$("#ID_INCIDENT_TYPE").trigger("change");
        $(".select2-results__option").not(':first').attr("aria-selected", false);
        $("[id^=state]").prop("checked", false);
        $(window).scroll();
    });
  }
</script>
<style>
.modal-dialog{
    position: relative !important;
    display: inline-block !important;
    overflow-y: auto !important;
    overflow-x: auto !important;
    width: auto !important;
    min-width: 100px !important;
    padding: 5px;
    z-index: 999 !important;
}


.modal a.close-modal {
	top: -50px !important;
	right: -50px !important;
}
</style>
