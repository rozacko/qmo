<style type="text/css">
  .select2-results__option .wrap:before{
    font-family:fontAwesome;
    color:#999;
    content:"\f096";
    width:25px;
    height:25px;
    padding-right: 10px;
    
  }
  .select2-results__option[aria-selected=true] .wrap:before{
      content:"\f14a";
  }

  /* not required css */

  .row
  {
    padding: 10px;
  }

  .select2-multiple, .select2-multiple2
  {
    width: 50%
  }
  .skin-red-light .wrapper, .skin-red-light .main-sidebar, .skin-red-light .left-side {
    background-color: #ecf0f5;
  }
  .header, .breadcrumb, h4, .sidebar-toggle, .sidebar, .main-footer{
    display: none
  }
  .skin-red-light .content-wrapper, .skin-red-light .main-footer {
    border-left: 0;
  }
</style>
<?php
// var_dump($this->data_user);
?>
   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
       <div class="row">
        <div class="col-xs-4"></div>
        <div class="col-md-4">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Activation</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="bodyContent">
              <div class="keterangan" style="text-align: left">

                  <center>
                      <h5 style="width: 400px;text-align: center">Your new account request has been approved, press the "activate button" to activate the account.</h5>
                  </center>
                  <hr>


                    <strong><i class="fa fa-user margin-r-5"></i> Nama & Username</strong>
                    <p class="text-muted"><?=$this->data_user->FULLNAME?> (<?=$this->data_user->USERNAME?>)</p>
                    <hr>

                    <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                    <p class="text-muted"><?=$this->data_user->EMAIL?></p>
                    <hr>

                    <strong><i class="fa fa-unlock-alt margin-r-5"></i> Password</strong> <small style="color: #aaa; font-weight: normal;" class="pull-right">Default (semenindonesia)</small>
                    <input type="password" id="password" name="password" class="form-control" placeholder="***">
                    <hr>

                    <strong><i class="fa fa-map-marker margin-r-5"></i> Company</strong>
                    <p class="text-muted">
                      <?=$this->data_user->NM_COMPANY ? $this->data_user->NM_COMPANY : '-'?> - <span class="label label-danger"> <?=$this->data_user->KD_COMPANY ? $this->data_user->KD_COMPANY : ''?></span>
                    </p>
                    <hr>

                    <strong><i class="fa fa-circle margin-r-5"></i> Plant</strong>
                    <p class="text-muted">
                      <?=$this->data_user->NM_PLANT ? $this->data_user->NM_PLANT : '-'?>
                    </p>
                    <hr>

                    <strong><i class="fa fa-circle margin-r-5"></i> Area</strong>
                    <p class="text-muted">
                      <?=$this->data_user->NM_AREA ? $this->data_user->NM_AREA : '-'?>
                    </p>
                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> Access</strong>
                    <p>

                    <?php
                    $color = array('danger', 'success', 'info', 'warning', 'primary', 'danger', 'success', 'info', 'warning', 'primary', 'danger', 'success', 'info', 'warning', 'primary', 'danger', 'success', 'info', 'warning', 'primary');
                    foreach ($this->roles as $i => $iv) {
                      echo '<span class="label label-'.$color[$i].'">'.$iv->NM_USERGROUP.'</span> ';
                    }
                    ?>
                    </p>

                    <hr>
                  <!-- /.box-body -->
                <center>
                  <button type="button" id="activate" class="btn btn-primary">Activate</button>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<script language="javascript" type="text/javascript">

  $("#activate").on("click", function(e) {
    e.preventDefault();
    var post_url = '<?php echo site_url("requester/updateActivation/".$this->data_user->ID_USER) ?>/agree'; //get form action url



    $.ajax({
      url : post_url,
      type: 'POST',
      data:{
        ISACTIVE : 'Y',
        EMAIL: '<?=$this->data_user->EMAIL?>',
        NOPEG: '<?=$this->data_user->NOPEG?>',
        USERNAME: '<?=$this->data_user->USERNAME?>',
        FULLNAME: '<?=$this->data_user->FULLNAME?>',
        NM_COMPANY: '<?=$this->data_user->NM_COMPANY?>',
        ID_COMPANY: '<?=$this->data_user->ID_COMPANY?>',
        ID_USERGROUP: '<?=$this->str_roles?>',
        PASSWORD: $('#password').val()
      }
    }).done(function(response){
      response = JSON.parse(response);
      if(response['status'] == 'success'){
        typeStatus = 1;
        $('#bodyContent').html('<div class="keterangan" style="text-align: center"><h5>Account successfully activated</h5></div>');
      }else{
        typeStatus = 4;
      }
    notif(typeStatus, response['message']);
    });
  });

</script>