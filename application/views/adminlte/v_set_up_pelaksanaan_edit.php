 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Data Set up pelaksanaan
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
		
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
			
		<?php if($notice->error): ?>

			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>

		<?php endif; ?>
		
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"> Set up pelaksanaan: </h3>
			  
            </div>
            <!-- /.box-header -->
            <!-- form start -->
           
              <div class="box-body" >
			  <div class="col-sm-4 " style="border-style: ridge; padding: 0.5em;">
				<label><u>Edit set up pelaksanaan </u></label>
			   <form role="form" method="POST" action="<?php echo site_url("Uji_proficiency_setup/do_edit") ?>" >
				<input type="hidden" name="ID_PROFICIENCY" id="ID_PROFICIENCY" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<div class="form-group clearfix">
					<div >
					<label>Periode Proficiency</label>
						<select class="form-control select2" NAME="ID_PP" id="ID_PP" placeholder="Select a Periode Proficiency ...">
						<option value="">Select a Periode Proficiency ...</option>
						<?php  foreach($this->list_periode as $dt_periode): ?>
						  <option value="<?php echo $dt_periode->ID_PP; ?>" <?php echo ($this->data_detil->ID_PRIODE == $dt_periode->ID_PP)?"SELECTED":""; ?> ><?= $dt_periode->TITLE_PP;?> [<?= $dt_periode->GROUP_PP;?> - <?= $dt_periode->YEAR_PP;?>]</option>
						<?php endforeach; ?>
						</select>              
					</div>
				</div>
				
				<div class="form-group clearfix">
					<div >
					<label>Commodity</label>
						<select class="form-control select2" NAME="ID_KOMODITI" id="ID_KOMODITI" placeholder="Select a Commodity ...">
						<option value="">Select a Commodity ...</option>
						<?php  foreach($this->list_comodity as $dt_comodity): ?>
						  <option value="<?php echo $dt_comodity->ID_SAMPLE ?>" <?php echo ($this->data_detil->ID_KOMODITI == $dt_comodity->ID_SAMPLE)?"SELECTED":""; ?> >[<?=  $dt_comodity->KODE_SAMPLE ?>] <?php echo $dt_comodity->NAMA_SAMPLE ?></option>
						<?php endforeach; ?>
						</select>              
					</div>
				</div>
				 
                <div class="form-group clearfix">
                  <div >
					<label>Laboratorium </label>
					<select class="form-control select2" NAME="ID_LAB" id="ID_LAB" placeholder="Select a Laboratorium ...">
					<option value="">Select a Laboratorium ...</option>
					<?php  foreach($this->list_lab as $dt_lab): ?>
					  <option value="<?php echo $dt_lab->ID_LAB ?>" <?php echo ($this->data_detil->ID_LAB == $dt_lab->ID_LAB)?"SELECTED":""; ?> ><?php echo $dt_lab->NAMA_LAB ?></option>
					<?php endforeach; ?>
					</select>    
				  </div>
                </div>
                
                <div class="form-group clearfix">                  
				 <div >
					<label>PIC</label>
					<select class="form-control select2" NAME="ID_PIC" id="ID_PIC" placeholder="Select a PIC ...">
					<option value="">Select a PIC ...</option>
					<?php  foreach($this->list_pic as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_USER ?>" <?php echo ($this->data_detil->ID_PIC == $dt_pic->ID_USER)?"SELECTED":""; ?> ><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>] - <?= $dt_pic->NM_AREA ?></option>
					<?php endforeach; ?>
					</select>     
					  
				  </div>                
				</div>
				
                <button type="submit" class="btn btn-success">Update</button>
                <a type="submit" class="btn btn-danger" href="<?php echo site_url("Uji_proficiency_setup") ?>" >Cancel</a>
				</form>
              </div>
              <!-- /.box-body -->
			  <div class="col-sm-8 ">
				<label>Activity Plan Schedule </label>
				<button style="display:none;" title="Add Activity" data-toggle="modal" data-target="#addModal" style="float: right; margin-bottom: 5px;" type="button" class="btn btn-primary">+</button>
				<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap">
					<thead>
						<tr>
							<th>No.</th>
							<th>Activity</th>
							<th>Start date</th>
							<th>End date</th>
							<th></th>
						</tr>
					</thead>
					<tbody style="font-weight: normal;">
					 <?php 
						$count = 1;
						foreach ($this->data_activity as $dt) { ?>
						<tr>
							<td><?= $count++; ?></td>
							<td><?= $dt->ACTIVITY_NAME; ?></td>
							<td><?= $dt->PLAN_DATE_START; ?></td>
							<td><?= $dt->PLAN_DATE_END; ?></td>
							<td><center>
							 <?php if($this->PERM_WRITE): ?>
                      <button title="Edit" class="btEdit btn btn-warning btn-xs" type="button" data-toggle="modal" data-target="#editModal<?= $dt->ID_ACTIVITY;?>"><i class="fa fa-pencil-square-o"></i> </button>
                      <a  style="display:none;" href="<?php echo site_url("Uji_proficiency_setup/do_hapus_activity/");?><?= $dt->ID_ACTIVITY; ?>/<?= $this->data_detil->ID_PROFICIENCY;?>" onClick="return doconfirm();"><button style="display:none;" title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> </button></a>
                    <?php endif; ?>
							</center></td>
						</tr>
						
<!-- Modal edit activity -->
<div id="editModal<?= $dt->ID_ACTIVITY; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("Uji_proficiency_setup/do_edit_activity") ?>" >
    <!-- Modal content-->
	<?php 
	// format date start
		$date1 = $dt->PLAN_DATE_START;
		$dateObj1 = DateTime::createFromFormat('d-M-y', $date1);
		$out1 = $dateObj1->format('Y-m-d');
	// format date end
		$date2 = $dt->PLAN_DATE_END;
		$dateObj2 = DateTime::createFromFormat('d-M-y', $date2);
		$out2 = $dateObj2->format('Y-m-d');
	?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Edit Activity:</b> <?= $dt->ACTIVITY_NAME; ?></h4>
      </div>
      <div class="modal-body">
				<input type="hidden" name="ID_PRO" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<input type="hidden" name="ID_ACTIVITY"  value="<?= $dt->ID_ACTIVITY;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-6 clearfix">
					<label>Activity </label>
					<input type="text" class="form-control" name="activity" placeholder="Activity name" value="<?= $dt->ACTIVITY_NAME; ?>" >
				  </div>
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date" value="<?= $out1;?>">
				  </div>
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date" value="<?= $out2; ?>">
				  </div>
                </div>
	  </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>

					<?php } ?>
					</tbody>
				</table>
			  </div>

             
            </form>
			
			
			<div class="gantt col-md-12" style="font-size: 12px;"></div>
			</div>
          </div>

    </section>

<link href="http://taitems.github.io/jQuery.Gantt/css/style.css" type="text/css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="http://taitems.github.io/jQuery.Gantt/js/jquery.fn.gantt.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js"></script>

<script>
 $(function() {
            "use strict";
			
			 $.ajax({
                type:"GET",
                url:"<?php echo site_url("Uji_proficiency_setup/timeline_act/") ?><?= $this->data_detil->ID_PROFICIENCY; ?>",
                dataType: "json",   
                success:function(data_in){
                    
					$(".gantt").gantt({
						source: data_in,
						navigate: "scroll",
						scale: "weeks",
						maxScale: "months",
						minScale: "days",
						itemsPerPage: 20,
						scrollToToday: false,
						useCookie: false,
						onItemClick: function(data) {
							//alert("Item clicked - show some details". );
						},
						onAddClick: function(dt, rowId) {
							//alert("Empty space clicked - add an item!");
						},
						onRender: function() {
							if (window.console && typeof console.log === "function") {
								console.log("chart rendered");
							}
						}
					});

					$(".gantt").popover({
						selector: ".bar",
						title: function _getItemText() {
							return this.textContent;
						},
						container: '.gantt',
						//content: "",
						trigger: "hover",
						placement: "auto right"
					});
					prettyPrint();
					
                },
                 error: function (xhr, ajaxOptions, thrownError) { 
                    alert(xhr.responseText);
                }
            });

        });
</script>

	
<!-- Modal add activity -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
	<form role="form" method="POST" action="<?php echo site_url("Uji_proficiency_setup/create_activity") ?>" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Activity</h4>
      </div>
      <div class="modal-body">
			<input type="hidden" name="PROFICIENCY" id="PROFICIENCY" value="<?= $this->data_detil->ID_PROFICIENCY;?>" />
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-5 clearfix">
					<label>Activity </label>
					<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  >
				  </div>
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date[]"  >
				  </div>
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date[]"  >
				  </div>
				  <div class="col-sm-1 ">
					<label>Add/Drop</label>
					<button type="button" class="btn btn-primary form-control add-more">+</button>
				  </div>
                </div>
				
				<div class="copy hide" id="second" style="display: none;">
					<div class="form-group c-group" >
					  <div class="col-sm-5 clearfix">
						<label>Activity </label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  >
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]"  >
					  </div>
					  <div class="col-sm-1 ">
						<label>Add/Drop</label>
						<button type="button" class="btn btn-danger form-control remove">-</button>
					  </div>
					</div>
				</div>
     
	 </div>
      <div class="modal-footer" style="margin-top: 2em;">
		<button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
      </div>
    </div>
	</form>
  </div>
</div>



<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>
	<script>
		alert('<?php echo $notice->error; ?>');
	</script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
	<script>
		alert('<?php echo $notice->success; ?>');
	</script>
<?php } ?>
<!-- eof msg confirm -->

<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

<script type="text/javascript">
    $(document).ready(function() {
	  $('select').selectize({
          sortField: 'text'
      });
		
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
		  
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>

<script>

$(document).ready(function(){

	/** DataTables Init **/
      var table = $("#dt_tables").DataTable(); 
});
</script>
<script>
	function doconfirm(){
	  job=confirm("Are you sure you want to delete data?");
	  if(job!=true){
		return false;
	  }
	}
</script>