<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  QAF Setting Time Outsilo
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      

      
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" method="post" action="<?php echo site_url("qaf_report_outsilo/export_xls") ?>" target="_blank">


            <div class="form-group row">
			  
              <div class="form-group col-sm-12 col-sm-3">
                <label class="control-label" >Company</label>
                <select id="opt_company" name="opt_company" class="form-control select2">
                  <option value="">Choose Company...</option>
                  <?php  foreach($this->list_company as $company):?>
                  <option value="<?php echo $company->ID_COMPANY;?>"><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>           
              </div>
              
            
			</div>
			<div class="form-group row">

              <div class="form-group col-sm-12 col-sm-3">
                <label for="ID_COMPANY">MONTH</label>
				<SELECT NAME="MONTH" ID="MONTH" class="form-control select2" >
					<?PHP
						for($i = 1 ; $i <= 12; $i++){
							$m = date("F",mktime(0,0,0,$i,1,date("Y")));
							// echo "<option value='".str_pad($i,0,STR_PAD_LEFT)."' ".(($i == date("m"))?"selected":"")." >".strtoupper($m)."</option>\n";
							echo "<option value='".date("m",mktime(0,0,0,$i,1,date("Y")))."' ".(($i == date("m"))?"selected":"")." >".strtoupper($m)."</option>\n";
						}
					?>
				</SELECT>
              </div>

              <div class="form-group col-sm-6 col-sm-3">
                <label for="ID_COMPANY">YEAR</label>
                <SELECT NAME="YEAR" ID="YEAR" class="form-control select2" >
					<?PHP
						for($i =date("Y"); $i >=2017; $i--){
							echo "<option value='".$i."'>$i</option>\n";
						}
					?>
				</SELECT>
              </div>

            </div>
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn-primary" name="load" id="btLoad">VIEW DATA</button> &nbsp; 
                <span id="saving" style="display:none;">
                  <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
                </span>
              </div>
            </div>
            <hr/>
          </form>
          

		   	<div id="divTable" style="display:;">
            	<div class="form-group row" id="row_qaf_area">
				
        	</div>
            
            
            
            
            <div id="divTable">
          <div class="form-group row" id="row_qaf_produksi">
				
          </div>
          </div>
            
            
          </div>
          
            
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
  
  canvas {
	-moz-user-select: none;
	-webkit-user-select: none;
	-ms-user-select: none;
  }
  
  #tbldata {
	  margin-bottom: 20px;
  }
  
  #tbldata td {
	  font-weight: normal;
	  color: #000;
  }
  
  #tbldata th {
	  background: #F5C9B3;
	  color: #150BD4;
	  font-weight: bold;
  }
  
  #tbldata th, #tbldata td {
	  border-color: #000;
	  text-align: center;
	  vertical-align: middle;
  }
  
  #tbldata #trstd th {
	  background: none;
  }
  
  #tbldata #trqaf td {
	  background: #C2E9FC;
	  font-weight: bold;
  }
  
  #tbldata #trtotal td {
	  background: #7CF7B5;
	  color: #150BD4;
	  font-weight: bold;
	  font-size: 1.2em;
  }

</style>



<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<script src="<?php echo base_url("js/jquery-ui.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<script src="<?php echo base_url("js/chartjs/Chart.bundle.js") ?>"></script>
<script src="<?php echo base_url("js/chartjs/utils.js") ?>"></script>

<script>
  $(document).ready(function(){
	
	
//   $("#checkallplant").click(function(){
//     if($("#checkallplant").is(':checked') ){
//         $("#id_plant > option").prop("selected","selected");
//         $("#id_plant").trigger("change");
//     }else{
//         $("#id_plant > option").removeAttr("selected");
//          $("#id_plant").trigger("change");
//      }
//   });


    $('.select2').select2();


  $('#opt_company').on('change', function(){
    idCompany = $("option:selected", this).val();
    //listPlant(idCompany);
  }); 
  

//   function listPlant(idCompany = ''){

//       $.ajax({
//         url : '<?//=base_url("login/getPlant/")?>'+idCompany,
//         type: 'GET',
//       }).done(function(data){
//         data = JSON.parse(data);
//         $('#id_plant').html('');
//         $.map( data, function( val, i ) {
//           selected = '';
//           if(val.ID_PLANT != ''){
//             // selected = 'selected';
//             $('#id_plant').append('<option value="'+val.ID_PLANT+'" '+selected+'>'+val.NM_PLANT+'</option>');
//           }
// 			$("#ID_PRODUCT").change();
//         });
//       });

//       $('.select2').select2();
//   }

//   $('#id_plant').on('change', function(){
//     idPlant = $(this).val();
//     // listProduct(idPlant);
//   }); 

//   $("#id_plant").change(function(){
// 		$("#div_table").html($("#loader").html());
// 		var url = "<?php // echo site_url("qaf_report/async_list_product_qaf1") ?>/"+$("#id_plant").val()+"/1";
// 		// $.get(url,function(data){
// 		// 	$("#ID_PRODUCT").empty();
// 		// 	data.forEach(function(r){
// 		// 		$("#ID_PRODUCT").append("<option value='"+r.ID_PRODUCT+"' "+((r.ID_PRODUCT==<?php //echo ($this->ID_PRODUCT)?$this->ID_PRODUCT:0; ?>)?"SELECTED":"")+" >"+r.KD_PRODUCT+"</option>");
// 		// 	});
// 		// 	$("#ID_PRODUCT").change();
// 		// },'json');		

// 		$.ajax({
//           type: "POST",
//           dataType: "json",
//           data: {id_plant: $("#id_plant").val(),ID_GROUPAREA:'1'},
//           url: '<?php //echo site_url("qaf_report/async_list_product_qaf1");?>',
//           success: function(data){
//             $("#ID_PRODUCT").empty();
// 			data.forEach(function(r){
// 				$("#ID_PRODUCT").append("<option value='"+r.ID_PRODUCT+"' "+((r.ID_PRODUCT==<?php //echo ($this->ID_PRODUCT)?$this->ID_PRODUCT:0; ?>)?"SELECTED":"")+" >"+r.KD_PRODUCT+"</option>");
// 			});
// 			$("#ID_PRODUCT").change();
 
//           },error: function(jqXHR, textStatus, errorThrown) {
//             // console.log("XHR: " + JSON.stringify(jqXHR));
//             console.log("Status: " + textStatus);
//             console.log("Error: " + errorThrown);
//           }
//         });
// 	});

	function qaf_company(r,arr_gr,curr_ik){
		if(r == undefined){
			$("#saving").css('display','none');					
			return false;				
		}
		$.post('<?php echo site_url("qaf_report_outsilo/generate_company_report");?>', { 
			MONTH: $("#MONTH").val(), 
			YEAR: $("#YEAR").val(),
			ID_COMPANY: r.ID_COMPANY,
			ID_GROUPAREA: 81
		}, function (nres) {
			$.post('<?php echo site_url("qaf_report_outsilo/get_company_report");?>', { 
				MONTH: $("#MONTH").val(), 
				YEAR: $("#YEAR").val(), 
				ID_COMPANY: r.ID_COMPANY,
				ID_GROUPAREA: 81
			}, function (res) {				
				$("#row_qaf_area").append(res);				
				curr_ik++;
				qaf_company(arr_gr[curr_ik],arr_gr,curr_ik);
							
			});
			$("#saving").css('display','');
		});
	}
	
	function qaf_produksi(idplant,idproduk){ 
		var ar_plant = [idplant];
		$.post('<?php echo site_url("qaf_report_outsilo/report_st_produksi");?>', { 
			MONTH: $("#MONTH").val(), 
			YEAR: $("#YEAR").val(), 
			//ID_PRODUCT: $("#ID_PRODUCT").val(), 
			//ID_PLANT: $("#id_plant").val(),
			ID_PRODUCT: idproduk, 
			ID_PLANT: ar_plant,
      		ID_COMPANY: $("#opt_company").val(),
			ID_GROUPAREA: 81
		}, function (nres) {
			$("#row_qaf_produksi").html(nres);
		});
	}
	
	function get_report(r,arr_gr,curr_ik){
		var idplant = r.ID_PLANT;
		var idproduk = r.ID_PRODUCT;
		if(r == undefined){
			qaf_produksi(idplant,idproduk);
			$("#saving").css('display','none');					
			return false;				
		}

    // console.log(r);
	// alert(r.id_plant);
		//generate data : calculate ke table QAF_DAILY
		$.post('<?php echo site_url("qaf_report_outsilo/generate_report_st");?>', { 
			MONTH: $("#MONTH").val(), 
			YEAR: $("#YEAR").val(),
			//ID_PRODUCT: $("#ID_PRODUCT").val(), 
			ID_PRODUCT: r.ID_PRODUCT, 
			ID_AREA: r.ID_AREA,
			ID_PLANT: r.ID_PLANT, //$("#id_plant").val(),
			ID_GROUPAREA: 81
		}, function (nres) {
			//menampilkan report
			$.post('<?php echo site_url("qaf_report_outsilo/get_report_st");?>', { 
				MONTH: $("#MONTH").val(), 
				YEAR: $("#YEAR").val(), 
				ID_COMPANY: $("#opt_company").val(),
				//ID_PRODUCT: $("#ID_PRODUCT").val(), 
				ID_PRODUCT: r.ID_PRODUCT, 
				ID_AREA: r.ID_AREA,						
				ID_PLANT: r.ID_PLANT,
				ID_GROUPAREA: 81
			}, function (res) {
				//$("#saving").css('display','none');
				//$("#row_qaf_area").append(res);
				
				$("#row_qaf_area").append(res);
				
				//console.log(curr_ik);
				//console.log(arr_gr.length-1);
				
				if(curr_ik == (arr_gr.length -1)){
					console.log("hitung produksi");
					//qaf_produksi();
					qaf_produksi(idplant,idproduk);
				}				
				curr_ik++;
				get_report(arr_gr[curr_ik],arr_gr,curr_ik);
				
				
			});
			$("#saving").css('display','');
		});
	}
	
    $("#btLoad").click(function(event){
      event.preventDefault();
      $("#saving").css('display','');
      $("#row_qaf_produksi").html("");
      $("#row_qaf_area").html("");
		if($("#ID_COMPANY").val() == 7){ //SMIG
			//get company list
			$.getJSON("<?php //echo site_url("qaf_report_outsilo/get_company_list") ?>",function(gr){
				var ik = 0;
				var arr_gr = new Array();
				
				if(gr.length >= 1){
					$.each(gr,function(key,r){
						arr_gr[ik] = r;
						ik++;
					});
					
					var curr_ik = 0; 
					qaf_company(arr_gr[curr_ik],arr_gr,curr_ik);
				}
				else{
					$("#saving").css('display','none');
				}
			});
		}
		else{ 
			$.ajax({
		          type: "POST",
		          dataType: "json",
		          data: {id_plant: $("#id_plant").val(),ID_GROUPAREA: '81',ID_COMPANY:$("#opt_company").val()},
		          url: '<?php echo site_url("qaf_report_outsilo/get_area_by_group");?>',
		          success: function(gr){
		            var ik = 0;
					var arr_gr = new Array();
					
					if(gr.length >= 1){
						$.each(gr,function(key,r){
							arr_gr[ik] = r;
							ik++;
						});
						
						var curr_ik = 0; 
						// console.log(curr_ik);
						get_report(arr_gr[curr_ik],arr_gr,curr_ik);
					}
					else{
						$("#saving").css('display','none');
					}
		 
		          },error: function(jqXHR, textStatus, errorThrown) {
		            // console.log("XHR: " + JSON.stringify(jqXHR));
		            console.log("Status: " + textStatus);
		            console.log("Error: " + errorThrown);
		          }
		        });

		}
    });
  //  $("#btLoad").click();
    
	$("#ID_COMPANY").change();
});
  
</script>
