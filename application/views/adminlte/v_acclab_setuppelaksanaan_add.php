   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Set up pelaksanaan Akurasi Labor
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
		
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
			
		<?php if($notice->error): ?>

			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>

		<?php endif; ?>
		
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo (isset($this->data_detil) ? 'Edit' : 'Add') ?> Data Set up pelaksanaan</h3>
            </div>
            <!-- /.box-header -->
		

            <!-- form start -->
            <form role="form" method="POST" action="<?php echo (isset($this->data_detil)) ? site_url("acclab_setuppelaksanaan/update") : site_url("acclab_setuppelaksanaan/create") ?>" >
              <div class="box-body" style="background-color:#c5d5ea;">
			  	<div class="col-sm-12 clearfix">
				  	<br>
					<div class="form-group row">
						<?php if (isset($this->data_detil)) { ?>
							<input type="hidden" name="ID" id="ID" value="<?php echo $this->data_detil->ID ?>">
						<?php } ?>
						<div class="col-sm-6">
							<label>Akurasi Laboratorium</label>
							<select class="form-control select2" NAME="ID_PP" id="ID_PP" placeholder="Select a Periode Proficiency ...">
							<option value="">Pilih Akurasi Laboratorium ...</option>
							<?php  foreach($this->list_periode as $dt_periode): ?>
							  <option value="<?php echo $dt_periode->ID; ?>" <?php echo (isset($this->data_detil) && $this->data_detil->FK_ID_PERIODE == $dt_periode->ID) ? 'selected' : '' ?>><?= $dt_periode->NAMA;?> [<?= $dt_periode->TAHUN;?>]</option>
							<?php endforeach; ?>
							</select>              
						</div>
						<div class="col-sm-6 ">
							<label>Jenis Pelaksanaan</label>
							<select class="form-control" NAME="JENIS" id="JENIS">
								<option value="OBSERVASI" <?php echo (isset($this->data_detil) && $this->data_detil->JENIS == 'OBSERVASI') ? 'selected' : '' ?>>OBSERVASI</option>
								<option value="BLIND TEST" <?php echo (isset($this->data_detil) && $this->data_detil->JENIS == 'BLIND TEST') ? 'selected' : '' ?>>BLIND TEST</option>
							</select>              
						</div>
					</div>
					
					<div class="form-group row">
					  <div class="col-sm-6">
						<label>Start Date </label>
						<input type="date" class="form-control" name="START_DATE" value="<?php echo (isset($this->data_detil)) ? date('Y-m-d', strtotime($this->data_detil->START_DATE)) : '' ?>">
					  </div>
					  <div class="col-sm-6">
						<label>End Date </label>
						<input type="date" class="form-control" name="END_DATE" value="<?php echo (isset($this->data_detil)) ? date('Y-m-d', strtotime($this->data_detil->END_DATE)) : '' ?>">
					  </div>
	                </div>
					
	                <div class="form-group row">
	                  <div class="col-sm-6" id="std" style="display: none;">
						<label>Standard Evaluasi </label>
						<select class="form-control select2" NAME="FK_ID_STD" id="FK_ID_STD" placeholder="Select a Standard ...">
							<option value="">Select a Standard ...</option>
							<?php  foreach($this->list_eval as $dt_eval): ?>
							  <option value="<?php echo $dt_eval->ID ?>" <?php echo (isset($this->data_detil) && $this->data_detil->FK_ID_STD == $dt_eval->ID) ? 'selected' : '' ?>><?php echo $dt_eval->NAMA_STANDARD ?> - <?php echo $dt_eval->KODE_STANDARD; ?></option>
							<?php endforeach; ?>
						</select>    
					  </div>
	                  <div class="col-sm-6">
						<label>Plant </label>
						<select class="form-control select2" NAME="ID_LAB" id="ID_LAB" placeholder="Select a Plant ...">
							<option value="">Select a Plant ...</option>
							<?php  foreach($this->list_lab as $dt_lab): ?>
							  <option value="<?php echo $dt_lab->ID_LAB ?>" <?php echo (isset($this->data_detil) && $this->data_detil->LAB == $dt_lab->ID_LAB) ? 'selected' : '' ?>><?php echo $dt_lab->NAMA_LAB ?></option>
							<?php endforeach; ?>
						</select>    
					  </div>
					</div>
					<div class="form-group row">
					  <div class="col-sm-6 ">
						<label>PIC OBSERVASI</label>
						<select class="form-control select2" NAME="ID_PIC_OBS" id="ID_PIC_OBS" placeholder="Select a PIC ...">
							<option value="">Select a PIC ...</option>
							<?php  foreach($this->list_pic_obs as $dt_pic): ?>
							  <option value="<?php echo $dt_pic->ID_USER ?>" <?php echo (isset($this->data_detil) && $this->data_detil->PIC_OBSERVASI == $dt_pic->ID_USER) ? 'selected' : '' ?>><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
							<?php endforeach; ?>
						</select>
					  </div>
					  <div class="col-sm-6 ">
						<label>PIC PLANT</label>
						<select class="form-control select2" NAME="ID_PIC_PLANT" id="ID_PIC_PLANT" placeholder="Select a PIC ...">
							<option value="">Select a PIC ...</option>
							<?php  foreach($this->list_pic_plant as $dt_pic): ?>
							  <option value="<?php echo $dt_pic->ID_USER ?>" <?php echo (isset($this->data_detil) && $this->data_detil->PIC_PLANT == $dt_pic->ID_USER) ? 'selected' : '' ?>><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
							<?php endforeach; ?>
						</select>
					  </div>
					</div>
              	</div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a type="submit" class="btn btn-danger" href="<?php echo site_url("acclab_setuppelaksanaan") ?>" >Cancel</a>
              </div>
            </form>
			</div>
          </div>

    </section>
    <!-- /.content -->
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

	
<script type="text/javascript">
    $(document).ready(function() {
    	var j = '<?php echo (isset($this->data_detil)) ? $this->data_detil->JENIS : '' ?>';
    	if (j == 'OBSERVASI') {
			$('#std').hide();
		}else{
			$('#std').show();
		}
		
		$("#a0").on("change",function(){ 
			var dateSet = $("#a0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#a1").val(newnextdate);
		});
		$("#b0").on("change",function(){ 
			var dateSet = $("#b0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#b1").val(newnextdate);
		});
		$("#c0").on("change",function(){ 
			var dateSet = $("#c0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#c1").val(newnextdate);
		});
		$("#d0").on("change",function(){ 
			var dateSet = $("#d0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#d1").val(newnextdate);
		});
		$("#e0").on("change",function(){ 
			var dateSet = $("#e0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#e1").val(newnextdate);
		});
		$("#f0").on("change",function(){ 
			var dateSet = $("#f0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#f1").val(newnextdate);
		});
		
	  $('select').selectize({
          sortField: 'text'
      });

      $("#JENIS").on("change",function(){ 
			var jenis = $("#JENIS").val();
			if (jenis == 'OBSERVASI') {
				$('#std').hide();
			}else{
				$('#std').show();
			}
		});
	  
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
		  
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>
