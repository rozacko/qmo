<div class="container">
  <div class="row">
    <h5 class="">Project:</h5>
    <button type="button" class="btn btn-primary float-right" name="simpan" id="simpan" onclick="simpan_plan()" data-dismiss="modal">Save</button>
  </div>
  <div id="handsontable"></div>
</div>
<!-- /.content-wrapper -->
<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
  var activity = [];
  var idprogres = [];
  var tanggal2 = [];
  var hot;
  // var project = $('#ID_PROJECT').val();
  var project = '1';
  $( document ).ready(function() {
    handsontable(project);
  });
  function simpan_plan(){
    var handsonData = hot.getData();
    var data_plan = [];
    handsonData.forEach(element => {
      element.shift(); // ini buat hapus kolom pertama
      element.shift(); // ini buat hapus kolom kedua
      data_plan.push(element);
    });
    data = {"data_plan": data_plan,"tanggal": tanggal2, "idprogres": idprogres, "project": project};
    console.log(data);
    // save("POST", url, data); // overhaul-js
  }
  function handsontable_old(project){
    url = 'http://10.15.5.150/dev/mso/index.php/overhaul/ajax/ajax_progres_plan/1';
        console.log(url);
    $.ajax({
      url: url, type: 'GET', dataType: 'json',
      success: function (result) {
        console.log(result);
        var hotElement = document.querySelector('#handsontable');
        var hotElementContainer = hotElement.parentNode;
        var hotSettings;
        var namaColumn = ['Tipe','Activity'];
        var columns = [{data: namaColumn[0], type: 'text', width: 70},{data: namaColumn[1], type: 'text', width: 250}];
        var dataObject = result.final;
        idprogres = result.idprogres;
        tanggal2 = result.tanggal;
        namaColumn = namaColumn.concat(tanggal2);
        
        for (let index = 2; index < namaColumn.length; index++) {
          columns.push({
            data: namaColumn[index], 
            type: 'text', width: 70
          });
        }
        hotSettings = {
          data: dataObject,
          rowHeaders: true,
          colHeaders: namaColumn,
          columns: columns,
          // width: 500,
          height: 300,
          autoWrapRow: true,
          autoWrapCell: true
        };
        hot = new Handsontable(hotElement, hotSettings);
        $("#handsontable").css('display','');
      },
      error: function(){console.log('error');}
    });
    
  }
  function handsontable(project){
    result = {
      "idprogres": [
        "1",
        "5",
        "7",
        "2",
        "3",
        "4",
        "6"
      ],
      "tanggal": [
        "20210318",
        "20210319",
        "20210320",
        "20210321",
        "20210322",
        "20210323",
        "20210324",
        "20210325"
      ],
      "final": [
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MAJOR",
          "Activity": "coba lagi"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MAJOR",
          "Activity": "coba aja"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MAJOR",
          "Activity": "coba aja ganti"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MINOR",
          "Activity": "coba lagi edit"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MINOR",
          "Activity": "aja"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MINOR",
          "Activity": "lagi"
        },
        {
          "20210318": "0",
          "20210319": "0",
          "20210320": "0",
          "20210321": "0",
          "20210322": "0",
          "20210323": "0",
          "20210324": "0",
          "20210325": "0",
          "Tipe": "MINOR",
          "Activity": "iya"
        }
      ]
    };
    // console.log(result);
    var hotElement = document.querySelector('#handsontable');
    var hotElementContainer = hotElement.parentNode;
    var hotSettings;
    var namaColumn = ['Tipe','Activity'];
    var columns = [{data: namaColumn[0], type: 'text', width: 70},{data: namaColumn[1], type: 'text', width: 250}];
    var dataObject = result.final;
    idprogres = result.idprogres;
    tanggal2 = result.tanggal;
    namaColumn = namaColumn.concat(tanggal2);
    
    for (let index = 2; index < namaColumn.length; index++) {
      columns.push({
        data: namaColumn[index], 
        type: 'text', width: 70
      });
    }
    console.log('dataObject'); console.log(dataObject);
    console.log('namaColumn'); console.log(namaColumn);
    console.log('columns'); console.log(columns);
    hotSettings = {
      data: dataObject,
      rowHeaders: true,
      colHeaders: namaColumn,
      columns: columns,
      // width: 500,
      height: 300,
      autoWrapRow: true,
      autoWrapCell: true
    };
    hot = new Handsontable(hotElement, hotSettings);
    $("#handsontable").css('display','');
  }
</script>
