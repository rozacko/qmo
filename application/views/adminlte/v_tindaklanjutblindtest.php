   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tindak Lanjut Blind Test
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
      <div class="row">
        <div class="col-xs-12">    
          <div class="box" id="viewArea">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Pelaksanaan Blind Test</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>

                  <!-- Trigger the modal with a button -->
                  <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

                </div>
              </div>
              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th width="5%">No. </th>
                      <th width="*">PELAKSANAAN</th>
                      <th width="*">PLANT</th>
                      <th width="*">TAHUN</th>
                      <th width="*">START DATE</th>
                      <th width="*">END DATE</th>
                      <th width="*">PIC PLANT</th>
                      <th width="5%">ACTION</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">    
    
          <div class="box" id="entryBlindTest" style="display: none;">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Display Blind test</h3>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="CancelEntry" type="button" class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </div>

            <div class="box-body">

              <div class="row col-sm-6">
                <table class="table table-bordered table-striped table-responsive" style="width: 100%;">
                  <thead>
                    <th colspan="3">INFORMASI BLIND TEST<span id="id_setup" style="display: none;"></span><span id="id_periode" style="display: none;"></span><span id="id_blindtest" style="display: none;"></span></th>
                    </thead>
                  <tbody style="font-size: x-small;">
                    <tr>
                      <td width="25%">PELAKSANAAN</td>
                      <td width="2%"> : </td>
                      <td width="*"><span id="pelaksanaan"></span></td>
                    </tr>
                    <tr>
                      <td>PLANT</td>
                      <td> : </td>
                      <td><span id="plant"></span></td>
                    </tr>
                    <tr>
                      <td>TAHUN</td>
                      <td> : </td>
                      <td><span id="tahun"></span></td>
                    </tr>
                    <tr>
                      <td>START DATE</td>
                      <td> : </td>
                      <td><span id="start_date"></span></td>
                    </tr>
                    <tr>
                      <td>END DATE</td>
                      <td> : </td>
                      <td><span id="end_date"></span></td>
                    </tr>
                    <tr>
                      <td>PIC OBSERVASI</td>
                      <td> : </td>
                      <td><span id="pic_observasi"></span></td>
                    </tr>
                    <tr>
                      <td>PIC PLANT</td>
                      <td> : </td>
                      <td><span id="pic_plant"></span></td>
                    </tr>
                    <tr>
                      <td>TANGGAL KIRIM</td>
                      <td> : </td>
                      <td><span id="tanggal_kirim"></span></td>
                    </tr>
                    <tr>
                      <td>TANGGAL TERIMA</td>
                      <td> : </td>
                      <td><span id="tanggal_terima"></span></td>
                    </tr>
                    <tr>
                      <td>STANDARD EVALUASI</td>
                      <td> : </td>
                      <td><span id="kodestd"></span> - <span id="namastd"></span></td>
                    </tr>
                  </tbody>
                  <tfoot class="evaluatestd">
                    <td>PILIH EVALUASI</td>
                    <td> : </td>
                    <td>
                      <select id="evaluate_std" name="evaluate_std" class="form-control">
                        <?php  foreach($this->list_stdevaluasi as $component): ?>
                          <?php  if ((int) $component->IS_ACTIVE == 1) { ?>
                            <option value="<?php echo $component->ID;?>"><?php echo ''.$component->NAMA_STANDARD.' ~ '.$component->KODE_STANDARD.'';?></option>
                          <?php  } ?>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tfoot>
                </table>
                <div class="box-tools pull-right evaluatestd">
                  <a id="EvaluasiBlind" onclick="evaluatesample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Evaluasi Data</a>
                </div>
                <div class="box-body" id="forminputblind" style="">
                  <div class="box box-warning box-solid col-sm-12 col-md-12 col-lg-12">
                    <div class="box-header with-border">
                      <h3 class="box-title"> ENTRY DATA DIBAWAH INI <i class="fa fa-hand-o-down"></i> </h3>
                    </div>
                    <div class="box-body" style="">
                      <div id="example1" class="hot handsontable htColumnHeaders" style="margin-left: 15px; margin-right: : 15px; max-height: 350px !important;"></div>
                      <div class="form-group row" style="margin-left: 15px; margin-right: : 15px;">
                        <div class="col-sm-2" style="">
                          <label for="addNewRow"></label>
                          <a  style="display: none;" name="addNewRow" id="addNewRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm addNewRow"><i class="fa fa-plus"></i> Row</a>
                        </div>
                        <div class="col-sm-2" style="">
                          <label for="delLastRow"></label>
                          <a style="display: none;" name="delNewRow" id="delLastRow" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-danger btn-sm delLastRow"><i class="fa fa-trash"></i> Row</a>
                        </div>
                        <div class="col-sm-2" style="float: right; margin-top: 25px;">
                          <a  name="savesample" id="savesample" style="display: none;" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-primary btn-sm"><i class="fa fa-save"></i> &nbsp;Save</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row col-sm-12">
                <div class="box-body" style="margin-left: 5px; ">
                  <div class="box box-danger box-solid col-sm-12 col-md-12 col-lg-12">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Preview Hasil Evaluasi Blind Test </h3>
                      <div class="box-tools pull-right">
                        <a id="SubmitBlind" onclick="submitsample()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a> 
                        <a id="ReloadDataBlind" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                      </div>
                    </div>
                    <div class="box-body" style="">
                      <table  id="blind_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th rowspan="2" width="5%">NO.<br>URUT</th>
                            <th rowspan="2" width="25%">KOMPONEN</th>
                            <th rowspan="2" width="*">SCORE<br>EVALUASI</th>
                            <th colspan="4" width="*">INVESTIGASI</th>
                            <th colspan="4" width="*">PERBAIKAN</th>
                            <th rowspan="2" width="*">VERIFIKASI<br>PERBAIKAN</th>
                            <th rowspan="2" width="5%">ACTION</th>
                          </tr>

                          <tr>
                            <th width="*">Man</th>
                            <th width="*">Methode</th>
                            <th width="*">Machine</th>
                            <th width="*">Environment</th>
                            <th width="*">Man</th>
                            <th width="*">Methode</th>
                            <th width="*">Machine</th>
                            <th width="*">Environment</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>

            <div class="box-footer">
            </div>
          </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>

      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Form Entry Tindak Lanjut</h4>
            </div>
            <div class="modal-body">
              <form ></form>
                <table class="table table-bordered table-striped table-responsive" style="width: 100%;">
                  <tbody>
                    <tr>
                      <td colspan="3">INVESTIGASI<span id="kodeevaluasi" style="display: none;"></span><span id="kodeblind" style="display: none;"></span></td> 
                    </tr>
                    <tr>
                      <td width="25%">Man</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="invman" id="invman"></td>
                    </tr>
                    <tr>
                      <td width="25%">Methode</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="invmethode" id="invmethode"></td>
                    </tr>
                    <tr>
                      <td width="25%">Machine</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="invmachine" id="invmachine"></td>
                    </tr>
                    <tr>
                      <td width="25%">Environment</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="invenvironment" id="invenvironment"></td>
                    </tr>
                    <tr>
                      <td colspan="3">PERBAIKAN</td>
                    </tr>
                    <tr>
                      <td width="25%">Man</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="perbman" id="perbman"></td>
                    </tr>
                    <tr>
                      <td width="25%">Methode</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="perbmethode" id="perbmethode"></td>
                    </tr>
                    <tr>
                      <td width="25%">Machine</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="perbmachine" id="perbmachine"></td>
                    </tr>
                    <tr>
                      <td width="25%">Environment</td>
                      <td width="2%"> : </td>
                      <td width="*"><input type="text" class="form-control" name="perbenvironment" id="perbenvironment"></td>
                    </tr>
                      <td colspan="3">VERIFIKASI PERBAIKAN</td>
                    </tr>
                    <tr>
                      <td colspan="3"><input type="file" class="form-control" name="fileverifikasi" id="fileverifikasi" multiple="multiple"><br><div id="downloadfile"></div></td>
                    </tr>

                  </tbody>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <a id="SaveTTL" onclick="savetindaklanjut()" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" type="button" class="btn btn-success btn-sm"><i class="fa fa-document"></i> Submit Data</a> 
            </div>
          </div>

        </div>
      </div>
    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


  
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

  
<script type="text/javascript">
    $(document).ready(function() {
    
    $("#a0").on("change",function(){ 
      var dateSet = $("#a0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#a1").val(newnextdate);
    });
    $("#b0").on("change",function(){ 
      var dateSet = $("#b0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#b1").val(newnextdate);
    });
    $("#c0").on("change",function(){ 
      var dateSet = $("#c0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#c1").val(newnextdate);
    });
    $("#d0").on("change",function(){ 
      var dateSet = $("#d0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#d1").val(newnextdate);
    });
    $("#e0").on("change",function(){ 
      var dateSet = $("#e0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#e1").val(newnextdate);
    });
    $("#f0").on("change",function(){ 
      var dateSet = $("#f0").val();
      var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
      var newnextdate = result.toISOString().substr(0, 10);
      $("#f1").val(newnextdate);
    });
    
    $('select').selectize({
          sortField: 'text'
      });
    
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
      
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>


<script type="text/javascript">

  function get_tindaklanjut(ideval, idblind) {
    console.log(idblind);
    // body...
    $.ajax({
      url: "<?php echo site_url("acclab_tindaklanjutblindtest/ttl_blindtest");?>/"+ideval,
      data: {"ID": ideval, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
      dataType: 'json',
      type: 'POST',
      success: function (res) {
        if (res['status'] == true) {
          var data = res['data'];
          // $("#a-notice-success").data("text", res.msg);
          // $("#a-notice-success").click();
          // $("#ReloadData").click(); 

          $('#kodeevaluasi').html(ideval);
          $('#kodeblind').html(idblind);
          $('#invman').val(data['I_MAN']);
          $('#invmethode').val(data['I_METHODE']);
          $('#invmachine').val(data['I_MACHINE']);
          $('#invenvironment').val(data['I_ENVIRONMENT']);
          $('#perbman').val(data['P_MAN']);
          $('#perbmethode').val(data['P_METHODE']);
          $('#perbmachine').val(data['P_MACHINE']);
          $('#perbenvironment').val(data['P_ENVIRONMENT']);
          $('#downloadfile').html('');
          if (data['VERIFIKASI'] != '' && data['VERIFIKASI'] != 'null' && data['VERIFIKASI'] != null) {
            $('#downloadfile').append(data['VERIFIKASI']);
          }
          // $('#fileverifikasi').val(data['NAMA']);

        } else {
          $("#a-notice-error").data("text", res.msg);
          $("#a-notice-error").click();
        }                
      },
      error: function () {
        $("#a-notice-error").click();
      }
    });

  }

  function savetindaklanjut() {
    // body...
    var form_data = new FormData();

    form_data.append('ID_BLIND', $("#kodeblind").text());
    form_data.append('ID', $("#kodeevaluasi").text());

    // form_data.append('VERIFIKASI', $("input[name^='fileverifikasi']")[0].files);
    // console.log($("#fileverifikasi"));
    for (var index = 0; index < $("#fileverifikasi")[0].files.length; ++index) {
      form_data.append('VERIFIKASI[' + index + ']', $("input[name^='fileverifikasi']")[0].files[index]);
    }
  // $("input[name^='idstandardcheck']").each(function(index) {
  //   form_data.append('VERIFIKASI[' + index + ']', $("input[name^='fileverifikasi']")[index].files[0]);
  // });

    form_data.append('I_MAN', $("#invman").val());    
    form_data.append('I_METHODE', $("#invmethode").val());
    form_data.append('I_MACHINE', $("#invmachine").val());
    form_data.append('I_ENVIRONMENT', $("#invenvironment").val());


    form_data.append('P_MAN', $("#perbman").val());    
    form_data.append('P_METHODE', $("#perbmethode").val());
    form_data.append('P_MACHINE', $("#perbmachine").val());
    form_data.append('P_ENVIRONMENT', $("#perbenvironment").val());

    $.ajax({
        url: "<?php echo site_url("acclab_tindaklanjutblindtest/save_tindaklanjutblindtest");?>",
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        dataType: 'json',
        beforeSend: function() {
            $('#SaveTTL').button('loading');
        },
        complete: function() {
            $('#SaveTTL').button('reset');
        },
        // error: function() {
        //     swal({
        //         title: "Failed to Process Your Request ... !!!",
        //         type: "error"
        //     });
        // },
        // success: function(data) {
        //     if (data.Status == 200) {
        //         swal("Saved!", "Your data has been saved.", "success");
        //         $('#idattachment').val(data['ID_ATTACHMENT']);
        //         $('#myModalLampiran').modal('hide');
        //         tabel_lampiran();
        //     } else {    
        //         swal("Erorr!", data.Message, "error");
        //     }
        // },
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadDataBlind").click();
            $('#myModal').modal('hide');
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
    });
  }
  
  function receiptsample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menerima sample sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_tindaklanjutblindtest/receipt_blindtest");?>",
        data: {"FK_ID_PERIODE": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }

  function evaluatesample() {
    // body...
    var r = confirm("Apakah anda yakin untuk mengevaluasi sample sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_tindaklanjutblindtest/evaluate_blindtest");?>",
        data: {"ID": $('#id_blindtest').text(), "FK_ID_STD": $('#evaluate_std').val(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            var evalcode = ($('#evaluate_std').val()).split(" ~ ");
            $('#kodestd').html(evalcode[1]);
            $('#namastd').html(evalcode[0]);
            $("#ReloadDataBlind").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
    }
  }
  
  function deletesample(idsetup) {
    // body...
    var r = confirm("Apakah anda yakin untuk menghapus data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_tindaklanjutblindtest/delete_blindtest");?>",
        data: {"ID": idsetup, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#ReloadDataBlind").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }
  
  function submitsample() {
    // body...
    var r = confirm("Apakah anda yakin untuk submit data ini sekarang ?");
    if (r == true) {
      $.ajax({
        url: "<?php echo site_url("acclab_tindaklanjutblindtest/submit_evaluasiblindtest");?>",
        data: {"ID": $('#id_setup').text(), "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $("#CancelEntry").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
      console.log(idsetup);
    } else {
    }
  }

$(document).ready(function(){

  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable; 
  var oBlindTable;
  var glbproduk = new Array();

  reloadtabel(); 

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
  });
  $(document).on('click',"#ReloadDataBlind",function () {
    reloadblindtabel($('#id_setup').text());
  });
  
  
  $(document).on('click',"#CancelEntry",function () {
    $('#entryBlindTest').hide();
    $('#viewArea').show();
    $("#ReloadData").click(); 
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
  });

  function reloadblindtabel(idsetup, action = true) {
    // body...
    oBlindTable = $('#blind_tables').dataTable({      
      "paging":   false,
      destroy: true,
      processing: true,
      select: true,
      "ajax": {
        "url": "<?php echo site_url("acclab_tindaklanjutblindtest/ajax_get_blindtest");?>/"+idsetup+"/"+action,
        "type": "POST"
      },
      // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      //   if (parseInt(aData[2]) >= 0 && parseInt(aData[2]) <= 2) {
      //     $('td', nRow).css('background-color', 'Red');
      //   } else if (parseInt(aData[2]) == 3) {
      //     $('td', nRow).css('background-color', 'Yellow');
      //   } else if (parseInt(aData[2]) == 4 || parseInt(aData[2]) == 5) {
      //     $('td', nRow).css('background-color', 'Green');
      //   }
      // }
      rowCallback: function(row, data, index){
        if (parseInt(data[2]) >= 0 && parseInt(data[2]) <= 2) {
          $(row).find('td:eq(2)').css('background-color', 'Red');
          $(row).find('td:eq(2)').css('color', 'White');
        } else if (parseInt(data[2]) == 3) {
          $(row).find('td:eq(2)').css('background-color', 'Yellow');
          $(row).find('td:eq(2)').css('color', 'Black');
        } else if (parseInt(data[2]) == 4 || parseInt(data[2]) == 5) {
          $(row).find('td:eq(2)').css('background-color', 'Green');
          $(row).find('td:eq(2)').css('color', 'White');
        }
        // if(data[3]> 11.7){
        //   $(row).find('td:eq(3)').css('background-color', 'red');
        // }
        // if(data[2].toUpperCase() == 'EE'){
        //   $(row).find('td:eq(2)').css('color', 'blue');
        // }
      }
    });
    
  }

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("acclab_tindaklanjutblindtest/blindtest_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "RNUM", "width": 50},
        {"data": "NAMA"},
        {"data": "NAMA_LAB"},
        {"data": "TAHUN"},
        {"data": "SETUP_START"},
        {"data": "SETUP_END"},
        {"data": "NAMA_PIC_PLANT"},
        {"data": "ID", "width": 100,
          "mRender": function(row, data, index){
            if (parseInt(index['STATUS_EVALUASI']) == 0) {
              return '<button title="Detail Blind Test" class="btDetail btn btn-primary btn-xs" type="button"><i class="fa fa-document"></i> Detail Blind Test</button> | <button title="Evaluasi Sample" class="btEvaluasi btn btn-warning btn-xs" type="button" ><i class="fa fa-download"></i> Evaluasi Blind Test</button>';
            } else {
              return '<button title="Detail Blind Test" class="btDetail btn btn-primary btn-xs" type="button"><i class="fa fa-document"></i> Detail Blind Test</button>';
            }

          }
        },
      ]
    });
    
    $('#ReloadData').show();
  }

$(document).on('click', ".btEntry", function() {
    // body...
    // $(document).off('focusin.modal');
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').show();    

    $('#SubmitBlind').show();    

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);
    $('#id_blindtest').html(data['ID_BLINDTEST']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    $('#kodestd').html(data['KODE_STANDARD']);
    $('#namastd').html(data['NAMA_STANDARD']);

    loadtabelinput();
    reloadblindtabel(data['ID']);
});

$(document).on('click', ".btDetail", function() {
    // body...
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').hide();    

    $('#SubmitBlind').hide();    
    $('.evaluatestd').hide();   

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);
    $('#id_blindtest').html(data['ID_BLINDTEST']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    $('#kodestd').html(data['KODE_STANDARD']);
    $('#namastd').html(data['NAMA_STANDARD']);
    reloadblindtabel(data['ID'], false); 
});

$(document).on('click', ".btEvaluasi", function() {
    // body...
    var data = oTable.row($(this).parents('tr')).data();
    $('#viewArea').hide();
    $('#entryBlindTest').show();
    $('#forminputblind').hide();    

    $('#SubmitBlind').show();   
    // $('.evaluatestd').show();    

    // $('#SubmitBlind').hide();    
    $('.evaluatestd').hide();   

    $('#id_setup').html(data['ID']);
    $('#id_periode').html(data['FK_ID_PERIODE']);
    $('#id_blindtest').html(data['ID_BLINDTEST']);

    $('#pelaksanaan').html(data['NAMA']);
    $('#plant').html(data['NAMA_LAB']);
    $('#tahun').html(data['TAHUN']);
    $('#start_date').html(data['SETUP_START']);
    $('#end_date').html(data['SETUP_END']);
    $('#pic_observasi').html(data['NAMA_PIC_OBS']);
    $('#pic_plant').html(data['NAMA_PIC_PLANT']);
    $('#tanggal_kirim').html(data['TANGGAL_KIRIM']);
    $('#tanggal_terima').html(data['TANGGAL_TERIMA']);

    $('#kodestd').html(data['KODE_STANDARD']);
    $('#namastd').html(data['NAMA_STANDARD']);
    reloadblindtabel(data['ID'], false); 
});

  var example1 = document.getElementById('example1'), hot1;
  var oglbcomponent = new Array();
  var oglbcomponentid = new Array();
  var oglbcomponentname = new Array();

  document.querySelector('.addNewRow').addEventListener('click', function() {
    hot1.alter('insert_row', hot1.countRows());
  });

  document.querySelector('.delLastRow').addEventListener('click', function() {
    hot1.alter('remove_row', hot1.countRows() - 1);
  });

    function getListComponent() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_tindaklanjutblindtest/get_component");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_COMPONENT']);

          }
        }else{
        }
      });
      return datal;
    }

  function getContentData() {
      return [
      ];
    }

    function getRowOptions() {
      var initial_coloptions = [
        {
          data: 'produk',
          type: 'autocomplete',
          source: getListComponent(),
          strict: true,
          allowInvalid: false,
          colWidths: 250,
          visibleRows: 10
        },
        {
          type: 'text',
        },
        {
          type: 'text',
        }
      ];
      return initial_coloptions;
    }

    function getColHeader() {
      var columnlist = ['Component', 'Testing Result 1', 'Testing Resul 2'];
      return columnlist;
    }

    function getTypeProduct() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("acclab_tindaklanjutblindtest/ajax_get_type_product");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['KD_PRODUCT']);
          }
        }else{
        }
      });
      
      return datal;
    }

    function getTypeProduct(merk) {
      return glbproduk[merk];
    }

    function getListArea() {
      var datal = new Array();
      $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_sample_area_scm");?>', function (result) {
        var values = result;
        if (values != undefined && values.length > 0) {
          for (var i = 0; i < values.length; i++) {
            datal.push(values[i]['NM_KOTA']);
          }
        }else{
        }
      });
      return datal;
    }

  function loadtabelinput() {

    if (hot1) {

      hot1.destroy();

    }   

    $.getJSON('<?php echo site_url("acclab_tindaklanjutblindtest/ajax_get_component_display");?>', function (result) {
      var values = result;
      if (values != undefined && values.length > 0) {
        for (var i = 0; i < values.length; i++) {
          if (parseInt(values[i]['STATUS_CHECKLIST']) == 1) {
            oglbcomponent.push(values[i]['KD_COMPONENT']);
            oglbcomponentid.push(parseInt(values[i]['ID_COMPONENT']));
            oglbcomponentname.push(values[i]['KD_COMPONENT']);
          }
        }
        hot1 = new Handsontable(example1, {
          data: getContentData(),
          height: 250,
          autoColumnSize : true,
          fixedColumnsLeft: 3,
          manualColumnFreeze: true,
          manualColumnResize: true,
          colHeaders: getColHeader(),
          columns: getRowOptions(),
        });
        hot1.alter('insert_row', hot1.countRows());

        hot1.updateSettings({
          afterChange: function(changes, src) {
            var row = changes[0][0],
              col = changes[0][1],
              newVal = changes[0][3];
          }
        })

        $('#delLastRow').show();
        $('#addNewRow').show();
        $('#savesample').show();
      }else{
      }
    });

  }

    $('button[name=savesample]').click(function () {
      saveBlindTest();
    });

    function saveBlindTest() {
        $body = $("body");
        $body.addClass("loading");
        $body.css("cursor", "progress");

      $('#savesample').button('loading');
        var handsonData = hot1.getData();
        $.ajax({
          url: "<?php echo site_url("acclab_tindaklanjutblindtest/save_sample_data_blindtest");?>",
          data: {"user": "<?php echo $this->USER->FULLNAME ?>", "data": handsonData, "id_setup": $('#id_setup').text(), "id_periode": $('#id_periode').text()}, //returns all cells' data
          dataType: 'json',
          type: 'POST',
          success: function (res) {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              if (res.msg == 'success') {
                // $("#a-notice-success").click();
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
                var rowcount = hot1.countRows();
                for (var i = 0; i < rowcount; i++) {
                  hot1.alter('remove_row', hot1.countRows() - 1);
                }               
                hot1.alter('insert_row', hot1.countRows());
              }
              else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }
              $('#savesample').button('reset');              
              $('#entryBlindTest').hide();
              $('#viewArea').show();

          },
          error: function () {
              $body.removeClass("loading");
              $body.css("cursor", "default");
              $("#a-notice-error").click();

              $('#savesample').button('reset');
          }
        });

    }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
