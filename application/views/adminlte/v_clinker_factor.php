<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
 Clinker Factor
  <small></small>
  </h1>
</section>
<style>
/*START CSS LOADING*/
  .spinner {
    margin: 100px auto 0;
    width: 70px;
    text-align: center;
  }

  .spinner>div {
    width: 18px;
    height: 18px;
    background-color: #333;

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
    animation: sk-bouncedelay 1.4s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0)
    }
    40% {
      -webkit-transform: scale(1.0)
    }
  }

  @keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0);
      transform: scale(0);
    }
    40% {
      -webkit-transform: scale(1.0);
      transform: scale(1.0);
    }
  }
  /*END CSS LOADING*/
</style>
<!-- Main content -->
<section class="content">

   
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <!--<div class="box-body" style="overflow-x:auto">-->
        <div class="box-body">
            <table class="table" style="border: 1px solid black;">
              <div class="form-group col-sm-2" >
                <label for="ID_COMPANY">Start</label>
                <input name="START" id="start" type="text" class="form-control" value="<?php echo date("m/Y");?>">
              </div>
              <div class="form-group col-sm-2">
                <label for="ID_COMPANY">END</label>
                <input name="END" id="end" type="text" class="form-control" value="<?php echo date("m/Y");?>">
              </div>
              <div class="form-group col-sm-2">
                <label for="ID_COMPANY">&nbsp;</label>
                <a id='btLoad' class="form-control btn btn-primary"><i class="fa fa-search"> Load</i></a>
              </div>
            </table>
            
            <div class="row" id="loading_css" >
                <div class="col-lg-12">
                  <div class="ibox float-e-margins">
                    <div class="ibox-content">
                      <div class="row">
                        <div class="col-md-12">
                          <center>
                            <div class="spinner" style="margin:40px;">
                              <div class="bounce1"></div>
                              <div class="bounce2"></div>
                              <div class="bounce3"></div>
                            </div>
                          </center>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-lg-12">
                    <div style="overflow-x:auto;">
                        <table id="tbl_informasi" class="table table-striped table-bordered table-hover dt-responsive nowrap" style="width:100%;font-size=60%">
                  
                        </table>
                    </div>
                </div>
                
                
                <div class="col-lg-6">
                    <div id="grafik_opc_0" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_opc_1" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_opc_2" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_opc_3" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-3">
                    <div id="grafik_0" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_opc_4" style="width:100%; height: 400px;"></div>
                </div>
                
                <div class="col-lg-3">
                    <div id="grafik_0" style="width:100%; height: 400px;"></div>
                </div>
                
                <div class="col-lg-6">
                    <div id="grafik_nonopc_0" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_nonopc_1" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_nonopc_2" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_nonopc_3" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-3">
                    <div id="grafik_0" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-6">
                    <div id="grafik_nonopc_4" style="width:100%; height: 400px;"></div>
                </div>
                <div class="col-lg-3">
                    <div id="grafik_0" style="width:100%; height: 400px;"></div>
                </div>
            </div>
        </div>
        
          
          
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<style type="text/css">
  th, td {
    padding: 9px;
    text-align: left;
  }

  div #tb_container {
    width: 250px;
    height:auto;
    min-height:200px;
    height:auto !important;  
    height:200px;
    text-align: center;
  }

  div #tb_label {
    border-radius: 100px;
    border: 3px solid #FFB432;
    width: 100px;
    height: 100px; 
    text-align: center;
    z-index: 1;position:relative;
  }

  #lb_text_opco {
    font-size: 30px;
    color: white;
  }

  #lb_text_nilai {
    float: left;
    font-size: 25px;
    color: white;
    width: 100%;
    margin-top: -8px;
  }

  span h3 { 
    font-weight: bold; 
    text-align: center;
  }

  div #tx_catatan {
    background: #CCDDFF;
    box-shadow: 12px 0 8px -4px rgba(31, 73, 125, 0.5), -12px 0 8px -4px rgba(31, 73, 125, 0.5);
    border-radius: 25px;
    width: 250px; 
    height:500px;
    margin-top: -15px;
    padding: 5px 5px 5px 0px;
    text-align: left;
  }

</style>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
     $('#loading_css').hide();
      $(document).ready(function(){
          
        $('#start, #end').datepicker({
            todayBtn: "linked",
            format: 'mm/yyyy',
            viewMode: 'months',
            minViewMode: 'months',
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

          get_table()
          
              $("#btLoad").click(function(){
                 get_table()
              });
    });
    
    
     function get_table(){
        var start = $("#start").val();
        var end = $("#end").val();
      $.ajax({
            url: "<?php echo site_url("data_table/table_clinker_factor");?>",
            data: {"start": start, "end": end}, 
            dataType: 'json',
            type: 'POST',
            success: function (data) {
                $('#tbl_informasi').html(data.tabel);
                // var data = JSON.parse(data)
                 var lebar = parseFloat(data.lebar);
                    var i;
                    for (i = 1; i <= lebar; i++) {
                            var value = $("#testing_"+i).text();
                             num = addCommas(value);
                            console.log(num)
                            $("#testing_"+i).text(num)
                    }
             for(var i=0;i< data.company.length; i++){
                 
                 var dataMerge = [data['3d'][i], data['7d'][i], data['28d'][i], data['index'][i]];    
                 var dataMergeNon = [data['3d_non'][i], data['7d_non'][i], data['28d_non'][i], data['index_non'][i]];    
                 var chart2 = new Highcharts.chart('grafik_opc_'+i, {
                 title: {
                      text: 'CLINKER FACTOR OPC '+data.company[i]
                    },
                  credits: {
                    enabled: false
                  },
                xAxis: {
                categories: data.kategori,
                title: {
                      text: ''
                    }
                },
                 yAxis: [{
                    lineWidth: 1,
                    title: {
                        text: ' '
                    },
                    plotLines: [{
                    value: data.min_3d,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '3D'
                    }
                }, {
                    value: data.min_7d,
                    color: 'red',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '7D'
                    }
                }, {
                    value: data.min_28d,
                    color: 'blue',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '28D'
                    }
                }]
                }, {
                    lineWidth: 1,
                    opposite: true,
                    title: {
                        text: ' '
                    }
                }],  
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  },

                  plotOptions: {
                    series: {
                      label: {
                        connectorAllowed: false
                      }
                    }
                  },colors: [
                                    '#05354d',
                                    '#f7ca78',
                                    '#7bc8a4',
                                    '#93648d',
                                    '#4cc3d9',
                                    '#1f7f91',
                                    '#074b6d',
                                    '#d18700',
                                    '#ffdd9e',
                                ],

                          series: dataMerge,

                          responsive: {
                            rules: [{
                              condition: {
                                maxWidth: 500
                              },
                              chartOptions: {
                                legend: {
                                  layout: 'horizontal',
                                  align: 'center',
                                  verticalAlign: 'bottom'
                                }
                              }
                            }]
                          }
                        },
                    );
                    
                    var chart3 = new Highcharts.chart('grafik_nonopc_'+i, {
                     title: {
                          text: 'CLINKER FACTOR NON OPC '+data.company[i]
                        },
                      credits: {
                        enabled: false
                      },
                    xAxis: {
                    categories: data.kategori,
                    title: {
                          text: ''
                        }
                    },
                     yAxis: [{
                        lineWidth: 1,
                        title: {
                            text: ' '
                        },
                    plotLines: [{
                        value: data.min_3d_non,
                        color: 'green',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: '3D'
                        }
                    }, {
                        value: data.min_7d_non,
                        color: 'red',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: '7D'
                        }
                    }, {
                        value: data.min_28d_non,
                        color: 'blue',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: '28D'
                        }
                    }]
                    }, {
                        lineWidth: 1,
                        opposite: true,
                        title: {
                            text: ' '
                        }
                    }],
                      legend: {
                          layout: 'horizontal',
                          align: 'center',
                          verticalAlign: 'bottom'
                      },

                      plotOptions: {
                        series: {
                          label: {
                            connectorAllowed: false
                          }
                        }
                      },colors: [
                                        '#05354d',
                                        '#f7ca78',
                                        '#7bc8a4',
                                        '#93648d',
                                        '#4cc3d9',
                                        '#1f7f91',
                                        '#074b6d',
                                        '#d18700',
                                        '#ffdd9e',
                                    ],

                              series: dataMergeNon,

                              responsive: {
                                rules: [{
                                  condition: {
                                    maxWidth: 500
                                  },
                                  chartOptions: {
                                    legend: {
                                      layout: 'horizontal',
                                      align: 'center',
                                      verticalAlign: 'bottom'
                                    }
                                  }
                                }]
                              }
                            },
                        );
                    
             }
                
                
            },
            error: function () {
                
              console.log('Save error');
            }
          });
           
     }
     
     function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
          function grafik_all(tipe){
                var start = $("#start").val();
                var end = $("#end").val();
              $.ajax({
                url: "<?php echo site_url("data_table/grafik_clinker_factor");?>",
                type: "POST",
                 data: {"start": start, "end": end, "tipe": tipe}, 
                dataType: "JSON",
                success: function (data) {                 
                  var chart2 = new Highcharts.chart('grafik_'+tipe, {
                 title: {
                      text: 'CLINKER FACTOR '+tipe 
                    },
                  credits: {
                    enabled: false
                  },
                xAxis: {
                categories: data.kategori,
                title: {
                      text: ''
                    }
                },
                  yAxis: {
                    title: {
                      text: 'skor'
                    },
                  },
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  },

                  plotOptions: {
                    series: {
                      label: {
                        connectorAllowed: false
                      }
                    }
                  },colors: [
                                    '#05354d',
                                    '#f7ca78',
                                    '#7bc8a4',
                                    '#93648d',
                                    '#4cc3d9',
                                    '#1f7f91',
                                    '#074b6d',
                                    '#d18700',
                                    '#ffdd9e',
                                ],

                          series: data.grafik,

                          responsive: {
                            rules: [{
                              condition: {
                                maxWidth: 500
                              },
                              chartOptions: {
                                legend: {
                                  layout: 'horizontal',
                                  align: 'center',
                                  verticalAlign: 'bottom'
                                }
                              }
                            }]
                          }
                        },
                    );
                  } 
                  
                  
                });
              
          }
          
</script>
