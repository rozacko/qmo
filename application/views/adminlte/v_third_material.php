   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Third Material
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">


          <div class="box" id="formArea" style="display: none;">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Form Third Material</h3>
              </div>

              <span id="saving" style="display:none;">
                <img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
              </span>

              <div class="box-body">
                  <div class="form-group clearfix">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>KODE MATERIAL</label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control hidden" name="ID_MATERIAL" id="ID_MATERIAL" placeholder="Id Material" required >
                      <input type="text" class="form-control" name="KODE_MATERIAL" id="KODE_MATERIAL" placeholder="Kode Material" required >
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>NAME MATERIAL </label><span  style="color:red; float: right;">(*)</span>
                      <input type="text" class="form-control" name="NAME_MATERIAL" id="NAME_MATERIAL" placeholder="Name Material" required >
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>Component Non - H<sub>2</sub>O </label><span  style="color:red; float: right;">(*)</span>                
                      <select id="COMP_RELATION" name="COMP_RELATION" class="form-control select2">
                        <?php  foreach($this->list_component as $component): ?>
                          <option value="<?php echo $component->ID_COMPONENT;?>"><?php echo '<b>'.$component->KD_COMPONENT.'</b> ( <i>'.$component->NM_COMPONENT.'</i> )';?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-group clearfix">
                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>H<sub>2</sub>O Percentage  (%) </label><span  style="color:red; float: right;">(*)</span>
                      <div class="input-group">
                        <input type="number" id="H20_PERCENT" name="H20_PERCENT" min="0" placeholder="Input Value (%)" class="form-control">
                        <span class="input-group-addon">%</span>
                      </div>
                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4">
                      <label>Percentage Component Non - H<sub>2</sub>O (%) </label><span  style="color:red; float: right;">(*)</span>
                      <div class="input-group">
                        <input type="number" id="COMP_PERCENT" name="COMP_PERCENT" min="0" placeholder="Input Value (%)" class="form-control">
                        <span class="input-group-addon">%</span>
                      </div>
                    </div>
                  </div>
              </div>

              <div class="box-footer">
                <button type="button" id="AddArea" class="btn btn-primary"><i class="fa fa-save"></i>  &nbsp;Submit</button>
                <button type="button" id="UpdateArea" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                <button type="button" class="btn btn-default btnCancel">Cancel</button>
              </div>
            </div>
          </div>
    
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">List Data Third Material</h3>
                <div class="input-group input-group-sm" style="width: 150px; float: right;">
                  <a id="AddNew" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-success btn-sm"><i class="fa fa-plus"></i> Create New</a>
                </div>
                <div class="input-group input-group-sm" style="width: 40px; float: right;">
                  <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
              </div>

              <div class="box-body">
                <table  id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th width="5%">No. </th>
                        <th width="*">KODE MATERIAL</th>
                        <th width="*">NAME MATERIAL</th>
                        <th width="5%">ACTION</th>
                      </tr>
                    </thead>
                  </table>
              </div>

              <div class="box-body">
                <div class="box box-danger box-solid col-sm-4 col-md-4 col-lg-4">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Third Material Preview </h3>
                    <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div>
                  </div>
                  <div class="box-body" style="">
                    <div id="example1" class="hot handsontable htColumnHeaders" ></div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<script>

$(document).ready(function(){


  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();

  var oTable;

  var example1 = document.getElementById('example1'), hot1;

  loadtabelinput()


  reloadtabel();  
  loadTypeProductList(); 

  $(document).on('click',"#AddNew",function () {
    $('#saving').show();

    $('#formArea').show();
    $('#viewArea').hide();

    $('#UpdateArea').hide();
    $('#AddArea').show();

    $('#ID_MATERIAL').val('');
    $('#KODE_MATERIAL').val('');
    $('#NAME_MATERIAL').val('');

    $('#saving').hide();

  });

  $(document).on('click',"#ReloadData",function () {
    reloadtabel();
    loadtabelinput();
  });

  $(document).on('click',"#AddArea",function () {
    $('#saving').show();

    if ($('#KODE_MATERIAL').val() != '' && $('#NAME_MATERIAL').val() != '' && $('#H20_PERCENT').val() != '' && $('#COMP_RELATION').val() != '' && $('#COMP_PERCENT').val() != '') {
      $.ajax({
        url: "<?php echo site_url("third_material/save_third_material");?>",
        data: {"KODE_MATERIAL": $('#KODE_MATERIAL').val(), "NAME_MATERIAL": $('#NAME_MATERIAL').val(), "H2O_PERCENTAGE": $('#H20_PERCENT').val(), "ID_COMPONENT": $('#COMP_RELATION').val(), "COMP_PERCENTAGE": $('#COMP_PERCENT').val(), "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();

            $("#ReloadData").click();

          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }              
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });
    } else {
      $("#a-notice-error").click();
      if ($('#KODE_MATERIAL').val() == '') {
        $('#KODE_MATERIAL').focus();
      } else if ($('#NAME_MATERIAL').val() == '') {
        $('#NAME_MATERIAL').focus();
      } else if ($('#H20_PERCENT').val() == '') {
        $('#H20_PERCENT').focus();
      } else if ($('#COMP_RELATION').val() == '') {
        $('#COMP_RELATION').focus();
      } else if ($('#COMP_PERCENT').val() == '') {
        $('#COMP_PERCENT').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',"#UpdateArea",function () {
    $('#saving').show();

    if ($('#ID_MATERIAL').val() != '' && $('#KODE_MATERIAL').val() != '' && $('#NAME_MATERIAL').val() != '' && $('#H20_PERCENT').val() != '' && $('#COMP_RELATION').val() != '' && $('#COMP_PERCENT').val() != '') {
      $.ajax({
        url: "<?php echo site_url("third_material/update_third_material");?>",
        data: {"ID_MATERIAL": $('#ID_MATERIAL').val(), "KODE_MATERIAL": $('#KODE_MATERIAL').val(), "NAME_MATERIAL": $('#NAME_MATERIAL').val(), "H2O_PERCENTAGE": $('#H20_PERCENT').val(), "ID_COMPONENT": $('#COMP_RELATION').val(), "COMP_PERCENTAGE": $('#COMP_PERCENT').val(),  "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          if (res['status'] == true) {
            $("#a-notice-success").data("text", res.msg);
            $("#a-notice-success").click();
            $('#formArea').hide();
            $('#viewArea').show();

            $("#ReloadData").click(); 
          } else {
            $("#a-notice-error").data("text", res.msg);
            $("#a-notice-error").click();
          }                
        },
        error: function () {
          $("#a-notice-error").click();
        }
      });      
    } else {
      $("#a-notice-error").click();
      if ($('#ID_MATERIAL').val() == '') {
        $('#ID_MATERIAL').focus();
      } else if ($('#KODE_MATERIAL').val() == '') {
        $('#KODE_MATERIAL').focus();
      } else if ($('#NAME_MATERIAL').val() == '') {
        $('#NAME_MATERIAL').focus();
      } else if ($('#H20_PERCENT').val() == '') {
        $('#H20_PERCENT').focus();
      } else if ($('#COMP_RELATION').val() == '') {
        $('#COMP_RELATION').focus();
      } else if ($('#COMP_PERCENT').val() == '') {
        $('#COMP_PERCENT').focus();
      }
    }
    $('#saving').hide();
  });

  $(document).on('click',".btnCancel",function () {
    $('#saving').show();
    $('#formArea').hide();
    $('#viewArea').show();

    $('#ID_MATERIAL').val('');
    $('#KODE_MATERIAL').val('');
    $('#NAME_MATERIAL').val('');
    $('#H20_PERCENT').val('');
    $('#COMP_RELATION').val('');
    $('#COMP_PERCENT').val('');
    $("#ReloadData").click();
    $('#saving').hide();      
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }

  /** btEdit Click **/
  $(document).on('click',".btEdit",function () {
    $('#saving').show();
    $('#formArea').show();
    $('#viewArea').hide();  

    $('#UpdateArea').show();
    $('#AddArea').hide(); 

    var data = oTable.row($(this).parents('tr')).data();

    $('#ID_MATERIAL').val(data['ID_MATERIAL']);
    $('#KODE_MATERIAL').val(data['KODE_MATERIAL']);
    $('#NAME_MATERIAL').val(data['NAME_MATERIAL']);
    $('#H20_PERCENT').val(data['H2O_PERCENTAGE']);
    $('#COMP_RELATION').val(data['ID_COMPONENT']);
    $('#COMP_PERCENT').val(data['COMP_PERCENTAGE']);
    $('#saving').hide();
  });

  /** btDelete Click **/
  $(document).on('click',".btDelete",function () {
    var data = oTable.row($(this).parents('tr')).data();
    $.confirm({
        title: "Remove Component",
        text: "This sample area will be removed. Are you sure?",
        confirmButton: "Remove",
        confirmButtonClass: "btn-danger",
        cancelButton: "Cancel",
        confirm: function() {
          $.ajax({
            url: "<?php echo site_url("third_material/delete_third_material");?>",
            data: {"ID_MATERIAL": data['ID_MATERIAL'], "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
            dataType: 'json',
            type: 'POST',
            success: function (res) {
              if (res['status'] == true) {
                $("#a-notice-success").data("text", res.msg);
                $("#a-notice-success").click();
              } else {
                $("#a-notice-error").data("text", res.msg);
                $("#a-notice-error").click();
              }

              $("#ReloadData").click();

            },
            error: function () {
              $("#a-notice-error").click();
            }
          });
        },
        cancel: function() {
        }
    });
  });

  function reloadtabel() {
    // body...
    $('#ReloadData').hide();
    $('#AddNew').hide();
    oTable = $('#dt_tables').DataTable({
      destroy: true,
      processing: true,
      serverSide: true,
      select: true,
      buttons: [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
      ajax: {
        url: '<?php echo site_url("third_material/third_material_list");?>',
        type: "POST"
      },
      columns: [
        {"data": "ID_MATERIAL", "width": 50},
        // {"data": "NAME_COMPETITOR"},
        {"data": "KODE_MATERIAL"},
        {"data": "NAME_MATERIAL"},
        {"data": "ID_MATERIAL", "width": 100,
          "mRender": function(row, data, index){
            return '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-pencil-square-o"></i> Edit</button><button title="Delete" class="btDelete btn btn-danger btn-xs delete" type="button"><i class="fa fa-trash-o"></i> Delete</button>';

          }
        },
      ]
    });
    
    $('#ReloadData').show();
    $('#AddNew').show();
  }

  function loadTypeProductList() {
	  var typeproduct = $('#TYPE_PRODUCT');
	  $.getJSON('<?php echo site_url("input_sample_quality/ajax_get_type_product");?>', function (result) {
	    var values = result;
	    typeproduct.find('option').remove();
	    if (values != undefined && values.length > 0) {
	      typeproduct.css("display","");
	      $(values).each(function(index, element) {
	        typeproduct.append($("<option></option>").attr("value", element.ID_PRODUCT).text(element.KD_PRODUCT));
	      });
	    }else{
	      typeproduct.find('option').remove();
	      typeproduct.append($("<option></option>").attr("value", '00').text("NO TYPE"));
	    }
	  });
	}


  function loadtabelinput() {

    if (hot1) {

      hot1.destroy();

    }  

    $.getJSON('<?php echo site_url("third_material/get_third_material_percentage");?>', function (result) {

      if (result && result['Status'] && result['Data']) {

        hot1 = new Handsontable(example1, {
          data: result['Data']['Body'],
          autoColumnSize : true,
          className: "htCenter",
          filters: true,
          dropdownMenu: ['alignment','filter_by_condition', 'filter_by_value', 'filter_action_bar'],
          columnSorting: true,
          colWidths: result['Data']['ColWidhts'],
          manualColumnFreeze: true,
          manualColumnResize: true,
          colHeaders: result['Data']['Header'],
          columns: result['Data']['Col Options']
        });

        $('#ReloadData').show();

      } else {        

        $('#ReloadData').show();
      }
      

    });    

  }

  function getContentData() {
      return [["TRASS", , , , ],["LIMESTONE", , , , ],["GYPSUM", , , , ]];
  }

  function getRowOptions() {
    var initial_coloptions = [
      {
        type: 'text',
        readOnly: true
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      },
      {
        type: "numeric"
      }
    ];

    return initial_coloptions;
  }

  function getColHeader() {
    var columnlist = ['Third Material', 'H20', 'LOi', 'Insol', 'SO3'];
    return columnlist;
  }


  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
