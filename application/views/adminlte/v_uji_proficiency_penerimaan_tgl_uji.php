<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Input Tanggal Uji Sample Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
                <div class="box-header with-border">
                    <b>Periode Proficiency: </b> <?= $this->list_proficiency[0]->TITLE_PP;?> [<?= $this->list_proficiency[0]->GROUP_PP;?> - <?= $this->list_proficiency[0]->YEAR_PP;?>] | <b>Commodity: </b> <?= $this->list_proficiency[0]->NAMA_SAMPLE; ?> | <b>Lab: </b> <?= $this->list_proficiency[0]->NAMA_LAB; ?> | <b>PIC: </b> <?= $this->list_proficiency[0]->FULLNAME; ?>
                </div>
				<div class="box-body">
                    <div class="row">
                        <form action="<?= base_url('uji_proficiency_penerimaan/action_add_tgl_uji/').$this->input->get('id') ?>" method="POST">
                            <div class="form-group">
                                <div class="col-sm-6 clearfix">
                                    <label>TANGGAL START UJI *</label>
                                    <input type="text" id="datepicker" class="form-control" name="START_TGL_UJI" value="<?= date('d/m/Y') ?>" required >
                                </div>
                                <!-- <div class="col-sm-6 clearfix">
                                    <label>TANGGAL UJI AKHIR</label>
                                    <input type="text" id="datepicker2" class="form-control" name="END_TGL_UJI" required >
                                </div> -->
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 clearfix">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    $(document).ready(function(){
        $("#ID_KOMODITI").change();
        $("#datepicker, #datepicker2").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    });

    $("form").on("submit", function(e) {
		e.preventDefault();
        
		var post_url = $(this).attr("action");
		var request_method = $(this).attr("method");
        var form_data = new FormData(this);
		
		$.ajax({
            processData: false,
            contentType: false,
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
				alert("Input Tanggal Uji Sampling Successfully");
                window.location = "<?= base_url('uji_proficiency_penerimaan') ?>"
			} else {
				alert("Input Tanggal Uji Sampling Failed")
			}
		});
	});
</script>