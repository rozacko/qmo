
<section class="content-header">
    <h1>
        Form Temuan Observasi (Akurasi Labor SIG)
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <form class="form-inline">
                        <?php if($this->PERM_WRITE): ?>
                            <div class="input-group input-group-sm" style="width: 150px; ">
                                <a data-toggle="modal" data-target="#addModal" type="button" class="btn btn-block btn-primary btn-sm">Create new</a>
                            </div>
                        <?PHP endif; ?>
                    </form>
                    <hr/>
                </div>

                <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover dt-responsive nowrap "
                            cellspacing="0"
                            width="100%">
                        <thead>
                        <tr>
                            <th width="1">No.</th>
                            <th >Nama Panel</th>
                            <th width="1">Kode</th>
                            <th >Parent</th>
                            <th >Tipe</th>
                            <th >Level</th>
                            <th ></th>
                        </tr>
                        </thead>
                        <tbody style="font-weight: normal;">
                        <?php
                        $count = 1;
                        foreach ($this->list_data as $dt) { ?>
                            <tr>
                                <td><?= $count++; ?></td>
                                <td><?= $dt->NAMA_KATEGORI; ?></td>
                                <td><?= $dt->KODE; ?></td>
                                <td><?= $dt->PARENT_NAME; ?></td>
                                <td><?= ($dt->JENIS_KATEGORI == "NAV") ? "Menu/Sub Menu" : "Isian Formulir"; ?></td>
                                <!-- <td>
					</td> -->
                                <td>
                                    <a data-toggle="modal" data-target="#editModal<?= $dt->ID;?>"><button title="Detail" class="btEdit btn btn-warning btn-xs" type="button"><i class="fa fa-edit "></i> edit</button></a>
                                    <a href="<?php echo site_url("acclab_masterFormObservasi/deleteForm/{$dt->ID}");?>" onClick="return doconfirm();"><button title="Detail" class="btEdit btn btn-danger btn-xs" type="button"><i class="fa fa-trash "></i> delete</button></a>
                                </td>
                            </tr>
                            <div id="editModal<?= $dt->ID;?>" class="modal fade" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <form role="form" method="POST" action="<?php echo site_url("acclab_masterFormObservasi/updateForm/") ?>" >
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b>Update Field Akurasi Labor:</b> </h4>
                                            </div>
                                            <div class="modal-body">
                                                <input type="hidden" value="<?php echo $dt->ID ?>" name="ID">
                                                <div class="form-group c-group after-add-more">
                                                    <div class="col-sm-6">
                                                        <label>Nama Form </label>
                                                        <input type="text" value="<?php echo $dt->NAMA_KATEGORI ?>" class="form-control" name="NAMA_FORM" placeholder="Nama Formulir"  >
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Parent</label>
                                                        <select class="form-control select2" NAME="ID_PARENT" id="ID_PARENT" placeholder="Select a Parent ...">
                                                            <option value="0">No Parent</option>
                                                            <?php  foreach($this->list_panel as $panel): ?>
                                                                <option value="<?php echo $panel->ID ?>" <?php echo ($dt->PARENT_ID == $panel->ID) ? "selected" : ""; ?>><?php echo "{$panel->KODE} - {$panel->NAMA_KATEGORI}" ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group c-group after-add-more">
                                                    <div class="col-sm-6">
                                                        <label>Kode </label>
                                                        <input type="text" class="form-control" value="<?php echo $dt->KODE ?>" name="KODE" placeholder="Kode Formulir E.g: 1A, 1-A, 1.A etc"  >
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <label>Kode </label>
                                                        <select class="form-control select2" NAME="JENIS" id="JENIS" placeholder="Select a Type ...">
                                                            <option value="NAV" <?php echo ($dt->JENIS_KATEGORI == "NAV") ? "selected" : ""; ?>>Komponen/Sub Komponen</option>
                                                            <option value="FIL" <?php echo ($dt->JENIS_KATEGORI == "FIL") ? "selected" : ""; ?>>Form Field (Isian Formulir)</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="margin-top: 2em;">
                                                <button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
                                                <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <form role="form" method="POST" action="<?php echo site_url("acclab_masterFormObservasi/addForm/") ?>" >
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Tambah Field Akurasi Labor:</b> </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group c-group after-add-more">
                        <div class="col-sm-6">
                            <label>Nama Form </label>
                            <input type="text" class="form-control" name="NAMA_FORM" placeholder="Nama Formulir"  >
                        </div>
                        <div class="col-sm-6">
                            <label>Parent</label>
                            <select class="form-control select2" NAME="ID_PARENT" id="ID_PARENT" placeholder="Select a Parent ...">
                                <option value="0" selected>No Parent</option>
                                <?php  foreach($this->list_panel as $panel): ?>
                                    <option value="<?php echo $panel->ID ?>"><?php echo "{$panel->KODE} - {$panel->NAMA_KATEGORI}" ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group c-group after-add-more">
                        <div class="col-sm-6">
                            <label>Kode </label>
                            <input type="text" class="form-control" name="KODE" placeholder="Kode Formulir E.g: 1A, 1-A, 1.A etc"  >
                        </div>
                        <div class="col-sm-6">
                            <label>Kode </label>
                            <select class="form-control select2" NAME="JENIS" id="JENIS" placeholder="Select a Type ...">
                                <option value="NAV" selected>Komponen/Sub Komponen</option>
                                <option value="FIL">Form Field (Isian Formulir)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="margin-top: 2em;">
                    <button type="submit" class="btn btn-primary" style="margin-top: 2em;">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-top: 2em;">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
    <a  id="a-notice-error"
        class="notice-error"
        style="display:none";
        href="#"
        data-title="Something Error"
        data-text="<?php echo $notice->error; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->error; ?>');
    </script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
    <a  id="a-notice-success"
        class="notice-success"
        style="display:none";
        href="#"
        data-title="Done!"
        data-text="<?php echo $notice->success; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->success; ?>');
    </script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
    .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
    $(document).ready(function(){

        /** DataTables Init **/
        var table = $("#dt_tables").DataTable();
    });
</script>
<script>
    function doconfirm(){
        job=confirm("Are you sure you want to delete data?");
        if(job!=true){
            return false;
        }
    }
</script>