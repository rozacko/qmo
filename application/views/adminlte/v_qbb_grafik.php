<style>
.highcharts-figure, .highcharts-data-table table {
  min-width: 360px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>
<section class="content-header">
    <h1>Grafik Kualitas Batubara</h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row ">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <form id="formData">
                        <div class="form-group row">
                            <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                                <label for="month_start">FROM</label>
                                <SELECT style="width:auto;" class="form-control select2" name="MONTH_START" id='month_start'>
                                <?php for($i=1;$i<=12;$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            <div class="form-group col-sm-2 col-sm-2 year">
                                <label for="year_start"></label>
                                <SELECT style="width:auto;" class="form-control select2" name="YEAR_START" id='year_start'>
                                <?php for($i=2016;$i<=date("Y");$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            
                            <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                                <label for="month_end">TO</label>
                                <SELECT style="width:auto;" class="form-control select2" name="MONTH_END" id='month_end'>
                                <?php for($i=1;$i<=12;$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            <div class="form-group col-sm-2 col-sm-2 year">
                                <label for="year_end">YEAR</label>
                                <SELECT style="width:auto;" class="form-control select2" name="YEAR_END" id='year_end'>
                                <?php for($i=2016;$i<=date("Y");$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="OPT_COMPANY">COMPANY</label><br>
                                <input type="checkbox" class="OPT_COMPANY_ALL" value="semuanya"> Check All
                                <?php  foreach($this->list_company as $company):
                                    $checked  = ($this->USER->ID_COMPANY==$company->ID_COMPANY) ? 'checked':NULL;
                                ?>
                                <div>
                                    <label>
                                        <input type="checkbox" NAME="OPT_COMPANY[]" class="minimal OPT_COMPANY" value="<?php echo $company->KD_COMPANY;?>" <?php echo $checked;?>>
                                        <?php echo $company->NM_COMPANY;?>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <!-- plant type radio list -->
                            <div id="plant" style="display:none;" class="col-sm-2">
                                <label for="OPT_PLANT">PLANT</label><br>
                                <div id="opt_plant" style="height: 250px;overflow-y: scroll;"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-sm-6 col-sm-4">
                                <button class="btn-primary" name="load" id="btLoad">Load Data</button>
                            </div>
                        </div>
                        <hr/>
                    </form>
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div id="qbb_chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
<a id="a-notice-error" class="notice-error" style="display:none"; href="#" data-title="Alert" data-text=""></a>
<div class="modal_load"><!-- Loading modal --></div>

	
<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
  $(document).ready(function(){

  });

  $(document).on('click', '.OPT_COMPANY:checkbox', function(e) {
    clear_plant_checkbox_list();
    load_plant_checkbox_list();
  });

  function load_plant_checkbox_list(){
    var company_ids = [];
    company_ids = $('.OPT_COMPANY:checkbox:checked').map(function(idx) {
      return this.value
    }).get();

    $.ajax({
      url: '<?php echo site_url("qbb_grafik/ajax_get_plant_by_company");?>',
      method: "GET",
      dataType: "JSON",
      data: {
        company_ids: company_ids
      },
      success: function(response) {
        if ( response.length == 0 ) return true;
        addCheckbox('semua', "OPT_PLANT_ALL", "opt_plant", 'Check All');
        $.each(response, function(idx, elm) {
          addCheckbox(elm.KD_PLANT, "OPT_PLANT", "opt_plant", elm.NAMA_PLANT, 'checkbox');
        });
        show_plant_checkbox_list();
      },
      error: function(jqXHR, textStatus, errorThrown) {
        alert(textStatus)
      }
    })
  }

  $(document).on('click', '.OPT_COMPANY_ALL:checkbox', function(e) {
    $('input.OPT_COMPANY[type=checkbox]').prop('checked', this.checked);
    clear_plant_checkbox_list();
    load_plant_checkbox_list();
  })

  $(document).on('click', '.OPT_PLANT_ALL:checkbox', function(e) {
    $('input.OPT_PLANT[type=checkbox]').prop('checked', this.checked);
  })

  function show_plant_checkbox_list() {
    $("#plant").show()
  }

  function clear_plant_checkbox_list() {
    $("#plant").hide();
    $("#opt_plant :checkbox").remove();
    $(".opt_plant").remove();
    $('label[for="OPT_PLANT[]"]').remove();
  }

  $(document).on('click', '#btLoad', function(e){
    e.preventDefault();

    var month_start   = $("#month_start").val();
    var year_start    = $("#year_start").val();
    var month_end     = $("#month_end").val();
    var year_end      = $("#year_end").val();
        
    var companys = $('.OPT_COMPANY:checkbox:checked').map(function() {
      return this.value;
    }).get();
        
    var plants = $('.OPT_PLANT:checkbox:checked').map(function() {
      return this.value;
    }).get();

    if ( [companys.length, plants.length].indexOf(0) != -1 ) {
      $("#a-notice-error").data("text", 'Either Company or Plant not selected yet');
      $("#a-notice-error").click();
      return false;
    }

    var date_start = new Date;
    date_start.setDate(1); date_start.setMonth(month_start-1); date_start.setYear(year_start);
    var date_end = new Date
    date_end.setDate(1); date_end.setMonth(month_end-1); date_end.setYear(year_end);
    if ( date_end < date_start ) {
      $("#a-notice-error").data("text", 'End period must be after or equal start period');
      $("#a-notice-error").click();
      return false;
    }

    get_chart(month_start, year_start, month_end, year_end, companys, plants);
  })

  function get_chart(month_start, year_start, month_end, year_end, companys, plants){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: '<?php echo site_url("qbb_grafik/get_chart");?>',
      data: {
        month_start: month_start,
        year_start: year_start,
        month_end: month_end,
        year_end: year_end,
        companys: companys,
        plants: plants
      },
      success: function(response){
        if(response.status == "200"){
          var flex = 12 / response.data.length;
          var html = '';
          var data = response.data;

          data.forEach(function(item, index){
            html += '<div class="col-xs-'+flex+'"><div class="box"><div class="box-body"><div id="qbb_'+index+'"></div></div></div></div>';
          });
          $("#qbb_chart").html(html);

          data.forEach(function(item, index){
            Highcharts.chart('qbb_'+index, {
              chart: {
                type: 'line'
              },
              title: {
                text: item.title
              },
              subtitle: {
                text: response.dt_y_axis[0]+' s/d '+response.dt_y_axis[(response.dt_y_axis.length - 1)]
              },
              xAxis: {
                categories: response.dt_y_axis
              },
              yAxis: {
                title: {
                  text: 'Kualitas Batubara'
                }
              },
              plotOptions: {
                line: {
                  dataLabels: {
                    enabled: true
                  },
                  enableMouseTracking: false
                }
              },
              series: item.value
            });
          });
          
          
        } else {
          $("#a-notice-error").data("text", 'Data not found!');
          $("#a-notice-error").click();
        }
        
      }
    });
  }

  function addCheckbox(val, namaFor, divid, txt, tipe) {
    tipe = tipe || 'checkbox';
    var container = document.getElementById(divid);
    var divgrup   = document.createElement('div');
        divgrup.className = divid;
    var input = document.createElement('input');
        input.type = tipe;
        input.className = namaFor;
        input.name = namaFor+"[]";
        input.id = namaFor + "-" + val
        input.value = val;
    var label = document.createElement('label');
        label.htmlFor = input.id;
            
    container.appendChild(divgrup);
    divgrup.appendChild(label);
    label.appendChild(input);
    label.appendChild(document.createTextNode("    "+txt));
  }

  $(".notice-error").confirm({
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

</script>


