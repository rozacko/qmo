
<section class="content-header">
    <h1>
        Tindak Lanjut Observasi (Akurasi Labor SIG)
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">

            <div class="box">

                <div class="box-header">
                    <form class="form-inline">

                    </form>
                    <!--                    <hr/>-->

                </div>

                <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover dt-responsive nowrap "
                            cellspacing="0"
                            width="100%">
                        <thead>
                        <tr>
                            <th width="1">No.</th>
                            <th >Nama Akurasi Labor</th>
                            <th width="1">Tahun </th>
                            <th >Jenis Pelaksanaan</th>
                            <th>Plant</th>
                            <th >PIC Observasi</th>
                            <th >PIC Plant</th>
                            <th >Start Date</th>
                            <th >End Date</th>
                            <th width="1">Status</th>
                            <th ></th>
                        </tr>
                        </thead>
                        <tbody style="font-weight: normal;">
                        <?php
                        $count = 1;
                        foreach ($this->list_data as $dt) { ?>
                            <tr>
                                <td><?= $count++; ?></td>
                                <td><?= $dt->NAMA;?></td>
                                <td><?= $dt->TAHUN;?></td>
                                <td><?= $dt->JENIS;?></td>
                                <td><?= $dt->NAMA_LAB;?> </td>
                                <td><?= $dt->FULLNAME_OBSERVASI;?> </td>
                                <td><?= $dt->FULLNAME_PLANT;?> </td>
                                <td><?= $dt->START_DATE;?> </td>
                                <td><?= $dt->END_DATE;?> </td>
                                <td><?= $dt->STATUS;?> </td>
                                <!-- <td>
					</td> -->
                                <td>
                                    <a href="#"><button title="Input Temuan & Rekomendasi" class="btEdit  btn btn-warning btn-xs" type="button"><i class="fa fa-arrows "></i> Detail</button></a>
                                    <a href="#"><button title="Input Temuan & Rekomendasi" class="btEdit  btn btn-success btn-xs" type="button"><i class="fa fa-calendar "></i> Input Tindak Lanjut</button></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
    <a  id="a-notice-error"
        class="notice-error"
        style="display:none";
        href="#"
        data-title="Something Error"
        data-text="<?php echo $notice->error; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->error; ?>');
    </script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
    <a  id="a-notice-success"
        class="notice-success"
        style="display:none";
        href="#"
        data-title="Done!"
        data-text="<?php echo $notice->success; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->success; ?>');
    </script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
    .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
    $(document).ready(function(){

        /** DataTables Init **/
        var table = $("#dt_tables").DataTable();
    });
</script>
<script>
    function doconfirm(){
        job=confirm("Are you sure you want to delete data?");
        if(job!=true){
            return false;
        }
    }
</script>