<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Form Pengujian Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="input-group input-group-sm" style="width: 150px; ">
						<a href="#" id="btn-new" type="button" class="btn btn-block btn-primary btn-sm">Create New</a>
					</div>
				</div>
				<div class="box-body">
					<div class="table-responsive">
						<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="3%">No</th>
									<th>Form Name</th>
									<th width="10%"></th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-default fade" id="modal-form">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<form role="form" method="POST" >
						<div class="box-body" style="background-color:#c5d5ea;">
							<div class="form-group">
								<div class="col-sm-12 clearfix">
									<label>TITLE</label>
									<input type="hidden" class="form-control" id="ID_FORM" name="ID_FORM">
									<input type="text" class="form-control" id="TITLE" name="TITLE" placeholder="Title Form Pengujian" REQUIRED >
								</div>
							</div>
							<div class="form-group">                  
								<div class="col-sm-12 clearfix">
									<label>KOMODITI</label>
									<select class="form-control" id="ID_KOMODITI" name="ID_KOMODITI">
									<?php  foreach($this->list_komoditi as $komoditi): ?>
									<option value="<?php echo $komoditi->ID_SAMPLE;?>"><?php echo $komoditi->NAMA_SAMPLE;?></option>
									<?php endforeach; ?>
									</select>
								</div>                
							</div>
							<!-- <div class="form-group">                  
								<div class="col-sm-12 clearfix">
									<label>TIPE</label>
									<select class="form-control" id="TIPE" name="TIPE">
										<option value="FISIKA">FISIKA</option>
										<option value="KIMIA">KIMIA</option>
									</select>
								</div>                
							</div> -->
						</div>
						<div class="box-footer">
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
            </div>
		</div>
	</div>
</section>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
	$(document).ready(function(){
		var table = $('#dt_tables').DataTable({ 
            "processing" : true, 
            "serverSide" : true, 
            "destroy" : true,
            "autoWidth" : false,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('form_pengujian/get_list')?>",
                "type": "POST"
            },
            "columns" : [
                {data: 'NO', name: 'NO', orderable: false, searchable: false, width:'3%'},
                {data: 'TITLE', name: 'TITLE', render: function(e, t, f){
					return f.TITLE+" - "+f.NAMA_SAMPLE
				}},
                {data: 'NO', name: 'NO', orderable: false, searchable: false, render: function(e, t, f){
                    return '<center>'+
                    '<button id="btn-edit" data-ID_FORM="'+f.ID_FORM+'" class="btn btn-xs btn-icon icon-left btn-warning"><i class="fa fa-edit"></i> Edit</button>&nbsp;'+
                    '<button type="button" id="btn-delete" data-ID_FORM="'+f.ID_FORM+'" class="btn btn-xs btn-icon icon-left btn-danger"><i class="fa fa-trash"></i> Delete</button>'+
                    '</center>';
                }},
            ],
        });
	});

	$("#btn-new").on("click", function(e){
		$('form')[0].reset();
		$("#ID_FORM").val("");
		$(".modal-title").html("Add Form Pengujian");
		$("#modal-form").modal("show");
	});

	$("form").on("submit", function(e) {
		e.preventDefault();
		var id = $("#ID_FORM").val();
		var type = id == "" ? "Add" : "Edit";
		var post_url = id == "" ? "create" : "update/"+id;
		var request_method = $(this).attr("method");
		var form_data = $(this).serialize();
		
		$.ajax({
			url : '<?php echo site_url("form_pengujian") ?>'+'/'+post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
				alert(type+" Form Pengujian Successfully");
				$("#modal-form").modal("hide");
				$("#dt_tables").DataTable().ajax.reload();
			} else {
				alert(type+" Form Pengujian Failed")
			}
		});
	});

	$(document).on("click", "#btn-edit", function(e){
		var id = $(this).data("id_form");
		$.ajax({
			url : '<?php echo site_url("form_pengujian/detail") ?>/'+id,
		}).done(function(response){
			$("#ID_FORM").val(response[0]['ID_FORM']);
			$("#TITLE").val(response[0]['TITLE']);
			$("#TIPE").val(response[0]['TIPE']).change();
			$("#ID_KOMODITI").val(response[0]['ID_KOMODITI']).change();
			$(".modal-title").html("Edit Form Pengujian");
			$("#modal-form").modal("show");
		});
	})

	$(document).on("click", "#btn-delete", function(e){
		var id = $(this).data("id_form");
		var c = confirm("Apakah yakin ingin menghapus ?");
		if(c){
			$.ajax({
				url : '<?php echo site_url("form_pengujian/delete") ?>'+'/'+id,
				type: "POST",
			}).done(function(response){
				if(response == 1){
					alert("Delete Form Pengujian Successfully");
					$("#dt_tables").DataTable().ajax.reload();
				} else {
					alert("Delete Form Pengujian Failed")
				}
			});
		}
	});

	$(document).on("click", "#btn-pertanyaan", function(e){
		var id = $(this).data("id_form");
		window.location = "<?= base_url('pertanyaan') ?>?id="+id;
	});
</script>