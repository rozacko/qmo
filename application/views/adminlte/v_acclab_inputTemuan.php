
<section class="content-header">
    <h1>
        Input Temuan Observasi (Akurasi Labor SIG)
        <small></small>
    </h1>
</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <form role="form" method="POST" action="<?php echo site_url("acclab_temuanObservasi/submitTemuan/") ?>">

                <div class="box-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <?php 
                        $i = 1;
                        foreach ($this->list_data as $item) { ?>
                        <li class="nav-item <?php echo ($i == 1) ? "active" : ""; ?>">
                            <a class="nav-link" href="#content<?php echo $item["ID"]; ?>" role="tab" data-toggle="tab"><?php echo $item["KODE"] ?></a>
                        </li>
                        <?php $i++; } ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <?php
                        $j = 1;
                        foreach ($this->list_data as $x) { ?>
                        <div role="tabpanel" class="tab-pane fade in <?php echo ($j == 1) ? "active" : ""; ?>" id="content<?php echo $x["ID"]; ?>">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"><?php echo $x["NAMA_KATEGORI"]; ?></h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-sm-12">
                                        <br>
                                        <?php foreach ($x["FIELD_LIST"] as $field) { ?>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label><?php echo $field["NAMA_FIELD"]; ?> </label>
                                                <input type="text" class="form-control" name="field_<?php echo $field["ID"] ?>" placeholder="<?php echo $field["NAMA_FIELD"]; ?>"  >
                                            </div>
                                        </div>
                                        <?php $j++; } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div >
                        <button type="submit" onClick="return confirmSubmit();" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>

                </form>
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->

<!-- msg confirm -->
<?php if($notice->error != '' or $notice->error != null){ ?>
    <a  id="a-notice-error"
        class="notice-error"
        style="display:none";
        href="#"
        data-title="Something Error"
        data-text="<?php echo $notice->error; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->error; ?>');
    </script>

<?php } ?>

<?php if($notice->success != '' or $notice->success != null){ ?>
    <a  id="a-notice-success"
        class="notice-success"
        style="display:none";
        href="#"
        data-title="Done!"
        data-text="<?php echo $notice->success; ?>"
    ></a>
    <script>
        alert('<?php echo $notice->success; ?>');
    </script>
<?php } ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
    .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
    $(document).ready(function(){

        /** DataTables Init **/
        var table = $("#dt_tables").DataTable();
    });
</script>
<script>
    function doconfirm(){
        job=confirm("Are you sure you want to delete data?");
        if(job!=true){
            return false;
        }
    }

    function confirmSubmit(){
        job=confirm("Are you sure you want to Submit data?");
        if(job!=true){
            return false;
        }
    }
</script>