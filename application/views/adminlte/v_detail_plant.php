<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Master Detail Plant</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<div class="input-group input-group-sm" style="width: 150px; float: right;">
						<a href="<?= base_url('detail_plant/add') ?>" id="btn-new" type="button" class="btn btn-block btn-primary btn-sm">Input Detail Plant</a>
					</div>
                </div>
				<div class="box-body">
                    <div class="table-responsive">
						<table id="dt_tables" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th width="3%">No</th>
									<th>Company</th>
									<th>Plant</th>
									<th>Kode Detail Plant</th>
									<th>Nama Detail Plant</th>
									<th width="10%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
                </div>
			</div>
		</div>
	</div>
</section>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
	$(document).ready(function(){
		var table = $('#dt_tables').DataTable({ 
            "processing" : true, 
            "serverSide" : true, 
            "destroy" : true,
            "autoWidth" : false,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('detail_plant/get_list') ?>",
                "type": "POST"
            },
            "columns" : [
                {data: 'NO', name: 'NO', orderable: false, searchable: false, width:'3%'},
                {data: 'NM_COMPANY', name: 'NM_COMPANY'},
                {data: 'NM_PLANT', name: 'NM_PLANT'},
                {data: 'KD_DETAIL_PLANT', name: 'KD_DETAIL_PLANT'},
                {data: 'NM_DETAIL_PLANT', name: 'NM_DETAIL_PLANT'},
                {data: 'NO', name: 'NO', orderable: false, searchable: false, render: function(e, t, f){
					return '<center>'+
					'<a href="<?= base_url().'detail_plant/edit/' ?>'+f.ID_DETAIL_PLANT+'" class="btn btn-xs btn-icon icon-left btn-warning"><i class="fa fa-pencil"></i> Edit</a>&nbsp;'+
					'<button id="btDelete" data-ID_DETAIL_PLANT="'+f.ID_DETAIL_PLANT+'" class="btn btn-xs btn-icon icon-left btn-danger delete"><i class="fa fa-trash"></i> Delete</button>'+
					'</center>'; 
                }},
            ],
        });

		$(document).on("click", "#btn-input", function(e){
			var proficiency = $(this).data("id_proficiency");
			var id = $(this).data("id_penerima");
			window.location = "<?= base_url('uji_proficiency_penerimaan/add_tgl_uji') ?>?proficiency="+proficiency+"&id="+id;
		});
	});

	$(document).on('click',"#btDelete",function () {
		var data = $(this).data('id_detail_plant');
		var url = '<?php echo site_url("detail_plant/delete/");?>' + data;
		$.confirm({
			title: "Remove Detail Plant",
			text: "This detail plant will be removed. Are you sure?",
			confirmButton: "Remove",
			confirmButtonClass: "btn-danger",
			cancelButton: "Cancel",
			confirm: function() {
				window.location.href = url;
			},
			cancel: function() {

			}
		});
	});

	$(".delete").confirm({ 
		confirmButton: "Remove",
		cancelButton: "Cancel",
		confirmButtonClass: "btn-danger"
	});
</script>