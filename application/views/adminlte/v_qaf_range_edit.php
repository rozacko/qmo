   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Component Range Configuration for QAF
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
	
       <div class="row">
        <div class="col-xs-12">
				
		<!-- <form class="form-horizontal" method="post"  > -->
		<form class="form-horizontal" method="post">
		
          <div class="box">
            <div class="box-header with-border">

            	<!-- <input type="checkbox" onclick="aktifkan('1');"> -->
				
					<div class="box-body">
						<div class="form-group">
							<?php if($this->USER->ID_COMPANY): ?>
							<input type=hidden id="ID_COMPANY" value="<?php echo $this->USER->ID_COMPANY; ?>" />
							<?php else: ?>
							<div class="col-sm-2">
								<label>COMPANY</label>
								<select class="form-control select2" id="ID_COMPANY" name="ID_COMPANY">
									<?php  foreach($this->list_company as $company): ?>
									<option value="<?php echo $company->ID_COMPANY ?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":""; ?> ><?php echo $company->NM_COMPANY ?></option>
									<?php endforeach; ?>
								</select>							
							</div>
							<?php endif; ?>
							
							<div class="col-sm-3">
								<label>GROUPAREA</label>
								<select class="form-control select2" id="ID_GROUPAREA" NAME="ID_GROUPAREA" >
									<?php  foreach($this->list_grouparea as $grouparea): ?>
									<option value="<?php echo $grouparea->ID_GROUPAREA ?>" <?php echo ($this->ID_GROUPAREA == $grouparea->ID_GROUPAREA)?"SELECTED":""; ?> ><?php echo $grouparea->NM_GROUPAREA ?></option>
									<?php endforeach; ?>
								</select>							
							</div>
							
							<div class="col-sm-3">
								<label>PRODUCT</label>
								<select class="form-control select2" id="ID_PRODUCT" NAME="ID_PRODUCT" >
									<?php  foreach($this->list_product as $product): ?>
									<option value="<?php echo $product->ID_PRODUCT ?>" <?php echo ($this->ID_PRODUCT == $product->ID_PRODUCT)?"SELECTED":""; ?> ><?php echo $product->KD_PRODUCT ?></option>
									<?php endforeach; ?>
								</select>							
							</div>
													
						</div>
						
					</div>
					
				
				
            </div>
            <!-- /.box-header -->
            <form id="form_config">
            <div class="box-body table-responsive padding">

				<table id="tconf">
					<thead>
					</thead>
					<tbody id="body_conf">
					
					</tbody>
				</table>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary submit_form_config">Submit</button>
                
                <button type="button" class="btn btn-danger" onclick="document.location.href='<?php echo site_url("qaf_range") ?>';" >Cancel</button>
                
             </div>
  
          </div>
          <!-- /.box -->
          </form>
        </div>
      </div>

    </section>
    <!-- /.content -->


<!-- msg confirm -->
<?php if($this->ERROR && $this->input->post("RANGE")): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $this->ERROR; ?>"
	></a>

<?php ELSE: ?>

	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="Configuration Updated"
	></a>            
<?php endif; ?>
<!-- eof msg confirm -->


<link href="<?php echo base_url("plugins/handsontable/handsontable.full.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>


	function aktifkan(id){
		// $('.aktif'+id).disabled(false);
		// alert(id);
		if($('#cek'+id).is(':checked')){
		// if($(".aktif"+id).is(checked)){
			$(".aktif"+id).prop('disabled',false);
			// alert(cek);
		}else{
			// alert(cek);
			$(".aktif"+id).prop('disabled',true);
		}

		// document.getElementsByClassName("aktif"+id).disabled = false;
	}
	var prod = 0;
	function find_range(){ // izza 27.07.21
	var url = '';
		if(prod == 0){url = "<?php echo site_url("qaf_range/async_configuration/") ?>"+$("#ID_COMPANY").val()+"/"+$("#ID_GROUPAREA").val()+"/NULL";}
		else{url = "<?php echo site_url("qaf_range/async_configuration/") ?>"+$("#ID_COMPANY").val()+"/"+$("#ID_GROUPAREA").val()+"/"+prod;}
		$.getJSON(url,function(data){
			$("#body_conf").empty();
			console.log(url);
			console.log(data);
			
			$.each(data,function(key,r){
				// console.log(r);
				// alert(r.FL_DATETIME);
				var ceked = "";
				var disabled = "";
					console.log(r.KD_COMPONET + r.FLAG);
				if(r.FLAG == 1){
					console.log(r.KD_COMPONET + ' ceklist');
					ceked = "checked";
				}else {
					console.log(r.KD_COMPONET + ' no');
					disabled = "disabled";
				}
				$("#body_conf").append("<tr><td WIDTH=50><input type='checkbox' "+ceked+" value='1' id='cek"+r.ID_COMPONENT+"' onclick='aktifkan("+r.ID_COMPONENT+");' name='RANGE["+r.ID_COMPONENT+"][FLAG]'></td>"+
				// $("#body_conf").append("<tr><td WIDTH=50><input type='checkbox' "+"checked"+" value='1' id='cek"+r.ID_COMPONENT+"' onclick='aktifkan("+r.ID_COMPONENT+");' name='RANGE["+r.ID_COMPONENT+"][FLAG]'></td>"+
				"<td WIDTH=150>"+r.KD_COMPONENT+"</td>"+
				"<td WIDTH=150>MIN: <input type=text "+disabled+" class='aktif"+r.ID_COMPONENT+"' name='RANGE["+r.ID_COMPONENT+"][V_MIN]' value='"+((r.V_MIN==null)?"":r.V_MIN)+"' size=5/></td>"+
				"<td WIDTH=150>MAX: <input "+disabled+" name='RANGE["+r.ID_COMPONENT+"][V_MAX]' type=text class='aktif"+r.ID_COMPONENT+"' value='"+((r.V_MAX==null)?"":r.V_MAX)+"' size=5/></td>"+
				"<td WIDTH=250>PERIODE: <input "+disabled+" name='RANGE["+r.ID_COMPONENT+"][PERIODE]' class='datepicker aktif"+r.ID_COMPONENT+"'  type='text' value='"+((r.FL_DATETIME==null)?"<?php echo date("d/m/Y");?>":r.FL_DATETIME)+"' size=7></td></tr>");
				
			});
			for(var i = 0; i< $('.datepicker').length ; i++){
			    var picker = new Pikaday({
			      	field: $('.datepicker')[i],
      				format: 'DD/MM/YYYY'
			    });
			}

		});
	}

	$(document).ready(function(){


		$(".delete").confirm({ 
			confirmButton: "Remove",
			cancelButton: "Cancel",
			confirmButtonClass: "btn-danger"
		});
		
		$("#ID_COMPANY").change(function(){
			$("#ID_GROUPAREA").change();
		});
		$("#ID_GROUPAREA").change(function(){
			if(this.value == "") return;
			$("#div_table").html($("#loader").html());
			if(this.value == 4) { // clinker tidak pakai produk jenis
				$("#ID_PRODUCT").empty();
				$("#ID_PRODUCT").change();
				prod = 0;
				find_range(); // izza 27.07.21
			}
			else{
				var url = "<?php echo site_url("qaf_range/async_list_product") ?>/"+$("#ID_COMPANY").val()+"/"+this.value;
				$.get(url,function(data){
					$("#ID_PRODUCT").empty();
					data.forEach(function(r){
						$("#ID_PRODUCT").append("<option value='"+r.ID_PRODUCT+"' "+((r.ID_PRODUCT==<?php echo ($this->ID_PRODUCT)?$this->ID_PRODUCT:0; ?>)?"SELECTED":"")+" >"+r.KD_PRODUCT+"</option>");
					});
					$("#ID_PRODUCT").change();
				},'json');
			}
		});
		
		$("#ID_PRODUCT").change(function(){
			prod = this.value;
			find_range(); // izza 27.07.21
		});
		
		
		$("#ID_COMPANY").change(); 
		$("#ID_PLANT").change();
		$("#ID_GROUPAREA").change();
		$("#ID_PRODUCT").change();
		
		$(".notice-error").confirm({ 
			confirm: function(button) { /* Nothing */ },
			confirmButton: "OK",
			cancelButton: "Cancel",
			confirmButtonClass: "btn-danger"
		});
		
		$(".notice-success").confirm({ 
			confirm: function(button) { /* Nothing */ },
			confirmButton: "OK",
			cancelButton: "Cancel",
			confirmButtonClass: "btn-success"
		});
		
		<?PHP IF($this->input->post("RANGE")): ?>
		<?php if($this->ERROR): ?>
		$("#a-notice-error").click();
		<?php ELSE: ?>
		
		$("#a-notice-success").click();
		<?php endif; ?>
		<?PHP ENDIF; ?>

		//Pika month change
	});
</script>

<style>
#body_conf td {
	padding: 3px;
}
</style>
