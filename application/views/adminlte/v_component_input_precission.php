   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Component Input Configuration
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
  
       <div class="row">
        <div class="col-xs-12">   
    
          <div class="box" id="viewArea">

            <div class="box box-primary">
              <div class="box-header with-border">
                
              </div>

              <div class="box-body">

                <div class="box box-danger box-solid col-sm-4 col-md-4 col-lg-4">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Component Input Precission </h3>

                    <div class="input-group input-group-sm" style="width: 150px; float: right;">
                      <a id="btn-Save-STD" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-block btn-default btn-sm"><i class="fa fa-save"></i> &nbsp; Save</a>
                    </div>
                    <div class="input-group input-group-sm" style="width: 40px; float: right;">
                      <a id="ReloadData" data-loading-text="<i class='fa fa-spinner fa-spin'></i> &nbsp;<font class='lowercase'></font>" style="display: none;" type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                  <div class="box-body" style="">                  	
	                <div id="progress-saving" style="display:none;">
	                  	<img src="<?php echo base_url("images/hourglass.gif");?>"> Please wait...
	                </div>
                    <div class="row col-sm-12">    

                      <div class="row col-sm-6">                      
                        <div id="example1" class="hot handsontable htColumnHeaders" style="width: 100%;" ></div>
                      </div>                    
                      <div class="row col-sm-6">  

                      <div class="box box-success box-solid col-sm-4 col-md-4 col-lg-4">
                        <div class="box-header with-border">
                          <h3 class="box-title"> Note : </h3>
                          <div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div>
                        </div>
                        <div class="box-body" style="">
                          <h3 class="title"> Petunjuk Pengisian : </h3>
                          <ul>
                            <li> Isikan 0 untuk pembulatan.</li>
                            <li> Isikan Angka 1 atau lebih untuk menampilkan digit angka dibelakang koma (,).</li>
                            <li> Kosongi untuk menampilkan angka sesuai inputan.</li>

                          </ul>

                        </div>
                      </div>

                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    

<!-- msg confirm -->
  <a  id="a-notice-error"
    class="notice-error"
    style="display:none";
    href="#"
    data-title="Something Error"
    data-text="Data not valid<BR>Failed to save data! :("
  ></a>

    <a  id="a-notice-success"
    class="notice-success"
    style="display:none";
    href="#"
    data-title="Done!"
    data-text="Save Successfully :)"
  ></a>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- HandsonTable CSS -->
<link href="<?php echo base_url("plugins/handsontable/handsontable.full.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/handsontable/pikaday/pikaday.css");?>" rel="stylesheet">

<!-- HandsonTable JS-->
<script src="<?php echo base_url("plugins/handsontable/moment/moment.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/pikaday/pikaday.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/zeroclipboard/ZeroClipboard.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/numbro.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/numbro/languages.js");?>"/></script>
<script src="<?php echo base_url("plugins/handsontable/handsontable.full.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>


<!-- Select2 Plugin-->
<link href="<?php echo base_url("plugins/select2/select2.min.css");?>" rel="stylesheet">
<script src="<?php echo base_url("plugins/select2/select2.full.min.js");?>"/></script>


<script>

$(document).ready(function(){


  $('#formArea').hide();
  $('#viewArea').show();
  $('#ReloadData').hide();
  $('#btn-Save-STD').hide();


  $("#progress-saving").show();


  var oTable;

  var example1 = document.getElementById('example1'), hot1, glbcomponent;

    loadtabelinput();

  $(document).on('click',"#ReloadData",function () {
    loadtabelinput();
  });

  $(document).on('click',"#btn-Save-STD",function () {
    
    $("#progress-saving").show();

      	var handsonData = hot1.getData();

      $.ajax({
        url: "<?php echo site_url("third_material/save_config_precission");?>",
        data: {"ID_TYPE_PRODUCT": 1, "SNI_DATA": handsonData, "COMPONENT_DATA": glbcomponent, "user": "<?php echo $this->USER->FULLNAME ?>"},  //returns all cells' data
        dataType: 'json',
        type: 'POST',
        success: function (res) {
          loadtabelinput();
        },
        error: function () {
          loadtabelinput();
        }
      });
    
      $("#progress-saving").hide();
  });

  /** DataTable Ajax Reload **/
  function dtReload(table,time) {
    var time = (isNaN(time)) ? 100:time;
    setTimeout(function(){ oTable.search('').draw(); }, time);
  }


  /** btEdit Click **/

  /** btDelete Click **/

  function loadtabelinput() {

    if (hot1) {

      hot1.destroy();

    }  

    $.ajax({
		url: "<?php echo site_url("third_material/get_config_precission/");?>",
		data: {"ID_TYPE_PRODUCT": 1, "user": "<?php echo $this->USER->FULLNAME ?>"}, //returns all cells' data
		dataType: 'json',
		type: 'POST',
		success: function (result) { 
			if (result && result['Status'] && result['Data']) {

				hot1 = new Handsontable(example1, {
					data: result['Data']['Body'],
					// height: 450,
					autoColumnSize : true,
					className: "htCenter",
					fixedColumnsLeft: 1,
					filters: true,
					dropdownMenu: ['alignment','filter_by_condition', 'filter_by_value', 'filter_action_bar'],
					columnSorting: true,
					colWidths: result['Data']['ColWidhts'],
					manualColumnFreeze: true,
					manualColumnResize: true,
					colHeaders: result['Data']['Header'],
					columns: getRowOptions()
				});

				glbcomponent = result['Data']['ComponentID'];

				$('#ReloadData').show();
        $('#btn-Save-STD').show();

      			$("#progress-saving").hide();

			} else {        

				$('#ReloadData').show();
        $('#btn-Save-STD').show();
				
      			$("#progress-saving").hide();
			}   
		},
		error: function () {

			$('#ReloadData').show();
        $('#btn-Save-STD').show();	
      		$("#progress-saving").hide();

		}
	});

  }

  function getContentData() {
      return [["TRASS", , , , ],["LIMESTONE", , , , ],["GYPSUM", , , , ]];
  }

  function getRowOptions() {
    var initial_coloptions = [
      {
        type: 'text',
        readOnly: true
      },
      {
        type: "numeric"
      }
    ];

    return initial_coloptions;
  }

  function getColHeader() {
    var columnlist = ['Third Material', 'H20', 'LOi', 'Insol', 'SO3'];
    return columnlist;
  }

  $(".delete").confirm({ 
    confirmButton: "Remove",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });

  $(".notice-error").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-danger"
  });
  
  $(".notice-success").confirm({ 
    confirm: function(button) { /* Nothing */ },
    confirmButton: "OK",
    cancelButton: "Cancel",
    confirmButtonClass: "btn-success"
  });
    
  <?php if($notice->error): ?>
  $("#a-notice-error").click();
  <?php endif; ?>
  
  <?php if($notice->success): ?>
  $("#a-notice-success").click();
  <?php endif; ?>
  
});
</script>
