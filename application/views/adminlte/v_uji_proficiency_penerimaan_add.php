<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Input Penerimaan Sample Proficiency</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
                    <div class="row">
                        <form action="<?= base_url('uji_proficiency_penerimaan/action_input') ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="col-sm-6 clearfix">
                                    <label>UJI PROFICIENCY *</label>
                                    <select class="form-control" id="ID_KOMODITI" name="ID_KOMODITI">
									<?php  foreach($this->list_proficiency as $proficiency): ?>
									<option value="<?php echo $proficiency->ID_PROFICIENCY;?>"><?php echo $proficiency->TITLE_PP." Komoditi ".$proficiency->NAMA_SAMPLE." ".$proficiency->YEAR_PP;?></option>
									<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <label>PENERIMA *</label>
                                    <select class="form-control" id="ID_PENERIMA" name="ID_PENERIMA" required>
                                    </select>
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <label>TANGGAL PENERIMAAN SAMPLE *</label>
                                    <input type="text" id="datepicker" class="form-control" name="TGL_PENERIMAAN" value="<?= date('d/m/Y') ?>" required />
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <label>FOTO SAMPLE DITERIMA *</label>
                                    <input type="file" class="form-control" name="FILE_PENERIMAAN_SAMPLE" required />
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <label>KONDISI SAMPLE SAAT DITERIMA *</label><br/>
                                    <input type="radio" id="baik" name="KONDISI_SAMPLE" value="baik"><label for="baik">&nbsp;Baik</label>
                                    <input type="radio" id="rusak" name="KONDISI_SAMPLE" value="rusak"><label for="rusak">&nbsp;Rusak</label><br>
                                </div>
                                <div class="col-sm-6 clearfix">
                                    <label>CATATAN</label>
                                    <textarea class="form-control" rows="5" name="CATATAN"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 clearfix">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="<?= base_url('uji_proficiency_penerimaan') ?>" class="btn btn-success">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
    $(document).ready(function(){
        $("#ID_KOMODITI").change();
        $("#datepicker").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    });
    
    $("#ID_KOMODITI").change(function(){
        var komoditi = $(this).val(), penerima = $('#ID_PENERIMA');
        $.getJSON('<?php echo site_url("uji_proficiency_penerimaan/penerima/");?>' + komoditi, function (result) {
            var values = result;
            penerima.find('option').remove();
            if (values != undefined && values.length > 0) {
                penerima.css("display","");
                $(values).each(function(index, element) {

                    penerima.append($("<option></option>").attr("value", element.ID_PENERIMA).text(element.NAMA_PENERIMA));
                });
            } else {
                penerima.find('option').remove();
                penerima.append($("<option></option>").attr("value", '00').text("TIDAK ADA PENERIMA"));
            }
        });
    });

    $("form").on("submit", function(e) {
		e.preventDefault();
        
		var post_url = $(this).attr("action");
		var request_method = $(this).attr("method");
        var form_data = new FormData(this);
		
		$.ajax({
            processData: false,
            contentType: false,
			url : post_url,
			type: request_method,
			data : form_data
		}).done(function(response){
			if(response == 1){
				alert("Input Penerimaan Sampling Successfully");
                window.location = "<?= base_url('uji_proficiency_penerimaan') ?>"
			} else {
				alert("Input Penerimaan Sampling Failed")
			}
		});
	});
</script>