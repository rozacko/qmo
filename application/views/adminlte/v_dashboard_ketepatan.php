<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<style>
.highest{
    color:Red;
}​
</style>
<section class="content-header">
  <h1>
 Dashboard Ketepatan
  <small></small>
  </h1>
</section>
<style>

.highcharts-figure, .highcharts-data-table table {
    min-width: 310px; 
    max-width: 800px;
    margin: 1em auto;
}

#DashboardKetepatan {
    height: 400px;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}
</style>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      
      <?php if($notice->error): ?>
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Error!</h4>
        <?php echo $notice->error; ?>
      </div>
      <?php endif; ?>
      
      <?php if($notice->success): ?>
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Done!</h4>
        <?php echo $notice->success; ?>
      </div>
      <?php endif; ?>
      
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header"> 
          
		<figure class="highcharts-figure grafik_header">
			<div id="DashboardKetepatan"></div> 
		</figure>
		<div class='pull-right'><button class="btn-warning" name="kembali" onClick="kembali()" id="kembali">Back</button> &nbsp;  </div>
		<figure class="highcharts-figure grafik_detail">
			<div id="DashboardKetepatanDetail"></div> 
		</figure>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
</style>


<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script src="<?php echo base_url("js/jquery-ui.js"); ?>" ></script>
<script src="<?php echo base_url("plugins/datepicker/bootstrap-datepicker.js"); ?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url("css/jquery-ui.css"); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url("plugins/datepicker/datepicker3.css"); ?>" />
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script>
  $(document).ready(function(){ 
	$(".grafik_detail, #kembali").hide()
	  
	  load_ketepatan()
	   
  
});


	  function load_ketepatan(){
		  
      //Set table columns | Update setting
      $.post('<?php echo site_url("Dashboard_ketepatan");?>/dashboard_waktu', function (result) {
			Highcharts.setOptions({
				colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
			});
			Highcharts.chart('DashboardKetepatan', {
			chart: {
				type: 'column', 
					events: {
						load: function() {
							Highcharts.each(this.series[0].points, function(p) {
								console.log("ssss")
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							}),
							
							Highcharts.each(this.series[1].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							});
						}, 
					} 
			},
			title: {
				text: ' Ketepatan Waktu Upload Dokumen'
			},
			subtitle: {
				text: result.bulan_tahun
			},
			xAxis: {
				categories: result.header,
				crosshair: true
			},
			yAxis: {
				min: 0,
				max: 5,
				title: {
					text: 'Skor Si Hebat'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				},
				  series: {
						dataLabels: {
							enabled: true,
							format: '{point.y:.1f}'
						},
						cursor: 'pointer',
						point: {
							events: {
								click: function () {
									load_detail(this.category);
								}
							}
						}
					}
			},
			
			series: result.data,
			
			 
		});
		
      },"json");
	  }
	  
	  
	  function load_detail(nm_group_plant){
		  
      //Set table columns | Update setting
      $.post('<?php echo site_url("Dashboard_ketepatan");?>/dashboard_waktu_detail',{'nm_group_plant' : nm_group_plant}, function (result) {
			Highcharts.setOptions({
				colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
			});
			Highcharts.chart('DashboardKetepatanDetail', {
			chart: {
				type: 'column', 
					events: {
						load: function() {
							Highcharts.each(this.series[0].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							}),
							
							Highcharts.each(this.series[1].points, function(p) { 
								if (p.y == 1) {
									p.update({
										color: 'red'
									});
								}
							});
						}, 
					} 
			},
			title: {
				text: ' Ketepatan Waktu'
			},
			subtitle: {
				text: ' Upload Dokumen'
			},
			xAxis: {
				categories: result.header,
				crosshair: true
			},
			yAxis: {
				min: 0,
				max: 5,
				title: {
					text: 'Skor Si Hebat'
				},
				
				stackLabels: { 
					enabled: true,
					verticalAlign: 'top'
				}
			},
			tooltip: {
				headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
				pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
					'<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
				footerFormat: '</table>',
				shared: true,
				useHTML: true
			},
			plotOptions: {
				column: {
					pointPadding: 0.2,
					borderWidth: 0
				}, 
			},
			
			series: result.data,
			
			 
		});
		
      },"json");
	  
	  $(".grafik_header").hide()
	  $(".grafik_detail, #kembali").show()
	  }
	  
	  function kembali(){
		  
	  $(".grafik_header").show()
	  $(".grafik_detail, #kembali").hide()
	  }
  
</script>
