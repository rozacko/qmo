<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quality Management Online</title>
    <link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="<?= base_url('css/bootstrap-extend.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/site.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('css/login-v2.css') ?>" rel="stylesheet">
</head>
<body class="page-login-v2 layout-full page-dark">
    <div class="page">
        <div class="page-content">
			<div class="page-brand-info">
                <div class="brand">
                    <h2 class="brand-text font-size-40"></h2>
                </div>
                <p class="font-size-20">
                    <!-- Purchasing Application System -->
                </p>
            </div>
            <div class="page-login-main animation-slide-right animation-duration-1">
                <center>
                    <img class="logo" src="<?php echo base_url("images/Logo Color.png") ?>" alt="..." width="100px">
                </center>
                <h3 class="font-size-24">Login</h3>
                <p>Silahkan masuk dengan mengunakan user ID anda.</p>
                <!-- <?php if($notice->error): ?>
                    <p class="login-error"><?php echo $notice->error; ?></p>
                <?php endif; ?> -->
                <form action="<?php echo site_url("login/verification") ?>" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="employee_code">Username</label>
                        <input type="text" class="form-control" name="USERNAME" placeholder="Enter a valid username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">Password</label>
                        <input type="password" class="form-control" name="PASSWORD" placeholder="Enter password">
                    </div>
                    <button type="submit" class="btn btn-danger text-center">Login</button>
                </form>
                <div class="row mb-2 px-3">
                    <small class="font-weight-bold">
                        <a href="<?php echo site_url("Full_behavior/Behavior") ?>" class="text-danger "> Coating Behavior</a><br>
                        Don't have an account ? <a href="<?php echo site_url("login/register") ?>" class="text-danger "> Register a new membership</a>
                    </small>
                </div>
            </div>
        </div>
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"  type="text/javascript" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
</body>
</html>