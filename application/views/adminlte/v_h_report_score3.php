<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Report Score
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <form method="POST">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="form-group col-sm-12 col-sm-4 picker_daily">
                <label for="ID_COMPANY">OPTION</label><br>
                <SELECT class="form-control select2" name="ASPEK" id='id_jenis_aspek'>
                  <option value="ALL">ALL</option>
                  <?php
                    foreach ($this->JENIS_ASPEK as $aspek) {
                      $slk = ($this->ASPEK == $aspek->ID_ASPEK) ? "selected":"";
                      echo "<option $slk value='".$aspek->ID_ASPEK."'>" . $aspek->ASPEK . "</option>";
                    }
                  ?>
                </SELECT>
              </div>
              
              <div class="form-group col-sm-12 col-sm-2">
                <label for="ID_COMPANY">YEAR</label><br>
                <SELECT class="form-control select2" name="YEAR" id='tahun'>

                <?php 
                error_reporting(E_ALL);

                function colorLevel($color){
                   switch ($color) {
                      case 1:
                          $color = '#FFD700';
                          break;
                      case 2:
                          $color = '#C0C0C0';
                          break;
                      case 3:
                          $color = '#CD853F';
                          break;
                      case 4:
                          $color = '#EEEEEE';
                          break;
                      case 5:
                          $color = '#008B8B';
                          break;
                      default:
                          $color = '#FFFFFF';
                  } 

                  return $color;
                }
                function colorPoScore($color){
                   switch ($color) {
                      case 1:
                          $color = 'goldenrod';
                          break;
                      case 2:
                          $color = '#A5998E';
                          break;
                      case 3:
                          $color = 'tan';
                          break;
                      case 4:
                          $color = '#CECECC';
                          break;
                      case 5:
                          $color = 'mediumseagreen';
                          break;
                      default:
                          $color = '#FFFFFF';
                  } 

                  return $color;
                }

                function colorBody($color){
                   switch ($color) {
                      case 1:
                          $color = 'khaki';
                          break;
                      case 2:
                          $color = '#BAB2AB';
                          break;
                      case 3:
                          $color = 'lightsalmon';
                          break;
                      case 4:
                          $color = '#DDDDD2';
                          break;
                      case 5:
                          $color = 'darkseagreen';
                          break;
                      default:
                          $color = '#FFFFFF';
                  } 

                  return $color;
                }

                for($i=2016;$i<=date("Y");$i++){
                  $selected = '';
                  if(isset($_POST['YEAR'])){
                    if($this->input->post('YEAR') == $i){
                      $selected = 'selected';
                    }else{
                    }
                  }else{
                      if(date('Y') == $i){
                        $selected = 'selected';
                      }
                  }
                  echo '<option value="'.$i.'" '.$selected.' >'.$i.'</option>';
                }
                ?>
               </SELECT>
              </div>

              <div class="form-group col-sm-12 col-sm-2 picker_daily" >
                <label for="ID_COMPANY">MONTH</label>
                <SELECT style="width:100%;" class="form-control select2" name="MONTH" id='bulan'>
                  <option value="01" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='01'):"01"==date("m")) ? "selected":"";?>>JANUARY</option>
                  <option value="02" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='02'):"02"==date("m")) ? "selected":"";?>>FEBRUARY</option>
                  <option value="03" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='03'):"03"==date("m")) ? "selected":"";?>>MARCH</option>
                  <option value="04" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='04'):"04"==date("m")) ? "selected":"";?>>APRIL</option>
                  <option value="05" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='05'):"05"==date("m")) ? "selected":"";?>>MAY</option>
                  <option value="06" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='06'):"06"==date("m")) ? "selected":"";?>>JUNE</option>
                  <option value="07" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='07'):"07"==date("m")) ? "selected":"";?>>JULY</option>
                  <option value="08" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='08'):"08"==date("m")) ? "selected":"";?>>AUGUST</option>
                  <option value="09" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='09'):"09"==date("m")) ? "selected":"";?>>SEPTEMBER</option>
                  <option value="10" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='10'):"10"==date("m")) ? "selected":"";?>>OCTOBER</option>
                  <option value="11" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='11'):"11"==date("m")) ? "selected":"";?>>NOVEMBER</option>
                  <option value="12" <?php echo (($this->input->post('MONTH')!=NULL)? ($this->input->post('MONTH')=='12'):"12"==date("m")) ? "selected":"";?>>DECEMBER</option>
                </SELECT>
              </div>

              <div class="form-group col-sm-12 col-sm-4">
                <div class="col-sm-4">
                  <label for="">&nbsp;</label><br>
                  <button type="submit" id='btLoad' class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Load Data</button>
                </div>
                <div class="col-sm-4">
                  <label for="">&nbsp;</label><br>
                  <a id="btn-Convert-Html2Image" class="btn btn-sm btn-success" href="#"> <i class="fa fa-image"></i> Export Image</a>
                </div>
              </div>

            </div>
            <div class="col-md-12">
            <hr>
            </div>


            <table style="background: #C2F1FF" id="dt_tables"
              class="table table-striped table-bordered table-hover dt-responsive nowrap"
              cellspacing="0"
              width="100%">
              <thead>
                <tr>
                  <th colspan="11" align="center" style="text-align: center"><h2>Periode (<span id="titlePeriode"></span>)</h2></th>
                </tr>
                <tr>
                  <!--th rowspan="2" valign="middle" width="2%">NO.</th-->
                  <th rowspan="2" width="40%" valign="top" style="position: relative;text-align: center;top: -16px;">KRITERIA</th>
                  <?php
                  $listCompany = array();

                  //urutane Company dari code lama
                  foreach ($this->LIST_COMPANY as $company) {
                    $total  = 0;
                    $arr_k  = array_keys($this->NILAI['comp']);
                    $nilai  = (in_array($company->ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$company->ID_COMPANY]:array(0);
                    if ($nilai[0]!==0) {
                      foreach ($nilai as $nl) {
                        $skor = $nl->SCORE*$nl->BOBOT;
                        $total += round($skor,2);
                      }
                    }else{
                      $total = 0;
                    }
                    $listCompany[] = array('TOTAL' => $total, 'ID_COMPANY' => $company->ID_COMPANY, 'KD_COMPANY' => $company->KD_COMPANY, 'NM_COMPANY' => $company->NM_COMPANY);
                  }
                  rsort($listCompany);
                  // var_dump($listCompany);

                    $i = 1;
                    foreach ($listCompany as $company) {
                      $color = colorLevel($i);
                      echo "<th style='background-color:$color;' colspan='2'><center>".$company['NM_COMPANY'] . "</center></th>";
                      $i++;
                    }
                  ?>
                </tr>
                <tr>
                  <?php
                    $i = 1;
                    foreach ($listCompany as $company) {
                      $color = colorLevel($i);
                      $colorPoScore = colorPoScore($i);
                      #echo "<th style='background-color:$color;'><center style='color:white;'>NILAI ASPEK</center></th>";
                      echo "<th style='background-color:$colorPoScore;'><center style=''>NILAI ASPEK</center></th>";
                      echo "<th style='background-color:$colorPoScore;'><center style=''>SKOR</center></th>";
                      $i++;
                    }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php
             
                $totalskor = array();
                $no = 1;
                // echo "<pre>";
                // print_r($this->NILAI);
                // echo "</pre>";

                //id batasan 6 new bispro cari total terkecil
                $nilairendah_nonopc =100;
                foreach($this->NILAI['comp'] as $n){
                    foreach($n as $dt){
                      if($dt->ID_BATASAN == 6 && (int)$dt->TOTAL_POINT != 0 )
                      {
                        $nilairendah_nonopc = ($nilairendah_nonopc < $dt->TOTAL_POINT) ? $nilairendah_nonopc : $dt->TOTAL_POINT;
                      }                      
                    }
                }


                foreach ($this->LIST_INPUT as $list) {
                  echo "<tr>";
                    echo "<td style='background-color:".company_color(1).";'>".$list->KRITERIA."</td>";
                      echo "<input name='ID_BATASAN[]' value='".$list->ID_KRITERIA."' type='hidden'>";
                      $iBodyC = 1;
                    foreach ($listCompany as $company) {
                      $ID_COMPANY = $company['ID_COMPANY'];
                      $KD_COMPANY = $company['KD_COMPANY'];
                      // $color  = company_color($KD_COMPANY,2);
                      $color = colorBody($iBodyC);
                      $arr_k  = array_keys($this->NILAI['comp']);
                      $arr_cek  = array_keys($this->NILAI['cek_data']);
                      $nilai  = (in_array($ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$ID_COMPANY]:array();
                      $cek_data  = $this->NILAI['cek_data'];
                      
                      $bts    = $list->ID_KRITERIA;
                      $fig    = array_filter($nilai, function ($e) use ($bts) {
                                return $e->ID_KRITERIA == $bts;
                              });
                      
                      // echo "<pre>"; 
                      // print_r($cek_data);
                      // print_r($fig);
                      // echo "</pre>"; 

                      // colorBody($iBody);
                      echo "<td style='background-color:$color;' align='center'>";
                        echo "<span class='opco_".$ID_COMPANY."' id='NILAI_".$list->ID_KRITERIA.$ID_COMPANY."'>";
                          //PERHITUNGAN POSISI MASTER-------------------------------------------
                          // foreach ($fig as $fNilai) {
                          //   echo round($fNilai->SCORE,2);
                          // }
                          $ttlposisi= array();
                          
                        if($fig){ //jika ada data
                         
                          foreach ($fig as $fNilai) {
                            if($fNilai->ID_ASPEK == 2){
                              $ttlposisi[$fNilai->ID_COMPANY.$fNilai->ID_ASPEK] = $fNilai->SCORE;
                              echo round($fNilai->SCORE,2);                              
                            // }
                            // else if($fNilai->ID_ASPEK == 3 || $fNilai->ID_ASPEK == 4){ 
                            //   $ttlposisi[$fNilai->ID_COMPANY.$fNilai->ID_ASPEK] = $fNilai->SCORE*$fNilai->BOBOT;
                            //   $ttlposisi[$fNilai->ID_COMPANY.$fNilai->ID_ASPEK] = $fNilai->TOTAL_POINT;
                            //   echo round($fNilai->SCORE*$fNilai->BOBOT ,2);
                            }else{
                              $ttlposisi[$fNilai->ID_COMPANY.$fNilai->ID_ASPEK] = $fNilai->TOTAL_POINT;
                              if($fNilai->ID_ASPEK == 6 && $fNilai->TOTAL_POINT == null)
                                  echo '';
                              else 
                                  echo round($fNilai->TOTAL_POINT,2);
                            }                    
                          }
                        }else{
                          if($list->ID_KRITERIA == 1){ //khusus ncqr jika tdk ada data maka nilai penuh
                            $ttlposisi[$ID_COMPANY.$bts] = 5;
                            echo '5';
                          }else{
                            echo '';
                          }
                        }

                          // $jumind = $ind / 8;
                          // echo round($jumind,2);
                        echo "</span>";
                      echo "</td>";
                      echo "<td style='background-color:$color;' align='center'>";
                        echo "<span class='opcox_".$ID_COMPANY.$list->ID_ASPEK."' id='NILAIX_".$list->ID_KRITERIA.$ID_COMPANY."'>";
                          //SCORE-------------------------------------------
                          
                        if($fig){
                          foreach ($fig as $fNilai) {
                            $f = $ttlposisi[$fNilai->ID_COMPANY.$fNilai->ID_ASPEK];

                            if($fNilai->ID_ASPEK == 1 || $fNilai->ID_ASPEK == 2){
                              $nilaiskor = ($f/5)*$fNilai->BOBOT;           
                            }else if($fNilai->ID_ASPEK == 6 && (int)$f != 0 ){
                              $nilaiskor = ($nilairendah_nonopc/$f)*$fNilai->BOBOT;  
                              //echo "nilai rendah:".$nilairendah_nonopc."<br>nilai aspek:".$f."<br>";
                            }else{
                              $nilaiskor = $fNilai->BOBOT*($f/100);
                              //echo  $fNilai->BOBOT."*(".$f."/.100)";
                            }

                            if($fNilai->ID_ASPEK == 6 && $nilaiskor == 0)
                                echo '';
                            else{
                                echo round($nilaiskor,2);
                            }
                            
                            $totalskor[$fNilai->ID_COMPANY] += $nilaiskor;
                            unset($nilaiskor);
                          }
                        }else{                          
                          if($list->ID_KRITERIA == 1){ //khusus ncqr jika tdk ada data maka nilai penuh                            
                            $totalskor[$ID_COMPANY] += 10;
                            echo '10';
                          }else{
                            echo '';
                          }
                        }                       
                        echo "</span>";
                      echo "</td>";
                      $iBodyC++;
                    }
                  echo "</tr>";
                  $no++;                  
                } 
                
                echo "<tr>";
                  echo '<th style="background-color:#C2F1FF;" colspan="1" valign="middle" width="2%">SCORE</th>';
                 
                  $iColor = 1;
                 
                  foreach ($listCompany as $company) {
                    #var_dump($this->ASPEK);
                    $ID_COMPANY = $company['ID_COMPANY'];
                    $KD_COMPANY = $company['KD_COMPANY'];

                    $total  = 0;
                    $arr_k  = array_keys($this->NILAI['comp']);
                    
                    if($totalskor[$ID_COMPANY] > 0){
                      $total += round($totalskor[$ID_COMPANY],2);
                    }else{
                      $total = 0;
                    }
                    $color = colorPoScore($iColor);
                    echo "<td style='background-color:$color;' colspan='2' align='center'>";
                      echo "<span class='total_".$ID_COMPANY."'>";
                        echo $total;
                      echo "</span>";
                    echo "</td>";
                  $iColor++;

                  }
                  ?>
                </tr>
              </tbody>
            </table>
              <div style="width:100%;height:110%;display:none;" >
                <div id="previewImage" >
                </div>
            </div>

          </div>
          <!-- /.box-body -->
        </form>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
<!-- <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
  $(document).ready(function(){
    $('.select2').select2();
    var tahun = $('#tahun').select2('data')[0];
    var bulan = $('#bulan').select2('data')[0];

    $('#titlePeriode').text(bulan.text +' '+tahun.text);
  });


  var element = $("#dt_tables"); // global variable
  var getCanvas; // global variable
        
  $(document).ready(function(){
      $("#btn-Convert-Html2Image").click(function(){
        var tahun = $('#tahun').select2('data')[0];
        var bulan = $('#bulan').select2('data')[0];

        var imgageData = getCanvas.toDataURL("image/png");
        var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
        $("#btn-Convert-Html2Image").attr("download", "Report Score SI Hebat ("+tahun.text+" "+bulan.text+").png").attr("href", newData);
      });
  });
  
  
   window.onload = function(e)
      {
          html2canvas(element, {
           onrendered: function (canvas) {
                  $("#previewImage").html(canvas);
                  getCanvas = canvas; 
               },
               // 1085 1092
              width:window.innerWidth,
              height:window.innerWidth - 300,
           });
           console.log(window.innerWidth +"  | "+window.innerHeight);
      };

</script>
