<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Notification Group Reciever
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  
  <div class="row">
    <div class="col-xs-12">
      

      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData" method="POST" action="<?php echo site_url("opco_group_notification/update/".$this->data_opco->ID_OPCO_NOTIFICATION_GROUP) ?>" >
            
            <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">COMPANY</label>
                <?php if($this->USER->ID_COMPANY): ?>
                <input type=text value="<?php echo $this->USER->NM_COMPANY ?>" class="form-control" readonly />
                <input type=hidden id="ID_COMPANY"  value="<?php echo $this->USER->ID_COMPANY ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_COMPANY" class="form-control select2">
                  <?php  foreach($this->list_company as $company): ?>
                    <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->data_opco->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
                  <?php endforeach; ?>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_COMPANY">PLANT</label>
                <?php if($this->USER->ID_PLANT): ?>
                <input type=text value="<?php echo $this->USER->NM_PLANT ?>" class="form-control" readonly />
                <input type=hidden id="ID_PLANT" value="<?php echo $this->USER->ID_PLANT ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_PLANT"  class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
            </div>

            <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="ID_COMPANY">GROUP AREA</label>
                <?php if($this->USER->ID_GROUPAREA): ?>
                <input type=text value="<?php echo $this->USER->NM_GROUPAREA ?>" class="form-control" readonly />
                <input type=hidden id="ID_GROUPAREA" value="<?php echo $this->USER->ID_GROUPAREA ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_GROUPAREA"  class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <label for="ID_AREA">AREA</label>
                <?php if($this->USER->ID_AREA): ?>
                <input type=text value="<?php echo $this->USER->NM_AREA ?>" class="form-control" readonly />
                <input type=hidden id="ID_AREA" NAME="ID_AREA" value="<?php echo $this->USER->ID_AREA ?>" class="form-control" readonly />
                <?php else: ?>
                <select id="ID_AREA" name="ID_AREA" class="form-control select2">
                  <option value="">Please wait...</option>
                </select>
                <?php endif; ?>
              </div>

            </div>
        
           <div class="form-group row">
              <div class="form-group col-sm-12 col-sm-4">
                <label for="TELEGRAM_CHAT_ID">TELEGRAM CHAT ID</label>
                
                <input id="telegram_chat_id" type=text value="<?php echo $this->data_opco->TELEGRAM_CHAT_ID?>" NAME="TELEGRAM_CHAT_ID" class="form-control" />
                
              </div>
              <div class="form-group col-sm-6 col-sm-4">
                <input type=checkbox name="TELEGRAM_NOTIFICATION_ENABLE"
                    value="1"
                    <?php echo $this->data_opco->TELEGRAM_NOTIFICATION_ENABLE == 1 ? "checked" : "" ?>
                    id="TELEGRAM_NOTIFICATION_ENABLE"/>
                <label for="TELEGRAM_NOTIFICATION_ENABLE"> ENABLE TELEGRAM NOTIFICATION </label>
              </div>
            </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?= base_url('opco_group_notification')?>" class="btn btn-default">Back</a>
              
          </form>
        </div>
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  hr { margin-top: 10px; }
</style>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"/></script> 
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
  $(document).ready(function(){
   
    $("#ID_COMPANY").change(function(){
      var company = $(this).val(), plant = $('#ID_PLANT');
      $.getJSON('<?php echo site_url("opco_group_notification/ajax_get_plant/");?>' + company, function (result) {
        var values = result;
        
        plant.find('option').remove();
        if (values != undefined && values.length > 0) {
          plant.css("display","");
          $('#ID_GROUPAREA').css("display","");
          $(values).each(function(index, element) {
            plant.append("<OPTION value='"+element.ID_PLANT+"' "+((element.ID_PLANT==<?php echo $this->data_opco->ID_PLANT ?>)?"selected":"")+"  >"+element.NM_PLANT+"</OPTION>");
          });
          
        }else{
          plant.find('option').remove();
          plant.append($("<option></option>").attr("value", '00').text("NO PLANT"));
        }
        $("#ID_PLANT").change();
      });
    });

    $("#ID_PLANT").change(function(){
      var plant = $(this).val(), grouparea = $('#ID_GROUPAREA');
      $.getJSON('<?php echo site_url("opco_group_notification/ajax_get_grouparea/");?>' + $("#ID_COMPANY").val() + '/' + plant, function (result) {
        var values = result;
        
        grouparea.find('option').remove();
        if (values != undefined && values.length > 0) {
          grouparea.css("display","");
          $(values).each(function(index, element) {
            if(!$("#ID_GROUPAREA option[value='"+element.ID_GROUPAREA+"']").length > 0){
              grouparea.append("<OPTION value='"+element.ID_GROUPAREA+"' "+((element.ID_GROUPAREA==<?php echo $this->data_opco->ID_GROUPAREA ?>)?"selected":"")+"  >"+element.NM_GROUPAREA+"</OPTION>");
            }
          });
        }else{
          grouparea.find('option').remove();
          grouparea.append($("<option></option>").attr("value", '00').text("NO GROUP AREA"));
        }
        $("#ID_GROUPAREA").change();
      });
    });

    $("#ID_GROUPAREA").change(function(){
      var grouparea = $(this).val(), area = $('#ID_AREA');
      $.getJSON('<?php echo site_url("opco_group_notification/ajax_get_area/");?>' + $("#ID_COMPANY").val() + '/' + $("#ID_PLANT").val() + '/' + grouparea, function (result) {
        var values = result;
        
        area.find('option').remove();
        if (values != undefined && values.length > 0) {
          area.css("display","");
          $(values).each(function(index, element) {
            area.append("<OPTION value='"+element.ID_AREA+"' "+((element.ID_AREA==<?php echo $this->data_opco->ID_AREA ?>)?"selected":"")+"  >"+element.NM_AREA+"</OPTION>");
          });
        }else{
          area.find('option').remove();
          area.append($("<option></option>").attr("value", '00').text("NO AREA"));
        }
        $("#ID_AREA").change();
      });
    });

  // $("#ID_AREA").change(function(){
  // });
  
  $("#ID_JABATAN").change(function(){
    $("#ID_AREA").change();
  });

  $("#ID_COMPANY").change();

    
  $("form").on("submit", function(e) {
    e.preventDefault();
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method");
    var form_data = $(this).serialize();
    // console.log( $( this ).serialize() );
    
    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data
    })
    .done(function(response){
      if(response == 1){
        alert("Success")
      }
      else if (response == 3) {
        alert("ERROR: Duplicate data. Please select other area");
      }
      else if (resposne == 0) {
        alert('ERROR: Unknown error');
      }
    })
    .fail(function(error) {
        alert(error);
    })
  });
});
  
</script>
