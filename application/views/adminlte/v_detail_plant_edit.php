<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>Master Detail Plant<small></small></h1>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
		<?php if($notice->error): ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>
		<?php endif; ?>

			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Data Detail Plant</h3>
				</div>
				<form role="form" method="POST" action="<?php echo site_url("detail_plant/update/").$this->data_detail_plant->ID_DETAIL_PLANT ?>">
					<div class="box-body" style="background-color:#c5d5ea;">
						<div class="form-group">
							<div class="col-sm-4 clearfix">
								<label>COMPANY </label>
								<select class="form-control" id="ID_COMPANY" required>
									<option value="">Select Company</option>
									<?php foreach($this->data_company as $dt_c){ ?>
									<option value="<?= $dt_c->ID_COMPANY ?>" <?= $dt_c->ID_COMPANY == $this->data_detail_plant->ID_COMPANY ? 'selected' : '' ?>><?= $dt_c->NM_COMPANY ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 clearfix">
								<label>PLANT </label>
								<select class="form-control" name="ID_PLANT" id="ID_PLANT" required>
									<option value="">Select Plant</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 clearfix">
								<label>KODE DETAIL PLANT </label>
								<input type="text" class="form-control" name="KD_DETAIL_PLANT" value="<?= $this->data_detail_plant->KD_DETAIL_PLANT ?>" placeholder="Kode Detail Plant" required >
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 clearfix">
								<label>NAMA DETAIL PLANT </label>
								<input type="text" class="form-control" name="NM_DETAIL_PLANT" value="<?= $this->data_detail_plant->NM_DETAIL_PLANT ?>" placeholder="Nama Detail Plant" required >
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a type="button" class="btn btn-danger" href="<?php echo site_url("detail_plant") ?>" >Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		company_id = $("#ID_COMPANY").val();
		get_plant(company_id, <?= $this->data_detail_plant->ID_PLANT ?>);
	});

	$(document).on('change', "#ID_COMPANY", function(){
		var company_id = $(this).val();
		get_plant(company_id);
	});

	function get_plant(company_id, val = null){
		$.ajax({
            url: '<?php echo site_url("detail_plant/ajax_get_plant_by_company");?>/'+company_id,
            method: "GET",
            dataType: "JSON",
            success: function(response) {
                if ( response.length == 0 ) return true;
                var html = '';
                $.each(response, function(idx, elm) {
                	html += '<option value="'+elm.ID_PLANT+'">'+elm.NM_PLANT+'</option>';
                });

                $("#ID_PLANT").html(html);
                $("#ID_PLANT").val(val); 
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus)
            }
        })
	}
</script>