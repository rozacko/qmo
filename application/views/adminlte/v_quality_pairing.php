<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<style type="text/css">
  g.pointtext {
display: none;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
  Component Quality Pairing
  <small></small>
  </h1>
</section>
<!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-xs-12">

      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
          <form id="formData">
            <div class="form-group row">
              <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                <label for="month_start">FROM</label>
                <SELECT style="width:auto;" class="form-control select2" name="MONTH_START" id='month_start'>
                  <?php for($i=1;$i<=12;$i++): ?>
                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                  <?php endfor; ?>
                </SELECT>
              </div>
              <div class="form-group col-sm-2 col-sm-2 year">
                <label for="year_start"></label>
                <SELECT style="width:auto;" class="form-control select2" name="YEAR_START" id='year_start'>
                <?php for($i=2016;$i<=date("Y");$i++): ?>
                  <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                <?php endfor; ?>
                </SELECT>
              </div>

              <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                <label for="month_end">TO</label>
                <SELECT style="width:auto;" class="form-control select2" name="MONTH_END" id='month_end'>
                  <?php for($i=1;$i<=12;$i++): ?>
                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                  <?php endfor; ?>
                </SELECT>
              </div>
              <div class="form-group col-sm-2 col-sm-2 year">
                <label for="year_end">YEAR</label>
                <SELECT style="width:auto;" class="form-control select2" name="YEAR_END" id='year_end'>
                <?php for($i=2016;$i<=date("Y");$i++): ?>
                  <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                <?php endfor; ?>
                </SELECT>
              </div>
            </div>
            <hr/>
            <div class="form-group row">
              <div class="col-sm-3">
                <label for="OPT_COMPANY">COMPANY</label><br>
                <input type="checkbox" class="OPT_COMPANY_ALL" value="semuanya">    Check All
                <?php  foreach($this->list_company as $company):
                  $checked  = ($this->USER->ID_COMPANY==$company->ID_COMPANY) ? 'checked':NULL;
                ?>
                  <div class="form-group">
                  <label>
                    <input type="checkbox"  NAME="OPT_COMPANY[]" class="minimal OPT_COMPANY" value="<?php echo $company->ID_COMPANY;?>" <?php echo $checked;?>>
                    <?php echo $company->NM_COMPANY;?>
                  </label>
                  </div>
                <?php endforeach; ?>
              </div>

              <!-- plant checkbox list -->
              <div id="planarea" class="col-sm-3" style="display:none;">
                <label for="OPT_PLANT">PLANT</label>&nbsp;&nbsp;&nbsp;&nbsp;
                <button class="btn-warning" name="loadProduct" id="btLoadProduct" type='button'>Load Product</button><br>
                <div id="opt_plant"></div>
              </div>

            
              <!-- product type radio list -->
              <div id="produk" style="display:none;" class="col-sm-3">
                <label for="OPT_PRODUCT">PRODUCT TYPE</label><br>
                <div id="opt_product"></div>
              </div>

              <!-- component radio list -->
              <div id="komponen" style="display:none;" class="col-sm-3">
                <label for="OPT_COMPONENT">COMPONENT</label><br>
                <div id="opt_component"></div>
              </div>
            </div>
            <hr/>
            <div class="form-group row">
              <div class="form-group col-sm-6 col-sm-4">
                <button class="btn-primary" name="load" id="btLoad">Load</button>
              </div>
            </div>

          </form>
          <hr/>
          <div id="divTable" style="display:none;text-align:center;" class="text-center row"></div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>

  <div id="myplot"></div>

</section>
<!-- /.content -->

<a  id="a-notice-error"
  class="notice-error"
  style="display:none";
  href="#"
  data-title="Alert"
  data-text=""
></a>
<div class="modal_load"><!-- Loading modal --></div>

<!-- css -->
<style type="text/css">
  label { margin-bottom: 0px; }
  .form-group { margin-bottom: 5px; }
  .boxPlot_div { display: inline-block;margin:auto;margin-bottom:10px; }
  #divTable { overflow-x: scroll }
  hr { margin-top: 10px; }

   /* Start by setting display:none to make this hidden.
   Then we position it in relation to the viewport window
   with position:fixed. Width, height, top and left speak
   for themselves. Background we set to 80% white with
   our animation centered, and no-repeating */
  .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>

<!-- Additional CSS -->
<link href="<?php echo base_url("plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>

<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>

<script>
  $(document).ready(function(){
    // $("#pd_start").datetimepicker({format: 'dd/mm/yyyy', autoclose: true, minView: 2});
    // $("#pd_end").datetimepicker({format: 'dd/mm/yyyy', autoclose: true, minView: 2});
    // $("#ph_start").datetimepicker({format: 'dd/mm/yyyy hh:ii', autoclose: true, minView: 0});
    // $("#ph_end").datetimepicker({format: 'dd/mm/yyyy hh:ii', autoclose: true, minView: 0});

    function addCheckbox(val, namaFor, divid, txt, tipe) {
      tipe = tipe || 'checkbox';
      var container = document.getElementById(divid);
      var divgrup   = document.createElement('div');
          divgrup.className = divid+' form-group';
      var input = document.createElement('input');
          input.type = tipe;
          input.className = namaFor;
          input.name = namaFor+"[]";
          input.id = namaFor + "-" + val
          input.value = val;
      var label = document.createElement('label');
          label.htmlFor = input.id;

      container.appendChild(divgrup);
      divgrup.appendChild(label);
      label.appendChild(input);
      label.appendChild(document.createTextNode("    "+txt));
    }

    function addBoxplotDiv(ctr){
      var container = document.getElementById('divTable');
      var divbox    = document.createElement('div');
          divbox.className = 'boxPlot_div';
          divbox.id = 'boxPlot_' + ctr;

      container.appendChild(divbox);
    }

    $("#dh_ly").change(function(){
      $('.opt_area input:checkbox').removeAttr('checked');
      var period = $("#dh_ly").val();

      switch(period){
        case 'Y':
          $('.pd_start').css('display', 'none');
          $('.pd_end').css('display', 'none');
          $('.ph_start').css('display', 'none');
          $('.ph_end').css('display', 'none');
          $('.month').css('display', 'none');
          $('.year').css('display', '');
          break;

        case 'M':
          $('.pd_start').css('display', 'none');
          $('.pd_end').css('display', 'none');
          $('.ph_start').css('display', 'none');
          $('.ph_end').css('display', 'none');
          $('.month').css('display', '');
          $('.year').css('display', '');
          break;

        case 'D':
          $('.pd_start').css('display', '');
          $('.pd_end').css('display', '');
          $('.ph_start').css('display', 'none');
          $('.ph_end').css('display', 'none');
          $('.month').css('display', 'none');
          $('.year').css('display', 'none');
          break;

        case 'H':
          $('.ph_start').css('display', '');
          $('.ph_end').css('display', '');
          $('.pd_start').css('display', 'none');
          $('.pd_end').css('display', 'none');
          $('.month').css('display', 'none');
          $('.year').css('display', 'none');
          break;


      }

    });

    //$(".OPT_GROUPAREA").change(function() {
    //  var opt_gid = $(".OPT_GROUPAREA:radio:checked").attr('id');
    //  $("#planarea").css("display", "");
    //  $(".boxPlot_div").remove();
    //  if(opt_gid=="FM"){ // || opt_gid=="CL"
    //    $("#produk").css("display", "");
    //    $("#komponen").css("display", "");
    //    $("#opt_component :checkbox").remove();
    //    $(".opt_component").remove();
    //    $('label[for="OPT_COMPONENT[]"]').remove();
    //  }else{
    //    $("#komponen").css("display", "");
    //    $("#produk").css("display", "none");
    //    $("#opt_product :checkbox").remove();
    //    $(".opt_product").remove();
    //    $('label[for="OPT_PRODUCT[]"]').remove();

    //    $("#opt_component :checkbox").remove();
    //    $(".opt_component").remove();
    //    $('label[for="OPT_COMPONENT[]"]').remove();
    //  }
    //  var optComp = $('.OPT_COMPANY:checkbox:checked').map(function() {
    //    return this.value;
    //  }).get();

    //  var optGarea = $('.OPT_GROUPAREA:radio:checked').map(function() {
    //    return this.value;
    //  }).get();

    //  $("#opt_area :checkbox").remove();
    //  $(".opt_area").remove();
    //  $('label[for="OPT_AREA[]"]').remove();
    //  $.ajax({
    //    type: "POST",
    //    dataType: "json",
    //    data: {
    //      opt_c: optComp,
    //      opt_g: optGarea
    //    },
    //    url: '<?php echo site_url("quality_trend/ajax_get_plant_by_company/");?>',
    //    success: function(res){
    //      var val = res;
    //      addCheckbox('semua', "OPT_AREA", "opt_area", 'Check All');
    //      $(val).each(function(idx, elm) {
    //        //console.log(elm.NM_PLANT+'-'+elm.NM_AREA);
    //        addCheckbox(elm.ID_AREA, "OPT_AREA", "opt_area", elm.NM_PLANT+' - '+elm.NM_AREA);
    //      });
    //    }
    //  });
    //});

    //$(document).on("change", ".OPT_AREA",function(event){
    //  var opt_gid = $(".OPT_GROUPAREA:radio:checked").attr('id');
    //  if(opt_gid=="FM"){ //|| opt_gid=="CL"
    //    var checkedVals = $('.OPT_AREA:checkbox:checked').map(function() {
    //      return this.value;
    //    }).get();

    //    $("#opt_product :radio").remove();
    //    $(".opt_product").remove();
    //    $('label[for="OPT_PRODUCT[]"]').remove();

    //    $.ajax({
    //      type: "POST",
    //      dataType: "json",
    //      data: {opt: checkedVals},
    //      url: '<?php echo site_url("quality_trend/ajax_get_product_by_area/");?>',
    //      success: function(res){
    //        for (var i = 0; i < res.length; i++) {
    //          delete res[i].ID_C_PRODUCT
    //          delete res[i].ID_AREA
    //          //delete res[i].KD_PRODUCT
    //        };

    //        var dupes =  [];
    //        var val = [];
    //        $.each(res, function (index, entry) {
    //          if (!dupes[entry.ID_PRODUCT]) {
    //            dupes[entry.ID_PRODUCT] = true;
    //            val.push(entry);
    //          }
    //        });

    //        $(val).each(function(idx, elm) {
    //          addCheckbox(elm.ID_PRODUCT, "OPT_PRODUCT", "opt_product", elm.KD_PRODUCT, 'radio');
    //        });
    //      }
    //    });
    //  }else{
    //    var vArea = $('.OPT_AREA:checkbox:checked').map(function() {
    //      return this.value;
    //    }).get();

    //    var vGroup = $('.OPT_GROUPAREA:radio:checked').map(function() {
    //      return this.value;
    //    }).get();

    //    $("#opt_component :checkbox").remove();
    //    $(".opt_component").remove();
    //    $('label[for="OPT_COMPONENT[]"]').remove();
    //    $.ajax({
    //      type: "POST",
    //      dataType: "json",
    //      data: {group: vGroup, area: vArea, dh_ly:$("#dh_ly").val()},
    //      url: '<?php echo site_url("quality_trend/ajax_get_component/");?>',
    //      success: function(res){

    //        for (var i = 0; i < res.length; i++) {
    //          delete res[i].ID_PARAMETER
    //          delete res[i].ID_PLANT
    //          delete res[i].ID_GROUPAREA
    //          delete res[i].NM_COMPANY
    //          delete res[i].NM_PLANT
    //          delete res[i].DISPLAY
    //        };

    //        var dupes =  [];
    //        var val = [];
    //        $.each(res, function (index, entry) {
    //          if (!dupes[entry.ID_COMPONENT]) {
    //            dupes[entry.ID_COMPONENT] = true;
    //            val.push(entry);
    //          }
    //        });
    //        addCheckbox('all', "OPT_COMPONENT", "opt_component", 'Check All');
    //        $(val).each(function(idx, elm) {
    //          addCheckbox(elm.ID_COMPONENT, "OPT_COMPONENT", "opt_component", strtoupper(elm.KD_COMPONENT));
    //        });
    //      }
    //    });
    //  }
    //})

    // $(document).on("change", ".OPT_PRODUCT",function(event){
    //   var vArea = $('.OPT_AREA:checkbox:checked').map(function() {
    //     return this.value;
    //   }).get();

    //   var vGroup = $('.OPT_GROUPAREA:radio:checked').map(function() {
    //     return this.value;
    //   }).get();

    //   $("#opt_component :checkbox").remove();
    //   $(".opt_component").remove();
    //   $('label[for="OPT_COMPONENT[]"]').remove();
    //   $.ajax({
    //     type: "POST",
    //     dataType: "json",
    //     data: {group: vGroup, area: vArea, dh_ly:$("#dh_ly").val()},
    //     url: '<?php echo site_url("quality_trend/ajax_get_component/");?>',
    //     success: function(res){

    //       for (var i = 0; i < res.length; i++) {
    //         delete res[i].ID_PARAMETER
    //         delete res[i].ID_PLANT
    //         delete res[i].ID_GROUPAREA
    //         delete res[i].NM_COMPANY
    //         delete res[i].NM_PLANT
    //         delete res[i].DISPLAY
    //       };

    //       var dupes =  [];
    //       var val = [];
    //       $.each(res, function (index, entry) {
    //         if (!dupes[entry.ID_COMPONENT]) {
    //           dupes[entry.ID_COMPONENT] = true;
    //           val.push(entry);
    //         }
    //       });
    //       addCheckbox('all', "OPT_COMPONENT", "opt_component", 'Check All');
    //       $(val).each(function(idx, elm) {
    //         addCheckbox(elm.ID_COMPONENT, "OPT_COMPONENT", "opt_component", strtoupper(elm.KD_COMPONENT));
    //       });
    //     }
    //   });
    // });

    // $(document).on('click', "input[type=checkbox][value='all']", function(){
    //   $('.OPT_COMPONENT').not(this).prop('checked', this.checked);
    // })

    $("#btLoad").click(function(event){
      event.preventDefault();


      var month_start   = $("#month_start").val();
      var year_start    = $("#year_start").val();
      var month_end     = $("#month_end").val();
      var year_end      = $("#year_end").val();

      var plants = $('.OPT_PLANT:checkbox:checked').map(function() {
        return this.value;
      }).get();

      var components = $('.OPT_COMPONENT:checkbox:checked').map(function() {
        return this.value;
      }).get();

      var products = $('.OPT_PRODUCT:radio:checked').map(function() {
        return this.value;
      }).get();


      if ( [plants.length, components.length, products.length].indexOf(0) != -1 ) {
        $("#a-notice-error").data("text", 'Either Plant, Product, or Component not selected yet');
        $("#a-notice-error").click();
        
        return false;
      }

      // TODO check whether to date is always after from date
      // elsewhere return false
      var date_start = new Date;
      date_start.setDate(1); date_start.setMonth(month_start-1); date_start.setYear(year_start);
      var date_end = new Date
      date_end.setDate(1); date_end.setMonth(month_end-1); date_end.setYear(year_end);

      if ( date_end < date_start ) {
        $("#a-notice-error").data("text", 'End period must be after or equal start period');
        $("#a-notice-error").click();
        
        return false;
      }

      /* Animation */
      $body = $("body");
      $body.addClass("loading");
      $body.css("cursor", "progress");

      $.ajax({
          type: "POST",
          dataType: "json",
          data: {
            month_start: $("#month_start").val(),
            year_start: $("#year_start").val(),
            month_end: $("#month_end").val(),
            year_end: $("#year_end").val(),
            plants: plants,
            components: components,
            products: products
          },
          url: '<?php echo site_url("quality_pairing/generate_graph/");?>',
          success: function(res){
            var val = res;

            //console.log(res);
            $("#divTable").show()
            $(".boxPlot_div").remove();

            if(val.msg=='200'){

              for (var i = 0; i < parseInt(val.data.length); i++) {

                addBoxplotDiv(i);

                BoxPlot = document.getElementById('boxPlot_' + i);
                $("#boxPlot_" + i).css('display','');
                Plotly.purge(BoxPlot);
                Plotly.plot(BoxPlot, {
                  data: val.data[i].data,
                  layout: val.layout[i].layout
                });
              };

            }else{
              $("#a-notice-error").data("text", 'Data not found!');
              $("#a-notice-error").click();
            }
          },
          error: function(jqXHR, textStatus, errorThrown) {
            $("#a-notice-error").data("text", 'Oops! Something went wrong.<br>Please check message in console');
            $("#a-notice-error").click();

            console.log("XHR: " + JSON.stringify(jqXHR));
            console.log("Status: " + textStatus);
            console.log("Error: " + errorThrown);
          }
        });


    });

    /** Notification **/
    $(".notice-error").confirm({
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });

    function load_component_checkbox_list() {
      var plant_ids = [];
      var product_ids= [];

      product_ids = $('.OPT_PRODUCT:radio:checked')
        .map(function(idx) {
          return this.value
        })
        .get()

      plant_ids = $('.OPT_PLANT:checkbox:checked')
        .map(function(idx) {
          return this.value
        })
        .get()
      
      clear_component_checkbox_list();

      // load request
      $.ajax({
        url: '<?php echo site_url("quality_pairing/ajax_get_component_by_product");?>',
        method: "GET",
        dataType: "json",
        data: {
          product_ids: product_ids,
          plant_ids: plant_ids
        },
        success: function(response) {

          if ( response.length == 0 ) {
            $("#a-notice-error").data("text", 'No component found');
            $("#a-notice-error").click();

            return true;
          }

          addCheckbox('semua', "OPT_COMPONENT_ALL", "opt_component", 'Check All');
          $.each(response, function(idx, elm) {
            addCheckbox(elm.ID_COMPONENT, "OPT_COMPONENT", "opt_component", elm.NM_COMPONENT, 'checkbox');
          });

          show_component_checkbox_list();

          console.log(response) 
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus)
          console.error(textStatus)
        }
      })
    }

    function load_product_checkbox_list() {
      let plant_ids = [];

      plant_ids = $('.OPT_PLANT:checkbox:checked')
        .map(function(idx) {
          return this.value
        })
        .get()

      clear_product_checkbox_list();
      

      // load request
      $.ajax({
        // url: '<?php echo site_url("quality_pairing/ajax_get_product_by_plant");?>',
        url: '<?php echo site_url("quality_pairing/ajax_get_product_by_plant_read");?>', // izza jan 2021
        method: "GET",
        dataType: "json",
        data: {
          plant_ids: plant_ids
        },
        success: function(response) {
          if ( response.length == 0 ) {
            $("#a-notice-error").data("text", 'No product found');
            $("#a-notice-error").click();

            return true;
          }

          $.each(response, function(idx, elm) {
            addCheckbox(elm.ID_PRODUCT, "OPT_PRODUCT", "opt_product", elm.NM_PRODUCT, 'radio');
          });
          
          show_product_checkbox_list();

          console.log(response) 
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus)
          console.error(textStatus)
        }
      })
    }

    function load_plant_checkbox_list() {
      let company_ids = [];

      company_ids = $('.OPT_COMPANY:checkbox:checked')
        .map(function(idx) {
          return this.value
        })
        .get()
      

      clear_plant_checkbox_list();

      // load request
      $.ajax({
        url: '<?php echo site_url("quality_pairing/ajax_get_plant_by_company");?>',
        method: "GET",
        dataType: "json",
        data: {
          company_ids: company_ids
        },
        success: function(response) {
          response.sort(function(first, second) {
            return first.ID_COMPANY < second.ID_COMPANY;
          })

          addCheckbox('semua', "OPT_PLANT_ALL", "opt_plant", 'Check All');
          $.each(response, function(idx, elm) {
            addCheckbox(elm.ID_PLANT, "OPT_PLANT", "opt_plant", elm.NM_PLANT);
          });

          show_plant_checkbox_list();

          console.log(response) 
        },
        error: function(jqXHR, textStatus, errorThrown) {
          alert(textStatus)
          console.error(textStatus)
        }
      })
    }

    function clear_plant_checkbox_list() {
      $("#planarea").hide()
      $("#opt_plant :checkbox").remove();
      $(".opt_plant").remove();
      $('label[for="OPT_PLANT[]"]').remove();
    }

    function show_plant_checkbox_list() {
      $("#planarea").show()
    }

    function clear_product_checkbox_list() {
      $("#produk").hide()
      $("#opt_product :checkbox").remove();
      $(".opt_product").remove();
      $('label[for="OPT_PRODUCT[]"]').remove();
    }

    function show_product_checkbox_list() {
      $("#produk").show()
    }

    function clear_component_checkbox_list() {
      $("#komponen").hide()
      $("#opt_component :checkbox").remove();
      $(".opt_component").remove();
      $('label[for="OPT_COMPONENT[]"]').remove();
    }

    function show_component_checkbox_list() {
      $("#komponen").show()
    }

    $(document).on('click', '.OPT_COMPANY:checkbox', function(e) {
      clear_component_checkbox_list();
      clear_product_checkbox_list();
      clear_plant_checkbox_list();
      load_plant_checkbox_list()
    })


    // $(document).on('click', '.OPT_PLANT:checkbox', function(e) {
    $("#btLoadProduct").click(function(event){ // izza 23.07.21
      clear_component_checkbox_list();
      clear_product_checkbox_list();
      load_product_checkbox_list();
    })


    $(document).on('click', '.OPT_PRODUCT:radio', function(e) {
      clear_component_checkbox_list();
      load_component_checkbox_list() 
    })

    $(document).on('click', "input.OPT_COMPANY_ALL[type=checkbox][value='semuanya']", function(){
      $('.OPT_COMPANY_ALL').not(this).prop('checked', this.checked);
      $('.OPT_COMPANY').not(this).prop('checked', this.checked);

      clear_component_checkbox_list();
      clear_product_checkbox_list();
      clear_plant_checkbox_list();
      load_plant_checkbox_list()
    })

    $(document).on('click', '.OPT_PLANT_ALL:checkbox', function(e) {
      $('input.OPT_PLANT[type=checkbox]').prop('checked', this.checked);

      clear_component_checkbox_list();
      clear_product_checkbox_list();
      load_product_checkbox_list()
    })

    $(document).on('click', '.OPT_COMPONENT_ALL:checkbox', function(e) {
      $('input.OPT_COMPONENT[type=checkbox]').prop('checked', this.checked);
    })
  
    $(document).on('click', "input.OPT_PLANT[type=checkbox][value='semua']", function(){
      $('.OPT_PLANT').not(this).prop('checked', this.checked); 

      clear_component_checkbox_list();
      clear_product_checkbox_list();
      load_product_checkbox_list();
    })


  });





</script>
