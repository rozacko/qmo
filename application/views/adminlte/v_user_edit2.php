<style type="text/css">
  .select2-results__option .wrap:before{
    font-family:fontAwesome;
    color:#999;
    content:"\f096";
    width:25px;
    height:25px;
    padding-right: 10px;
    
  }
  .select2-results__option[aria-selected=true] .wrap:before{
      content:"\f14a";
  }

  /* not required css */

  .row
  {
    padding: 10px;
  }

  .select2-multiple, .select2-multiple2
  {
    width: 50%
  }
</style>

   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      	Master data user
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
       <div class="row">
        <div class="col-xs-3"></div>
        <div class="col-md-6">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Edit</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="bodyContent">


            	<form role="form" method="POST" action="<?php echo site_url("user/updateProcess/".$this->data_user->ID_USER) ?>" >
		          <div class="box-body" style="">
		            
		            <div class="form-group">
		                <label>FULLNAME </label>
		                <input type="text" class="form-control" readonly placeholder="Full Name" value="<?php echo $this->data_user->FULLNAME ?>" >
		            </div>
		            <div class="form-group">
		                <label>USERNAME </label>
		                <input type="text" class="form-control" placeholder="User Name" readonly value="<?php echo $this->data_user->USERNAME ?>">
		            </div>
		            <div class="form-group">
		                <label>USER GROUP </label>   
                    <input type="checkbox" id="checkbox" title="Select All" class="pull-right">
		                <select multiple="multiple" class="form-control select2"  style="width: 100%" id="ID_USERGROUP" NAME="ID_USERGROUP" >
		                  <?php  
                      foreach($this->list_usergroup as $a){
                        $selected = '';
                        foreach ($this->data_user->USERGROUP as $b) {
                          if($a->ID_USERGROUP == $b->ID_USERGROUP){
                            $selected = 'SELECTED'; 
                          }
                        }
                        if($this->adm){
                          echo '<option value="'.$a->ID_USERGROUP.'" '.$selected.'>'.$a->NM_USERGROUP.'</option>';
                        }else{
                          if($a->ID_USERGROUP != 1){
                            echo '<option value="'.$a->ID_USERGROUP.'" '.$selected.'>'.$a->NM_USERGROUP.'</option>';
                          }                          
                        }
                      }
                      ?>
		                </select>
		            </div>
		            
		            <div class="form-group" id="div_company">
		                <label>COMPANY </label>
		                <select class="form-control select2" style="width: 100%" name="ID_COMPANY" id="ID_COMPANY">
                      <option value="">Choose Company...</option>
		                  <?php  foreach($this->list_company as $company): ?>
		                  <option value="<?php echo $company->ID_COMPANY ?>" <?php echo ($this->data_user->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":""; ?> ><?php echo $company->NM_COMPANY ?></option>
		                  <?php endforeach; ?>
		                </select>
		            </div>
		            
		            <div class="form-group" id="div_plant">
		                <label>PLANT </label>
		                <select class="form-control select2" style="width: 100%" NAME="ID_PLANT" id="ID_PLANT">
							<option value=""></option>
		                </select>
		            </div>
		            
                <div class="form-group" id="div_area" >
                    <label>AREA</label>
                    <select class="form-control select2" style="width: 100%" NAME="ID_AREA" id="ID_AREA">
                      <option value=""></option>
                    </select>
                </div>
                <div class="form-group" id="div_jobgrup" >
                    <label>Job Grup</label>                   
                    <select class="form-control select" style="width: 100%" name="FLAG_GRUP" id="FLAG_GRUP">
                      <option value="">Choose Job Group...</option>
                      <?php                        
                          foreach($this->list_jobgrup as $jg){ 
                            $selected = "";
                            if($this->data_user->FLAG_GRUP == $jg[FLAG_GRUP]){
                              $selected = "selected";
                            }
                              echo "<option value=".$jg[FLAG_GRUP]." ".$selected."> ".$jg[NM_JOBGRUP]."</option>";
                          }
                      ?>
		                </select>
                </div>
		            <div class="form-group" id="div_area" >
		                <label>ACTIVE DIRECTORY</label><br>
                    <input type="radio" name="LDAP" <?=$this->data_user->LDAP == 'Y' ? 'checked' : '' ?> value="Y"> Active 
                    <input type="radio" name="LDAP" <?=$this->data_user->LDAP == 'N' ? 'checked' : '' ?> value="N"> Non Active 
		            </div>

		          </div>
		          <!-- /.box-body -->
		          <div class="box-footer">
		            <button type="submit" class="btn btn-primary">Save</button>
                <a href="<?=base_url('user')?>">
                  <button type="button" class="btn btn-danger">Cancel</button>
                </a>
		          </div>
		        </form>
            </div>
          </div> 
        </div>
      </div>
    </section>

    <!-- Additional CSS -->
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css");?>" rel="stylesheet">

<!-- Additional JS-->
<script src="<?php echo base_url("plugins/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js");?>"/></script>        
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
    
<script language="javascript" type="text/javascript">

$(function(){
$("#checkbox").click(function(){
    if($("#checkbox").is(':checked') ){
        $("#ID_USERGROUP > option").prop("selected","selected");
        $("#ID_USERGROUP").trigger("change");
    }else{
        $("#ID_USERGROUP > option").removeAttr("selected");
         $("#ID_USERGROUP").trigger("change");
     }
});

// $("#button").click(function(){
//        alert($("#e1").val());
// });
	USER_COMPANY = '<?php echo $this->data_user->ID_COMPANY ?>';
	USER_PLANT = '<?php echo $this->data_user->ID_PLANT ?>';
	USER_AREA = '<?php echo $this->data_user->ID_AREA ?>';

	$('.select2').select2();

	listPlant(USER_COMPANY);
	listArea(USER_PLANT);

  $('#ID_COMPANY').on('change', function(){
    idCompany = $("option:selected", this).val();
    listPlant(idCompany);
  });	
  $('#ID_PLANT').on('change', function(){
    idPlant = $("option:selected", this).val();
    listArea(idPlant);
  });

  function listPlant(idCompany = ''){

      $.ajax({
        url : '<?=base_url("login/getPlant/")?>'+idCompany,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#ID_PLANT').html('');
        $.map( data, function( val, i ) {
        	user_plant = '<?=$this->data_user->ID_PLANT?>';
        	selected = '';
        	if(val.ID_PLANT == user_plant){
        		selected = 'selected';
        	}
          $('#ID_PLANT').append('<option value="'+val.ID_PLANT+'" '+selected+'>'+val.NM_PLANT+'</option>');
        });
      });

      $('select2').select2();
  }

  function listArea(idPlant = ''){
      $.ajax({
        url : '<?=base_url("login/getArea/")?>'+idPlant,
        type: 'GET',
      }).done(function(data){
        data = JSON.parse(data);
        $('#ID_AREA').html('');
        $.map( data, function( val, i ) {
        	user_area = '<?=$this->data_user->ID_AREA?>';
        	selected = '';
        	if(val.ID_AREA == user_area){
        		selected = 'selected';
        	}
          $('#ID_AREA').append('<option value="'+val.ID_AREA+'" '+selected+'>'+val.NM_AREA+'</option>');
        });
      });

      $('select2').select2();
  }

  /* Auto Complete */
  var options = {
    url: function(username) {
      return '<?php echo site_url("user/get_username/") ?>';
    },

    getValue: function(element) {
      return element.MK_NAMA;
    },

    //Description
    template: {
      type: "description",
      fields: {
        description: "MK_CCTR_TEXT"
      }
    },
    
    //Set return data
    list: {
      onSelectItemEvent: function() {
        var selectedItemValue = $("#fullName").getSelectedItemData().USERNAME;
        $("#uName").val(selectedItemValue).trigger("change");
      }
    },

    ajaxSettings: {
      dataType: "json",
      method: "POST",
      data: {
        dataType: "json"
      }
    },

    //Sending post data
    preparePostData: function(data) {
      data.username = $("#fullName").val();
      return data;
    },
    theme: "plate-dark", //square | round | plate-dark | funky
    requestDelay: 400
  };

  $("#fullName").easyAutocomplete(options);

});

$('#agree').on('click', function(){
	$('.keterangan').hide();
	$('form').show();
});
$('#cancelAgree').on('click', function(){
	$('.keterangan').show();
	$('form').hide();
});
$('#disagree').on('click', function(){
  	$.confirm({
      	title: 'Disagree',
      	text:'Are you sure, you want to reject this account request?',
		confirmButton: 'Disagree',
		confirmButtonClass: 'btn-danger',
		cancelButton: "Cancel",
		confirm: function() {
			$.ajax({
					url: '../../update/<?=$this->data_user->ID_USER?>/disagree',
					method: "GET",
				}).done(function(response) {
				  	response = JSON.parse(response);
				  	if(response['status'] == 'success'){
				        notif(1, response['message'])                        
				        setTimeout(function(){ window.location = "../../"; }, 2500);
				  	}else{
				   	 	notif(4, response['message']);
					   }	

				});      	},
 		cancel: function() {
          // nothing to do
      	}
  	});
});

  $("form").on("submit", function(e) {
    e.preventDefault();
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method");
    var form_data = $(this).serialize();
    var valUserGroup = $('#ID_USERGROUP').val();
    // console.log(form_data);
    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data + '&ID_USERGROUP='+valUserGroup
    }).done(function(response){
    	response = JSON.parse(response);
    	if(response['status'] == 'success'){
    		typeStatus = 1;
        setTimeout(function(){window.location.assign("../"); }, 2000);    	
      }else{
    		typeStatus = 4;
    	}
		notif(typeStatus, response['message']);
    });
  });

</script>