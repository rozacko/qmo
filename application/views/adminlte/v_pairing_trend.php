<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<section class="content-header">
	<h1>Trend & Boxplot Pairing Data Quality Outsilo</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
                    <form id="formData">
                        <div class="form-group row">
                            <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                                <label for="month_start">FROM</label>
                                <SELECT style="width:auto;" class="form-control select2" name="MONTH_START" id='month_start'>
                                <?php for($i=1;$i<=12;$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            <div class="form-group col-sm-2 col-sm-2 year">
                                <label for="year_start"></label>
                                <SELECT style="width:auto;" class="form-control select2" name="YEAR_START" id='year_start'>
                                <?php for($i=2016;$i<=date("Y");$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            
                            <div class="form-group col-sm-2 col-sm-1 month" style="margin-right:60px;">
                                <label for="month_end">TO</label>
                                <SELECT style="width:auto;" class="form-control select2" name="MONTH_END" id='month_end'>
                                <?php for($i=1;$i<=12;$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("m")==$i) ? "selected":"";?>><?php echo strtoupper(date("F", mktime(0, 0, 0, $i, 10))); ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                            <div class="form-group col-sm-2 col-sm-2 year">
                                <label for="year_end">YEAR</label>
                                <SELECT style="width:auto;" class="form-control select2" name="YEAR_END" id='year_end'>
                                <?php for($i=2016;$i<=date("Y");$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo (date("Y")==$i) ? "selected":"";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                                </SELECT>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                <label for="OPT_COMPANY">COMPANY</label><br>
                                <input type="checkbox" class="OPT_COMPANY_ALL" value="semuanya"> Check All
                                <?php  foreach($this->list_company as $company):
                                    $checked  = ($this->USER->ID_COMPANY==$company->ID_COMPANY) ? 'checked':NULL;
                                ?>
                                <div>
                                    <label>
                                        <input type="checkbox" NAME="OPT_COMPANY[]" class="minimal OPT_COMPANY" value="<?php echo $company->ID_COMPANY;?>" <?php echo $checked;?>>
                                        <?php echo $company->NM_COMPANY;?>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                            <!-- product type radio list -->
                            <div id="produk" style="display:none;" class="col-sm-2">
                                <label for="OPT_PRODUCT">PRODUCT TYPE</label><br>
                                <div id="opt_product" style="height: 250px;overflow-y: scroll;"></div>
                            </div>
                            
                            <!-- component radio list -->
                            <div id="komponen" style="display:none;" class="col-sm-2">
                                <label for="OPT_COMPONENT">COMPONENT</label><br>
                                <div id="opt_component" style="height: 250px;overflow-y: scroll;"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="form-group col-sm-6 col-sm-4">
                                <button class="btn-primary" name="load" id="btLoad">Load Data</button>
                            </div>
                        </div>
                    </form>
                </div>
				<div class="box-body">
                    <div class="row">
                        <div class="col-xs-12" id="box-trend-chart" style="display:none;">
                            <div class="box">
                                <div class="box-header"><center><h4><b>Trend Pairing Data Quality</b></h4></center></div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12" id="trend_pairing_chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12" id="box-boxplot-chart" style="display:none;">
                            <div class="box">
                                <div class="box-header"><center><h4><b>Boxplot Pairing Data Quality</b></h4></center></div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12" id="boxplot_pairing_chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</section>
<a id="a-notice-error" class="notice-error" style="display:none"; href="#" data-title="Alert" data-text=""></a>
<div class="modal_load"><!-- Loading modal --></div>
<style>
    .modal_load {
      display:    none;
      position:   fixed;
      z-index:    1000;
      top:        0;
      left:       0;
      height:     100%;
      width:      100%;
      background: rgba( 255, 255, 255, .8 )
                  url('<?php echo base_url("images/ajax-loader-modal.gif");?>')
                  50% 50%
                  no-repeat;
  }

  /* When the body has the loading class, we turn
     the scrollbar off with overflow:hidden */
  body.loading {
      overflow: hidden;
  }

  /* Anytime the body has the loading class, our
     modal element will be visible */
  body.loading .modal_load {
      display: block;
  }
</style>
<script src="<?php echo base_url("plugins/plotly/plotly-latest.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>
    $(document).ready(function(){

    });

    $("#btLoad").click(function(e){
        e.preventDefault();

        var month_start   = $("#month_start").val();
        var year_start    = $("#year_start").val();
        var month_end     = $("#month_end").val();
        var year_end      = $("#year_end").val();
        
        var companys = $('.OPT_COMPANY:checkbox:checked').map(function() {
            return this.value;
        }).get();
        
        var components = $('.OPT_COMPONENT:checkbox:checked').map(function() {
            return this.value;
        }).get();
        
        var products = $('.OPT_PRODUCT:radio:checked').map(function() {
            return this.value;
        }).get();

        if ( [companys.length, components.length, products.length].indexOf(0) != -1 ) {
            $("#a-notice-error").data("text", 'Either Company, Product, or Component not selected yet');
            $("#a-notice-error").click();
            
            return false;
        }
        
        var date_start = new Date;
        date_start.setDate(1); date_start.setMonth(month_start-1); date_start.setYear(year_start);
        var date_end = new Date
        date_end.setDate(1); date_end.setMonth(month_end-1); date_end.setYear(year_end);
        if ( date_end < date_start ) {
            $("#a-notice-error").data("text", 'End period must be after or equal start period');
            $("#a-notice-error").click();
            return false;
        }

        trend(month_start, year_start, month_end, year_end, companys, components, products);
    });
    function trend(month_start, year_start, month_end, year_end, companys, components, products){
        $("#box-trend-chart").css("display", "none");
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {
                month_start: month_start,
                year_start: year_start,
                month_end: month_end,
                year_end: year_end,
                companys: companys,
                components: components,
                products: products
            },
            url: '<?php echo site_url("pairing_trend/trend_chart");?>',
            success: function(response){
                // console.log(response)
                if(response.status == "200"){
                    var flex = 12 / response.data.length;
                    var html = '';
                    var data = response.data;

                    data.forEach(function(item, index){
                        html += '<div class="col-xs-'+flex+'"><div id="trend_pairing_'+index+'"></div></div>';
                    });
                    $("#trend_pairing_chart").html(html);

                    data.forEach(function(item, index){
                        // console.log(item.value)
                        var layout = {
                            title: item.title,
                            autosize: false,
                            width:500,
                            height:500,
                            showlegend: true,
                            legend: {
                                "orientation": "h"
                            },
                            yaxis: {
                                autorange: false,
                                range: response.y_range,
                                type: 'linear'
                            }
                        };

                        Plotly.newPlot('trend_pairing_' + index, item.value, layout);

                    });

                    $("#box-trend-chart").css("display", "block");
                    boxplot(month_start, year_start, month_end, year_end, companys, components, products);
                } else {
                    $("#a-notice-error").data("text", 'Data not found!');
                    $("#a-notice-error").click();
                }
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus)
            },
            complete: function(data){
                var dt = data.responseJSON;
                var dt = dt.data;
                dt.forEach(function(item, index){
                    Plotly.relayout('trend_pairing_'+index, {
                        'xaxis.autorange': true
                    });
                });
                
            }
        });
    }

    function boxplot(month_start, year_start, month_end, year_end, companys, components, products){
        $("#box-boxplot-chart").css("display", "none");
        $.ajax({
            type: "POST",
            dataType: "json",
            data: {
                month_start: month_start,
                year_start: year_start,
                month_end: month_end,
                year_end: year_end,
                companys: companys,
                components: components,
                products: products
            },
            url: '<?php echo site_url("pairing_trend/boxplot_chart");?>',
            success: function(response){
                // console.log(response)
                if(response.status == "200"){
                    var flex = 12 / response.data.length;
                    var html = '';
                    var data = response.data;
                    data.forEach(function(item, index){
                        html += '<div class="col-xs-'+flex+'"><div id="boxplot_pairing_'+index+'"></div></div>';
                    });
                    $("#boxplot_pairing_chart").html(html);

                    data.forEach(function(item, index){
                        // start izza 16.03.2021
                        var value_fix = [];
                        item.value.forEach(value => {
                            if(value.type=='lines'){
                                value['line'] = {"dash": "dot", "width": 1};
                                value['mode'] = "lines";
                            }
                            value_fix.push(value);
                        });
                        console.log(item.value)
                        console.log(value_fix)
                        // end izza 16.03.2021
                    
                        var layout = {
                            title: item.title,
                            autosize: true,
                            boxmode: 'group',
                            showlegend: true,
                            legend: {
                                "orientation": "h"
                            },
                            yaxis: {
                                autorange: false,
                                range: response.y_range,
                                type: 'linear'
                            }
                        };
                        // Plotly.newPlot('boxplot_pairing_' + index, value_fix, layout);
                        Plotly.newPlot('boxplot_pairing_' + index, value_fix, layout); // izza 16.03.2021

                    });

                    $("#box-boxplot-chart").css("display", "block");
                } else {
                    $("#a-notice-error").data("text", 'Data not found!');
                    $("#a-notice-error").click();
                }
                
                
            },
            complete: function(data){
                var dt = data.responseJSON;
                var dt = dt.data;
                dt.forEach(function(item, index){
                    Plotly.relayout('boxplot_pairing_'+index, {
                        'xaxis.autorange': true
                    });
                });
            }
        });
    }

    $(document).on('click', '.OPT_COMPANY:checkbox', function(e) {
        clear_product_checkbox_list();
        clear_component_checkbox_list();
        load_product_checkbox_list();
    });
    
    $(document).on('click', "input.OPT_COMPANY_ALL[type=checkbox][value='semuanya']", function(){
        $('.OPT_COMPANY_ALL').not(this).prop('checked', this.checked);
        $('.OPT_COMPANY').not(this).prop('checked', this.checked);
        clear_product_checkbox_list();
        clear_component_checkbox_list();
        load_product_checkbox_list();
    })

    $(document).on('click', '.OPT_PRODUCT:radio', function(e) {
      clear_component_checkbox_list();
      load_component_checkbox_list() 
    })

    $(document).on('click', '.OPT_COMPONENT_ALL:checkbox', function(e) {
        $('input.OPT_COMPONENT[type=checkbox]').prop('checked', this.checked);
    })

    function load_product_checkbox_list(){
        var company_ids = [];
        
        company_ids = $('.OPT_COMPANY:checkbox:checked').map(function(idx) {
            return this.value
        }).get();
        
        $.ajax({
            // url: '<?php echo site_url("pairing_trend/ajax_get_product_by_company");?>',
            url: '<?php echo site_url("pairing_trend/ajax_get_product_by_company_read");?>', // izza jan 2021
            method: "GET",
            dataType: "JSON",
            data: {
                company_ids: company_ids
            },
            success: function(response) {
                if ( response.length == 0 ) return true;
                $.each(response, function(idx, elm) {
                    addCheckbox(elm.ID_PRODUCT, "OPT_PRODUCT", "opt_product", elm.NM_PRODUCT, 'radio');
                });
                
                show_product_checkbox_list();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus)
            }
        })
    }

    function load_component_checkbox_list() {
        var company_ids = [];
        var product_ids = [];
        
        product_ids = $('.OPT_PRODUCT:radio:checked').map(function(idx) {
            return this.value
        }).get()
        
        company_ids = $('.OPT_COMPANY:checkbox:checked').map(function(idx) {
            return this.value
        }).get()
        
        $.ajax({
            url: '<?php echo site_url("pairing_trend/ajax_get_component_by_company");?>',
            method: "GET",
            dataType: "json",
            data: {
                product_ids: product_ids,
                company_ids: company_ids
            },
            success: function(response) {
                if ( response.length == 0 ) {
                    $("#a-notice-error").data("text", 'NO COMPONENT ON PLANT OUTSILO');
                    $("#a-notice-error").click()
                    return true;
                }
                
                addCheckbox('semua', "OPT_COMPONENT_ALL", "opt_component", 'Check All');
                $.each(response, function(idx, elm) {
                    addCheckbox(elm.ID_COMPONENT, "OPT_COMPONENT", "opt_component", elm.KD_COMPONENT, 'checkbox');
                });
                
                show_component_checkbox_list();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus)
            }
        })
    }

    function show_product_checkbox_list() {
        $("#produk").show()
    }

    function show_component_checkbox_list() {
        $("#komponen").show()
    }

    function clear_product_checkbox_list() {
        $("#produk").hide()
        $("#opt_product :checkbox").remove();
        $(".opt_product").remove();
        $('label[for="OPT_PRODUCT[]"]').remove();
    }

    function clear_component_checkbox_list() {
        $("#komponen").hide()
        $("#opt_component :checkbox").remove();
        $(".opt_component").remove();
        $('label[for="OPT_COMPONENT[]"]').remove();
    }

    function addCheckbox(val, namaFor, divid, txt, tipe) {
        tipe = tipe || 'checkbox';
        var container = document.getElementById(divid);
        var divgrup   = document.createElement('div');
            divgrup.className = divid;
        var input = document.createElement('input');
            input.type = tipe;
            input.className = namaFor;
            input.name = namaFor+"[]";
            input.id = namaFor + "-" + val
            input.value = val;
        var label = document.createElement('label');
            label.htmlFor = input.id;
            
        container.appendChild(divgrup);
        divgrup.appendChild(label);
        label.appendChild(input);
        label.appendChild(document.createTextNode("    "+txt));
    }

    $(".notice-error").confirm({
        confirm: function(button) { /* Nothing */ },
        confirmButton: "OK",
        cancelButton: "Cancel",
        confirmButtonClass: "btn-danger"
    });
</script>