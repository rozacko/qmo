   <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Master Data Set up pelaksanaan
        <small></small>
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
		
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
			
		<?php if($notice->error): ?>

			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Error!</h4>
				<?php echo $notice->error; ?>
			</div>

		<?php endif; ?>
		
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Data Set up pelaksanaan</h3>
            </div>
            <!-- /.box-header -->
		

            <!-- form start -->
            <form role="form" method="POST" action="<?php echo site_url("Uji_proficiency_setup/create") ?>" >
              <div class="box-body" style="background-color:#c5d5ea;">
			  <div class="col-sm-12 clearfix">
			  <br>
				<div class="form-group">
				<div class="col-sm-6 clearfix">
					<label>Periode Proficiency</label>
						<select class="form-control select2" NAME="ID_PP" id="ID_PP" placeholder="Select a Periode Proficiency ...">
						<option value="">Select a Periode Proficiency ...</option>
						<?php  foreach($this->list_periode as $dt_periode): ?>
						  <option value="<?php echo $dt_periode->ID_PP; ?>" ><?= $dt_periode->TITLE_PP;?> [<?= $dt_periode->GROUP_PP;?> - <?= $dt_periode->YEAR_PP;?>]</option>
						<?php endforeach; ?>
						</select>              
					</div>
					<div class="col-sm-6 ">
					<label>Commodity</label>
						<select class="form-control select2" NAME="ID_KOMODITI" id="ID_KOMODITI" placeholder="Select a Commodity ...">
						<option value="">Select a Commodity ...</option>
						<?php  foreach($this->list_comodity as $dt_comodity): ?>
						  <option value="<?php echo $dt_comodity->ID_SAMPLE ?>"  >[<?=  $dt_comodity->KODE_SAMPLE ?>] <?php echo $dt_comodity->NAMA_SAMPLE ?></option>
						<?php endforeach; ?>
						</select>              
					</div>
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
                  <div class="col-sm-6 clearfix">
					<label>Activity 1: <span style="color: blue;"><i>Preparing</i> (Persiapan)</span></label>
					<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Preparing" >
				  </div>
				  <div class="col-sm-3 ">
					<label>Start Date </label>
					<input type="date" class="form-control" name="start_date[]"  >
				  </div>
				  <div class="col-sm-3 ">
					<label>End Date </label>
					<input type="date" class="form-control" name="end_date[]"  id="a0">
				  </div>
				  
                </div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 2: <span style="color: blue;"><i>Delivering</i> (Pengiriman)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Delivering" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  id="a1">
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]"  id="b0">
					  </div>
					  
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 3: <span style="color: blue;"><i>Testing</i> (Pengujian)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Testing" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  id="b1">
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]"  id="c0">
					  </div>
					  
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 4: <span style="color: blue;"><i>Report to Holding</i> (Pelaporan ke Holding)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Report to Holding" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  id="c1">
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]" id="d0" >
					  </div>
					  
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 5: <span style="color: blue;"><i>Data Processing</i> (Pemrosesan Data)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Data Processing" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  id="d1">
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]" id="e0" >
					  </div>
					  
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 6: <span style="color: blue;"><i>Reporting</i> (Pelaporan)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Reporting" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]"  id="e1">
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]"  id="f0">
					  </div>
					  
				</div>
				
				<div class="form-group c-group after-add-more" id="utama">
					  <div class="col-sm-6 clearfix">
						<label>Activity 7: <span style="color: blue;"><i>Discuss / Workshop</i> (Diskusi / Workshop)</span></label>
						<input type="text" class="form-control" name="activity[]" placeholder="Activity name"  value="Discuss/Workshop" >
					  </div>
					  <div class="col-sm-3 ">
						<label>Start Date </label>
						<input type="date" class="form-control" name="start_date[]" id="f1" >
					  </div>
					  <div class="col-sm-3 ">
						<label>End Date </label>
						<input type="date" class="form-control" name="end_date[]"  >
					  </div>
					  
				</div>
				
                <div class="form-group">
                  <div class="col-sm-4 clearfix">
					<label>Laboratorium </label>
					<select class="form-control select2" NAME="ID_LAB" id="ID_LAB" placeholder="Select a Laboratorium ...">
					<option value="">Select a Laboratorium ...</option>
					<?php  foreach($this->list_lab as $dt_lab): ?>
					  <option value="<?php echo $dt_lab->ID_LAB ?>"  ><?php echo $dt_lab->NAMA_LAB ?></option>
					<?php endforeach; ?>
					</select>    
				  </div>
                               
				 <div class="col-sm-8 ">
					<label>PIC</label>
					<select class="form-control select2" NAME="ID_PIC" id="ID_PIC" placeholder="Select a PIC ...">
					<option value="">Select a PIC ...</option>
					<?php  foreach($this->list_pic as $dt_pic): ?>
					  <option value="<?php echo $dt_pic->ID_USER ?>"  ><?php echo $dt_pic->FULLNAME ?> [<?= $dt_pic->EMAIL ?>]</option>
					<?php endforeach; ?>
					</select>     
					   
				  </div>                
				</div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a type="submit" class="btn btn-danger" href="<?php echo site_url("Uji_proficiency_setup") ?>" >Cancel</a>
              </div>
            </form>
			</div>
          </div>

    </section>
    <!-- /.content -->
	
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />

	
<script type="text/javascript">
    $(document).ready(function() {
		
		$("#a0").on("change",function(){ 
			var dateSet = $("#a0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#a1").val(newnextdate);
		});
		$("#b0").on("change",function(){ 
			var dateSet = $("#b0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#b1").val(newnextdate);
		});
		$("#c0").on("change",function(){ 
			var dateSet = $("#c0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#c1").val(newnextdate);
		});
		$("#d0").on("change",function(){ 
			var dateSet = $("#d0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#d1").val(newnextdate);
		});
		$("#e0").on("change",function(){ 
			var dateSet = $("#e0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#e1").val(newnextdate);
		});
		$("#f0").on("change",function(){ 
			var dateSet = $("#f0").val();
			var result = new Date(new Date(dateSet).setDate(new Date(dateSet).getDate() + 1));
			var newnextdate = result.toISOString().substr(0, 10);
			$("#f1").val(newnextdate);
		});
		
	  $('select').selectize({
          sortField: 'text'
      });
	  
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").before(html);
		  
      });

      // saat tombol remove dklik control group akan dihapus 
      $("body").on("click",".remove",function(){ 
          $(this).parents(".c-group").remove();
      });
    });
</script>
