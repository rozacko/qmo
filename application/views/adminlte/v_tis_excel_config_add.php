<section class="content-header">
	<h1>Input Config Excel Data TIS</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
                    <div class="row">
                        <form action="<?= base_url('tis_excel_config/action_add') ?>" method="POST">
                            <div class="form-group">
                                <div class="col-sm-6 clearfix">
                                    <label>PLANT *</label>
                                    <select class="form-control" name="ID_PLANT" required>
                                        <option value="">Pilih Plant</option>
                                        <?php  foreach($this->list_plant as $plant): ?>
                                        <option value="<?= $plant->ID_PLANT ?>" <?= $selected ?> ><?= $plant->NM_PLANT ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 clearfix">
                                    <label>GROUPAREA *</label>
                                    <select class="form-control" name="ID_GROUPAREA" required>
                                        <option value="">Pilih Grouparea</option>
                                        <?php  foreach($this->list_grouparea as $grouparea):
                                            $selected  = ($this->USER->ID_GROUPAREA==$grouparea->ID_GROUPAREA) ? 'selected':NULL;
                                        ?>
                                        <option value="<?= $grouparea->ID_GROUPAREA ?>" <?= $selected ?> ><?= $grouparea->NM_GROUPAREA ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6 clearfix">
                                    <label>URL *</label>
                                    <input type="text" name="URL" class="form-control" required />
                                    <sub>Tuliskan direktori penyimpanan file excel TIS. ex: nama_plant/nama_grouparea<br/>
                                    (Nb : case sensitive)</sub>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 clearfix">
                                    <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                                    <a class="btn btn-warning" href="<?= base_url('tis_excel_config') ?>">Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>