<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quality Management Online</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/bootstrap/css/bootstrap.min.css"); ?>">
  
  <!-- Font Awesome -->
  <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"-->
  <link href="<?php echo base_url("css/font-awesome.min.css");?>" rel="stylesheet">

  <!-- Ionicons -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("assets/templates/adminlte/bootstrap/css/bootstrap.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("css/style_login.css") ?>">


  <!-- Ionicons -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/AdminLTE.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo base_url("templates/adminlte/dist/css/skins/skin-red-light.min.css") ?>">

</head>
<body class="LoginPage">


<div class="container">
            <div class="row">
                <div class="col-md-4 login-form-1">
                    <h3>Login</h3>
                    <?php if($notice->error): ?>
                    <p class="login-error"><?php echo $notice->error; ?></p>
                    <?php endif; ?>
                    <form action="<?php echo site_url("login/verification") ?>" method="post">
                        <div class="form-group has-feedback">
                            <input type="text" class="border"  placeholder="Username" name="USERNAME" />
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="border" placeholder="Password" name="PASSWORD"/>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btnSubmit">LOGIN</button>
                        </div>
                        <div class="link">
                            <p><a style="color:white;" href="<?php echo site_url("Full_behavior/Behavior") ?>">Coating Behavior</a></p>
                            <p><a style="color:white;" href="<?php echo site_url("login/register") ?>">Register a new membership</a></p>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 login-form-2" style="text-align: "> 
                    <p><img src="<?php echo base_url("images/Logo Color.png") ?>" width="20%"/></p>

                    <h1 style="text-align: center">Quality Management Online</h1>
                    <!-- <p align="center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p> -->
                
                    <div class="logoSemen">
                      <p align="center"><img src="<?php echo base_url("images/LogoSemenIndonesia.png") ?>" width="30%"/></p>
                      <p align="center" style="font-size:9px;">Copyright2018 All rights reserved. PT Semen Indonesia</p>
                    </div>
                </div>
            </div>
        </div>
 
<style>
.login-box {
  width: 500px;
}
.login-error {
  display: block;
  padding: 10px;
  border: 1px solid white;
  color: white;
}
</style>
</body>
</html>