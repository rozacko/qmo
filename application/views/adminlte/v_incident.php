  <script>
  </script>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1> NCQR Incident <small></small> </h1>

    </section>
    <input type="hidden" id="pageInfo" value="<?php echo $this->CURRENT;?>"> 
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <label>Status</label>
            <input type="checkbox" checked="checked" id="checkAllStatus" title="Select All" class="pull-right">
            <select id="status" name="status" multiple="multiple" class="form-control select2" style="width: 100%;" tabindex="-1" aria-hidden="true">
              <option selected="selected" value="1">In Progres</option>
              <?php
              if(!$this->receivAct){
                echo '<option selected="selected" value="2">Normal by System</option>';
              }
              ?>
              <option selected="selected" value="3">Resolved</option>
              <option selected="selected" value="closed">Closed</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label>Company</label>
            <select id="id_company" name="id_company" class="form-control select2">
              <option value="" SELECTED >All Company</option> <!-- izza 2021.03.03 -->
              <?php  foreach($this->list_company as $company):  
                ?>
                <option value="<?php echo $company->ID_COMPANY;?>" <?php echo ($this->ID_COMPANY == $company->ID_COMPANY)?"SELECTED":"";?> ><?php echo $company->NM_COMPANY;?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label style="width: 100%">&nbsp;</label>
              <button class=" btn btn-sm btn-primary" id="btnFilter"> <i class="fa fa-search"></i></button>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <table>
              <tr>
                <td>
                  <div class="box-header">
                  </div>
                </td>
              </tr>
              <!-- /.box-header -->
              <tr>
                <td>
                  <div class="box-body">
                    <table  id="dt_tables"
                            class="table table-striped table-bordered table-hover table-responsive dt-responsive nowrap"
                            cellspacing="0"
                            width="100%">
                      <thead>
                        <tr>
                          <th width="5%">ID</th>
                          <th width="5%">No.</th>
                          <th width="5%">Plant</th>
                          <th width="5%">Area</th>
                          <th width="30%">Subject</th>
                          <th width="30%">Date</th>
                          <!-- <th width="30%">Solved</th> -->
                          <!-- <th width="30%">Solution</th> -->
                          <th width="30%">Type</th>
                          <th width="30%">Status</th>
                          <th width="20%"></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>

      <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Open modal for @mdo</button> -->

      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h1 class="modal-title" id="exampleModalLabel">Detail Incident</h1>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?= base_url('incident/solving2')?>" method="POST">
              <input type="hidden" class="form-control" id="ID_INCIDENT" name="ID_INCIDENT" >
              <input type="hidden" class="form-control" id="ID_NCQR_STATUS" name="ID_NCQR_STATUS" >
              <input type="hidden" class="form-control" id="ID_ACTION" name="ID_ACTION" >
              <input type="hidden" class="form-control" id="UK_KODE" name="UK_KODE" >
              <input type="hidden" class="form-control" id="KD_AREA" name="KD_AREA" >
              <div class="modal-body">
                  <div class="row">
                    <div class="form-group col-lg-6">
                      <label for="DATE_INCIDENT" class="col-form-label">DATE_INCIDENT</label>
                      <input type="text" class="form-control" id="DATE_INCIDENT" name="DATE_INCIDENT" >
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-6">
                      <label for="UK_NAMA" class="col-form-label">UNIT KERJA</label>
                      <input type="text" class="form-control" id="UK_NAMA" name="UK_NAMA" >
                    </div>
                    <div class="form-group col-lg-6">
                      <label for="AREA" class="col-form-label">INCIDENT AREA</label>
                      <input type="text" class="form-control" id="AREA" name="AREA" >
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-lg-6">
                      <label for="REFERENCE" class="col-form-label">REFERENCE</label>
                      <input type="text" class="form-control" id="REFERENCE" name="REFERENCE" >
                    </div>
                    <div class="form-group col-lg-6">
                      <label for="CLAUSE" class="col-form-label">CLAUSE</label>
                      <input type="text" class="form-control" id="CLAUSE" name="CLAUSE" >
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="INCIDENT DETAIL" class="col-form-label">INCIDENT DETAIL</label>
                    <table class="table-fixed table-responsive no-padding" style="width: 80%;" id="tabel_detail">
                      <tr>
                        <th>Component</th>
                        <th>Time</th>
                        <th>Analize</th>
                        <th>Standard</th>
                      </tr>
                      
                      <?php for($i=0;$i<5;$i++): ?>
                        <tr>
                          <td>Component</td>
                          <td>Time</td>
                          <td>Analize</td>
                          <td>Standard</td>
                        </tr>
                      <?php endfor; ?>
                    </table>
                  </div>

                  <div class="form-group">
                    <label for="ANALISA" class="col-form-label">ANALISA <i style="color: red;"> ( * Wajib di isi ) </i></label>
                    <textarea class="form-control" id="ANALISA" name="ANALISA" required></textarea>
                  </div>
                  <div class="form-group">
                    <label for="TINDAKAN" class="col-form-label">TINDAKAN <i style="color: red;"> ( * Wajib di isi ) </i></label>
                    <textarea class="form-control" id="TINDAKAN" name="TINDAKAN" required></textarea>
                  </div>
                  <div class="form-group">
                    <label for="PENCEGAHAN" class="col-form-label">PENCEGAHAN <i style="color: red;"> ( * Wajib di isi ) </i></label>
                    <textarea class="form-control" id="PENCEGAHAN" name="PENCEGAHAN" required></textarea>
                  </div>

                  <div class="form-group">
                    <label for="VERIFIKASI" class="col-form-label">VERIFIKASI <i style="color: red;"> ( * Wajib di isi ) </i></label>
                    <textarea class="form-control" id="VERIFIKASI" name="VERIFIKASI" required></textarea>
                  </div>
                  
                  
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="btn_approve" name="tombol_reject" value="approve">Approve</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                <button type="submit" class="btn btn-danger" id="btn_reject" name="tombol_reject" value="reject">Reject</button>
              </div>
            </form>
          </div>
        </div>
      </div>


    </section>
    <!-- /.content -->
	
	
<!-- msg confirm -->
<?php if($notice->error): ?>
	<a  id="a-notice-error"
		class="notice-error"
		style="display:none";
		href="#"
		data-title="Something Error"
		data-text="<?php echo $notice->error; ?>"
	></a>

<?php endif; ?>

<?php if($notice->success): ?>
	  <a  id="a-notice-success"
		class="notice-success"
		style="display:none";
		href="#"
		data-title="Done!"
		data-text="<?php echo $notice->success; ?>"
	></a>            
<?php endif; ?>
<!-- eof msg confirm -->

<!-- css -->
<style type="text/css">
  .btEdit { margin-right:5px; }
</style>
  
<!-- DataTables css -->
<link href="<?php echo base_url("plugins/datatables/datatables.net-bs/css/dataTables.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/css/buttons.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/css/responsive.bootstrap.min.css");?>" rel="stylesheet">
<link href="<?php echo base_url("plugins/datatables/datatables.net-scroller-bs/css/scroller.bootstrap.min.css");?>" rel="stylesheet">
<!-- DataTables js -->
<script src="<?php echo base_url("plugins/datatables/datatables.net/js/jquery.dataTables.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/dataTables.buttons.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons-bs/js/buttons.bootstrap.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.h5.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-buttons/js/buttons.print.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-keytable/js/dataTables.keyTable.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive/js/dataTables.responsive.min.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-responsive-bs/js/responsive.bootstrap.js");?>"/></script>
<script src="<?php echo base_url("plugins/datatables/datatables.net-scroller/js/dataTables.scroller.min.js");?>"/></script>
<script src="<?php echo base_url("js/jquery.confirm.js"); ?>" ></script>
<script>


  $(document).ready(function(){
    // coba();
    $(".select2").select2(); 
    
    $("#checkAllStatus").click(function(){
      if($("#checkAllStatus").is(':checked') ){
          $("#status > option").prop("selected","selected");
          $("#status").trigger("change");
      }else{
          $("#status > option").removeAttr("selected");
          $("#status").trigger("change");
      }
    }); 

    $(".delete").confirm({ 
      confirmButton: "Remove",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });

    $(".notice-error").confirm({ 
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-danger"
    });
    
    $(".notice-success").confirm({ 
      confirm: function(button) { /* Nothing */ },
      confirmButton: "OK",
      cancelButton: "Cancel",
      confirmButtonClass: "btn-success"
    });	
    
    <?php if($notice->error): ?>
    $("#a-notice-error").click();
    <?php endif; ?>
    
    <?php if($notice->success): ?>
    $("#a-notice-success").click();
    <?php endif; ?>
    
    /** DataTables Init **/
    // alert($('#pageInfo').val());

    var table = $("#dt_tables").DataTable({
        language: {
          searchPlaceholder: ""
        },
        "paging": true,
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "dom":  "<'row'<'col-sm-6 text-left'B><'col-sm-6 text-right'f>>" +
                "<'row'<'col-sm-12'rt>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        "buttons": [
        {
          extend: "pageLength",
          className: "btn-sm bt-separ"
        },
        {
          text: "<i class='fa fa-refresh'></i> Reload",
          className: "btn-sm",
            action: function(){
              dtReload(table);
            }
          }
        ],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
        "url" : '<?php echo site_url("incident/get_list2");?>',
        "type": 'post',
        "data": function(data){ 
            // console.log(data);
            data.j_incident = $("#status").val();
            data.id_company = $("#id_company").val();
          }
        },
        "columnDefs": [{
          "targets": 0,
          "visible": false,
          "searchable": false
          },
          { responsivePriority: 1, targets: -1 },
          {
          "render" : function(data){
            // console.log(data);
            var btn = '<button title="Edit" class="btEdit btn btn-warning btn-xs" type="button">View Incident Detail</button>';
            if (data[8]=='Y') {
              btn += '<button title="Export" class="btExport btn btn-primary btn-xs" type="button" >Export</button>';
            }else{
              btn += '<button title="Export" class="btExport btn btn-primary btn-xs" type="button" disabled>Export</button>';
            }
            return btn;
            // console.log(data[6]);
          },
          "targets": -1,
          "data": null
          }
        ],

        initComplete: function () {
          setTimeout( function () {
            // alert(infopage);
            if($('#pageInfo').val() != ''){
              infopage = parseInt($('#pageInfo').val());
            }else{
              infopage = parseInt(0);
            }
            table.page(infopage).draw(false);
          }, 10 );
        }
      })

    $('#dt_tables').on( 'page.dt', function () {
      var info = table.page.info();
      $('#pageInfo').val( info.page);
    } );

      /** DataTable Ajax Reload **/
      function dtReload(table,time) {
        var time = (isNaN(time)) ? 100:time;
        setTimeout(function(){ table.search('').draw(); }, time);
      }

      $("#btnFilter").click(function(){
        $('#dt_tables').DataTable().ajax.reload();
        // console.log($('#status').val());

        
      });

      //DataTable search on enter
      $('#dt_tables_filter input').unbind();
      $('#dt_tables_filter input').bind('keyup', function(e) {
        var val = this.value;
        var len = val.length;

        if(len==0) table.search('').draw();
        if(e.keyCode == 13) {
          table.search(this.value).draw();
        }
      });

      /** btEdit Click **/
      $(document).on('click',".btEdit",function () {
        linkinfopage = $('#pageInfo').val();
        ID_COMPANY = $('#id_company').val();
          // alert(linkinfopage);
          var data = table.row($(this).parents('tr')).data();
          // ------------------ start izza 07.04.2021 ------------------
          // var url = '<?php echo site_url("incident/solve/");?>' + data[0] +'/'+ID_COMPANY+'/'+linkinfopage;
          // window.open(url,'_blank');
          var id_incident = data[0];
          call_get_dialog(id_incident);
          
          // ------------------ end izza 07.04.2021 ------------------
          
      });    
      /** btExport Click **/
      $(document).on('click',".btExport",function () {
        var data = table.row($(this).parents('tr')).data();
        var url = '<?php echo site_url("incident/export/");?>' + data[0];
        window.location.target = '_blank';
        window.location.href = url;
      });  
  });
  function call_get_dialog(id_incident){
    url = '<?php echo base_url("incident/get_ncqr_detail");?>/'+id_incident;
    // kolom = ["UNIT_KERJA", "ACUAN", "KLAUSUL", "ANALISA", "TINDAKAN", "PENCEGAHAN", "VERIFIKASI", "AREA", "DATE_INCIDENT"];
    var field = ["ID_ACTION", "DATE_INCIDENT", "UK_NAMA", "AREA", "REFERENCE", "CLAUSE", "ANALISA", "TINDAKAN", "PENCEGAHAN", "VERIFIKASI", "UK_KODE", "KD_AREA"]; // ini yg dikirimkan
    var kolom = ["ID_ACTION", "DATE_INCIDENT", "UNIT_KERJA", "AREA", "ACUAN", "KLAUSUL", "ANALISA", "TINDAKAN", "PENCEGAHAN", "VERIFIKASI"]; // ini nama kolom yg diterima

    var kolom_readonly1 = ["DATE_INCIDENT", "AREA"]; // IN PROGRESS
    var kolom_readonly2 = ["DATE_INCIDENT", "AREA"]; // NORMAL BY SYSTEM
    // var kolom_readonly2 = ["DATE_INCIDENT", "UK_NAMA", "AREA", "REFERENCE", "CLAUSE", "ANALISA", "TINDAKAN", "PENCEGAHAN"]; // NORMAL BY SYSTEM / .....
    var kolom_readonly3 = ["DATE_INCIDENT", "UK_NAMA", "AREA", "REFERENCE", "CLAUSE", "ANALISA", "TINDAKAN", "PENCEGAHAN"]; // RESOLVED
    var read = [];
    read.push([],kolom_readonly1,kolom_readonly2,kolom_readonly3);
    
    $('#ID_INCIDENT').val(id_incident);

    $.ajax({
      url: url, type: 'GET', dataType: 'json',
      success: function (result) {
        var incident = result.incident;
        kolom_baca = read[incident['ID_NCQR_STATUS']];
        // console.log(incident['TANGGAL']);

        if(incident["CLOSED_STATUS"] == 'Y'){
          kolom_baca.push("VERIFIKASI");
          $("#btn_approve").css('display','none');
          $("#btn_reject").css('display','none');
        } else{
          $("#btn_approve").css('display','');
          $("#btn_reject").css('display','');
        }
        var receiver = result.receiver;
        detail = result.detail;
        $view = '';
        $view += '<tr>';
          $view += '<th>Component</th>';
          $view += '<th>Time</th>';
          $view += '<th>Analize</th>';
          $view += '<th>Standard</th>';
        $view += '</tr>';
        detail.forEach(element => {
          $view += '<tr>';
            $view += '<td>'+element["NM_COMPONENT"]+'('+element["KD_COMPONENT"]+')</td>';
            $view += '<td>'+element["JAM_ANALISA"]+'</td>';
            $view += '<td>'+element["ANALISA"]+'</td>';
            $view += '<td>'+element["NILAI_STANDARD_MIN"]+' - '+element["NILAI_STANDARD_MAX"]+'</td>';
          $view += '</tr>';
        });
        document.getElementById('tabel_detail').innerHTML = $view;
        // if (receiver != undefined || receiver.length > 0) {
        $('#ID_NCQR_STATUS').val(incident['ID_NCQR_STATUS']);
        if (receiver) {
          $i = 0;
          kolom.forEach(element => {
            $('#'+field[$i]).val(receiver[element]);
            if(kolom_baca.includes(field[$i])){
              $('#'+field[$i]).prop('readonly', true);
            }else{
              $('#'+field[$i]).prop('readonly', false);
            }
            $i++;
          });
          
        } else{
          $i = 0;
          kolom.forEach(element => {
            $('#'+field[$i]).val('');
            if(kolom_baca.includes(field[$i])){
              $('#'+field[$i]).prop('readonly', true);
            }else{
              $('#'+field[$i]).prop('readonly', false);
            }
            $i++;
          });
          $('#AREA').val(incident['NM_AREA']);
          $('#REFERENCE').val('ISO/IEC 17025:205');
          $('#CLAUSE').val('5.9 : 4.11');
        }
        $('#DATE_INCIDENT').val(incident['TANGGAL']); // izza 20.08.21

        $('#exampleModal').modal('show');
      },
      error: function(){console.log('error');}
    });
  }


</script>
