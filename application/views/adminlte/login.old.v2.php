<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quality Management Online</title>
  <!-- Tell the browser to be responsive to screen width -->
  
	<link type="text/css" rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
<style>
body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-color: #B0BEC5;
    background-repeat: no-repeat
}

.card0 {
    box-shadow: 0px 4px 8px 0px #757575;
    border-radius: 0px
}

.card2 {
    margin: 0px 40px
}

.logo {
    width: 20%;
   
    margin-top: 20px;
    margin-left: 35px
}

.logo2 {
    width: 10%;
    
    margin-right: 15px
}

.image {
	border-radius: 5px;
    width: 550px;
    height: 310px 
}

.border-line {
    border-right: 1px solid #EEEEEE
}

.facebook {
    background-color: #3b5998;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.twitter {
    background-color: #1DA1F2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.linkedin {
    background-color: #2867B2;
    color: #fff;
    font-size: 18px;
    padding-top: 5px;
    border-radius: 50%;
    width: 35px;
    height: 35px;
    cursor: pointer
}

.line {
    height: 1px;
    width: 45%;
    background-color: #E0E0E0;
    margin-top: 10px
}

.or {
    width: 10%;
    font-weight: bold;
	font-size: 14px;
}

.text-sm {
    font-size: 14px !important
}

::placeholder {
    color: #BDBDBD;
    opacity: 1;
    font-weight: 300
}

:-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

::-ms-input-placeholder {
    color: #BDBDBD;
    font-weight: 300
}

input,
textarea {
    padding: 10px 12px 10px 12px;
    border: 1px solid lightgrey;
    border-radius: 2px;
    margin-bottom: 5px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    color: #2C3E50;
    font-size: 14px;
    letter-spacing: 1px
}

input:focus,
textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #304FFE;
    outline-width: 0
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}

a {
    color: inherit;
    cursor: pointer
}

.btn-blue {
    background-color: #d50000;
    width: 150px;
    color: #fff;
    border-radius: 2px
}

.btn-blue:hover {
    background-color: #E2E8FE;
    cursor: pointer
}

.bg-blue {
    color: #fff;
    background-color: #d50000;
	text-align: center;
}

@media screen and (max-width: 991px) {
    .logo {
        margin-left: 1em;
        align-content: center;
        display: block;
  margin-left: auto;
  margin-right: auto;
  width: 40%;
    }

    .image {
        display: none;
    }

    .border-line {
        border-right: none
    }

    .card2 {
        border-top: 1px solid #EEEEEE !important;
        margin: 0px 15px
    }
}

.login-box {
  width: 500px;
}
.login-error {
  display: block;
  padding: 10px;
  border: 1px solid white;
  color: white;
}
</style>

</head>
<body >
<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-3">
                       
                    <div class="row"> 
                        <img src="<?php echo base_url("images/Logo Color.png") ?>" class="logo"> 
                        
                    </div>
                    
                    <div class="row px-3 justify-content-center mt-1 mb-1 border-line"> <img src="https://image.freepik.com/free-vector/chemistry-laboratory-concept_74855-6997.jpg" class="image"> 
                     
                    
                    </div>
                    <div class="row px-3 justify-content-center mt-1 mb-1 border-line">
                     <img src="<?php echo base_url("images/Semenindonesia.png") ?>" class="logo2">
                     <img src="<?php echo base_url("images/LogoSemenIndonesia.png") ?>" style=" width: 27%; height: 27%; margin-top:0.7em;">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                    <div class="row mb-4 px-3">
                        <h5 class="mb-0 mr-4 mt-2">Sign in:</h5>
                       
                    </div>
                    <div class="row px-3 mb-4">
                        <div class="line"></div> <small class="or text-center"><span style="color:#d50000; font-size: 25px;" class="fa fa-user-circle-o"></span></small>
                        <div class="line"></div>
                    </div>
					<?php if($notice->error): ?>
                    <p class="login-error"><?php echo $notice->error; ?></p>
                    <?php endif; ?>
                    <form action="<?php echo site_url("login/verification") ?>" method="post">
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">Username</h6>
                        </label> <input class="mb-4" type="text" name="USERNAME" placeholder="Enter a valid username"> </div>
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">Password</h6>
                        </label> <input type="password" name="PASSWORD" placeholder="Enter password"> </div>
                    <div class="row px-3 mb-4">
                       
                    </div>
                    <div class="row mb-3 px-3"> <button type="submit" class="btn btn-blue text-center">Login</button> </div>
					</form>
					
                    <div class="row mb-2 px-3">
					
					<small class="font-weight-bold"><a href="<?php echo site_url("Full_behavior/Behavior") ?>" class="text-danger "> Coating Behavior</a><br>
					Don't have an account ? <a href="<?php echo site_url("login/register") ?>" class="text-danger "> Register a new membership</a></small> </div>
                </div>
            </div>
        </div>
        <div class="bg-blue py-2">
            <div class="row px-3" > <small class="ml-3 ml-sm-12 mb-1" >Copyright &copy; 2018 PT Semen Indonesia. All rights reserved.</small>
                
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>
</html>