<?php

require_once __DIR__ . "/vendor/autoload.php";

// Config
$client = new \nusoap_client('http://10.15.5.242/tele/Telegram.php?wsdl', 'wsdl');
$client->soap_defencoding = 'UTF-8';
$client->decode_utf8 = FALSE;

$action = 'Telegram.sending_massage';
$data = [
    'To' => '+6283831718319',
    'parse_mode' => 'Markdown',
    'Message' => rawurlencode('*halo ini*
ini baris baru, 
*huruf tebal*
_huruf miring_
__huruf apa__
?
üüüü
⬇️⬇️
help: https://duck.com/?q=url+encoding
'),
    // 'To' => null,
    // 'Message' => 'halohalo dari telegram_wsdl',
//    'Message' =>  htmlentities("
//NON CONFIRMITY QUALITY REPORT
//
//SO3 OUT OF STANDARD (MINOR 1)
//
//COMPANY: SEMEN TONASA
//PLANT:  TONASA V
//AREA: FINISH MILL 5.52
//PRODUCT: POZZOLAND PORTLAND CEMENT
//
//Component: SO3
//Time: 17-12-2020 04:00
//Analize: 3.1
//Standard: 1.4 - 3
//
//Component: SO3
//Time: 17-12-2020 05:00
//Analize: 3.2
//Standard: 1.4 - 3
//
//Component: SO3
//Time: 17-12-2020 06:00
//Analize: 3.3
//Standard: 1.4 - 3
//
//
//
//Click link below to Solve this NCQR
//
//http://10.15.2.130/DEV/qmonline/incident/solve/33769
//")
];

// Calls
$result = $client->call($action, $data);

/*
if ($client->fault) {
	echo '<h2>Fault</h2><pre>'; print_r($result); echo '</pre>';
} else {
	$err = $client->getError();

    if ($err) {
        echo '<h2>Error</h2><pre>' . $err . '</pre>';
    } else {
        echo '<h2>Result</h2><pre>'; print_r($result); echo '</pre>';
    }
}
*/

echo '<h2>Request</h2>' . htmlspecialchars($client->request, ENT_QUOTES) . '';
echo '<h2>Response</h2>' . $client->response . '';
echo '<h2>Debug</h2>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '';
echo $result;
