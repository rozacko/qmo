<?php defined('BASEPATH') OR exit('No direct script access allowed.');

$config['default'] = [
  'api_key' => '',
  'username' => '',
  'webhook_token' => '',
  'webhook_url' => '',
  'commands_paths' => [],
];
