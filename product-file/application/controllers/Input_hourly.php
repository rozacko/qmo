<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_hourly extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_area");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("c_parameter");
		$this->load->model("c_product");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("C_range_component");
	}

	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_hourly", $data);
	}

	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY, $this->USER->ID_PLANT);
		to_json($plant);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
		to_json($area);
	}

	private function get_component($id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'H');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->KD_COMPONENT);
		}

		//Tambahkan Remark
		if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';

		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_hourly
		$colHeader 	= array();
		$data 		= array();
		$t_prod 	= $this->t_production_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-2;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_hourly->get_by_id($row->ID_PRODUCTION_HOURLY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

	public function load_cement(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_hourly
		$colHeader 	= array();
		$data 		= array();
		if ($form->ID_GROUPAREA==4) {
			$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		}else{
			$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' AND ID_PRODUCT='".$form->ID_PRODUCT."'");
		}
		#echo $this->db->last_query();die();
		$colHeader 	= $this->get_component($form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-2;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_cement_hourly->get_by_id($row->ID_CEMENT_HOURLY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) {
						$data[$key][] = "";
					}
				}

				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		to_json($result);
	}

	public function save_table(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;

		//T_PRODUCTION_HOURLY (1): ID_PRODUCTION_HOURLY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_ENTRY,JAM_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_HOURLY (M): ID_PRODUCTION_HOURLY, ID_COMPONENT, NILAI

		//read line by line
		$jam_auto = 1;
		$ct_row   = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if ($mati == $t_kolom) {
				$tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			$ID_PRODUCTION_HOURLY = $this->t_production_hourly->get_id($tdata[ID_AREA],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			#echo $this->db->last_query() . "\n";
			if(!$ID_PRODUCTION_HOURLY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_PRODUCTION_HOURLY = $this->t_production_hourly->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_production_hourly->update($tdata,$ID_PRODUCTION_HOURLY);
				//echo $this->db->last_query() . "\n";
			}

			$jam_auto++;

			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_HOURLY'] 	= $ID_PRODUCTION_HOURLY;
				$ddata['ID_COMPONENT']					= $comp[$x];
				$ddata['NILAI']									= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']							= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_production_hourly->d_exists($ddata)){
						$this->t_production_hourly->d_insert($ddata);
					}
					else{
						$this->t_production_hourly->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);
				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_production_hourly->d_exists($ddata)){
							$this->t_production_hourly->d_insert($ddata);
						}
						else{
							$this->t_production_hourly->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}
				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}

				if(!$this->t_production_hourly->d_exists($ddata)){
					$this->t_production_hourly->d_insert($ddata);
				}
				else{
					$this->t_production_hourly->d_update($ddata);
				}
			}

			//exit;
			$ct_row++;
		}
		to_json(array("result" => 'ok'));
	}

	public function save_table_cement(){
		$post = $this->input->post();

		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];

		foreach($form as $r){
			$tmp[$r[name]] = $r[value];
		}

		$form = (object)$tmp;

		//T_PRODUCTION_HOURLY (1): ID_CEMENT_HOURLY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,JAM_DATA,DATE_ENTRY,JAM_ENTRY,DATE_ENTRY,JAM_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_HOURLY (M): ID_CEMENT_HOURLY, ID_COMPONENT, NILAI

		//read line by line
		$jam_auto = 1;
		$ct_row	  = 0;
		foreach($data as $y => $row){ //y index

			//sub index
			$i_jam			= 0;
			$t_kolom		= count($row);
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);

			#if(!$row[$i_jam]) continue; //break null data

			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_PRODUCT']		= ($form->ID_GROUPAREA==4) ? '':$form->ID_PRODUCT;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			$tdata['DATE_DATA']			= $form->TANGGAL; # dd/mm/yyyy
			$tdata['JAM_DATA']			= $jam_auto;
			$status_mesin 				= strtoupper($row[$i_mesin_status]);

			#var_dump($tdata);
			#save
			//cek dulu
			$exists = null;

			/* If Empty row, Machine stat = off */
			$mati = 0;
			for ($i=0; $i < $t_kolom; $i++) {
				if ($this->str_clean($row[$i])=='') {
					$mati += 1;
				}else{
					$mati = 0;
				}
			}

			if ($mati == $t_kolom) {
				$tdata['ID_MESIN_STATUS'] = 3;
				$status_mesin = 'OFF';
			}

			//var_dump($tdata);
			//echo $tdata[ID_AREA]."\n".$tdata[ID_PRODUCT]."\n".$tdata[DATE_DATA]."\n".$tdata[JAM_DATA]."\n\n";
			$ID_CEMENT_HOURLY = $this->t_cement_hourly->get_id($tdata[ID_AREA],$tdata[ID_PRODUCT],$tdata[DATE_DATA],$tdata[JAM_DATA]);
			#echo $this->db->last_query() . "\n";
			//var_dump($ID_CEMENT_HOURLY);
			//echo $this->db->last_query();die();
			if(!$ID_CEMENT_HOURLY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_CEMENT_HOURLY = $this->t_cement_hourly->insert($tdata);
			}
			else{
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['JAM_ENTRY']  = date("H");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_cement_hourly->update($tdata,$ID_CEMENT_HOURLY);
			}

			$jam_auto++;


			//D
			for($x=0;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_CEMENT_HOURLY'] 	= $ID_CEMENT_HOURLY;
				$ddata['ID_COMPONENT']		= $comp[$x];
				$ddata['NILAI']				= $this->str_clean($row[$x]);
				$ddata['NO_FIELD']			= "$x";

				/* Check Mesin Status, Data NULL if Status OFF */
				if ($status_mesin=='OFF'){
					$ddata['NILAI']	= '';
					if(!$this->t_cement_hourly->d_exists($ddata)){
						$this->t_cement_hourly->d_insert($ddata);
					}
					else{
						$this->t_cement_hourly->d_update($ddata);
					}
					continue;
				}

				/* Check Global Range */
				$range = $this->C_range_component->get_id($ddata['ID_COMPONENT']);

				if ($range) {
					$range = $range[0];

					if ($ddata['NILAI']=='') {
						if(!$this->t_cement_hourly->d_exists($ddata)){
							$this->t_cement_hourly->d_insert($ddata);
						}
						else{
							$this->t_cement_hourly->d_update($ddata);
						}
						continue;
					}

					if ($ddata['NILAI'] < (float) $range->V_MIN || $ddata['NILAI'] > (float) $range->V_MAX) {
						$msg['result'] 	= 'nok';
						$msg['msg'] 	= "<b>". trim($range->KD_COMPONENT) . "</b> Out of Range";
						$msg['col']		= $x;
						$msg['row']		= $ct_row;
						to_json($msg);
						continue;
					}
				}else{
					to_json(array("result" => 'nok', "msg" => 'Please Configure Global Component Range First!'));
					continue;
				}

				if(!$this->t_cement_hourly->d_exists($ddata)){
					#echo $this->db->last_query() . ";\n";
					$this->t_cement_hourly->d_insert($ddata);
				}
				else{
					$this->t_cement_hourly->d_update($ddata);
				}

				#echo $this->db->last_query() . ";\n\n";
			}

			#exit;
			$ct_row++;
		}

		to_json(array("result" => 'ok'));
	}

	private function str_clean($chr=''){
		$str ='([^.0-9-]+)';
		return preg_replace($str, '', $chr);
	}

	public function tes($value=''){
		var_dump($this->str_clean('+-'));
	}

	public function preview_boxplot(){
		$post = $this->input->post();
		$form = $post['formData'];
		$comp = json_decode($post['comp']);
		$data = $post['data'];
		$g_area = ($post['g_area']=="FM") ? 2:2;

		foreach($data as $key => $subdata){
			foreach ($subdata as $subkey => $subval) {
				$trace[$subkey][$key] = $subval;
			}
		}

		for ($i=0; $i < (count($trace)-$g_area); $i++) {
			$color = getFixColor($i);
			$nilai = array_filter($trace[$i], 'is_numeric');

			if(empty($nilai)) {
				continue;
			}

			/* Nilai tambahan */
			$box['min_value']	= @min($nilai);
			$box['max_value']	= @max($nilai);
			$box['avg_value']	= @round(@array_sum($nilai) / @count($nilai),2);
			$box['dev_value']	= @round(@$this->standard_deviation($nilai),2);

			/* Plotly */
			$box['name'] 		= $comp[$i]->title;
			$box['marker'] 		= array('color'=>"rgba($color,1.0)");
			$box['boxmean']		= TRUE;
			$box['y'] 			= $trace[$i];
			$box['line'] 		= array('width' => "1.5");
			$box['type'] 		= "box";
			$box['boxpoints'] 	= false;

			$plot['data'][] = $box;
		}

		$plot['layout'] = array(
			"title" => "",
		    "paper_bgcolor" => "#F5F6F9",
		    "plot_bgcolor" => "#F5F6F9",
		    "xaxis1" => array(
		    	"tickfont" => array(
		    		"color" => "#4D5663",
		    		"size" => 8
		    	),
		    	"gridcolor" => "#E1E5ED",
		    	"titlefont" => array(
		    		"color" => "#4D5663"
		    	),
		    	"zerolinecolor" => "#E1E5ED",
      			"title" => "Component"
		    ),
		    "legend" => array(
		    	"bgcolor" => "#F5F6F9",
		    	"font" => array(
		    		"color" => "#4D5663",
		    		"size" => 10
		    	)
		    )
		);
		to_json($plot);
	}

	private function standard_deviation($aValues, $bSample = false){
		$aValues   = array_filter($aValues, 'is_numeric');
	    $fMean     = array_sum($aValues) / count($aValues);
	    $fVariance = 0.0;
	    foreach ($aValues as $i)
	    {
	        $fVariance += pow($i - $fMean, 2);
	    }
	    $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
	    return (float) sqrt($fVariance);
	}
}

/* End of file Input_hourly.php */
/* Location: ./application/controllers/Input_hourly.php */
?>
