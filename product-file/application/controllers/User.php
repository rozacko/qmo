<?php

class User extends QMUser {
	
	public $list_company = array();
	public $list_plant = array();
	public $list_user = array();
	public $list_usergroup = array();
	
	public $ID_COMPANY;
	public $ID_USER;
	public $data_user;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_user");
		$this->load->model("m_usergroup");
		$this->load->model("employee");
	}
	
	public function index(){
	//	$this->list_company   = $this->m_company->datalist();
	//	$this->list_plant 	  = $this->m_plant->datalist($this->ID_COMPANY); #echo $this->m_plant->get_sql();
	//	$this->list_usergroup = $this->m_usergroup->datalist();
		$this->list_user 	  = $this->m_user->datalist(); #echo $this->m_plant->get_sql();
		$this->template->adminlte("v_user");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}	

	public function by_plant($ID_USER=NULL){
		$this->ID_USER   = $ID_USER;
		$this->ID_COMPANY = $this->m_plant->data("ID_USER='$ID_USER'")->ID_COMPANY;
		$this->index();
	}
	
	public function add(){
		$this->ID_COMPANY 	= $ID_COMPANY;
		$this->ID_USER   	= $ID_USER;
		$this->list_company = $this->m_company->list_company();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->template->adminlte("v_user_add");
	}
	
	public function create(){
		
	//	var_dump($this->input->post()); exit;
		//check user exists
		$uname = array('USERNAME'=> trim(strtolower($this->input->post("USERNAME"))));
		$cek = $this->m_user->data($uname);
		if($cek){
			$this->notice->error("USERNAME ALREADY EXISTS");
			redirect("user");
		}
		else{
			$this->m_user->insert($this->input->post()); #die($this->m_user->get_sql());
			if($this->m_user->error()){
				$this->notice->error($this->m_user->error());
				redirect("user/add");
			}
			else{
				$this->notice->success("Users Data Saved.");
				redirect("user");
			}
		}
	}
	
	public function edit($ID_USER){
		$this->data_user 	= $this->m_user->get_data_by_id($ID_USER);
		$this->ID_COMPANY 	= $ID_COMPANY;
		$this->ID_USER   	= $ID_USER;
		$this->list_company = $this->m_company->list_company();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->template->adminlte("v_user_edit");
	}
	
	public function update($ID_USER){
		if($this->m_user->data_except_id(array('USERNAME'=>$this->input->post("USERNAME")),$ID_USER)){
			$this->notice->error("USERNAME ALREADY EXISTS");
		}
		else{
			$data = $this->input->post();
			$data['ID_COMPANY'] = $this->input->post("ID_COMPANY");
			$data['ID_PLANT'] = $this->input->post("ID_PLANT");
			$data['ID_AREA'] = $this->input->post("ID_AREA");
			$this->m_user->update($data,$ID_USER); #die($this->m_user->get_sql());
			if($this->m_user->error()){
				$this->notice->error($this->m_user->error());
				redirect("user/edit/".$ID_USER);
			}
			else{
				$this->notice->success("Users Data Updated.");
				redirect("user");
			}
		}
	}
	
	public function delete($ID_USER){
		$this->m_user->delete($ID_USER);
		if($this->m_user->error()){
			$this->notice->error($this->m_user->error());
		}
		else{
			$this->notice->success("Users Data Removed.");
		}
		redirect("user");
	}

	public function get_username(){
		$username = $this->input->post('username');
		$get = $this->employee->get_username(strtoupper($username));
		to_json($get);
	}

	public function get_list(){
		$list = $this->m_user->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USER;
			$row[] = $no;
			$row[] = $column->FULLNAME;
			$row[] = $column->USERNAME;
			$row[] = $column->NM_USERGROUP;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_user->count_all(),
            "recordsFiltered" => $this->m_user->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
