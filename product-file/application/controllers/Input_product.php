<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_product extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("c_product");
		$this->load->model("m_product");
		$this->load->model("t_production");
		$this->load->model("d_production");
	}
	
	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_input_product", $data);
	}

	public function ajax_get_area_cement($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);# echo $this->m_area->get_sql();
		to_json($area);
	}

	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA, TRUE);#echo $this->m_area->get_sql();
		to_json($area);
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_product($id_group_area='', $id_area='', $id_plant='', $id_company=''){
		//HandsonTable Column Header
		$prod = $this->get_product_nm($id_company, $id_plant, $id_area);
		//echo $this->db->last_query();die();
		foreach ($prod as $val):
			$rows[] = $val;
		endforeach;
		
		$header[]['title'] = 'PRODUCT TYPE';
		$header[]['title'] = 'PRODUCTION';
		$data['colHeader'] = $header;
		$data['rows'] 	   = ($id_group_area==1)?count($rows):1;
		$data['product']   = ($id_group_area==1)?$rows:'-';
		to_json($data);
	}

	public function get_product_nm($id_company='', $id_plant='', $id_area=''){
		$data = array();
		$param = $this->c_product->datalist($id_area, $id_plant, $id_company);

		foreach ($param as $col):
			$data[] = $col->KD_PRODUCT;
		endforeach;
		
		return $data;
	}

	public function get_product_id($id_company='', $id_plant='', $id_area=''){
		$data = array();
		$param = $this->c_product->datalist($id_area, $id_plant, $id_company);

		foreach ($param as $col):
			$data[] = $col->ID_PRODUCT;
		endforeach;
		
		return $data;
	}

	public function load_table(){
		$data = array();
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];

		//Load from T_production
		$data = array();
		$t_prod = $this->t_production->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."' AND ID_AREA='" .$form->ID_AREA."' ");
		//echo $this->db->last_query();die();
		
		if (empty($t_prod)) {
			$prod = $this->get_product_nm($form->ID_COMPANY, $form->ID_PLANT, $form->ID_AREA);
			if(!$prod) $prod[0] = "-";
			foreach ($prod as $key => $val):
				$data[] = array(($form->ID_GROUPAREA == 4)?'CLINKER PRODUCTION':$val, NULL);
			endforeach;
		}else{
			$prod = $this->get_product_nm($form->ID_COMPANY, $form->ID_PLANT, $form->ID_AREA); 
			//if($form->ID_GROUPAREA == 4) $prod = "(NO TYPE)";
			if(!$prod) $prod[0] = "-";
			$id_tprod = $t_prod[0]->ID_PRODUCTION;
			$id = 1;
			foreach ($prod as $val):
				$nilai  = $this->d_production->get_nilai($id_tprod, $id);
				$data[] = array(($form->ID_GROUPAREA == 4)?'CLINKER PRODUCTION':$val, $nilai);
				$id++;
			endforeach;
		}

		to_json($data);
	}

	public function save_table(){
		$post  = $this->input->post();
		$form  = $post['formData'];
		$data  = $post['data'];
		$count = count($data);

		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];
			
		//T_PRODUCTION_DAILY (1): ID_PRODUCTION,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION, ID_COMPONENT, NILAI
		$tdata['BULAN']	= $form->BULAN;
		$tdata['TAHUN']	= $form->TAHUN;
		$tdata['ID_AREA']	= $form->ID_AREA;
		$ID_PRODUCTION  = $this->t_production->get_id($tdata['BULAN'],$tdata['TAHUN'],$tdata['ID_AREA']);

		if(!$ID_PRODUCTION){
			$tdata['DATE_ENTRY'] = date("d/m/Y");
			$tdata['USER_ENTRY'] = $this->USER->ID_USER;
			$tdata['ID_AREA']	= $form->ID_AREA;
			$ID_PRODUCTION = $this->t_production->insert($tdata);
		}
		else{
			$tdata['DATE_UPDATE'] = date("d/m/Y");
			$tdata['USER_UPDATE'] = $this->USER->ID_USER;
			$tdata['ID_AREA']	= $form->ID_AREA;
			$this->t_production->update($ID_PRODUCTION,$tdata);
		}
		
		//read line by line
		$ctr = 1;
		foreach($data as $y => $row){ //y index	
			//D
			$ddata = null;
			$ddata['ID_PRODUCTION'] = $ID_PRODUCTION;
			$ddata['ID_AREA']		= $form->ID_AREA;
			$ddata['ID_PRODUCT']	= $this->m_product->get_id_by_kode($row[0]);
			$ddata['NILAI']			= $row[1];
			$ddata['NO_FIELD']		= $ctr;
			#save 
			
			if(!$this->d_production->exists($ddata)){
				$this->d_production->insert($ddata);
				#echo $this->db->last_query() . "\n";
			}
			else{
				$where['ID_PRODUCTION'] = $ID_PRODUCTION;
				$where['ID_PRODUCT']    = $ddata['ID_PRODUCT'];
				#echo $this->db->last_query() . "\n";
				$this->d_production->update_where($where,$ddata);
				#echo $this->db->last_query() . "\n";
			}
			
			$ctr++;
		}
		
		to_json(array("result" => 'ok'));
	}
}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
