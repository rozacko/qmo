<?php

class Usergroup extends QMUser {
	
	public $list_data = array();
	public $data_usergroup;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_usergroup");
	}
	
	public function index(){
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->template->adminlte("v_usergroup");
	}
	
	public function add(){
		$this->template->adminlte("v_usergroup_add");
	}
	
	public function create(){
		$this->m_usergroup->insert($this->input->post());
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
			redirect("usergroup/add");
		}
		else{
			$this->notice->success("Usergroup Data Saved.");
			redirect("usergroup");
		}
	}
	
	public function edit($ID_USERGROUP){
		$this->data_usergroup = $this->m_usergroup->get_data_by_id($ID_USERGROUP);
		$this->template->adminlte("v_usergroup_edit");	
	}
	
	public function update($ID_USERGROUP){
		$data = $this->input->post();
		$data['PERM_WRITE'] = $this->input->post('PERM_WRITE');
		$data['PERM_READ'] = $this->input->post('PERM_READ');
		$this->m_usergroup->update($data,$ID_USERGROUP);
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
			redirect("usergroup/edit/".$ID_USERGROUP);
		}
		else{
			$this->notice->success("Usergroup Data Updated.");
			redirect("usergroup");
		}
	}
	
	public function delete($ID_USERGROUP){
		$this->m_usergroup->delete($ID_USERGROUP);
		if($this->m_usergroup->error()){
			$this->notice->error($this->m_usergroup->error());
		}
		else{
			$this->notice->success("Usergroup Data Removed.");
		}
		redirect("usergroup");
	}

	public function get_list(){
		$list = $this->m_usergroup->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_USERGROUP;
			$row[] = $no;
			$row[] = $column->KD_USERGROUP;
			$row[] = $column->NM_USERGROUP;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_usergroup->count_all(),
            "recordsFiltered" => $this->m_usergroup->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
