<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_table extends QMUser {
	
	public $data;
	public $NM_AREA;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("c_parameter");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_cement_daily");
		$this->load->model("t_cement_hourly");
		$this->load->model("t_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_cement_daily");
		$this->load->model("d_cement_hourly");
		$this->load->model("d_production_daily");
		$this->load->model("d_production_hourly");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("c_product");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_data_table", $data);
	}
	
	private function data_daily(){
		
		$data = $this->input->post();
		
		$json = null;
		
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
		$this->load->model("c_parameter");

		//get th
		$cpar['ID_PLANT'] 		= $data['ID_PLANT'];
		$cpar['DISPLAY']		= 'D';
		$cpar['ID_GROUPAREA'] 	= $this->m_area->get_data_by_id($data['ID_AREA'])->ID_GROUPAREA;
		$nm_grouparea			= $this->m_grouparea->get_data_by_id($cpar['ID_GROUPAREA'])->NM_GROUPAREA;
		$json['th'] 			= $this->c_parameter->data($cpar); #die(var_dump($this->c_parameter->get_sql()));
		//$mod = (trim($nm_grouparea)=="FINISH MILL" || trim($nm_grouparea)=="CLINKER") ? "t_cement_daily":"t_production_daily";
		//var_dump($json['th']);
		
		//get tr
		
		
		$tproduction = (in_array($cpar['ID_GROUPAREA'],array(1,4)))?"t_cement_daily":"t_production_daily";
		
		$month = "'".$data['MONTH']."/".$data['YEAR']."'";
		//$d = $this->{$mod}->table($month,$data['ID_AREA']);
		$d = $this->$tproduction->table($month,$data['ID_AREA']); 
		#echo $this->t_production_daily->get_sql();
		#var_dump($d);
		$n = array();
		foreach($d as $i => $r){
			$n[$r->DATE_DATA][$r->ID_COMPONENT] = $r->NILAI;
		}
		
		foreach($d as $i => $r){
			$n[$r->DATE_DATA]['USER_ENTRY']   = $r->USER_ENTRY;
			$n[$r->DATE_DATA]['USER_UPDATE']  = $r->USER_ENTRY;
			$n[$r->DATE_DATA]['DATE_ENTRY']   = $r->DATE_ENTRY;
			$n[$r->DATE_DATA]['DATE_DATA']    = $r->DATE_DATA;
			$n[$r->DATE_DATA]['MESIN_STATUS'] = $r->NM_MESIN_STATUS;
			$n[$r->DATE_DATA]['MESIN_REMARK'] = $r->MESIN_REMARK;
		}

		$json['tr'] = (object)$n;
		//var_dump($json['tr']);
		return $json;
	}
	
	private function data_hourly(){
		$this->load->model("t_production_hourly");
		
		$data = $this->input->post();
		
		#var_dump($data);
		
		$json = null;
		
		$this->load->model("m_area");
		
		//get th
		$cpar['ID_PLANT'] 		= $data['ID_PLANT'];
		$cpar['ID_GROUPAREA'] 	= $this->m_area->get_data_by_id($data['ID_AREA'])->ID_GROUPAREA;
		$this->load->model("c_parameter");
		$json['th'] = $this->c_parameter->data($cpar);
		
		
		//get tr
		$data['TANGGAL'] = "'".$data['TANGGAL']."'";
		$this->load->model("t_production_daily");
		$d = $this->t_production_hourly->table($data['TANGGAL'],$data['ID_AREA']);
		
		
		$n = array();
		foreach($d as $i => $r){
			$n[$r->JAM_DATA][$r->ID_COMPONENT] = $r->NILAI;
		}
		
		foreach($d as $i => $r){
			$n[$r->JAM_DATA]['MESIN_STATUS'] = $r->NM_MESIN_STATUS;
			$n[$r->JAM_DATA]['MESIN_REMARK'] = $r->MESIN_REMARK;
		}

		$json['tr'] = (object)$n;
		//var_dump($json['tr']);
		return $json;
	}
	
	public function daily(){
		if(in_array($this->input->post("ID_GROUPAREA"),array(1,4))){
			$data = $this->load_cement_daily();
		}
		else{
			$data = $this->load_table_daily();
		}
		echo $data;
	}
	
	public function hourly(){
		if(in_array($this->input->post("ID_GROUPAREA"),array(1,4))){
			$data = $this->load_cement_hourly();
		}
		else{
			$data = $this->load_table_hourly();
		}
		echo $data;
	}
	
	public function export(){
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		if($this->input->post("DISPLAY") == 'daily'){
			$this->export_daily();
		}
		else{
			$this->export_hourly();
		}
	}
	
	private function export_daily(){
		if(in_array($this->input->post("ID_GROUPAREA"),array(1,4))){
			$data = $this->load_cement_daily();
		}
		else{
			$data = $this->load_table_daily();
		}
		$this->data = $data;
		$this->template->nostyle('data_table_daily');
	}
	
	private function export_hourly(){
		if(in_array($this->input->post("ID_GROUPAREA"),array(1,4))){
			$data = $this->load_cement_daily();
		}
		else{
			$data = $this->load_table_hourly();
		}
		$this->data = $data;
		$this->template->nostyle('data_table_hourly');
	}
	
	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}
	
	public function ajax_get_grouparea($ID_COMPANY=NULL,$ID_PLANT=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->grouplist($ID_COMPANY,$ID_PLANT,$this->USER->ID_AREA);
		to_json($area);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$this->load->model("m_area");
		$area= $this->m_area->datalist($ID_COMPANY,$ID_PLANT,$ID_GROUPAREA);#echo $this->m_area->get_sql();
		to_json($area);
	}
	
	
	public function load_table_daily(){
		$this->load->model("m_user");

		$post = $tmp = $this->input->post();
		
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_daily
		$colHeader 	= array();
		$data 		= array();
		$t_prod 	= $this->t_production_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component('D', $form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-6;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_daily->get_by_id($row->ID_PRODUCTION_DAILY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) { 
						$data[$key][] = "";
					}	
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
				$data[$key][] = $row->DATE_ENTRY;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
				$data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE)?$row->DATE_ENTRY:$row->DATE_UPDATE;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		return ret_json($result);
	}
	
	public function load_table_hourly(){
		$this->load->model("m_user");

		$post = $tmp = $this->input->post();
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_hourly
		$colHeader 	= array();
		$data 		= array();
		$t_prod 	= $this->t_production_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		$colHeader 	= $this->get_component('H',$form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-6;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_production_hourly->get_by_id($row->ID_PRODUCTION_HOURLY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) { 
						$data[$key][] = "";
					}	
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
				$data[$key][] = $row->DATE_ENTRY;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
				$data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE)?$row->DATE_ENTRY:$row->DATE_UPDATE;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		return ret_json($result);
	}

	public function load_cement_daily(){
		$this->load->model("m_user");
		$post = $tmp = $this->input->post();
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_daily
		$colHeader 	= array();
		$data 		= array();
		if ($form->ID_GROUPAREA==4) {
			//$t_prod = $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."'");
			$t_prod = $this->t_cement_daily->data_where_prod("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."'");
		}else{
			$prod = ($form->ID_PRODUCT=='ALL') ? "":"AND a.ID_PRODUCT='".$form->ID_PRODUCT."'";
			$t_prod = $this->t_cement_daily->data_where_prod("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
			//$t_prod = $this->t_cement_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->MONTH."/".$form->YEAR."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
		}		
		
		#echo $this->db->last_query();
		
		$colHeader 	= $this->get_component('D',$form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-7;
		
		#print_r($t_prod);
		
		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) { #print_r($row);
				$d_prod = $this->d_cement_daily->get_by_id($row->ID_CEMENT_DAILY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}	
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) { 
						$data[$key][] = "";
					}	
				}
				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
				$data[$key][] = $row->DATE_ENTRY;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
				$data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE)?$row->DATE_ENTRY:$row->DATE_UPDATE;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
				$data[$key][] = $row->KD_PRODUCT;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		return ret_json($result);
	}
	
	public function load_cement_hourly(){
		$this->load->model("m_user");
		$post = $tmp = $this->input->post();

		//Convert array to object
		$form = (object)$tmp;

		//Load from T_cement_hourly
		$colHeader 	= array();
		$data 		= array();
		if ($form->ID_GROUPAREA==4) {
			$t_prod 	= $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
			#$t_prod 	= $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		}else{
			$prod 	= ($form->ID_PRODUCT=='ALL') ? "":"AND a.ID_PRODUCT='".$form->ID_PRODUCT."'";
			$t_prod = $this->t_cement_hourly->data_where_prod("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
			#$t_prod = $this->t_cement_hourly->data_where("TO_CHAR(DATE_DATA, 'DD/MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."' $prod");	
		}
		$colHeader 	= $this->get_component('H',$form->ID_PLANT, $form->ID_GROUPAREA, TRUE);
		$ctHead		= count($colHeader['id_comp'])-7;

		if (!empty($t_prod)) {
			foreach ($t_prod as $key => $row) {
				$d_prod = $this->d_cement_hourly->get_by_id($row->ID_CEMENT_HOURLY);
				foreach ($d_prod as $k => $vl) {
					if (in_array($vl->ID_COMPONENT, $colHeader['id_comp'])) {
						$data[$key][] = ($vl->NILAI=='') ? '': (float) $vl->NILAI;
					}
				}
				if ($ctHead > count($d_prod)) {
					for ($i=0; $i < (($ctHead)-(count($d_prod))); $i++) { 
						$data[$key][] = "";
					}	
				}

				$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
				$data[$key][] = $row->MESIN_REMARK;
				$data[$key][] = $row->DATE_ENTRY;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_ENTRY)->USERNAME;
				$data[$key][] = ($row->USER_UPDATE && !$row->DATE_UPDATE)?$row->DATE_ENTRY:$row->DATE_UPDATE;
				$data[$key][] = $this->m_user->get_data_by_id($row->USER_UPDATE)->USERNAME;
				$data[$key][] = $row->KD_PRODUCT;
			}
		}

		$result['header'] = $colHeader;
		$result['data']   = $data;
		return ret_json($result);
	}
	
	
	public function ajax_get_product($ID_AREA=NULL, $ID_PLANT=NULL, $ID_COMPANY=NULL){
		$product= $this->c_product->datalist($ID_AREA,$ID_PLANT,$ID_COMPANY);
		to_json($product);
	}
	
	private function get_component($display='',$id_plant='',$id_grouparea='', $tipe=FALSE){
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,$display);
		#echo $this->db->last_query();die();
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->KD_COMPONENT);
		}

		//Tambahkan Remark
		if(!empty($tipe)) $header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';
		$header[]['title'] = 'DATE ENTRY';
		$header[]['title'] = 'USER ENTRY';
		$header[]['title'] = 'DATE UPDATE';
		$header[]['title'] = 'USER UPDATE';
		$header[]['title'] = 'TYPE';
		
		
		
		if(!empty($tipe)) $id_comp[] = '_machine_status';
		$id_comp[] = '_remark';
		$id_comp[] = '_date_entry';
		$id_comp[] = '_user_entry';
		$id_comp[] = '_date_update';
		$id_comp[] = '_user_update';
		$id_comp[] = '_type';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		return $data;
	}
	
}
