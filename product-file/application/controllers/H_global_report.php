<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class H_global_report extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
	}
	
	public function index(){
		$this->template->adminlte("v_h_global_report");
	}

}

/* End of file H_global_report.php */
/* Location: ./application/controllers/H_global_report.php */
?>