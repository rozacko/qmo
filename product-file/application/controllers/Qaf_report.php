<?php

class Qaf_report extends QMUser {
	
	public $list_data = array();
	public $list_product = array();
	public $list_qaf = array();
	public $NM_PRODUCT;
	public $list_component = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->model("c_range_qaf");
		$this->load->model("qaf_daily");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_product");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_qaf_report");
	}
	
	public function get_company_list(){
		echo json_encode($this->m_company->list_company());
	}
	
	
	public function generate_company_report(){
		$this->qaf_daily->generate_report($this->input->post(),'company'); #echo $this->qaf_daily->get_sql();
	}
	
	public function get_company_report(){ #var_dump($this->input->post()); 
		$this->NM_COMPANY = $this->m_company->get_data_by_id($this->input->post("ID_COMPANY"))->NM_COMPANY;
		$this->list_qaf = $this->qaf_daily->report_company($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_report_company");
	}
	
	public function generate_report($REPORT='cement'){
		$this->qaf_daily->generate_report($this->input->post(),$REPORT); #echo $this->qaf_daily->get_sql();
	}
	
	public function get_report(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post());# var_dump($this->list_component); # echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_report");
	}
	
	
	public function get_report_cs(){ 
		#var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post(),array(19,20,21)); #var_dump($this->list_component);  echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report_cs($this->input->post());  #echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_cs");
	}
	
	public function get_report_st(){ 
		#var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post(),array(22,23)); #var_dump($this->list_component);  echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report_st($this->input->post());  #echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_st");
	}
	
	public function get_report_clinker(){ 
		#var_dump($this->input->post());
		#$this->output->enable_profiler(TRUE);
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post()); #var_dump($this->list_component);  echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report_clinker($this->input->post());  #echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_clinker");
	}
	
	public function report_produksi(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi($this->input->post()); # echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi");
	}
	
	public function report_produksi_cs(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_cs($this->input->post()); # echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			$r->JML_COMPONENT = 3; //cs3, cs7, cs28
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi_cs");
	}
	
	public function report_produksi_st(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_st($this->input->post()); # echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			$r->JML_COMPONENT = 2; //INIT, FINAL
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi_cs");
	}
	
	public function report_clinker(){ #var_dump($this->input->post());
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_qaf = $this->qaf_daily->qaf_produksi_clinker($this->input->post()); # echo $this->qaf_daily->get_sql();

		foreach($this->list_qaf as $i => $r){
			$this->list_qaf[$i]->QAF_AREA = round((float)$r->QAF/$r->JML_COMPONENT,2);
		}
		
		$this->template->nostyle("v_qaf_produksi_clinker");
	}
	
	
	public function export_report(){
		$this->data = json_encode($this->m_incident->report($this->input->post()));
		$this->template->nostyle("xls_qaf_incident_report");
	}	
	
	public function grafik_qaf(){
		$this->list_company = $this->m_company->list_company_auth();
		$this->template->adminlte("v_grafik_qaf");
	}
	
	public function get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_PRODUCT){
		echo json_encode($this->qaf_daily->get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_PRODUCT)); #echo $this->qaf_daily->get_sql();
	}
	
	/*
	public function compressive_strength(){
		$this->NM_PRODUCT = $this->m_product->get_data_by_id($this->input->post("ID_PRODUCT"))->KD_PRODUCT; 
		$this->NM_AREA = $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->NM_AREA;
		$this->list_component = $this->qaf_daily->component($this->input->post());# var_dump($this->list_component); # echo $this->qaf_daily->get_sql(); exit;
		$this->list_qaf = $this->qaf_daily->report($this->input->post()); # echo $this->qaf_daily->get_sql();
		$this->template->nostyle("v_qaf_cs");
	}
	*/
	
	public function compressive_strength($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_PRODUCT=NULL){
		$ID_GROUPAREA=1; //cement
		switch($sm){		
			case 'get_area':
				echo json_encode($this->qaf_daily->get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_PRODUCT));
				#echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				$this->template->adminlte("v_qaf_cs");
			break;
		}
	}
	
	
	public function setting_time($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL,$ID_PRODUCT=NULL){
		$ID_GROUPAREA=1; //cement
		switch($sm){		
			case 'get_area':
				echo json_encode($this->qaf_daily->get_area_by_group($ID_PLANT,$ID_GROUPAREA,$ID_PRODUCT));
				#echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				$this->template->adminlte("v_qaf_st");
			break;
		}
	}
	
	public function clinker($sm='default',$ID_PLANT=NULL,$ID_AREA=NULL){
		$ID_GROUPAREA=4; //cement
		switch($sm){		
			case 'get_area':
				echo json_encode($this->qaf_daily->get_area_by_group($ID_PLANT,$ID_GROUPAREA));
				#echo $this->qaf_daily->get_sql();
			break;
			
			default:
				$this->list_company = $this->m_company->datalist();
				$this->template->adminlte("v_qaf_clinker");
			break;
		}
	}
	
	public function json_plant_list($ID_COMPANY){
		$this->load->model("m_plant");
		$data = $this->list_plant = $this->m_plant->datalist($ID_COMPANY);
		echo json_encode($data);
	}
	
	public function async_list_product_qaf($ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$this->load->model("c_range_qaf");
		$data = $this->c_range_qaf->list_configured_product_qaf($ID_PLANT,$ID_GROUPAREA); # echo $this->c_range_qaf->get_sql();
		echo json_encode($data);
	}
	
}	

?>
