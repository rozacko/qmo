<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batasan extends QMUser {

	public $LIST;
	public $ID_JENIS_ASPEK;
	public $JENIS_ASPEK;
	public $ID_ASPEK;
	public $ASPEK;
	public $ID_BATASAN;
	public $BATASAN;
	public $KRITERIA;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_aspek');
		$this->load->model('M_kriteria');
		$this->load->model('M_batasan');
	}
	
	public function index(){
		$this->template->adminlte("v_batasan");
	}

	public function ajax_get_aspek($id_jenis_aspek){
		$aspek = $this->M_aspek->get_where(array("ID_JENIS_ASPEK" => $id_jenis_aspek));
		to_json($aspek);
	}

	public function ajax_get_aspek_bobot($id_aspek){
		$bobot = $this->M_aspek->get_by_id($id_aspek);
		to_json($bobot);
	}

	public function ajax_get_kriteria($id_aspek){
		$kriteria = $this->M_kriteria->get_where(array("ID_ASPEK" => $id_aspek));
		to_json($kriteria);
	}

	public function add(){
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->ASPEK = $this->M_aspek->get_list();
		$this->template->adminlte("v_batasan_add");
	}

	public function edit(){
		$this->LIST = $this->M_batasan->get_by_id($this->input->post('ID_BATASAN'));
		$this->KRITERIA = $this->M_kriteria->get_by_id($this->LIST->ID_KRITERIA);
		$this->ID_ASPEK = $this->KRITERIA->ID_ASPEK;
		$this->ID_JENIS_ASPEK = $this->M_aspek->get_by_id($this->ID_ASPEK)->ID_JENIS_ASPEK;
		
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_batasan_edit", $list);
	}

	public function get_list(){
		$list = $this->M_batasan->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_BATASAN;
			$row[] = $no;
			$row[] = $column->JENIS_ASPEK;
			$row[] = $column->ASPEK;
			$row[] = $column->BOBOT;
			$row[] = $column->KRITERIA;
			$row[] = $column->BATASAN;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_batasan->count_all(),
            "recordsFiltered" => $this->M_batasan->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		//$post = array();
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);
		unset($post['ID_ASPEK']);

		$this->M_batasan->insert(array_map('strtoupper', $post));

		if($this->M_batasan->error()){
			$this->notice->error($this->M_batasan->error());
			redirect("batasan/add");
		}
		else{
			$this->notice->success("Batasan Saved.");
			redirect("batasan");
		}
	}

	public function update(){
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);
		unset($post['ID_ASPEK']);

		$this->M_batasan->update($this->input->post('ID_BATASAN'), $post);
		if($this->M_batasan->error()){
			$this->notice->error($this->M_batasan->error());
			redirect("batasan/edit");
		}
		else{
			$this->notice->success("Batasan Updated.");
			redirect("batasan");
		}
	}

	public function remove(){
		$this->M_batasan->delete($this->input->post('ID_BATASAN'));
		if($this->M_batasan->error()){
			$this->notice->error($this->M_batasan->error());
		}
		else{
			$this->notice->success("Batasan Removed.");
		}
		redirect("batasan");
	}

}

/* End of file Batasan.php */
/* Location: ./application/controllers/Batasan.php */
?>