<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_qaf extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("c_product");
		$this->load->model("m_product");
		$this->load->model("t_production");
		$this->load->model("d_production");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_input_qaf", $data);
	}

	public function ajax_get_product(){
		//HandsonTable Column Header
		$prod = $this->get_product_nm($form->ID_COMPANY, $form->ID_PLANT, $form->ID_AREA);
			
		foreach ($prod as $val):
			$rows[] = $val;
		endforeach;

		$header[]['title'] = 'PRODUCT TYPE';
		$header[]['title'] = 'PRODUCTION';
		$data['colHeader'] = $header;
		$data['rows'] 	   = count($rows);
		$data['product']   = $rows;
		to_json($data);
	}

	public function get_product_nm($id_company='', $id_plant='', $id_area=''){
		$data = array();
		$param = $this->c_product->datalist($id_area, $id_plant, $id_company);

		foreach ($param as $col):
			$data[] = $col->NM_PRODUCT;
		endforeach;
		
		return $data;
	}

	public function get_product_id($id_company='', $id_plant='', $id_area=''){
		$data = array();
		$param = $this->c_product->datalist($id_area, $id_plant, $id_company);

		foreach ($param as $col):
			$data[] = $col->ID_PRODUCT;
		endforeach;
		
		return $data;
	}

	public function load_table(){
		$data = array();
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];

		//Load from T_production
		$data = array();
		$t_prod = $this->t_production->get_where("TAHUN='".$form->TAHUN."' AND BULAN='" .$form->BULAN."'");

		if (empty($t_prod)) {
			$prod = $this->get_product_nm($form->ID_COMPANY, $form->ID_PLANT, $form->ID_AREA);

			foreach ($prod as $key => $val):
				$data[] = array($val, NULL);
			endforeach;
		}else{
			$prod = $this->get_product_nm($form->ID_COMPANY, $form->ID_PLANT, $form->ID_AREA);
			$id_tprod = $t_prod[0]->ID_PRODUCTION;
			$id = 1;
			foreach ($prod as $val):
				$nilai  = $this->d_production->get_nilai($id_tprod, $id);
				$data[] = array($val, $nilai);
				$id++;
			endforeach;
		}

		to_json($data);
	}

	public function save_table(){
		$post  = $this->input->post();
		$form  = $post['formData'];
		$data  = $post['data'];
		$count = count($data);

		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		$tgl  = explode("/", $form->TANGGAL);
		$form->BULAN = $tgl[0];
		$form->TAHUN = $tgl[1];
			
		//T_PRODUCTION_DAILY (1): ID_PRODUCTION,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION, ID_COMPONENT, NILAI
		$tdata['BULAN']	= $form->BULAN;
		$tdata['TAHUN']	= $form->TAHUN;
		$ID_PRODUCTION  = $this->t_production->get_id($tdata['BULAN'],$tdata['TAHUN']);

		if(!$ID_PRODUCTION){
			$tdata['DATE_ENTRY'] = date("d/m/Y");
			$tdata['USER_ENTRY'] = $this->USER->ID_USER;
			$ID_PRODUCTION = $this->t_production->insert($tdata);
		}
		else{
			$tdata['DATE_UPDATE'] = date("d/m/Y");
			$tdata['USER_UPDATE'] = $this->USER->ID_USER;
			$this->t_production->update($tdata,$ID_PRODUCTION);
		}
		

		//read line by line
		$ctr = 1;
		foreach($data as $y => $row){ //y index	
			//D
			$ddata = null;
			$ddata['ID_PRODUCTION'] = $ID_PRODUCTION;
			$ddata['ID_AREA']		= $form->ID_AREA;
			$ddata['ID_PRODUCT']	= $this->m_product->get_id_by_name($row[0]);
			$ddata['NILAI']			= $row[1];
			$ddata['NO_FIELD']		= $ctr;
			#save 
			
			if(!$this->d_production->exists($ddata)){
				$this->d_production->insert($ddata);
			}
			else{
				$this->d_production->update($ddata);
			}
			
			$ctr++;
		}
		


	}
}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
