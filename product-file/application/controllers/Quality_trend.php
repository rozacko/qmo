<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quality_trend extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
		$this->load->model("c_product");
		$this->load->model("c_parameter");
		$this->load->model("m_product");
		$this->load->model("m_component");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
		$this->load->model("c_qaf_component");
	}

	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist_auth($this->USER->ID_GROUPAREA);
		$this->list_product   = $this->m_product->datalist();
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_quality_trend", $data);
	}

	public function ajax_get_area_by_company(){
		$opt_c = (NULL==$this->input->post('opt_c'))? 0:$this->input->post('opt_c');
		$opt_g = (NULL==$this->input->post('opt_g'))? 0:$this->input->post('opt_g');
		$area = $this->m_area->or_where($opt_c, $opt_g);
		to_json($area);
	}

	public function ajax_get_product_by_area(){
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');
		$c_product = $this->c_product->or_where($opt);

		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
		}

		$tmp = array_count_values($tmp);

		foreach ($tmp as $key => $value) {
			if (count($opt) > 1) {
				if($value < 2) {
					unset($tmp[$key]);
				}
			}
		}

		$tmp = array_keys($tmp);

		foreach ($c_product as $key => $prod) {
			if (!in_array($prod->ID_PRODUCT, $tmp)) {
				unset($c_product[$key]);
			}
		}

		to_json($c_product);
	}

	public function ajax_get_component(){
		$area = (null !== $this->input->post('area')) ? $this->input->post('area'):array(0);
		$group = $this->input->post('group');
		$dh_ly = ($this->input->post('dh_ly')=="D" || $this->input->post('dh_ly')=="H") ? "H":"D";
		foreach ($area as $val) {
			$plant[] = $this->m_area->get_data_by_id($val)->ID_PLANT;
		}

		if (in_array($group[0], array("1","4"))) {
			$c_parameter = $this->c_qaf_component->or_where($plant, $group, $dh_ly);
		}else{
			$c_parameter = $this->c_parameter->or_where($plant, $group, $dh_ly);
		}
		#echo $this->db->last_query();
		to_json($c_parameter);
	}

	public function generate_boxplot(){
		$start 		= $this->input->post('h_start');
		$start 		= explode(":", $start);
		$start 		= $start[0];
		$end 			= $this->input->post('h_end');
		$end 			= explode(":", $end);
		$end 			= $end[0];
		$tgl1			= $this->input->post('d_start');
		$tgl2			= $this->input->post('d_end');
		$bulan 		= $this->input->post('bulan');
		$tahun 		= $this->input->post('tahun');
		$dh_ly	 	= $this->input->post('dh_ly');
		switch ($dh_ly) {
			case 'Y':
				$date = '21/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				$tPer = $tahun;
				break;
			case 'M':
				$date = '21/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				$tPer = strtoupper(date("F", mktime(0, 0, 0, $bulan, 10))) . " " . $tahun;
				break;
			case 'D':
				$date = array($tgl1, $tgl2);
				$tPer = $tgl1 . ' to ' . $tgl2;
				break;
			case 'H':
				$date = array($start, $end);
				$tPer = $start .':00'. ' to ' . $end . ':00';
				break;
			default:
				$date = '21/' . str_pad($bulan, 2, '0', STR_PAD_LEFT) . '/' . $tahun;
				break;
		}
		#var_dump($date);
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= (null !== $this->input->post('param')) ? $this->input->post('param'):array(0);
		$component 	= array_filter($component, 'is_numeric');
		$mod   		= (in_array($grouparea[0], array("1","4"))) ?  (($dh_ly=="D" || $dh_ly=="H") ? "t_cement_hourly":"t_cement_daily"):(($dh_ly=="D" || $dh_ly=="H") ? "t_production_hourly":"t_production_daily");
		$parameter	= (in_array($grouparea[0], array("1","4"))) ?  array($product[0],$component):$component;

		$cmp = array();
		$plotdata = array();
		$komponen = "";
		$production = $this->{$mod}->get_qtrend($date, $area, $parameter, $dh_ly, $grouparea[0]);
		#$temp_prod  = new stdClass();
		#$tmp = array();
		#echo $this->db->last_query();
		if($production){

			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_AREA);
				$dash  = getDash($prod->ID_AREA);

				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'solid','width' => "3", "color" => "rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['mode'] = "lines+markers";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] = array("symbol" => "square", "size" => 8);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);

						/* Max */
						#if ($prod->V_MAX <= 900) {
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->V_MAX;
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "3", "color" => "red");
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MAX " . trim($prod->KD_COMPONENT);
							$max[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						#}

						/* Min */
						#if ($prod->V_MIN > 0) {
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][]  = $prod->V_MIN;
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['x'][]  = is_numeric($prod->PERIODE) ? str_pad($prod->PERIODE,2,'0',STR_PAD_LEFT):trim($prod->PERIODE);
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] = array('dash' => 'dashdot','width' => "3", "color" => "blue");
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] = "scatter";
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] = "MIN " . trim($prod->KD_COMPONENT);
							$min[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter']  = trim($prod->KD_COMPONENT);
						#}

					}
				}
			}

			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}

			
			if (isset($max)) {
				$k = 0;
				foreach ($max as $key => $value) {
					$ck = $max[$key];
					foreach ($ck as $ky => $v) {
						$pk[$ky] = count($ck[$ky]['y']);
					}
					$maks = array_keys($pk, max($pk));
					$ab[$k]['data'][] = $max[$key][$maks[0]];
					$k++;
				}
			}
			
			if (isset($min)) {
				$k = 0;
				foreach ($min as $key => $value) {
					$cl = $min[$key];
					foreach ($cl as $ky => $v) {
						$pl[$ky] = count($cl[$ky]['y']);
					}
					$mins = array_keys($pl, max($pl));
					$ab[$k]['data'][] = $min[$key][$mins[0]];
					$k++;
				}
			}
			

			//Generate Layout
			foreach ($ab as $key => $value) {
				$lay[$key]['layout'] = array(
					"title" => "Quality Trend - $tPer<BR>" . trim($value['data'][0]['parameter']),
					"xaxis" => array(
						"title" => "",
						"titlefont" => array('size' => ($dh_ly=='H') ? 18:14),
						"tickfont" => array('size' => ($dh_ly=='H') ? 5:10),
						"autorange" => TRUE
					),
					"yaxis" => array("title" => 'NILAI'),
					"legend"=> array(
						"font" => array("size" => 10)
					),
					"showlegend" => TRUE
				);
			}
			$plotdata['msg'] = "200";
		}else{
			$plotdata['msg'] = "404";
		}

		$plotdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$plotdata['layout'] = array_values((empty($lay)) ? array(0): $lay);
		to_json($plotdata);
	}

	private function getColor($num) {
        $hash = md5('warnawarni' . $num);
        return array(hexdec(substr($hash, 0, 2)), hexdec(substr($hash, 2, 2)), hexdec(substr($hash, 4, 2)));
	}

}

/* End of file Quality_trend.php */
/* Location: ./application/controllers/Quality_trend.php */
?>
