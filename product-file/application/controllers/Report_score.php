<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_score extends QMUser {

	public $JENIS_ASPEK;
	public $LIST_INPUT;
	public $LIST_COMPANY;
	public $LIST_REPORT;
	public $ASPEK;
	public $NILAI = array();
	public $MOON;
	public $YEAR;
	public $CATATAN;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_company');
		$this->load->model('M_indikator');
		$this->load->model('M_skoring');
		$this->load->model('T_skoring_ramah');
	}

	public function index(){
		$date  					= ($this->input->post('MONTH')==NULL) ? date("m/Y"):str_pad($this->input->post('MONTH'), 2, '0', STR_PAD_LEFT) . "/" . $this->input->post('YEAR');
		$this->ASPEK 			= ($this->input->post('ASPEK')==NULL ? "ALL":$this->input->post('ASPEK'));
		$this->JENIS_ASPEK 		= $this->M_jenis_aspek->get_list();
		$this->LIST_COMPANY 	= $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY 	= count($this->LIST_COMPANY);
		$this->LIST_INPUT 		= $this->M_skoring->get_list($this->ASPEK);
		$this->NILAI 			= $this->load_nilai($this->ASPEK, $date);
		$this->template->adminlte("v_report_score");
	}

	public function global(){
		$this->MOON = $this->input->post('MONTH');
		$this->YEAR = $this->input->post('YEAR');
		$date  = ($this->MOON==NULL) ? date("m/Y"):str_pad($this->MOON, 2, '0', STR_PAD_LEFT) . "/" . $this->YEAR;
		$this->LIST_COMPANY = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->JENIS_ASPEK 	= $this->M_jenis_aspek->get_list();
		$this->NILAI 		= $this->load_nilai("ALL", $date);
		$this->template->adminlte("v_report_score_global");
	}

	public function score_graph(){
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_ramah_score_graph");
	}

	public function score_graph_json(){
		$this->MOON  = $this->input->post('bulan');
		$this->YEAR  = $this->input->post('tahun');
		$this->ASPEK = $this->input->post('aspek');
		$date  = ($this->MOON==NULL) ? date("m/Y"):str_pad($this->MOON, 2, '0', STR_PAD_LEFT) . "/" . $this->YEAR;
		$this->LIST_COMPANY = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->JENIS_ASPEK 	= $this->M_jenis_aspek->get_list();
		$this->NILAI = $this->load_nilai($this->ASPEK, $date);

		foreach ($this->LIST_COMPANY as $key => $company) {
			$opco  = short_opco($company->KD_COMPANY);
			$nilai = empty($this->NILAI['comp']) ? array(0):$this->NILAI['comp'][$company->ID_COMPANY];
			$ctr   = 5*count($nilai);
			$sTot  = 0;

			if ($this->ASPEK!="ALL") {
				foreach ($nilai as $nl) {
					$sTot += $nl->NILAI_SKORING;
				}
				$total = $sTot/$ctr*$nl->BOBOT;
			}else {
				$vSCORE  = ARRAY();
				foreach ($this->JENIS_ASPEK as $aspek) {
					$vSCORE[$aspek->ID_JENIS_ASPEK][BOBOT] = $aspek->BOBOT;
					$vSCORE[$aspek->ID_JENIS_ASPEK][SCORE] = 0;
					$vSCORE[$aspek->ID_JENIS_ASPEK][ITEM] = 0;
					foreach ($nilai as $nl) {
						if ($aspek->ID_JENIS_ASPEK==$nl->ID_JENIS_ASPEK) {
							$vSCORE[$aspek->ID_JENIS_ASPEK][SCORE]  +=  $nl->NILAI_SKORING;
							$vSCORE[$aspek->ID_JENIS_ASPEK][ITEM]   +=  1;
						}
					}
				}
				$total = 0;
				foreach($vSCORE as $r){
					@$total += ($r[SCORE] / (5*$r[ITEM]) * $r[BOBOT]);
				}
			}
			$total = is_nan($total)?0:number_format($total, 2, ".", ".");
			$color = "";
			if($total < 70 ) $color = "255,0,0";
			if($total >= 70 && $total < 85) $color = "0,0,255";
			if($total >= 85 && $total < 90) $color = "0,204,0";
			if($total >= 90) $color = "255,192,0";

			$vArray[] = $total;
			$res[$key]['x'][]  		= $total;
			$res[$key]['type'] 		= 'bar';
			$res[$key]['marker'] 	= array(
							'color' => "rgb($color, 1.0)",
							'line' => array('color'=>'#FFFFFF','width' => 5)
						);
			$res[$key]['name'] 		= short_opco($company->KD_COMPANY) . ', ' . $total;
		}

		$code = (array_key_exists(0, array_count_values($vArray))) ? 404:200;
		$lay['autosize'] = true;
		$lay['yaxis'] 	 = array(
							"showspikes" => true,
							"showticklabels" => false,
							"title" => "",
							"showline" => true,
							"type" => '-',
							"autorange" => true,
						);
		$lay['dragmode'] 	= 'zoom';
		$lay['showlegend'] 	= true;
		$lay['barmode'] = 'group';
		$lay['xaxis'] 	= array(
							"showspikes" => true,
							"showticklabels" => true,
							"title" => "SCORE",
							"range" => array(0,100),
							"showline" => true,
							"type" => 'linear',
							"autorange" => true,
						);
		$lay['title'] 		= 'SCORE SI RAMAH - ' . strtoupper(date("F", mktime(0, 0, 0, $this->MOON, 10))) . ' ' . $this->YEAR;
		$lay['hovermode'] = 'closest';
		$lay['legend'] 		= array(
							"traceorder" => "normal",
							"x" => 1,
							"y" => 1,
							"width" => 200
						);

		$data['data'] 	= $res;
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}

	public function trend_graph_json(){
		$this->YEAR = $this->input->post('tahun');
		$this->LIST_COMPANY = $this->M_company->list_company($this->USER->ID_COMPANY);
		foreach ($this->LIST_COMPANY as $key => $company) {
			for ($i=1; $i <= 12; $i++) {
				$this->MOON = $i;
				$date  = ($this->MOON==NULL) ? date("m/Y"):str_pad($this->MOON, 2, '0', STR_PAD_LEFT) . "/" . $this->YEAR;
				$this->JENIS_ASPEK 	= $this->M_jenis_aspek->get_list();
				$this->NILAI = $this->load_nilai("ALL", $date);
				$opco    = short_opco($company->KD_COMPANY);
				$nilai   = empty($this->NILAI['comp']) ? array(0):$this->NILAI['comp'][$company->ID_COMPANY];
				$vSCORE  = ARRAY();
				foreach ($this->JENIS_ASPEK as $aspek) {
					$vSCORE[$aspek->ID_JENIS_ASPEK][BOBOT] = $aspek->BOBOT;
					$vSCORE[$aspek->ID_JENIS_ASPEK][SCORE] = 0;
					$vSCORE[$aspek->ID_JENIS_ASPEK][ITEM] = 0;
					foreach ($nilai as $nl) {
						if ($aspek->ID_JENIS_ASPEK==$nl->ID_JENIS_ASPEK) {
							$vSCORE[$aspek->ID_JENIS_ASPEK][SCORE]  +=  $nl->NILAI_SKORING;
							$vSCORE[$aspek->ID_JENIS_ASPEK][ITEM]   +=  1;
						}
					}
				}
				$total = 0;
				foreach($vSCORE as $r){
					@$total += ($r[SCORE] / (5*$r[ITEM]) * $r[BOBOT]);
				}

				$total = is_nan($total)?0:number_format($total, 2, ".", ".");
				$color = "";
				if($total < 70 ) $color = "255,0,0";
				if($total >= 70 && $total < 85) $color = "0,0,255";
				if($total >= 85 && $total < 90) $color = "0,204,0";
				if($total >= 90) $color = "255,192,0";

				#$res['BULAN'][$key][$i] = $i;
				$res[$key]['KD_COMPANY'] = $company->KD_COMPANY;
				$res[$key]['NM_COMPANY'] = $company->NM_COMPANY;
				$res[$key]['TOTAL'][] = $total;
				$res[$key]['BULAN'][] = $i;
			}

			$color = company_color_rgb($res[$key]['KD_COMPANY']);
			$temp[$key]['y'] = $res[$key]['TOTAL'];
			$temp[$key]['x'] = $res[$key]['BULAN'];
			$temp[$key]['type'] 	= 'scatter';
			$temp[$key]['line'] 	= array('dash' => 'solid','width' => "3", "color" => "rgba($color,2)");
			$temp[$key]['marker'] = array('symbol' => "square", "size" => 8);
			$temp[$key]['name'] 	= $res[$key]['NM_COMPANY'];
			$temp[$key]['hoverinfo'] 	= 'x+y';

		}

		//Generate Layout
		$lay['autosize'] = true;
		$lay['dragmode'] 	= 'zoom';
		$lay['showlegend'] 	= true;
		$lay['yaxis'] 	 = array(
			"showticklabels" => true,
			"title" => "SCORE",
			"showline" => true,
			"autorange" => false,
			"range" => array(0,100)
		);
		$lay['xaxis'] 	= array(
							"showticklabels" => true,
							"title" => "MONTH",
							"showline" => true,
							"autorange" => true
						);
		$lay['title'] 		= 'TREND SI RAMAH - ' . $this->YEAR;
		$lay['legend'] 		= array(
							"font" => array("size" => 10),
							"traceorder" => "normal",
							"x" => 1,
							"y" => 1,
							"width" => 200
						);


		$data['data'] 	= $temp;
		$data['layout'] = $lay;
		$data['result'] 	= 200;
		to_json($data);
	}

	public function trend_graph(){
		$this->template->adminlte("v_ramah_trend_graph");
	}

	private function load_nilai_trend($aspek, $date){
		$com_id = array();
		$jaspek = array();
		$result = array();
		$load   = $this->T_skoring_ramah->get_nilai_trend($aspek,$date);
		#echo $this->db->last_query();
		foreach ($load as $key => $value) {
			$load[$key]->CATATAN = $this->M_indikator->get_catatan($value->ID_BATASAN, $value->NILAI_SKORING)->CATATAN;
		}

		//Group by company
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			if(isset($com_id[$id_com])){
				$com_id[$id_com][] = $data;
			}else{
				$com_id[$id_com] = array($data);
			}
		}

		//Group by jenis aspek
		foreach ($load as $data) {
			$aspekj = $data->ID_JENIS_ASPEK;
			if(isset($jaspek[$aspekj])){
				$jaspek[$aspekj][] = $data;
			}else{
				$jaspek[$aspekj] = array($data);
			}
		}

		$result['comp']		= $com_id;
		$result['jaspek']	= $jaspek;
		return $result;
	}

	private function load_nilai($aspek, $date){
		$com_id = array();
		$jaspek = array();
		$result = array();
		$load   = $this->T_skoring_ramah->get_nilai($aspek,$date);
		#echo $this->db->last_query();
		foreach ($load as $key => $value) {
			$load[$key]->CATATAN = $this->M_indikator->get_catatan($value->ID_BATASAN, $value->NILAI_SKORING)->CATATAN;
		}

		//Group by company
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			if(isset($com_id[$id_com])){
				$com_id[$id_com][] = $data;
			}else{
				$com_id[$id_com] = array($data);
			}
		}

		//Group by jenis aspek
		foreach ($load as $data) {
			$aspekj = $data->ID_JENIS_ASPEK;
			if(isset($jaspek[$aspekj])){
				$jaspek[$aspekj][] = $data;
			}else{
				$jaspek[$aspekj] = array($data);
			}
		}

		$result['comp']		= $com_id;
		$result['jaspek']	= $jaspek;
		return $result;
	}

	public function get_nilai($id_jenis_aspek, $date){
		$date = str_replace("-", "/", $date);
		$nilai = $this->T_skoring_ramah->get_nilai($id_jenis_aspek,$date);
		to_json($nilai);
	}

	public function generate_report(){
		$date 			= $this->input->post('TANGGAL');
		$id_jenis_aspek = $this->input->post('ID_JENIS_ASPEK');

		$this->LIST_COMPANY = $this->M_company->list_company();
		$this->LIST_INPUT 	= $this->M_skoring->get_list();
		$this->LIST_REPORT  = $this->T_skoring_ramah->generate_report($id_jenis_aspek, $date);

		$this->template->ajax_response("v_ajax_report_score");
	}

	public function flipDiagonally($arr) {
	    $out = array();
	    foreach ($arr as $key => $subarr) {
	        foreach ($subarr as $subkey => $subvalue) {
	            $out[$subkey][$key] = $subvalue;
	        }
	    }
	    return $out;
	}
}

/* End of file Report_score.php */
/* Location: ./application/controllers/Report_score.php */
?>
