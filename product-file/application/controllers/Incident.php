<?php

class Incident extends QMUser {

	public $list_data = array();
	public $list_company = array();
	public $list_type = array();
	public $data_incident;
	public $solution;

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_incident");
		$this->load->model("m_company");
		$this->load->model("c_ncqr_product");
	}

	public function index(){
		$this->list_incident = $this->m_incident->datalist();#echo $this->m_incident->get_sql();
		$this->template->adminlte("v_incident");
	}

	public function assigned(){
		$this->list_incident = $this->m_incident->assigned();
		$this->template->adminlte("v_incident_assigned");
	}


	public function add(){
		$this->template->adminlte("v_incident_add");
	}

	public function create(){
		$this->m_incident->insert($this->input->post());
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/add");
		}
		else{
			$this->notice->success("Incident Data Saved.");
			redirect("incident");
		}
	}

	public function edit($ID_INCIDENT){
		$this->load->model("m_technician");
		$this->list_technician = $this->m_technician->datalist();
		$this->data_incident = $this->m_incident->get_data_by_id($ID_INCIDENT);
		$this->template->adminlte("v_incident_edit");
	}

	public function update($ID_INCIDENT){
		$this->m_incident->update($this->input->post(),$ID_INCIDENT);
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/edit/".$ID_INCIDENT);
		}
		else{
			$this->notice->success("Incident Data Updated.");
			redirect("incident");
		}
	}

	public function assign($ID_INCIDENT=NULL){
		$data['ID_TEKNISI']  = $this->input->post("ID_TEKNISI");
		$data['ASSIGN_NOTE']  = $this->input->post("ASSIGN_NOTE");
		$this->m_incident->assign($data,$ID_INCIDENT);
		if($this->m_incident->error()){
			$this->notice->error($this->m_incident->error());
			redirect("incident/edit/".$ID_INCIDENT);
		}
		else{
			$this->notice->success("Incident Assigned.");
			redirect("incident");
		}
	}

	public function solve($ID_INCIDENT){
		if($this->input->post("JUDUL_SOLUTION")){
			$this->solving($ID_INCIDENT);
		}
		else{
			$this->load->model("m_technician");
			$this->list_technician = $this->m_technician->datalist();
			$this->data_incident = $this->m_incident->get_data_by_id($ID_INCIDENT); # echo $this->m_incident->get_sql();

			if($this->data_incident[0]->ID_SOLUTION){
				$this->load->model("m_solution");
				$this->solution = $this->m_solution->get_data_by_id($this->data_incident[0]->ID_SOLUTION);
			}

			$this->template->adminlte("v_incident_solving");
		}
	}

	public function solving($ID_INCIDENT){
		$this->load->model("m_solution");
		$sol['JUDUL_SOLUTION'] = $this->input->post("JUDUL_SOLUTION");
		$sol['DETAIL_SOLUTION'] = $this->input->post("DETAIL_SOLUTION");
		$sol['MASALAH'] = $this->input->post("MASALAH");

		//CHECK SOLTION ID
		$ID_SOLUTION = $this->m_incident->get_data_by_id($ID_INCIDENT)->ID_SOLUTION;
		if($ID_SOLUTION){
				//UPDATE SOLUTION
			$this->m_solution->update($sol,$ID_SOLUTION);
			die($this->m_solution->get_sql());
		}
		else{
			//new
			$ID_SOLUTION = $this->m_solution->insert($sol);
			$this->m_incident->solved($ID_SOLUTION,$ID_INCIDENT);
		}

		if(!$this->m_incident->error()){
			$this->notice->success("Incident Solved");
			redirect("incident");
		}
		else{
			$this->notice->error("Something error while solving incident");
			redirect("incident/solve/".$ID_INCIDENT);
		}

	}

	public function solved($ID_INCIDENT=NULL){
		if($ID_INCIDENT){
			$this->solved_incident($ID_INCIDENT);
		}
		else{
			$this->list_incident = $this->m_incident->solved_list();
			$this->template->adminlte("v_incident_solved");
		}
	}

	public function solved_incident($ID_INCIDENT){
		$this->data_incident = $this->m_incident->get_solved_by_id($ID_INCIDENT);
		$this->template->adminlte("v_incident_solved_detail");
	}

	public function report(){
		$this->load->model("m_company");
		$this->list_company = $this->m_company->datalist();
		$this->list_type = $this->m_incident->type();
		#$this->list_incident = $this->i->datalist();
		$this->template->adminlte("v_incident_report");
	}

	public function get_report(){
		echo json_encode($this->m_incident->report($this->input->post())); # echo $this->m_incident->get_sql();
	}

	public function export_report(){
		$this->data = json_encode($this->m_incident->report($this->input->post()));
		$this->template->nostyle("xls_ncqr_incident_report");
	}

	public function dashboard($is_notifikasi=null){
		$this->list_company = $this->m_company->list_company_auth();
		if($is_notifikasi == 'notifikasi'){
			$this->template->adminlte("v_incident_dashboard_notif");
		}
		elseif($is_notifikasi == 'notifikasi_company'){
			$this->template->adminlte("v_incident_notif_company");
		}
		elseif($is_notifikasi == 'score'){
			$this->template->adminlte("v_score_ncqr");
		}
		else{
			$this->template->adminlte("v_incident_dashboard");
		}
	}

	public function get_dashboard(){
		echo json_encode($this->m_incident->get_dashboard($this->input->post()));# echo $this->m_incident->get_sql();
	}

	public function get_dashboard_notifikasi(){
		echo json_encode($this->m_incident->get_dashboard_notifikasi($this->input->post()));
	}

	public function get_notifikasi_company(){
		echo json_encode($this->m_incident->get_notifikasi_company($this->input->post()));
	}

	public function get_score_company(){
		echo json_encode($this->m_incident->avg_score_ncqr($this->input->post()));# echo $this->m_incident->get_sql();
	}

	public function grafik_ncqr(){
		$this->list_company = $this->m_company->list_company_auth();
		$this->template->adminlte("v_grafik_ncqr");
	}

	public function notif_sent($ID_INCIDENT){
		$this->list_notif = $this->m_incident->list_notif($ID_INCIDENT); # echo $this->m_incident->get_sql();
		$this->template->nostyle("v_notif_ncqr");
	}

	public function ajax_get_plant($ID_COMPANY=NULL){
		$this->load->model("m_plant");
		$plant= $this->m_plant->datalist($ID_COMPANY);
		to_json($plant);
	}

	public function ajax_get_area($ID_COMPANY=NULL,$ID_PLANT=NULL,$ID_GROUPAREA=NULL){
		$area= $this->c_ncqr_product->ncqr_area($ID_COMPANY,$ID_PLANT);# echo $this->m_area->get_sql();
		echo json_encode($area);
	}

	public function get_list(){
		$j_incident = ($this->input->post('j_incident')=='solved') ? 'solved':NULL; 
		$list = $this->m_incident->get_list($j_incident);
		#echo $this->db->last_query();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_INCIDENT;
			$row[] = $no;
			$row[] = $column->SUBJECT;
			$row[] = $column->TANGGAL;
			$row[] = ($column->JUDUL_SOLUTION) ? "Solved":"-";
			$row[] = ($column->JUDUL_SOLUTION) ? $column->JUDUL_SOLUTION:"-";
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_incident->count_all($j_incident),
            "recordsFiltered" => $this->m_incident->count_filtered($j_incident),
            "data" => $data,
        );

		to_json($output);
	}
}

?>
