<?php

class Incident_solving_score extends QMUser {
	
	public $list_data = array();
	public $data_score_penanganan;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_score_penanganan");
	}
	
	public function index(){
		$this->list_data = $this->m_score_penanganan->datalist();
		$this->template->adminlte("v_score_penanganan");
	}
	
	public function add(){
		$this->template->adminlte("v_score_penanganan_add");
	}
	
	public function create(){
		$this->m_score_penanganan->insert($this->input->post());
		if($this->m_score_penanganan->error()){
			$this->notice->error($this->m_score_penanganan->error());
			redirect("incident_solving_score/add");
		}
		else{
			$this->notice->success("Configuration Data Saved.");
			redirect("incident_solving_score");
		}
	}
	
	public function edit($ID_SCORE_PENANGANAN){
		$this->data_score_penanganan = $this->m_score_penanganan->get_data_by_id($ID_SCORE_PENANGANAN);
		$this->template->adminlte("v_score_penanganan_edit");
	}
	
	public function update($ID_SCORE_PENANGANAN){
		$this->m_score_penanganan->update($this->input->post(),$ID_SCORE_PENANGANAN);
		if($this->m_score_penanganan->error()){
			$this->notice->error($this->m_score_penanganan->error());
			redirect("incident_solving_score/edit/".$ID_SCORE_PENANGANAN);
		}
		else{
			$this->notice->success("Configuration Data Updated.");
			redirect("incident_solving_score");
		}
	}
	
	public function delete($ID_SCORE_PENANGANAN){
		$this->m_score_penanganan->delete($ID_SCORE_PENANGANAN);
		if($this->m_score_penanganan->error()){
			$this->notice->error($this->m_score_penanganan->error());
		}
		else{
			$this->notice->success("Incident Solving Score Data Removed.");
		}
		redirect("incident_solving_score");
	}
}	

?>
