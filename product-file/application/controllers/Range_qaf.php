<?php

class Range_qaf extends QMUser {
	
	public $list_config = array();
	public $list_plant = array();
	public $list_company = array();
	public $list_grouparea = array();
	public $list_component = array();
	
	public $ID_COMPANY;
	public $ID_PLANT;
	public $ID_GROUPAREA;
	public $DISPLAY;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("c_range_qaf");
		$this->load->model("m_company");
		$this->load->model("m_plant");
		$this->load->model("m_area");
	}
	
	
	public function index(){#
		$this->ID_COMPANY 	= $this->input->get("id_company");
		$this->ID_PLANT   	= $this->input->get("id_plan");
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist();
		$this->template->adminlte("v_range_qaf");
	}
	
	public function by_company($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->index();
	}
	
	public function add($ID_COMPANY=NULL){
		$this->ID_COMPANY = $ID_COMPANY;
		$this->list_company = $this->m_company->datalist($this->ID_COMPANY);
		$this->template->adminlte("v_plant_add");
	}
	
	public function create(){ 
		
		$ID_PLANT = $this->input->post("ID_PLANT");
		$ID_GROUPAREA = $this->input->post("ID_GROUPAREA");
		$DISPLAY = $this->input->post("DISPLAY");
		
		IF($ID_GROUPAREA){
			//clean
			$this->c_range_qaf->clean($ID_PLANT,$ID_GROUPAREA,$this->input->post("OPT_COMPONENT"),$DISPLAY);
			
			foreach($this->input->post("OPT_COMPONENT") as $ID_COMPONENT){
				$data = false;
				$data['ID_PLANT'] 		= $ID_PLANT;
				$data['ID_GROUPAREA'] 	= $ID_GROUPAREA;
				$data['ID_COMPONENT'] 	= $ID_COMPONENT;
				$data['DISPLAY'] 		= $DISPLAY;
				@$this->c_range_qaf->insert($data);
			}
		}
		redirect("component_assignment/edit/".$ID_PLANT."/".$ID_GROUPAREA."/".$DISPLAY);
	}
	
	public function edit($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){ 
		
		if($this->input->post("RANGE")){ 
			//update if exists, insert if new
			$range = $this->input->post("RANGE");
			$this->ERROR = null;
			foreach($range as $ID_COMPONENT => $v){
				$data = null;
				$data['ID_COMPONENT'] = $ID_COMPONENT;
				$data['ID_AREA'] = $this->input->post("ID_AREA");
				$data['V_MIN']	 = $v['V_MIN'];
				$data['V_MAX']	 = $v['V_MAX'];

				if($this->c_range_qaf->if_exists($data['ID_AREA'],$ID_COMPONENT)){ 
					//update
					$this->c_range_qaf->update($data);
					if($this->c_range_qaf->error()){
						$this->ERROR = $this->c_range_qaf->error();
					}
				}
				else{
					//insert
					$this->c_range_qaf->insert($data);
					if($this->c_range_qaf->error()){
						$this->ERROR = $this->c_range_qaf->error();
					}
				}
			}	
		}
		
		$this->load->model("m_component");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_AREA		= $this->input->post("ID_AREA");
		$this->ID_PLANT 	= $this->m_area->get_data_by_id($this->input->post("ID_AREA"))->ID_PLANT;
		$this->ID_COMPANY 	= $this->m_plant->get_data_by_id($this->ID_PLANT)->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant 	= $this->m_plant->datalist($this->ID_COMPANY);
		$this->list_area 	= $this->m_area->datalist($this->ID_COMPANY,$this->ID_PLANT); # ECHO $this->m_area->get_sql(); var_dump($this->list_area);
		$this->list_component = $this->m_component->datalist();
		
		$this->template->adminlte("v_qaf_range_edit");
	}
	
	public function view($ID_PLANT=NULL,$ID_GROUPAREA=NULL,$DISPLAY=NULL){
		$this->load->model("m_component");
		$this->data_plant = $this->m_plant->get_data_by_id($ID_PLANT);
		
		$this->ID_PLANT = $ID_PLANT;
		$this->ID_GROUPAREA = $ID_GROUPAREA;
		$this->DISPLAY = $DISPLAY;
		$this->ID_COMPANY = $this->data_plant->ID_COMPANY;
		
		$this->list_company = $this->m_company->datalist();
		$this->list_plant = $this->m_plant->datalist($this->data_plant->ID_COMPANY);
		$this->list_grouparea = $this->c_range_qaf->list_grouparea($ID_PLANT);
		$this->list_component = $this->m_component->datalist();
		
		$this->template->adminlte("v_parameter_view");
	}
	
	// ajax
	public function list_config_grouparea($ID_PLANT=NULL){#
		$this->list_config = $this->c_range_qaf->list_config($ID_PLANT); 
		$this->template->ajax_response("v_qaf_range_config_list");
	}
	
	public function async_list_area($ID_PLANT=NULL){
		$data = $this->m_area->list_area($ID_PLANT);
		echo json_encode($data);
	}
	
	public function async_configuration($ID_PLANT=NULL,$ID_AREA=NULL){
		$data = $this->c_range_qaf->configuration($ID_PLANT,$ID_AREA); #echo $this->c_range_qaf->get_sql();
		echo json_encode($data);
	}
}

