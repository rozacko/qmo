<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aspek extends QMUser {

	public $LIST;
	public $JENIS_ASPEK;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_aspek');
	}
	
	public function index(){
		$this->template->adminlte("v_aspek");
	}

	public function add(){
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_aspek_add");
	}

	public function edit(){
		$this->LIST = $this->M_aspek->get_by_id($this->input->post('ID_ASPEK'));
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_aspek_edit", $list);
	}

	public function get_list(){
		$list = $this->M_aspek->get_list();
		//echo $this->db->last_query();die();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_ASPEK;
			$row[] = $no;
			$row[] = $column->JENIS_ASPEK;
			$row[] = $column->ASPEK;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_aspek->count_all(),
            "recordsFiltered" => $this->M_aspek->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		$this->M_aspek->insert(array_map('strtoupper', $this->input->post()));
		if($this->M_aspek->error()){
			$this->notice->error($this->M_aspek->error());
			redirect("aspek/add");
		}
		else{
			$this->notice->success("Aspek Saved.");
			redirect("aspek");
		}
	}

	public function update(){
		$data["ID_JENIS_ASPEK"] = $this->input->post('ID_JENIS_ASPEK');
		$data["ASPEK"] = strtoupper($this->input->post('ASPEK'));

		$this->M_aspek->update($this->input->post('ID_ASPEK'), $data);
		
		if($this->M_aspek->error()){
			$this->notice->error($this->M_aspek->error());
			redirect("aspek/edit");
		}
		else{
			$this->notice->success("Aspek Updated.");
			redirect("aspek");
		}
	}

	public function remove(){
		$this->M_aspek->delete($this->input->post('ID_ASPEK'));
		if($this->M_aspek->error()){
			$this->notice->error($this->M_aspek->error());
		}
		else{
			$this->notice->success("Aspek Removed.");
		}
		redirect("aspek");
	}

}

/* End of file Aspek.php */
/* Location: ./application/controllers/Aspek.php */
?>
