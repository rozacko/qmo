<?php

class Authority extends QMUser {
	
	public $list_authority = array();
	public $list_menu	   = array();
	public $list_usergroup = array();
	public $list_groupmenu = array();
	public $data_authority;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_authority");
		$this->load->model("m_menu");
		$this->load->model("m_usergroup");
		$this->load->model("m_groupmenu");
	}
	
	public function index(){
		$this->list_menu	  = $this->m_menu->datalist();
		$this->list_usergroup = $this->m_usergroup->datalist();
		$this->list_groupmenu = $this->m_groupmenu->datalist();
		$this->template->adminlte("v_authority");
	}
	
	
	public function authlist($ID_USERGROUP=NULL){
		echo json_encode($this->m_authority->datalist($ID_USERGROUP));
	}
	
	public function auth($ID_USERGROUP,$ID_MENU){
		$data['ID_USERGROUP'] = $ID_USERGROUP;
		$data['ID_MENU']	  = $ID_MENU;
		$this->m_authority->insert($data);
	}
	
	public function deauth($ID_USERGROUP,$ID_MENU){
		$data['ID_USERGROUP'] = $ID_USERGROUP;
		$data['ID_MENU']	  = $ID_MENU;
		$this->m_authority->delete($data);
	}
}	

?>
