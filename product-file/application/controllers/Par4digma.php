<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

class Par4digma extends CI_Controller {

	public $ASPEK;
	public $NILAI = array();
	public $CATATAN;
	
	public $LIST_COMPANY;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model('M_company');
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_indikator');
		$this->load->model('T_skoring_ramah');
	}

	private function load_nilai($aspek, $date){
		$com_id = array();
		$jaspek = array();
		$result = array();
		$load   = $this->T_skoring_ramah->get_nilai($aspek,$date);
		#echo $this->db->last_query();
		foreach ($load as $key => $value) {
			$load[$key]->CATATAN = $this->M_indikator->get_catatan($value->ID_BATASAN, $value->NILAI_SKORING)->CATATAN;
		}

		//Group by company
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			if(isset($com_id[$id_com])){
				$com_id[$id_com][] = $data;
			}else{
				$com_id[$id_com] = array($data);
			}
		}

		//Group by jenis aspek
		foreach ($load as $data) {
			$aspekj = $data->ID_JENIS_ASPEK;
			if(isset($jaspek[$aspekj])){
				$jaspek[$aspekj][] = $data;
			}else{
				$jaspek[$aspekj] = array($data);
			}
		}

		$result['comp']		= $com_id;
		$result['jaspek']	= $jaspek;
		return $result;
	}
	
	public function global_report(){
		$result = array();
		$date = $this->input->get('date');
		$date = str_replace("-", "/", $date);
		$this->JENIS_ASPEK 		= $this->M_jenis_aspek->get_list();
		$this->LIST_COMPANY 	= $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->NILAI = $this->load_nilai('ALL', $date);
		foreach ($this->LIST_COMPANY as $company) {
			$data = array();
			$opco    = short_opco($company->KD_COMPANY);
			$nilai   = empty($this->NILAI['comp']) ? array(0):$this->NILAI['comp'][$company->ID_COMPANY];
			$vSCORE  = ARRAY();
			$catatan = "";
			foreach ($this->JENIS_ASPEK as $aspek) {
			  $vSCORE[$aspek->ID_JENIS_ASPEK][BOBOT] = $aspek->BOBOT;
			  $vSCORE[$aspek->ID_JENIS_ASPEK][SCORE] = 0;
			  $vSCORE[$aspek->ID_JENIS_ASPEK][ITEM] = 0;                
			  foreach ($nilai as $nl) {
				if ($nl->NILAI_SKORING <= 2) {
				  if (strpos($catatan, $nl->CATATAN)===FALSE) {
					# code...
					$catatan .= (!empty($nl->CATATAN)) ? "<li>" . $nl->CATATAN . "</li>" : "";
				  }
				}

				if ($aspek->ID_JENIS_ASPEK==$nl->ID_JENIS_ASPEK) {
				  $vSCORE[$aspek->ID_JENIS_ASPEK][SCORE]  +=  $nl->NILAI_SKORING;
				  $vSCORE[$aspek->ID_JENIS_ASPEK][ITEM]   +=  1;
				}
			  }
			}
			$total = 0;
			foreach($vSCORE as $r){
			  @$total += ($r[SCORE] / (5*$r[ITEM]) * $r[BOBOT]);
			}

			$total = is_nan($total)?0:number_format($total, 2, ".", ".");
			
			$color = "#";
				
			if($total < 70 ) $color = "#FF0000";
			if($total >= 70 && $total < 85) $color = "#0000FF"; 
			if($total >= 85 && $total < 90) $color = "#00CC00"; 
			if($total >= 90) $color = "#FFC000";
			
			$data['opco'] = $opco;
			$data['total'] = $total;
			$data['catatan'] = $catatan;
			$data['color'] = $color;
			$result[]= $data;
			
		}
		print_r(json_encode($result));
	}
	
	public function report_score(){
		$date = $this->input->get('date');
		$date = str_replace("-", "/", $date);
		$this->JENIS_ASPEK 		= $this->M_jenis_aspek->get_list();
		$this->LIST_COMPANY 	= $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->NILAI = $this->load_nilai('ALL', $date);
		print_r(json_encode($this->NILAI));
	}
}

/* End of file Par4digma.php */
/* Location: ./application/controllers/Par4digma.php */
?>
