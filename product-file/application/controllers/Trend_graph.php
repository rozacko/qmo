<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trend_graph extends QMUSER {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("m_area");
		$this->load->model("m_grouparea");
		$this->load->model("c_product");
		$this->load->model("c_parameter");
		$this->load->model("m_product");
		$this->load->model("m_component");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
		$this->load->model("t_production_hourly");
		$this->load->model("d_production_hourly");
		$this->load->model("t_cement_hourly");
		$this->load->model("d_cement_hourly");
		$this->load->model("t_cement_daily");
		$this->load->model("d_cement_daily");
	}
	
	public function index(){
		$this->list_company   = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->list_grouparea = $this->m_grouparea->datalist_auth($this->USER->ID_GROUPAREA);
		$this->list_product   = $this->m_product->datalist();
		$this->list_component = $this->m_component->datalist();
		$this->template->adminlte("v_trend_graph");
	}

	public function ajax_get_area_by_company(){
		$opt_c = (NULL==$this->input->post('opt_c'))? 0:$this->input->post('opt_c');
		$opt_g = (NULL==$this->input->post('opt_g'))? 0:$this->input->post('opt_g');
		$area = $this->m_area->or_where($opt_c, $opt_g);
		to_json($area);
	}

	public function ajax_get_product_by_area(){
		$tmp = array();
		$opt = (NULL==$this->input->post('opt'))? 0:$this->input->post('opt');
		$c_product = $this->c_product->or_where($opt);

		foreach ($c_product as $prod) {
			$tmp[] = $prod->ID_PRODUCT;
		}

		$tmp = array_count_values($tmp);

		foreach ($tmp as $key => $value) {
			if (count($opt) > 1) {
				if($value < 2) {
					unset($tmp[$key]);
				}
			}
		}
		
		$tmp = array_keys($tmp);

		foreach ($c_product as $key => $prod) {
			if (!in_array($prod->ID_PRODUCT, $tmp)) {
				unset($c_product[$key]);
			}
		}
		
		to_json($c_product);
	}

	public function ajax_get_component(){
		$area = (null !== $this->input->post('area')) ? $this->input->post('area'):array(0);
		$group = $this->input->post('group');
		$dh_ly = $this->input->post('dh_ly');
		foreach ($area as $val) {
			$plant[] = $this->m_area->get_data_by_id($val)->ID_PLANT;
		}
		
		$c_parameter = $this->c_parameter->or_where($plant, $group, $dh_ly);
		//echo $this->db->last_query();die();
		to_json($c_parameter);
	}

	public function test_boxplot(){
		$this->template->adminlte("testing_boxplot");
	}

	public function generate_boxplot(){
		$date 		= array($this->input->post('start'), $this->input->post('end'));
		$dh_ly	 	= $this->input->post('dh_ly');
		$company 	= $this->input->post('company');
		$area 		= $this->input->post('area');
		$grouparea 	= $this->input->post('grouparea');
		$product 	= $this->input->post('prod');
		$component 	= $this->input->post('param');
		$component 	= array_filter($component, 'is_numeric');
		$mod   		= (in_array($grouparea[0], array("1","4"))) ?  (($dh_ly=="D") ? "t_cement_daily":"t_cement_hourly"):(($dh_ly=="D") ? "t_production_daily":"t_production_hourly");
		$parameter	= ($grouparea[0]==1) ? array($product[0], $component):$component;
		$boxdata 	= array();
		$production = $this->{$mod}->get_nilai($date, $area, $parameter, $grouparea[0]);
		#var_dump($component);
		#echo $this->db->last_query();die();
		
		if($production){
			foreach ($production as $prod) {
				$color = getFixColor($prod->ID_AREA);

				if (in_array($prod->ID_AREA, $area)) {
					if (in_array($prod->ID_COMPONENT, $component)) {
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['parameter'] 	= trim($prod->KD_COMPONENT);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['name'] 		= trim($prod->AREA);
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['marker'] 		= array('color'=>"rgba($color,1.0)");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['boxmean']		= TRUE;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['y'][] 		= $prod->NILAI;
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['line'] 		= array('width' => "1.5");
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['type'] 		= "box";
						$cmp[$component[array_search($prod->ID_COMPONENT, $component)]][array_search($prod->ID_AREA, $area)]['boxpoints'] 	= false;
					}
				}
			}
			
			//var_dump ($cmp);
			//die();
			$ab = array();
			$k = 0;
			foreach ($cmp as $key => $value) {
				$ab[$k]['data'] = array_values($cmp[$key]);
				$k++;
			}
			


			//Generate Layout
			foreach ($ab as $key => $value) {
				$lay[$key]['layout'] = array(
					"title" => "Box Plot - " . trim($value['data'][0]['parameter']),
				    "paper_bgcolor" => "#F5F6F9",
				    "plot_bgcolor" => "#F5F6F9",
				    "xaxis1" => array(
				    	"tickfont" => array(
				    		"color" => "#4D5663",
				    		"size" => 8
				    	),
				    	"gridcolor" => "#E1E5ED",
				    	"titlefont" => array(
				    		"color" => "#4D5663"
				    	),
				    	"zerolinecolor" => "#E1E5ED",
		      			"title" => "PLANT - AREA"
				    ),
				    "legend" => array(
				    	"bgcolor" => "#F5F6F9",
				    	"font" => array(
				    		"color" => "#4D5663",
				    		"size" => 10
				    	)
				    )
				);
			}
			$boxdata['msg'] = "200";
		}else{
			$boxdata['msg'] = "404";
		}

		$boxdata['data'] 	= array_values((empty($ab)) ? array(0): $ab);
		$boxdata['layout'] 	= array_values((empty($lay)) ? array(0): $lay);
		to_json($boxdata);
	}

}

/* End of file Trend_graph.php */
/* Location: ./application/controllers/Trend_graph.php */
?>