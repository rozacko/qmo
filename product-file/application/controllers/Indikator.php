<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indikator extends QMUser {

	public $LIST;
	public $ID_JENIS_ASPEK;
	public $JENIS_ASPEK;
	public $ID_ASPEK;
	public $ASPEK;
	public $ID_BATASAN;
	public $BATASAN;
	public $KRITERIA;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_jenis_aspek');
		$this->load->model('M_aspek');
		$this->load->model('M_kriteria');
		$this->load->model('M_batasan');
		$this->load->model('M_indikator');
	}
	
	public function index(){
		$this->template->adminlte("v_indikator");
	}

	public function ajax_get_aspek($id_jenis_aspek){
		$aspek = $this->M_aspek->get_where(array("ID_JENIS_ASPEK" => $id_jenis_aspek));
		to_json($aspek);
	}

	public function ajax_get_aspek_bobot($id_aspek){
		$bobot = $this->M_aspek->get_by_id($id_aspek);
		to_json($bobot);
	}

	public function ajax_get_kriteria($id_aspek){
		$kriteria = $this->M_kriteria->get_where(array("ID_ASPEK" => $id_aspek));
		to_json($kriteria);
	}

	public function ajax_get_batasan($id_kriteria){
		$kriteria = $this->M_batasan->get_where(array("ID_KRITERIA" => $id_kriteria));
		to_json($kriteria);
	}

	public function add(){
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->ASPEK = $this->M_aspek->get_list();
		$this->template->adminlte("v_indikator_add");
	}

	public function edit(){
		$this->LIST = $this->M_indikator->get_by_id($this->input->post('ID_INDIKATOR'));
		$this->BATASAN = $this->M_batasan->get_by_id($this->LIST->ID_BATASAN);
		$this->KRITERIA = $this->M_kriteria->get_by_id($this->BATASAN->ID_KRITERIA);
		$this->ID_ASPEK = $this->KRITERIA->ID_ASPEK;
		$this->ID_JENIS_ASPEK = $this->M_aspek->get_by_id($this->ID_ASPEK)->ID_JENIS_ASPEK;
		$this->JENIS_ASPEK = $this->M_jenis_aspek->get_list();
		$this->template->adminlte("v_indikator_edit", $list);
	}

	public function get_list(){
		$list = $this->M_indikator->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_INDIKATOR;
			$row[] = $no;
			$row[] = $column->KRITERIA;
			$row[] = $column->BATASAN;
			$row[] = $column->RASIO_AWAL;
			$row[] = $column->RASIO_AKHIR;
			$row[] = $column->SKOR;
			$row[] = $column->CATATAN;
			$data[] = $row;
		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_indikator->count_all(),
            "recordsFiltered" => $this->M_indikator->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);
		unset($post['ID_ASPEK']);
		unset($post['ID_KRITERIA']);

		$this->M_indikator->insert(array_map('strtoupper', $post));

		if($this->M_indikator->error()){
			$this->notice->error($this->M_indikator->error());
			redirect("indikator/add");
		}
		else{
			$this->notice->success("Indikator Saved.");
			redirect("indikator");
		}
	}

	public function update(){
		$post = $this->input->post();
		unset($post['ID_JENIS_ASPEK']);
		unset($post['ID_ASPEK']);
		unset($post['ID_KRITERIA']);

		$this->M_indikator->update($this->input->post('ID_INDIKATOR'), $post);

		if($this->M_indikator->error()){
			$this->notice->error($this->M_indikator->error());
			redirect("indikator/edit");
		}
		else{
			$this->notice->success("Indikator Updated.");
			redirect("indikator");
		}
	}

	public function remove(){
		$this->M_indikator->delete($this->input->post('ID_INDIKATOR'));
		if($this->M_indikator->error()){
			$this->notice->error($this->M_indikator->error());
		}
		else{
			$this->notice->success("Indikator Removed.");
		}
		redirect("indikator");
	}

}

/* End of file Indikator.php */
/* Location: ./application/controllers/Indikator.php */
?>