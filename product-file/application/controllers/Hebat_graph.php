<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hebat_graph extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_company");
		$this->load->model("M_hebat_report");
	}
	
	public function index(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_cement_progress");
	}

	public function cement_progress(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_cement_progress");
	}

	public function clinker_progress(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_clinker_progress");
	}

	public function qaf_st(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_qaf_st");
	}

	public function qaf_cs(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_qaf_cs");
	}

	public function score_input_cement(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_input_cement");
	}

	public function score_input_production(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_input_production");
	}

	public function score_ncqr(){
		$this->list_company = $this->m_company->list_company_auth($this->USER->ID_COMPANY);
		$this->template->adminlte("v_hebat_graph_score_ncqr");
	}

	public function generate_json($tipe){
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');

		switch (strtolower($tipe)) {
			case 'cement':
				$query = "qaf_cement_progress";
				$title = "QAF CEMENT PROGRESS";
				break;

			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			case 'score_input_cement':
				$query = "score_input_cement";
				$title = "SCORE INPUT CEMENT";
				break;
			
			case 'score_input_production':
				$query = "score_input_production";
				$title = "SCORE INPUT PRODUCTION";
				break;
			
			case 'score_ncqr':
				$query = "score_ncqr";
				$title = "SCORE NCQR";
				break;
			
			default:
				$query = "qaf_cement_progress";
				$title = "QAF CEMENT PROGRESS";
				break;
		}

		$result = $this->M_hebat_report->{$query}($opco, $tahun);
		#echo $this->db->last_query();die();
		$temp   = array();
		$anno 	= array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $key => $res) {
				$color = monthColor($res->BULAN);
				$temp[$key]['x'][]    = $res->NILAI;
				$temp[$key]['type']   = 'bar';
				$temp[$key]['marker'] = array('color' => "rgb($color, 1.0)");
				$temp[$key]['name']   = strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10))) . ', ' . $res->NILAI;
			}

			$lastMonth   = $temp[count($temp)-1];
			$lastMonth_2 = $temp[count($temp)-2];
			$arrow_value = $lastMonth['x'][0]-$lastMonth_2['x'][0];
			$arrow_text	 = ($arrow_value < 0) ? $arrow_value:'+'.$arrow_value; 
			$arrow_color = ($arrow_value < 0) ? "rgb(214, 39, 40)":"rgb(44, 160, 44)"; 
			$arrow_sign  = ($arrow_value < 0) ? -30:30; 

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => false,
								"title" => "",
								//"range" => array(0,1),
								"showline" => true,
								"type" => '-',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['showlegend'] 	= true;
			$lay['barmode'] = 'group';
			$lay['xaxis'] 	= array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => "TOTAL QAF",
								//"range" => array(-0.5,0.5),
								"showline" => true,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.4
								)
							);
			$lay['title'] 		= $title;
			$lay['hovermode'] 	= 'closest';
			$lay['margin'] 		= array("l" => 200);

			if (count($temp)>1) {
				$lay['annotations'] = array(
					array(
						"xref" => "paper",
				        "yref" => "paper",
				        "text" => "<b>$arrow_text%</b>",
				        "arrowwidth" => 4,
				        "arrowsize" => 1,
				        "ay" => $arrow_sign,
				        "ax" => 0,
				        "y" => 0.5,
				        "x" => 1.1,
				        "font" => array("color" => $arrow_color),
				        "arrowcolor" => $arrow_color,
						"showarrow" => true
					)
				);
			}


			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= $temp;
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}

	public function json_cement_progress(){
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');

		$result = $this->M_hebat_report->qaf_cement_progress($bulan, $tahun, $opco);
		$temp   = array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $res) {
				$color 			= monthColor($res->BULAN);
				$temp['x'][]  	= $res->TOTAL_QAF;
				$temp['type'] 	= 'bar';
				$temp['marker'] = array('color' => "rgb($color, 1.0)");
				$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
			}

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => false,
								"title" => "",
								//"range" => array(0,1),
								"showline" => true,
								"type" => '-',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['showlegend'] 	= true;
			$lay['barmode'] = 'group';
			$lay['xaxis'] 	= array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => "TOTAL QAF",
								//"range" => array(-0.5,0.5),
								"showline" => true,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.23
								)
							);
			$lay['title'] 		= 'QAF CEMENT PROGRESS';
			$lay['hovermode'] 	= 'closest';
			$lay['margin'] 		= array("l" => 200);
			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= array($temp);
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);

	}

	public function json_line($tipe=''){
		$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$opco  = $this->input->post('opt_company');

		switch ($tipe) {
			case 'clinker':
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;

			case 'st':
				$query = "qaf_st";
				$title = "QAF OF SETTING TIME";
				break;

			case 'cs':
				$query = "qaf_cs";
				$title = "QAF OF COMPRESSIVE STRENGTH";
				break;
			
			default:
				$query = "qaf_clinker_progress";
				$title = "QAF CLINKER PROGRESS";
				break;
		}

		$result = $this->M_hebat_report->{$query}($opco, $tahun);
		#echo $this->db->last_query();die();
		$temp   = array();
		$lay    = array();
		$data   = array();

		if ($result) {
			foreach ($result as $res) {
				$color 			= monthColor($res->BULAN);
				$temp['x'][]  	= $res->BULAN;
				$temp['y'][]  	= $res->NILAI;
				$temp['type'] 	= 'scatter';
				$temp['line'] 	= array('dash' => 'dashdot');
				$temp['marker'] = array('symbol' => "square", "size" => 8);
				$temp['name'] 	= strtoupper(date("F", mktime(0, 0, 0, $res->BULAN, 10)));
				$temp['hoverinfo'] 	= 'x+y';
			}

			$lay['autosize'] = true;
			$lay['yaxis'] 	 = array(
								"showspikes" => true,
								"showticklabels" => true,
								"title" => 'TOTAL QAF',
								"showline" => true,
								"showgrid" => true,
								"gridwidth" => 4,
								"type" => 'linear',
								"autorange" => true,
							);
			$lay['dragmode'] 	= 'zoom';
			$lay['xaxis'] 	= array(
								"title" => "MONTH",
								"showgrid" => false,
								"showline" => true,
								"type" => 'linear',
								"autorange" => false,
								"range" => array(0,12),
							);
			$lay['images'] 	= array(
								array(
									"yanchor" => "middle",
							        "layer" => "above",
							        "xref" => "paper",
							        "yref" => "y",
							        "sizex" => 0.4,
							        "sizey" => 0.5,
							        "source" => base_url("images/" . $opco . ".png"),
							        "y" => 0,
							        "x" => -0.23
								)
							);
			$lay['title'] 		= $title;
			$lay['showlegend'] 	= false;
			$lay['hovermode'] 	= 'closest';
			$code				= 200;
		}else{
			$temp = NULL;
			$lay  = NULL;
			$code = 404;
		}

		

		$data['data'] 	= array($temp);
		$data['layout'] = $lay;
		$data['result'] = $code;
		to_json($data);
	}
	
	public function calculate_qaf_cement(){
		$this->M_hebat_report->calculate_qaf_cement($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}
	
	public function calculate_qaf_cs(){
		$this->M_hebat_report->calculate_qaf_cs($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}
	
	public function calculate_qaf_st(){
		$this->M_hebat_report->calculate_qaf_st($this->input->post("YEAR"),$this->input->post("ID_COMPANY"));
		echo "done";
	}
	
}

/* End of file Hebat_graph.php */
/* Location: ./application/controllers/Hebat_graph.php */
?>
