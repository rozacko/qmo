<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class H_report_score extends QMUser {

	public $JENIS_ASPEK;
	public $LIST_INPUT;
	public $LIST_COMPANY;
	public $LIST_REPORT;
	public $ASPEK;
	public $NILAI = array();

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model('M_aspek_hebat');
		$this->load->model('M_company');
		$this->load->model('M_kriteria_hebat');
		$this->load->model('M_hebat_report');
	}

	public function index(){
		$bulan 	= ($this->input->post('MONTH')==NULL) ? date("n"):$this->input->post('MONTH');
		$tahun 	= ($this->input->post('YEAR')==NULL) ? date("Y"):$this->input->post('YEAR');
		$this->ASPEK 		 = ($this->input->post('ASPEK')==NULL ? "ALL":$this->input->post('ASPEK'));
		$this->JENIS_ASPEK 	 = $this->M_aspek_hebat->get_list();
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT 	 = $this->M_kriteria_hebat->get_list($this->ASPEK);
		$this->NILAI 		 = $this->load_nilai($this->ASPEK, $bulan, $tahun);
		#var_dump($this->NILAI['jaspek']);
		$this->template->adminlte("v_h_report_score");
	}

	public function global(){
		$this->template->adminlte("v_h_global_report_svg");
	}

	public function global_json(){
		$bulan 	= ($this->input->post('MONTH')==NULL) ? date("n"):$this->input->post('MONTH');
		$tahun 	= ($this->input->post('YEAR')==NULL) ? date("Y"):$this->input->post('YEAR');
		$this->ASPEK 		 = ($this->input->post('ASPEK')==NULL ? "ALL":$this->input->post('ASPEK'));
		$this->JENIS_ASPEK 	 = $this->M_aspek_hebat->get_list();
		$this->LIST_COMPANY  = $this->M_company->list_company($this->USER->ID_COMPANY);
		$this->COUNT_COMPANY = count($this->LIST_COMPANY);
		$this->LIST_INPUT 	 = $this->M_kriteria_hebat->get_list($this->ASPEK);
		$this->NILAI 		 = $this->load_nilai($this->ASPEK, $bulan, $tahun);

		$data = array();
		$odd  = 1;
		foreach ($this->LIST_COMPANY  as $key => $company) {
	        $total  = NULL;
	        //$action = '';
	        $arr_k  = array_keys($this->NILAI['comp']);
	        $nilai  = (in_array($company->ID_COMPANY, $arr_k)) ? $this->NILAI['comp'][$company->ID_COMPANY]:array(0);
	        if ($nilai[0]!==0) {
	          foreach ($nilai as $nl) {
	            $skor = $nl->SCORE*$nl->BOBOT;
	            $total += round($skor,2);
	            $action[] = $nl->ACTION_PLAN;
	          }
	        }else{
	          $total = NULL;
	        }

	        $data[$key]['TOT_SCORE']  = $total;
	        $data[$key]['ID_COMPANY'] = $company->ID_COMPANY;
	        $data[$key]['NM_COMPANY'] = $company->NM_COMPANY;
	        $data[$key]['KD_COMPANY'] = $company->KD_COMPANY;
	        $data[$key]['ACTION_PLAN'] = $action;
	        $data[$key]['POS_X'] = ($odd % 2 == 0) ? 0.85:0.15;
	        $data[$key]['POS_Y'] = ($odd % 2 == 0) ? 1:0.2;

	        $odd++;
	    }

	    $tmp['sort'] 		= TRUE;
	    $tmp['textfont'] 	= array('size' => 16);
	    /*$tmp['marker'] 		= array('colors' => array(
	    						0 => 'rgb(255, 0, 0)',
	    						1 => '',
	    						2 => '',
	    						3 => '',
	    					));*/

	    usort($data, function($a, $b) {
		    return (float) $b['TOT_SCORE'] <=> (float) $a['TOT_SCORE'];
			});

	    $rank = 1;
	    foreach ($data as $res) {
	    	#$tmp['labels'][] = short_opco($res['KD_COMPANY']) . "<br>" . $res['TOT_SCORE'];
	    	#$tmp['values'][] = 25;
	    	$tmp['labels'][]  = short_opco($res['KD_COMPANY']);
	    	$tmp['values'][]  = (is_numeric($res['TOT_SCORE'])) ? round($res['TOT_SCORE'],2):$res['TOT_SCORE'];
	    	$tmp['warna'][]   = getRankColor($rank);
	    	$tmp['rank'][]    = $rank;
	    	$tmp['act_plan'][] = $res['ACTION_PLAN'];

	    	$ano['xref'] = 'x';
	    	$ano['yref'] = 'y';
	    	$ano['text'] = "<b>ACTION PLAN</b>" . $res['ACTION_PLAN'];
	    	$ano['y'] = $res['POS_Y'];
	    	$ano['x'] = $res['POS_X'];
	    	$ano['font'] = array('size' => 12);
	    	$ano['showarrow'] = false;
	    	$anno[] = $ano;
	    	$rank++;
	    }


	    $tmp['direction'] 	= 'counterclockwise';
	    $tmp['hoverinfo'] 	= 'label+text';
	    $tmp['textinfo'] 	= 'label';
	    $tmp['type'] 		= 'pie';
	    $tmp['name'] 		= 'VAL';

	    $lay['layout']		= array(
	    	"autosize" => true,
		    "title" => "GLOBAL REPORT",
		    "showlegend" => false,
		    "hovermode" => "closest",
		    "margin"  => array("r" => 0, "l" => 100),
		    "annotations" => $anno
	    );

	    $pie['data'] = array($tmp);
	    $pie['layout'] = $lay;

	    to_json($pie);
	}

	public function load_nilai($aspek, $bulan, $tahun){
		$com_id = array();
		$jaspek = array();
		$result = array();
		$load = $this->M_hebat_report->v_skoring_hebat($aspek, $bulan, $tahun);

		//Group by company
		foreach ($load as $data) {
			$id_com = $data->ID_COMPANY;
			if(isset($com_id[$id_com])){
				$com_id[$id_com][] = $data;
			}else{
				$com_id[$id_com] = array($data);
			}
		}

		//Group by jenis aspek
		foreach ($load as $data) {
			$aspekj = $data->ID_ASPEK;
			if(isset($jaspek[$aspekj])){
				$jaspek[$aspekj][] = $data;
			}else{
				$jaspek[$aspekj] = array($data);
			}
		}

		$result['comp']		= $com_id;
		$result['jaspek']	= $jaspek;
		return $result;
	}

}

/* End of file H_report_score.php */
/* Location: ./application/controllers/H_report_score.php */
?>
