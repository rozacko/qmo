<?php

class Product extends QMUser {
	
	public $list_data = array();
	public $data_product;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_product");
	}
	
	public function index(){
		$this->list_product = $this->m_product->datalist();
		$this->template->adminlte("v_product");
	}
	
	public function add(){
		$this->template->adminlte("v_product_add");
	}
	
	public function create(){
		$this->m_product->insert($this->input->post());
		if($this->m_product->error()){
			$this->notice->error($this->m_product->error());
			redirect("product/add");
		}
		else{
			$this->notice->success("Product Data Saved.");
			redirect("product");
		}
	}
	
	public function edit($ID_PRODUCT){
		$this->data_product = $this->m_product->get_data_by_id($ID_PRODUCT);
		$this->template->adminlte("v_product_edit");
	}
	
	public function update($ID_PRODUCT){
		$this->m_product->update($this->input->post(),$ID_PRODUCT);
		if($this->m_product->error()){
			$this->notice->error($this->m_product->error());
			redirect("product/edit/".$ID_PRODUCT);
		}
		else{
			$this->notice->success("Product Data Updated.");
			redirect("product");
		}
	}
	
	public function delete($ID_PRODUCT){
		$this->m_product->delete($ID_PRODUCT);
		if($this->m_product->error()){
			$this->notice->error($this->m_product->error());
		}
		else{
			$this->notice->success("Product Data Removed.");
		}
		redirect("product");
	}

	public function get_list(){
		$list = $this->m_product->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_PRODUCT;
			$row[] = $no;
			$row[] = $column->KD_PRODUCT;
			$row[] = $column->NM_PRODUCT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_product->count_all(),
            "recordsFiltered" => $this->m_product->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}	

}	

?>
