<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input_area_daily extends QMUser {

	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_company");
		$this->load->model("c_parameter");
		$this->load->model("m_component");
		$this->load->model("m_machinestatus");
		$this->load->model("t_production_daily");
		$this->load->model("d_production_daily");
	}
	
	public function index(){
		$this->list_company = $this->m_company->datalist();
		$this->template->adminlte("v_input_area_daily", $data);
	}

	public function ajax_get_component($id_plant='',$id_grouparea=''){
		//HandsonTable Column Header
		$id_comp[] = '_date';
		$header[]['title'] = 'DATE'; 
		$param = $this->c_parameter->configuration($id_plant, $id_grouparea,'D');
		foreach ($param as $col) {
			$cmp = $this->m_component->get_data_by_id($col->ID_COMPONENT);
			$id_comp[] = $cmp->ID_COMPONENT;
			$header[]['title'] = strtoupper($cmp->NM_COMPONENT);
		}
		

		//Tambahkan Status dan Remark
		$header[]['title'] = 'MACHINE STATUS';
		$header[]['title'] = 'REMARK';

		$id_comp[] = '_machine_status';
		$id_comp[] = '_remark';

		//Var
		$data['colHeader'] 	= $header;	//Set header
		$data['id_comp'] 	= $id_comp;	//Set header
		$data['jsonData'] 	= '';
		to_json($data);
	}

	public function load_table(){
		$post = $this->input->post();
		$form = $post['formData'];
		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		//Convert array to object
		$form = (object)$tmp;

		//Load from T_production_daily
		$data = array();
		$t_prod = $this->t_production_daily->data_where("TO_CHAR(DATE_DATA, 'MM/YYYY')='".$form->TANGGAL."' AND ID_AREA='" .$form->ID_AREA."'");
		foreach ($t_prod as $key => $row) {
			$d_prod = $this->d_production_daily->get_by_id($row->ID_PRODUCTION_DAILY);
			$data[$key][] = $row->TANGGAL;
			foreach ($d_prod as $k => $vl) {
				$data[$key][] = $vl->NILAI;
			}
			$data[$key][] = $this->m_machinestatus->get_data_by_id($row->ID_MESIN_STATUS)->NM_MESIN_STATUS;
			$data[$key][] = $row->MESIN_REMARK;
		}

		to_json($data);
	}

	public function save_table(){
		$hanClean = array();
		$kolom 	  = 0;
		$rows 	  = array();
		$hanData  = $this->input->post('data');
		$formData = $this->input->post('formData');
		$id_comp  = $this->input->post('id_comp');
		$ct_cmp	  = count($id_comp);


#		/** D_PRODUCTION_HOURLY **/
#
#		## Cleansing empty row
#		foreach ($hanData as $value) {
#			if(count(array_filter($value))!==0){
#				$hanClean[] = $value;
#			}
#		}
#
#		## Merge id_componen with handsontable data
#		foreach ($hanClean as $key => $rw):
#			foreach ($id_comp as $k => $vl):
#				$rows[$key][] = array('id'=>$vl, 'nilai'=> $rw[$k]);
#			endforeach;
#		endforeach;
#		
#		
#		var_dump($row);
		
		
		/** T_PRODUCTION_HOURLY **/
#		sleep(1);


		$post = $this->input->post();
		
		
		
		$form = $post['formData'];
		$comp = $post['id_comp'];
		$data = $post['data'];
		
		
		#var_dump($comp); exit;
		/*
		 *     *--->x
		 *     |
		 *     y
		 * */
				
		//clean form
		//$tmp = $form;

		foreach($form as $r){ 
			$tmp[$r[name]] = $r[value];
		}
		
		$form = (object)$tmp;
		#var_dump($form);exit();
		//bulan
		//$x = explode("/",$form->TANGGAL);
		//$bulan = $x[0]."/".$x[2];
		$bulan = $form->TANGGAL;
		
		$form->BULAN = $bulan;
		//$form->ID_AREA=1;  ################################################################## ojo lali di isi dari form
					#var_dump($comp)
		/*	array(8) {
			  [0]=>
			  string(5) "_date"
			  [1]=>
			  string(1) "1"
			  [2]=>
			  string(1) "2"
			  [3]=>
			  string(1) "3"
			  [4]=>
			  string(1) "4"
			  [5]=>
			  string(1) "6"
			  [6]=>
			  string(15) "_machine_status"
			  [7]=>
			  string(7) "_remark"
			}
		*/
			#var_dump($row); exit;
		/*	array(8) {
			  [0]=>
			  string(1) "1"
			  [1]=>
			  string(2) "11"
			  [2]=>
			  string(2) "21"
			  [3]=>
			  string(2) "31"
			  [4]=>
			  string(2) "41"
			  [5]=>
			  string(2) "51"
			  [6]=>
			  string(0) ""
			  [7]=>
			  string(0) ""
			}
		*/
			//T
			
			//D
			
		//T_PRODUCTION_DAILY (1): ID_PRODUCTION_DAILY,ID_AREA,ID_MESIN_STATUS,DATE_DATA,DATE_ENTRY,USER_ENTRY,USER_UPDATE,MESIN_REMARK
		//D_PRODUCTION_DAILY (M): ID_PRODUCTION_DAILY, ID_COMPONENT, NILAI
		
		//read line by line
		foreach($data as $y => $row){ //y index
			
			//sub index
			$i_date			= 0;
			$i_mesin_status = array_search("_machine_status",$comp);
			$i_remark		= array_search("_remark",$comp);
			
			if(!$row[$i_date]) continue;
			
			//T
			$tdata['ID_AREA']			= $form->ID_AREA;
			$tdata['ID_MESIN_STATUS']	= $this->m_machinestatus->get_data_by_name($row[$i_mesin_status],'ID_MESIN_STATUS');
			$tdata['DATE_DATA']			= $row[$i_date]."/".$form->BULAN; # dd/mm/yyyy
			//$tdata['DATE_SET']			= date("d/m/Y"); //if entrydate exists then update_date else entry_date
			//$tdata['USER_SET']			= $this->USER->ID_USER; //if entryuser exists then update user else entry user
			$tdata['MESIN_REMARK']		= $row[$i_remark];
			
			#var_dump($tdata);
			#save T
			//cek dulu
			$exists = null;
			
			$ID_PRODUCTION_DAILY = $this->t_production_daily->get_id($tdata[ID_AREA],$tdata[DATE_DATA]);

			
			if(!$ID_PRODUCTION_DAILY){
				$tdata['DATE_ENTRY'] = date("d/m/Y");
				$tdata['USER_ENTRY'] = $this->USER->ID_USER;
				$ID_PRODUCTION_DAILY = $this->t_production_daily->insert($tdata);
			}
			else{
				$tdata['DATE_UPDATE'] = date("d/m/Y");
				$tdata['USER_UPDATE'] = $this->USER->ID_USER;
				$this->t_production_daily->update($tdata,$ID_PRODUCTION_DAILY);
			}
						
			//D
			for($x=1;$x<$i_mesin_status;$x++){
				$ddata = null;
				$ddata['ID_PRODUCTION_DAILY'] 	= $ID_PRODUCTION_DAILY;
				$ddata['ID_COMPONENT']			= $comp[$x];
				$ddata['NILAI']					= $row[$x];
				$ddata['NO_FIELD']				= $x;
				#save 
				
				#var_dump($ddata);
				
				#exit;
				
				if(!$this->t_production_daily->d_exists($ddata)){
					$this->t_production_daily->d_insert($ddata);
				}
				else{
					$this->t_production_daily->d_update($ddata);
				}
			}
			
			//exit;

		}
		


	}
}

/* End of file Input_area_hourly.php */
/* Location: ./application/controllers/Input_area_hourly.php */
?>
