<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kriteria_hebat extends QMUser {

	public $LIST;
	public $ASPEK;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_aspek_hebat');
		$this->load->model('M_kriteria_hebat');
	}
	
	public function index(){
		$this->template->adminlte("v_kriteria_hebat");
	}

	public function add(){
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->template->adminlte("v_kriteria_hebat_add");
	}

	public function edit(){
		$this->LIST = $this->M_kriteria_hebat->get_by_id($this->input->post('ID_KRITERIA'));
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->template->adminlte("v_kriteria_hebat_edit", $list);
	}

	public function get_list(){
		$list = $this->M_kriteria_hebat->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_KRITERIA;
			$row[] = $no;
			$row[] = $column->ASPEK;
			$row[] = $column->KRITERIA;
			$row[] = $column->BOBOT;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_kriteria_hebat->count_all(),
            "recordsFiltered" => $this->M_kriteria_hebat->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		//$post = array();
		$post = $this->input->post();

		$this->M_kriteria_hebat->insert(array_map('strtoupper', $post));
		if($this->M_kriteria_hebat->error()){
			$this->notice->error($this->M_kriteria_hebat->error());
			redirect("kriteria_hebat/add");
		}
		else{
			$this->notice->success("Kriteria Saved.");
			redirect("kriteria_hebat");
		}
	}

	public function update(){
		$post = $this->input->post();

		$this->M_kriteria_hebat->update($this->input->post('ID_KRITERIA'), $post);

		if($this->M_kriteria_hebat->error()){
			$this->notice->error($this->M_kriteria_hebat->error());
			redirect("kriteria_hebat/edit");
		}
		else{
			$this->notice->success("Kriteria Updated.");
			redirect("kriteria_hebat");
		}
	}

	public function remove(){
		$this->M_kriteria_hebat->delete($this->input->post('ID_KRITERIA'));
		if($this->M_kriteria_hebat->error()){
			$this->notice->error($this->M_kriteria_hebat->error());
		}
		else{
			$this->notice->success("Kriteria Removed.");
		}
		redirect("kriteria_hebat");
	}

}

/* End of file Kriteria_hebat.php */
/* Location: ./application/controllers/Kriteria_hebat.php */
?>