<?php

class Jabatan extends QMUser {
	
	public $list_data = array();
	public $data_jabatan;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_jabatan");
	}
	
	public function index(){
		$this->list_jabatan = $this->m_jabatan->datalist();
		$this->template->adminlte("v_jabatan");
	}
	
	public function add(){
		$this->template->adminlte("v_jabatan_add");
	}
	
	public function create(){
		$this->m_jabatan->insert($this->input->post());
		if($this->m_jabatan->error()){
			$this->notice->error($this->m_jabatan->error());
			redirect("jabatan/add");
		}
		else{
			$this->notice->success("Notification Group Saved.");
			redirect("jabatan");
		}
	}
	
	public function edit($ID_JABATAN){
		$this->data_jabatan = $this->m_jabatan->get_data_by_id($ID_JABATAN);
		$this->template->adminlte("v_jabatan_edit");
	}
	
	public function update($ID_JABATAN){
		$this->m_jabatan->update($this->input->post(),$ID_JABATAN);
		if($this->m_jabatan->error()){
			$this->notice->error($this->m_jabatan->error());
			redirect("jabatan/edit/".$ID_JABATAN);
		}
		else{
			$this->notice->success("Notification Group Updated.");
			redirect("jabatan");
		}
	}
	
	public function delete($ID_JABATAN){
		$this->m_jabatan->delete($ID_JABATAN);
		if($this->m_jabatan->error()){
			$this->notice->error($this->m_jabatan->error());
		}
		else{
			$this->notice->success("Notification Group Removed.");
		}
		redirect("jabatan");
	}
}	

?>
