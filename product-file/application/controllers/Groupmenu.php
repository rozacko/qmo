<?php

class Groupmenu extends QMUser {
	
	public $list_data = array();
	public $data_groupmenu;
	
	public function __construct(){
		parent::__construct();
		$this->load->helper("string");
		$this->load->model("m_groupmenu");
	}
	
	public function index(){
		$this->list_groupmenu = $this->m_groupmenu->datalist();
		$this->template->adminlte("v_groupmenu");
	}
	
	public function add(){
		$this->template->adminlte("v_groupmenu_add");
	}
	
	public function create(){
		$this->m_groupmenu->insert($this->input->post());
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
			redirect("groupmenu/add");
		}
		else{
			$this->notice->success("Group Menu Data Saved.");
			redirect("groupmenu");
		}
	}
	
	public function edit($ID_GROUPMENU){
		$this->data_groupmenu = $this->m_groupmenu->get_data_by_id($ID_GROUPMENU);
		$this->template->adminlte("v_groupmenu_edit");
	}
	
	public function update($ID_GROUPMENU){
		$this->m_groupmenu->update($this->input->post(),$ID_GROUPMENU);
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
			redirect("groupmenu/edit/".$ID_GROUPMENU);
		}
		else{
			$this->notice->success("Group Menu Data Updated.");
			redirect("groupmenu");
		}
	}
	
	public function delete($ID_GROUPMENU){
		$this->m_groupmenu->delete($ID_GROUPMENU); 
		if($this->m_groupmenu->error()){
			$this->notice->error($this->m_groupmenu->error());
		}
		else{
			$this->notice->success("Group Menu Data Removed.");
		}
		redirect("groupmenu");
	}

	public function get_list(){
		$list = $this->m_groupmenu->get_list();
		$data = array();
		$no   = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_GROUPMENU;
			$row[] = $no;
			$row[] = $column->NM_GROUPMENU;
			$row[] = $column->NO_ORDER;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->m_groupmenu->count_all(),
            "recordsFiltered" => $this->m_groupmenu->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

}	

?>
