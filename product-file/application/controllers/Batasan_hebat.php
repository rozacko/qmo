<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Batasan_hebat extends QMUser {

	public $LIST;
	public $ASPEK;
	public $BATASAN;
	public $KRITERIA;

	public function __construct(){
		parent::__construct();

		$this->load->helper("string");
		$this->load->model('M_aspek_hebat');
		$this->load->model('M_kriteria_hebat');
		$this->load->model('M_batasan_hebat');
	}
	
	public function index(){
		$this->template->adminlte("v_batasan_hebat");
	}

	public function ajax_get_aspek($id_jenis_aspek){
		$aspek = $this->M_aspek_hebat->get_where(array("ID_JENIS_ASPEK" => $id_jenis_aspek));
		to_json($aspek);
	}

	public function ajax_get_aspek_bobot($id_aspek){
		$bobot = $this->M_aspek_hebat->get_by_id($id_aspek);
		to_json($bobot);
	}

	public function ajax_get_kriteria($id_aspek){
		$kriteria = $this->M_kriteria_hebat->get_where(array("ID_ASPEK" => $id_aspek));
		to_json($kriteria);
	}

	public function ajax_get_kriteria_by_id($id_kriteria){
		$kriteria = $this->M_kriteria_hebat->get_by_id($id_kriteria);
		to_json($kriteria);
	}

	public function add(){
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->template->adminlte("v_batasan_hebat_add");
	}

	public function edit(){
		$this->ASPEK = $this->M_aspek_hebat->get_list();
		$this->LIST = $this->M_batasan_hebat->get_by_id($this->input->post('ID_BATASAN'));
		$this->KRITERIA = $this->M_kriteria_hebat->get_by_id($this->LIST->ID_KRITERIA);
		$this->ID_ASPEK = $this->KRITERIA->ID_ASPEK;
		$this->template->adminlte("v_batasan_hebat_edit", $list);
	}

	public function get_list(){
		$list = $this->M_batasan_hebat->get_list();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $column) {
			$no++;
			$row = array();
			$row[] = $column->ID_BATASAN;
			$row[] = $no;
			$row[] = $column->ASPEK;
			$row[] = $column->KRITERIA;
			$row[] = $column->BOBOT;
			$row[] = $column->BATASAN;
			$data[] = $row;

		}

		$output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->M_batasan_hebat->count_all(),
            "recordsFiltered" => $this->M_batasan_hebat->count_filtered(),
            "data" => $data,
        );

		to_json($output);
	}

	public function create(){
		//$post = array();
		$post = $this->input->post();
		unset($post['ID_ASPEK']);

		$this->M_batasan_hebat->insert(array_map('strtoupper', $post));

		if($this->M_batasan_hebat->error()){
			$this->notice->error($this->M_batasan_hebat->error());
			redirect("batasan_hebat/add");
		}
		else{
			$this->notice->success("Batasan_hebat Saved.");
			redirect("batasan_hebat");
		}
	}

	public function update(){
		$post = $this->input->post();
		unset($post['ID_ASPEK']);

		$this->M_batasan_hebat->update($this->input->post('ID_BATASAN'), $post);
		
		if($this->M_batasan_hebat->error()){
			$this->notice->error($this->M_batasan_hebat->error());
			redirect("batasan_hebat/edit");
		}
		else{
			$this->notice->success("Batasan_hebat Updated.");
			redirect("batasan_hebat");
		}
	}

	public function remove(){
		$this->M_batasan_hebat->delete($this->input->post('ID_BATASAN'));
		if($this->M_batasan_hebat->error()){
			$this->notice->error($this->M_batasan_hebat->error());
		}
		else{
			$this->notice->success("Batasan_hebat Removed.");
		}
		redirect("batasan_hebat");
	}

}

/* End of file Batasan_hebat.php */
/* Location: ./application/controllers/Batasan_hebat.php */
?>