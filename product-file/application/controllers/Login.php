<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Home {

	public function index(){
		if($this->USER){
			$this->session->unset_userdata("USER");
			$this->USER = NULL;
		}
		$this->template->login("login");
	}

	public function verification($redirect=NULL){
		
		#var_dump($this->input->post());
		
		if($this->input->post("USERNAME") && $this->input->post("PASSWORD")){
				
			$this->load->library("password");
			$this->load->model("m_user");
			$user = $this->m_user->data(array('USERNAME'=>strtolower($this->input->post("USERNAME")))); #die($this->m_user->get_sql());
			
			if($user){				
				if(!$this->login_ldap($this->input->post("USERNAME"),$this->input->post("PASSWORD"))){
					if(!$this->login_ldap($this->input->post("USERNAME")."@smig.corp",$this->input->post("PASSWORD"))){
						unset($user);
						$user = NULL;
					}
				}				
			}			
			if($user){
				$this->session->set_userdata("USER",$user);
			}
		}
		
		if(!$user){
			$redirect = "login";
			$this->notice->error("Login invalid.");
		}
		
		redirect($redirect);
	}

	public function set_log_user($ID_USER){
		$this->load->model("log_users");
		$this->log_users->set($ID_USER);
	}
	
	function login_ldap($username,$password){
		$ldap['user'] = $username; 
		$ldap['pass'] = $password;				
		$ldap['host'] = '10.15.3.121';
		$ldap['port'] = 389;
		$ldap['conn'] = @ldap_connect($ldap['host'], $ldap['port']);
		
		if(!$ldap['conn']){
			return false;
		}
		else{
			#var_dump($ldap);
			@ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
			@$ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
			
			#var_dump($ldap); 
			if(!$ldap['bind']){
				return false;
			}
			@ldap_close($ldap['conn']);
			return true;
		}
	}

}
