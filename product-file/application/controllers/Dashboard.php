<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends QMUser {
	
	var $list_company = array();
	var $list_grouparea = array();
	var $list_area = array();
	var $list_data = array();
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_company");
		$this->load->model("m_grouparea");
		$this->load->model("m_area");
		$this->load->model("m_plant");
		$this->load->model("treport");
	}

	public function index(){
		$this->list_company 	= $this->m_company->list_company();
		$this->list_grouparea 	= $this->m_grouparea->datalist();
		$this->list_area 		= $this->m_area->datalist();
		$this->template->adminlte("v_dashboard");
		#$this->access_log();
	}
	
	public function dashboard(){
		
	}
	
	public function get_plant(){
		$list_plant = $this->m_plant->datalist($this->input->post("ID_COMPANY")); #echo $this->m_plant->get_sql();
		echo json_encode($list_plant);
	}
	
	public function daily(){
		
		
		
		$plants = $this->input->post("ID_PLANT");
		$data   = $this->input->post();
		
		if(!$data or !$plants){
			die("No data selected");
		}
		
		foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
			$this->list_grouparea[] = $this->m_grouparea->get_data_by_id($ID_GROUPAREA);
		endforeach;
		
		foreach($plants as $ID_COMPANY => $ID_PLANTS):
			foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
				$i = 0;
				foreach($ID_PLANTS as $ID_PLANT):	
					$r = @$this->treport->dashboard_daily($data['STARTDATE'],$data['ENDDATE'],$ID_GROUPAREA,$ID_COMPANY,$ID_PLANT);#echo $this->treport->get_sql();
					$nm_company 				 = $this->m_company->get_data_by_id($ID_COMPANY)->NM_COMPANY;
					
					$status = "D";
					IF($r->JML_DATA < $r->JML_HARI) $status = "NC";
					if($r->JML_DATA <= 0) $status = "NY";
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($ID_PLANT)->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
					$i++;
					
				endforeach;
			endforeach;
		endforeach; #
		$this->list_data = $d;
		$this->template->nostyle("v_dashboard_daily");
	}
	
	public function hourly(){		
		
		
		$plants = $this->input->post("ID_PLANT");
		$data   = $this->input->post();
		
		if(!$data or !$plants){
			die("No data selected");
		}
		
		foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
			$this->list_grouparea[] = $this->m_grouparea->get_data_by_id($ID_GROUPAREA);
		endforeach;
		
		foreach($plants as $ID_COMPANY => $ID_PLANTS):
			foreach($this->input->post("LIST_GROUPAREA") as $ID_GROUPAREA):
				$i = 0;
				foreach($ID_PLANTS as $ID_PLANT):	
					$r = @$this->treport->dashboard_hourly($data['STARTDATE'],$data['ENDDATE'],$ID_GROUPAREA,$ID_COMPANY,$ID_PLANT);
					$nm_company 				 = $this->m_company->get_data_by_id($ID_COMPANY)->NM_COMPANY;
					
					$status = "D";
					IF($r->JML_DATA < $r->JML_JAM) $status = "NC";
					if($r->JML_DATA <= 0) $status = "NY";
					
					$NM_PLANT 									= $this->m_plant->get_data_by_id($ID_PLANT)->NM_PLANT;
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_DATA] 	= ($r->TANGGAL_DATA)?$r->TANGGAL_DATA:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][TANGGAL_ENTRI] = ($r->TANGGAL_ENTRI)?$r->TANGGAL_ENTRI:"-";
					$d[$NM_PLANT][$ID_GROUPAREA][STATUS] 		= $status; 
					
					$i++;
					
				endforeach;
			endforeach;
		endforeach; #
		$this->list_data = $d;
		$this->template->nostyle("v_dashboard_daily");
	}
	
	
	public function access_log(){
		$this->load->helper("string");
		$this->load->helper("color");
		$this->load->model("m_access_log");
		$this->list_company = $this->m_company->list_company();
		$this->topGroupmenu = $this->m_access_log->topGroupmenu(date("m-Y"));
		$this->topMenu 			= $this->m_access_log->topMenu(date("m-Y"));
		$this->topUserList  = $this->m_access_log->topUser();
		$this->template->adminlte("v_access_log_dashboard");
	}
	
}
