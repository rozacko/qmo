<?php

class User_level extends QMUser {
	
	public $list_data = array();
	public $data_user_level;
	
	public function __construct(){
		parent::__construct();
		$this->load->model("m_user_level");
	}
	
	public function index(){
		$this->list_user_level = $this->m_user_level->datalist();
		$this->template->adminlte("v_user_level");
	}
	
	public function add(){
		$this->template->adminlte("v_user_level_add");
	}
	
	public function create(){
		$this->m_user_level->insert($this->input->post());
		if($this->m_user_level->error()){
			$this->notice->error($this->m_user_level->error());
			redirect("user_level/add");
		}
		else{
			$this->notice->success("Level User Data Saved.");
			redirect("user_level");
		}
	}
	
	public function edit($ID_LEVEL_USER){
		$this->data_user_level = $this->m_user_level->get_data_by_id($ID_LEVEL_USER);
		$this->template->adminlte("v_user_level_edit");
	}
	
	public function update($ID_LEVEL_USER){
		$this->m_user_level->update($this->input->post(),$ID_LEVEL_USER);
		if($this->m_user_level->error()){
			$this->notice->error($this->m_user_level->error());
			redirect("user_level/edit/".$ID_LEVEL_USER);
		}
		else{
			$this->notice->success("Level User Data Updated.");
			redirect("user_level");
		}
	}
	
	public function delete($ID_LEVEL_USER){
		$this->m_user_level->delete($ID_LEVEL_USER);
		if($this->m_user_level->error()){
			$this->notice->error($this->m_user_level->error());
		}
		else{
			$this->notice->success("Level User Data Removed.");
		}
		redirect("user_level");
	}
}	

?>
