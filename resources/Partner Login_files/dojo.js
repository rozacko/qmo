(function(){
var _1=null;
if((_1||(typeof djConfig!="undefined"&&djConfig.scopeMap))&&(typeof window!="undefined")){
var _2="",_3="",_4="",_5={},_6={};
_1=_1||djConfig.scopeMap;
for(var i=0;i<_1.length;i++){
var _8=_1[i];
_2+="var "+_8[0]+" = {}; "+_8[1]+" = "+_8[0]+";"+_8[1]+"._scopeName = '"+_8[1]+"';";
_3+=(i==0?"":",")+_8[0];
_4+=(i==0?"":",")+_8[1];
_5[_8[0]]=_8[1];
_6[_8[1]]=_8[0];
}
eval(_2+"dojo._scopeArgs = ["+_4+"];");
dojo._scopePrefixArgs=_3;
dojo._scopePrefix="(function("+_3+"){";
dojo._scopeSuffix="})("+_4+")";
dojo._scopeMap=_5;
dojo._scopeMapRev=_6;
}
(function(){
if(typeof this["loadFirebugConsole"]=="function"){
this["loadFirebugConsole"]();
}else{
this.console=this.console||{};
var cn=["assert","count","debug","dir","dirxml","error","group","groupEnd","info","profile","profileEnd","time","timeEnd","trace","warn","log"];
var i=0,tn;
while((tn=cn[i++])){
if(!console[tn]){
(function(){
var _c=tn+"";
console[_c]=("log" in console)?function(){
var a=Array.apply({},arguments);
a.unshift(_c+":");
console["log"](a.join(" "));
}:function(){
};
})();
}
}
}
if(typeof dojo=="undefined"){
this.dojo={_scopeName:"dojo",_scopePrefix:"",_scopePrefixArgs:"",_scopeSuffix:"",_scopeMap:{},_scopeMapRev:{}};
}
var d=dojo;
if(typeof dijit=="undefined"){
this.dijit={_scopeName:"dijit"};
}
if(typeof dojox=="undefined"){
this.dojox={_scopeName:"dojox"};
}
if(!d._scopeArgs){
d._scopeArgs=[dojo,dijit,dojox];
}
d.global=this;
d.config={isDebug:false,debugAtAllCosts:false};
if(typeof djConfig!="undefined"){
for(var _f in djConfig){
d.config[_f]=djConfig[_f];
}
}
dojo.locale=d.config.locale;
var rev="$Rev: 18832 $".match(/\d+/);
dojo.version={major:0,minor:0,patch:0,flag:"dev",revision:rev?+rev[0]:NaN,toString:function(){
with(d.version){
return major+"."+minor+"."+patch+flag+" ("+revision+")";
}
}};
if(typeof OpenAjax!="undefined"){
OpenAjax.hub.registerLibrary(dojo._scopeName,"http://dojotoolkit.org",d.version.toString());
}
var _11={};
dojo._mixin=function(obj,_13){
for(var x in _13){
if(_11[x]===undefined||_11[x]!=_13[x]){
obj[x]=_13[x];
}
}
if(d.isIE&&_13){
var p=_13.toString;
if(typeof p=="function"&&p!=obj.toString&&p!=_11.toString&&p!="\nfunction toString() {\n    [native code]\n}\n"){
obj.toString=_13.toString;
}
}
return obj;
};
dojo.mixin=function(obj,_17){
if(!obj){
obj={};
}
for(var i=1,l=arguments.length;i<l;i++){
d._mixin(obj,arguments[i]);
}
return obj;
};
dojo._getProp=function(_1a,_1b,_1c){
var obj=_1c||d.global;
for(var i=0,p;obj&&(p=_1a[i]);i++){
if(i==0&&this._scopeMap[p]){
p=this._scopeMap[p];
}
obj=(p in obj?obj[p]:(_1b?obj[p]={}:undefined));
}
return obj;
};
dojo.setObject=function(_20,_21,_22){
var _23=_20.split("."),p=_23.pop(),obj=d._getProp(_23,true,_22);
return obj&&p?(obj[p]=_21):undefined;
};
dojo.getObject=function(_26,_27,_28){
return d._getProp(_26.split("."),_27,_28);
};
dojo.exists=function(_29,obj){
return !!d.getObject(_29,false,obj);
};
dojo["eval"]=function(_2b){
return d.global.eval?d.global.eval(_2b):eval(_2b);
};
d.deprecated=d.experimental=function(){
};
})();
(function(){
var d=dojo;
d.mixin(d,{_loadedModules:{},_inFlightCount:0,_hasResource:{},_modulePrefixes:{dojo:{name:"dojo",value:"."},doh:{name:"doh",value:"../util/doh"},tests:{name:"tests",value:"tests"}},_moduleHasPrefix:function(_2d){
var mp=this._modulePrefixes;
return !!(mp[_2d]&&mp[_2d].value);
},_getModulePrefix:function(_2f){
var mp=this._modulePrefixes;
if(this._moduleHasPrefix(_2f)){
return mp[_2f].value;
}
return _2f;
},_loadedUrls:[],_postLoad:false,_loaders:[],_unloaders:[],_loadNotifying:false});
dojo._loadPath=function(_31,_32,cb){
var uri=((_31.charAt(0)=="/"||_31.match(/^\w+:/))?"":this.baseUrl)+_31;
try{
return !_32?this._loadUri(uri,cb):this._loadUriAndCheck(uri,_32,cb);
}
catch(e){
console.error(e);
return false;
}
};
dojo._loadUri=function(uri,cb){
if(this._loadedUrls[uri]){
return true;
}
var _37=this._getText(uri,true);
if(!_37){
return false;
}
this._loadedUrls[uri]=true;
this._loadedUrls.push(uri);
if(cb){
_37="("+_37+")";
}else{
_37=this._scopePrefix+_37+this._scopeSuffix;
}
if(d.isMoz){
_37+="\r\n//@ sourceURL="+uri;
}
var _38=d["eval"](_37);
if(cb){
cb(_38);
}
return true;
};
dojo._loadUriAndCheck=function(uri,_3a,cb){
var ok=false;
try{
ok=this._loadUri(uri,cb);
}
catch(e){
console.error("failed loading "+uri+" with error: "+e);
}
return !!(ok&&this._loadedModules[_3a]);
};
dojo.loaded=function(){
this._loadNotifying=true;
this._postLoad=true;
var mll=d._loaders;
this._loaders=[];
for(var x=0;x<mll.length;x++){
mll[x]();
}
this._loadNotifying=false;
if(d._postLoad&&d._inFlightCount==0&&mll.length){
d._callLoaded();
}
};
dojo.unloaded=function(){
var mll=d._unloaders;
while(mll.length){
(mll.pop())();
}
};
d._onto=function(arr,obj,fn){
if(!fn){
arr.push(obj);
}else{
if(fn){
var _43=(typeof fn=="string")?obj[fn]:fn;
arr.push(function(){
_43.call(obj);
});
}
}
};
dojo.addOnLoad=function(obj,_45){
d._onto(d._loaders,obj,_45);
if(d._postLoad&&d._inFlightCount==0&&!d._loadNotifying){
d._callLoaded();
}
};
var dca=d.config.addOnLoad;
if(dca){
d.addOnLoad[(dca instanceof Array?"apply":"call")](d,dca);
}
dojo._modulesLoaded=function(){
if(d._postLoad){
return;
}
if(d._inFlightCount>0){
console.warn("files still in flight!");
return;
}
d._callLoaded();
};
dojo._callLoaded=function(){
if(typeof setTimeout=="object"||(dojo.config.useXDomain&&d.isOpera)){
if(dojo.isAIR){
setTimeout(function(){
dojo.loaded();
},0);
}else{
setTimeout(dojo._scopeName+".loaded();",0);
}
}else{
d.loaded();
}
};
dojo._getModuleSymbols=function(_47){
var _48=_47.split(".");
for(var i=_48.length;i>0;i--){
var _4a=_48.slice(0,i).join(".");
if((i==1)&&!this._moduleHasPrefix(_4a)){
_48[0]="../"+_48[0];
}else{
var _4b=this._getModulePrefix(_4a);
if(_4b!=_4a){
_48.splice(0,i,_4b);
break;
}
}
}
return _48;
};
dojo._global_omit_module_check=false;
dojo.loadInit=function(_4c){
_4c();
};
dojo._loadModule=dojo.require=function(_4d,_4e){
_4e=this._global_omit_module_check||_4e;
var _4f=this._loadedModules[_4d];
if(_4f){
return _4f;
}
var _50=this._getModuleSymbols(_4d).join("/")+".js";
var _51=(!_4e)?_4d:null;
var ok=this._loadPath(_50,_51);
if(!ok&&!_4e){
throw new Error("Could not load '"+_4d+"'; last tried '"+_50+"'");
}
if(!_4e&&!this._isXDomain){
_4f=this._loadedModules[_4d];
if(!_4f){
throw new Error("symbol '"+_4d+"' is not defined after loading '"+_50+"'");
}
}
return _4f;
};
dojo.provide=function(_53){
_53=_53+"";
return (d._loadedModules[_53]=d.getObject(_53,true));
};
dojo.platformRequire=function(_54){
var _55=_54.common||[];
var _56=_55.concat(_54[d._name]||_54["default"]||[]);
for(var x=0;x<_56.length;x++){
var _58=_56[x];
if(_58.constructor==Array){
d._loadModule.apply(d,_58);
}else{
d._loadModule(_58);
}
}
};
dojo.requireIf=function(_59,_5a){
if(_59===true){
var _5b=[];
for(var i=1;i<arguments.length;i++){
_5b.push(arguments[i]);
}
d.require.apply(d,_5b);
}
};
dojo.requireAfterIf=d.requireIf;
dojo.registerModulePath=function(_5d,_5e){
d._modulePrefixes[_5d]={name:_5d,value:_5e};
};
dojo.requireLocalization=function(_5f,_60,_61,_62){
d.require("dojo.i18n");
d.i18n._requireLocalization.apply(d.hostenv,arguments);
};
var ore=new RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?$");
var ire=new RegExp("^((([^\\[:]+):)?([^@]+)@)?(\\[([^\\]]+)\\]|([^\\[:]*))(:([0-9]+))?$");
dojo._Url=function(){
var n=null;
var _a=arguments;
var uri=[_a[0]];
for(var i=1;i<_a.length;i++){
if(!_a[i]){
continue;
}
var _69=new d._Url(_a[i]+"");
var _6a=new d._Url(uri[0]+"");
if(_69.path==""&&!_69.scheme&&!_69.authority&&!_69.query){
if(_69.fragment!=n){
_6a.fragment=_69.fragment;
}
_69=_6a;
}else{
if(!_69.scheme){
_69.scheme=_6a.scheme;
if(!_69.authority){
_69.authority=_6a.authority;
if(_69.path.charAt(0)!="/"){
var _6b=_6a.path.substring(0,_6a.path.lastIndexOf("/")+1)+_69.path;
var _6c=_6b.split("/");
for(var j=0;j<_6c.length;j++){
if(_6c[j]=="."){
if(j==_6c.length-1){
_6c[j]="";
}else{
_6c.splice(j,1);
j--;
}
}else{
if(j>0&&!(j==1&&_6c[0]=="")&&_6c[j]==".."&&_6c[j-1]!=".."){
if(j==(_6c.length-1)){
_6c.splice(j,1);
_6c[j-1]="";
}else{
_6c.splice(j-1,2);
j-=2;
}
}
}
}
_69.path=_6c.join("/");
}
}
}
}
uri=[];
if(_69.scheme){
uri.push(_69.scheme,":");
}
if(_69.authority){
uri.push("//",_69.authority);
}
uri.push(_69.path);
if(_69.query){
uri.push("?",_69.query);
}
if(_69.fragment){
uri.push("#",_69.fragment);
}
}
this.uri=uri.join("");
var r=this.uri.match(ore);
this.scheme=r[2]||(r[1]?"":n);
this.authority=r[4]||(r[3]?"":n);
this.path=r[5];
this.query=r[7]||(r[6]?"":n);
this.fragment=r[9]||(r[8]?"":n);
if(this.authority!=n){
r=this.authority.match(ire);
this.user=r[3]||n;
this.password=r[4]||n;
this.host=r[6]||r[7];
this.port=r[9]||n;
}
};
dojo._Url.prototype.toString=function(){
return this.uri;
};
dojo.moduleUrl=function(_6f,url){
var loc=d._getModuleSymbols(_6f).join("/");
if(!loc){
return null;
}
if(loc.lastIndexOf("/")!=loc.length-1){
loc+="/";
}
var _72=loc.indexOf(":");
if(loc.charAt(0)!="/"&&(_72==-1||_72>loc.indexOf("/"))){
loc=d.baseUrl+loc;
}
return new d._Url(loc,url);
};
})();
if(typeof window!="undefined"){
dojo.isBrowser=true;
dojo._name="browser";
(function(){
var d=dojo;
if(document&&document.getElementsByTagName){
var _74=document.getElementsByTagName("script");
var _75=/dojo(\.xd)?\.js(\W|$)/i;
for(var i=0;i<_74.length;i++){
var src=_74[i].getAttribute("src");
if(!src){
continue;
}
var m=src.match(_75);
if(m){
if(!d.config.baseUrl){
d.config.baseUrl=src.substring(0,m.index);
}
var cfg=_74[i].getAttribute("djConfig");
if(cfg){
var _7a=eval("({ "+cfg+" })");
for(var x in _7a){
dojo.config[x]=_7a[x];
}
}
break;
}
}
}
d.baseUrl=d.config.baseUrl;
var n=navigator;
var dua=n.userAgent,dav=n.appVersion,tv=parseFloat(dav);
if(dua.indexOf("Opera")>=0){
d.isOpera=tv;
}
if(dua.indexOf("AdobeAIR")>=0){
d.isAIR=1;
}
d.isKhtml=(dav.indexOf("Konqueror")>=0)?tv:0;
d.isWebKit=parseFloat(dua.split("WebKit/")[1])||undefined;
d.isChrome=parseFloat(dua.split("Chrome/")[1])||undefined;
var _80=Math.max(dav.indexOf("WebKit"),dav.indexOf("Safari"),0);
if(_80&&!dojo.isChrome){
d.isSafari=parseFloat(dav.split("Version/")[1]);
if(!d.isSafari||parseFloat(dav.substr(_80+7))<=419.3){
d.isSafari=2;
}
}
if(dua.indexOf("Gecko")>=0&&!d.isKhtml&&!d.isWebKit){
d.isMozilla=d.isMoz=tv;
}
if(d.isMoz){
d.isFF=parseFloat(dua.split("Firefox/")[1]||dua.split("Minefield/")[1]||dua.split("Shiretoko/")[1])||undefined;
}
if(document.all&&!d.isOpera){
d.isIE=parseFloat(dav.split("MSIE ")[1])||undefined;
if(d.isIE>=8&&document.documentMode!=5){
d.isIE=document.documentMode;
}
}
if(dojo.isIE&&window.location.protocol==="file:"){
dojo.config.ieForceActiveXXhr=true;
}
var cm=document.compatMode;
d.isQuirks=cm=="BackCompat"||cm=="QuirksMode"||d.isIE<6;
d.locale=dojo.config.locale||(d.isIE?n.userLanguage:n.language).toLowerCase();
d._XMLHTTP_PROGIDS=["Msxml2.XMLHTTP","Microsoft.XMLHTTP","Msxml2.XMLHTTP.4.0"];
d._xhrObj=function(){
var _82,_83;
if(!dojo.isIE||!dojo.config.ieForceActiveXXhr){
try{
_82=new XMLHttpRequest();
}
catch(e){
}
}
if(!_82){
for(var i=0;i<3;++i){
var _85=d._XMLHTTP_PROGIDS[i];
try{
_82=new ActiveXObject(_85);
}
catch(e){
_83=e;
}
if(_82){
d._XMLHTTP_PROGIDS=[_85];
break;
}
}
}
if(!_82){
throw new Error("XMLHTTP not available: "+_83);
}
return _82;
};
d._isDocumentOk=function(_86){
var _87=_86.status||0;
return (_87>=200&&_87<300)||_87==304||_87==1223||(!_87&&(location.protocol=="file:"||location.protocol=="chrome:"));
};
var _88=window.location+"";
var _89=document.getElementsByTagName("base");
var _8a=(_89&&_89.length>0);
d._getText=function(uri,_8c){
var _8d=this._xhrObj();
if(!_8a&&dojo._Url){
uri=(new dojo._Url(_88,uri)).toString();
}
if(d.config.cacheBust){
uri+="";
uri+=(uri.indexOf("?")==-1?"?":"&")+String(d.config.cacheBust).replace(/\W+/g,"");
}
_8d.open("GET",uri,false);
try{
_8d.send(null);
if(!d._isDocumentOk(_8d)){
var err=Error("Unable to load "+uri+" status:"+_8d.status);
err.status=_8d.status;
err.responseText=_8d.responseText;
throw err;
}
}
catch(e){
if(_8c){
return null;
}
throw e;
}
return _8d.responseText;
};
var _w=window;
var _90=function(_91,fp){
var _93=_w[_91]||function(){
};
_w[_91]=function(){
fp.apply(_w,arguments);
_93.apply(_w,arguments);
};
};
d._windowUnloaders=[];
d.windowUnloaded=function(){
var mll=d._windowUnloaders;
while(mll.length){
(mll.pop())();
}
};
var _95=0;
d.addOnWindowUnload=function(obj,_97){
d._onto(d._windowUnloaders,obj,_97);
if(!_95){
_95=1;
_90("onunload",d.windowUnloaded);
}
};
var _98=0;
d.addOnUnload=function(obj,_9a){
d._onto(d._unloaders,obj,_9a);
if(!_98){
_98=1;
_90("onbeforeunload",dojo.unloaded);
}
};
})();
dojo._initFired=false;
dojo._loadInit=function(e){
dojo._initFired=true;
var _9c=e&&e.type?e.type.toLowerCase():"load";
if(arguments.callee.initialized||(_9c!="domcontentloaded"&&_9c!="load")){
return;
}
arguments.callee.initialized=true;
if("_khtmlTimer" in dojo){
clearInterval(dojo._khtmlTimer);
delete dojo._khtmlTimer;
}
if(dojo._inFlightCount==0){
dojo._modulesLoaded();
}
};
if(!dojo.config.afterOnLoad){
if(document.addEventListener){
if(dojo.isWebKit>525||dojo.isOpera||dojo.isFF>=3||(dojo.isMoz&&dojo.config.enableMozDomContentLoaded===true)){
document.addEventListener("DOMContentLoaded",dojo._loadInit,null);
}
window.addEventListener("load",dojo._loadInit,null);
}
if(dojo.isAIR){
window.addEventListener("load",dojo._loadInit,null);
}else{
if((dojo.isWebKit<525)||dojo.isKhtml){
dojo._khtmlTimer=setInterval(function(){
if(/loaded|complete/.test(document.readyState)){
dojo._loadInit();
}
},10);
}
}
}
if(dojo.isIE){
if(!dojo.config.afterOnLoad){
document.write("<scr"+"ipt defer src=\"//:\" "+"onreadystatechange=\"if(this.readyState=='complete'){"+dojo._scopeName+"._loadInit();}\">"+"</scr"+"ipt>");
}
try{
document.namespaces.add("v","urn:schemas-microsoft-com:vml");
document.createStyleSheet().addRule("v\\:*","behavior:url(#default#VML);  display:inline-block");
}
catch(e){
}
}
}
(function(){
var mp=dojo.config["modulePaths"];
if(mp){
for(var _9e in mp){
dojo.registerModulePath(_9e,mp[_9e]);
}
}
})();
if(dojo.config.isDebug){
dojo.require("dojo._firebug.firebug");
}
if(dojo.config.debugAtAllCosts){
dojo.config.useXDomain=true;
dojo.require("dojo._base._loader.loader_xd");
dojo.require("dojo._base._loader.loader_debug");
dojo.require("dojo.i18n");
}
if(!dojo._hasResource["dojo._base.lang"]){
dojo._hasResource["dojo._base.lang"]=true;
dojo.provide("dojo._base.lang");
dojo.isString=function(it){
return !!arguments.length&&it!=null&&(typeof it=="string"||it instanceof String);
};
dojo.isArray=function(it){
return it&&(it instanceof Array||typeof it=="array");
};
dojo.isFunction=(function(){
var _a1=function(it){
var t=typeof it;
return it&&(t=="function"||it instanceof Function);
};
return dojo.isSafari?function(it){
if(typeof it=="function"&&it=="[object NodeList]"){
return false;
}
return _a1(it);
}:_a1;
})();
dojo.isObject=function(it){
return it!==undefined&&(it===null||typeof it=="object"||dojo.isArray(it)||dojo.isFunction(it));
};
dojo.isArrayLike=function(it){
var d=dojo;
return it&&it!==undefined&&!d.isString(it)&&!d.isFunction(it)&&!(it.tagName&&it.tagName.toLowerCase()=="form")&&(d.isArray(it)||isFinite(it.length));
};
dojo.isAlien=function(it){
return it&&!dojo.isFunction(it)&&/\{\s*\[native code\]\s*\}/.test(String(it));
};
dojo.extend=function(_a9,_aa){
for(var i=1,l=arguments.length;i<l;i++){
dojo._mixin(_a9.prototype,arguments[i]);
}
return _a9;
};
dojo._hitchArgs=function(_ad,_ae){
var pre=dojo._toArray(arguments,2);
var _b0=dojo.isString(_ae);
return function(){
var _b1=dojo._toArray(arguments);
var f=_b0?(_ad||dojo.global)[_ae]:_ae;
return f&&f.apply(_ad||this,pre.concat(_b1));
};
};
dojo.hitch=function(_b3,_b4){
if(arguments.length>2){
return dojo._hitchArgs.apply(dojo,arguments);
}
if(!_b4){
_b4=_b3;
_b3=null;
}
if(dojo.isString(_b4)){
_b3=_b3||dojo.global;
if(!_b3[_b4]){
throw (["dojo.hitch: scope[\"",_b4,"\"] is null (scope=\"",_b3,"\")"].join(""));
}
return function(){
return _b3[_b4].apply(_b3,arguments||[]);
};
}
return !_b3?_b4:function(){
return _b4.apply(_b3,arguments||[]);
};
};
dojo.delegate=dojo._delegate=(function(){
function TMP(){
};
return function(obj,_b7){
TMP.prototype=obj;
var tmp=new TMP();
if(_b7){
dojo._mixin(tmp,_b7);
}
return tmp;
};
})();
(function(){
var _b9=function(obj,_bb,_bc){
return (_bc||[]).concat(Array.prototype.slice.call(obj,_bb||0));
};
var _bd=function(obj,_bf,_c0){
var arr=_c0||[];
for(var x=_bf||0;x<obj.length;x++){
arr.push(obj[x]);
}
return arr;
};
dojo._toArray=dojo.isIE?function(obj){
return ((obj.item)?_bd:_b9).apply(this,arguments);
}:_b9;
})();
dojo.partial=function(_c4){
var arr=[null];
return dojo.hitch.apply(dojo,arr.concat(dojo._toArray(arguments)));
};
dojo.clone=function(o){
if(!o){
return o;
}
if(dojo.isArray(o)){
var r=[];
for(var i=0;i<o.length;++i){
r.push(dojo.clone(o[i]));
}
return r;
}
if(!dojo.isObject(o)){
return o;
}
if(o.nodeType&&o.cloneNode){
return o.cloneNode(true);
}
if(o instanceof Date){
return new Date(o.getTime());
}
r=new o.constructor();
for(i in o){
if(!(i in r)||r[i]!=o[i]){
r[i]=dojo.clone(o[i]);
}
}
return r;
};
dojo.trim=String.prototype.trim?function(str){
return str.trim();
}:function(str){
return str.replace(/^\s\s*/,"").replace(/\s\s*$/,"");
};
}
if(!dojo._hasResource["dojo._base.declare"]){
dojo._hasResource["dojo._base.declare"]=true;
dojo.provide("dojo._base.declare");
dojo.declare=function(_cb,_cc,_cd){
var dd=arguments.callee,_cf;
if(dojo.isArray(_cc)){
_cf=_cc;
_cc=_cf.shift();
}
if(_cf){
dojo.forEach(_cf,function(m,i){
if(!m){
throw (_cb+": mixin #"+i+" is null");
}
_cc=dd._delegate(_cc,m);
});
}
var _d2=dd._delegate(_cc);
_cd=_cd||{};
_d2.extend(_cd);
dojo.extend(_d2,{declaredClass:_cb,_constructor:_cd.constructor});
_d2.prototype.constructor=_d2;
return dojo.setObject(_cb,_d2);
};
dojo.mixin(dojo.declare,{_delegate:function(_d3,_d4){
var bp=(_d3||0).prototype,mp=(_d4||0).prototype,dd=dojo.declare;
var _d8=dd._makeCtor();
dojo.mixin(_d8,{superclass:bp,mixin:mp,extend:dd._extend});
if(_d3){
_d8.prototype=dojo._delegate(bp);
}
dojo.extend(_d8,dd._core,mp||0,{_constructor:null,preamble:null});
_d8.prototype.constructor=_d8;
_d8.prototype.declaredClass=(bp||0).declaredClass+"_"+(mp||0).declaredClass;
return _d8;
},_extend:function(_d9){
var i,fn;
for(i in _d9){
if(dojo.isFunction(fn=_d9[i])&&!0[i]){
fn.nom=i;
fn.ctor=this;
}
}
dojo.extend(this,_d9);
},_makeCtor:function(){
return function(){
this._construct(arguments);
};
},_core:{_construct:function(_dc){
var c=_dc.callee,s=c.superclass,ct=s&&s.constructor,m=c.mixin,mct=m&&m.constructor,a=_dc,ii,fn;
if(a[0]){
if(((fn=a[0].preamble))){
a=fn.apply(this,a)||a;
}
}
if((fn=c.prototype.preamble)){
a=fn.apply(this,a)||a;
}
if(ct&&ct.apply){
ct.apply(this,a);
}
if(mct&&mct.apply){
mct.apply(this,a);
}
if((ii=c.prototype._constructor)){
ii.apply(this,_dc);
}
if(this.constructor.prototype==c.prototype&&(ct=this.postscript)){
ct.apply(this,_dc);
}
},_findMixin:function(_e5){
var c=this.constructor,p,m;
while(c){
p=c.superclass;
m=c.mixin;
if(m==_e5||(m instanceof _e5.constructor)){
return p;
}
if(m&&m._findMixin&&(m=m._findMixin(_e5))){
return m;
}
c=p&&p.constructor;
}
},_findMethod:function(_e9,_ea,_eb,has){
var p=_eb,c,m,f;
do{
c=p.constructor;
m=c.mixin;
if(m&&(m=this._findMethod(_e9,_ea,m,has))){
return m;
}
if((f=p[_e9])&&(has==(f==_ea))){
return p;
}
p=c.superclass;
}while(p);
return !has&&(p=this._findMixin(_eb))&&this._findMethod(_e9,_ea,p,has);
},inherited:function(_f1,_f2,_f3){
var a=arguments;
if(!dojo.isString(a[0])){
_f3=_f2;
_f2=_f1;
_f1=_f2.callee.nom;
}
a=_f3||_f2;
var c=_f2.callee,p=this.constructor.prototype,fn,mp;
if(this[_f1]!=c||p[_f1]==c){
mp=(c.ctor||0).superclass||this._findMethod(_f1,c,p,true);
if(!mp){
throw (this.declaredClass+": inherited method \""+_f1+"\" mismatch");
}
p=this._findMethod(_f1,c,mp,false);
}
fn=p&&p[_f1];
if(!fn){
throw (mp.declaredClass+": inherited method \""+_f1+"\" not found");
}
return fn.apply(this,a);
}}});
}
if(!dojo._hasResource["dojo._base.connect"]){
dojo._hasResource["dojo._base.connect"]=true;
dojo.provide("dojo._base.connect");
dojo._listener={getDispatcher:function(){
return function(){
var ap=Array.prototype,c=arguments.callee,ls=c._listeners,t=c.target;
var r=t&&t.apply(this,arguments);
var lls;
lls=[].concat(ls);
for(var i in lls){
if(!(i in ap)){
lls[i].apply(this,arguments);
}
}
return r;
};
},add:function(_100,_101,_102){
_100=_100||dojo.global;
var f=_100[_101];
if(!f||!f._listeners){
var d=dojo._listener.getDispatcher();
d.target=f;
d._listeners=[];
f=_100[_101]=d;
}
return f._listeners.push(_102);
},remove:function(_105,_106,_107){
var f=(_105||dojo.global)[_106];
if(f&&f._listeners&&_107--){
delete f._listeners[_107];
}
}};
dojo.connect=function(obj,_10a,_10b,_10c,_10d){
var a=arguments,args=[],i=0;
args.push(dojo.isString(a[0])?null:a[i++],a[i++]);
var a1=a[i+1];
args.push(dojo.isString(a1)||dojo.isFunction(a1)?a[i++]:null,a[i++]);
for(var l=a.length;i<l;i++){
args.push(a[i]);
}
return dojo._connect.apply(this,args);
};
dojo._connect=function(obj,_113,_114,_115){
var l=dojo._listener,h=l.add(obj,_113,dojo.hitch(_114,_115));
return [obj,_113,h,l];
};
dojo.disconnect=function(_118){
if(_118&&_118[0]!==undefined){
dojo._disconnect.apply(this,_118);
delete _118[0];
}
};
dojo._disconnect=function(obj,_11a,_11b,_11c){
_11c.remove(obj,_11a,_11b);
};
dojo._topics={};
dojo.subscribe=function(_11d,_11e,_11f){
return [_11d,dojo._listener.add(dojo._topics,_11d,dojo.hitch(_11e,_11f))];
};
dojo.unsubscribe=function(_120){
if(_120){
dojo._listener.remove(dojo._topics,_120[0],_120[1]);
}
};
dojo.publish=function(_121,args){
var f=dojo._topics[_121];
if(f){
f.apply(this,args||[]);
}
};
dojo.connectPublisher=function(_124,obj,_126){
var pf=function(){
dojo.publish(_124,arguments);
};
return (_126)?dojo.connect(obj,_126,pf):dojo.connect(obj,pf);
};
}
if(!dojo._hasResource["dojo._base.Deferred"]){
dojo._hasResource["dojo._base.Deferred"]=true;
dojo.provide("dojo._base.Deferred");
dojo.Deferred=function(_128){
this.chain=[];
this.id=this._nextId();
this.fired=-1;
this.paused=0;
this.results=[null,null];
this.canceller=_128;
this.silentlyCancelled=false;
};
dojo.extend(dojo.Deferred,{_nextId:(function(){
var n=1;
return function(){
return n++;
};
})(),cancel:function(){
var err;
if(this.fired==-1){
if(this.canceller){
err=this.canceller(this);
}else{
this.silentlyCancelled=true;
}
if(this.fired==-1){
if(!(err instanceof Error)){
var res=err;
var msg="Deferred Cancelled";
if(err&&err.toString){
msg+=": "+err.toString();
}
err=new Error(msg);
err.dojoType="cancel";
err.cancelResult=res;
}
this.errback(err);
}
}else{
if((this.fired==0)&&(this.results[0] instanceof dojo.Deferred)){
this.results[0].cancel();
}
}
},_resback:function(res){
this.fired=((res instanceof Error)?1:0);
this.results[this.fired]=res;
this._fire();
},_check:function(){
if(this.fired!=-1){
if(!this.silentlyCancelled){
throw new Error("already called!");
}
this.silentlyCancelled=false;
return;
}
},callback:function(res){
this._check();
this._resback(res);
},errback:function(res){
this._check();
if(!(res instanceof Error)){
res=new Error(res);
}
this._resback(res);
},addBoth:function(cb,cbfn){
var _132=dojo.hitch.apply(dojo,arguments);
return this.addCallbacks(_132,_132);
},addCallback:function(cb,cbfn){
return this.addCallbacks(dojo.hitch.apply(dojo,arguments));
},addErrback:function(cb,cbfn){
return this.addCallbacks(null,dojo.hitch.apply(dojo,arguments));
},addCallbacks:function(cb,eb){
this.chain.push([cb,eb]);
if(this.fired>=0){
this._fire();
}
return this;
},_fire:function(){
var _139=this.chain;
var _13a=this.fired;
var res=this.results[_13a];
var self=this;
var cb=null;
while((_139.length>0)&&(this.paused==0)){
var f=_139.shift()[_13a];
if(!f){
continue;
}
var func=function(){
var ret=f(res);
if(typeof ret!="undefined"){
res=ret;
}
_13a=((res instanceof Error)?1:0);
if(res instanceof dojo.Deferred){
cb=function(res){
self._resback(res);
self.paused--;
if((self.paused==0)&&(self.fired>=0)){
self._fire();
}
};
this.paused++;
}
};
if(dojo.config.debugAtAllCosts){
func.call(this);
}else{
try{
func.call(this);
}
catch(err){
_13a=1;
res=err;
}
}
}
this.fired=_13a;
this.results[_13a]=res;
if((cb)&&(this.paused)){
res.addBoth(cb);
}
}});
}
if(!dojo._hasResource["dojo._base.json"]){
dojo._hasResource["dojo._base.json"]=true;
dojo.provide("dojo._base.json");
dojo.fromJson=function(json){
return eval("("+json+")");
};
dojo._escapeString=function(str){
return ("\""+str.replace(/(["\\])/g,"\\$1")+"\"").replace(/[\f]/g,"\\f").replace(/[\b]/g,"\\b").replace(/[\n]/g,"\\n").replace(/[\t]/g,"\\t").replace(/[\r]/g,"\\r");
};
dojo.toJsonIndentStr="\t";
dojo.toJson=function(it,_145,_146){
if(it===undefined){
return "undefined";
}
var _147=typeof it;
if(_147=="number"||_147=="boolean"){
return it+"";
}
if(it===null){
return "null";
}
if(dojo.isString(it)){
return dojo._escapeString(it);
}
var _148=arguments.callee;
var _149;
_146=_146||"";
var _14a=_145?_146+dojo.toJsonIndentStr:"";
var tf=it.__json__||it.json;
if(dojo.isFunction(tf)){
_149=tf.call(it);
if(it!==_149){
return _148(_149,_145,_14a);
}
}
if(it.nodeType&&it.cloneNode){
throw new Error("Can't serialize DOM nodes");
}
var sep=_145?" ":"";
var _14d=_145?"\n":"";
if(dojo.isArray(it)){
var res=dojo.map(it,function(obj){
var val=_148(obj,_145,_14a);
if(typeof val!="string"){
val="undefined";
}
return _14d+_14a+val;
});
return "["+res.join(","+sep)+_14d+_146+"]";
}
if(_147=="function"){
return null;
}
var _151=[],key;
for(key in it){
var _153,val;
if(typeof key=="number"){
_153="\""+key+"\"";
}else{
if(typeof key=="string"){
_153=dojo._escapeString(key);
}else{
continue;
}
}
val=_148(it[key],_145,_14a);
if(typeof val!="string"){
continue;
}
_151.push(_14d+_14a+_153+":"+sep+val);
}
return "{"+_151.join(","+sep)+_14d+_146+"}";
};
}
if(!dojo._hasResource["dojo._base.array"]){
dojo._hasResource["dojo._base.array"]=true;
dojo.provide("dojo._base.array");
(function(){
var _155=function(arr,obj,cb){
return [dojo.isString(arr)?arr.split(""):arr,obj||dojo.global,dojo.isString(cb)?new Function("item","index","array",cb):cb];
};
dojo.mixin(dojo,{indexOf:function(_159,_15a,_15b,_15c){
var step=1,end=_159.length||0,i=0;
if(_15c){
i=end-1;
step=end=-1;
}
if(_15b!=undefined){
i=_15b;
}
if((_15c&&i>end)||i<end){
for(;i!=end;i+=step){
if(_159[i]==_15a){
return i;
}
}
}
return -1;
},lastIndexOf:function(_15f,_160,_161){
return dojo.indexOf(_15f,_160,_161,true);
},forEach:function(arr,_163,_164){
if(!arr||!arr.length){
return;
}
var _p=_155(arr,_164,_163);
arr=_p[0];
for(var i=0,l=arr.length;i<l;++i){
_p[2].call(_p[1],arr[i],i,arr);
}
},_everyOrSome:function(_168,arr,_16a,_16b){
var _p=_155(arr,_16b,_16a);
arr=_p[0];
for(var i=0,l=arr.length;i<l;++i){
var _16f=!!_p[2].call(_p[1],arr[i],i,arr);
if(_168^_16f){
return _16f;
}
}
return _168;
},every:function(arr,_171,_172){
return dojo._everyOrSome(true,arr,_171,_172);
},some:function(arr,_174,_175){
return dojo._everyOrSome(false,arr,_174,_175);
},map:function(arr,_177,_178){
var _p=_155(arr,_178,_177);
arr=_p[0];
var _17a=(arguments[3]?(new arguments[3]()):[]);
for(var i=0,l=arr.length;i<l;++i){
_17a.push(_p[2].call(_p[1],arr[i],i,arr));
}
return _17a;
},filter:function(arr,_17e,_17f){
var _p=_155(arr,_17f,_17e);
arr=_p[0];
var _181=[];
for(var i=0,l=arr.length;i<l;++i){
if(_p[2].call(_p[1],arr[i],i,arr)){
_181.push(arr[i]);
}
}
return _181;
}});
})();
}
if(!dojo._hasResource["dojo._base.Color"]){
dojo._hasResource["dojo._base.Color"]=true;
dojo.provide("dojo._base.Color");
(function(){
var d=dojo;
dojo.Color=function(_185){
if(_185){
this.setColor(_185);
}
};
dojo.Color.named={black:[0,0,0],silver:[192,192,192],gray:[128,128,128],white:[255,255,255],maroon:[128,0,0],red:[255,0,0],purple:[128,0,128],fuchsia:[255,0,255],green:[0,128,0],lime:[0,255,0],olive:[128,128,0],yellow:[255,255,0],navy:[0,0,128],blue:[0,0,255],teal:[0,128,128],aqua:[0,255,255]};
dojo.extend(dojo.Color,{r:255,g:255,b:255,a:1,_set:function(r,g,b,a){
var t=this;
t.r=r;
t.g=g;
t.b=b;
t.a=a;
},setColor:function(_18b){
if(d.isString(_18b)){
d.colorFromString(_18b,this);
}else{
if(d.isArray(_18b)){
d.colorFromArray(_18b,this);
}else{
this._set(_18b.r,_18b.g,_18b.b,_18b.a);
if(!(_18b instanceof d.Color)){
this.sanitize();
}
}
}
return this;
},sanitize:function(){
return this;
},toRgb:function(){
var t=this;
return [t.r,t.g,t.b];
},toRgba:function(){
var t=this;
return [t.r,t.g,t.b,t.a];
},toHex:function(){
var arr=d.map(["r","g","b"],function(x){
var s=this[x].toString(16);
return s.length<2?"0"+s:s;
},this);
return "#"+arr.join("");
},toCss:function(_191){
var t=this,rgb=t.r+", "+t.g+", "+t.b;
return (_191?"rgba("+rgb+", "+t.a:"rgb("+rgb)+")";
},toString:function(){
return this.toCss(true);
}});
dojo.blendColors=function(_194,end,_196,obj){
var t=obj||new d.Color();
d.forEach(["r","g","b","a"],function(x){
t[x]=_194[x]+(end[x]-_194[x])*_196;
if(x!="a"){
t[x]=Math.round(t[x]);
}
});
return t.sanitize();
};
dojo.colorFromRgb=function(_19a,obj){
var m=_19a.toLowerCase().match(/^rgba?\(([\s\.,0-9]+)\)/);
return m&&dojo.colorFromArray(m[1].split(/\s*,\s*/),obj);
};
dojo.colorFromHex=function(_19d,obj){
var t=obj||new d.Color(),bits=(_19d.length==4)?4:8,mask=(1<<bits)-1;
_19d=Number("0x"+_19d.substr(1));
if(isNaN(_19d)){
return null;
}
d.forEach(["b","g","r"],function(x){
var c=_19d&mask;
_19d>>=bits;
t[x]=bits==4?17*c:c;
});
t.a=1;
return t;
};
dojo.colorFromArray=function(a,obj){
var t=obj||new d.Color();
t._set(Number(a[0]),Number(a[1]),Number(a[2]),Number(a[3]));
if(isNaN(t.a)){
t.a=1;
}
return t.sanitize();
};
dojo.colorFromString=function(str,obj){
var a=d.Color.named[str];
return a&&d.colorFromArray(a,obj)||d.colorFromRgb(str,obj)||d.colorFromHex(str,obj);
};
})();
}
if(!dojo._hasResource["dojo._base"]){
dojo._hasResource["dojo._base"]=true;
dojo.provide("dojo._base");
}
if(!dojo._hasResource["dojo._base.window"]){
dojo._hasResource["dojo._base.window"]=true;
dojo.provide("dojo._base.window");
dojo.doc=window["document"]||null;
dojo.body=function(){
return dojo.doc.body||dojo.doc.getElementsByTagName("body")[0];
};
dojo.setContext=function(_1aa,_1ab){
dojo.global=_1aa;
dojo.doc=_1ab;
};
dojo.withGlobal=function(_1ac,_1ad,_1ae,_1af){
var _1b0=dojo.global;
try{
dojo.global=_1ac;
return dojo.withDoc.call(null,_1ac.document,_1ad,_1ae,_1af);
}
finally{
dojo.global=_1b0;
}
};
dojo.withDoc=function(_1b1,_1b2,_1b3,_1b4){
var _1b5=dojo.doc,_1b6=dojo._bodyLtr;
try{
dojo.doc=_1b1;
delete dojo._bodyLtr;
if(_1b3&&dojo.isString(_1b2)){
_1b2=_1b3[_1b2];
}
return _1b2.apply(_1b3,_1b4||[]);
}
finally{
dojo.doc=_1b5;
if(_1b6!==undefined){
dojo._bodyLtr=_1b6;
}
}
};
}
if(!dojo._hasResource["dojo._base.event"]){
dojo._hasResource["dojo._base.event"]=true;
dojo.provide("dojo._base.event");
(function(){
var del=(dojo._event_listener={add:function(node,name,fp){
if(!node){
return;
}
name=del._normalizeEventName(name);
fp=del._fixCallback(name,fp);
var _1bb=name;
if(!dojo.isIE&&(name=="mouseenter"||name=="mouseleave")){
var ofp=fp;
name=(name=="mouseenter")?"mouseover":"mouseout";
fp=function(e){
if(dojo.isFF<=2){
try{
e.relatedTarget.tagName;
}
catch(e2){
return;
}
}
if(!dojo.isDescendant(e.relatedTarget,node)){
return ofp.call(this,e);
}
};
}
node.addEventListener(name,fp,false);
return fp;
},remove:function(node,_1bf,_1c0){
if(node){
_1bf=del._normalizeEventName(_1bf);
if(!dojo.isIE&&(_1bf=="mouseenter"||_1bf=="mouseleave")){
_1bf=(_1bf=="mouseenter")?"mouseover":"mouseout";
}
node.removeEventListener(_1bf,_1c0,false);
}
},_normalizeEventName:function(name){
return name.slice(0,2)=="on"?name.slice(2):name;
},_fixCallback:function(name,fp){
return name!="keypress"?fp:function(e){
return fp.call(this,del._fixEvent(e,this));
};
},_fixEvent:function(evt,_1c6){
switch(evt.type){
case "keypress":
del._setKeyChar(evt);
break;
}
return evt;
},_setKeyChar:function(evt){
evt.keyChar=evt.charCode?String.fromCharCode(evt.charCode):"";
evt.charOrCode=evt.keyChar||evt.keyCode;
},_punctMap:{106:42,111:47,186:59,187:43,188:44,189:45,190:46,191:47,192:96,219:91,220:92,221:93,222:39}});
dojo.fixEvent=function(evt,_1c9){
return del._fixEvent(evt,_1c9);
};
dojo.stopEvent=function(evt){
evt.preventDefault();
evt.stopPropagation();
};
var _1cb=dojo._listener;
dojo._connect=function(obj,_1cd,_1ce,_1cf,_1d0){
var _1d1=obj&&(obj.nodeType||obj.attachEvent||obj.addEventListener);
var lid=_1d1?(_1d0?2:1):0,l=[dojo._listener,del,_1cb][lid];
var h=l.add(obj,_1cd,dojo.hitch(_1ce,_1cf));
return [obj,_1cd,h,lid];
};
dojo._disconnect=function(obj,_1d6,_1d7,_1d8){
([dojo._listener,del,_1cb][_1d8]).remove(obj,_1d6,_1d7);
};
dojo.keys={BACKSPACE:8,TAB:9,CLEAR:12,ENTER:13,SHIFT:16,CTRL:17,ALT:18,PAUSE:19,CAPS_LOCK:20,ESCAPE:27,SPACE:32,PAGE_UP:33,PAGE_DOWN:34,END:35,HOME:36,LEFT_ARROW:37,UP_ARROW:38,RIGHT_ARROW:39,DOWN_ARROW:40,INSERT:45,DELETE:46,HELP:47,LEFT_WINDOW:91,RIGHT_WINDOW:92,SELECT:93,NUMPAD_0:96,NUMPAD_1:97,NUMPAD_2:98,NUMPAD_3:99,NUMPAD_4:100,NUMPAD_5:101,NUMPAD_6:102,NUMPAD_7:103,NUMPAD_8:104,NUMPAD_9:105,NUMPAD_MULTIPLY:106,NUMPAD_PLUS:107,NUMPAD_ENTER:108,NUMPAD_MINUS:109,NUMPAD_PERIOD:110,NUMPAD_DIVIDE:111,F1:112,F2:113,F3:114,F4:115,F5:116,F6:117,F7:118,F8:119,F9:120,F10:121,F11:122,F12:123,F13:124,F14:125,F15:126,NUM_LOCK:144,SCROLL_LOCK:145};
if(dojo.isIE){
var _1d9=function(e,code){
try{
return (e.keyCode=code);
}
catch(e){
return 0;
}
};
var iel=dojo._listener;
var _1dd=(dojo._ieListenersName="_"+dojo._scopeName+"_listeners");
if(!dojo.config._allow_leaks){
_1cb=iel=dojo._ie_listener={handlers:[],add:function(_1de,_1df,_1e0){
_1de=_1de||dojo.global;
var f=_1de[_1df];
if(!f||!f[_1dd]){
var d=dojo._getIeDispatcher();
d.target=f&&(ieh.push(f)-1);
d[_1dd]=[];
f=_1de[_1df]=d;
}
return f[_1dd].push(ieh.push(_1e0)-1);
},remove:function(_1e4,_1e5,_1e6){
var f=(_1e4||dojo.global)[_1e5],l=f&&f[_1dd];
if(f&&l&&_1e6--){
delete ieh[l[_1e6]];
delete l[_1e6];
}
}};
var ieh=iel.handlers;
}
dojo.mixin(del,{add:function(node,_1ea,fp){
if(!node){
return;
}
_1ea=del._normalizeEventName(_1ea);
if(_1ea=="onkeypress"){
var kd=node.onkeydown;
if(!kd||!kd[_1dd]||!kd._stealthKeydownHandle){
var h=del.add(node,"onkeydown",del._stealthKeyDown);
kd=node.onkeydown;
kd._stealthKeydownHandle=h;
kd._stealthKeydownRefs=1;
}else{
kd._stealthKeydownRefs++;
}
}
return iel.add(node,_1ea,del._fixCallback(fp));
},remove:function(node,_1ef,_1f0){
_1ef=del._normalizeEventName(_1ef);
iel.remove(node,_1ef,_1f0);
if(_1ef=="onkeypress"){
var kd=node.onkeydown;
if(--kd._stealthKeydownRefs<=0){
iel.remove(node,"onkeydown",kd._stealthKeydownHandle);
delete kd._stealthKeydownHandle;
}
}
},_normalizeEventName:function(_1f2){
return _1f2.slice(0,2)!="on"?"on"+_1f2:_1f2;
},_nop:function(){
},_fixEvent:function(evt,_1f4){
if(!evt){
var w=_1f4&&(_1f4.ownerDocument||_1f4.document||_1f4).parentWindow||window;
evt=w.event;
}
if(!evt){
return (evt);
}
evt.target=evt.srcElement;
evt.currentTarget=(_1f4||evt.srcElement);
evt.layerX=evt.offsetX;
evt.layerY=evt.offsetY;
var se=evt.srcElement,doc=(se&&se.ownerDocument)||document;
var _1f8=((dojo.isIE<6)||(doc["compatMode"]=="BackCompat"))?doc.body:doc.documentElement;
var _1f9=dojo._getIeDocumentElementOffset();
evt.pageX=evt.clientX+dojo._fixIeBiDiScrollLeft(_1f8.scrollLeft||0)-_1f9.x;
evt.pageY=evt.clientY+(_1f8.scrollTop||0)-_1f9.y;
if(evt.type=="mouseover"){
evt.relatedTarget=evt.fromElement;
}
if(evt.type=="mouseout"){
evt.relatedTarget=evt.toElement;
}
evt.stopPropagation=del._stopPropagation;
evt.preventDefault=del._preventDefault;
return del._fixKeys(evt);
},_fixKeys:function(evt){
switch(evt.type){
case "keypress":
var c=("charCode" in evt?evt.charCode:evt.keyCode);
if(c==10){
c=0;
evt.keyCode=13;
}else{
if(c==13||c==27){
c=0;
}else{
if(c==3){
c=99;
}
}
}
evt.charCode=c;
del._setKeyChar(evt);
break;
}
return evt;
},_stealthKeyDown:function(evt){
var kp=evt.currentTarget.onkeypress;
if(!kp||!kp[_1dd]){
return;
}
var k=evt.keyCode;
var _1ff=k!=13&&k!=32&&k!=27&&(k<48||k>90)&&(k<96||k>111)&&(k<186||k>192)&&(k<219||k>222);
if(_1ff||evt.ctrlKey){
var c=_1ff?0:k;
if(evt.ctrlKey){
if(k==3||k==13){
return;
}else{
if(c>95&&c<106){
c-=48;
}else{
if((!evt.shiftKey)&&(c>=65&&c<=90)){
c+=32;
}else{
c=del._punctMap[c]||c;
}
}
}
}
var faux=del._synthesizeEvent(evt,{type:"keypress",faux:true,charCode:c});
kp.call(evt.currentTarget,faux);
evt.cancelBubble=faux.cancelBubble;
evt.returnValue=faux.returnValue;
_1d9(evt,faux.keyCode);
}
},_stopPropagation:function(){
this.cancelBubble=true;
},_preventDefault:function(){
this.bubbledKeyCode=this.keyCode;
if(this.ctrlKey){
_1d9(this,0);
}
this.returnValue=false;
}});
dojo.stopEvent=function(evt){
evt=evt||window.event;
del._stopPropagation.call(evt);
del._preventDefault.call(evt);
};
}
del._synthesizeEvent=function(evt,_204){
var faux=dojo.mixin({},evt,_204);
del._setKeyChar(faux);
faux.preventDefault=function(){
evt.preventDefault();
};
faux.stopPropagation=function(){
evt.stopPropagation();
};
return faux;
};
if(dojo.isOpera){
dojo.mixin(del,{_fixEvent:function(evt,_207){
switch(evt.type){
case "keypress":
var c=evt.which;
if(c==3){
c=99;
}
c=c<41&&!evt.shiftKey?0:c;
if(evt.ctrlKey&&!evt.shiftKey&&c>=65&&c<=90){
c+=32;
}
return del._synthesizeEvent(evt,{charCode:c});
}
return evt;
}});
}
if(dojo.isWebKit){
del._add=del.add;
del._remove=del.remove;
dojo.mixin(del,{add:function(node,_20a,fp){
if(!node){
return;
}
var _20c=del._add(node,_20a,fp);
if(del._normalizeEventName(_20a)=="keypress"){
_20c._stealthKeyDownHandle=del._add(node,"keydown",function(evt){
var k=evt.keyCode;
var _20f=k!=13&&k!=32&&k!=27&&(k<48||k>90)&&(k<96||k>111)&&(k<186||k>192)&&(k<219||k>222);
if(_20f||evt.ctrlKey){
var c=_20f?0:k;
if(evt.ctrlKey){
if(k==3||k==13){
return;
}else{
if(c>95&&c<106){
c-=48;
}else{
if(!evt.shiftKey&&c>=65&&c<=90){
c+=32;
}else{
c=del._punctMap[c]||c;
}
}
}
}
var faux=del._synthesizeEvent(evt,{type:"keypress",faux:true,charCode:c});
fp.call(evt.currentTarget,faux);
}
});
}
return _20c;
},remove:function(node,_213,_214){
if(node){
if(_214._stealthKeyDownHandle){
del._remove(node,"keydown",_214._stealthKeyDownHandle);
}
del._remove(node,_213,_214);
}
},_fixEvent:function(evt,_216){
switch(evt.type){
case "keypress":
if(evt.faux){
return evt;
}
var c=evt.charCode;
c=c>=32?c:0;
return del._synthesizeEvent(evt,{charCode:c,faux:true});
}
return evt;
}});
}
})();
if(dojo.isIE){
dojo._ieDispatcher=function(args,_219){
var ap=Array.prototype,h=dojo._ie_listener.handlers,c=args.callee,ls=c[dojo._ieListenersName],t=h[c.target];
var r=t&&t.apply(_219,args);
var lls=[].concat(ls);
for(var i in lls){
var f=h[lls[i]];
if(!(i in ap)&&f){
f.apply(_219,args);
}
}
return r;
};
dojo._getIeDispatcher=function(){
return new Function(dojo._scopeName+"._ieDispatcher(arguments, this)");
};
dojo._event_listener._fixCallback=function(fp){
var f=dojo._event_listener._fixEvent;
return function(e){
return fp.call(this,f(e,this));
};
};
}
}
if(!dojo._hasResource["dojo._base.html"]){
dojo._hasResource["dojo._base.html"]=true;
dojo.provide("dojo._base.html");
try{
document.execCommand("BackgroundImageCache",false,true);
}
catch(e){
}
if(dojo.isIE||dojo.isOpera){
dojo.byId=function(id,doc){
if(dojo.isString(id)){
var _d=doc||dojo.doc;
var te=_d.getElementById(id);
if(te&&(te.attributes.id.value==id||te.id==id)){
return te;
}else{
var eles=_d.all[id];
if(!eles||eles.nodeName){
eles=[eles];
}
var i=0;
while((te=eles[i++])){
if((te.attributes&&te.attributes.id&&te.attributes.id.value==id)||te.id==id){
return te;
}
}
}
}else{
return id;
}
};
}else{
dojo.byId=function(id,doc){
return dojo.isString(id)?(doc||dojo.doc).getElementById(id):id;
};
}
(function(){
var d=dojo;
var _22f=null;
d.addOnWindowUnload(function(){
_22f=null;
});
dojo._destroyElement=dojo.destroy=function(node){
node=d.byId(node);
try{
if(!_22f||_22f.ownerDocument!=node.ownerDocument){
_22f=node.ownerDocument.createElement("div");
}
_22f.appendChild(node.parentNode?node.parentNode.removeChild(node):node);
_22f.innerHTML="";
}
catch(e){
}
};
dojo.isDescendant=function(node,_232){
try{
node=d.byId(node);
_232=d.byId(_232);
while(node){
if(node===_232){
return true;
}
node=node.parentNode;
}
}
catch(e){
}
return false;
};
dojo.setSelectable=function(node,_234){
node=d.byId(node);
if(d.isMozilla){
node.style.MozUserSelect=_234?"":"none";
}else{
if(d.isKhtml||d.isWebKit){
node.style.KhtmlUserSelect=_234?"auto":"none";
}else{
if(d.isIE){
var v=(node.unselectable=_234?"":"on");
d.query("*",node).forEach("item.unselectable = '"+v+"'");
}
}
}
};
var _236=function(node,ref){
var _239=ref.parentNode;
if(_239){
_239.insertBefore(node,ref);
}
};
var _23a=function(node,ref){
var _23d=ref.parentNode;
if(_23d){
if(_23d.lastChild==ref){
_23d.appendChild(node);
}else{
_23d.insertBefore(node,ref.nextSibling);
}
}
};
dojo.place=function(node,_23f,_240){
_23f=d.byId(_23f);
if(d.isString(node)){
node=node.charAt(0)=="<"?d._toDom(node,_23f.ownerDocument):d.byId(node);
}
if(typeof _240=="number"){
var cn=_23f.childNodes;
if(!cn.length||cn.length<=_240){
_23f.appendChild(node);
}else{
_236(node,cn[_240<0?0:_240]);
}
}else{
switch(_240){
case "before":
_236(node,_23f);
break;
case "after":
_23a(node,_23f);
break;
case "replace":
_23f.parentNode.replaceChild(node,_23f);
break;
case "only":
d.empty(_23f);
_23f.appendChild(node);
break;
case "first":
if(_23f.firstChild){
_236(node,_23f.firstChild);
break;
}
default:
_23f.appendChild(node);
}
}
return node;
};
dojo.boxModel="content-box";
if(d.isIE){
var _dcm=document.compatMode;
d.boxModel=_dcm=="BackCompat"||_dcm=="QuirksMode"||d.isIE<6?"border-box":"content-box";
}
var gcs;
if(d.isWebKit){
gcs=function(node){
var s;
if(node.nodeType==1){
var dv=node.ownerDocument.defaultView;
s=dv.getComputedStyle(node,null);
if(!s&&node.style){
node.style.display="";
s=dv.getComputedStyle(node,null);
}
}
return s||{};
};
}else{
if(d.isIE){
gcs=function(node){
return node.nodeType==1?node.currentStyle:{};
};
}else{
gcs=function(node){
return node.nodeType==1?node.ownerDocument.defaultView.getComputedStyle(node,null):{};
};
}
}
dojo.getComputedStyle=gcs;
if(!d.isIE){
d._toPixelValue=function(_249,_24a){
return parseFloat(_24a)||0;
};
}else{
d._toPixelValue=function(_24b,_24c){
if(!_24c){
return 0;
}
if(_24c=="medium"){
return 4;
}
if(_24c.slice&&_24c.slice(-2)=="px"){
return parseFloat(_24c);
}
with(_24b){
var _24d=style.left;
var _24e=runtimeStyle.left;
runtimeStyle.left=currentStyle.left;
try{
style.left=_24c;
_24c=style.pixelLeft;
}
catch(e){
_24c=0;
}
style.left=_24d;
runtimeStyle.left=_24e;
}
return _24c;
};
}
var px=d._toPixelValue;
var astr="DXImageTransform.Microsoft.Alpha";
var af=function(n,f){
try{
return n.filters.item(astr);
}
catch(e){
return f?{}:null;
}
};
dojo._getOpacity=d.isIE?function(node){
try{
return af(node).Opacity/100;
}
catch(e){
return 1;
}
}:function(node){
return gcs(node).opacity;
};
dojo._setOpacity=d.isIE?function(node,_257){
var ov=_257*100;
node.style.zoom=1;
af(node,1).Enabled=!(_257==1);
if(!af(node)){
node.style.filter+=" progid:"+astr+"(Opacity="+ov+")";
}else{
af(node,1).Opacity=ov;
}
if(node.nodeName.toLowerCase()=="tr"){
d.query("> td",node).forEach(function(i){
d._setOpacity(i,_257);
});
}
return _257;
}:function(node,_25b){
return node.style.opacity=_25b;
};
var _25c={left:true,top:true};
var _25d=/margin|padding|width|height|max|min|offset/;
var _25e=function(node,type,_261){
type=type.toLowerCase();
if(d.isIE){
if(_261=="auto"){
if(type=="height"){
return node.offsetHeight;
}
if(type=="width"){
return node.offsetWidth;
}
}
if(type=="fontweight"){
switch(_261){
case 700:
return "bold";
case 400:
default:
return "normal";
}
}
}
if(!(type in _25c)){
_25c[type]=_25d.test(type);
}
return _25c[type]?px(node,_261):_261;
};
var _262=d.isIE?"styleFloat":"cssFloat",_263={"cssFloat":_262,"styleFloat":_262,"float":_262};
dojo.style=function(node,_265,_266){
var n=d.byId(node),args=arguments.length,op=(_265=="opacity");
_265=_263[_265]||_265;
if(args==3){
return op?d._setOpacity(n,_266):n.style[_265]=_266;
}
if(args==2&&op){
return d._getOpacity(n);
}
var s=gcs(n);
if(args==2&&!d.isString(_265)){
for(var x in _265){
d.style(node,x,_265[x]);
}
return s;
}
return (args==1)?s:_25e(n,_265,s[_265]||n.style[_265]);
};
dojo._getPadExtents=function(n,_26d){
var s=_26d||gcs(n),l=px(n,s.paddingLeft),t=px(n,s.paddingTop);
return {l:l,t:t,w:l+px(n,s.paddingRight),h:t+px(n,s.paddingBottom)};
};
dojo._getBorderExtents=function(n,_272){
var ne="none",s=_272||gcs(n),bl=(s.borderLeftStyle!=ne?px(n,s.borderLeftWidth):0),bt=(s.borderTopStyle!=ne?px(n,s.borderTopWidth):0);
return {l:bl,t:bt,w:bl+(s.borderRightStyle!=ne?px(n,s.borderRightWidth):0),h:bt+(s.borderBottomStyle!=ne?px(n,s.borderBottomWidth):0)};
};
dojo._getPadBorderExtents=function(n,_278){
var s=_278||gcs(n),p=d._getPadExtents(n,s),b=d._getBorderExtents(n,s);
return {l:p.l+b.l,t:p.t+b.t,w:p.w+b.w,h:p.h+b.h};
};
dojo._getMarginExtents=function(n,_27d){
var s=_27d||gcs(n),l=px(n,s.marginLeft),t=px(n,s.marginTop),r=px(n,s.marginRight),b=px(n,s.marginBottom);
if(d.isWebKit&&(s.position!="absolute")){
r=l;
}
return {l:l,t:t,w:l+r,h:t+b};
};
dojo._getMarginBox=function(node,_284){
var s=_284||gcs(node),me=d._getMarginExtents(node,s);
var l=node.offsetLeft-me.l,t=node.offsetTop-me.t,p=node.parentNode;
if(d.isMoz){
var sl=parseFloat(s.left),st=parseFloat(s.top);
if(!isNaN(sl)&&!isNaN(st)){
l=sl,t=st;
}else{
if(p&&p.style){
var pcs=gcs(p);
if(pcs.overflow!="visible"){
var be=d._getBorderExtents(p,pcs);
l+=be.l,t+=be.t;
}
}
}
}else{
if(d.isOpera||(d.isIE>7&&!d.isQuirks)){
if(p){
be=d._getBorderExtents(p);
l-=be.l;
t-=be.t;
}
}
}
return {l:l,t:t,w:node.offsetWidth+me.w,h:node.offsetHeight+me.h};
};
dojo._getContentBox=function(node,_28f){
var s=_28f||gcs(node),pe=d._getPadExtents(node,s),be=d._getBorderExtents(node,s),w=node.clientWidth,h;
if(!w){
w=node.offsetWidth,h=node.offsetHeight;
}else{
h=node.clientHeight,be.w=be.h=0;
}
if(d.isOpera){
pe.l+=be.l;
pe.t+=be.t;
}
return {l:pe.l,t:pe.t,w:w-pe.w-be.w,h:h-pe.h-be.h};
};
dojo._getBorderBox=function(node,_296){
var s=_296||gcs(node),pe=d._getPadExtents(node,s),cb=d._getContentBox(node,s);
return {l:cb.l-pe.l,t:cb.t-pe.t,w:cb.w+pe.w,h:cb.h+pe.h};
};
dojo._setBox=function(node,l,t,w,h,u){
u=u||"px";
var s=node.style;
if(!isNaN(l)){
s.left=l+u;
}
if(!isNaN(t)){
s.top=t+u;
}
if(w>=0){
s.width=w+u;
}
if(h>=0){
s.height=h+u;
}
};
dojo._isButtonTag=function(node){
return node.tagName=="BUTTON"||node.tagName=="INPUT"&&node.getAttribute("type").toUpperCase()=="BUTTON";
};
dojo._usesBorderBox=function(node){
var n=node.tagName;
return d.boxModel=="border-box"||n=="TABLE"||d._isButtonTag(node);
};
dojo._setContentSize=function(node,_2a5,_2a6,_2a7){
if(d._usesBorderBox(node)){
var pb=d._getPadBorderExtents(node,_2a7);
if(_2a5>=0){
_2a5+=pb.w;
}
if(_2a6>=0){
_2a6+=pb.h;
}
}
d._setBox(node,NaN,NaN,_2a5,_2a6);
};
dojo._setMarginBox=function(node,_2aa,_2ab,_2ac,_2ad,_2ae){
var s=_2ae||gcs(node),bb=d._usesBorderBox(node),pb=bb?_2b2:d._getPadBorderExtents(node,s);
if(d.isWebKit){
if(d._isButtonTag(node)){
var ns=node.style;
if(_2ac>=0&&!ns.width){
ns.width="4px";
}
if(_2ad>=0&&!ns.height){
ns.height="4px";
}
}
}
var mb=d._getMarginExtents(node,s);
if(_2ac>=0){
_2ac=Math.max(_2ac-pb.w-mb.w,0);
}
if(_2ad>=0){
_2ad=Math.max(_2ad-pb.h-mb.h,0);
}
d._setBox(node,_2aa,_2ab,_2ac,_2ad);
};
var _2b2={l:0,t:0,w:0,h:0};
dojo.marginBox=function(node,box){
var n=d.byId(node),s=gcs(n),b=box;
return !b?d._getMarginBox(n,s):d._setMarginBox(n,b.l,b.t,b.w,b.h,s);
};
dojo.contentBox=function(node,box){
var n=d.byId(node),s=gcs(n),b=box;
return !b?d._getContentBox(n,s):d._setContentSize(n,b.w,b.h,s);
};
var _2bf=function(node,prop){
if(!(node=(node||0).parentNode)){
return 0;
}
var val,_2c3=0,_b=d.body();
while(node&&node.style){
if(gcs(node).position=="fixed"){
return 0;
}
val=node[prop];
if(val){
_2c3+=val-0;
if(node==_b){
break;
}
}
node=node.parentNode;
}
return _2c3;
};
dojo._docScroll=function(){
var _b=d.body(),_w=d.global,de=d.doc.documentElement;
return {y:(_w.pageYOffset||de.scrollTop||_b.scrollTop||0),x:(_w.pageXOffset||d._fixIeBiDiScrollLeft(de.scrollLeft)||_b.scrollLeft||0)};
};
dojo._isBodyLtr=function(){
return ("_bodyLtr" in d)?d._bodyLtr:d._bodyLtr=gcs(d.body()).direction=="ltr";
};
dojo._getIeDocumentElementOffset=function(){
var de=d.doc.documentElement;
if(d.isIE<7){
return {x:d._isBodyLtr()||window.parent==window?de.clientLeft:de.offsetWidth-de.clientWidth-de.clientLeft,y:de.clientTop};
}else{
if(d.isIE<8){
return {x:de.getBoundingClientRect().left,y:de.getBoundingClientRect().top};
}else{
return {x:0,y:0};
}
}
};
dojo._fixIeBiDiScrollLeft=function(_2c9){
var dd=d.doc;
if(d.isIE<8&&!d._isBodyLtr()){
var de=dd.compatMode=="BackCompat"?dd.body:dd.documentElement;
return _2c9+de.clientWidth-de.scrollWidth;
}
return _2c9;
};
dojo._abs=function(node,_2cd){
var db=d.body(),dh=d.body().parentNode,ret;
if(node["getBoundingClientRect"]){
var _2d1=node.getBoundingClientRect();
ret={x:_2d1.left,y:_2d1.top};
if(d.isFF>=3){
var cs=gcs(dh);
ret.x-=px(dh,cs.marginLeft)+px(dh,cs.borderLeftWidth);
ret.y-=px(dh,cs.marginTop)+px(dh,cs.borderTopWidth);
}
if(d.isIE){
var _2d3=d._getIeDocumentElementOffset();
ret.x-=_2d3.x+(d.isQuirks?db.clientLeft:0);
ret.y-=_2d3.y+(d.isQuirks?db.clientTop:0);
}
}else{
ret={x:0,y:0};
if(node["offsetParent"]){
ret.x-=_2bf(node,"scrollLeft");
ret.y-=_2bf(node,"scrollTop");
var _2d4=node;
do{
var n=_2d4.offsetLeft,t=_2d4.offsetTop;
ret.x+=isNaN(n)?0:n;
ret.y+=isNaN(t)?0:t;
cs=gcs(_2d4);
if(_2d4!=node){
if(d.isFF){
ret.x+=2*px(_2d4,cs.borderLeftWidth);
ret.y+=2*px(_2d4,cs.borderTopWidth);
}else{
ret.x+=px(_2d4,cs.borderLeftWidth);
ret.y+=px(_2d4,cs.borderTopWidth);
}
}
if(d.isFF&&cs.position=="static"){
var _2d7=_2d4.parentNode;
while(_2d7!=_2d4.offsetParent){
var pcs=gcs(_2d7);
if(pcs.position=="static"){
ret.x+=px(_2d4,pcs.borderLeftWidth);
ret.y+=px(_2d4,pcs.borderTopWidth);
}
_2d7=_2d7.parentNode;
}
}
_2d4=_2d4.offsetParent;
}while((_2d4!=dh)&&_2d4);
}else{
if(node.x&&node.y){
ret.x+=isNaN(node.x)?0:node.x;
ret.y+=isNaN(node.y)?0:node.y;
}
}
}
if(_2cd){
var _2d9=d._docScroll();
ret.x+=_2d9.x;
ret.y+=_2d9.y;
}
return ret;
};
dojo.coords=function(node,_2db){
var n=d.byId(node),s=gcs(n),mb=d._getMarginBox(n,s);
var abs=d._abs(n,_2db);
mb.x=abs.x;
mb.y=abs.y;
return mb;
};
var _2e0=d.isIE<8;
var _2e1=function(name){
switch(name.toLowerCase()){
case "tabindex":
return _2e0?"tabIndex":"tabindex";
case "readonly":
return "readOnly";
case "class":
return "className";
case "for":
case "htmlfor":
return _2e0?"htmlFor":"for";
default:
return name;
}
};
var _2e3={colspan:"colSpan",enctype:"enctype",frameborder:"frameborder",method:"method",rowspan:"rowSpan",scrolling:"scrolling",shape:"shape",span:"span",type:"type",valuetype:"valueType",classname:"className",innerhtml:"innerHTML"};
dojo.hasAttr=function(node,name){
node=d.byId(node);
var _2e6=_2e1(name);
_2e6=_2e6=="htmlFor"?"for":_2e6;
var attr=node.getAttributeNode&&node.getAttributeNode(_2e6);
return attr?attr.specified:false;
};
var _2e8={},_ctr=0,_2ea=dojo._scopeName+"attrid",_2eb={col:1,colgroup:1,table:1,tbody:1,tfoot:1,thead:1,tr:1,title:1};
dojo.attr=function(node,name,_2ee){
node=d.byId(node);
var args=arguments.length;
if(args==2&&!d.isString(name)){
for(var x in name){
d.attr(node,x,name[x]);
}
return;
}
name=_2e1(name);
if(args==3){
if(d.isFunction(_2ee)){
var _2f1=d.attr(node,_2ea);
if(!_2f1){
_2f1=_ctr++;
d.attr(node,_2ea,_2f1);
}
if(!_2e8[_2f1]){
_2e8[_2f1]={};
}
var h=_2e8[_2f1][name];
if(h){
d.disconnect(h);
}else{
try{
delete node[name];
}
catch(e){
}
}
_2e8[_2f1][name]=d.connect(node,name,_2ee);
}else{
if(typeof _2ee=="boolean"){
node[name]=_2ee;
}else{
if(name==="style"&&!d.isString(_2ee)){
d.style(node,_2ee);
}else{
if(name=="className"){
node.className=_2ee;
}else{
if(name==="innerHTML"){
if(d.isIE&&node.tagName.toLowerCase() in _2eb){
d.empty(node);
node.appendChild(d._toDom(_2ee,node.ownerDocument));
}else{
node[name]=_2ee;
}
}else{
node.setAttribute(name,_2ee);
}
}
}
}
}
}else{
var prop=_2e3[name.toLowerCase()];
if(prop){
return node[prop];
}
var _2f4=node[name];
return (typeof _2f4=="boolean"||typeof _2f4=="function")?_2f4:(d.hasAttr(node,name)?node.getAttribute(name):null);
}
};
dojo.removeAttr=function(node,name){
d.byId(node).removeAttribute(_2e1(name));
};
dojo.create=function(tag,_2f8,_2f9,pos){
var doc=d.doc;
if(_2f9){
_2f9=d.byId(_2f9);
doc=_2f9.ownerDocument;
}
if(d.isString(tag)){
tag=doc.createElement(tag);
}
if(_2f8){
d.attr(tag,_2f8);
}
if(_2f9){
d.place(tag,_2f9,pos);
}
return tag;
};
d.empty=d.isIE?function(node){
node=d.byId(node);
for(var c;c=node.lastChild;){
d.destroy(c);
}
}:function(node){
d.byId(node).innerHTML="";
};
var _2ff={option:["select"],tbody:["table"],thead:["table"],tfoot:["table"],tr:["table","tbody"],td:["table","tbody","tr"],th:["table","thead","tr"],legend:["fieldset"],caption:["table"],colgroup:["table"],col:["table","colgroup"],li:["ul"]},_300=/<\s*([\w\:]+)/,_301={},_302=0,_303="__"+d._scopeName+"ToDomId";
for(var _304 in _2ff){
var tw=_2ff[_304];
tw.pre=_304=="option"?"<select multiple=\"multiple\">":"<"+tw.join("><")+">";
tw.post="</"+tw.reverse().join("></")+">";
}
d._toDom=function(frag,doc){
doc=doc||d.doc;
var _308=doc[_303];
if(!_308){
doc[_303]=_308=++_302+"";
_301[_308]=doc.createElement("div");
}
frag+="";
var _309=frag.match(_300),tag=_309?_309[1].toLowerCase():"",_30b=_301[_308],wrap,i,fc,df;
if(_309&&_2ff[tag]){
wrap=_2ff[tag];
_30b.innerHTML=wrap.pre+frag+wrap.post;
for(i=wrap.length;i;--i){
_30b=_30b.firstChild;
}
}else{
_30b.innerHTML=frag;
}
if(_30b.childNodes.length==1){
return _30b.removeChild(_30b.firstChild);
}
df=doc.createDocumentFragment();
while(fc=_30b.firstChild){
df.appendChild(fc);
}
return df;
};
var _30f="className";
dojo.hasClass=function(node,_311){
return ((" "+d.byId(node)[_30f]+" ").indexOf(" "+_311+" ")>=0);
};
dojo.addClass=function(node,_313){
node=d.byId(node);
var cls=node[_30f];
if((" "+cls+" ").indexOf(" "+_313+" ")<0){
node[_30f]=cls+(cls?" ":"")+_313;
}
};
dojo.removeClass=function(node,_316){
node=d.byId(node);
var t=d.trim((" "+node[_30f]+" ").replace(" "+_316+" "," "));
if(node[_30f]!=t){
node[_30f]=t;
}
};
dojo.toggleClass=function(node,_319,_31a){
if(_31a===undefined){
_31a=!d.hasClass(node,_319);
}
d[_31a?"addClass":"removeClass"](node,_319);
};
})();
}
if(!dojo._hasResource["dojo._base.NodeList"]){
dojo._hasResource["dojo._base.NodeList"]=true;
dojo.provide("dojo._base.NodeList");
(function(){
var d=dojo;
var ap=Array.prototype,aps=ap.slice,apc=ap.concat;
var tnl=function(a){
a.constructor=d.NodeList;
dojo._mixin(a,d.NodeList.prototype);
return a;
};
var _321=function(f,a,o){
a=[0].concat(aps.call(a,0));
if(!a.sort){
a=aps.call(a,0);
}
o=o||d.global;
return function(node){
a[0]=node;
return f.apply(o,a);
};
};
var _326=function(f,o){
return function(){
this.forEach(_321(f,arguments,o));
return this;
};
};
var _329=function(f,o){
return function(){
return this.map(_321(f,arguments,o));
};
};
var _32c=function(f,o){
return function(){
return this.filter(_321(f,arguments,o));
};
};
var _32f=function(f,g,o){
return function(){
var a=arguments,body=_321(f,a,o);
if(g.call(o||d.global,a)){
return this.map(body);
}
this.forEach(body);
return this;
};
};
var _335=function(a){
return a.length==1&&d.isString(a[0]);
};
var _337=function(node){
var p=node.parentNode;
if(p){
p.removeChild(node);
}
};
dojo.NodeList=function(){
return tnl(Array.apply(null,arguments));
};
var nl=d.NodeList,nlp=nl.prototype;
nl._wrap=tnl;
nl._adaptAsMap=_329;
nl._adaptAsForEach=_326;
nl._adaptAsFilter=_32c;
nl._adaptWithCondition=_32f;
d.forEach(["slice","splice"],function(name){
var f=ap[name];
nlp[name]=function(){
return tnl(f.apply(this,arguments));
};
});
d.forEach(["indexOf","lastIndexOf","every","some"],function(name){
var f=d[name];
nlp[name]=function(){
return f.apply(d,[this].concat(aps.call(arguments,0)));
};
});
d.forEach(["attr","style"],function(name){
nlp[name]=_32f(d[name],_335);
});
d.forEach(["connect","addClass","removeClass","toggleClass","empty"],function(name){
nlp[name]=_326(d[name]);
});
dojo.extend(dojo.NodeList,{concat:function(item){
var t=d.isArray(this)?this:aps.call(this,0),m=d.map(arguments,function(a){
return a&&!d.isArray(a)&&(a.constructor===NodeList||a.constructor==nl)?aps.call(a,0):a;
});
return tnl(apc.apply(t,m));
},map:function(func,obj){
return tnl(d.map(this,func,obj));
},forEach:function(_348,_349){
d.forEach(this,_348,_349);
return this;
},coords:_329(d.coords),place:function(_34a,_34b){
var item=d.query(_34a)[0];
return this.forEach(function(node){
d.place(node,item,_34b);
});
},orphan:function(_34e){
return (_34e?d._filterQueryResult(this,_34e):this).forEach(_337);
},adopt:function(_34f,_350){
return d.query(_34f).place(this[0],_350);
},query:function(_351){
if(!_351){
return this;
}
var ret=this.map(function(node){
return d.query(_351,node).filter(function(_354){
return _354!==undefined;
});
});
return tnl(apc.apply([],ret));
},filter:function(_355){
var a=arguments,_357=this,_358=0;
if(d.isString(_355)){
_357=d._filterQueryResult(this,a[0]);
if(a.length==1){
return _357;
}
_358=1;
}
return tnl(d.filter(_357,a[_358],a[_358+1]));
},addContent:function(_359,_35a){
var c=d.isString(_359)?d._toDom(_359,this[0]&&this[0].ownerDocument):_359,i,l=this.length-1;
for(i=0;i<l;++i){
d.place(c.cloneNode(true),this[i],_35a);
}
if(l>=0){
d.place(c,this[l],_35a);
}
return this;
},instantiate:function(_35d,_35e){
var c=d.isFunction(_35d)?_35d:d.getObject(_35d);
_35e=_35e||{};
return this.forEach(function(node){
new c(_35e,node);
});
},at:function(){
var t=new dojo.NodeList();
d.forEach(arguments,function(i){
if(this[i]){
t.push(this[i]);
}
},this);
return t;
}});
d.forEach(["blur","focus","change","click","error","keydown","keypress","keyup","load","mousedown","mouseenter","mouseleave","mousemove","mouseout","mouseover","mouseup","submit"],function(evt){
var _oe="on"+evt;
nlp[_oe]=function(a,b){
return this.connect(_oe,a,b);
};
});
})();
}
if(!dojo._hasResource["dojo._base.query"]){
dojo._hasResource["dojo._base.query"]=true;
if(typeof dojo!="undefined"){
dojo.provide("dojo._base.query");
}
(function(d){
var trim=d.trim;
var each=d.forEach;
var qlc=d._queryListCtor=d.NodeList;
var _36b=d.isString;
var _36c=function(){
return d.doc;
};
var _36d=((d.isWebKit||d.isMozilla)&&((_36c().compatMode)=="BackCompat"));
var _36e=!!_36c().firstChild["children"]?"children":"childNodes";
var _36f=">~+";
var _370=false;
var _371=function(){
return true;
};
var _372=function(_373){
if(_36f.indexOf(_373.slice(-1))>=0){
_373+=" * ";
}else{
_373+=" ";
}
var ts=function(s,e){
return trim(_373.slice(s,e));
};
var _377=[];
var _378=-1,_379=-1,_37a=-1,_37b=-1,_37c=-1,inId=-1,_37e=-1,lc="",cc="",_381;
var x=0,ql=_373.length,_384=null,_cp=null;
var _386=function(){
if(_37e>=0){
var tv=(_37e==x)?null:ts(_37e,x);
_384[(_36f.indexOf(tv)<0)?"tag":"oper"]=tv;
_37e=-1;
}
};
var _388=function(){
if(inId>=0){
_384.id=ts(inId,x).replace(/\\/g,"");
inId=-1;
}
};
var _389=function(){
if(_37c>=0){
_384.classes.push(ts(_37c+1,x).replace(/\\/g,""));
_37c=-1;
}
};
var _38a=function(){
_388();
_386();
_389();
};
var _38b=function(){
_38a();
if(_37b>=0){
_384.pseudos.push({name:ts(_37b+1,x)});
}
_384.loops=(_384.pseudos.length||_384.attrs.length||_384.classes.length);
_384.oquery=_384.query=ts(_381,x);
_384.otag=_384.tag=(_384["oper"])?null:(_384.tag||"*");
if(_384.tag){
_384.tag=_384.tag.toUpperCase();
}
if(_377.length&&(_377[_377.length-1].oper)){
_384.infixOper=_377.pop();
_384.query=_384.infixOper.query+" "+_384.query;
}
_377.push(_384);
_384=null;
};
for(;lc=cc,cc=_373.charAt(x),x<ql;x++){
if(lc=="\\"){
continue;
}
if(!_384){
_381=x;
_384={query:null,pseudos:[],attrs:[],classes:[],tag:null,oper:null,id:null,getTag:function(){
return (_370)?this.otag:this.tag;
}};
_37e=x;
}
if(_378>=0){
if(cc=="]"){
if(!_cp.attr){
_cp.attr=ts(_378+1,x);
}else{
_cp.matchFor=ts((_37a||_378+1),x);
}
var cmf=_cp.matchFor;
if(cmf){
if((cmf.charAt(0)=="\"")||(cmf.charAt(0)=="'")){
_cp.matchFor=cmf.slice(1,-1);
}
}
_384.attrs.push(_cp);
_cp=null;
_378=_37a=-1;
}else{
if(cc=="="){
var _38d=("|~^$*".indexOf(lc)>=0)?lc:"";
_cp.type=_38d+cc;
_cp.attr=ts(_378+1,x-_38d.length);
_37a=x+1;
}
}
}else{
if(_379>=0){
if(cc==")"){
if(_37b>=0){
_cp.value=ts(_379+1,x);
}
_37b=_379=-1;
}
}else{
if(cc=="#"){
_38a();
inId=x+1;
}else{
if(cc=="."){
_38a();
_37c=x;
}else{
if(cc==":"){
_38a();
_37b=x;
}else{
if(cc=="["){
_38a();
_378=x;
_cp={};
}else{
if(cc=="("){
if(_37b>=0){
_cp={name:ts(_37b+1,x),value:null};
_384.pseudos.push(_cp);
}
_379=x;
}else{
if((cc==" ")&&(lc!=cc)){
_38b();
}
}
}
}
}
}
}
}
}
return _377;
};
var _38e=function(_38f,_390){
if(!_38f){
return _390;
}
if(!_390){
return _38f;
}
return function(){
return _38f.apply(window,arguments)&&_390.apply(window,arguments);
};
};
var _391=function(i,arr){
var r=arr||[];
if(i){
r.push(i);
}
return r;
};
var _395=function(n){
return (1==n.nodeType);
};
var _397="";
var _398=function(elem,attr){
if(!elem){
return _397;
}
if(attr=="class"){
return elem.className||_397;
}
if(attr=="for"){
return elem.htmlFor||_397;
}
if(attr=="style"){
return elem.style.cssText||_397;
}
return (_370?elem.getAttribute(attr):elem.getAttribute(attr,2))||_397;
};
var _39b={"*=":function(attr,_39d){
return function(elem){
return (_398(elem,attr).indexOf(_39d)>=0);
};
},"^=":function(attr,_3a0){
return function(elem){
return (_398(elem,attr).indexOf(_3a0)==0);
};
},"$=":function(attr,_3a3){
var tval=" "+_3a3;
return function(elem){
var ea=" "+_398(elem,attr);
return (ea.lastIndexOf(_3a3)==(ea.length-_3a3.length));
};
},"~=":function(attr,_3a8){
var tval=" "+_3a8+" ";
return function(elem){
var ea=" "+_398(elem,attr)+" ";
return (ea.indexOf(tval)>=0);
};
},"|=":function(attr,_3ad){
var _3ae=" "+_3ad+"-";
return function(elem){
var ea=" "+_398(elem,attr);
return ((ea==_3ad)||(ea.indexOf(_3ae)==0));
};
},"=":function(attr,_3b2){
return function(elem){
return (_398(elem,attr)==_3b2);
};
}};
var _3b4=(typeof _36c().firstChild.nextElementSibling=="undefined");
var _ns=!_3b4?"nextElementSibling":"nextSibling";
var _ps=!_3b4?"previousElementSibling":"previousSibling";
var _3b7=(_3b4?_395:_371);
var _3b8=function(node){
while(node=node[_ps]){
if(_3b7(node)){
return false;
}
}
return true;
};
var _3ba=function(node){
while(node=node[_ns]){
if(_3b7(node)){
return false;
}
}
return true;
};
var _3bc=function(node){
var root=node.parentNode;
var i=0,tret=root[_36e],ci=(node["_i"]||-1),cl=(root["_l"]||-1);
if(!tret){
return -1;
}
var l=tret.length;
if(cl==l&&ci>=0&&cl>=0){
return ci;
}
root["_l"]=l;
ci=-1;
for(var te=root["firstElementChild"]||root["firstChild"];te;te=te[_ns]){
if(_3b7(te)){
te["_i"]=++i;
if(node===te){
ci=i;
}
}
}
return ci;
};
var _3c5=function(elem){
return !((_3bc(elem))%2);
};
var _3c7=function(elem){
return ((_3bc(elem))%2);
};
var _3c9={"checked":function(name,_3cb){
return function(elem){
return !!d.attr(elem,"checked");
};
},"first-child":function(){
return _3b8;
},"last-child":function(){
return _3ba;
},"only-child":function(name,_3ce){
return function(node){
if(!_3b8(node)){
return false;
}
if(!_3ba(node)){
return false;
}
return true;
};
},"empty":function(name,_3d1){
return function(elem){
var cn=elem.childNodes;
var cnl=elem.childNodes.length;
for(var x=cnl-1;x>=0;x--){
var nt=cn[x].nodeType;
if((nt===1)||(nt==3)){
return false;
}
}
return true;
};
},"contains":function(name,_3d8){
var cz=_3d8.charAt(0);
if(cz=="\""||cz=="'"){
_3d8=_3d8.slice(1,-1);
}
return function(elem){
return (elem.innerHTML.indexOf(_3d8)>=0);
};
},"not":function(name,_3dc){
var p=_372(_3dc)[0];
var _3de={el:1};
if(p.tag!="*"){
_3de.tag=1;
}
if(!p.classes.length){
_3de.classes=1;
}
var ntf=_3e0(p,_3de);
return function(elem){
return (!ntf(elem));
};
},"nth-child":function(name,_3e3){
var pi=parseInt;
if(_3e3=="odd"){
return _3c7;
}else{
if(_3e3=="even"){
return _3c5;
}
}
if(_3e3.indexOf("n")!=-1){
var _3e5=_3e3.split("n",2);
var pred=_3e5[0]?((_3e5[0]=="-")?-1:pi(_3e5[0])):1;
var idx=_3e5[1]?pi(_3e5[1]):0;
var lb=0,ub=-1;
if(pred>0){
if(idx<0){
idx=(idx%pred)&&(pred+(idx%pred));
}else{
if(idx>0){
if(idx>=pred){
lb=idx-idx%pred;
}
idx=idx%pred;
}
}
}else{
if(pred<0){
pred*=-1;
if(idx>0){
ub=idx;
idx=idx%pred;
}
}
}
if(pred>0){
return function(elem){
var i=_3bc(elem);
return (i>=lb)&&(ub<0||i<=ub)&&((i%pred)==idx);
};
}else{
_3e3=idx;
}
}
var _3ec=pi(_3e3);
return function(elem){
return (_3bc(elem)==_3ec);
};
}};
var _3ee=(d.isIE)?function(cond){
var clc=cond.toLowerCase();
if(clc=="class"){
cond="className";
}
return function(elem){
return (_370?elem.getAttribute(cond):elem[cond]||elem[clc]);
};
}:function(cond){
return function(elem){
return (elem&&elem.getAttribute&&elem.hasAttribute(cond));
};
};
var _3e0=function(_3f4,_3f5){
if(!_3f4){
return _371;
}
_3f5=_3f5||{};
var ff=null;
if(!("el" in _3f5)){
ff=_38e(ff,_395);
}
if(!("tag" in _3f5)){
if(_3f4.tag!="*"){
ff=_38e(ff,function(elem){
return (elem&&(elem.tagName==_3f4.getTag()));
});
}
}
if(!("classes" in _3f5)){
each(_3f4.classes,function(_3f8,idx,arr){
var re=new RegExp("(?:^|\\s)"+_3f8+"(?:\\s|$)");
ff=_38e(ff,function(elem){
return re.test(elem.className);
});
ff.count=idx;
});
}
if(!("pseudos" in _3f5)){
each(_3f4.pseudos,function(_3fd){
var pn=_3fd.name;
if(_3c9[pn]){
ff=_38e(ff,_3c9[pn](pn,_3fd.value));
}
});
}
if(!("attrs" in _3f5)){
each(_3f4.attrs,function(attr){
var _400;
var a=attr.attr;
if(attr.type&&_39b[attr.type]){
_400=_39b[attr.type](a,attr.matchFor);
}else{
if(a.length){
_400=_3ee(a);
}
}
if(_400){
ff=_38e(ff,_400);
}
});
}
if(!("id" in _3f5)){
if(_3f4.id){
ff=_38e(ff,function(elem){
return (!!elem&&(elem.id==_3f4.id));
});
}
}
if(!ff){
if(!("default" in _3f5)){
ff=_371;
}
}
return ff;
};
var _403=function(_404){
return function(node,ret,bag){
while(node=node[_ns]){
if(_3b4&&(!_395(node))){
continue;
}
if((!bag||_408(node,bag))&&_404(node)){
ret.push(node);
}
break;
}
return ret;
};
};
var _409=function(_40a){
return function(root,ret,bag){
var te=root[_ns];
while(te){
if(_3b7(te)){
if(bag&&!_408(te,bag)){
break;
}
if(_40a(te)){
ret.push(te);
}
}
te=te[_ns];
}
return ret;
};
};
var _40f=function(_410){
_410=_410||_371;
return function(root,ret,bag){
var te,x=0,tret=root[_36e];
while(te=tret[x++]){
if(_3b7(te)&&(!bag||_408(te,bag))&&(_410(te,x))){
ret.push(te);
}
}
return ret;
};
};
var _417=function(node,root){
var pn=node.parentNode;
while(pn){
if(pn==root){
break;
}
pn=pn.parentNode;
}
return !!pn;
};
var _41b={};
var _41c=function(_41d){
var _41e=_41b[_41d.query];
if(_41e){
return _41e;
}
var io=_41d.infixOper;
var oper=(io?io.oper:"");
var _421=_3e0(_41d,{el:1});
var qt=_41d.tag;
var _423=("*"==qt);
var ecs=_36c()["getElementsByClassName"];
if(!oper){
if(_41d.id){
_421=(!_41d.loops&&_423)?_371:_3e0(_41d,{el:1,id:1});
_41e=function(root,arr){
var te=d.byId(_41d.id,(root.ownerDocument||root));
if(!te||!_421(te)){
return;
}
if(9==root.nodeType){
return _391(te,arr);
}else{
if(_417(te,root)){
return _391(te,arr);
}
}
};
}else{
if(ecs&&/\{\s*\[native code\]\s*\}/.test(String(ecs))&&_41d.classes.length&&!_36d){
_421=_3e0(_41d,{el:1,classes:1,id:1});
var _428=_41d.classes.join(" ");
_41e=function(root,arr,bag){
var ret=_391(0,arr),te,x=0;
var tret=root.getElementsByClassName(_428);
while((te=tret[x++])){
if(_421(te,root)&&_408(te,bag)){
ret.push(te);
}
}
return ret;
};
}else{
if(!_423&&!_41d.loops){
_41e=function(root,arr,bag){
var ret=_391(0,arr),te,x=0;
var tret=root.getElementsByTagName(_41d.getTag());
while((te=tret[x++])){
if(_408(te,bag)){
ret.push(te);
}
}
return ret;
};
}else{
_421=_3e0(_41d,{el:1,tag:1,id:1});
_41e=function(root,arr,bag){
var ret=_391(0,arr),te,x=0;
var tret=root.getElementsByTagName(_41d.getTag());
while((te=tret[x++])){
if(_421(te,root)&&_408(te,bag)){
ret.push(te);
}
}
return ret;
};
}
}
}
}else{
var _43e={el:1};
if(_423){
_43e.tag=1;
}
_421=_3e0(_41d,_43e);
if("+"==oper){
_41e=_403(_421);
}else{
if("~"==oper){
_41e=_409(_421);
}else{
if(">"==oper){
_41e=_40f(_421);
}
}
}
}
return _41b[_41d.query]=_41e;
};
var _43f=function(root,_441){
var _442=_391(root),qp,x,te,qpl=_441.length,bag,ret;
for(var i=0;i<qpl;i++){
ret=[];
qp=_441[i];
x=_442.length-1;
if(x>0){
bag={};
ret.nozip=true;
}
var gef=_41c(qp);
while(te=_442[x--]){
gef(te,ret,bag);
}
if(!ret.length){
break;
}
_442=ret;
}
return ret;
};
var _44b={},_44c={};
var _44d=function(_44e){
var _44f=_372(trim(_44e));
if(_44f.length==1){
var tef=_41c(_44f[0]);
return function(root){
var r=tef(root,new qlc());
if(r){
r.nozip=true;
}
return r;
};
}
return function(root){
return _43f(root,_44f);
};
};
var nua=navigator.userAgent;
var wk="WebKit/";
var _456=(d.isWebKit&&(nua.indexOf(wk)>0)&&(parseFloat(nua.split(wk)[1])>528));
var _457=d.isIE?"commentStrip":"nozip";
var qsa="querySelectorAll";
var _459=(!!_36c()[qsa]&&(!d.isSafari||(d.isSafari>3.1)||_456));
var _45a=function(_45b,_45c){
if(_459){
var _45d=_44c[_45b];
if(_45d&&!_45c){
return _45d;
}
}
var _45e=_44b[_45b];
if(_45e){
return _45e;
}
var qcz=_45b.charAt(0);
var _460=(-1==_45b.indexOf(" "));
if((_45b.indexOf("#")>=0)&&(_460)){
_45c=true;
}
var _461=(_459&&(!_45c)&&(_36f.indexOf(qcz)==-1)&&(!d.isIE||(_45b.indexOf(":")==-1))&&(!(_36d&&(_45b.indexOf(".")>=0)))&&(_45b.indexOf(":contains")==-1)&&(_45b.indexOf("|=")==-1));
if(_461){
var tq=(_36f.indexOf(_45b.charAt(_45b.length-1))>=0)?(_45b+" *"):_45b;
return _44c[_45b]=function(root){
try{
if(!((9==root.nodeType)||_460)){
throw "";
}
var r=root[qsa](tq);
r[_457]=true;
return r;
}
catch(e){
return _45a(_45b,true)(root);
}
};
}else{
var _465=_45b.split(/\s*,\s*/);
return _44b[_45b]=((_465.length<2)?_44d(_45b):function(root){
var _467=0,ret=[],tp;
while((tp=_465[_467++])){
ret=ret.concat(_44d(tp)(root));
}
return ret;
});
}
};
var _46a=0;
var _46b=d.isIE?function(node){
if(_370){
return (node.getAttribute("_uid")||node.setAttribute("_uid",++_46a)||_46a);
}else{
return node.uniqueID;
}
}:function(node){
return (node._uid||(node._uid=++_46a));
};
var _408=function(node,bag){
if(!bag){
return 1;
}
var id=_46b(node);
if(!bag[id]){
return bag[id]=1;
}
return 0;
};
var _471="_zipIdx";
var _zip=function(arr){
if(arr&&arr.nozip){
return (qlc._wrap)?qlc._wrap(arr):arr;
}
var ret=new qlc();
if(!arr||!arr.length){
return ret;
}
if(arr[0]){
ret.push(arr[0]);
}
if(arr.length<2){
return ret;
}
_46a++;
if(d.isIE&&_370){
var _475=_46a+"";
arr[0].setAttribute(_471,_475);
for(var x=1,te;te=arr[x];x++){
if(arr[x].getAttribute(_471)!=_475){
ret.push(te);
}
te.setAttribute(_471,_475);
}
}else{
if(d.isIE&&arr.commentStrip){
try{
for(var x=1,te;te=arr[x];x++){
if(_395(te)){
ret.push(te);
}
}
}
catch(e){
}
}else{
if(arr[0]){
arr[0][_471]=_46a;
}
for(var x=1,te;te=arr[x];x++){
if(arr[x][_471]!=_46a){
ret.push(te);
}
te[_471]=_46a;
}
}
}
return ret;
};
d.query=function(_478,root){
qlc=d._queryListCtor;
if(!_478){
return new qlc();
}
if(_478.constructor==qlc){
return _478;
}
if(!_36b(_478)){
return new qlc(_478);
}
if(_36b(root)){
root=d.byId(root);
if(!root){
return new qlc();
}
}
root=root||_36c();
var od=root.ownerDocument||root.documentElement;
_370=(root.contentType&&root.contentType=="application/xml")||(d.isOpera&&(root.doctype||od.toString()=="[object XMLDocument]"))||(!!od)&&(d.isIE?od.xml:(root.xmlVersion||od.xmlVersion));
var r=_45a(_478)(root);
if(r&&r.nozip&&!qlc._wrap){
return r;
}
return _zip(r);
};
d.query.pseudos=_3c9;
d._filterQueryResult=function(_47c,_47d){
var _47e=new d._queryListCtor();
var _47f=_3e0(_372(_47d)[0]);
for(var x=0,te;te=_47c[x];x++){
if(_47f(te)){
_47e.push(te);
}
}
return _47e;
};
})(this["queryPortability"]||this["acme"]||dojo);
}
if(!dojo._hasResource["dojo._base.xhr"]){
dojo._hasResource["dojo._base.xhr"]=true;
dojo.provide("dojo._base.xhr");
(function(){
var _d=dojo;
function _483(obj,name,_486){
var val=obj[name];
if(_d.isString(val)){
obj[name]=[val,_486];
}else{
if(_d.isArray(val)){
val.push(_486);
}else{
obj[name]=_486;
}
}
};
dojo.formToObject=function(_488){
var ret={};
var _48a="file|submit|image|reset|button|";
_d.forEach(dojo.byId(_488).elements,function(item){
var _in=item.name;
var type=(item.type||"").toLowerCase();
if(_in&&type&&_48a.indexOf(type)==-1&&!item.disabled){
if(type=="radio"||type=="checkbox"){
if(item.checked){
_483(ret,_in,item.value);
}
}else{
if(item.multiple){
ret[_in]=[];
_d.query("option",item).forEach(function(opt){
if(opt.selected){
_483(ret,_in,opt.value);
}
});
}else{
_483(ret,_in,item.value);
if(type=="image"){
ret[_in+".x"]=ret[_in+".y"]=ret[_in].x=ret[_in].y=0;
}
}
}
}
});
return ret;
};
dojo.objectToQuery=function(map){
var enc=encodeURIComponent;
var _491=[];
var _492={};
for(var name in map){
var _494=map[name];
if(_494!=_492[name]){
var _495=enc(name)+"=";
if(_d.isArray(_494)){
for(var i=0;i<_494.length;i++){
_491.push(_495+enc(_494[i]));
}
}else{
_491.push(_495+enc(_494));
}
}
}
return _491.join("&");
};
dojo.formToQuery=function(_497){
return _d.objectToQuery(_d.formToObject(_497));
};
dojo.formToJson=function(_498,_499){
return _d.toJson(_d.formToObject(_498),_499);
};
dojo.queryToObject=function(str){
var ret={};
var qp=str.split("&");
var dec=decodeURIComponent;
_d.forEach(qp,function(item){
if(item.length){
var _49f=item.split("=");
var name=dec(_49f.shift());
var val=dec(_49f.join("="));
if(_d.isString(ret[name])){
ret[name]=[ret[name]];
}
if(_d.isArray(ret[name])){
ret[name].push(val);
}else{
ret[name]=val;
}
}
});
return ret;
};
dojo._blockAsync=false;
dojo._contentHandlers={text:function(xhr){
return xhr.responseText;
},json:function(xhr){
return _d.fromJson(xhr.responseText||null);
},"json-comment-filtered":function(xhr){
if(!dojo.config.useCommentedJson){
console.warn("Consider using the standard mimetype:application/json."+" json-commenting can introduce security issues. To"+" decrease the chances of hijacking, use the standard the 'json' handler and"+" prefix your json with: {}&&\n"+"Use djConfig.useCommentedJson=true to turn off this message.");
}
var _4a5=xhr.responseText;
var _4a6=_4a5.indexOf("/*");
var _4a7=_4a5.lastIndexOf("*/");
if(_4a6==-1||_4a7==-1){
throw new Error("JSON was not comment filtered");
}
return _d.fromJson(_4a5.substring(_4a6+2,_4a7));
},javascript:function(xhr){
return _d.eval(xhr.responseText);
},xml:function(xhr){
var _4aa=xhr.responseXML;
if(_d.isIE&&(!_4aa||!_4aa.documentElement)){
var ms=function(n){
return "MSXML"+n+".DOMDocument";
};
var dp=["Microsoft.XMLDOM",ms(6),ms(4),ms(3),ms(2)];
_d.some(dp,function(p){
try{
var dom=new ActiveXObject(p);
dom.async=false;
dom.loadXML(xhr.responseText);
_4aa=dom;
}
catch(e){
return false;
}
return true;
});
}
return _4aa;
}};
dojo._contentHandlers["json-comment-optional"]=function(xhr){
var _4b1=_d._contentHandlers;
if(xhr.responseText&&xhr.responseText.indexOf("/*")!=-1){
return _4b1["json-comment-filtered"](xhr);
}else{
return _4b1["json"](xhr);
}
};
dojo._ioSetArgs=function(args,_4b3,_4b4,_4b5){
var _4b6={args:args,url:args.url};
var _4b7=null;
if(args.form){
var form=_d.byId(args.form);
var _4b9=form.getAttributeNode("action");
_4b6.url=_4b6.url||(_4b9?_4b9.value:null);
_4b7=_d.formToObject(form);
}
var _4ba=[{}];
if(_4b7){
_4ba.push(_4b7);
}
if(args.content){
_4ba.push(args.content);
}
if(args.preventCache){
_4ba.push({"dojo.preventCache":new Date().valueOf()});
}
_4b6.query=_d.objectToQuery(_d.mixin.apply(null,_4ba));
_4b6.handleAs=args.handleAs||"text";
var d=new _d.Deferred(_4b3);
d.addCallbacks(_4b4,function(_4bc){
return _4b5(_4bc,d);
});
var ld=args.load;
if(ld&&_d.isFunction(ld)){
d.addCallback(function(_4be){
return ld.call(args,_4be,_4b6);
});
}
var err=args.error;
if(err&&_d.isFunction(err)){
d.addErrback(function(_4c0){
return err.call(args,_4c0,_4b6);
});
}
var _4c1=args.handle;
if(_4c1&&_d.isFunction(_4c1)){
d.addBoth(function(_4c2){
return _4c1.call(args,_4c2,_4b6);
});
}
d.ioArgs=_4b6;
return d;
};
var _4c3=function(dfd){
dfd.canceled=true;
var xhr=dfd.ioArgs.xhr;
var _at=typeof xhr.abort;
if(_at=="function"||_at=="object"||_at=="unknown"){
xhr.abort();
}
var err=dfd.ioArgs.error;
if(!err){
err=new Error("xhr cancelled");
err.dojoType="cancel";
}
return err;
};
var _4c8=function(dfd){
var ret=_d._contentHandlers[dfd.ioArgs.handleAs](dfd.ioArgs.xhr);
return ret===undefined?null:ret;
};
var _4cb=function(_4cc,dfd){
console.error(_4cc);
return _4cc;
};
var _4ce=null;
var _4cf=[];
var _4d0=function(){
var now=(new Date()).getTime();
if(!_d._blockAsync){
for(var i=0,tif;i<_4cf.length&&(tif=_4cf[i]);i++){
var dfd=tif.dfd;
var func=function(){
if(!dfd||dfd.canceled||!tif.validCheck(dfd)){
_4cf.splice(i--,1);
}else{
if(tif.ioCheck(dfd)){
_4cf.splice(i--,1);
tif.resHandle(dfd);
}else{
if(dfd.startTime){
if(dfd.startTime+(dfd.ioArgs.args.timeout||0)<now){
_4cf.splice(i--,1);
var err=new Error("timeout exceeded");
err.dojoType="timeout";
dfd.errback(err);
dfd.cancel();
}
}
}
}
};
if(dojo.config.debugAtAllCosts){
func.call(this);
}else{
try{
func.call(this);
}
catch(e){
dfd.errback(e);
}
}
}
}
if(!_4cf.length){
clearInterval(_4ce);
_4ce=null;
return;
}
};
dojo._ioCancelAll=function(){
try{
_d.forEach(_4cf,function(i){
try{
i.dfd.cancel();
}
catch(e){
}
});
}
catch(e){
}
};
if(_d.isIE){
_d.addOnWindowUnload(_d._ioCancelAll);
}
_d._ioWatch=function(dfd,_4d9,_4da,_4db){
var args=dfd.ioArgs.args;
if(args.timeout){
dfd.startTime=(new Date()).getTime();
}
_4cf.push({dfd:dfd,validCheck:_4d9,ioCheck:_4da,resHandle:_4db});
if(!_4ce){
_4ce=setInterval(_4d0,50);
}
if(args.sync){
_4d0();
}
};
var _4dd="application/x-www-form-urlencoded";
var _4de=function(dfd){
return dfd.ioArgs.xhr.readyState;
};
var _4e0=function(dfd){
return 4==dfd.ioArgs.xhr.readyState;
};
var _4e2=function(dfd){
var xhr=dfd.ioArgs.xhr;
if(_d._isDocumentOk(xhr)){
dfd.callback(dfd);
}else{
var err=new Error("Unable to load "+dfd.ioArgs.url+" status:"+xhr.status);
err.status=xhr.status;
err.responseText=xhr.responseText;
dfd.errback(err);
}
};
dojo._ioAddQueryToUrl=function(_4e6){
if(_4e6.query.length){
_4e6.url+=(_4e6.url.indexOf("?")==-1?"?":"&")+_4e6.query;
_4e6.query=null;
}
};
dojo.xhr=function(_4e7,args,_4e9){
var dfd=_d._ioSetArgs(args,_4c3,_4c8,_4cb);
dfd.ioArgs.xhr=_d._xhrObj(dfd.ioArgs.args);
if(_4e9){
if("postData" in args){
dfd.ioArgs.query=args.postData;
}else{
if("putData" in args){
dfd.ioArgs.query=args.putData;
}
}
}else{
_d._ioAddQueryToUrl(dfd.ioArgs);
}
var _4eb=dfd.ioArgs;
var xhr=_4eb.xhr;
xhr.open(_4e7,_4eb.url,args.sync!==true,args.user||undefined,args.password||undefined);
if(args.headers){
for(var hdr in args.headers){
if(hdr.toLowerCase()==="content-type"&&!args.contentType){
args.contentType=args.headers[hdr];
}else{
xhr.setRequestHeader(hdr,args.headers[hdr]);
}
}
}
xhr.setRequestHeader("Content-Type",args.contentType||_4dd);
if(!args.headers||!args.headers["X-Requested-With"]){
xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");
}
if(dojo.config.debugAtAllCosts){
xhr.send(_4eb.query);
}else{
try{
xhr.send(_4eb.query);
}
catch(e){
dfd.ioArgs.error=e;
dfd.cancel();
}
}
_d._ioWatch(dfd,_4de,_4e0,_4e2);
xhr=null;
return dfd;
};
dojo.xhrGet=function(args){
return _d.xhr("GET",args);
};
dojo.rawXhrPost=dojo.xhrPost=function(args){
return _d.xhr("POST",args,true);
};
dojo.rawXhrPut=dojo.xhrPut=function(args){
return _d.xhr("PUT",args,true);
};
dojo.xhrDelete=function(args){
return _d.xhr("DELETE",args);
};
})();
}
if(!dojo._hasResource["dojo._base.fx"]){
dojo._hasResource["dojo._base.fx"]=true;
dojo.provide("dojo._base.fx");
(function(){
var d=dojo;
var _4f3=d.mixin;
dojo._Line=function(_4f4,end){
this.start=_4f4;
this.end=end;
};
dojo._Line.prototype.getValue=function(n){
return ((this.end-this.start)*n)+this.start;
};
d.declare("dojo._Animation",null,{constructor:function(args){
_4f3(this,args);
if(d.isArray(this.curve)){
this.curve=new d._Line(this.curve[0],this.curve[1]);
}
},duration:350,repeat:0,rate:10,_percent:0,_startRepeatCount:0,_fire:function(evt,args){
if(this[evt]){
if(dojo.config.debugAtAllCosts){
this[evt].apply(this,args||[]);
}else{
try{
this[evt].apply(this,args||[]);
}
catch(e){
console.error("exception in animation handler for:",evt);
console.error(e);
}
}
}
return this;
},play:function(_4fa,_4fb){
var _t=this;
if(_t._delayTimer){
_t._clearTimer();
}
if(_4fb){
_t._stopTimer();
_t._active=_t._paused=false;
_t._percent=0;
}else{
if(_t._active&&!_t._paused){
return _t;
}
}
_t._fire("beforeBegin");
var de=_4fa||_t.delay,_p=dojo.hitch(_t,"_play",_4fb);
if(de>0){
_t._delayTimer=setTimeout(_p,de);
return _t;
}
_p();
return _t;
},_play:function(_4ff){
var _t=this;
if(_t._delayTimer){
_t._clearTimer();
}
_t._startTime=new Date().valueOf();
if(_t._paused){
_t._startTime-=_t.duration*_t._percent;
}
_t._endTime=_t._startTime+_t.duration;
_t._active=true;
_t._paused=false;
var _501=_t.curve.getValue(_t._percent);
if(!_t._percent){
if(!_t._startRepeatCount){
_t._startRepeatCount=_t.repeat;
}
_t._fire("onBegin",[_501]);
}
_t._fire("onPlay",[_501]);
_t._cycle();
return _t;
},pause:function(){
var _t=this;
if(_t._delayTimer){
_t._clearTimer();
}
_t._stopTimer();
if(!_t._active){
return _t;
}
_t._paused=true;
_t._fire("onPause",[_t.curve.getValue(_t._percent)]);
return _t;
},gotoPercent:function(_503,_504){
var _t=this;
_t._stopTimer();
_t._active=_t._paused=true;
_t._percent=_503;
if(_504){
_t.play();
}
return _t;
},stop:function(_506){
var _t=this;
if(_t._delayTimer){
_t._clearTimer();
}
if(!_t._timer){
return _t;
}
_t._stopTimer();
if(_506){
_t._percent=1;
}
_t._fire("onStop",[_t.curve.getValue(_t._percent)]);
_t._active=_t._paused=false;
return _t;
},status:function(){
if(this._active){
return this._paused?"paused":"playing";
}
return "stopped";
},_cycle:function(){
var _t=this;
if(_t._active){
var curr=new Date().valueOf();
var step=(curr-_t._startTime)/(_t._endTime-_t._startTime);
if(step>=1){
step=1;
}
_t._percent=step;
if(_t.easing){
step=_t.easing(step);
}
_t._fire("onAnimate",[_t.curve.getValue(step)]);
if(_t._percent<1){
_t._startTimer();
}else{
_t._active=false;
if(_t.repeat>0){
_t.repeat--;
_t.play(null,true);
}else{
if(_t.repeat==-1){
_t.play(null,true);
}else{
if(_t._startRepeatCount){
_t.repeat=_t._startRepeatCount;
_t._startRepeatCount=0;
}
}
}
_t._percent=0;
_t._fire("onEnd");
_t._stopTimer();
}
}
return _t;
},_clearTimer:function(){
clearTimeout(this._delayTimer);
delete this._delayTimer;
}});
var ctr=0,_50c=[],_50d=null,_50e={run:function(){
}};
dojo._Animation.prototype._startTimer=function(){
if(!this._timer){
this._timer=d.connect(_50e,"run",this,"_cycle");
ctr++;
}
if(!_50d){
_50d=setInterval(d.hitch(_50e,"run"),this.rate);
}
};
dojo._Animation.prototype._stopTimer=function(){
if(this._timer){
d.disconnect(this._timer);
this._timer=null;
ctr--;
}
if(ctr<=0){
clearInterval(_50d);
_50d=null;
ctr=0;
}
};
var _50f=d.isIE?function(node){
var ns=node.style;
if(!ns.width.length&&d.style(node,"width")=="auto"){
ns.width="auto";
}
}:function(){
};
dojo._fade=function(args){
args.node=d.byId(args.node);
var _513=_4f3({properties:{}},args),_514=(_513.properties.opacity={});
_514.start=!("start" in _513)?function(){
return +d.style(_513.node,"opacity")||0;
}:_513.start;
_514.end=_513.end;
var anim=d.animateProperty(_513);
d.connect(anim,"beforeBegin",d.partial(_50f,_513.node));
return anim;
};
dojo.fadeIn=function(args){
return d._fade(_4f3({end:1},args));
};
dojo.fadeOut=function(args){
return d._fade(_4f3({end:0},args));
};
dojo._defaultEasing=function(n){
return 0.5+((Math.sin((n+1.5)*Math.PI))/2);
};
var _519=function(_51a){
this._properties=_51a;
for(var p in _51a){
var prop=_51a[p];
if(prop.start instanceof d.Color){
prop.tempColor=new d.Color();
}
}
};
_519.prototype.getValue=function(r){
var ret={};
for(var p in this._properties){
var prop=this._properties[p],_521=prop.start;
if(_521 instanceof d.Color){
ret[p]=d.blendColors(_521,prop.end,r,prop.tempColor).toCss();
}else{
if(!d.isArray(_521)){
ret[p]=((prop.end-_521)*r)+_521+(p!="opacity"?prop.units||"px":0);
}
}
}
return ret;
};
dojo.animateProperty=function(args){
args.node=d.byId(args.node);
if(!args.easing){
args.easing=d._defaultEasing;
}
var anim=new d._Animation(args);
d.connect(anim,"beforeBegin",anim,function(){
var pm={};
for(var p in this.properties){
if(p=="width"||p=="height"){
this.node.display="block";
}
var prop=this.properties[p];
prop=pm[p]=_4f3({},(d.isObject(prop)?prop:{end:prop}));
if(d.isFunction(prop.start)){
prop.start=prop.start();
}
if(d.isFunction(prop.end)){
prop.end=prop.end();
}
var _527=(p.toLowerCase().indexOf("color")>=0);
function _528(node,p){
var v={height:node.offsetHeight,width:node.offsetWidth}[p];
if(v!==undefined){
return v;
}
v=d.style(node,p);
return (p=="opacity")?+v:(_527?v:parseFloat(v));
};
if(!("end" in prop)){
prop.end=_528(this.node,p);
}else{
if(!("start" in prop)){
prop.start=_528(this.node,p);
}
}
if(_527){
prop.start=new d.Color(prop.start);
prop.end=new d.Color(prop.end);
}else{
prop.start=(p=="opacity")?+prop.start:parseFloat(prop.start);
}
}
this.curve=new _519(pm);
});
d.connect(anim,"onAnimate",d.hitch(d,"style",anim.node));
return anim;
};
dojo.anim=function(node,_52d,_52e,_52f,_530,_531){
return d.animateProperty({node:node,duration:_52e||d._Animation.prototype.duration,properties:_52d,easing:_52f,onEnd:_530}).play(_531||0);
};
})();
}
if(!dojo._hasResource["dojo._base.browser"]){
dojo._hasResource["dojo._base.browser"]=true;
dojo.provide("dojo._base.browser");
dojo.forEach(dojo.config.require,function(i){
dojo["require"](i);
});
}
if(dojo.config.afterOnLoad&&dojo.isBrowser){
window.setTimeout(dojo._loadInit,1000);
}
})();

if(!dojo._hasResource["dojo.data.util.simpleFetch"]){
dojo._hasResource["dojo.data.util.simpleFetch"]=true;
dojo.provide("dojo.data.util.simpleFetch");
dojo.data.util.simpleFetch.fetch=function(_1){
_1=_1||{};
if(!_1.store){
_1.store=this;
}
var _2=this;
var _3=function(_4,_5){
if(_5.onError){
var _6=_5.scope||dojo.global;
_5.onError.call(_6,_4,_5);
}
};
var _7=function(_8,_9){
var _a=_9.abort||null;
var _b=false;
var _c=_9.start?_9.start:0;
var _d=(_9.count&&(_9.count!==Infinity))?(_c+_9.count):_8.length;
_9.abort=function(){
_b=true;
if(_a){
_a.call(_9);
}
};
var _e=_9.scope||dojo.global;
if(!_9.store){
_9.store=_2;
}
if(_9.onBegin){
_9.onBegin.call(_e,_8.length,_9);
}
if(_9.sort){
_8.sort(dojo.data.util.sorter.createSortFunction(_9.sort,_2));
}
if(_9.onItem){
for(var i=_c;(i<_8.length)&&(i<_d);++i){
var _10=_8[i];
if(!_b){
_9.onItem.call(_e,_10,_9);
}
}
}
if(_9.onComplete&&!_b){
var _11=null;
if(!_9.onItem){
_11=_8.slice(_c,_d);
}
_9.onComplete.call(_e,_11,_9);
}
};
this._fetchItems(_1,_7,_3);
return _1;
};
}
if(!dojo._hasResource["dojo.data.ItemFileReadStore"]){
dojo._hasResource["dojo.data.ItemFileReadStore"]=true;
dojo.provide("dojo.data.ItemFileReadStore");
dojo.declare("dojo.data.ItemFileReadStore",null,{constructor:function(_12){
this._arrayOfAllItems=[];
this._arrayOfTopLevelItems=[];
this._loadFinished=false;
this._jsonFileUrl=_12.url;
this._jsonData=_12.data;
this._datatypeMap=_12.typeMap||{};
if(!this._datatypeMap["Date"]){
this._datatypeMap["Date"]={type:Date,deserialize:function(_13){
return dojo.date.stamp.fromISOString(_13);
}};
}
this._features={"dojo.data.api.Read":true,"dojo.data.api.Identity":true};
this._itemsByIdentity=null;
this._storeRefPropName="_S";
this._itemNumPropName="_0";
this._rootItemPropName="_RI";
this._reverseRefMap="_RRM";
this._loadInProgress=false;
this._queuedFetches=[];
if(_12.urlPreventCache!==undefined){
this.urlPreventCache=_12.urlPreventCache?true:false;
}
if(_12.clearOnClose){
this.clearOnClose=true;
}
},url:"",data:null,typeMap:null,clearOnClose:false,urlPreventCache:false,_assertIsItem:function(_14){
if(!this.isItem(_14)){
throw new Error("dojo.data.ItemFileReadStore: Invalid item argument.");
}
},_assertIsAttribute:function(_15){
if(typeof _15!=="string"){
throw new Error("dojo.data.ItemFileReadStore: Invalid attribute argument.");
}
},getValue:function(_16,_17,_18){
var _19=this.getValues(_16,_17);
return (_19.length>0)?_19[0]:_18;
},getValues:function(_1a,_1b){
this._assertIsItem(_1a);
this._assertIsAttribute(_1b);
return _1a[_1b]||[];
},getAttributes:function(_1c){
this._assertIsItem(_1c);
var _1d=[];
for(var key in _1c){
if((key!==this._storeRefPropName)&&(key!==this._itemNumPropName)&&(key!==this._rootItemPropName)&&(key!==this._reverseRefMap)){
_1d.push(key);
}
}
return _1d;
},hasAttribute:function(_1f,_20){
return this.getValues(_1f,_20).length>0;
},containsValue:function(_21,_22,_23){
var _24=undefined;
if(typeof _23==="string"){
_24=dojo.data.util.filter.patternToRegExp(_23,false);
}
return this._containsValue(_21,_22,_23,_24);
},_containsValue:function(_25,_26,_27,_28){
return dojo.some(this.getValues(_25,_26),function(_29){
if(_29!==null&&!dojo.isObject(_29)&&_28){
if(_29.toString().match(_28)){
return true;
}
}else{
if(_27===_29){
return true;
}
}
});
},isItem:function(_2a){
if(_2a&&_2a[this._storeRefPropName]===this){
if(this._arrayOfAllItems[_2a[this._itemNumPropName]]===_2a){
return true;
}
}
return false;
},isItemLoaded:function(_2b){
return this.isItem(_2b);
},loadItem:function(_2c){
this._assertIsItem(_2c.item);
},getFeatures:function(){
return this._features;
},getLabel:function(_2d){
if(this._labelAttr&&this.isItem(_2d)){
return this.getValue(_2d,this._labelAttr);
}
return undefined;
},getLabelAttributes:function(_2e){
if(this._labelAttr){
return [this._labelAttr];
}
return null;
},_fetchItems:function(_2f,_30,_31){
var _32=this;
var _33=function(_34,_35){
var _36=[];
var i,key;
if(_34.query){
var _39;
var _3a=_34.queryOptions?_34.queryOptions.ignoreCase:false;
var _3b={};
for(key in _34.query){
_39=_34.query[key];
if(typeof _39==="string"){
_3b[key]=dojo.data.util.filter.patternToRegExp(_39,_3a);
}
}
for(i=0;i<_35.length;++i){
var _3c=true;
var _3d=_35[i];
if(_3d===null){
_3c=false;
}else{
for(key in _34.query){
_39=_34.query[key];
if(!_32._containsValue(_3d,key,_39,_3b[key])){
_3c=false;
}
}
}
if(_3c){
_36.push(_3d);
}
}
_30(_36,_34);
}else{
for(i=0;i<_35.length;++i){
var _3e=_35[i];
if(_3e!==null){
_36.push(_3e);
}
}
_30(_36,_34);
}
};
if(this._loadFinished){
_33(_2f,this._getItemsArray(_2f.queryOptions));
}else{
if(this._jsonFileUrl){
if(this._loadInProgress){
this._queuedFetches.push({args:_2f,filter:_33});
}else{
this._loadInProgress=true;
var _3f={url:_32._jsonFileUrl,handleAs:"json-comment-optional",preventCache:this.urlPreventCache};
var _40=dojo.xhrGet(_3f);
_40.addCallback(function(_41){
try{
_32._getItemsFromLoadedData(_41);
_32._loadFinished=true;
_32._loadInProgress=false;
_33(_2f,_32._getItemsArray(_2f.queryOptions));
_32._handleQueuedFetches();
}
catch(e){
_32._loadFinished=true;
_32._loadInProgress=false;
_31(e,_2f);
}
});
_40.addErrback(function(_42){
_32._loadInProgress=false;
_31(_42,_2f);
});
var _43=null;
if(_2f.abort){
_43=_2f.abort;
}
_2f.abort=function(){
var df=_40;
if(df&&df.fired===-1){
df.cancel();
df=null;
}
if(_43){
_43.call(_2f);
}
};
}
}else{
if(this._jsonData){
try{
this._loadFinished=true;
this._getItemsFromLoadedData(this._jsonData);
this._jsonData=null;
_33(_2f,this._getItemsArray(_2f.queryOptions));
}
catch(e){
_31(e,_2f);
}
}else{
_31(new Error("dojo.data.ItemFileReadStore: No JSON source data was provided as either URL or a nested Javascript object."),_2f);
}
}
}
},_handleQueuedFetches:function(){
if(this._queuedFetches.length>0){
for(var i=0;i<this._queuedFetches.length;i++){
var _46=this._queuedFetches[i];
var _47=_46.args;
var _48=_46.filter;
if(_48){
_48(_47,this._getItemsArray(_47.queryOptions));
}else{
this.fetchItemByIdentity(_47);
}
}
this._queuedFetches=[];
}
},_getItemsArray:function(_49){
if(_49&&_49.deep){
return this._arrayOfAllItems;
}
return this._arrayOfTopLevelItems;
},close:function(_4a){
if(this.clearOnClose&&(this._jsonFileUrl!=="")){
this._arrayOfAllItems=[];
this._arrayOfTopLevelItems=[];
this._loadFinished=false;
this._itemsByIdentity=null;
this._loadInProgress=false;
this._queuedFetches=[];
}
},_getItemsFromLoadedData:function(_4b){
var _4c=false;
function _4d(_4e){
var _4f=((_4e!==null)&&(typeof _4e==="object")&&(!dojo.isArray(_4e)||_4c)&&(!dojo.isFunction(_4e))&&(_4e.constructor==Object||dojo.isArray(_4e))&&(typeof _4e._reference==="undefined")&&(typeof _4e._type==="undefined")&&(typeof _4e._value==="undefined"));
return _4f;
};
var _50=this;
function _51(_52){
_50._arrayOfAllItems.push(_52);
for(var _53 in _52){
var _54=_52[_53];
if(_54){
if(dojo.isArray(_54)){
var _55=_54;
for(var k=0;k<_55.length;++k){
var _57=_55[k];
if(_4d(_57)){
_51(_57);
}
}
}else{
if(_4d(_54)){
_51(_54);
}
}
}
}
};
this._labelAttr=_4b.label;
var i;
var _59;
this._arrayOfAllItems=[];
this._arrayOfTopLevelItems=_4b.items;
for(i=0;i<this._arrayOfTopLevelItems.length;++i){
_59=this._arrayOfTopLevelItems[i];
if(dojo.isArray(_59)){
_4c=true;
}
_51(_59);
_59[this._rootItemPropName]=true;
}
var _5a={};
var key;
for(i=0;i<this._arrayOfAllItems.length;++i){
_59=this._arrayOfAllItems[i];
for(key in _59){
if(key!==this._rootItemPropName){
var _5c=_59[key];
if(_5c!==null){
if(!dojo.isArray(_5c)){
_59[key]=[_5c];
}
}else{
_59[key]=[null];
}
}
_5a[key]=key;
}
}
while(_5a[this._storeRefPropName]){
this._storeRefPropName+="_";
}
while(_5a[this._itemNumPropName]){
this._itemNumPropName+="_";
}
while(_5a[this._reverseRefMap]){
this._reverseRefMap+="_";
}
var _5d;
var _5e=_4b.identifier;
if(_5e){
this._itemsByIdentity={};
this._features["dojo.data.api.Identity"]=_5e;
for(i=0;i<this._arrayOfAllItems.length;++i){
_59=this._arrayOfAllItems[i];
_5d=_59[_5e];
var _5f=_5d[0];
if(!this._itemsByIdentity[_5f]){
this._itemsByIdentity[_5f]=_59;
}else{
if(this._jsonFileUrl){
throw new Error("dojo.data.ItemFileReadStore:  The json data as specified by: ["+this._jsonFileUrl+"] is malformed.  Items within the list have identifier: ["+_5e+"].  Value collided: ["+_5f+"]");
}else{
if(this._jsonData){
throw new Error("dojo.data.ItemFileReadStore:  The json data provided by the creation arguments is malformed.  Items within the list have identifier: ["+_5e+"].  Value collided: ["+_5f+"]");
}
}
}
}
}else{
this._features["dojo.data.api.Identity"]=Number;
}
for(i=0;i<this._arrayOfAllItems.length;++i){
_59=this._arrayOfAllItems[i];
_59[this._storeRefPropName]=this;
_59[this._itemNumPropName]=i;
}
for(i=0;i<this._arrayOfAllItems.length;++i){
_59=this._arrayOfAllItems[i];
for(key in _59){
_5d=_59[key];
for(var j=0;j<_5d.length;++j){
_5c=_5d[j];
if(_5c!==null&&typeof _5c=="object"){
if(_5c._type&&_5c._value){
var _61=_5c._type;
var _62=this._datatypeMap[_61];
if(!_62){
throw new Error("dojo.data.ItemFileReadStore: in the typeMap constructor arg, no object class was specified for the datatype '"+_61+"'");
}else{
if(dojo.isFunction(_62)){
_5d[j]=new _62(_5c._value);
}else{
if(dojo.isFunction(_62.deserialize)){
_5d[j]=_62.deserialize(_5c._value);
}else{
throw new Error("dojo.data.ItemFileReadStore: Value provided in typeMap was neither a constructor, nor a an object with a deserialize function");
}
}
}
}
if(_5c._reference){
var _63=_5c._reference;
if(!dojo.isObject(_63)){
_5d[j]=this._itemsByIdentity[_63];
}else{
for(var k=0;k<this._arrayOfAllItems.length;++k){
var _65=this._arrayOfAllItems[k];
var _66=true;
for(var _67 in _63){
if(_65[_67]!=_63[_67]){
_66=false;
}
}
if(_66){
_5d[j]=_65;
}
}
}
if(this.referenceIntegrity){
var _68=_5d[j];
if(this.isItem(_68)){
this._addReferenceToMap(_68,_59,key);
}
}
}else{
if(this.isItem(_5c)){
if(this.referenceIntegrity){
this._addReferenceToMap(_5c,_59,key);
}
}
}
}
}
}
}
},_addReferenceToMap:function(_69,_6a,_6b){
},getIdentity:function(_6c){
var _6d=this._features["dojo.data.api.Identity"];
if(_6d===Number){
return _6c[this._itemNumPropName];
}else{
var _6e=_6c[_6d];
if(_6e){
return _6e[0];
}
}
return null;
},fetchItemByIdentity:function(_6f){
var _70;
var _71;
if(!this._loadFinished){
var _72=this;
if(this._jsonFileUrl){
if(this._loadInProgress){
this._queuedFetches.push({args:_6f});
}else{
this._loadInProgress=true;
var _73={url:_72._jsonFileUrl,handleAs:"json-comment-optional",preventCache:this.urlPreventCache};
var _74=dojo.xhrGet(_73);
_74.addCallback(function(_75){
var _76=_6f.scope?_6f.scope:dojo.global;
try{
_72._getItemsFromLoadedData(_75);
_72._loadFinished=true;
_72._loadInProgress=false;
_70=_72._getItemByIdentity(_6f.identity);
if(_6f.onItem){
_6f.onItem.call(_76,_70);
}
_72._handleQueuedFetches();
}
catch(error){
_72._loadInProgress=false;
if(_6f.onError){
_6f.onError.call(_76,error);
}
}
});
_74.addErrback(function(_77){
_72._loadInProgress=false;
if(_6f.onError){
var _78=_6f.scope?_6f.scope:dojo.global;
_6f.onError.call(_78,_77);
}
});
}
}else{
if(this._jsonData){
_72._getItemsFromLoadedData(_72._jsonData);
_72._jsonData=null;
_72._loadFinished=true;
_70=_72._getItemByIdentity(_6f.identity);
if(_6f.onItem){
_71=_6f.scope?_6f.scope:dojo.global;
_6f.onItem.call(_71,_70);
}
}
}
}else{
_70=this._getItemByIdentity(_6f.identity);
if(_6f.onItem){
_71=_6f.scope?_6f.scope:dojo.global;
_6f.onItem.call(_71,_70);
}
}
},_getItemByIdentity:function(_79){
var _7a=null;
if(this._itemsByIdentity){
_7a=this._itemsByIdentity[_79];
}else{
_7a=this._arrayOfAllItems[_79];
}
if(_7a===undefined){
_7a=null;
}
return _7a;
},getIdentityAttributes:function(_7b){
var _7c=this._features["dojo.data.api.Identity"];
if(_7c===Number){
return null;
}else{
return [_7c];
}
},_forceLoad:function(){
var _7d=this;
if(this._jsonFileUrl){
var _7e={url:_7d._jsonFileUrl,handleAs:"json-comment-optional",preventCache:this.urlPreventCache,sync:true};
var _7f=dojo.xhrGet(_7e);
_7f.addCallback(function(_80){
try{
if(_7d._loadInProgress!==true&&!_7d._loadFinished){
_7d._getItemsFromLoadedData(_80);
_7d._loadFinished=true;
}else{
if(_7d._loadInProgress){
throw new Error("dojo.data.ItemFileReadStore:  Unable to perform a synchronous load, an async load is in progress.");
}
}
}
catch(e){
console.log(e);
throw e;
}
});
_7f.addErrback(function(_81){
throw _81;
});
}else{
if(this._jsonData){
_7d._getItemsFromLoadedData(_7d._jsonData);
_7d._jsonData=null;
_7d._loadFinished=true;
}
}
}});
dojo.extend(dojo.data.ItemFileReadStore,dojo.data.util.simpleFetch);
}
if(!dojo._hasResource["dojo.fx"]){
dojo._hasResource["dojo.fx"]=true;
dojo.provide("dojo.fx");
(function(){
var d=dojo,_83={_fire:function(evt,_85){
if(this[evt]){
this[evt].apply(this,_85||[]);
}
return this;
}};
var _86=function(_87){
this._index=-1;
this._animations=_87||[];
this._current=this._onAnimateCtx=this._onEndCtx=null;
this.duration=0;
d.forEach(this._animations,function(a){
this.duration+=a.duration;
if(a.delay){
this.duration+=a.delay;
}
},this);
};
d.extend(_86,{_onAnimate:function(){
this._fire("onAnimate",arguments);
},_onEnd:function(){
d.disconnect(this._onAnimateCtx);
d.disconnect(this._onEndCtx);
this._onAnimateCtx=this._onEndCtx=null;
if(this._index+1==this._animations.length){
this._fire("onEnd");
}else{
this._current=this._animations[++this._index];
this._onAnimateCtx=d.connect(this._current,"onAnimate",this,"_onAnimate");
this._onEndCtx=d.connect(this._current,"onEnd",this,"_onEnd");
this._current.play(0,true);
}
},play:function(_89,_8a){
if(!this._current){
this._current=this._animations[this._index=0];
}
if(!_8a&&this._current.status()=="playing"){
return this;
}
var _8b=d.connect(this._current,"beforeBegin",this,function(){
this._fire("beforeBegin");
}),_8c=d.connect(this._current,"onBegin",this,function(arg){
this._fire("onBegin",arguments);
}),_8e=d.connect(this._current,"onPlay",this,function(arg){
this._fire("onPlay",arguments);
d.disconnect(_8b);
d.disconnect(_8c);
d.disconnect(_8e);
});
if(this._onAnimateCtx){
d.disconnect(this._onAnimateCtx);
}
this._onAnimateCtx=d.connect(this._current,"onAnimate",this,"_onAnimate");
if(this._onEndCtx){
d.disconnect(this._onEndCtx);
}
this._onEndCtx=d.connect(this._current,"onEnd",this,"_onEnd");
this._current.play.apply(this._current,arguments);
return this;
},pause:function(){
if(this._current){
var e=d.connect(this._current,"onPause",this,function(arg){
this._fire("onPause",arguments);
d.disconnect(e);
});
this._current.pause();
}
return this;
},gotoPercent:function(_92,_93){
this.pause();
var _94=this.duration*_92;
this._current=null;
d.some(this._animations,function(a){
if(a.duration<=_94){
this._current=a;
return true;
}
_94-=a.duration;
return false;
});
if(this._current){
this._current.gotoPercent(_94/this._current.duration,_93);
}
return this;
},stop:function(_96){
if(this._current){
if(_96){
for(;this._index+1<this._animations.length;++this._index){
this._animations[this._index].stop(true);
}
this._current=this._animations[this._index];
}
var e=d.connect(this._current,"onStop",this,function(arg){
this._fire("onStop",arguments);
d.disconnect(e);
});
this._current.stop();
}
return this;
},status:function(){
return this._current?this._current.status():"stopped";
},destroy:function(){
if(this._onAnimateCtx){
d.disconnect(this._onAnimateCtx);
}
if(this._onEndCtx){
d.disconnect(this._onEndCtx);
}
}});
d.extend(_86,_83);
dojo.fx.chain=function(_99){
return new _86(_99);
};
var _9a=function(_9b){
this._animations=_9b||[];
this._connects=[];
this._finished=0;
this.duration=0;
d.forEach(_9b,function(a){
var _9d=a.duration;
if(a.delay){
_9d+=a.delay;
}
if(this.duration<_9d){
this.duration=_9d;
}
this._connects.push(d.connect(a,"onEnd",this,"_onEnd"));
},this);
this._pseudoAnimation=new d._Animation({curve:[0,1],duration:this.duration});
var _9e=this;
d.forEach(["beforeBegin","onBegin","onPlay","onAnimate","onPause","onStop"],function(evt){
_9e._connects.push(d.connect(_9e._pseudoAnimation,evt,function(){
_9e._fire(evt,arguments);
}));
});
};
d.extend(_9a,{_doAction:function(_a0,_a1){
d.forEach(this._animations,function(a){
a[_a0].apply(a,_a1);
});
return this;
},_onEnd:function(){
if(++this._finished==this._animations.length){
this._fire("onEnd");
}
},_call:function(_a3,_a4){
var t=this._pseudoAnimation;
t[_a3].apply(t,_a4);
},play:function(_a6,_a7){
this._finished=0;
this._doAction("play",arguments);
this._call("play",arguments);
return this;
},pause:function(){
this._doAction("pause",arguments);
this._call("pause",arguments);
return this;
},gotoPercent:function(_a8,_a9){
var ms=this.duration*_a8;
d.forEach(this._animations,function(a){
a.gotoPercent(a.duration<ms?1:(ms/a.duration),_a9);
});
this._call("gotoPercent",arguments);
return this;
},stop:function(_ac){
this._doAction("stop",arguments);
this._call("stop",arguments);
return this;
},status:function(){
return this._pseudoAnimation.status();
},destroy:function(){
d.forEach(this._connects,dojo.disconnect);
}});
d.extend(_9a,_83);
dojo.fx.combine=function(_ad){
return new _9a(_ad);
};
dojo.fx.wipeIn=function(_ae){
_ae.node=d.byId(_ae.node);
var _af=_ae.node,s=_af.style,o;
var _b2=d.animateProperty(d.mixin({properties:{height:{start:function(){
o=s.overflow;
s.overflow="hidden";
if(s.visibility=="hidden"||s.display=="none"){
s.height="1px";
s.display="";
s.visibility="";
return 1;
}else{
var _b3=d.style(_af,"height");
return Math.max(_b3,1);
}
},end:function(){
return _af.scrollHeight;
}}}},_ae));
d.connect(_b2,"onEnd",function(){
s.height="auto";
s.overflow=o;
});
return _b2;
};
dojo.fx.wipeOut=function(_b4){
var _b5=_b4.node=d.byId(_b4.node),s=_b5.style,o;
var _b8=d.animateProperty(d.mixin({properties:{height:{end:1}}},_b4));
d.connect(_b8,"beforeBegin",function(){
o=s.overflow;
s.overflow="hidden";
s.display="";
});
d.connect(_b8,"onEnd",function(){
s.overflow=o;
s.height="auto";
s.display="none";
});
return _b8;
};
dojo.fx.slideTo=function(_b9){
var _ba=_b9.node=d.byId(_b9.node),top=null,_bc=null;
var _bd=(function(n){
return function(){
var cs=d.getComputedStyle(n);
var pos=cs.position;
top=(pos=="absolute"?n.offsetTop:parseInt(cs.top)||0);
_bc=(pos=="absolute"?n.offsetLeft:parseInt(cs.left)||0);
if(pos!="absolute"&&pos!="relative"){
var ret=d.coords(n,true);
top=ret.y;
_bc=ret.x;
n.style.position="absolute";
n.style.top=top+"px";
n.style.left=_bc+"px";
}
};
})(_ba);
_bd();
var _c2=d.animateProperty(d.mixin({properties:{top:_b9.top||0,left:_b9.left||0}},_b9));
d.connect(_c2,"beforeBegin",_c2,_bd);
return _c2;
};
})();
}
if(!dojo._hasResource["dijit._base.focus"]){
dojo._hasResource["dijit._base.focus"]=true;
dojo.provide("dijit._base.focus");
dojo.mixin(dijit,{_curFocus:null,_prevFocus:null,isCollapsed:function(){
var _c3=dojo.doc;
if(_c3.selection){
var s=_c3.selection;
if(s.type=="Text"){
return !s.createRange().htmlText.length;
}else{
return !s.createRange().length;
}
}else{
var _c5=dojo.global;
var _c6=_c5.getSelection();
if(dojo.isString(_c6)){
return !_c6;
}else{
return !_c6||_c6.isCollapsed||!_c6.toString();
}
}
},getBookmark:function(){
var _c7,_c8=dojo.doc.selection;
if(_c8){
var _c9=_c8.createRange();
if(_c8.type.toUpperCase()=="CONTROL"){
if(_c9.length){
_c7=[];
var i=0,len=_c9.length;
while(i<len){
_c7.push(_c9.item(i++));
}
}else{
_c7=null;
}
}else{
_c7=_c9.getBookmark();
}
}else{
if(window.getSelection){
_c8=dojo.global.getSelection();
if(_c8){
_c9=_c8.getRangeAt(0);
_c7=_c9.cloneRange();
}
}else{
console.warn("No idea how to store the current selection for this browser!");
}
}
return _c7;
},moveToBookmark:function(_cc){
var _cd=dojo.doc;
if(_cd.selection){
var _ce;
if(dojo.isArray(_cc)){
_ce=_cd.body.createControlRange();
dojo.forEach(_cc,function(n){
_ce.addElement(n);
});
}else{
_ce=_cd.selection.createRange();
_ce.moveToBookmark(_cc);
}
_ce.select();
}else{
var _d0=dojo.global.getSelection&&dojo.global.getSelection();
if(_d0&&_d0.removeAllRanges){
_d0.removeAllRanges();
_d0.addRange(_cc);
}else{
console.warn("No idea how to restore selection for this browser!");
}
}
},getFocus:function(_d1,_d2){
return {node:_d1&&dojo.isDescendant(dijit._curFocus,_d1.domNode)?dijit._prevFocus:dijit._curFocus,bookmark:!dojo.withGlobal(_d2||dojo.global,dijit.isCollapsed)?dojo.withGlobal(_d2||dojo.global,dijit.getBookmark):null,openedForWindow:_d2};
},focus:function(_d3){
if(!_d3){
return;
}
var _d4="node" in _d3?_d3.node:_d3,_d5=_d3.bookmark,_d6=_d3.openedForWindow;
if(_d4){
var _d7=(_d4.tagName.toLowerCase()=="iframe")?_d4.contentWindow:_d4;
if(_d7&&_d7.focus){
try{
_d7.focus();
}
catch(e){
}
}
dijit._onFocusNode(_d4);
}
if(_d5&&dojo.withGlobal(_d6||dojo.global,dijit.isCollapsed)){
if(_d6){
_d6.focus();
}
try{
dojo.withGlobal(_d6||dojo.global,dijit.moveToBookmark,null,[_d5]);
}
catch(e){
}
}
},_activeStack:[],registerIframe:function(_d8){
dijit.registerWin(_d8.contentWindow,_d8);
},registerWin:function(_d9,_da){
dojo.connect(_d9.document,"onmousedown",function(evt){
dijit._justMouseDowned=true;
setTimeout(function(){
dijit._justMouseDowned=false;
},0);
dijit._onTouchNode(_da||evt.target||evt.srcElement);
});
var doc=_d9.document;
if(doc){
if(dojo.isIE){
doc.attachEvent("onactivate",function(evt){
if(evt.srcElement.tagName.toLowerCase()!="#document"){
dijit._onFocusNode(_da||evt.srcElement);
}
});
doc.attachEvent("ondeactivate",function(evt){
dijit._onBlurNode(_da||evt.srcElement);
});
}else{
doc.addEventListener("focus",function(evt){
dijit._onFocusNode(_da||evt.target);
},true);
doc.addEventListener("blur",function(evt){
dijit._onBlurNode(_da||evt.target);
},true);
}
}
doc=null;
},_onBlurNode:function(_e1){
dijit._prevFocus=dijit._curFocus;
dijit._curFocus=null;
if(dijit._justMouseDowned){
return;
}
if(dijit._clearActiveWidgetsTimer){
clearTimeout(dijit._clearActiveWidgetsTimer);
}
dijit._clearActiveWidgetsTimer=setTimeout(function(){
delete dijit._clearActiveWidgetsTimer;
dijit._setStack([]);
dijit._prevFocus=null;
},100);
},_onTouchNode:function(_e2){
if(dijit._clearActiveWidgetsTimer){
clearTimeout(dijit._clearActiveWidgetsTimer);
delete dijit._clearActiveWidgetsTimer;
}
var _e3=[];
try{
while(_e2){
if(_e2.dijitPopupParent){
_e2=dijit.byId(_e2.dijitPopupParent).domNode;
}else{
if(_e2.tagName&&_e2.tagName.toLowerCase()=="body"){
if(_e2===dojo.body()){
break;
}
_e2=dijit.getDocumentWindow(_e2.ownerDocument).frameElement;
}else{
var id=_e2.getAttribute&&_e2.getAttribute("widgetId");
if(id){
_e3.unshift(id);
}
_e2=_e2.parentNode;
}
}
}
}
catch(e){
}
dijit._setStack(_e3);
},_onFocusNode:function(_e5){
if(!_e5){
return;
}
if(_e5.nodeType==9){
return;
}
dijit._onTouchNode(_e5);
if(_e5==dijit._curFocus){
return;
}
if(dijit._curFocus){
dijit._prevFocus=dijit._curFocus;
}
dijit._curFocus=_e5;
dojo.publish("focusNode",[_e5]);
},_setStack:function(_e6){
var _e7=dijit._activeStack;
dijit._activeStack=_e6;
for(var _e8=0;_e8<Math.min(_e7.length,_e6.length);_e8++){
if(_e7[_e8]!=_e6[_e8]){
break;
}
}
for(var i=_e7.length-1;i>=_e8;i--){
var _ea=dijit.byId(_e7[i]);
if(_ea){
_ea._focused=false;
_ea._hasBeenBlurred=true;
if(_ea._onBlur){
_ea._onBlur();
}
if(_ea._setStateClass){
_ea._setStateClass();
}
dojo.publish("widgetBlur",[_ea]);
}
}
for(i=_e8;i<_e6.length;i++){
_ea=dijit.byId(_e6[i]);
if(_ea){
_ea._focused=true;
if(_ea._onFocus){
_ea._onFocus();
}
if(_ea._setStateClass){
_ea._setStateClass();
}
dojo.publish("widgetFocus",[_ea]);
}
}
}});
dojo.addOnLoad(function(){
dijit.registerWin(window);
});
}
if(!dojo._hasResource["dijit._base.manager"]){
dojo._hasResource["dijit._base.manager"]=true;
dojo.provide("dijit._base.manager");
dojo.declare("dijit.WidgetSet",null,{constructor:function(){
this._hash={};
},add:function(_eb){
if(this._hash[_eb.id]){
throw new Error("Tried to register widget with id=="+_eb.id+" but that id is already registered");
}
this._hash[_eb.id]=_eb;
},remove:function(id){
delete this._hash[id];
},forEach:function(_ed){
for(var id in this._hash){
_ed(this._hash[id]);
}
},filter:function(_ef){
var res=new dijit.WidgetSet();
this.forEach(function(_f1){
if(_ef(_f1)){
res.add(_f1);
}
});
return res;
},byId:function(id){
return this._hash[id];
},byClass:function(cls){
return this.filter(function(_f4){
return _f4.declaredClass==cls;
});
}});
dijit.registry=new dijit.WidgetSet();
dijit._widgetTypeCtr={};
dijit.getUniqueId=function(_f5){
var id;
do{
id=_f5+"_"+(_f5 in dijit._widgetTypeCtr?++dijit._widgetTypeCtr[_f5]:dijit._widgetTypeCtr[_f5]=0);
}while(dijit.byId(id));
return id;
};
dijit.findWidgets=function(_f7){
var _f8=[];
function _f9(_fa){
var _fb=dojo.isIE?_fa.children:_fa.childNodes,i=0,_fd;
while(_fd=_fb[i++]){
if(_fd.nodeType!=1){
continue;
}
var _fe=_fd.getAttribute("widgetId");
if(_fe){
var _ff=dijit.byId(_fe);
_f8.push(_ff);
}else{
_f9(_fd);
}
}
};
_f9(_f7);
return _f8;
};
if(dojo.isIE){
dojo.addOnWindowUnload(function(){
dojo.forEach(dijit.findWidgets(dojo.body()),function(_100){
if(_100.destroyRecursive){
_100.destroyRecursive();
}else{
if(_100.destroy){
_100.destroy();
}
}
});
});
}
dijit.byId=function(id){
return (dojo.isString(id))?dijit.registry.byId(id):id;
};
dijit.byNode=function(node){
return dijit.registry.byId(node.getAttribute("widgetId"));
};
dijit.getEnclosingWidget=function(node){
while(node){
if(node.getAttribute&&node.getAttribute("widgetId")){
return dijit.registry.byId(node.getAttribute("widgetId"));
}
node=node.parentNode;
}
return null;
};
dijit._tabElements={area:true,button:true,input:true,object:true,select:true,textarea:true};
dijit._isElementShown=function(elem){
var _105=dojo.style(elem);
return (_105.visibility!="hidden")&&(_105.visibility!="collapsed")&&(_105.display!="none")&&(dojo.attr(elem,"type")!="hidden");
};
dijit.isTabNavigable=function(elem){
if(dojo.hasAttr(elem,"disabled")){
return false;
}
var _107=dojo.hasAttr(elem,"tabindex");
var _108=dojo.attr(elem,"tabindex");
if(_107&&_108>=0){
return true;
}
var name=elem.nodeName.toLowerCase();
if(((name=="a"&&dojo.hasAttr(elem,"href"))||dijit._tabElements[name])&&(!_107||_108>=0)){
return true;
}
return false;
};
dijit._getTabNavigable=function(root){
var _10b,last,_10d,_10e,_10f,_110;
var _111=function(_112){
dojo.query("> *",_112).forEach(function(_113){
var _114=dijit._isElementShown(_113);
if(_114&&dijit.isTabNavigable(_113)){
var _115=dojo.attr(_113,"tabindex");
if(!dojo.hasAttr(_113,"tabindex")||_115==0){
if(!_10b){
_10b=_113;
}
last=_113;
}else{
if(_115>0){
if(!_10d||_115<_10e){
_10e=_115;
_10d=_113;
}
if(!_10f||_115>=_110){
_110=_115;
_10f=_113;
}
}
}
}
if(_114&&_113.nodeName.toUpperCase()!="SELECT"){
_111(_113);
}
});
};
if(dijit._isElementShown(root)){
_111(root);
}
return {first:_10b,last:last,lowest:_10d,highest:_10f};
};
dijit.getFirstInTabbingOrder=function(root){
var _117=dijit._getTabNavigable(dojo.byId(root));
return _117.lowest?_117.lowest:_117.first;
};
dijit.getLastInTabbingOrder=function(root){
var _119=dijit._getTabNavigable(dojo.byId(root));
return _119.last?_119.last:_119.highest;
};
dijit.defaultDuration=dojo.config["defaultDuration"]||200;
}
if(!dojo._hasResource["dojo.AdapterRegistry"]){
dojo._hasResource["dojo.AdapterRegistry"]=true;
dojo.provide("dojo.AdapterRegistry");
dojo.AdapterRegistry=function(_11a){
this.pairs=[];
this.returnWrappers=_11a||false;
};
dojo.extend(dojo.AdapterRegistry,{register:function(name,_11c,wrap,_11e,_11f){
this.pairs[((_11f)?"unshift":"push")]([name,_11c,wrap,_11e]);
},match:function(){
for(var i=0;i<this.pairs.length;i++){
var pair=this.pairs[i];
if(pair[1].apply(this,arguments)){
if((pair[3])||(this.returnWrappers)){
return pair[2];
}else{
return pair[2].apply(this,arguments);
}
}
}
throw new Error("No match found");
},unregister:function(name){
for(var i=0;i<this.pairs.length;i++){
var pair=this.pairs[i];
if(pair[0]==name){
this.pairs.splice(i,1);
return true;
}
}
return false;
}});
}
if(!dojo._hasResource["dijit._base.place"]){
dojo._hasResource["dijit._base.place"]=true;
dojo.provide("dijit._base.place");
dijit.getViewport=function(){
var _125=(dojo.doc.compatMode=="BackCompat")?dojo.body():dojo.doc.documentElement;
var _126=dojo._docScroll();
return {w:_125.clientWidth,h:_125.clientHeight,l:_126.x,t:_126.y};
};
dijit.placeOnScreen=function(node,pos,_129,_12a){
var _12b=dojo.map(_129,function(_12c){
var c={corner:_12c,pos:{x:pos.x,y:pos.y}};
if(_12a){
c.pos.x+=_12c.charAt(1)=="L"?_12a.x:-_12a.x;
c.pos.y+=_12c.charAt(0)=="T"?_12a.y:-_12a.y;
}
return c;
});
return dijit._place(node,_12b);
};
dijit._place=function(node,_12f,_130){
var view=dijit.getViewport();
if(!node.parentNode||String(node.parentNode.tagName).toLowerCase()!="body"){
dojo.body().appendChild(node);
}
var best=null;
dojo.some(_12f,function(_133){
var _134=_133.corner;
var pos=_133.pos;
if(_130){
_130(node,_133.aroundCorner,_134);
}
var _136=node.style;
var _137=_136.display;
var _138=_136.visibility;
_136.visibility="hidden";
_136.display="";
var mb=dojo.marginBox(node);
_136.display=_137;
_136.visibility=_138;
var _13a=(_134.charAt(1)=="L"?pos.x:Math.max(view.l,pos.x-mb.w)),_13b=(_134.charAt(0)=="T"?pos.y:Math.max(view.t,pos.y-mb.h)),endX=(_134.charAt(1)=="L"?Math.min(view.l+view.w,_13a+mb.w):pos.x),endY=(_134.charAt(0)=="T"?Math.min(view.t+view.h,_13b+mb.h):pos.y),_13e=endX-_13a,_13f=endY-_13b,_140=(mb.w-_13e)+(mb.h-_13f);
if(best==null||_140<best.overflow){
best={corner:_134,aroundCorner:_133.aroundCorner,x:_13a,y:_13b,w:_13e,h:_13f,overflow:_140};
}
return !_140;
});
node.style.left=best.x+"px";
node.style.top=best.y+"px";
if(best.overflow&&_130){
_130(node,best.aroundCorner,best.corner);
}
return best;
};
dijit.placeOnScreenAroundNode=function(node,_142,_143,_144){
_142=dojo.byId(_142);
var _145=_142.style.display;
_142.style.display="";
var _146=_142.offsetWidth;
var _147=_142.offsetHeight;
var _148=dojo.coords(_142,true);
_142.style.display=_145;
return dijit._placeOnScreenAroundRect(node,_148.x,_148.y,_146,_147,_143,_144);
};
dijit.placeOnScreenAroundRectangle=function(node,_14a,_14b,_14c){
return dijit._placeOnScreenAroundRect(node,_14a.x,_14a.y,_14a.width,_14a.height,_14b,_14c);
};
dijit._placeOnScreenAroundRect=function(node,x,y,_150,_151,_152,_153){
var _154=[];
for(var _155 in _152){
_154.push({aroundCorner:_155,corner:_152[_155],pos:{x:x+(_155.charAt(1)=="L"?0:_150),y:y+(_155.charAt(0)=="T"?0:_151)}});
}
return dijit._place(node,_154,_153);
};
dijit.placementRegistry=new dojo.AdapterRegistry();
dijit.placementRegistry.register("node",function(n,x){
return typeof x=="object"&&typeof x.offsetWidth!="undefined"&&typeof x.offsetHeight!="undefined";
},dijit.placeOnScreenAroundNode);
dijit.placementRegistry.register("rect",function(n,x){
return typeof x=="object"&&"x" in x&&"y" in x&&"width" in x&&"height" in x;
},dijit.placeOnScreenAroundRectangle);
dijit.placeOnScreenAroundElement=function(node,_15b,_15c,_15d){
return dijit.placementRegistry.match.apply(dijit.placementRegistry,arguments);
};
}
if(!dojo._hasResource["dijit._base.sniff"]){
dojo._hasResource["dijit._base.sniff"]=true;
dojo.provide("dijit._base.sniff");
(function(){
var d=dojo,html=d.doc.documentElement,ie=d.isIE,_161=d.isOpera,maj=Math.floor,ff=d.isFF,_164=d.boxModel.replace(/-/,""),_165={dj_ie:ie,dj_ie6:maj(ie)==6,dj_ie7:maj(ie)==7,dj_iequirks:ie&&d.isQuirks,dj_opera:_161,dj_opera8:maj(_161)==8,dj_opera9:maj(_161)==9,dj_khtml:d.isKhtml,dj_webkit:d.isWebKit,dj_safari:d.isSafari,dj_gecko:d.isMozilla,dj_ff2:maj(ff)==2,dj_ff3:maj(ff)==3};
_165["dj_"+_164]=true;
for(var p in _165){
if(_165[p]){
if(html.className){
html.className+=" "+p;
}else{
html.className=p;
}
}
}
dojo._loaders.unshift(function(){
if(!dojo._isBodyLtr()){
html.className+=" dijitRtl";
for(var p in _165){
if(_165[p]){
html.className+=" "+p+"-rtl";
}
}
}
});
})();
}
if(!dojo._hasResource["dijit._base.wai"]){
dojo._hasResource["dijit._base.wai"]=true;
dojo.provide("dijit._base.wai");
dijit.wai={onload:function(){
var div=dojo.create("div",{id:"a11yTestNode",style:{cssText:"border: 1px solid;"+"border-color:red green;"+"position: absolute;"+"height: 5px;"+"top: -999px;"+"background-image: url(\""+(dojo.config.blankGif||dojo.moduleUrl("dojo","resources/blank.gif"))+"\");"}},dojo.body());
var cs=dojo.getComputedStyle(div);
if(cs){
var _16a=cs.backgroundImage;
var _16b=(cs.borderTopColor==cs.borderRightColor)||(_16a!=null&&(_16a=="none"||_16a=="url(invalid-url:)"));
dojo[_16b?"addClass":"removeClass"](dojo.body(),"dijit_a11y");
if(dojo.isIE){
div.outerHTML="";
}else{
dojo.body().removeChild(div);
}
}
}};
if(dojo.isIE||dojo.isMoz){
dojo._loaders.unshift(dijit.wai.onload);
}
dojo.mixin(dijit,{_XhtmlRoles:/banner|contentinfo|definition|main|navigation|search|note|secondary|seealso/,hasWaiRole:function(elem,role){
var _16e=this.getWaiRole(elem);
return role?(_16e.indexOf(role)>-1):(_16e.length>0);
},getWaiRole:function(elem){
return dojo.trim((dojo.attr(elem,"role")||"").replace(this._XhtmlRoles,"").replace("wairole:",""));
},setWaiRole:function(elem,role){
var _172=dojo.attr(elem,"role")||"";
if(dojo.isFF<3||!this._XhtmlRoles.test(_172)){
dojo.attr(elem,"role",dojo.isFF<3?"wairole:"+role:role);
}else{
if((" "+_172+" ").indexOf(" "+role+" ")<0){
var _173=dojo.trim(_172.replace(this._XhtmlRoles,""));
var _174=dojo.trim(_172.replace(_173,""));
dojo.attr(elem,"role",_174+(_174?" ":"")+role);
}
}
},removeWaiRole:function(elem,role){
var _177=dojo.attr(elem,"role");
if(!_177){
return;
}
if(role){
var _178=dojo.isFF<3?"wairole:"+role:role;
var t=dojo.trim((" "+_177+" ").replace(" "+_178+" "," "));
dojo.attr(elem,"role",t);
}else{
elem.removeAttribute("role");
}
},hasWaiState:function(elem,_17b){
if(dojo.isFF<3){
return elem.hasAttributeNS("http://www.w3.org/2005/07/aaa",_17b);
}
return elem.hasAttribute?elem.hasAttribute("aria-"+_17b):!!elem.getAttribute("aria-"+_17b);
},getWaiState:function(elem,_17d){
if(dojo.isFF<3){
return elem.getAttributeNS("http://www.w3.org/2005/07/aaa",_17d);
}
return elem.getAttribute("aria-"+_17d)||"";
},setWaiState:function(elem,_17f,_180){
if(dojo.isFF<3){
elem.setAttributeNS("http://www.w3.org/2005/07/aaa","aaa:"+_17f,_180);
}else{
elem.setAttribute("aria-"+_17f,_180);
}
},removeWaiState:function(elem,_182){
if(dojo.isFF<3){
elem.removeAttributeNS("http://www.w3.org/2005/07/aaa",_182);
}else{
elem.removeAttribute("aria-"+_182);
}
}});
}
if(!dojo._hasResource["dijit._base"]){
dojo._hasResource["dijit._base"]=true;
dojo.provide("dijit._base");
}
if(!dojo._hasResource["dijit._Widget"]){
dojo._hasResource["dijit._Widget"]=true;
dojo.provide("dijit._Widget");
dojo.require("dijit._base");
dojo.connect(dojo,"connect",function(_183,_184){
if(_183&&dojo.isFunction(_183._onConnect)){
_183._onConnect(_184);
}
});
dijit._connectOnUseEventHandler=function(_185){
};
(function(){
var _186={};
var _187=function(dc){
if(!_186[dc]){
var r=[];
var _18a;
var _18b=dojo.getObject(dc).prototype;
for(var _18c in _18b){
if(dojo.isFunction(_18b[_18c])&&(_18a=_18c.match(/^_set([a-zA-Z]*)Attr$/))&&_18a[1]){
r.push(_18a[1].charAt(0).toLowerCase()+_18a[1].substr(1));
}
}
_186[dc]=r;
}
return _186[dc]||[];
};
dojo.declare("dijit._Widget",null,{id:"",lang:"",dir:"","class":"",style:"",title:"",srcNodeRef:null,domNode:null,containerNode:null,attributeMap:{id:"",dir:"",lang:"","class":"",style:"",title:""},_deferredConnects:{onClick:"",onDblClick:"",onKeyDown:"",onKeyPress:"",onKeyUp:"",onMouseMove:"",onMouseDown:"",onMouseOut:"",onMouseOver:"",onMouseLeave:"",onMouseEnter:"",onMouseUp:""},onClick:dijit._connectOnUseEventHandler,onDblClick:dijit._connectOnUseEventHandler,onKeyDown:dijit._connectOnUseEventHandler,onKeyPress:dijit._connectOnUseEventHandler,onKeyUp:dijit._connectOnUseEventHandler,onMouseDown:dijit._connectOnUseEventHandler,onMouseMove:dijit._connectOnUseEventHandler,onMouseOut:dijit._connectOnUseEventHandler,onMouseOver:dijit._connectOnUseEventHandler,onMouseLeave:dijit._connectOnUseEventHandler,onMouseEnter:dijit._connectOnUseEventHandler,onMouseUp:dijit._connectOnUseEventHandler,_blankGif:(dojo.config.blankGif||dojo.moduleUrl("dojo","resources/blank.gif")),postscript:function(_18d,_18e){
this.create(_18d,_18e);
},create:function(_18f,_190){
this.srcNodeRef=dojo.byId(_190);
this._connects=[];
this._deferredConnects=dojo.clone(this._deferredConnects);
for(var attr in this.attributeMap){
delete this._deferredConnects[attr];
}
for(attr in this._deferredConnects){
if(this[attr]!==dijit._connectOnUseEventHandler){
delete this._deferredConnects[attr];
}
}
if(this.srcNodeRef&&(typeof this.srcNodeRef.id=="string")){
this.id=this.srcNodeRef.id;
}
if(_18f){
this.params=_18f;
dojo.mixin(this,_18f);
}
this.postMixInProperties();
if(!this.id){
this.id=dijit.getUniqueId(this.declaredClass.replace(/\./g,"_"));
}
dijit.registry.add(this);
this.buildRendering();
if(this.domNode){
this._applyAttributes();
var _192=this.srcNodeRef;
if(_192&&_192.parentNode){
_192.parentNode.replaceChild(this.domNode,_192);
}
for(attr in this.params){
this._onConnect(attr);
}
}
if(this.domNode){
this.domNode.setAttribute("widgetId",this.id);
}
this.postCreate();
if(this.srcNodeRef&&!this.srcNodeRef.parentNode){
delete this.srcNodeRef;
}
this._created=true;
},_applyAttributes:function(){
var _193=function(attr,_195){
if((_195.params&&attr in _195.params)||_195[attr]){
_195.attr(attr,_195[attr]);
}
};
for(var attr in this.attributeMap){
_193(attr,this);
}
dojo.forEach(_187(this.declaredClass),function(a){
if(!(a in this.attributeMap)){
_193(a,this);
}
},this);
},postMixInProperties:function(){
},buildRendering:function(){
this.domNode=this.srcNodeRef||dojo.create("div");
},postCreate:function(){
},startup:function(){
this._started=true;
},destroyRecursive:function(_198){
this.destroyDescendants(_198);
this.destroy(_198);
},destroy:function(_199){
this.uninitialize();
dojo.forEach(this._connects,function(_19a){
dojo.forEach(_19a,dojo.disconnect);
});
dojo.forEach(this._supportingWidgets||[],function(w){
if(w.destroy){
w.destroy();
}
});
this.destroyRendering(_199);
dijit.registry.remove(this.id);
},destroyRendering:function(_19c){
if(this.bgIframe){
this.bgIframe.destroy(_19c);
delete this.bgIframe;
}
if(this.domNode){
if(_19c){
dojo.removeAttr(this.domNode,"widgetId");
}else{
dojo.destroy(this.domNode);
}
delete this.domNode;
}
if(this.srcNodeRef){
if(!_19c){
dojo.destroy(this.srcNodeRef);
}
delete this.srcNodeRef;
}
},destroyDescendants:function(_19d){
dojo.forEach(this.getChildren(),function(_19e){
if(_19e.destroyRecursive){
_19e.destroyRecursive(_19d);
}
});
},uninitialize:function(){
return false;
},onFocus:function(){
},onBlur:function(){
},_onFocus:function(e){
this.onFocus();
},_onBlur:function(){
this.onBlur();
},_onConnect:function(_1a0){
if(_1a0 in this._deferredConnects){
var _1a1=this[this._deferredConnects[_1a0]||"domNode"];
this.connect(_1a1,_1a0.toLowerCase(),_1a0);
delete this._deferredConnects[_1a0];
}
},_setClassAttr:function(_1a2){
var _1a3=this[this.attributeMap["class"]||"domNode"];
dojo.removeClass(_1a3,this["class"]);
this["class"]=_1a2;
dojo.addClass(_1a3,_1a2);
},_setStyleAttr:function(_1a4){
var _1a5=this[this.attributeMap["style"]||"domNode"];
if(dojo.isObject(_1a4)){
dojo.style(_1a5,_1a4);
}else{
if(_1a5.style.cssText){
_1a5.style.cssText+="; "+_1a4;
}else{
_1a5.style.cssText=_1a4;
}
}
this["style"]=_1a4;
},setAttribute:function(attr,_1a7){
dojo.deprecated(this.declaredClass+"::setAttribute() is deprecated. Use attr() instead.","","2.0");
this.attr(attr,_1a7);
},_attrToDom:function(attr,_1a9){
var _1aa=this.attributeMap[attr];
dojo.forEach(dojo.isArray(_1aa)?_1aa:[_1aa],function(_1ab){
var _1ac=this[_1ab.node||_1ab||"domNode"];
var type=_1ab.type||"attribute";
switch(type){
case "attribute":
if(dojo.isFunction(_1a9)){
_1a9=dojo.hitch(this,_1a9);
}
if(/^on[A-Z][a-zA-Z]*$/.test(attr)){
attr=attr.toLowerCase();
}
dojo.attr(_1ac,attr,_1a9);
break;
case "innerHTML":
_1ac.innerHTML=_1a9;
break;
case "class":
dojo.removeClass(_1ac,this[attr]);
dojo.addClass(_1ac,_1a9);
break;
}
},this);
this[attr]=_1a9;
},attr:function(name,_1af){
var args=arguments.length;
if(args==1&&!dojo.isString(name)){
for(var x in name){
this.attr(x,name[x]);
}
return this;
}
var _1b2=this._getAttrNames(name);
if(args==2){
if(this[_1b2.s]){
return this[_1b2.s](_1af)||this;
}else{
if(name in this.attributeMap){
this._attrToDom(name,_1af);
}
this[name]=_1af;
}
return this;
}else{
if(this[_1b2.g]){
return this[_1b2.g]();
}else{
return this[name];
}
}
},_attrPairNames:{},_getAttrNames:function(name){
var apn=this._attrPairNames;
if(apn[name]){
return apn[name];
}
var uc=name.charAt(0).toUpperCase()+name.substr(1);
return apn[name]={n:name+"Node",s:"_set"+uc+"Attr",g:"_get"+uc+"Attr"};
},toString:function(){
return "[Widget "+this.declaredClass+", "+(this.id||"NO ID")+"]";
},getDescendants:function(){
if(this.containerNode){
var list=dojo.query("[widgetId]",this.containerNode);
return list.map(dijit.byNode);
}else{
return [];
}
},getChildren:function(){
if(this.containerNode){
return dijit.findWidgets(this.containerNode);
}else{
return [];
}
},nodesWithKeyClick:["input","button"],connect:function(obj,_1b8,_1b9){
var d=dojo;
var dc=dojo.connect;
var _1bc=[];
if(_1b8=="ondijitclick"){
if(!this.nodesWithKeyClick[obj.nodeName]){
var m=d.hitch(this,_1b9);
_1bc.push(dc(obj,"onkeydown",this,function(e){
if(!d.isFF&&e.keyCode==d.keys.ENTER&&!e.ctrlKey&&!e.shiftKey&&!e.altKey&&!e.metaKey){
return m(e);
}else{
if(e.keyCode==d.keys.SPACE){
d.stopEvent(e);
}
}
}),dc(obj,"onkeyup",this,function(e){
if(e.keyCode==d.keys.SPACE&&!e.ctrlKey&&!e.shiftKey&&!e.altKey&&!e.metaKey){
return m(e);
}
}));
if(d.isFF){
_1bc.push(dc(obj,"onkeypress",this,function(e){
if(e.keyCode==d.keys.ENTER&&!e.ctrlKey&&!e.shiftKey&&!e.altKey&&!e.metaKey){
return m(e);
}
}));
}
}
_1b8="onclick";
}
_1bc.push(dc(obj,_1b8,this,_1b9));
this._connects.push(_1bc);
return _1bc;
},disconnect:function(_1c1){
for(var i=0;i<this._connects.length;i++){
if(this._connects[i]==_1c1){
dojo.forEach(_1c1,dojo.disconnect);
this._connects.splice(i,1);
return;
}
}
},isLeftToRight:function(){
return dojo._isBodyLtr();
},isFocusable:function(){
return this.focus&&(dojo.style(this.domNode,"display")!="none");
},placeAt:function(_1c3,_1c4){
if(_1c3["declaredClass"]&&_1c3["addChild"]){
_1c3.addChild(this,_1c4);
}else{
dojo.place(this.domNode,_1c3,_1c4);
}
return this;
}});
})();
}
if(!dojo._hasResource["dojo.string"]){
dojo._hasResource["dojo.string"]=true;
dojo.provide("dojo.string");
dojo.string.substitute=function(_1c5,map,_1c7,_1c8){
_1c8=_1c8||dojo.global;
_1c7=(!_1c7)?function(v){
return v;
}:dojo.hitch(_1c8,_1c7);
return _1c5.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g,function(_1ca,key,_1cc){
var _1cd=dojo.getObject(key,false,map);
if(_1cc){
_1cd=dojo.getObject(_1cc,false,_1c8).call(_1c8,_1cd,key);
}
return _1c7(_1cd,key).toString();
});
};
dojo.string.trim=String.prototype.trim?dojo.trim:function(str){
str=str.replace(/^\s+/,"");
for(var i=str.length-1;i>=0;i--){
if(/\S/.test(str.charAt(i))){
str=str.substring(0,i+1);
break;
}
}
return str;
};
}
if(!dojo._hasResource["dojo.parser"]){
dojo._hasResource["dojo.parser"]=true;
dojo.provide("dojo.parser");
dojo.parser=new function(){
var d=dojo;
var _1d1=d._scopeName+"Type";
var qry="["+_1d1+"]";
var _1d3=0,_1d4={};
var _1d5=function(_1d6,_1d7){
var nso=_1d7||_1d4;
if(dojo.isIE){
var cn=_1d6["__dojoNameCache"];
if(cn&&nso[cn]===_1d6){
return cn;
}
}
var name;
do{
name="__"+_1d3++;
}while(name in nso);
nso[name]=_1d6;
return name;
};
function _1db(_1dc){
if(d.isString(_1dc)){
return "string";
}
if(typeof _1dc=="number"){
return "number";
}
if(typeof _1dc=="boolean"){
return "boolean";
}
if(d.isFunction(_1dc)){
return "function";
}
if(d.isArray(_1dc)){
return "array";
}
if(_1dc instanceof Date){
return "date";
}
if(_1dc instanceof d._Url){
return "url";
}
return "object";
};
function _1dd(_1de,type){
switch(type){
case "string":
return _1de;
case "number":
return _1de.length?Number(_1de):NaN;
case "boolean":
return typeof _1de=="boolean"?_1de:!(_1de.toLowerCase()=="false");
case "function":
if(d.isFunction(_1de)){
_1de=_1de.toString();
_1de=d.trim(_1de.substring(_1de.indexOf("{")+1,_1de.length-1));
}
try{
if(_1de.search(/[^\w\.]+/i)!=-1){
_1de=_1d5(new Function(_1de),this);
}
return d.getObject(_1de,false);
}
catch(e){
return new Function();
}
case "array":
return _1de?_1de.split(/\s*,\s*/):[];
case "date":
switch(_1de){
case "":
return new Date("");
case "now":
return new Date();
default:
return d.date.stamp.fromISOString(_1de);
}
case "url":
return d.baseUrl+_1de;
default:
return d.fromJson(_1de);
}
};
var _1e0={};
function _1e1(_1e2){
if(!_1e0[_1e2]){
var cls=d.getObject(_1e2);
if(!d.isFunction(cls)){
throw new Error("Could not load class '"+_1e2+"'. Did you spell the name correctly and use a full path, like 'dijit.form.Button'?");
}
var _1e4=cls.prototype;
var _1e5={},_1e6={};
for(var name in _1e4){
if(name.charAt(0)=="_"){
continue;
}
if(name in _1e6){
continue;
}
var _1e8=_1e4[name];
_1e5[name]=_1db(_1e8);
}
_1e0[_1e2]={cls:cls,params:_1e5};
}
return _1e0[_1e2];
};
this._functionFromScript=function(_1e9){
var _1ea="";
var _1eb="";
var _1ec=_1e9.getAttribute("args");
if(_1ec){
d.forEach(_1ec.split(/\s*,\s*/),function(part,idx){
_1ea+="var "+part+" = arguments["+idx+"]; ";
});
}
var _1ef=_1e9.getAttribute("with");
if(_1ef&&_1ef.length){
d.forEach(_1ef.split(/\s*,\s*/),function(part){
_1ea+="with("+part+"){";
_1eb+="}";
});
}
return new Function(_1ea+_1e9.innerHTML+_1eb);
};
this.instantiate=function(_1f1,_1f2){
var _1f3=[];
_1f2=_1f2||{};
d.forEach(_1f1,function(node){
if(!node){
return;
}
var type=_1d1 in _1f2?_1f2[_1d1]:node.getAttribute(_1d1);
if(!type||!type.length){
return;
}
var _1f6=_1e1(type),_1f7=_1f6.cls,ps=_1f7._noScript||_1f7.prototype._noScript;
var _1f9={},_1fa=node.attributes;
for(var name in _1f6.params){
var item=name in _1f2?{value:_1f2[name],specified:true}:_1fa.getNamedItem(name);
if(!item||(!item.specified&&(!dojo.isIE||name.toLowerCase()!="value"))){
continue;
}
var _1fd=item.value;
switch(name){
case "class":
_1fd="className" in _1f2?_1f2.className:node.className;
break;
case "style":
_1fd="style" in _1f2?_1f2.style:(node.style&&node.style.cssText);
}
var _1fe=_1f6.params[name];
if(typeof _1fd=="string"){
_1f9[name]=_1dd(_1fd,_1fe);
}else{
_1f9[name]=_1fd;
}
}
if(!ps){
var _1ff=[],_200=[];
d.query("> script[type^='dojo/']",node).orphan().forEach(function(_201){
var _202=_201.getAttribute("event"),type=_201.getAttribute("type"),nf=d.parser._functionFromScript(_201);
if(_202){
if(type=="dojo/connect"){
_1ff.push({event:_202,func:nf});
}else{
_1f9[_202]=nf;
}
}else{
_200.push(nf);
}
});
}
var _204=_1f7["markupFactory"];
if(!_204&&_1f7["prototype"]){
_204=_1f7.prototype["markupFactory"];
}
var _205=_204?_204(_1f9,node,_1f7):new _1f7(_1f9,node);
_1f3.push(_205);
var _206=node.getAttribute("jsId");
if(_206){
d.setObject(_206,_205);
}
if(!ps){
d.forEach(_1ff,function(_207){
d.connect(_205,_207.event,null,_207.func);
});
d.forEach(_200,function(func){
func.call(_205);
});
}
});
d.forEach(_1f3,function(_209){
if(_209&&_209.startup&&!_209._started&&(!_209.getParent||!_209.getParent())){
_209.startup();
}
});
return _1f3;
};
this.parse=function(_20a){
var list=d.query(qry,_20a);
var _20c=this.instantiate(list);
return _20c;
};
}();
(function(){
var _20d=function(){
if(dojo.config["parseOnLoad"]==true){
dojo.parser.parse();
}
};
if(dojo.exists("dijit.wai.onload")&&(dijit.wai.onload===dojo._loaders[0])){
dojo._loaders.splice(1,0,_20d);
}else{
dojo._loaders.unshift(_20d);
}
})();
}
if(!dojo._hasResource["dijit._Templated"]){
dojo._hasResource["dijit._Templated"]=true;
dojo.provide("dijit._Templated");
dojo.declare("dijit._Templated",null,{templateString:null,templatePath:null,widgetsInTemplate:false,_skipNodeCache:false,_stringRepl:function(tmpl){
var _20f=this.declaredClass,_210=this;
return dojo.string.substitute(tmpl,this,function(_211,key){
if(key.charAt(0)=="!"){
_211=dojo.getObject(key.substr(1),false,_210);
}
if(typeof _211=="undefined"){
throw new Error(_20f+" template:"+key);
}
if(_211==null){
return "";
}
return key.charAt(0)=="!"?_211:_211.toString().replace(/"/g,"&quot;");
},this);
},buildRendering:function(){
var _213=dijit._Templated.getCachedTemplate(this.templatePath,this.templateString,this._skipNodeCache);
var node;
if(dojo.isString(_213)){
node=dojo._toDom(this._stringRepl(_213));
}else{
node=_213.cloneNode(true);
}
this.domNode=node;
this._attachTemplateNodes(node);
if(this.widgetsInTemplate){
var cw=(this._supportingWidgets=dojo.parser.parse(node));
this._attachTemplateNodes(cw,function(n,p){
return n[p];
});
}
this._fillContent(this.srcNodeRef);
},_fillContent:function(_218){
var dest=this.containerNode;
if(_218&&dest){
while(_218.hasChildNodes()){
dest.appendChild(_218.firstChild);
}
}
},_attachTemplateNodes:function(_21a,_21b){
_21b=_21b||function(n,p){
return n.getAttribute(p);
};
var _21e=dojo.isArray(_21a)?_21a:(_21a.all||_21a.getElementsByTagName("*"));
var x=dojo.isArray(_21a)?0:-1;
for(;x<_21e.length;x++){
var _220=(x==-1)?_21a:_21e[x];
if(this.widgetsInTemplate&&_21b(_220,"dojoType")){
continue;
}
var _221=_21b(_220,"dojoAttachPoint");
if(_221){
var _222,_223=_221.split(/\s*,\s*/);
while((_222=_223.shift())){
if(dojo.isArray(this[_222])){
this[_222].push(_220);
}else{
this[_222]=_220;
}
}
}
var _224=_21b(_220,"dojoAttachEvent");
if(_224){
var _225,_226=_224.split(/\s*,\s*/);
var trim=dojo.trim;
while((_225=_226.shift())){
if(_225){
var _228=null;
if(_225.indexOf(":")!=-1){
var _229=_225.split(":");
_225=trim(_229[0]);
_228=trim(_229[1]);
}else{
_225=trim(_225);
}
if(!_228){
_228=_225;
}
this.connect(_220,_225,_228);
}
}
}
var role=_21b(_220,"waiRole");
if(role){
dijit.setWaiRole(_220,role);
}
var _22b=_21b(_220,"waiState");
if(_22b){
dojo.forEach(_22b.split(/\s*,\s*/),function(_22c){
if(_22c.indexOf("-")!=-1){
var pair=_22c.split("-");
dijit.setWaiState(_220,pair[0],pair[1]);
}
});
}
}
}});
dijit._Templated._templateCache={};
dijit._Templated.getCachedTemplate=function(_22e,_22f,_230){
var _231=dijit._Templated._templateCache;
var key=_22f||_22e;
var _233=_231[key];
if(_233){
if(!_233.ownerDocument||_233.ownerDocument==dojo.doc){
return _233;
}
dojo.destroy(_233);
}
if(!_22f){
_22f=dijit._Templated._sanitizeTemplateString(dojo.trim(dojo._getText(_22e)));
}
_22f=dojo.string.trim(_22f);
if(_230||_22f.match(/\$\{([^\}]+)\}/g)){
return (_231[key]=_22f);
}else{
return (_231[key]=dojo._toDom(_22f));
}
};
dijit._Templated._sanitizeTemplateString=function(_234){
if(_234){
_234=_234.replace(/^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,"");
var _235=_234.match(/<body[^>]*>\s*([\s\S]+)\s*<\/body>/im);
if(_235){
_234=_235[1];
}
}else{
_234="";
}
return _234;
};
if(dojo.isIE){
dojo.addOnWindowUnload(function(){
var _236=dijit._Templated._templateCache;
for(var key in _236){
var _238=_236[key];
if(!isNaN(_238.nodeType)){
dojo.destroy(_238);
}
delete _236[key];
}
});
}
dojo.extend(dijit._Widget,{dojoAttachEvent:"",dojoAttachPoint:"",waiRole:"",waiState:""});
}
if(!dojo._hasResource["dijit._Contained"]){
dojo._hasResource["dijit._Contained"]=true;
dojo.provide("dijit._Contained");
dojo.declare("dijit._Contained",null,{getParent:function(){
for(var p=this.domNode.parentNode;p;p=p.parentNode){
var id=p.getAttribute&&p.getAttribute("widgetId");
if(id){
var _23b=dijit.byId(id);
return _23b.isContainer?_23b:null;
}
}
return null;
},_getSibling:function(_23c){
var node=this.domNode;
do{
node=node[_23c+"Sibling"];
}while(node&&node.nodeType!=1);
if(!node){
return null;
}
var id=node.getAttribute("widgetId");
return dijit.byId(id);
},getPreviousSibling:function(){
return this._getSibling("previous");
},getNextSibling:function(){
return this._getSibling("next");
},getIndexInParent:function(){
var p=this.getParent();
if(!p||!p.getIndexOfChild){
return -1;
}
return p.getIndexOfChild(this);
}});
}
if(!dojo._hasResource["dijit._Container"]){
dojo._hasResource["dijit._Container"]=true;
dojo.provide("dijit._Container");
dojo.declare("dijit._Container",null,{isContainer:true,buildRendering:function(){
this.inherited(arguments);
if(!this.containerNode){
this.containerNode=this.domNode;
}
},addChild:function(_240,_241){
var _242=this.containerNode;
if(_241&&typeof _241=="number"){
var _243=this.getChildren();
if(_243&&_243.length>=_241){
_242=_243[_241-1].domNode;
_241="after";
}
}
dojo.place(_240.domNode,_242,_241);
if(this._started&&!_240._started){
_240.startup();
}
},removeChild:function(_244){
if(typeof _244=="number"&&_244>0){
_244=this.getChildren()[_244];
}
if(!_244||!_244.domNode){
return;
}
var node=_244.domNode;
node.parentNode.removeChild(node);
},_nextElement:function(node){
do{
node=node.nextSibling;
}while(node&&node.nodeType!=1);
return node;
},_firstElement:function(node){
node=node.firstChild;
if(node&&node.nodeType!=1){
node=this._nextElement(node);
}
return node;
},getChildren:function(){
return dojo.query("> [widgetId]",this.containerNode).map(dijit.byNode);
},hasChildren:function(){
return !!this._firstElement(this.containerNode);
},destroyDescendants:function(_248){
dojo.forEach(this.getChildren(),function(_249){
_249.destroyRecursive(_248);
});
},_getSiblingOfChild:function(_24a,dir){
var node=_24a.domNode;
var _24d=(dir>0?"nextSibling":"previousSibling");
do{
node=node[_24d];
}while(node&&(node.nodeType!=1||!dijit.byNode(node)));
return node?dijit.byNode(node):null;
},getIndexOfChild:function(_24e){
var _24f=this.getChildren();
for(var i=0,c;c=_24f[i];i++){
if(c==_24e){
return i;
}
}
return -1;
}});
}
if(!dojo._hasResource["dijit.layout._LayoutWidget"]){
dojo._hasResource["dijit.layout._LayoutWidget"]=true;
dojo.provide("dijit.layout._LayoutWidget");
dojo.declare("dijit.layout._LayoutWidget",[dijit._Widget,dijit._Container,dijit._Contained],{baseClass:"dijitLayoutContainer",isLayoutContainer:true,postCreate:function(){
dojo.addClass(this.domNode,"dijitContainer");
dojo.addClass(this.domNode,this.baseClass);
},startup:function(){
if(this._started){
return;
}
dojo.forEach(this.getChildren(),function(_252){
_252.startup();
});
if(!this.getParent||!this.getParent()){
this.resize();
this._viewport=dijit.getViewport();
this.connect(dojo.global,"onresize",function(){
var _253=dijit.getViewport();
if(_253.w!=this._viewport.w||_253.h!=this._viewport.h){
this._viewport=_253;
this.resize();
}
});
}
this.inherited(arguments);
},resize:function(_254,_255){
var node=this.domNode;
if(_254){
dojo.marginBox(node,_254);
if(_254.t){
node.style.top=_254.t+"px";
}
if(_254.l){
node.style.left=_254.l+"px";
}
}
var mb=_255||{};
dojo.mixin(mb,_254||{});
if(!("h" in mb)||!("w" in mb)){
mb=dojo.mixin(dojo.marginBox(node),mb);
}
var cs=dojo.getComputedStyle(node);
var me=dojo._getMarginExtents(node,cs);
var be=dojo._getBorderExtents(node,cs);
var bb=(this._borderBox={w:mb.w-(me.w+be.w),h:mb.h-(me.h+be.h)});
var pe=dojo._getPadExtents(node,cs);
this._contentBox={l:dojo._toPixelValue(node,cs.paddingLeft),t:dojo._toPixelValue(node,cs.paddingTop),w:bb.w-pe.w,h:bb.h-pe.h};
this.layout();
},layout:function(){
},_setupChild:function(_25d){
dojo.addClass(_25d.domNode,this.baseClass+"-child");
if(_25d.baseClass){
dojo.addClass(_25d.domNode,this.baseClass+"-"+_25d.baseClass);
}
},addChild:function(_25e,_25f){
this.inherited(arguments);
if(this._started){
this._setupChild(_25e);
}
},removeChild:function(_260){
dojo.removeClass(_260.domNode,this.baseClass+"-child");
if(_260.baseClass){
dojo.removeClass(_260.domNode,this.baseClass+"-"+_260.baseClass);
}
this.inherited(arguments);
}});
dijit.layout.marginBox2contentBox=function(node,mb){
var cs=dojo.getComputedStyle(node);
var me=dojo._getMarginExtents(node,cs);
var pb=dojo._getPadBorderExtents(node,cs);
return {l:dojo._toPixelValue(node,cs.paddingLeft),t:dojo._toPixelValue(node,cs.paddingTop),w:mb.w-(me.w+pb.w),h:mb.h-(me.h+pb.h)};
};
(function(){
var _266=function(word){
return word.substring(0,1).toUpperCase()+word.substring(1);
};
var size=function(_269,dim){
_269.resize?_269.resize(dim):dojo.marginBox(_269.domNode,dim);
dojo.mixin(_269,dojo.marginBox(_269.domNode));
dojo.mixin(_269,dim);
};
dijit.layout.layoutChildren=function(_26b,dim,_26d){
dim=dojo.mixin({},dim);
dojo.addClass(_26b,"dijitLayoutContainer");
_26d=dojo.filter(_26d,function(item){
return item.layoutAlign!="client";
}).concat(dojo.filter(_26d,function(item){
return item.layoutAlign=="client";
}));
dojo.forEach(_26d,function(_270){
var elm=_270.domNode,pos=_270.layoutAlign;
var _273=elm.style;
_273.left=dim.l+"px";
_273.top=dim.t+"px";
_273.bottom=_273.right="auto";
dojo.addClass(elm,"dijitAlign"+_266(pos));
if(pos=="top"||pos=="bottom"){
size(_270,{w:dim.w});
dim.h-=_270.h;
if(pos=="top"){
dim.t+=_270.h;
}else{
_273.top=dim.t+dim.h+"px";
}
}else{
if(pos=="left"||pos=="right"){
size(_270,{h:dim.h});
dim.w-=_270.w;
if(pos=="left"){
dim.l+=_270.w;
}else{
_273.left=dim.l+dim.w+"px";
}
}else{
if(pos=="client"){
size(_270,dim);
}
}
}
});
};
})();
}
if(!dojo._hasResource["dojo.html"]){
dojo._hasResource["dojo.html"]=true;
dojo.provide("dojo.html");
(function(){
var _274=0;
dojo.html._secureForInnerHtml=function(cont){
return cont.replace(/(?:\s*<!DOCTYPE\s[^>]+>|<title[^>]*>[\s\S]*?<\/title>)/ig,"");
};
dojo.html._emptyNode=dojo.empty;
dojo.html._setNodeContent=function(node,cont,_278){
if(_278){
dojo.html._emptyNode(node);
}
if(typeof cont=="string"){
var pre="",post="",walk=0,name=node.nodeName.toLowerCase();
switch(name){
case "tr":
pre="<tr>";
post="</tr>";
walk+=1;
case "tbody":
case "thead":
pre="<tbody>"+pre;
post+="</tbody>";
walk+=1;
case "table":
pre="<table>"+pre;
post+="</table>";
walk+=1;
break;
}
if(walk){
var n=node.ownerDocument.createElement("div");
n.innerHTML=pre+cont+post;
do{
n=n.firstChild;
}while(--walk);
dojo.forEach(n.childNodes,function(n){
node.appendChild(n.cloneNode(true));
});
}else{
node.innerHTML=cont;
}
}else{
if(cont.nodeType){
node.appendChild(cont);
}else{
dojo.forEach(cont,function(n){
node.appendChild(n.cloneNode(true));
});
}
}
return node;
};
dojo.declare("dojo.html._ContentSetter",null,{node:"",content:"",id:"",cleanContent:false,extractContent:false,parseContent:false,constructor:function(_280,node){
dojo.mixin(this,_280||{});
node=this.node=dojo.byId(this.node||node);
if(!this.id){
this.id=["Setter",(node)?node.id||node.tagName:"",_274++].join("_");
}
if(!(this.node||node)){
new Error(this.declaredClass+": no node provided to "+this.id);
}
},set:function(cont,_283){
if(undefined!==cont){
this.content=cont;
}
if(_283){
this._mixin(_283);
}
this.onBegin();
this.setContent();
this.onEnd();
return this.node;
},setContent:function(){
var node=this.node;
if(!node){
console.error("setContent given no node");
}
try{
node=dojo.html._setNodeContent(node,this.content);
}
catch(e){
var _285=this.onContentError(e);
try{
node.innerHTML=_285;
}
catch(e){
console.error("Fatal "+this.declaredClass+".setContent could not change content due to "+e.message,e);
}
}
this.node=node;
},empty:function(){
if(this.parseResults&&this.parseResults.length){
dojo.forEach(this.parseResults,function(w){
if(w.destroy){
w.destroy();
}
});
delete this.parseResults;
}
dojo.html._emptyNode(this.node);
},onBegin:function(){
var cont=this.content;
if(dojo.isString(cont)){
if(this.cleanContent){
cont=dojo.html._secureForInnerHtml(cont);
}
if(this.extractContent){
var _288=cont.match(/<body[^>]*>\s*([\s\S]+)\s*<\/body>/im);
if(_288){
cont=_288[1];
}
}
}
this.empty();
this.content=cont;
return this.node;
},onEnd:function(){
if(this.parseContent){
this._parse();
}
return this.node;
},tearDown:function(){
delete this.parseResults;
delete this.node;
delete this.content;
},onContentError:function(err){
return "Error occured setting content: "+err;
},_mixin:function(_28a){
var _28b={},key;
for(key in _28a){
if(key in _28b){
continue;
}
this[key]=_28a[key];
}
},_parse:function(){
var _28d=this.node;
try{
this.parseResults=dojo.parser.parse(_28d,true);
}
catch(e){
this._onError("Content",e,"Error parsing in _ContentSetter#"+this.id);
}
},_onError:function(type,err,_290){
var _291=this["on"+type+"Error"].call(this,err);
if(_290){
console.error(_290,err);
}else{
if(_291){
dojo.html._setNodeContent(this.node,_291,true);
}
}
}});
dojo.html.set=function(node,cont,_294){
if(undefined==cont){
console.warn("dojo.html.set: no cont argument provided, using empty string");
cont="";
}
if(!_294){
return dojo.html._setNodeContent(node,cont,true);
}else{
var op=new dojo.html._ContentSetter(dojo.mixin(_294,{content:cont,node:node}));
return op.set();
}
};
})();
}
if(!dojo._hasResource["dojo.i18n"]){
dojo._hasResource["dojo.i18n"]=true;
dojo.provide("dojo.i18n");
dojo.i18n.getLocalization=function(_296,_297,_298){
_298=dojo.i18n.normalizeLocale(_298);
var _299=_298.split("-");
var _29a=[_296,"nls",_297].join(".");
var _29b=dojo._loadedModules[_29a];
if(_29b){
var _29c;
for(var i=_299.length;i>0;i--){
var loc=_299.slice(0,i).join("_");
if(_29b[loc]){
_29c=_29b[loc];
break;
}
}
if(!_29c){
_29c=_29b.ROOT;
}
if(_29c){
var _29f=function(){
};
_29f.prototype=_29c;
return new _29f();
}
}
throw new Error("Bundle not found: "+_297+" in "+_296+" , locale="+_298);
};
dojo.i18n.normalizeLocale=function(_2a0){
return "en-us";
};
dojo.i18n._requireLocalization=function(_2a2,_2a3,_2a4,_2a5){
var _2a6=dojo.i18n.normalizeLocale(_2a4);
var _2a7=[_2a2,"nls",_2a3].join(".");
var _2a8="";
if(_2a5){
var _2a9=_2a5.split(",");
for(var i=0;i<_2a9.length;i++){
if(_2a6["indexOf"](_2a9[i])==0){
if(_2a9[i].length>_2a8.length){
_2a8=_2a9[i];
}
}
}
if(!_2a8){
_2a8="ROOT";
}
}
var _2ab=_2a5?_2a8:_2a6;
var _2ac=dojo._loadedModules[_2a7];
var _2ad=null;
if(_2ac){
if(dojo.config.localizationComplete&&_2ac._built){
return;
}
var _2ae=_2ab.replace(/-/g,"_");
var _2af=_2a7+"."+_2ae;
_2ad=dojo._loadedModules[_2af];
}
if(!_2ad){
_2ac=dojo["provide"](_2a7);
var syms=dojo._getModuleSymbols(_2a2);
var _2b1=syms.concat("nls").join("/");
var _2b2;
dojo.i18n._searchLocalePath(_2ab,_2a5,function(loc){
var _2b4=loc.replace(/-/g,"_");
var _2b5=_2a7+"."+_2b4;
var _2b6=false;
if(!dojo._loadedModules[_2b5]){
dojo["provide"](_2b5);
var _2b7=[_2b1];
if(loc!="ROOT"){
_2b7.push(loc);
}
_2b7.push(_2a3);
var _2b8=_2b7.join("/")+".js";
_2b6=dojo._loadPath(_2b8,null,function(hash){
var _2ba=function(){
};
_2ba.prototype=_2b2;
_2ac[_2b4]=new _2ba();
for(var j in hash){
_2ac[_2b4][j]=hash[j];
}
});
}else{
_2b6=true;
}
if(_2b6&&_2ac[_2b4]){
_2b2=_2ac[_2b4];
}else{
_2ac[_2b4]=_2b2;
}
if(_2a5){
return true;
}
});
}
if(_2a5&&_2a6!=_2a8){
_2ac[_2a6.replace(/-/g,"_")]=_2ac[_2a8.replace(/-/g,"_")];
}
};
(function(){
var _2bc=dojo.config.extraLocale;
if(_2bc){
if(!_2bc instanceof Array){
_2bc=[_2bc];
}
var req=dojo.i18n._requireLocalization;
dojo.i18n._requireLocalization=function(m,b,_2c0,_2c1){
req(m,b,_2c0,_2c1);
if(_2c0){
return;
}
for(var i=0;i<_2bc.length;i++){
req(m,b,_2bc[i],_2c1);
}
};
}
})();
dojo.i18n._searchLocalePath=function(_2c3,down,_2c5){
_2c3=dojo.i18n.normalizeLocale(_2c3);
var _2c6=_2c3.split("-");
var _2c7=[];
for(var i=_2c6.length;i>0;i--){
_2c7.push(_2c6.slice(0,i).join("-"));
}
_2c7.push(false);
if(down){
_2c7.reverse();
}
for(var j=_2c7.length-1;j>=0;j--){
var loc=_2c7[j]||"ROOT";
var stop=_2c5(loc);
if(stop){
break;
}
}
};
dojo.i18n._preloadLocalizations=function(_2cc,_2cd){
function _2ce(_2cf){
_2cf=dojo.i18n.normalizeLocale(_2cf);
dojo.i18n._searchLocalePath(_2cf,true,function(loc){
for(var i=0;i<_2cd.length;i++){
if(_2cd[i]==loc){
dojo["require"](_2cc+"_"+loc);
return true;
}
}
return false;
});
};
_2ce();
var _2d2=dojo.config.extraLocale||[];
for(var i=0;i<_2d2.length;i++){
_2ce(_2d2[i]);
}
};
}
if(!dojo._hasResource["dijit.layout.ContentPane"]){
dojo._hasResource["dijit.layout.ContentPane"]=true;
dojo.provide("dijit.layout.ContentPane");
dojo.declare("dijit.layout.ContentPane",dijit._Widget,{href:"",extractContent:false,parseOnLoad:true,preventCache:false,preload:false,refreshOnShow:false,loadingMessage:"<span class='dijitContentPaneLoading'>${loadingState}</span>",errorMessage:"<span class='dijitContentPaneError'>${errorState}</span>",isLoaded:false,baseClass:"dijitContentPane",doLayout:true,ioArgs:{},isContainer:true,postMixInProperties:function(){
this.inherited(arguments);
var _2d4=dojo.i18n.getLocalization("dijit","loading",this.lang);
this.loadingMessage=dojo.string.substitute(this.loadingMessage,_2d4);
this.errorMessage=dojo.string.substitute(this.errorMessage,_2d4);
if(!this.href&&this.srcNodeRef&&this.srcNodeRef.innerHTML){
this.isLoaded=true;
}
},buildRendering:function(){
this.inherited(arguments);
if(!this.containerNode){
this.containerNode=this.domNode;
}
},postCreate:function(){
this.domNode.title="";
if(!dojo.attr(this.domNode,"role")){
dijit.setWaiRole(this.domNode,"group");
}
dojo.addClass(this.domNode,this.baseClass);
},startup:function(){
if(this._started){
return;
}
if(this.isLoaded){
dojo.forEach(this.getChildren(),function(_2d5){
_2d5.startup();
});
if(this.doLayout){
this._checkIfSingleChild();
}
if(!this._singleChild||!dijit._Contained.prototype.getParent.call(this)){
this._scheduleLayout();
}
}
this._loadCheck();
this.inherited(arguments);
},_checkIfSingleChild:function(){
var _2d6=dojo.query(">",this.containerNode),_2d7=_2d6.filter(function(node){
return dojo.hasAttr(node,"dojoType")||dojo.hasAttr(node,"widgetId");
}),_2d9=dojo.filter(_2d7.map(dijit.byNode),function(_2da){
return _2da&&_2da.domNode&&_2da.resize;
});
if(_2d6.length==_2d7.length&&_2d9.length==1){
this._singleChild=_2d9[0];
}else{
delete this._singleChild;
}
},setHref:function(href){
dojo.deprecated("dijit.layout.ContentPane.setHref() is deprecated. Use attr('href', ...) instead.","","2.0");
return this.attr("href",href);
},_setHrefAttr:function(href){
this.cancel();
this.href=href;
if(this._created&&(this.preload||this._isShown())){
return this.refresh();
}else{
this._hrefChanged=true;
}
},setContent:function(data){
dojo.deprecated("dijit.layout.ContentPane.setContent() is deprecated.  Use attr('content', ...) instead.","","2.0");
this.attr("content",data);
},_setContentAttr:function(data){
this.href="";
this.cancel();
this._setContent(data||"");
this._isDownloaded=false;
},_getContentAttr:function(){
return this.containerNode.innerHTML;
},cancel:function(){
if(this._xhrDfd&&(this._xhrDfd.fired==-1)){
this._xhrDfd.cancel();
}
delete this._xhrDfd;
},uninitialize:function(){
if(this._beingDestroyed){
this.cancel();
}
},destroyRecursive:function(_2df){
if(this._beingDestroyed){
return;
}
this._beingDestroyed=true;
this.inherited(arguments);
},resize:function(size){
dojo.marginBox(this.domNode,size);
var node=this.containerNode,mb=dojo.mixin(dojo.marginBox(node),size||{});
var cb=(this._contentBox=dijit.layout.marginBox2contentBox(node,mb));
if(this._singleChild&&this._singleChild.resize){
this._singleChild.resize({w:cb.w,h:cb.h});
}
},_isShown:function(){
if("open" in this){
return this.open;
}else{
var node=this.domNode;
return (node.style.display!="none")&&(node.style.visibility!="hidden")&&!dojo.hasClass(node,"dijitHidden");
}
},_onShow:function(){
if(this._needLayout){
this._layoutChildren();
}
this._loadCheck();
if(this.onShow){
this.onShow();
}
},_loadCheck:function(){
if((this.href&&!this._xhrDfd)&&(!this.isLoaded||this._hrefChanged||this.refreshOnShow)&&(this.preload||this._isShown())){
delete this._hrefChanged;
this.refresh();
}
},refresh:function(){
this.cancel();
var self=this;
var _2e6={preventCache:(this.preventCache||this.refreshOnShow),url:this.href,handleAs:"text"};
if(dojo.isObject(this.ioArgs)){
dojo.mixin(_2e6,this.ioArgs);
}
var hand=(this._xhrDfd=(this.ioMethod||dojo.xhrGet)(_2e6));
hand.addCallback(function(html){
try{
self._isDownloaded=true;
self._setContent(html,false);
self.onDownloadEnd();
}
catch(err){
self._onError("Content",err);
}
delete self._xhrDfd;
return html;
});
hand.addErrback(function(err){
if(!hand.canceled){
self._onError("Download",err);
}
delete self._xhrDfd;
return err;
});
},_onLoadHandler:function(data){
this.isLoaded=true;
try{
this.onLoad(data);
}
catch(e){
console.error("Error "+this.widgetId+" running custom onLoad code: "+e.message);
}
},_onUnloadHandler:function(){
this.isLoaded=false;
try{
this.onUnload();
}
catch(e){
console.error("Error "+this.widgetId+" running custom onUnload code: "+e.message);
}
},destroyDescendants:function(){
if(this.isLoaded){
this._onUnloadHandler();
}
var _2eb=this._contentSetter;
dojo.forEach(this.getChildren(),function(_2ec){
if(_2ec.destroyRecursive){
_2ec.destroyRecursive();
}
});
if(_2eb){
dojo.forEach(_2eb.parseResults,function(_2ed){
if(_2ed.destroyRecursive&&_2ed.domNode&&_2ed.domNode.parentNode==dojo.body()){
_2ed.destroyRecursive();
}
});
delete _2eb.parseResults;
}
dojo.html._emptyNode(this.containerNode);
},_setContent:function(cont,_2ef){
this.destroyDescendants();
delete this._singleChild;
var _2f0=this._contentSetter;
if(!(_2f0&&_2f0 instanceof dojo.html._ContentSetter)){
_2f0=this._contentSetter=new dojo.html._ContentSetter({node:this.containerNode,_onError:dojo.hitch(this,this._onError),onContentError:dojo.hitch(this,function(e){
var _2f2=this.onContentError(e);
try{
this.containerNode.innerHTML=_2f2;
}
catch(e){
console.error("Fatal "+this.id+" could not change content due to "+e.message,e);
}
})});
}
var _2f3=dojo.mixin({cleanContent:this.cleanContent,extractContent:this.extractContent,parseContent:this.parseOnLoad},this._contentSetterParams||{});
dojo.mixin(_2f0,_2f3);
_2f0.set((dojo.isObject(cont)&&cont.domNode)?cont.domNode:cont);
delete this._contentSetterParams;
if(!_2ef){
dojo.forEach(this.getChildren(),function(_2f4){
_2f4.startup();
});
if(this.doLayout){
this._checkIfSingleChild();
}
this._scheduleLayout();
this._onLoadHandler(cont);
}
},_onError:function(type,err,_2f7){
var _2f8=this["on"+type+"Error"].call(this,err);
if(_2f7){
console.error(_2f7,err);
}else{
if(_2f8){
this._setContent(_2f8,true);
}
}
},_scheduleLayout:function(){
if(this._isShown()){
this._layoutChildren();
}else{
this._needLayout=true;
}
},_layoutChildren:function(){
if(this._singleChild&&this._singleChild.resize){
var cb=this._contentBox||dojo.contentBox(this.containerNode);
this._singleChild.resize({w:cb.w,h:cb.h});
}else{
dojo.forEach(this.getChildren(),function(_2fa){
if(_2fa.resize){
_2fa.resize();
}
});
}
delete this._needLayout;
},onLoad:function(data){
},onUnload:function(){
},onDownloadStart:function(){
return this.loadingMessage;
},onContentError:function(_2fc){
},onDownloadError:function(_2fd){
return this.errorMessage;
},onDownloadEnd:function(){
}});
}
if(!dojo._hasResource["dojo.regexp"]){
dojo._hasResource["dojo.regexp"]=true;
dojo.provide("dojo.regexp");
dojo.regexp.escapeString=function(str,_2ff){
return str.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,function(ch){
if(_2ff&&_2ff.indexOf(ch)!=-1){
return ch;
}
return "\\"+ch;
});
};
dojo.regexp.buildGroupRE=function(arr,re,_303){
if(!(arr instanceof Array)){
return re(arr);
}
var b=[];
for(var i=0;i<arr.length;i++){
b.push(re(arr[i]));
}
return dojo.regexp.group(b.join("|"),_303);
};
dojo.regexp.group=function(_306,_307){
return "("+(_307?"?:":"")+_306+")";
};
}
if(!dojo._hasResource["dojo.cookie"]){
dojo._hasResource["dojo.cookie"]=true;
dojo.provide("dojo.cookie");
dojo.cookie=function(name,_309,_30a){
var c=document.cookie;
if(arguments.length==1){
var _30c=c.match(new RegExp("(?:^|; )"+dojo.regexp.escapeString(name)+"=([^;]*)"));
return _30c?decodeURIComponent(_30c[1]):undefined;
}else{
_30a=_30a||{};
var exp=_30a.expires;
if(typeof exp=="number"){
var d=new Date();
d.setTime(d.getTime()+exp*24*60*60*1000);
exp=_30a.expires=d;
}
if(exp&&exp.toUTCString){
_30a.expires=exp.toUTCString();
}
_309=encodeURIComponent(_309);
var _30f=name+"="+_309,_310;
for(_310 in _30a){
_30f+="; "+_310;
var _311=_30a[_310];
if(_311!==true){
_30f+="="+_311;
}
}
document.cookie=_30f;
}
};
dojo.cookie.isSupported=function(){
if(!("cookieEnabled" in navigator)){
this("__djCookieTest__","CookiesAllowed");
navigator.cookieEnabled=this("__djCookieTest__")=="CookiesAllowed";
if(navigator.cookieEnabled){
this("__djCookieTest__","",{expires:-1});
}
}
return navigator.cookieEnabled;
};
}
if(!dojo._hasResource["dijit.tree.TreeStoreModel"]){
dojo._hasResource["dijit.tree.TreeStoreModel"]=true;
dojo.provide("dijit.tree.TreeStoreModel");
dojo.declare("dijit.tree.TreeStoreModel",null,{store:null,childrenAttrs:["children"],labelAttr:"",root:null,query:null,constructor:function(args){
dojo.mixin(this,args);
this.connects=[];
var _313=this.store;
if(!_313.getFeatures()["dojo.data.api.Identity"]){
throw new Error("dijit.Tree: store must support dojo.data.Identity");
}
if(_313.getFeatures()["dojo.data.api.Notification"]){
this.connects=this.connects.concat([dojo.connect(_313,"onNew",this,"_onNewItem"),dojo.connect(_313,"onDelete",this,"_onDeleteItem"),dojo.connect(_313,"onSet",this,"_onSetItem")]);
}
},destroy:function(){
dojo.forEach(this.connects,dojo.disconnect);
},getRoot:function(_314,_315){
if(this.root){
_314(this.root);
}else{
this.store.fetch({query:this.query,onComplete:dojo.hitch(this,function(_316){
if(_316.length!=1){
throw new Error(this.declaredClass+": query "+dojo.toJson(this.query)+" returned "+_316.length+" items, but must return exactly one item");
}
this.root=_316[0];
_314(this.root);
}),onError:_315});
}
},mayHaveChildren:function(item){
return dojo.some(this.childrenAttrs,function(attr){
return this.store.hasAttribute(item,attr);
},this);
},getChildren:function(_319,_31a,_31b){
var _31c=this.store;
var _31d=[];
for(var i=0;i<this.childrenAttrs.length;i++){
var vals=_31c.getValues(_319,this.childrenAttrs[i]);
_31d=_31d.concat(vals);
}
var _320=0;
dojo.forEach(_31d,function(item){
if(!_31c.isItemLoaded(item)){
_320++;
}
});
if(_320==0){
_31a(_31d);
}else{
var _322=function _322(item){
if(--_320==0){
_31a(_31d);
}
};
dojo.forEach(_31d,function(item){
if(!_31c.isItemLoaded(item)){
_31c.loadItem({item:item,onItem:_322,onError:_31b});
}
});
}
},getIdentity:function(item){
return this.store.getIdentity(item);
},getLabel:function(item){
if(this.labelAttr){
return this.store.getValue(item,this.labelAttr);
}else{
return this.store.getLabel(item);
}
},newItem:function(args,_328){
var _329={parent:_328,attribute:this.childrenAttrs[0]};
return this.store.newItem(args,_329);
},pasteItem:function(_32a,_32b,_32c,_32d,_32e){
var _32f=this.store,_330=this.childrenAttrs[0];
if(_32b){
dojo.forEach(this.childrenAttrs,function(attr){
if(_32f.containsValue(_32b,attr,_32a)){
if(!_32d){
var _332=dojo.filter(_32f.getValues(_32b,attr),function(x){
return x!=_32a;
});
_32f.setValues(_32b,attr,_332);
}
_330=attr;
}
});
}
if(_32c){
if(typeof _32e=="number"){
var _334=_32f.getValues(_32c,_330);
_334.splice(_32e,0,_32a);
_32f.setValues(_32c,_330,_334);
}else{
_32f.setValues(_32c,_330,_32f.getValues(_32c,_330).concat(_32a));
}
}
},onChange:function(item){
},onChildrenChange:function(_336,_337){
},onDelete:function(_338,_339){
},_onNewItem:function(item,_33b){
if(!_33b){
return;
}
this.getChildren(_33b.item,dojo.hitch(this,function(_33c){
this.onChildrenChange(_33b.item,_33c);
}));
},_onDeleteItem:function(item){
this.onDelete(item);
},_onSetItem:function(item,_33f,_340,_341){
if(dojo.indexOf(this.childrenAttrs,_33f)!=-1){
this.getChildren(item,dojo.hitch(this,function(_342){
this.onChildrenChange(item,_342);
}));
}else{
this.onChange(item);
}
}});
}
if(!dojo._hasResource["dijit.tree.ForestStoreModel"]){
dojo._hasResource["dijit.tree.ForestStoreModel"]=true;
dojo.provide("dijit.tree.ForestStoreModel");
dojo.declare("dijit.tree.ForestStoreModel",dijit.tree.TreeStoreModel,{rootId:"$root$",rootLabel:"ROOT",query:null,constructor:function(_343){
this.root={store:this,root:true,id:_343.rootId,label:_343.rootLabel,children:_343.rootChildren};
},mayHaveChildren:function(item){
return item===this.root||this.inherited(arguments);
},getChildren:function(_345,_346,_347){
if(_345===this.root){
if(this.root.children){
_346(this.root.children);
}else{
this.store.fetch({query:this.query,onComplete:dojo.hitch(this,function(_348){
this.root.children=_348;
_346(_348);
}),onError:_347});
}
}else{
this.inherited(arguments);
}
},getIdentity:function(item){
return (item===this.root)?this.root.id:this.inherited(arguments);
},getLabel:function(item){
return (item===this.root)?this.root.label:this.inherited(arguments);
},newItem:function(args,_34c){
if(_34c===this.root){
this.onNewRootItem(args);
return this.store.newItem(args);
}else{
return this.inherited(arguments);
}
},onNewRootItem:function(args){
},pasteItem:function(_34e,_34f,_350,_351,_352){
if(_34f===this.root){
if(!_351){
this.onLeaveRoot(_34e);
}
}
dijit.tree.TreeStoreModel.prototype.pasteItem.call(this,_34e,_34f===this.root?null:_34f,_350===this.root?null:_350,_351,_352);
if(_350===this.root){
this.onAddToRoot(_34e);
}
},onAddToRoot:function(item){
console.log(this,": item ",item," added to root");
},onLeaveRoot:function(item){
console.log(this,": item ",item," removed from root");
},_requeryTop:function(){
var _355=this.root.children||[];
this.store.fetch({query:this.query,onComplete:dojo.hitch(this,function(_356){
this.root.children=_356;
if(_355.length!=_356.length||dojo.some(_355,function(item,idx){
return _356[idx]!=item;
})){
this.onChildrenChange(this.root,_356);
}
})});
},_onNewItem:function(item,_35a){
this._requeryTop();
this.inherited(arguments);
},_onDeleteItem:function(item){
if(dojo.indexOf(this.root.children,item)!=-1){
this._requeryTop();
}
this.inherited(arguments);
}});
}
if(!dojo._hasResource["dijit.Tree"]){
dojo._hasResource["dijit.Tree"]=true;
dojo.provide("dijit.Tree");
dojo.declare("dijit._TreeNode",[dijit._Widget,dijit._Templated,dijit._Container,dijit._Contained],{item:null,isTreeNode:true,label:"",isExpandable:null,isExpanded:false,state:"UNCHECKED",templateString:"<div class=\"dijitTreeNode\" waiRole=\"presentation\"\n\t><div dojoAttachPoint=\"rowNode\" class=\"dijitTreeRow\" waiRole=\"presentation\" dojoAttachEvent=\"onmouseenter:_onMouseEnter, onmouseleave:_onMouseLeave\"\n\t\t><img src=\"${_blankGif}\" alt=\"\" dojoAttachPoint=\"expandoNode\" class=\"dijitTreeExpando\" waiRole=\"presentation\"\n\t\t><span dojoAttachPoint=\"expandoNodeText\" class=\"dijitExpandoText\" waiRole=\"presentation\"\n\t\t></span\n\t\t><span dojoAttachPoint=\"contentNode\"\n\t\t\tclass=\"dijitTreeContent\" waiRole=\"presentation\">\n\t\t\t<img src=\"${_blankGif}\" alt=\"\" dojoAttachPoint=\"iconNode\" class=\"dijitTreeIcon\" waiRole=\"presentation\"\n\t\t\t><span dojoAttachPoint=\"labelNode\" class=\"dijitTreeLabel\" wairole=\"treeitem\" tabindex=\"-1\" waiState=\"selected-false\" dojoAttachEvent=\"onfocus:_onLabelFocus, onblur:_onLabelBlur\"></span>\n\t\t</span\n\t></div>\n\t<div dojoAttachPoint=\"containerNode\" class=\"dijitTreeContainer\" waiRole=\"presentation\" style=\"display: none;\"></div>\n</div>\n",postCreate:function(){
this.setLabelNode(this.label);
this._setExpando();
this._updateItemClasses(this.item);
if(this.isExpandable){
dijit.setWaiState(this.labelNode,"expanded",this.isExpanded);
if(this==this.tree.rootNode){
dijit.setWaitState(this.tree.domNode,"expanded",this.isExpanded);
}
}
},_setIndentAttr:function(_35c){
this.indent=_35c;
var _35d=(Math.max(_35c,0)*19)+"px";
dojo.style(this.domNode,"backgroundPosition",_35d+" 0px");
dojo.style(this.rowNode,dojo._isBodyLtr()?"paddingLeft":"paddingRight",_35d);
dojo.forEach(this.getChildren(),function(_35e){
_35e.attr("indent",_35c+1);
});
},markProcessing:function(){
this.state="LOADING";
this._setExpando(true);
},unmarkProcessing:function(){
this._setExpando(false);
},_updateItemClasses:function(item){
var tree=this.tree,_361=tree.model;
if(tree._v10Compat&&item===_361.root){
item=null;
}
if(this._iconClass){
dojo.removeClass(this.iconNode,this._iconClass);
}
this._iconClass=tree.getIconClass(item,this.isExpanded);
if(this._iconClass){
dojo.addClass(this.iconNode,this._iconClass);
}
dojo.style(this.iconNode,tree.getIconStyle(item,this.isExpanded)||{});
if(this._labelClass){
dojo.removeClass(this.labelNode,this._labelClass);
}
this._labelClass=tree.getLabelClass(item,this.isExpanded);
if(this._labelClass){
dojo.addClass(this.labelNode,this._labelClass);
}
dojo.style(this.labelNode,tree.getLabelStyle(item,this.isExpanded)||{});
},_updateLayout:function(){
var _362=this.getParent();
if(!_362||_362.rowNode.style.display=="none"){
dojo.addClass(this.domNode,"dijitTreeIsRoot");
}else{
dojo.toggleClass(this.domNode,"dijitTreeIsLast",!this.getNextSibling());
}
},_setExpando:function(_363){
var _364=["dijitTreeExpandoLoading","dijitTreeExpandoOpened","dijitTreeExpandoClosed","dijitTreeExpandoLeaf"];
var _365=["*","-","+","*"];
var idx=_363?0:(this.isExpandable?(this.isExpanded?1:2):3);
dojo.forEach(_364,function(s){
dojo.removeClass(this.expandoNode,s);
},this);
dojo.addClass(this.expandoNode,_364[idx]);
this.expandoNodeText.innerHTML=_365[idx];
},expand:function(){
if(this.isExpanded){
return;
}
this._wipeOut&&this._wipeOut.stop();
this.isExpanded=true;
dijit.setWaiState(this.labelNode,"expanded","true");
dijit.setWaiRole(this.containerNode,"group");
dojo.addClass(this.contentNode,"dijitTreeContentExpanded");
this._setExpando();
this._updateItemClasses(this.item);
if(this==this.tree.rootNode){
dijit.setWaiState(this.tree.domNode,"expanded","true");
}
if(!this._wipeIn){
this._wipeIn=dojo.fx.wipeIn({node:this.containerNode,duration:dijit.defaultDuration});
}
this._wipeIn.play();
},collapse:function(){
if(!this.isExpanded){
return;
}
this._wipeIn&&this._wipeIn.stop();
this.isExpanded=false;
dijit.setWaiState(this.labelNode,"expanded","false");
if(this==this.tree.rootNode){
dijit.setWaiState(this.tree.domNode,"expanded","false");
}
dojo.removeClass(this.contentNode,"dijitTreeContentExpanded");
this._setExpando();
this._updateItemClasses(this.item);
if(!this._wipeOut){
this._wipeOut=dojo.fx.wipeOut({node:this.containerNode,duration:dijit.defaultDuration});
}
this._wipeOut.play();
},setLabelNode:function(_368){
this.labelNode.innerHTML=_368;
},indent:0,setChildItems:function(_369){
var tree=this.tree,_36b=tree.model;
this.getChildren().forEach(function(_36c){
dijit._Container.prototype.removeChild.call(this,_36c);
},this);
this.state="LOADED";
if(_369&&_369.length>0){
this.isExpandable=true;
dojo.forEach(_369,function(item){
var id=_36b.getIdentity(item),_36f=tree._itemNodeMap[id],node=(_36f&&!_36f.getParent())?_36f:this.tree._createTreeNode({item:item,tree:tree,isExpandable:_36b.mayHaveChildren(item),label:tree.getLabel(item),indent:this.indent+1});
if(_36f){
_36f.attr("indent",this.indent+1);
}
this.addChild(node);
tree._itemNodeMap[id]=node;
if(this.tree._state(item)){
tree._expandNode(node);
}
},this);
dojo.forEach(this.getChildren(),function(_371,idx){
_371._updateLayout();
});
}else{
this.isExpandable=false;
}
if(this._setExpando){
this._setExpando(false);
}
if(this==tree.rootNode){
var fc=this.tree.showRoot?this:this.getChildren()[0];
if(fc){
fc.setSelected(true);
tree.lastFocused=fc;
}else{
tree.domNode.setAttribute("tabIndex","0");
}
}
},removeChild:function(node){
this.inherited(arguments);
var _375=this.getChildren();
if(_375.length==0){
this.isExpandable=false;
this.collapse();
}
dojo.forEach(_375,function(_376){
_376._updateLayout();
});
},makeExpandable:function(){
this.isExpandable=true;
this._setExpando(false);
},_onLabelFocus:function(evt){
dojo.addClass(this.labelNode,"dijitTreeLabelFocused");
this.tree._onNodeFocus(this);
},_onLabelBlur:function(evt){
dojo.removeClass(this.labelNode,"dijitTreeLabelFocused");
},setSelected:function(_379){
var _37a=this.labelNode;
_37a.setAttribute("tabIndex",_379?"0":"-1");
dijit.setWaiState(_37a,"selected",_379);
dojo.toggleClass(this.rowNode,"dijitTreeNodeSelected",_379);
},_onMouseEnter:function(evt){
dojo.addClass(this.rowNode,"dijitTreeNodeHover");
this.tree._onNodeMouseEnter(this,evt);
},_onMouseLeave:function(evt){
dojo.removeClass(this.rowNode,"dijitTreeNodeHover");
this.tree._onNodeMouseLeave(this,evt);
}});
dojo.declare("dijit.Tree",[dijit._Widget,dijit._Templated],{store:null,model:null,query:null,label:"",showRoot:true,childrenAttr:["children"],openOnClick:false,openOnDblClick:false,templateString:"<div class=\"dijitTreeContainer\" waiRole=\"tree\"\n\tdojoAttachEvent=\"onclick:_onClick,onkeypress:_onKeyPress,ondblclick:_onDblClick\">\n</div>\n",isExpandable:true,isTree:true,persist:true,dndController:null,dndParams:["onDndDrop","itemCreator","onDndCancel","checkAcceptance","checkItemAcceptance","dragThreshold","betweenThreshold"],onDndDrop:null,itemCreator:null,onDndCancel:null,checkAcceptance:null,checkItemAcceptance:null,dragThreshold:0,betweenThreshold:0,_publish:function(_37d,_37e){
dojo.publish(this.id,[dojo.mixin({tree:this,event:_37d},_37e||{})]);
},postMixInProperties:function(){
this.tree=this;
this._itemNodeMap={};
if(!this.cookieName){
this.cookieName=this.id+"C";
}
},postCreate:function(){
this._initState();
if(!this.model){
this._store2model();
}
this.connect(this.model,"onChange","_onItemChange");
this.connect(this.model,"onChildrenChange","_onItemChildrenChange");
this.connect(this.model,"onDelete","_onItemDelete");
this._load();
this.inherited(arguments);
if(this.dndController){
if(dojo.isString(this.dndController)){
this.dndController=dojo.getObject(this.dndController);
}
var _37f={};
for(var i=0;i<this.dndParams.length;i++){
if(this[this.dndParams[i]]){
_37f[this.dndParams[i]]=this[this.dndParams[i]];
}
}
this.dndController=new this.dndController(this,_37f);
}
},_store2model:function(){
this._v10Compat=true;
dojo.deprecated("Tree: from version 2.0, should specify a model object rather than a store/query");
var _381={id:this.id+"_ForestStoreModel",store:this.store,query:this.query,childrenAttrs:this.childrenAttr};
if(this.params.mayHaveChildren){
_381.mayHaveChildren=dojo.hitch(this,"mayHaveChildren");
}
if(this.params.getItemChildren){
_381.getChildren=dojo.hitch(this,function(item,_383,_384){
this.getItemChildren((this._v10Compat&&item===this.model.root)?null:item,_383,_384);
});
}
this.model=new dijit.tree.ForestStoreModel(_381);
this.showRoot=Boolean(this.label);
},_load:function(){
this.model.getRoot(dojo.hitch(this,function(item){
var rn=this.rootNode=this.tree._createTreeNode({item:item,tree:this,isExpandable:true,label:this.label||this.getLabel(item),indent:this.showRoot?0:-1});
if(!this.showRoot){
rn.rowNode.style.display="none";
}
this.domNode.appendChild(rn.domNode);
this._itemNodeMap[this.model.getIdentity(item)]=rn;
rn._updateLayout();
this._expandNode(rn);
}),function(err){
console.error(this,": error loading root: ",err);
});
},mayHaveChildren:function(item){
},getItemChildren:function(_389,_38a){
},getLabel:function(item){
if(item.href){
return "<a class=\"dijitTreeLink\" href=\""+item.href+"\">"+this.model.getLabel(item)+"</a>";
}
return this.model.getLabel(item);
},getIconClass:function(item,_38d){
return "";
},getLabelClass:function(item,_38f){
},getIconStyle:function(item,_391){
var icon="images/icon-rarrow22.png",_393={backgroundRepeat:"no-repeat",width:"22px",height:"22px"};
if(item!=null&&item.icon){
icon=item.icon;
}
if(typeof g_nwaSpriteIndex[icon]!="undefined"){
var pos=g_nwaSpriteIndex[icon];
_393.backgroundImage="url(images/sprite5.png)";
_393.backgroundPosition="-"+pos.x+"px -"+pos.y+"px";
}else{
_393.backgroundImage="url("+item.icon+")";
}
return _393;
},getLabelStyle:function(item,_396){
},_onKeyPress:function(e){
if(e.altKey){
return;
}
var dk=dojo.keys;
var _399=dijit.getEnclosingWidget(e.target);
if(!_399){
return;
}
var key=e.charOrCode;
if(typeof key=="string"){
if(!e.altKey&&!e.ctrlKey&&!e.shiftKey&&!e.metaKey){
this._onLetterKeyNav({node:_399,key:key.toLowerCase()});
dojo.stopEvent(e);
}
}else{
var map=this._keyHandlerMap;
if(!map){
map={};
map[dk.ENTER]="_onEnterKey";
map[this.isLeftToRight()?dk.LEFT_ARROW:dk.RIGHT_ARROW]="_onLeftArrow";
map[this.isLeftToRight()?dk.RIGHT_ARROW:dk.LEFT_ARROW]="_onRightArrow";
map[dk.UP_ARROW]="_onUpArrow";
map[dk.DOWN_ARROW]="_onDownArrow";
map[dk.HOME]="_onHomeKey";
map[dk.END]="_onEndKey";
this._keyHandlerMap=map;
}
if(this._keyHandlerMap[key]){
this[this._keyHandlerMap[key]]({node:_399,item:_399.item});
dojo.stopEvent(e);
}
}
},_onEnterKey:function(_39c){
this._publish("execute",{item:_39c.item,node:_39c.node});
this.onClick(_39c.item,_39c.node);
},_onDownArrow:function(_39d){
var node=this._getNextNode(_39d.node);
if(node&&node.isTreeNode){
this.focusNode(node);
}
},_onUpArrow:function(_39f){
var node=_39f.node;
var _3a1=node.getPreviousSibling();
if(_3a1){
node=_3a1;
while(node.isExpandable&&node.isExpanded&&node.hasChildren()){
var _3a2=node.getChildren();
node=_3a2[_3a2.length-1];
}
}else{
var _3a3=node.getParent();
if(!(!this.showRoot&&_3a3===this.rootNode)){
node=_3a3;
}
}
if(node&&node.isTreeNode){
this.focusNode(node);
}
},_onRightArrow:function(_3a4){
var node=_3a4.node;
if(node.isExpandable&&!node.isExpanded){
this._expandNode(node);
}else{
if(node.hasChildren()){
node=node.getChildren()[0];
if(node&&node.isTreeNode){
this.focusNode(node);
}
}
}
},_onLeftArrow:function(_3a6){
var node=_3a6.node;
if(node.isExpandable&&node.isExpanded){
this._collapseNode(node);
}else{
var _3a8=node.getParent();
if(_3a8&&_3a8.isTreeNode&&!(!this.showRoot&&_3a8===this.rootNode)){
this.focusNode(_3a8);
}
}
},_onHomeKey:function(){
var node=this._getRootOrFirstNode();
if(node){
this.focusNode(node);
}
},_onEndKey:function(_3aa){
var node=this.rootNode;
while(node.isExpanded){
var c=node.getChildren();
node=c[c.length-1];
}
if(node&&node.isTreeNode){
this.focusNode(node);
}
},_onLetterKeyNav:function(_3ad){
var node=_3ad.node,_3af=node,key=_3ad.key;
do{
node=this._getNextNode(node);
if(!node){
node=this._getRootOrFirstNode();
}
}while(node!==_3af&&(node.label.charAt(0).toLowerCase()!=key));
if(node&&node.isTreeNode){
if(node!==_3af){
this.focusNode(node);
}
}
},_onClick:function(e){
var _3b2=e.target;
var _3b3=dijit.getEnclosingWidget(_3b2);
if(!_3b3||!_3b3.isTreeNode){
return;
}
if((this.openOnClick&&_3b3.isExpandable)||(_3b2==_3b3.expandoNode||_3b2==_3b3.expandoNodeText)){
if(_3b3.isExpandable){
this._onExpandoClick({node:_3b3});
}
}else{
this._publish("execute",{item:_3b3.item,node:_3b3});
this.onClick(_3b3.item,_3b3);
this.focusNode(_3b3);
}
dojo.stopEvent(e);
},_onDblClick:function(e){
var _3b5=e.target;
var _3b6=dijit.getEnclosingWidget(_3b5);
if(!_3b6||!_3b6.isTreeNode){
return;
}
if((this.openOnDblClick&&_3b6.isExpandable)||(_3b5==_3b6.expandoNode||_3b5==_3b6.expandoNodeText)){
if(_3b6.isExpandable){
this._onExpandoClick({node:_3b6});
}
}else{
this._publish("execute",{item:_3b6.item,node:_3b6});
this.onDblClick(_3b6.item,_3b6);
this.focusNode(_3b6);
}
dojo.stopEvent(e);
},_onExpandoClick:function(_3b7){
var node=_3b7.node;
this.focusNode(node);
if(node.isExpanded){
this._collapseNode(node);
}else{
this._expandNode(node);
}
},onExpandoClick:function(node){
this._onExpandoClick({node:node});
},onClick:function(item,node){
},onDblClick:function(item,node){
},onOpen:function(item,node){
},onClose:function(item,node){
},_getNextNode:function(node){
if(node.isExpandable&&node.isExpanded&&node.hasChildren()){
return node.getChildren()[0];
}else{
while(node&&node.isTreeNode){
var _3c3=node.getNextSibling();
if(_3c3){
return _3c3;
}
node=node.getParent();
}
return null;
}
},_getRootOrFirstNode:function(){
return this.showRoot?this.rootNode:this.rootNode.getChildren()[0];
},_collapseNode:function(node){
if(node.isExpandable){
if(node.state=="LOADING"){
return;
}
node.collapse();
this.onClose(node.item,node);
if(node.item){
this._state(node.item,false);
this._saveState();
}
}
},_expandNode:function(node){
if(!node.isExpandable){
return;
}
var _3c6=this.model,item=node.item;
switch(node.state){
case "LOADING":
return;
case "UNCHECKED":
node.markProcessing();
var _3c8=this;
_3c6.getChildren(item,function(_3c9){
node.unmarkProcessing();
node.setChildItems(_3c9);
_3c8._expandNode(node);
},function(err){
console.error(_3c8,": error loading root children: ",err);
});
break;
default:
node.expand();
this.onOpen(node.item,node);
if(item){
this._state(item,true);
this._saveState();
}
}
},focusNode:function(node){
node.labelNode.focus();
},_onNodeFocus:function(node){
if(node){
if(node!=this.lastFocused){
this.lastFocused.setSelected(false);
}
node.setSelected(true);
this.lastFocused=node;
}
},_onNodeMouseEnter:function(node){
},_onNodeMouseLeave:function(node){
},_onItemChange:function(item){
var _3d0=this.model,_3d1=_3d0.getIdentity(item),node=this._itemNodeMap[_3d1];
if(node){
node.setLabelNode(this.getLabel(item));
node._updateItemClasses(item);
}
},_onItemChildrenChange:function(_3d3,_3d4){
var _3d5=this.model,_3d6=_3d5.getIdentity(_3d3),_3d7=this._itemNodeMap[_3d6];
if(_3d7){
_3d7.setChildItems(_3d4);
}
},_onItemDelete:function(item){
var _3d9=this.model,_3da=_3d9.getIdentity(item),node=this._itemNodeMap[_3da];
if(node){
var _3dc=node.getParent();
if(_3dc){
_3dc.removeChild(node);
}
node.destroyRecursive();
delete this._itemNodeMap[_3da];
}
},_initState:function(){
if(this.persist){
var _3dd=dojo.cookie(this.cookieName);
this._openedItemIds={};
if(_3dd){
dojo.forEach(_3dd.split(","),function(item){
this._openedItemIds[item]=true;
},this);
}
}
},_state:function(item,_3e0){
if(!this.persist){
return false;
}
var id=this.model.getIdentity(item);
if(arguments.length===1){
return this._openedItemIds[id];
}
if(_3e0){
this._openedItemIds[id]=true;
}else{
delete this._openedItemIds[id];
}
},_saveState:function(){
if(!this.persist){
return;
}
var ary=[];
for(var id in this._openedItemIds){
ary.push(id);
}
dojo.cookie(this.cookieName,ary.join(","),{expires:365});
},destroy:function(){
if(this.rootNode){
this.rootNode.destroyRecursive();
}
if(this.dndController&&!dojo.isString(this.dndController)){
this.dndController.destroy();
}
this.rootNode=null;
this.inherited(arguments);
},destroyRecursive:function(){
this.destroy();
},_createTreeNode:function(args){
return new dijit._TreeNode(args);
}});
}
if(!dojo._hasResource["dijit.layout.LayoutContainer"]){
dojo._hasResource["dijit.layout.LayoutContainer"]=true;
dojo.provide("dijit.layout.LayoutContainer");
dojo.declare("dijit.layout.LayoutContainer",dijit.layout._LayoutWidget,{baseClass:"dijitLayoutContainer",constructor:function(){
dojo.deprecated("dijit.layout.LayoutContainer is deprecated","use BorderContainer instead",2);
},layout:function(){
dijit.layout.layoutChildren(this.domNode,this._contentBox,this.getChildren());
},addChild:function(_3e5,_3e6){
this.inherited(arguments);
if(this._started){
dijit.layout.layoutChildren(this.domNode,this._contentBox,this.getChildren());
}
},removeChild:function(_3e7){
this.inherited(arguments);
if(this._started){
dijit.layout.layoutChildren(this.domNode,this._contentBox,this.getChildren());
}
}});
dojo.extend(dijit._Widget,{layoutAlign:"none"});
}
if(!dojo._hasResource["dijit.layout.SplitContainer"]){
dojo._hasResource["dijit.layout.SplitContainer"]=true;
dojo.provide("dijit.layout.SplitContainer");
dojo.declare("dijit.layout.SplitContainer",dijit.layout._LayoutWidget,{constructor:function(){
dojo.deprecated("dijit.layout.SplitContainer is deprecated","use BorderContainer with splitter instead",2);
},activeSizing:false,sizerWidth:7,orientation:"horizontal",persist:true,baseClass:"dijitSplitContainer",postMixInProperties:function(){
this.inherited("postMixInProperties",arguments);
this.isHorizontal=(this.orientation=="horizontal");
},postCreate:function(){
this.inherited(arguments);
this.sizers=[];
if(dojo.isMozilla){
this.domNode.style.overflow="-moz-scrollbars-none";
}
if(typeof this.sizerWidth=="object"){
try{
this.sizerWidth=parseInt(this.sizerWidth.toString());
}
catch(e){
this.sizerWidth=7;
}
}
var _3e8=dojo.doc.createElement("div");
this.virtualSizer=_3e8;
_3e8.style.position="relative";
_3e8.style.zIndex=10;
_3e8.className=this.isHorizontal?"dijitSplitContainerVirtualSizerH":"dijitSplitContainerVirtualSizerV";
this.domNode.appendChild(_3e8);
dojo.setSelectable(_3e8,false);
},destroy:function(){
delete this.virtualSizer;
dojo.forEach(this._ownconnects,dojo.disconnect);
this.inherited(arguments);
},startup:function(){
if(this._started){
return;
}
dojo.forEach(this.getChildren(),function(_3e9,i,_3eb){
this._setupChild(_3e9);
if(i<_3eb.length-1){
this._addSizer();
}
},this);
if(this.persist){
this._restoreState();
}
this.inherited(arguments);
},_setupChild:function(_3ec){
this.inherited(arguments);
_3ec.domNode.style.position="absolute";
dojo.addClass(_3ec.domNode,"dijitSplitPane");
},_onSizerMouseDown:function(e){
if(e.target.id){
for(var i=0;i<this.sizers.length;i++){
if(this.sizers[i].id==e.target.id){
break;
}
}
if(i<this.sizers.length){
this.beginSizing(e,i);
}
}
},_addSizer:function(_3ef){
_3ef=_3ef===undefined?this.sizers.length:_3ef;
var _3f0=dojo.doc.createElement("div");
_3f0.id=dijit.getUniqueId("dijit_layout_SplitterContainer_Splitter");
this.sizers.splice(_3ef,0,_3f0);
this.domNode.appendChild(_3f0);
_3f0.className=this.isHorizontal?"dijitSplitContainerSizerH":"dijitSplitContainerSizerV";
var _3f1=dojo.doc.createElement("div");
_3f1.className="thumb";
_3f1.id=_3f0.id;
_3f0.appendChild(_3f1);
this.connect(_3f0,"onmousedown","_onSizerMouseDown");
dojo.setSelectable(_3f0,false);
},removeChild:function(_3f2){
if(this.sizers.length){
var i=dojo.indexOf(this.getChildren(),_3f2);
if(i!=-1){
if(i==this.sizers.length){
i--;
}
dojo.destroy(this.sizers[i]);
this.sizers.splice(i,1);
}
}
this.inherited(arguments);
if(this._started){
this.layout();
}
},addChild:function(_3f4,_3f5){
this.inherited(arguments);
if(this._started){
var _3f6=this.getChildren();
if(_3f6.length>1){
this._addSizer(_3f5);
}
this.layout();
}
},layout:function(){
this.paneWidth=this._contentBox.w;
this.paneHeight=this._contentBox.h;
var _3f7=this.getChildren();
if(!_3f7.length){
return;
}
var _3f8=this.isHorizontal?this.paneWidth:this.paneHeight;
if(_3f7.length>1){
_3f8-=this.sizerWidth*(_3f7.length-1);
}
var _3f9=0;
dojo.forEach(_3f7,function(_3fa){
_3f9+=_3fa.sizeShare;
});
var _3fb=_3f8/_3f9;
var _3fc=0;
dojo.forEach(_3f7.slice(0,_3f7.length-1),function(_3fd){
var size=Math.round(_3fb*_3fd.sizeShare);
_3fd.sizeActual=size;
_3fc+=size;
});
_3f7[_3f7.length-1].sizeActual=_3f8-_3fc;
this._checkSizes();
var pos=0;
var size=_3f7[0].sizeActual;
this._movePanel(_3f7[0],pos,size);
_3f7[0].position=pos;
pos+=size;
if(!this.sizers){
return;
}
dojo.some(_3f7.slice(1),function(_401,i){
if(!this.sizers[i]){
return true;
}
this._moveSlider(this.sizers[i],pos,this.sizerWidth);
this.sizers[i].position=pos;
pos+=this.sizerWidth;
size=_401.sizeActual;
this._movePanel(_401,pos,size);
_401.position=pos;
pos+=size;
},this);
},_movePanel:function(_403,pos,size){
if(this.isHorizontal){
_403.domNode.style.left=pos+"px";
_403.domNode.style.top=0;
var box={w:size,h:this.paneHeight};
if(_403.resize){
_403.resize(box);
}else{
dojo.marginBox(_403.domNode,box);
}
}else{
_403.domNode.style.left=0;
_403.domNode.style.top=pos+"px";
var box={w:this.paneWidth,h:size};
if(_403.resize){
_403.resize(box);
}else{
dojo.marginBox(_403.domNode,box);
}
}
},_moveSlider:function(_407,pos,size){
if(this.isHorizontal){
_407.style.left=pos+"px";
_407.style.top=0;
dojo.marginBox(_407,{w:size,h:this.paneHeight});
}else{
_407.style.left=0;
_407.style.top=pos+"px";
dojo.marginBox(_407,{w:this.paneWidth,h:size});
}
},_growPane:function(_40a,pane){
if(_40a>0){
if(pane.sizeActual>pane.sizeMin){
if((pane.sizeActual-pane.sizeMin)>_40a){
pane.sizeActual=pane.sizeActual-_40a;
_40a=0;
}else{
_40a-=pane.sizeActual-pane.sizeMin;
pane.sizeActual=pane.sizeMin;
}
}
}
return _40a;
},_checkSizes:function(){
var _40c=0;
var _40d=0;
var _40e=this.getChildren();
dojo.forEach(_40e,function(_40f){
_40d+=_40f.sizeActual;
_40c+=_40f.sizeMin;
});
if(_40c<=_40d){
var _410=0;
dojo.forEach(_40e,function(_411){
if(_411.sizeActual<_411.sizeMin){
_410+=_411.sizeMin-_411.sizeActual;
_411.sizeActual=_411.sizeMin;
}
});
if(_410>0){
var list=this.isDraggingLeft?_40e.reverse():_40e;
dojo.forEach(list,function(_413){
_410=this._growPane(_410,_413);
},this);
}
}else{
dojo.forEach(_40e,function(_414){
_414.sizeActual=Math.round(_40d*(_414.sizeMin/_40c));
});
}
},beginSizing:function(e,i){
var _417=this.getChildren();
this.paneBefore=_417[i];
this.paneAfter=_417[i+1];
this.isSizing=true;
this.sizingSplitter=this.sizers[i];
if(!this.cover){
this.cover=dojo.create("div",{style:{position:"absolute",zIndex:5,top:0,left:0,width:"100%",height:"100%"}},this.domNode);
}else{
this.cover.style.zIndex=5;
}
this.sizingSplitter.style.zIndex=6;
this.originPos=dojo.coords(_417[0].domNode,true);
if(this.isHorizontal){
var _418=e.layerX||e.offsetX||0;
var _419=e.pageX;
this.originPos=this.originPos.x;
}else{
var _418=e.layerY||e.offsetY||0;
var _419=e.pageY;
this.originPos=this.originPos.y;
}
this.startPoint=this.lastPoint=_419;
this.screenToClientOffset=_419-_418;
this.dragOffset=this.lastPoint-this.paneBefore.sizeActual-this.originPos-this.paneBefore.position;
if(!this.activeSizing){
this._showSizingLine();
}
this._ownconnects=[];
this._ownconnects.push(dojo.connect(dojo.doc.documentElement,"onmousemove",this,"changeSizing"));
this._ownconnects.push(dojo.connect(dojo.doc.documentElement,"onmouseup",this,"endSizing"));
dojo.stopEvent(e);
},changeSizing:function(e){
if(!this.isSizing){
return;
}
this.lastPoint=this.isHorizontal?e.pageX:e.pageY;
this.movePoint();
if(this.activeSizing){
this._updateSize();
}else{
this._moveSizingLine();
}
dojo.stopEvent(e);
},endSizing:function(e){
if(!this.isSizing){
return;
}
if(this.cover){
this.cover.style.zIndex=-1;
}
if(!this.activeSizing){
this._hideSizingLine();
}
this._updateSize();
this.isSizing=false;
if(this.persist){
this._saveState(this);
}
dojo.forEach(this._ownconnects,dojo.disconnect);
},movePoint:function(){
var p=this.lastPoint-this.screenToClientOffset;
var a=p-this.dragOffset;
a=this.legaliseSplitPoint(a);
p=a+this.dragOffset;
this.lastPoint=p+this.screenToClientOffset;
},legaliseSplitPoint:function(a){
a+=this.sizingSplitter.position;
this.isDraggingLeft=!!(a>0);
if(!this.activeSizing){
var min=this.paneBefore.position+this.paneBefore.sizeMin;
if(a<min){
a=min;
}
var max=this.paneAfter.position+(this.paneAfter.sizeActual-(this.sizerWidth+this.paneAfter.sizeMin));
if(a>max){
a=max;
}
}
a-=this.sizingSplitter.position;
this._checkSizes();
return a;
},_updateSize:function(){
var pos=this.lastPoint-this.dragOffset-this.originPos;
var _422=this.paneBefore.position;
var _423=this.paneAfter.position+this.paneAfter.sizeActual;
this.paneBefore.sizeActual=pos-_422;
this.paneAfter.position=pos+this.sizerWidth;
this.paneAfter.sizeActual=_423-this.paneAfter.position;
dojo.forEach(this.getChildren(),function(_424){
_424.sizeShare=_424.sizeActual;
});
if(this._started){
this.layout();
}
},_showSizingLine:function(){
this._moveSizingLine();
dojo.marginBox(this.virtualSizer,this.isHorizontal?{w:this.sizerWidth,h:this.paneHeight}:{w:this.paneWidth,h:this.sizerWidth});
this.virtualSizer.style.display="block";
},_hideSizingLine:function(){
this.virtualSizer.style.display="none";
},_moveSizingLine:function(){
var pos=(this.lastPoint-this.startPoint)+this.sizingSplitter.position;
dojo.style(this.virtualSizer,(this.isHorizontal?"left":"top"),pos+"px");
},_getCookieName:function(i){
return this.id+"_"+i;
},_restoreState:function(){
dojo.forEach(this.getChildren(),function(_427,i){
var _429=this._getCookieName(i);
var _42a=dojo.cookie(_429);
if(_42a){
var pos=parseInt(_42a);
if(typeof pos=="number"){
_427.sizeShare=pos;
}
}
},this);
},_saveState:function(){
if(!this.persist){
return;
}
dojo.forEach(this.getChildren(),function(_42c,i){
dojo.cookie(this._getCookieName(i),_42c.sizeShare,{expires:365});
},this);
}});
dojo.extend(dijit._Widget,{sizeMin:10,sizeShare:10});
}
if(!dojo._hasResource["dijit.form._FormWidget"]){
dojo._hasResource["dijit.form._FormWidget"]=true;
dojo.provide("dijit.form._FormWidget");
dojo.declare("dijit.form._FormWidget",[dijit._Widget,dijit._Templated],{baseClass:"",name:"",alt:"",value:"",type:"text",tabIndex:"0",disabled:false,readOnly:false,intermediateChanges:false,scrollOnFocus:true,attributeMap:dojo.delegate(dijit._Widget.prototype.attributeMap,{value:"focusNode",disabled:"focusNode",readOnly:"focusNode",id:"focusNode",tabIndex:"focusNode",alt:"focusNode"}),postMixInProperties:function(){
this.nameAttrSetting=this.name?("name='"+this.name+"'"):"";
this.inherited(arguments);
},_setDisabledAttr:function(_42e){
this.disabled=_42e;
dojo.attr(this.focusNode,"disabled",_42e);
dijit.setWaiState(this.focusNode,"disabled",_42e);
if(_42e){
this._hovering=false;
this._active=false;
this.focusNode.removeAttribute("tabIndex");
}else{
this.focusNode.setAttribute("tabIndex",this.tabIndex);
}
this._setStateClass();
},setDisabled:function(_42f){
dojo.deprecated("setDisabled("+_42f+") is deprecated. Use attr('disabled',"+_42f+") instead.","","2.0");
this.attr("disabled",_42f);
},_onFocus:function(e){
if(this.scrollOnFocus){
dijit.scrollIntoView(this.domNode);
}
this.inherited(arguments);
},_onMouse:function(_431){
var _432=_431.currentTarget;
if(_432&&_432.getAttribute){
this.stateModifier=_432.getAttribute("stateModifier")||"";
}
if(!this.disabled){
switch(_431.type){
case "mouseenter":
case "mouseover":
this._hovering=true;
this._active=this._mouseDown;
break;
case "mouseout":
case "mouseleave":
this._hovering=false;
this._active=false;
break;
case "mousedown":
this._active=true;
this._mouseDown=true;
var _433=this.connect(dojo.body(),"onmouseup",function(){
if(this._mouseDown&&this.isFocusable()){
this.focus();
}
this._active=false;
this._mouseDown=false;
this._setStateClass();
this.disconnect(_433);
});
break;
}
this._setStateClass();
}
},isFocusable:function(){
return !this.disabled&&!this.readOnly&&this.focusNode&&(dojo.style(this.domNode,"display")!="none");
},focus:function(){
dijit.focus(this.focusNode);
},_setStateClass:function(){
var _434=this.baseClass.split(" ");
function _435(_436){
_434=_434.concat(dojo.map(_434,function(c){
return c+_436;
}),"dijit"+_436);
};
if(this.checked){
_435("Checked");
}
if(this.state){
_435(this.state);
}
if(this.selected){
_435("Selected");
}
if(this.disabled){
_435("Disabled");
}else{
if(this.readOnly){
_435("ReadOnly");
}else{
if(this._active){
_435(this.stateModifier+"Active");
}else{
if(this._focused){
_435("Focused");
}
if(this._hovering){
_435(this.stateModifier+"Hover");
}
}
}
}
var tn=this.stateNode||this.domNode,_439={};
dojo.forEach(tn.className.split(" "),function(c){
_439[c]=true;
});
if("_stateClasses" in this){
dojo.forEach(this._stateClasses,function(c){
delete _439[c];
});
}
dojo.forEach(_434,function(c){
_439[c]=true;
});
var _43d=[];
for(var c in _439){
_43d.push(c);
}
tn.className=_43d.join(" ");
this._stateClasses=_434;
},compare:function(val1,val2){
if((typeof val1=="number")&&(typeof val2=="number")){
return (isNaN(val1)&&isNaN(val2))?0:(val1-val2);
}else{
if(val1>val2){
return 1;
}else{
if(val1<val2){
return -1;
}else{
return 0;
}
}
}
},onChange:function(_441){
},_onChangeActive:false,_handleOnChange:function(_442,_443){
this._lastValue=_442;
if(this._lastValueReported==undefined&&(_443===null||!this._onChangeActive)){
this._resetValue=this._lastValueReported=_442;
}
if((this.intermediateChanges||_443||_443===undefined)&&((typeof _442!=typeof this._lastValueReported)||this.compare(_442,this._lastValueReported)!=0)){
this._lastValueReported=_442;
if(this._onChangeActive){
this.onChange(_442);
}
}
},create:function(){
this.inherited(arguments);
this._onChangeActive=true;
this._setStateClass();
},destroy:function(){
if(this._layoutHackHandle){
clearTimeout(this._layoutHackHandle);
}
this.inherited(arguments);
},setValue:function(_444){
dojo.deprecated("dijit.form._FormWidget:setValue("+_444+") is deprecated.  Use attr('value',"+_444+") instead.","","2.0");
this.attr("value",_444);
},getValue:function(){
dojo.deprecated(this.declaredClass+"::getValue() is deprecated. Use attr('value') instead.","","2.0");
return this.attr("value");
},_layoutHack:function(){
if(dojo.isFF==2&&!this._layoutHackHandle){
var node=this.domNode;
var old=node.style.opacity;
node.style.opacity="0.999";
this._layoutHackHandle=setTimeout(dojo.hitch(this,function(){
this._layoutHackHandle=null;
node.style.opacity=old;
}),0);
}
}});
}
if(!dojo._hasResource["dijit.form.Button"]){
dojo._hasResource["dijit.form.Button"]=true;
dojo.provide("dijit.form.Button");
dojo.declare("dijit.form.Button",dijit.form._FormWidget,{label:"",showLabel:true,iconClass:"",type:"button",baseClass:"dijitButton",templateString:"<span class=\"dijit dijitReset dijitLeft dijitInline\"\n\tdojoAttachEvent=\"ondijitclick:_onButtonClick,onmouseenter:_onMouse,onmouseleave:_onMouse,onmousedown:_onMouse\"\n\t><span class=\"dijitReset dijitRight dijitInline\"\n\t\t><span class=\"dijitReset dijitInline dijitButtonNode\"\n\t\t\t><button class=\"dijitReset dijitStretch dijitButtonContents\"\n\t\t\t\tdojoAttachPoint=\"titleNode,focusNode\" \n\t\t\t\t${nameAttrSetting} type=\"${type}\" value=\"${value}\" waiRole=\"button\" waiState=\"labelledby-${id}_label\"\n\t\t\t\t><span class=\"dijitReset dijitInline\" dojoAttachPoint=\"iconNode\" \n\t\t\t\t\t><span class=\"dijitReset dijitToggleButtonIconChar\">&#10003;</span \n\t\t\t\t></span \n\t\t\t\t><span class=\"dijitReset dijitInline dijitButtonText\" \n\t\t\t\t\tid=\"${id}_label\"  \n\t\t\t\t\tdojoAttachPoint=\"containerNode\"\n\t\t\t\t></span\n\t\t\t></button\n\t\t></span\n\t></span\n></span>\n",attributeMap:dojo.delegate(dijit.form._FormWidget.prototype.attributeMap,{label:{node:"containerNode",type:"innerHTML"},iconClass:{node:"iconNode",type:"class"}}),_onClick:function(e){
if(this.disabled||this.readOnly){
return false;
}
this._clicked();
return this.onClick(e);
},_onButtonClick:function(e){
if(e.type!="click"&&!(this.type=="submit"||this.type=="reset")){
dojo.stopEvent(e);
}
if(this._onClick(e)===false){
e.preventDefault();
}else{
if(this.type=="submit"&&!this.focusNode.form){
for(var node=this.domNode;node.parentNode;node=node.parentNode){
var _44a=dijit.byNode(node);
if(_44a&&typeof _44a._onSubmit=="function"){
_44a._onSubmit(e);
break;
}
}
}
}
},_setValueAttr:function(_44b){
var attr=this.attributeMap.value||"";
if(this[attr.node||attr||"domNode"].tagName=="BUTTON"){
if(_44b!=this.value){
console.debug("Cannot change the value attribute on a Button widget.");
}
}
},_fillContent:function(_44d){
if(_44d&&!("label" in this.params)){
this.attr("label",_44d.innerHTML);
}
},postCreate:function(){
if(this.showLabel==false){
dojo.addClass(this.containerNode,"dijitDisplayNone");
}
dojo.setSelectable(this.focusNode,false);
this.inherited(arguments);
},onClick:function(e){
return true;
},_clicked:function(e){
},setLabel:function(_450){
dojo.deprecated("dijit.form.Button.setLabel() is deprecated.  Use attr('label', ...) instead.","","2.0");
this.attr("label",_450);
},_setLabelAttr:function(_451){
this.containerNode.innerHTML=this.label=_451;
this._layoutHack();
if(this.showLabel==false&&!this.params.title){
this.titleNode.title=dojo.trim(this.containerNode.innerText||this.containerNode.textContent||"");
}
}});
dojo.declare("dijit.form.ToggleButton",dijit.form.Button,{baseClass:"dijitToggleButton",checked:false,attributeMap:dojo.mixin(dojo.clone(dijit.form.Button.prototype.attributeMap),{checked:"focusNode"}),_clicked:function(evt){
this.attr("checked",!this.checked);
},_setCheckedAttr:function(_453){
this.checked=_453;
dojo.attr(this.focusNode||this.domNode,"checked",_453);
dijit.setWaiState(this.focusNode||this.domNode,"pressed",_453);
this._setStateClass();
this._handleOnChange(_453,true);
},setChecked:function(_454){
dojo.deprecated("setChecked("+_454+") is deprecated. Use attr('checked',"+_454+") instead.","","2.0");
this.attr("checked",_454);
},reset:function(){
this._hasBeenBlurred=false;
this.attr("checked",this.params.checked||false);
}});
}
if(!dojo._hasResource["dijit.form.ToggleButton"]){
dojo._hasResource["dijit.form.ToggleButton"]=true;
dojo.provide("dijit.form.ToggleButton");
}
if(!dojo._hasResource["dijit.layout.StackController"]){
dojo._hasResource["dijit.layout.StackController"]=true;
dojo.provide("dijit.layout.StackController");
dojo.declare("dijit.layout.StackController",[dijit._Widget,dijit._Templated,dijit._Container],{templateString:"<span wairole='tablist' dojoAttachEvent='onkeypress' class='dijitStackController'></span>",containerId:"",buttonWidget:"dijit.layout._StackButton",postCreate:function(){
dijit.setWaiRole(this.domNode,"tablist");
this.pane2button={};
this.pane2handles={};
this.pane2menu={};
this._subscriptions=[dojo.subscribe(this.containerId+"-startup",this,"onStartup"),dojo.subscribe(this.containerId+"-addChild",this,"onAddChild"),dojo.subscribe(this.containerId+"-removeChild",this,"onRemoveChild"),dojo.subscribe(this.containerId+"-selectChild",this,"onSelectChild"),dojo.subscribe(this.containerId+"-containerKeyPress",this,"onContainerKeyPress")];
},onStartup:function(info){
dojo.forEach(info.children,this.onAddChild,this);
this.onSelectChild(info.selected);
},destroy:function(){
for(var pane in this.pane2button){
this.onRemoveChild(pane);
}
dojo.forEach(this._subscriptions,dojo.unsubscribe);
this.inherited(arguments);
},onAddChild:function(page,_458){
var _459=dojo.doc.createElement("span");
this.domNode.appendChild(_459);
var cls=dojo.getObject(this.buttonWidget);
var _45b=new cls({label:page.title,closeButton:page.closable},_459);
this.addChild(_45b,_458);
this.pane2button[page]=_45b;
page.controlButton=_45b;
var _45c=[];
_45c.push(dojo.connect(_45b,"onClick",dojo.hitch(this,"onButtonClick",page)));
if(page.closable){
_45c.push(dojo.connect(_45b,"onClickCloseButton",dojo.hitch(this,"onCloseButtonClick",page)));
var _45d=dojo.i18n.getLocalization("dijit","common");
var _45e=new dijit.Menu({targetNodeIds:[_45b.id],id:_45b.id+"_Menu"});
var _45f=new dijit.MenuItem({label:_45d.itemClose});
_45c.push(dojo.connect(_45f,"onClick",dojo.hitch(this,"onCloseButtonClick",page)));
_45e.addChild(_45f);
this.pane2menu[page]=_45e;
}
this.pane2handles[page]=_45c;
if(!this._currentChild){
_45b.focusNode.setAttribute("tabIndex","0");
this._currentChild=page;
}
if(!this.isLeftToRight()&&dojo.isIE&&this._rectifyRtlTabList){
this._rectifyRtlTabList();
}
},onRemoveChild:function(page){
if(this._currentChild===page){
this._currentChild=null;
}
dojo.forEach(this.pane2handles[page],dojo.disconnect);
delete this.pane2handles[page];
var menu=this.pane2menu[page];
if(menu){
menu.destroyRecursive();
delete this.pane2menu[page];
}
var _462=this.pane2button[page];
if(_462){
_462.destroy();
delete this.pane2button[page];
}
},onSelectChild:function(page){
if(!page){
return;
}
if(this._currentChild){
var _464=this.pane2button[this._currentChild];
_464.attr("checked",false);
_464.focusNode.setAttribute("tabIndex","-1");
}
var _465=this.pane2button[page];
_465.attr("checked",true);
this._currentChild=page;
_465.focusNode.setAttribute("tabIndex","0");
var _466=dijit.byId(this.containerId);
dijit.setWaiState(_466.containerNode,"labelledby",_465.id);
},onButtonClick:function(page){
var _468=dijit.byId(this.containerId);
_468.selectChild(page);
},onCloseButtonClick:function(page){
var _46a=dijit.byId(this.containerId);
_46a.closeChild(page);
var b=this.pane2button[this._currentChild];
if(b){
dijit.focus(b.focusNode||b.domNode);
}
},adjacent:function(_46c){
if(!this.isLeftToRight()&&(!this.tabPosition||/top|bottom/.test(this.tabPosition))){
_46c=!_46c;
}
var _46d=this.getChildren();
var _46e=dojo.indexOf(_46d,this.pane2button[this._currentChild]);
var _46f=_46c?1:_46d.length-1;
return _46d[(_46e+_46f)%_46d.length];
},onkeypress:function(e){
if(this.disabled||e.altKey){
return;
}
var _471=null;
if(e.ctrlKey||!e._djpage){
var k=dojo.keys;
switch(e.charOrCode){
case k.LEFT_ARROW:
case k.UP_ARROW:
if(!e._djpage){
_471=false;
}
break;
case k.PAGE_UP:
if(e.ctrlKey){
_471=false;
}
break;
case k.RIGHT_ARROW:
case k.DOWN_ARROW:
if(!e._djpage){
_471=true;
}
break;
case k.PAGE_DOWN:
if(e.ctrlKey){
_471=true;
}
break;
case k.DELETE:
if(this._currentChild.closable){
this.onCloseButtonClick(this._currentChild);
}
dojo.stopEvent(e);
break;
default:
if(e.ctrlKey){
if(e.charOrCode===k.TAB){
this.adjacent(!e.shiftKey).onClick();
dojo.stopEvent(e);
}else{
if(e.charOrCode=="w"){
if(this._currentChild.closable){
this.onCloseButtonClick(this._currentChild);
}
dojo.stopEvent(e);
}
}
}
}
if(_471!==null){
this.adjacent(_471).onClick();
dojo.stopEvent(e);
}
}
},onContainerKeyPress:function(info){
info.e._djpage=info.page;
this.onkeypress(info.e);
}});
dojo.declare("dijit.layout._StackButton",dijit.form.ToggleButton,{tabIndex:"-1",postCreate:function(evt){
dijit.setWaiRole((this.focusNode||this.domNode),"tab");
this.inherited(arguments);
},onClick:function(evt){
dijit.focus(this.focusNode);
},onClickCloseButton:function(evt){
evt.stopPropagation();
}});
}
if(!dojo._hasResource["dijit.layout.StackContainer"]){
dojo._hasResource["dijit.layout.StackContainer"]=true;
dojo.provide("dijit.layout.StackContainer");
dojo.declare("dijit.layout.StackContainer",dijit.layout._LayoutWidget,{doLayout:true,persist:false,baseClass:"dijitStackContainer",_started:false,postCreate:function(){
this.inherited(arguments);
dojo.addClass(this.domNode,"dijitLayoutContainer");
dijit.setWaiRole(this.containerNode,"tabpanel");
this.connect(this.domNode,"onkeypress",this._onKeyPress);
},startup:function(){
if(this._started){
return;
}
var _477=this.getChildren();
dojo.forEach(_477,this._setupChild,this);
if(this.persist){
this.selectedChildWidget=dijit.byId(dojo.cookie(this.id+"_selectedChild"));
}else{
dojo.some(_477,function(_478){
if(_478.selected){
this.selectedChildWidget=_478;
}
return _478.selected;
},this);
}
var _479=this.selectedChildWidget;
if(!_479&&_477[0]){
_479=this.selectedChildWidget=_477[0];
_479.selected=true;
}
dojo.publish(this.id+"-startup",[{children:_477,selected:_479}]);
if(_479){
this._showChild(_479);
}
this.inherited(arguments);
},_setupChild:function(_47a){
this.inherited(arguments);
dojo.removeClass(_47a.domNode,"dijitVisible");
dojo.addClass(_47a.domNode,"dijitHidden");
_47a.domNode.title="";
return _47a;
},addChild:function(_47b,_47c){
this.inherited(arguments);
if(this._started){
dojo.publish(this.id+"-addChild",[_47b,_47c]);
this.layout();
if(!this.selectedChildWidget){
this.selectChild(_47b);
}
}
},removeChild:function(page){
this.inherited(arguments);
if(this._beingDestroyed){
return;
}
if(this._started){
dojo.publish(this.id+"-removeChild",[page]);
this.layout();
}
if(this.selectedChildWidget===page){
this.selectedChildWidget=undefined;
if(this._started){
var _47e=this.getChildren();
if(_47e.length){
this.selectChild(_47e[0]);
}
}
}
},selectChild:function(page){
page=dijit.byId(page);
if(this.selectedChildWidget!=page){
this._transition(page,this.selectedChildWidget);
this.selectedChildWidget=page;
dojo.publish(this.id+"-selectChild",[page]);
if(this.persist){
dojo.cookie(this.id+"_selectedChild",this.selectedChildWidget.id);
}
}
},_transition:function(_480,_481){
if(_481){
this._hideChild(_481);
}
this._showChild(_480);
if(this.doLayout&&_480.resize){
_480.resize(this._containerContentBox||this._contentBox);
}
},_adjacent:function(_482){
var _483=this.getChildren();
var _484=dojo.indexOf(_483,this.selectedChildWidget);
_484+=_482?1:_483.length-1;
return _483[_484%_483.length];
},forward:function(){
this.selectChild(this._adjacent(true));
},back:function(){
this.selectChild(this._adjacent(false));
},_onKeyPress:function(e){
dojo.publish(this.id+"-containerKeyPress",[{e:e,page:this}]);
},layout:function(){
if(this.doLayout&&this.selectedChildWidget&&this.selectedChildWidget.resize){
this.selectedChildWidget.resize(this._contentBox);
}
},_showChild:function(page){
var _487=this.getChildren();
page.isFirstChild=(page==_487[0]);
page.isLastChild=(page==_487[_487.length-1]);
page.selected=true;
dojo.removeClass(page.domNode,"dijitHidden");
dojo.addClass(page.domNode,"dijitVisible");
if(page._onShow){
page._onShow();
}else{
if(page.onShow){
page.onShow();
}
}
},_hideChild:function(page){
page.selected=false;
dojo.removeClass(page.domNode,"dijitVisible");
dojo.addClass(page.domNode,"dijitHidden");
if(page.onHide){
page.onHide();
}
},closeChild:function(page){
var _48a=page.onClose(this,page);
if(_48a){
this.removeChild(page);
page.destroyRecursive();
}
},destroy:function(){
this._beingDestroyed=true;
this.inherited(arguments);
}});
dojo.extend(dijit._Widget,{title:"",selected:false,closable:false,onClose:function(){
return true;
}});
}
if(!dojo._hasResource["dijit.layout.AccordionPane"]){
dojo._hasResource["dijit.layout.AccordionPane"]=true;
dojo.provide("dijit.layout.AccordionPane");
dojo.declare("dijit.layout.AccordionPane",dijit.layout.ContentPane,{constructor:function(){
dojo.deprecated("dijit.layout.AccordionPane deprecated, use ContentPane instead","","2.0");
},onSelected:function(){
}});
}
if(!dojo._hasResource["dijit.layout.AccordionContainer"]){
dojo._hasResource["dijit.layout.AccordionContainer"]=true;
dojo.provide("dijit.layout.AccordionContainer");
dojo.declare("dijit.layout.AccordionContainer",dijit.layout.StackContainer,{duration:dijit.defaultDuration,_verticalSpace:0,baseClass:"dijitAccordionContainer",postCreate:function(){
this.domNode.style.overflow="hidden";
this.inherited(arguments);
dijit.setWaiRole(this.domNode,"tablist");
},startup:function(){
if(this._started){
return;
}
this.inherited(arguments);
if(this.selectedChildWidget){
var _48b=this.selectedChildWidget.containerNode.style;
_48b.display="";
_48b.overflow="auto";
this.selectedChildWidget._buttonWidget._setSelectedState(true);
}
},_getTargetHeight:function(node){
var cs=dojo.getComputedStyle(node);
return Math.max(this._verticalSpace-dojo._getPadBorderExtents(node,cs).h,0);
},layout:function(){
var _48e=this.selectedChildWidget;
var _48f=0;
dojo.forEach(this.getChildren(),function(_490){
_48f+=_490._buttonWidget.getTitleHeight();
});
var _491=this._contentBox;
this._verticalSpace=_491.h-_48f;
this._containerContentBox={h:this._verticalSpace,w:_491.w};
if(_48e){
_48e.resize(this._containerContentBox);
}
},_setupChild:function(_492){
_492._buttonWidget=new dijit.layout._AccordionButton({contentWidget:_492,title:_492.title,id:_492.id+"_button",parent:this});
dojo.place(_492._buttonWidget.domNode,_492.domNode,"before");
this.inherited(arguments);
},removeChild:function(_493){
_493._buttonWidget.destroy();
this.inherited(arguments);
},getChildren:function(){
return dojo.filter(this.inherited(arguments),function(_494){
return _494.declaredClass!="dijit.layout._AccordionButton";
});
},destroy:function(){
dojo.forEach(this.getChildren(),function(_495){
_495._buttonWidget.destroy();
});
this.inherited(arguments);
},_transition:function(_496,_497){
if(this._inTransition){
return;
}
this._inTransition=true;
var _498=[];
var _499=this._verticalSpace;
if(_496){
_496._buttonWidget.setSelected(true);
this._showChild(_496);
if(this.doLayout&&_496.resize){
_496.resize(this._containerContentBox);
}
var _49a=_496.domNode;
dojo.addClass(_49a,"dijitVisible");
dojo.removeClass(_49a,"dijitHidden");
var _49b=_49a.style.overflow;
_49a.style.overflow="hidden";
_498.push(dojo.animateProperty({node:_49a,duration:this.duration,properties:{height:{start:1,end:this._getTargetHeight(_49a)}},onEnd:dojo.hitch(this,function(){
_49a.style.overflow=_49b;
delete this._inTransition;
})}));
}
if(_497){
_497._buttonWidget.setSelected(false);
var _49c=_497.domNode,_49d=_49c.style.overflow;
_49c.style.overflow="hidden";
_498.push(dojo.animateProperty({node:_49c,duration:this.duration,properties:{height:{start:this._getTargetHeight(_49c),end:1}},onEnd:function(){
dojo.addClass(_49c,"dijitHidden");
dojo.removeClass(_49c,"dijitVisible");
_49c.style.overflow=_49d;
if(_497.onHide){
_497.onHide();
}
}}));
}
dojo.fx.combine(_498).play();
},_onKeyPress:function(e,_49f){
if(this._inTransition||this.disabled||e.altKey||!(_49f||e.ctrlKey)){
if(this._inTransition){
dojo.stopEvent(e);
}
return;
}
var k=dojo.keys,c=e.charOrCode;
if((_49f&&(c==k.LEFT_ARROW||c==k.UP_ARROW))||(e.ctrlKey&&c==k.PAGE_UP)){
this._adjacent(false)._buttonWidget._onTitleClick();
dojo.stopEvent(e);
}else{
if((_49f&&(c==k.RIGHT_ARROW||c==k.DOWN_ARROW))||(e.ctrlKey&&(c==k.PAGE_DOWN||c==k.TAB))){
this._adjacent(true)._buttonWidget._onTitleClick();
dojo.stopEvent(e);
}
}
}});
dojo.declare("dijit.layout._AccordionButton",[dijit._Widget,dijit._Templated],{templateString:"<div dojoAttachPoint='titleNode,focusNode' dojoAttachEvent='ondijitclick:_onTitleClick,onkeypress:_onTitleKeyPress,onfocus:_handleFocus,onblur:_handleFocus,onmouseenter:_onTitleEnter,onmouseleave:_onTitleLeave'\n\t\tclass='dijitAccordionTitle' wairole=\"tab\" waiState=\"expanded-false\"\n\t\t><span class='dijitInline dijitAccordionArrow' waiRole=\"presentation\"></span\n\t\t><span class='arrowTextUp' waiRole=\"presentation\">+</span\n\t\t><span class='arrowTextDown' waiRole=\"presentation\">-</span\n\t\t><span waiRole=\"presentation\" dojoAttachPoint='titleTextNode' class='dijitAccordionText'></span>\n</div>\n",attributeMap:dojo.mixin(dojo.clone(dijit.layout.ContentPane.prototype.attributeMap),{title:{node:"titleTextNode",type:"innerHTML"}}),baseClass:"dijitAccordionTitle",getParent:function(){
return this.parent;
},postCreate:function(){
this.inherited(arguments);
dojo.setSelectable(this.domNode,false);
this.setSelected(this.selected);
var _4a2=dojo.attr(this.domNode,"id").replace(" ","_");
dojo.attr(this.titleTextNode,"id",_4a2+"_title");
dijit.setWaiState(this.focusNode,"labelledby",dojo.attr(this.titleTextNode,"id"));
},getTitleHeight:function(){
return dojo.marginBox(this.titleNode).h;
},_onTitleClick:function(){
var _4a3=this.getParent();
if(!_4a3._inTransition){
_4a3.selectChild(this.contentWidget);
dijit.focus(this.focusNode);
}
},_onTitleEnter:function(){
dojo.addClass(this.focusNode,"dijitAccordionTitle-hover");
},_onTitleLeave:function(){
dojo.removeClass(this.focusNode,"dijitAccordionTitle-hover");
},_onTitleKeyPress:function(evt){
return this.getParent()._onKeyPress(evt,this.contentWidget);
},_setSelectedState:function(_4a5){
this.selected=_4a5;
dojo[(_4a5?"addClass":"removeClass")](this.titleNode,"dijitAccordionTitle-selected");
dijit.setWaiState(this.focusNode,"expanded",_4a5);
dijit.setWaiState(this.focusNode,"selected",_4a5);
this.focusNode.setAttribute("tabIndex",_4a5?"0":"-1");
},_handleFocus:function(e){
dojo[(e.type=="focus"?"addClass":"removeClass")](this.focusNode,"dijitAccordionFocused");
},setSelected:function(_4a7){
this._setSelectedState(_4a7);
if(_4a7){
var cw=this.contentWidget;
if(cw.onSelected){
cw.onSelected();
}
}
}});
}
dojo.provide("dijit.nls.tips-dojo_en-us");
dojo.provide("dijit.nls.loading");
dijit.nls.loading._built=true;
dojo.provide("dijit.nls.loading.en_us");
dijit.nls.loading.en_us={"loadingState":"Loading...","errorState":"Sorry, an error occurred"};
dojo.provide("dijit.nls.common");
dijit.nls.common._built=true;
dojo.provide("dijit.nls.common.en_us");
dijit.nls.common.en_us={"buttonOk":"OK","buttonCancel":"Cancel","buttonSave":"Save","itemClose":"Close"};

