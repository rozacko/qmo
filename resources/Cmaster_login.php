<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmaster_login extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
       // $this->load->library('sap'); 
       // $this->is_logged_in();
		date_default_timezone_set("Asia/Bangkok");
        $this->load->model('Memployee/Mmaster_employee');
    }

	public function index(){
		$this->load->view('VLogin/login');
		/*$this->load->view('Template/footer');*/
	}
    public function login() {
			
			$user=$_POST['username'];
			$pass=$_POST['password'];
	
			$check=$this->Mmaster_employee->check_employee($user);

			if($check!=NULL){
				$ldap['user'] = $user . "@smig.corp"; // di tambahn @semenindonesia
				if (empty($pass)) {
					$ldap['pass'] = $pass . 'aascs'; //biar error
				} else {
					$ldap['pass'] = $pass;
				}
			
				$ldap['host'] = '10.15.3.121';
				$ldap['port'] = 389;
				$ldap['conn'] = ldap_connect($ldap['host'], $ldap['port'])or die("Could not conenct to {$ldap['host']}");
				ldap_set_option($ldap['conn'], LDAP_OPT_PROTOCOL_VERSION, 3);
				
				@$ldap['bind'] = ldap_bind($ldap['conn'], $ldap['user'], $ldap['pass']);
				if($ldap['bind']){
						redirect(base_url() . 'index.php/Project/Cproject');
				}
				else{
					echo "<script type=\"text/javascript\">alert('Username atau Password Active Direktori anda salah');
					document.location='index'</script>";
				}
				ldap_close($ldap['conn']);

			}
			else{
					echo "<script type=\"text/javascript\">alert('Username Anda belum terdaftar pada Sistem');
					document.location='index'</script>";
			}
	}
	
	public function logout() {
        $this->session->sess_destroy();

        redirect(base_url(), 'refresh');
    }

}
