function notif(type, textVal = 'Message not found!'){
	bgColorVal = '';
	if(type == 1){
		headingVal 	= 'Success';
		iconVal 	= 'success';
	}else if(type == 2){
		headingVal 	= 'Information';
		iconVal 	= 'info';		
	}else if(type == 3){
		headingVal 	= 'Warning';
		iconVal 	= 'warning';
		bgColorVal 	= '#F1C40F';
	}else if(type == 4){
		headingVal 	= 'Error';
		iconVal 	= 'error';
		bgColorVal 	= '#CD201F';
	}
	
	new PNotify({
        title: headingVal,
        text: textVal,
        type: iconVal,
        styling: 'bootstrap3',
        delay: 3000
    });
}
