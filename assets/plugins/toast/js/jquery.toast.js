// jQuery toast plugin created by Kamran Ahmed copyright MIT license 2015
if ( typeof Object.create !== 'function' ) {
    Object.create = function( obj ) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}

(function( $, window, document, undefined ) {

    "use strict";
    
    var Toast = {

        _positionClasses : ['bottom-left', 'bottom-right', 'top-right', 'top-left', 'bottom-center', 'top-center', 'mid-center'],
        _defaultIcons : ['success', 'error', 'info', 'warning'],

        init: function (options, elem) {
            this.prepareOptions(options, $.toast.options);
            this.process();
        },

        prepareOptions: function(options, options_to_extend) {
            var _options = {};
            if ( ( typeof options === 'string' ) || ( options instanceof Array ) ) {
                _options.text = options;
            } else {
                _options = options;
            }
            this.options = $.extend( {}, options_to_extend, _options );
        },

        process: function () {
            this.setup();
            this.addToDom();
            this.position();
            this.bindToast();
            this.animate();
        },

        setup: function () {
            
            var _toastContent = '';
            
            this._toastEl = this._toastEl || $('<div></div>', {
                class : 'jq-toast-single'
            });

            // For the loader on top
            _toastContent += '<span class="jq-toast-loader"></span>';            

            if ( this.options.allowToastClose ) {
                _toastContent += '<span class="close-jq-toast-single">&times;</span>';
            };

            if ( this.options.text instanceof Array ) {

                if ( this.options.heading ) {
                    _toastContent +='<h2 class="jq-toast-heading">' + this.options.heading + '</h2>';
                };

                _toastContent += '<ul class="jq-toast-ul">';
                for (var i = 0; i < this.options.text.length; i++) {
                    _toastContent += '<li class="jq-toast-li" id="jq-toast-item-' + i + '">' + this.options.text[i] + '</li>';
                }
                _toastContent += '</ul>';

            } else {
                if ( this.options.heading ) {
                    _toastContent +='<h2 class="jq-toast-heading">' + this.options.heading + '</h2>';
                };
                _toastContent += this.options.text;
            }

            this._toastEl.html( _toastContent );

            if ( this.options.bgColor !== false ) {
                this._toastEl.css("background-color", this.options.bgColor);
            };

            if ( this.options.textColor !== false ) {
                this._toastEl.css("color", this.options.textColor);
            };

            if ( this.options.textAlign ) {
                this._toastEl.css('text-align', this.options.textAlign);
            }

            if ( this.options.icon !== false ) {
                this._toastEl.addClass('jq-has-icon');

                if ( $.inArray(this.options.icon, this._defaultIcons) !== -1 ) {
                    this._toastEl.addClass('jq-icon-' + this.options.icon);
                };
            };
        },

        position: function () {
            if ( ( typeof this.options.position === 'string' ) && ( $.inArray( this.options.position, this._positionClasses) !== -1 ) ) {

                if ( this.options.position === 'bottom-center' ) {
                    this._container.css({
                        left: ( $(window).outerWidth() / 2 ) - this._container.outerWidth()/2,
                        bottom: 20
                    });
                } else if ( this.options.position === 'top-center' ) {
                    this._container.css({
                        left: ( $(window).outerWidth() / 2 ) - this._container.outerWidth()/2,
                        top: 20
                    });
                } else if ( this.options.position === 'mid-center' ) {
                    this._container.css({
                        left: ( $(window).outerWidth() / 2 ) - this._container.outerWidth()/2,
                        top: ( $(window).outerHeight() / 2 ) - this._container.outerHeight()/2
                    });
                } else {
                    this._container.addClass( this.options.position );
                }

            } else if ( typeof this.options.position === 'object' ) {
                this._container.css({
                    top : this.options.position.top ? this.options.position.top : 'auto',
                    bottom : this.options.position.bottom ? this.options.position.bottom : 'auto',
                    left : this.options.position.left ? this.options.position.left : 'auto',
                    right : this.options.position.right ? this.options.position.right : 'auto'
                });
            } else {
                this._container.addClass( 'bottom-left' );
            }
        },

        bindToast: function () {

            var that = this;

            this._toastEl.on('afterShown', function () {
                that.processLoader();
            });

            this._toastEl.find('.close-jq-toast-single').on('click', function ( e ) {

                e.preventDefault();

                if( that.options.showHideTransition === 'fade') {
                    that._toastEl.trigger('beforeHide');
                    that._toastEl.fadeOut(function () {
                        that._toastEl.trigger('afterHidden');
                    });
                } else if ( that.options.showHideTransition === 'slide' ) {
                    that._toastEl.trigger('beforeHide');
                    that._toastEl.slideUp(function () {
                        that._toastEl.trigger('afterHidden');
                    });
                } else {
                    that._toastEl.trigger('beforeHide');
                    that._toastEl.hide(function () {
                        that._toastEl.trigger('afterHidden');
                    });
                }
            });

            if ( typeof this.options.beforeShow == 'function' ) {
                this._toastEl.on('beforeShow', function () {
                    that.options.beforeShow();
                });
            };

            if ( typeof this.options.afterShown == 'function' ) {
                this._toastEl.on('afterShown', function () {
                    that.options.afterShown();
                });
            };

            if ( typeof this.options.beforeHide == 'function' ) {
                this._toastEl.on('beforeHide', function () {
                    that.options.beforeHide();
                });
            };

            if ( typeof this.options.afterHidden == 'function' ) {
                this._toastEl.on('afterHidden', function () {
                    that.options.afterHidden();
                });
            };          
        },

        addToDom: function () {

             var _container = $('.jq-toast-wrap');
             
             if ( _container.length === 0 ) {
                
                _container = $('<div></div>',{
                    class: "jq-toast-wrap"
                });

                $('body').append( _container );

             } else if ( !this.options.stack || isNaN( parseInt(this.options.stack, 10) ) ) {
                _container.empty();
             }

             _container.find('.jq-toast-single:hidden').remove();

             _container.append( this._toastEl );

            if ( this.options.stack && !isNaN( parseInt( this.options.stack ), 10 ) ) {
                
                var _prevToastCount = _container.find('.jq-toast-single').length,
                    _extToastCount = _prevToastCount - this.options.stack;

                if ( _extToastCount > 0 ) {
                    $('.jq-toast-wrap').find('.jq-toast-single').slice(0, _extToastCount).remove();
                };

            }

            this._container = _container;
        },

        canAutoHide: function () {
            return ( this.options.hideAfter !== false ) && !isNaN( parseInt( this.options.hideAfter, 10 ) );
        },

        processLoader: function () {
            // Show the loader only, if auto-hide is on and loader is demanded
            if (!this.canAutoHide() || this.options.loader === false) {
                return false;
            }

            var loader = this._toastEl.find('.jq-toast-loader');

            // 400 is the default time that jquery uses for fade/slide
            // Divide by 1000 for milliseconds to seconds conversion
            var transitionTime = (this.options.hideAfter - 400) / 1000 + 's';
            var loaderBg = this.options.loaderBg;

            var style = loader.attr('style') || '';
            style = style.substring(0, style.indexOf('-webkit-transition')); // Remove the last transition definition

            style += '-webkit-transition: width ' + transitionTime + ' ease-in; \
                      -o-transition: width ' + transitionTime + ' ease-in; \
                      transition: width ' + transitionTime + ' ease-in; \
                      background-color: ' + loaderBg + ';';


            loader.attr('style', style).addClass('jq-toast-loaded');
        },

        animate: function () {

            var that = this;

            this._toastEl.hide();

            this._toastEl.trigger('beforeShow');

            if ( this.options.showHideTransition.toLowerCase() === 'fade' ) {
                this._toastEl.fadeIn(function ( ){
                    that._toastEl.trigger('afterShown');
                });
            } else if ( this.options.showHideTransition.toLowerCase() === 'slide' ) {
                this._toastEl.slideDown(function ( ){
                    that._toastEl.trigger('afterShown');
                });
            } else {
                this._toastEl.show(function ( ){
                    that._toastEl.trigger('afterShown');
                });
            }

            if (this.canAutoHide()) {

                var that = this;

                window.setTimeout(function(){
                    
                    if ( that.options.showHideTransition.toLowerCase() === 'fade' ) {
                        that._toastEl.trigger('beforeHide');
                        that._toastEl.fadeOut(function () {
                            that._toastEl.trigger('afterHidden');
                        });
                    } else if ( that.options.showHideTransition.toLowerCase() === 'slide' ) {
                        that._toastEl.trigger('beforeHide');
                        that._toastEl.slideUp(function () {
                            that._toastEl.trigger('afterHidden');
                        });
                    } else {
                        that._toastEl.trigger('beforeHide');
                        that._toastEl.hide(function () {
                            that._toastEl.trigger('afterHidden');
                        });
                    }

                }, this.options.hideAfter);
            };
        },

        reset: function ( resetWhat ) {

            if ( resetWhat === 'all' ) {
                $('.jq-toast-wrap').remove();
            } else {
                this._toastEl.remove();
            }

        },

        update: function(options) {
            this.prepareOptions(options, this.options);
            this.setup();
            this.bindToast();
        }
    };
    
    $.toast = function(options) {
        var toast = Object.create(Toast);
        toast.init(options, this);

        return {
            
            reset: function ( what ) {
                toast.reset( what );
            },

            update: function( options ) {
                toast.update( options );
            }
        }
    };

    $.toast.options = {
        text: '',
        heading: '',
        showHideTransition: 'fade',
        allowToastClose: true,
        hideAfter: 3000,
        loader: true,
        loaderBg: '#9EC600',
        stack: 5,
        position: 'bottom-left',
        bgColor: false,
        textColor: false,
        textAlign: 'left',
        icon: false,
        beforeShow: function () {},
        afterShown: function () {},
        beforeHide: function () {},
        afterHidden: function () {}
    };

})( jQuery, window, document );

// You are about to see some extremely horrible code that can be MUCH MUCH improved,
            // I've knowlingly done it that way, please don't judge me based upon this ;)
            $(document).ready(function () {
                
                function generateCode () {
                    var text = $('.plugin-options #toast-text').val(); 
                    var heading = $('.plugin-options #toast-heading').val(); 
                    var transition = $('.toast-transition').val(); 
                    var allowToastClose = $('#allow-toast-close').val(); 
                    var autoHide = $('#auto-hide-toast').val(); 
                    var stackToasts = $('#stack-toasts').val(); 
                    var toastPosition = $('#toast-position').val() 
                    var toastBg = $('#toast-bg').val(); 
                    var toastTextColor = $('#toast-text-color').val();
                    var toastIcon = $('#icon-type').val();
                    var textAlign = $('#text-align').val();
                    var toastEvents = $('#add-toast-events').val();
                    var loader = $('#show-loader').val();
                    var loaderBg = $('#loader-bg').val();

                    if ( text ) {
                        $('.toast-text-line').show(); 
                        $('.toast-text-line .toast-text').text( text ); 
                    } else {
                        $('.toast-text-line').hide() 
                        $('.toast-text-line .toast-text').text(''); 
                    };

                    if ( heading ) {
                        $('.toast-heading-line').show(); 
                        $('.toast-heading-line .toast-heading').text( heading ); 
                    } else {
                        $('.toast-heading-line').hide() 
                        $('.toast-heading-line .toast-heading').text(''); 
                    }; 

                    if ( transition ) {
                        $('.toast-transition-line').show() 
                        $('.toast-transition-line .toast-transition').text( transition ); 
                    } else {
                        $('.toast-transition-line').hide(); 
                        $('.toast-transition-line .toast-transition').text('fade'); 
                    } 

                    if ( allowToastClose ) {
                        $('.toast-allowToastClose-line').show(); 
                        $('.toast-allowToastClose-line .toast-allowToastClose').text( allowToastClose ); 
                    } else {
                        $('.toast-allowToastClose-line').hide(); 
                        $('.toast-allowToastClose-line .toast-allowToastClose').text( false ); 
                    } 

                    if ( autoHide && ( autoHide == 'false' ) ) {
                        $('.toast-hideAfter-line').show(); 
                        $('.toast-hideAfter-line .toast-hideAfter').text('false'); 
                        $('.autohide-after').hide(); 
                    } else {
                        $('.toast-hideAfter-line').show(); 
                        $('.toast-hideAfter-line .toast-hideAfter').text( $('#autohide-after').val() ? $('#autohide-after').val() : 3000 ); 
                        $('.autohide-after').show(); 
                    } 

                    if ( stackToasts && stackToasts != 'true') {
                        $('.toast-stackLength-line').show(); 
                        $('.toast-stackLength-line .toast-stackLength').text( stackToasts ); 
                        $('.stack-length').hide(); 
                    } else {
                        $('.stack-length').show(); 
                        $('.toast-stackLength-line').show(); 
                        $('.toast-stackLength-line .toast-stackLength').text( $('#stack-length').val() ? $('#stack-length').val() : 5 ); 
                    } 

                    if ( toastPosition && ( toastPosition !== 'custom-position' ) ) {
                        $('.toast-position-string-line').show(); 
                        $('.custom-toast-position').hide(); 
                        $('.toast-position-string-line .toast-position').text( toastPosition ); 
                    } else {
                        $('.toast-position-string-line').hide(); 
                        $('.toast-position-string-line .toast-position').text(''); 
                    } 

                    if ( toastPosition && ( toastPosition === 'custom-position' ) ) {
                        $('.custom-toast-position').show(); 
                        $('.toast-position-string-obj').show(); 
                        var left = $('#left-position').val() ? $('#left-position').val() : 'auto'; 
                        var right = $('#right-position').val() ? $('#right-position').val() : 'auto'; 
                        var top = $('#top-position').val() ? $('#top-position').val() : 'auto'; 
                        var bottom = $('#bottom-position').val() ? $('#bottom-position').val() : 'auto'; 
                        $('.toast-position-string-obj .toast-position-left').text( ( left !== 'auto' ) ? left : "'" + left + "'" ); 
                        $('.toast-position-string-obj .toast-position-right').text( ( right !== 'auto' ) ? right : "'" + right + "'" ); 
                        $('.toast-position-string-obj .toast-position-top').text( ( top !== 'auto' ) ? top : "'" + top + "'" ); 
                        $('.toast-position-string-obj .toast-position-bottom').text(  ( bottom !== 'auto' ) ? bottom : "'" + bottom + "'"  ); 
                    } else {
                        $('.toast-position-string-obj').hide(); 
                        // $('.toast-position-string-obj toast-position').text('');
                    } 

                    if ( !toastIcon ) {
                        if ( toastBg ) {
                            $('.toast-bgColor-line').show(); 
                            $('.toast-bgColor-line .toast-bgColor').text( toastBg ); 
                        } else {
                            $('.toast-bgColor-line').hide(); 
                            $('.toast-bgColor-line .toast-bgColor').text(''); 
                        } 

                        if ( toastTextColor ) {
                            $('.toast-textColor-line').show(); 
                            $('.toast-textColor-line .toast-textColor').text( toastTextColor ); 
                        } else {
                            $('.toast-textColor-line').hide(); 
                            $('.toast-textColor-line .toast-textColor').text(''); 
                        } 
                    }

                    if ( textAlign ) {
                        $('.toast-textAlign-line').show(); 
                        $('.toast-textAlign-line .toast-textAlign').text( textAlign ); 
                    } else {
                        $('.toast-textAlign-line').hide(); 
                        $('.toast-textAlign-line .toast-textAlign').text( ''); 
                    } 

                    if (loader == 'false') {
                        $('.toast-textLoader').html('false');
                    } else {
                        $('.toast-textLoader').html('true');
                    }
                    
                    if (loaderBg) {
                        $('.toast-textLoaderBg').html(loaderBg);
                    }

                    if ( toastEvents == 'false' ) {
                        $('.toast-beforeShow-line').hide(); 
                        $('.toast-afterShown-line').hide(); 
                        $('.toast-beforeHide-line').hide(); 
                        $('.toast-afterHidden-line').hide(); 
                    } else {
                        $('.toast-beforeShow-line').show(); 
                        $('.toast-afterShown-line').show(); 
                        $('.toast-beforeHide-line').show(); 
                        $('.toast-afterHidden-line').show(); 
                    } 
                }

                $('#top-position').on('change', function () { $('#bottom-position').val('auto'); });
                $('#bottom-position').on('change', function () { $('#top-position').val('auto'); });
                $('#left-position').on('change', function () { $('#right-position').val('auto'); });
                $('#right-position').on('change', function () {$('#left-position').val('auto'); });
                $('.plugin-options :input').on('change', function () {
                  $.toast().reset('all');
                  generateCode();
                });

                $('.generate-toast').on('click', function( e ) {
                  e.preventDefault();
                  generateToast();
                });

                function generateToast () {
                    var options = {};

                    if ( $('.toast-text-line').is(':visible') ) {
                        options.text = $('.toast-text-line .toast-text').text();
                    } 

                    if ( $('.toast-heading-line').is(':visible') ) {
                        options.heading = $('.toast-heading').text(); 
                    }; 

                    if ( $('.toast-transition-line').is(':visible') ) {
                        options.showHideTransition = $('.toast-transition-line .toast-transition').text(); 
                    }; 

                    if ( $('.toast-allowToastClose-line').is(':visible') ) {
                        options.allowToastClose = ( $('.toast-allowToastClose-line .toast-allowToastClose').text() === 'true' ) ? true : false; 
                    }; 

                    if ( $('.toast-hideAfter-line').is(':visible') ) {
                        options.hideAfter = parseInt($('.toast-hideAfter-line .toast-hideAfter').text(), 10) || false; 
                    }; 

                    if ( $('.toast-stackLength-line').is(':visible') ) {
                        options.stack = parseInt($('.toast-stackLength-line .toast-stackLength').text(), 10) || false; 
                    }; 

                    if ( $('.toast-position-string-line').is(':visible') ) {
                        options.position = $('.toast-position-string-line .toast-position').text(); 
                    }; 

                    if ( $('.toast-position-string-obj').is(':visible') ) {
                        options.position = {}; 
                        options.position.left =  parseFloat( $('.toast-position .toast-position-left').text() ) || 'auto'; 
                        options.position.right =  parseFloat( $('.toast-position .toast-position-right').text() ) || 'auto'; 
                        options.position.top =  parseFloat( $('.toast-position .toast-position-top').text() ) || 'auto'; 
                        options.position.bottom =  parseFloat( $('.toast-position .toast-position-bottom').text() ) || 'auto'; 
                    }; 

                    if ( $('.toast-icon-line').is(':visible') ) {
                        options.icon = $('.toast-icon-line .toast-icon').text();
                    };

                    if ( $('.toast-bgColor-line').is(':visible') ) {
                        options.bgColor = $('#toast-bg').val(); 
                    }; 

                    if ( $('.toast-text-color').is(':visible') ) {
                        options.textColor = $('#toast-text-color').val(); 
                    }; 

                    if ( $("#text-align").is(':visible') ) {
                        options.textAlign = $('#text-align').val(); 
                    };

                    options.loader = $('.toast-textLoader').html() === 'false' ? false : true;
                    options.loaderBg = $('.toast-textLoaderBg').html();

                    $.toast( options ); 
                }

                generateCode(); 
            });

			
	function show_toast(a, b){
		var toast_notify, toast_icon, toast_background, toast_heading;
		if(a == 1){
			toast_icon = 'success';
			toast_heading = '<b>Sukses</b>';
			toast_background = 'green';
		}else if(a == 2){
			toast_icon = 'error';
			toast_heading = '<b>Gagal!</b>';
			toast_background = 'brown';
		}else if(a == 3){
			toast_icon = 'warning';
			toast_heading = 'Peringatan!';
			toast_background = '#FF5400';
		}else if(a == 4){
			toast_icon = 'info';
			toast_heading = 'Info!';
			toast_background = '#6FB3E0';
		}
		
		$.toast({
			heading: toast_heading,
			text: b,
			showHideTransition: 'slide',
			position: 'top-right',
			bgColor: toast_background,
			icon: toast_icon
		});
	}
